DROP TABLE marketplace_db.Company_Client;
CREATE TABLE marketplace_db.Company_Client(
    Company_Client_ID    INT             AUTO_INCREMENT,
    Company_ID           INT             NOT NULL,
    Client_Name          VARCHAR(128)    NOT NULL,
    Create_User_ID       INT             NOT NULL,
    Create_Dt            DATETIME        NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME        NOT NULL,
    PRIMARY KEY (Company_Client_ID), 
    CONSTRAINT FK_Company_Client_Company_ID FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
)ENGINE=INNODB
;



