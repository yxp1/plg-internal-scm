use marketplace_db;

ALTER TABLE marketplace_db.Company ADD COLUMN Business_Headline       VARCHAR(1024);
ALTER TABLE marketplace_db.Company ADD COLUMN Years_Of_Operation      INT;
ALTER TABLE marketplace_db.Company ADD COLUMN Business_Description    VARCHAR(1024);

CREATE TABLE marketplace_db.Company_Branch(
    Company_Branch_ID     INT             AUTO_INCREMENT,
    Branch_Name           VARCHAR(128)    NOT NULL,
    Is_Truck_Pool         BOOL            NOT NULL,
    Company_ID            INT             NOT NULL,
    Company_Address_ID    INT             NOT NULL,
    Create_User_ID        INT             NOT NULL,
    Create_Dt             DATETIME        NOT NULL,
    Update_User_ID        INT,
    Update_Dt             DATETIME        NOT NULL,
    PRIMARY KEY (Company_Branch_ID), 
    CONSTRAINT FK_Company_Branch_Company_ID FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID),
    CONSTRAINT FK_Company_Branch_Company_Address_ID FOREIGN KEY (Company_Address_ID)
    REFERENCES Company_Address(Company_Address_ID)
)ENGINE=INNODB
;



