use marketplace_db;

ALTER TABLE `user` ADD COLUMN `Is_Password_Weak` bit(1) NOT NULL DEFAULT b'0';

-- DROP TABLE IF EXISTS `email_log`;
CREATE TABLE `email_log` (
  `Email_Log_ID` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(128) NOT NULL,
  `sendTo` varchar(128) NOT NULL,
  `subject` varchar(128) NOT NULL,
  `message` varchar(2048) NOT NULL,
--  `Create_User_ID` int(11) NOT NULL,
  `Create_Dt` datetime NOT NULL,
  PRIMARY KEY (`Email_Log_ID`)
--  CONSTRAINT `FK_Create_User_ID_User_Id` FOREIGN KEY (`Create_User_ID`) REFERENCES `User` (`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

