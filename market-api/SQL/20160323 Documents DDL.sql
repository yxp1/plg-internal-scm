DROP TABLE IF EXISTS tms_utf8_db.File_Document_Type;
use marketplace_db;

-- 
-- TABLE: Document_Type 
--

CREATE TABLE marketplace_db.Document_Type(
    Document_Type_Id    INT             NOT NULL,
    Description_Txt     VARCHAR(128)    NOT NULL,
    PRIMARY KEY (Document_Type_Id)
)
;


-- 
-- TABLE: File_Document_Type 
--

CREATE TABLE marketplace_db.File_Document_Type(
    File_Document_Type_Id    INT    AUTO_INCREMENT,
    Company_File_ID          INT    NOT NULL,
    Document_Type_Id         INT    NOT NULL,
    PRIMARY KEY (File_Document_Type_Id)
)
;

-- 
-- TABLE: File_Document_Type 
--

ALTER TABLE marketplace_db.File_Document_Type ADD CONSTRAINT FK_File_Document_Type_Company_File_ID 
    FOREIGN KEY (Company_File_ID)
    REFERENCES Company_File(Company_File_ID)
;

ALTER TABLE marketplace_db.File_Document_Type ADD CONSTRAINT FK_File_Document_Type_Document_Type_Id 
    FOREIGN KEY (Document_Type_Id)
    REFERENCES Document_Type(Document_Type_Id)
;


ALTER TABLE marketplace_db.Company_File ADD COLUMN Document_Name        VARCHAR(128)    NOT NULL AFTER Is_Legal_Document;
ALTER TABLE marketplace_db.Company_File ADD COLUMN Relative_Path        VARCHAR(128)    NOT NULL AFTER Document_Name;
