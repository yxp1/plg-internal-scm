﻿use marketplace_db;
DROP VIEW `vw_carrier`;
CREATE VIEW `vw_carrier` AS 
select c.company_id, c.company_name, c.company_code, sa.service_area_name, s.service_name

, s.Is_Transportation
, cs.Company_Service_ID, cs.Service_ID 
, cr.Carrier_Region_ID, cr.Service_Area_ID

from `company` `c` 
left join `company_address` `hq` on `hq`.`Company_Address_ID` = `cmpny`.`Corporate_Address_ID`
left join `province` `hq_p` on `hq_p`.`Province_ID` = `hq`.`Province_ID`
left join `country` `hq_cntry` on `hq_cntry`.`Country_ID` = `hq`.`Country_ID` 
left join `company_address` `truckyard` on `truckyard`.`Company_Address_ID` = `cmpny`.`Truck_Pool_Address_ID` 
left join `province` `ty_p` on `ty_p`.`Province_ID` = `hq`.`Province_ID`
left join `country` `ty_cntry` on `ty_cntry`.`Country_ID` = `hq`.`Country_ID` 
left join `company_address` `branch` on `branch`.`Company_Address_ID` = `cmpny`.`Truck_Pool_Address_ID`
left join `province` `b_p` on `b_p`.`Province_ID` = `hq`.`Province_ID`
left join `country` `b_cntry` on `b_cntry`.`Country_ID` = `hq`.`Country_ID` 

left join `company_service` `cs` on `cs`.`Company_ID` = `c`.`Company_ID`
left join `service` `s` on `s`.`Service_ID` = `cs`.`Service_ID`
left join `carrier_region` `cr` on `cr`.`Company_ID` = `c`.`Company_ID` 
left join `service_area` `sa` on `sa`.`Service_Area_ID` = `cr`.`Service_Area_ID`
where c.is_carrier = 1;
