
drop database marketplace_db;

create database marketplace_db;
use marketplace_db;



-- Date Created : Thursday, January 21, 2016 00:40:34
-- Target DBMS : MySQL 5.x
--

-- 
-- TABLE: Carrier_Region 
--

CREATE TABLE Carrier_Region(
    Carrier_Region_ID    INT         AUTO_INCREMENT,
    Company_ID           INT         NOT NULL,
    Service_Area_ID      INT         NOT NULL,
    Create_User_ID       INT         NOT NULL,
    Create_Dt            DATETIME    NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME    NOT NULL,
    PRIMARY KEY (Carrier_Region_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company 
--

CREATE TABLE Company(
    Company_ID               INT             AUTO_INCREMENT,
    Company_Name             VARCHAR(256),
    Is_Carrier               BIT(1)          NOT NULL,
    Is_Shipper               BIT(1)          NOT NULL,
    Company_Code             VARCHAR(8),
    Corporate_Address_ID     INT             NOT NULL,
    Truck_Pool_Address_ID    INT             NOT NULL,
    Branch_Address_ID        INT             NOT NULL,
    Create_User_ID           INT             NOT NULL,
    Create_Dt                DATETIME        NOT NULL,
    Update_User_ID           INT,
    Update_Dt                DATETIME        NOT NULL,
    PRIMARY KEY (Company_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company_Address 
--

CREATE TABLE Company_Address(
    Company_Address_ID      INT              NOT NULL,
    Address_Line_1          VARCHAR(256),
    Address_Line_2          VARCHAR(256),
    Address_Line_3          VARCHAR(256),
    Province_ID             INT              NOT NULL,
    Country_ID              INT              NOT NULL,
    Years_Of_Operation      INT,
    Business_Headline       VARCHAR(1024),
    Business_Description    VARCHAR(1024),
    Create_User_ID          INT              NOT NULL,
    Create_Dt               DATETIME         NOT NULL,
    Update_User_ID          INT,
    Update_Dt               DATETIME         NOT NULL,
    PRIMARY KEY (Company_Address_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company_Client 
--

CREATE TABLE Company_Client(
    Company_Client_ID    CHAR(10)        NOT NULL,
    Company_ID           INT             NOT NULL,
    Client_Name          VARCHAR(128)    NOT NULL,
    Create_User_ID       INT             NOT NULL,
    Create_Dt            DATETIME        NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME        NOT NULL,
    PRIMARY KEY (Company_Client_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company_Contract 
--

CREATE TABLE Company_Contract(
    Company_Contract_ID    INT         AUTO_INCREMENT,
    Company_ID             INT         NOT NULL,
    Contract_Type_ID       INT         NOT NULL,
    Payment_Term_ID        INT         NOT NULL,
    Create_User_ID         INT         NOT NULL,
    Create_Dt              DATETIME    NOT NULL,
    Update_User_ID         INT,
    Update_Dt              DATETIME    NOT NULL,
    PRIMARY KEY (Company_Contract_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company_Equipment 
--

CREATE TABLE Company_Equipment(
    Company_Equipment_ID    INT               AUTO_INCREMENT,
    Company_ID              INT               NOT NULL,
    Equipment_ID            INT               NOT NULL,
    Equipment_Count         DECIMAL(10, 2)    NOT NULL,
    Create_User_ID          INT               NOT NULL,
    Create_Dt               DATETIME          NOT NULL,
    Update_User_ID          INT,
    Update_Dt               DATETIME          NOT NULL,
    PRIMARY KEY (Company_Equipment_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company_File 
--

CREATE TABLE Company_File(
    Company_File_ID      INT         AUTO_INCREMENT,
    Company_ID           INT         NOT NULL,
    Is_Legal_Document    BIT(1)      NOT NULL,
    Create_User_ID       INT         NOT NULL,
    Create_Dt            DATETIME    NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME    NOT NULL,
    PRIMARY KEY (Company_File_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company_Service 
--

CREATE TABLE Company_Service(
    Company_Service_ID    INT         AUTO_INCREMENT,
    Company_ID            INT         NOT NULL,
    Service_ID            INT         NOT NULL,
    Create_User_ID        INT         NOT NULL,
    Create_Dt             DATETIME    NOT NULL,
    Update_User_ID        INT,
    Update_Dt             DATETIME    NOT NULL,
    PRIMARY KEY (Company_Service_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Company_User 
--

CREATE TABLE Company_User(
    Company_User_ID         INT         AUTO_INCREMENT,
    Company_ID              INT         NOT NULL,
    User_ID                 INT         NOT NULL,
    Is_Primary_Contact      BIT(1)      NOT NULL,
    Is_Technical_Contact    BIT(1)      NOT NULL,
    Create_User_ID          INT         NOT NULL,
    Create_Dt               DATETIME    NOT NULL,
    Update_User_ID          INT,
    Update_Dt               DATETIME    NOT NULL,
    PRIMARY KEY (Company_User_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Contract_Type 
--

CREATE TABLE Contract_Type(
    Contract_Type_ID      INT             AUTO_INCREMENT,
    Contract_Type_Name    VARCHAR(128)    NOT NULL,
    Create_User_ID        INT             NOT NULL,
    Create_Dt             DATETIME        NOT NULL,
    Update_User_ID        INT,
    Update_Dt             DATETIME        NOT NULL,
    PRIMARY KEY (Contract_Type_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Country 
--

CREATE TABLE Country(
    Country_ID        INT             AUTO_INCREMENT,
    Country_Name      VARCHAR(128)    NOT NULL,
    Create_User_ID    INT             NOT NULL,
    Create_Dt         DATETIME        NOT NULL,
    Update_User_ID    INT,
    Update_Dt         DATETIME        NOT NULL,
    PRIMARY KEY (Country_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Equipment 
--

CREATE TABLE Equipment(
    Equipment_ID         INT             AUTO_INCREMENT,
    Equipment_Type_ID    INT             NOT NULL,
    Equipment_Name       VARCHAR(128)    NOT NULL,
    Create_User_ID       INT             NOT NULL,
    Create_Dt            DATETIME        NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME        NOT NULL,
    PRIMARY KEY (Equipment_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Equipment_Type 
--

CREATE TABLE Equipment_Type(
    Equipment_Type_ID      INT             AUTO_INCREMENT,
    Equipment_Type_Name    VARCHAR(128)    NOT NULL,
    Create_User_ID         INT             NOT NULL,
    Create_Dt              DATETIME        NOT NULL,
    Update_User_ID         INT,
    Update_Dt              DATETIME        NOT NULL,
    PRIMARY KEY (Equipment_Type_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Language 
--

CREATE TABLE Language(
    Language_ID       INT             AUTO_INCREMENT,
    Language_Name     VARCHAR(128)    NOT NULL,
    Iso_639_2_CD      CHAR(2),
    Create_User_ID    INT             NOT NULL,
    Create_Dt         DATETIME        NOT NULL,
    Update_User_ID    INT,
    Update_Dt         DATETIME        NOT NULL,
    PRIMARY KEY (Language_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Payment_Term 
--

CREATE TABLE Payment_Term(
    Payment_Term_ID    INT             AUTO_INCREMENT,
    Payment_Term       VARCHAR(128)    NOT NULL,
    Create_User_ID     INT             NOT NULL,
    Create_Dt          DATETIME        NOT NULL,
    Update_User_ID     INT,
    Update_Dt          DATETIME        NOT NULL,
    PRIMARY KEY (Payment_Term_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Privilege 
--

CREATE TABLE Privilege(
    Privilege_ID      CHAR(10)        NOT NULL,
    Privilege_Name    VARCHAR(128)    NOT NULL,
    Create_User_ID    INT             NOT NULL,
    Create_Dt         DATETIME        NOT NULL,
    Update_User_ID    INT,
    Update_Dt         DATETIME        NOT NULL,
    PRIMARY KEY (Privilege_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Province 
--

CREATE TABLE Province(
    Province_ID       INT             AUTO_INCREMENT,
    Country_ID        INT             NOT NULL,
    Province_Name     VARCHAR(128)    NOT NULL,
    Create_User_ID    INT             NOT NULL,
    Create_Dt         DATETIME        NOT NULL,
    Update_User_ID    INT,
    Update_Dt         DATETIME        NOT NULL,
    PRIMARY KEY (Province_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: role 
--

CREATE TABLE role(
    Role_Id           INT            AUTO_INCREMENT,
    Role_Name         VARCHAR(64),
    Create_User_ID    INT            NOT NULL,
    Create_Dt         DATETIME       NOT NULL,
    Update_User_ID    INT,
    Update_Dt         DATETIME       NOT NULL,
    PRIMARY KEY (Role_Id)
)ENGINE=INNODB
;



-- 
-- TABLE: Role_Privilege 
--

CREATE TABLE Role_Privilege(
    Role_Privilege_ID    CHAR(10)    NOT NULL,
    Role_Id              INT         NOT NULL,
    Privilege_ID         CHAR(10)    NOT NULL,
    Create_User_ID       INT         NOT NULL,
    Create_Dt            DATETIME    NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME    NOT NULL,
    PRIMARY KEY (Role_Privilege_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Service 
--

CREATE TABLE Service(
    Service_ID           INT             AUTO_INCREMENT,
    Service_Name         VARCHAR(128)    NOT NULL,
    Is_Transportation    BIT(1)          NOT NULL,
    Create_User_ID       INT             NOT NULL,
    Create_Dt            DATETIME        NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME        NOT NULL,
    PRIMARY KEY (Service_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: Service_Area 
--

CREATE TABLE Service_Area(
    Service_Area_ID      INT             AUTO_INCREMENT,
    Service_Area_Name    VARCHAR(128)    NOT NULL,
    Create_User_ID       INT             NOT NULL,
    Create_Dt            DATETIME        NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME        NOT NULL,
    PRIMARY KEY (Service_Area_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: User 
--

CREATE TABLE User(
    User_ID                 INT             AUTO_INCREMENT,
    email_address           VARCHAR(128)    NOT NULL,
    first_name              VARCHAR(64)     NOT NULL,
    last_name               VARCHAR(64)     NOT NULL,
    password_hash           VARCHAR(256)    NOT NULL,
    phone_nbr               VARCHAR(15),
    Primary_User_Role_Id    INT             DEFAULT -1 NOT NULL,
    Is_Test                 BIT(1)          DEFAULT b'0' NOT NULL,
    Is_Active               BIT(1)          NOT NULL,
    Is_Internal             BIT(1)          NOT NULL,
    Create_User_ID          INT             NOT NULL,
    Create_Dt               DATETIME        NOT NULL,
    Update_User_ID          INT,
    Update_Dt               DATETIME        NOT NULL,
    PRIMARY KEY (User_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: User_Language 
--

CREATE TABLE User_Language(
    User_Language_ID    INT         AUTO_INCREMENT,
    User_ID             INT         NOT NULL,
    Language_ID         INT         NOT NULL,
    Create_User_ID      INT         NOT NULL,
    Create_Dt           DATETIME    NOT NULL,
    Update_User_ID      INT,
    Update_Dt           DATETIME    NOT NULL,
    PRIMARY KEY (User_Language_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: User_Privilege 
--

CREATE TABLE User_Privilege(
    User_Privilege_ID    CHAR(10)    NOT NULL,
    User_ID              INT         NOT NULL,
    Privilege_ID         CHAR(10)    NOT NULL,
    Create_User_ID       INT         NOT NULL,
    Create_Dt            DATETIME    NOT NULL,
    Update_User_ID       INT,
    Update_Dt            DATETIME    NOT NULL,
    PRIMARY KEY (User_Privilege_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: User_Role 
--

CREATE TABLE User_Role(
    User_Role_ID      CHAR(10)    NOT NULL,
    User_ID           INT         NOT NULL,
    Role_Id           INT         NOT NULL,
    Create_User_ID    INT         NOT NULL,
    Create_Dt         DATETIME    NOT NULL,
    Update_User_ID    INT,
    Update_Dt         DATETIME    NOT NULL,
    PRIMARY KEY (User_Role_ID)
)ENGINE=INNODB
;



-- 
-- TABLE: User_Session 
--

CREATE TABLE User_Session(
    User_Session_ID        INT             AUTO_INCREMENT,
    token                  VARCHAR(40)     NOT NULL,
    token_expiration_dt    DATETIME,
    device_name            VARCHAR(128),
    device_type            VARCHAR(128),
    identify_key           VARCHAR(128),
    device_version         VARCHAR(128),
    app_from               VARCHAR(128),
    app_id                 VARCHAR(128),
    app_version            VARCHAR(128),
    User_ID                INT             NOT NULL,
    Create_User_ID         INT             NOT NULL,
    Create_Dt              DATETIME        NOT NULL,
    Update_User_ID         INT,
    Update_Dt              DATETIME        NOT NULL,
    PRIMARY KEY (User_Session_ID)
)ENGINE=INNODB
;



-- 
-- INDEX: FK_Carrier_Region_Company_ID 
--

CREATE INDEX FK_Carrier_Region_Company_ID ON Carrier_Region(Company_ID)
;
-- 
-- INDEX: FK_Carrier_Region_Service_Area_ID 
--

CREATE INDEX FK_Carrier_Region_Service_Area_ID ON Carrier_Region(Service_Area_ID)
;
-- 
-- INDEX: FK_Company_Corporate_Address_ID 
--

CREATE INDEX FK_Company_Corporate_Address_ID ON Company(Corporate_Address_ID)
;
-- 
-- INDEX: FK_Company_Branch_Address_ID 
--

CREATE INDEX FK_Company_Branch_Address_ID ON Company(Branch_Address_ID)
;
-- 
-- INDEX: FK_Company_Truck_Pool_Address_ID 
--

CREATE INDEX FK_Company_Truck_Pool_Address_ID ON Company(Truck_Pool_Address_ID)
;
-- 
-- INDEX: FK_Company_Address_Province_ID 
--

CREATE INDEX FK_Company_Address_Province_ID ON Company_Address(Province_ID)
;
-- 
-- INDEX: FK_Company_Address_Country_ID 
--

CREATE INDEX FK_Company_Address_Country_ID ON Company_Address(Country_ID)
;
-- 
-- INDEX: FK_Company_Client_Company_ID 
--

CREATE INDEX FK_Company_Client_Company_ID ON Company_Client(Company_ID)
;
-- 
-- INDEX: FK_Company_Contract_Company_ID 
--

CREATE INDEX FK_Company_Contract_Company_ID ON Company_Contract(Company_ID)
;
-- 
-- INDEX: FK_Company_Contract_Contract_Type_ID 
--

CREATE INDEX FK_Company_Contract_Contract_Type_ID ON Company_Contract(Contract_Type_ID)
;
-- 
-- INDEX: FK_Company_Contract_Payment_Term_ID 
--

CREATE INDEX FK_Company_Contract_Payment_Term_ID ON Company_Contract(Payment_Term_ID)
;
-- 
-- INDEX: FK_Company_Equipment_Company_ID 
--

CREATE INDEX FK_Company_Equipment_Company_ID ON Company_Equipment(Company_ID)
;
-- 
-- INDEX: FK_Company_Equipment_Equipment_ID 
--

CREATE INDEX FK_Company_Equipment_Equipment_ID ON Company_Equipment(Equipment_ID)
;
-- 
-- INDEX: FK_Company_File_Company_ID 
--

CREATE INDEX FK_Company_File_Company_ID ON Company_File(Company_ID)
;
-- 
-- INDEX: FK_Company_Service_Company_ID 
--

CREATE INDEX FK_Company_Service_Company_ID ON Company_Service(Company_ID)
;
-- 
-- INDEX: FK_Company_Service_Service_ID 
--

CREATE INDEX FK_Company_Service_Service_ID ON Company_Service(Service_ID)
;
-- 
-- INDEX: FK_Company_User_Company_ID 
--

CREATE INDEX FK_Company_User_Company_ID ON Company_User(Company_ID)
;
-- 
-- INDEX: FK_Company_User_User_ID 
--

CREATE INDEX FK_Company_User_User_ID ON Company_User(User_ID)
;
-- 
-- INDEX: FK_Equipment_Equipment_Type_ID 
--

CREATE INDEX FK_Equipment_Equipment_Type_ID ON Equipment(Equipment_Type_ID)
;
-- 
-- INDEX: FK_Province_Country_ID 
--

CREATE INDEX FK_Province_Country_ID ON Province(Country_ID)
;
-- 
-- INDEX: FK_Role_Privilege_Role_ID 
--

CREATE INDEX FK_Role_Privilege_Role_ID ON Role_Privilege(Role_Id)
;
-- 
-- INDEX: FK_Role_Privilege_Privilege_ID 
--

CREATE INDEX FK_Role_Privilege_Privilege_ID ON Role_Privilege(Privilege_ID)
;
-- 
-- INDEX: ix_user_email_address 
--

CREATE UNIQUE INDEX ix_user_email_address ON User(email_address)
;
-- 
-- INDEX: ix_user_phone_nbr 
--

CREATE UNIQUE INDEX ix_user_phone_nbr ON User(phone_nbr)
;
-- 
-- INDEX: FK_user_Primary_Role_Id 
--

CREATE INDEX FK_user_Primary_Role_Id ON User(Primary_User_Role_Id)
;
-- 
-- INDEX: FK_User_Language_User_ID 
--

CREATE INDEX FK_User_Language_User_ID ON User_Language(User_ID)
;
-- 
-- INDEX: FK_User_Language_Language_ID 
--

CREATE INDEX FK_User_Language_Language_ID ON User_Language(Language_ID)
;
-- 
-- INDEX: FK_User_Privilege_User_ID 
--

CREATE INDEX FK_User_Privilege_User_ID ON User_Privilege(User_ID)
;
-- 
-- INDEX: FK_User_Privilege_Privilege_ID 
--

CREATE INDEX FK_User_Privilege_Privilege_ID ON User_Privilege(Privilege_ID)
;
-- 
-- INDEX: FK_User_Role_User_ID 
--

CREATE INDEX FK_User_Role_User_ID ON User_Role(User_ID)
;
-- 
-- INDEX: FK_User_Role_Role_ID 
--

CREATE INDEX FK_User_Role_Role_ID ON User_Role(Role_Id)
;
-- 
-- INDEX: FK_User_session_user_id 
--

CREATE INDEX FK_User_session_user_id ON User_Session(User_ID)
;
-- 
-- TABLE: Carrier_Region 
--

ALTER TABLE Carrier_Region ADD CONSTRAINT FK_Carrier_Region_Company_ID 
    FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
;

ALTER TABLE Carrier_Region ADD CONSTRAINT FK_Carrier_Region_Service_Area_ID 
    FOREIGN KEY (Service_Area_ID)
    REFERENCES Service_Area(Service_Area_ID)
;


-- 
-- TABLE: Company 
--

ALTER TABLE Company ADD CONSTRAINT FK_Company_Branch_Address_ID 
    FOREIGN KEY (Branch_Address_ID)
    REFERENCES Company_Address(Company_Address_ID)
;

ALTER TABLE Company ADD CONSTRAINT FK_Company_Corporate_Address_ID 
    FOREIGN KEY (Corporate_Address_ID)
    REFERENCES Company_Address(Company_Address_ID)
;

ALTER TABLE Company ADD CONSTRAINT FK_Company_Truck_Pool_Address_ID 
    FOREIGN KEY (Truck_Pool_Address_ID)
    REFERENCES Company_Address(Company_Address_ID)
;


-- 
-- TABLE: Company_Address 
--

ALTER TABLE Company_Address ADD CONSTRAINT FK_Company_Address_Country_ID 
    FOREIGN KEY (Country_ID)
    REFERENCES Country(Country_ID)
;

ALTER TABLE Company_Address ADD CONSTRAINT FK_Company_Address_Province_ID 
    FOREIGN KEY (Province_ID)
    REFERENCES Province(Province_ID)
;


-- 
-- TABLE: Company_Client 
--

ALTER TABLE Company_Client ADD CONSTRAINT FK_Company_Client_Company_ID 
    FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
;


-- 
-- TABLE: Company_Contract 
--

ALTER TABLE Company_Contract ADD CONSTRAINT FK_Company_Contract_Company_ID 
    FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
;

ALTER TABLE Company_Contract ADD CONSTRAINT FK_Company_Contract_Contract_Type_ID 
    FOREIGN KEY (Contract_Type_ID)
    REFERENCES Contract_Type(Contract_Type_ID)
;

ALTER TABLE Company_Contract ADD CONSTRAINT FK_Company_Contract_Payment_Term_ID 
    FOREIGN KEY (Payment_Term_ID)
    REFERENCES Payment_Term(Payment_Term_ID)
;


-- 
-- TABLE: Company_Equipment 
--

ALTER TABLE Company_Equipment ADD CONSTRAINT FK_Company_Equipment_Company_ID 
    FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
;

ALTER TABLE Company_Equipment ADD CONSTRAINT FK_Company_Equipment_Equipment_ID 
    FOREIGN KEY (Equipment_ID)
    REFERENCES Equipment(Equipment_ID)
;


-- 
-- TABLE: Company_File 
--

ALTER TABLE Company_File ADD CONSTRAINT FK_Company_File_Company_ID 
    FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
;


-- 
-- TABLE: Company_Service 
--

ALTER TABLE Company_Service ADD CONSTRAINT FK_Company_Service_Company_ID 
    FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
;

ALTER TABLE Company_Service ADD CONSTRAINT FK_Company_Service_Service_ID 
    FOREIGN KEY (Service_ID)
    REFERENCES Service(Service_ID)
;


-- 
-- TABLE: Company_User 
--

ALTER TABLE Company_User ADD CONSTRAINT FK_Company_User_Company_ID 
    FOREIGN KEY (Company_ID)
    REFERENCES Company(Company_ID)
;

ALTER TABLE Company_User ADD CONSTRAINT FK_Company_User_User_ID 
    FOREIGN KEY (User_ID)
    REFERENCES User(User_ID)
;


-- 
-- TABLE: Equipment 
--

ALTER TABLE Equipment ADD CONSTRAINT FK_Equipment_Equipment_Type_ID 
    FOREIGN KEY (Equipment_Type_ID)
    REFERENCES Equipment_Type(Equipment_Type_ID)
;


-- 
-- TABLE: Province 
--

ALTER TABLE Province ADD CONSTRAINT FK_Province_Country_ID 
    FOREIGN KEY (Country_ID)
    REFERENCES Country(Country_ID)
;


-- 
-- TABLE: Role_Privilege 
--

ALTER TABLE Role_Privilege ADD CONSTRAINT FK_Role_Privilege_Privilege_ID 
    FOREIGN KEY (Privilege_ID)
    REFERENCES Privilege(Privilege_ID)
;

ALTER TABLE Role_Privilege ADD CONSTRAINT FK_Role_Privilege_Role_ID 
    FOREIGN KEY (Role_Id)
    REFERENCES role(Role_Id)
;


-- 
-- TABLE: User 
--

ALTER TABLE User ADD CONSTRAINT FK_User_Primary_Role_Id 
    FOREIGN KEY (Primary_User_Role_Id)
    REFERENCES role(Role_Id)
;


-- 
-- TABLE: User_Language 
--

ALTER TABLE User_Language ADD CONSTRAINT FK_User_Language_Language_ID 
    FOREIGN KEY (Language_ID)
    REFERENCES Language(Language_ID)
;

ALTER TABLE User_Language ADD CONSTRAINT FK_User_Language_User_ID 
    FOREIGN KEY (User_ID)
    REFERENCES User(User_ID)
;


-- 
-- TABLE: User_Privilege 
--

ALTER TABLE User_Privilege ADD CONSTRAINT FK_User_Privilege_Privilege_ID 
    FOREIGN KEY (Privilege_ID)
    REFERENCES Privilege(Privilege_ID)
;

ALTER TABLE User_Privilege ADD CONSTRAINT FK_User_Privilege_User_ID 
    FOREIGN KEY (User_ID)
    REFERENCES User(User_ID)
;


-- 
-- TABLE: User_Role 
--

ALTER TABLE User_Role ADD CONSTRAINT FK_User_Role_Role_ID 
    FOREIGN KEY (Role_Id)
    REFERENCES role(Role_Id)
;

ALTER TABLE User_Role ADD CONSTRAINT FK_User_Role_User_ID 
    FOREIGN KEY (User_ID)
    REFERENCES User(User_ID)
;


-- 
-- TABLE: User_Session 
--

ALTER TABLE User_Session ADD CONSTRAINT FK_User_session_user_id 
    FOREIGN KEY (User_ID)
    REFERENCES User(User_ID)
;





insert into role (Role_Name, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt)
values ('Unknown',0,now(),0,now())
