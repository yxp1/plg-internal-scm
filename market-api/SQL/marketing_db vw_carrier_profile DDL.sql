﻿use marketplace_db;
DROP VIEW `vw_carrier_profile`;
CREATE VIEW `vw_carrier_profile` AS 
select `cmpny`.Company_ID, `cmpny`.`Company_Name` AS `company_name`,`cmpny`.`Company_Code` AS `company_code`
      ,rtrim(concat(`hq`.`Address_Line_1`,' ',`hq`.`Address_Line_2`,' ',`hq`.`Address_Line_3`)) AS `company_address`
      ,`hq`.`Address_Line_1`,`hq`.`Address_Line_2`,`hq`.`Address_Line_3`
      ,`hq_p`.`Province_Name` AS `HQ_Province`,`hq_cntry`.`Country_Name` AS `HQ_Country`
      
      ,rtrim(concat(`truckyard`.`Address_Line_1`,' ',`truckyard`.`Address_Line_2`,' ',`truckyard`.`Address_Line_3`)) AS `Truck_Pool_address`
      ,`truckyard`.`Address_Line_1` as Yard_Address_Line_1,`truckyard`.`Address_Line_2` as Yard_Address_Line_2,`truckyard`.`Address_Line_3` as Yard_Address_Line_3
      ,`ty_p`.`Province_Name` AS `Yard_Province`,`ty_cntry`.`Country_Name` AS `Yard_Country`
      
      ,rtrim(concat(`branch`.`Address_Line_1`,' ',`branch`.`Address_Line_2`,' ',`branch`.`Address_Line_3`)) AS `branch_address` 
      ,`branch`.`Address_Line_1` as Branch_Address_Line_1,`branch`.`Address_Line_2` as Branch_Address_Line_2,`branch`.`Address_Line_3` as Branch_Address_Line_3
      ,`b_p`.`Province_Name` AS `Branch_Province`,`b_cntry`.`Country_Name` AS `Branch_Country`
      , cmpny.is_carrier, cmpny.is_shipper
      , cmpny.create_dt, cmpny.create_user_id
      , cmpny.update_dt, cmpny.update_user_id
      
from `company` `cmpny` 
left join `company_address` `hq` on `hq`.`Company_Address_ID` = `cmpny`.`Corporate_Address_ID`
left join `province` `hq_p` on `hq_p`.`Province_ID` = `hq`.`Province_ID`
left join `country` `hq_cntry` on `hq_cntry`.`Country_ID` = `hq`.`Country_ID` 
left join `company_address` `truckyard` on `truckyard`.`Company_Address_ID` = `cmpny`.`Truck_Pool_Address_ID` 
left join `province` `ty_p` on `ty_p`.`Province_ID` = `hq`.`Province_ID`
left join `country` `ty_cntry` on `ty_cntry`.`Country_ID` = `hq`.`Country_ID` 
left join `company_address` `branch` on `branch`.`Company_Address_ID` = `cmpny`.`Truck_Pool_Address_ID`
left join `province` `b_p` on `b_p`.`Province_ID` = `hq`.`Province_ID`
left join `country` `b_cntry` on `b_cntry`.`Country_ID` = `hq`.`Country_ID` 
where cmpny.is_carrier = 1;

