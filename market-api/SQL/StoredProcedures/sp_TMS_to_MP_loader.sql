DROP PROCEDURE IF EXISTS marketplace_db.sp_TMS_to_MP_loader;
CREATE PROCEDURE marketplace_db.`sp_TMS_to_MP_loader`(
            pPartnerCd varchar(10), 
            pParentPartnerCd varchar(10), 
            pShipperCd varchar(10), 
            pCustomerCd varchar(10), 
            pPONbr varchar(50), 
            pReleaseNbr varchar(15),
            pPaymentTermId int(11),
            pLowEndBid double,
            pHighEndBid double,
            pTargetRate double,
            pCurrencyCd varchar(6),
            pTenderExpirationDt datetime,
            pEquipmentTypeID int(11)
            )
BEGIN
  /* v2016-06-08: Initial draft, populate SHIPMENT table
     v2016-06-13: Additional params: Low & High End Bid, Target Rate, Currency
     v2016-06-19: Post to Message WorkQ to alert registered carriers in MarketPlace
     v2016-06-20: Shipment Status should be set to 15, populate SHIPMENT_EQUIPMENT
     v2016-06-21: Equipment Type ID is now passed as a parameter
  */
  BEGIN  -- main with vars
    -- DECLARE pEquipmentTypeID  int(11);
    
    DECLARE v_ProBillNbr varchar(30);
    
    DECLARE v_Shipment_Id int(11);
    DECLARE v_shipment_nbr varchar(16);
    DECLARE v_Shipper_Id int(11);
    DECLARE v_Status_Id int(11);
    DECLARE v_assigned_provider_Id int(11);
    DECLARE v_pickup_address_id int(11);
    DECLARE v_delivery_address_id int(11);
    DECLARE v_requested_equipment_type_id int(11);
    DECLARE v_requested_pickup_dt datetime;
    DECLARE v_Requested_Delivery_Dt datetime;
    DECLARE v_Unit_Count int(11);
    DECLARE v_unit_of_measure varchar(6);
    DECLARE v_Create_Dt datetime;
    DECLARE v_create_user_id varchar(120);
    DECLARE v_Update_dt datetime;
    DECLARE v_update_user_id varchar(129);
    DECLARE v_tender_expiration_dt datetime;
    DECLARE v_origin_service_area_id int(11);
    DECLARE v_destination_service_area_id int(11);
    DECLARE v_ccy_cd varchar(6);

    DECLARE v_pickup_name varchar(100);
    DECLARE v_pickup_address varchar(100); 
   	DECLARE v_pickup_city varchar(100); 
    DECLARE v_pickup_state varchar(100); 
    DECLARE v_pickup_postal_cd varchar(30);
    DECLARE v_pickup_country_cd varchar(100);
          
    DECLARE v_dest_name varchar(100);
    DECLARE v_dest_address varchar(100); 
   	DECLARE v_dest_city varchar(100); 
    DECLARE v_dest_state varchar(100); 
    DECLARE v_dest_postal_cd varchar(30);
    DECLARE v_dest_country_cd varchar(100);  
    
    DECLARE v_skipflg int;
  
    DECLARE done int DEFAULT FALSE;
  
    DECLARE cursor_poHeader CURSOR FOR  
      	SELECT a.release_nbr, 
	        a.available_dt,
		      a.requested_delivery_dt,
		      a.total_qty,
		      a.qty_uom,
		      a.create_user_id,
          a.pro_bill_nbr,
          pu.LOCATION_NAME as pickup_name,
    			pu.LINE_1_ADDR as pickup_address, 
   			  pu.CITY_NAME as pickup_city, 
          pu.STATE_NAME as pickup_state, 
          pu.POSTAL_CD as pickup_postal_cd,
    			pu.COUNTRY_CD as pickup_country_cd,
          dv.LOCATION_NAME as dest_name,
    			dv.LINE_1_ADDR as dest_address, 
   			  dv.CITY_NAME as dest_city, 
          dv.STATE_NAME as dest_state, 
          dv.POSTAL_CD as dest_postal_cd,
    			dv.COUNTRY_CD as dest_country_cd
	      FROM tms_utf8_db.PO_HEADER a
        INNER JOIN tms_utf8_db.PO_REFERENCE_INFO pu 
          ON  a.partner_cd = pu.partner_cd
          AND a.parent_partner_cd = pu.parent_partner_cd
          AND a.customer_cd = pu.customer_cd
          AND a.po_nbr = pu.po_nbr
          AND a.release_nbr = pu.release_nbr
          AND a.equipment_shipment_seq_nbr = pu.equipment_shipment_seq_nbr
          AND pu.reference_type_cd = 'Pickup'
        INNER JOIN tms_utf8_db.PO_REFERENCE_INFO dv 
          ON  a.partner_cd = dv.partner_cd
          AND a.parent_partner_cd = dv.parent_partner_cd
          AND a.customer_cd = dv.customer_cd
          AND a.po_nbr = dv.po_nbr
          AND a.release_nbr = dv.release_nbr
          AND a.equipment_shipment_seq_nbr = dv.equipment_shipment_seq_nbr
          AND dv.reference_type_cd = 'Delivery'  
	      WHERE a.partner_cd = pPartnerCd
          AND a.parent_partner_cd = pParentPartnerCd
          AND a.customer_cd = pCustomerCd
          AND a.po_nbr = pPONbr
          AND a.release_nbr = pReleaseNbr
          AND a.equipment_shipment_seq_nbr = 1; 
		
    DECLARE EXIT handler FOR NOT FOUND SET done  = TRUE;
  
    proc1:BEGIN
    
        SET pEquipmentTypeID = -1;
    
        SELECT company_id INTO v_shipper_id FROM marketplace_db.COMPANY WHERE company_code = pShipperCd;
        -- select concat('shipper ID:',v_shipper_id);

        CALL marketplace_db.sp_check_delete_shipment_ok(pReleaseNbr, v_shipper_id, v_skipflg);
        IF v_skipflg = 1 THEN
           -- select concat('Shipment:', pReleaseNbr,' with Shipper ID ',v_shipper_id,' already in progress:');
           LEAVE proc1;
        END IF;        
        
        -- select concat('Create SHIPMENT: shipperID',v_shipper_id, ' RLS#:',pReleaseNbr);        
        SET done = FALSE;
        OPEN cursor_poHeader;
   			FETCH cursor_poHeader INTO
              v_shipment_nbr,
              v_requested_pickup_dt,
              v_Requested_Delivery_Dt,
              v_Unit_Count,
              v_unit_of_measure,
              v_create_user_id,
              v_ProBillNbr,
              v_pickup_name,
              v_pickup_address,
              v_pickup_city,
              v_pickup_state,
              v_pickup_postal_cd,
              v_pickup_country_cd,
              v_dest_name,
              v_dest_address,
              v_dest_city,
              v_dest_state,
              v_dest_postal_cd,
              v_dest_country_cd;
        
        BEGIN
            SET v_pickup_address_id = -1;
            CALL marketplace_db.sp_get_entity_address(v_pickup_name, v_pickup_address, v_pickup_city, v_pickup_state,v_pickup_postal_cd, v_pickup_country_cd, v_pickup_address_id);
            
            SET v_delivery_address_id = -1;
            CALL marketplace_db.sp_get_entity_address(v_dest_name, v_dest_address, v_dest_city, v_dest_state,v_dest_postal_cd, v_dest_country_cd, v_delivery_address_id);
        END;
        -- select concat('AFTER v_pickup_address_id:',v_pickup_address_id, '/ v_delivery_address_id:',v_delivery_address_id);

        INSERT INTO marketplace_db.SHIPMENT(
                  shipment_nbr
                  ,Shipper_Id 
                  ,Status_Id  
                  ,assigned_provider_Id
                  ,pickup_address_id  
                  ,delivery_address_id
                  ,requested_equipment_type_id 
                  ,requested_pickup_dt  
                  ,requested_delivery_dt 
                  ,unit_count 
                  ,unit_of_measure 
                  ,create_dt 
                  ,create_user_id 
                  ,update_dt 
                  ,update_user_id
                  ,tender_expiration_dt
                  ,payment_term_id, low_end_bid_amt, high_end_bid_amt, target_rate_amt, currency_cd 
                  ,Partner_Cd, Parent_Partner_Cd, Shipper_Cd, Customer_Cd, PO_Nbr, Release_Nbr
        ) SELECT 
                  v_shipment_nbr AS shipment_nbr,
                  v_shipper_id   AS shipper_id,
                  15  AS status_id,
                  -1 AS assigned_provider_Id,
                  v_pickup_address_id   AS pickup_address_id,
                  v_delivery_address_id AS delivery_address_id,
                  pEquipmentTypeID AS requested_equipment_type_id,
                  v_requested_pickup_dt   AS requested_pickup_dt,
                  v_Requested_Delivery_Dt AS requested_delivery_dt,
                  v_Unit_Count      AS unit_count,
                  v_unit_of_measure AS unit_of_measure,
                  now()  AS create_dt,
                  v_create_user_id AS create_user_id,
                  now()  AS update_dt,
                  v_create_user_id AS update_user_id
                  ,pTenderExpirationDt    -- ,DATE_ADD(NOW(), INTERVAL 2 DAY) AS tender_expiration_dt
                  ,pPaymentTermId, pLowEndBid, pHighEndBid, pTargetRate, pCurrencyCd
                  ,pPartnerCd, pParentPartnerCd, pShipperCd, pCustomerCd, pPONbr, pReleaseNbr
                  ;
                    	
        SELECT LAST_INSERT_ID() INTO v_Shipment_Id;           
        -- select concat('v_Shipment_Id:',v_Shipment_Id);
                  
        INSERT INTO marketplace_db.SHIPMENT_EQUIPMENT (shipment_id, equipment_type_id) VALUES (v_Shipment_Id, pEquipmentTypeID);
                  
    		CLOSE cursor_poHeader;	
    
        dtl:BEGIN
            DECLARE v_qty int(11);
            DECLARE v_uom varchar(15);
            DECLARE v_wt double;
            DECLARE v_vol double;
        
            DECLARE done2 int DEFAULT FALSE;
        		DECLARE cursor_poLineItem CURSOR FOR
        			  SELECT DISTINCT 
                    sum(ordered_qty),
                    package_uom,
                    sum(ordered_wt),
                    sum(ordered_vol)
        			  FROM tms_utf8_db.PO_LINE_ITEM 
                WHERE partner_cd = pPartnerCd
                  AND parent_partner_cd = pParentPartnerCd
                  AND customer_cd = pCustomerCd
                  AND po_nbr = pPONbr
                  AND release_nbr = pReleaseNbr
                  AND equipment_shipment_seq_nbr = 1
                GROUP BY package_uom;  
                
          	DECLARE EXIT handler FOR NOT FOUND SET done2 = TRUE;    
            
            SET done2 = FALSE;
        		OPEN cursor_poLineItem;
          	FETCH cursor_poLineItem INTO
          		    v_qty,
                  v_uom,
                  v_wt,
                  v_vol;
            -- select concat('v_Shipment_Id:',v_Shipment_Id, '/ Qty:',v_qty,'/ uom:',v_uom, '/ wt:',v_wt, '/ vol:',v_vol);
                
          	INSERT INTO marketplace_db.SHIPMENT_DETAIL(
                    Shipment_Id,
                    qty,
                    unit_of_measure,
                    shipment_wt,
                    wt_uom,
                    shipment_vol,
                    vol_uom
            ) SELECT 
                    v_shipment_id,
                    v_qty,
                    v_uom,
                    v_wt,
                    'KGS',
                    v_vol,
                    'CBM';
        		CLOSE cursor_poLineItem;        
        END dtl;
        
        rates:BEGIN
            
            DECLARE v_carrier_id int(11);
            
            DECLARE v_carrier_cd varchar(8);
            DECLARE v_transit_days_dur double;
            DECLARE v_total_transport_amt double;
            DECLARE v_transport_amt_ccy_cd varchar(6);
            DECLARE v_rate_type_cd varchar(15);
            DECLARE v_equipment_type_id int(11);
            
            DECLARE done3 int DEFAULT FALSE;
        		DECLARE cursor_poReleaseCarrier CURSOR FOR
        			  SELECT DISTINCT 
                    carrier_cd,
                    transit_days_dur,
                    total_transport_amt,
                    transport_amt_ccy_cd,
                    coalesce(rate_type_cd,'CONTRACT') as rate_type_cd
        			  FROM tms_utf8_db.PO_RELEASE_CARRIER 
                WHERE charge_type_cd = 'C' 
                  AND partner_cd = pPartnerCd
                  AND parent_partner_cd = pParentPartnerCd
                  AND customer_cd = pCustomerCd
                  AND po_nbr = pPONbr
                  AND release_nbr = pReleaseNbr
                  AND equipment_shipment_seq_nbr = 1;

          	DECLARE EXIT handler FOR NOT FOUND SET done3 = TRUE;
        		
            OPEN cursor_poReleaseCarrier;
            REPEAT
                FETCH cursor_poReleaseCarrier INTO
                      v_carrier_cd, 
                      v_transit_days_dur,
                      v_total_transport_amt,
                      v_transport_amt_ccy_cd,
                      v_rate_type_cd;
                
                SELECT company_id INTO v_carrier_id FROM marketplace_db.company WHERE company_code = v_carrier_cd;
                -- select concat('Contract Rate:',v_carrier_cd, ':',v_total_transport_amt,':',v_transport_amt_ccy_cd);
                
                INSERT INTO marketplace_db.shipment_carrier_bid (
                    carrier_id,
                    shipment_id,
                    quote_status_cd,
                    rate_type_cd,
                    rate_type_desc_txt,
                    transit_tm,
                    equipment_type_id,
                    quote_amt,
                    quote_currency_cd,
                    quote_datestamp,
                    quote_expiration_value,
                    quote_expiration_uom,
                    comment_txt,
                    create_Dt,
                    create_user_id,
                    update_dt,
                    update_user_id
                ) SELECT 
                    v_carrier_id AS carrier_id,
                    v_shipment_id AS shipment_id,
                    'A' AS quote_status_cd,
                    'CONTRACT' AS rate_type_cd,
                    'Contract Rate' AS rate_type_desc_txt,
                    v_transit_days_dur AS transit_tm,
                    pEquipmentTypeID AS equipment_type_id,
                    v_total_transport_amt AS quote_amt,
                    v_transport_amt_ccy_cd AS quote_currency_cd,
                    now() AS quote_datestamp,
                    24 AS quote_expiration_value,
                    'HR' AS quote_expiration_uom,
                    concat(pPartnerCd,' TMS contract rate for carrier ',v_carrier_cd)  AS comment_txt,
                    now() AS create_Dt,
                    v_create_user_id AS create_user_id,
                    now() AS update_dt,
                    v_create_user_id AS update_user_id;
            
            UNTIL done3 END REPEAT;
            CLOSE cursor_poReleaseCarrier;   
        END rates;
        
        BEGIN
            -- parmUserID ,  parmCarrierCd ,  parmPartnerCd,  parmReferenceNbr,   parmPONbr,  parmReleaseNbr varchar(50)
            CALL marketplace_db.sp_marketplace_BID_messageQ_loader(v_create_user_id, '*', pShipperCd, pReleaseNbr, pPONbr, pReleaseNbr);
            
            select concat('Log to T&T:',v_ProBillNbr, ' Shipment Booked via MarketPlace');
            CALL ods_utf8_db.sp_RecordTrackAndTrace_By_ProBillNbr(v_ProBillNbr, 'MarketPlace', 'MP', 'Shipment Posted to MarketPlace', 
                                                                      now(), '', 'MarketPlace' );
            
        END;
    
    END proc1;
  
  END;  -- main with vars
END;
