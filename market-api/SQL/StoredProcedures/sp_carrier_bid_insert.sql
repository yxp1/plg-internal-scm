DROP PROCEDURE IF EXISTS marketplace_db.sp_carrier_bid_insert;
CREATE PROCEDURE marketplace_db.`sp_carrier_bid_insert`( 
                                  ShipmentId int,
                                  CarrierId int,
                                  RateTypeCd varchar(25) ,
                                  RateTypeDescTxt varchar(50) ,
                                  TransitTm varchar(3) ,
                                  EquipmentTypeId int,
                                  QuoteAmt double,
                                  QuoteCurrencyCd varchar(10) ,
                                  CommentTxt varchar(225) ,
                                  UserId int)
BEGIN
      
      DECLARE ShipmentCarrierBidId int;
      
      SELECT shipment_carrier_bid_id into ShipmentCarrierBidId FROM shipment_carrier_bid where shipment_id = ShipmentId  and carrier_id = CarrierId;
      
      IF (ShipmentCarrierBidId > 0) THEN
          UPDATE shipment_carrier_bid
          SET TRANSIT_TM= TransitTm , QUOTE_AMT = QuoteAmt , QUOTE_CURRENCY_CD=QuoteCurrencyCd, EQUIPMENT_TYPE_ID=EquipmentTypeId, RATE_TYPE_CD = RateTypeCd, COMMENT_TXT =CommentTxt, UPDATE_DT=now()
          where shipment_id = ShipmentId  and carrier_id=CarrierId;
      ELSE 
          INSERT INTO marketplace_db.shipment_carrier_bid
          (carrier_id, shipment_id, quote_status_cd, rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt, quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, comment_txt, create_Dt, create_user_id, update_dt, update_user_id) 
          values
          (CarrierId, ShipmentId, 'P', RateTypeCd, RateTypeDescTxt, TransitTm, EquipmentTypeId, QuoteAmt, QuoteCurrencyCd, now(), 0, QuoteCurrencyCd, CommentTxt, now(), UserId, now(), UserId);
      END IF;
      -- select equipmentTypeId, SERVICE_CLASS_ ;
END;
