DROP PROCEDURE IF EXISTS marketplace_db.sp_carrier_rate_shop;
CREATE PROCEDURE marketplace_db.`sp_carrier_rate_shop`( shipmentId int, mode varchar(10) )
BEGIN
      BEGIN
      
      DECLARE equipmentTypeId int;
      DECLARE CUSTOMER_CD_ VARCHAR(100); 
      DECLARE PARENT_PARTNER_CD_ VARCHAR(100);
      DECLARE SERVICE_CLASS_ VARCHAR(100);
      DECLARE SERVICE_TYPE_ VARCHAR(100);
      DECLARE CARRIER_CD_ VARCHAR(100);
      DECLARE TRANSPORT_MODE_CD_ VARCHAR(100);
      DECLARE EQUIPMENT_TYPE_CD_ VARCHAR(100);
      DECLARE ORIGIN_CITY_ VARCHAR(100);
      DECLARE ORIGIN_STATE_ VARCHAR(100);
      DECLARE ORIGIN_COUNTRY_CD_ VARCHAR(100);
      DECLARE ORIGIN_POSTAL_START_ VARCHAR(100);
      DECLARE ORIGIN_POSTAL_END_ VARCHAR(100);
      DECLARE ORIGIN_PROVINCE_ VARCHAR(100);
      DECLARE ORIGIN_DISTRICT_ VARCHAR(100);
      DECLARE DESTINATION_CITY_ VARCHAR(100);
      DECLARE DESTINATION_STATE_ VARCHAR(100);
      DECLARE DESTINATION_COUNTRY_CD_ VARCHAR(100);
      DECLARE DESTINATION_POSTAL_START_ VARCHAR(100);
      DECLARE DESTINATION_POSTAL_END_ VARCHAR(100);
      DECLARE DESTINATION_PROVINCE_ VARCHAR(100);
      DECLARE DESTINATION_DISTRICT_ VARCHAR(100);
      DECLARE TRANSIT_TM_ VARCHAR(100);
      DECLARE strQuery VARCHAR(1024);
      
      SET PARENT_PARTNER_CD_ = 'MRKTPLC';
      SET CUSTOMER_CD_ = null; 
      SET SERVICE_CLASS_ = null;
      SET SERVICE_TYPE_ = null;
      SET CARRIER_CD_ = null;
      SET TRANSPORT_MODE_CD_ = null;
      SET EQUIPMENT_TYPE_CD_ = null;
      SET ORIGIN_CITY_ = null;
      SET ORIGIN_STATE_ = null;
      SET ORIGIN_COUNTRY_CD_ = null;
      SET ORIGIN_POSTAL_START_ = null;
      SET ORIGIN_POSTAL_END_ = null;
      SET ORIGIN_PROVINCE_ = null;
      SET ORIGIN_DISTRICT_ = null;
      SET DESTINATION_CITY_ = null;
      SET DESTINATION_STATE_ = null;
      SET DESTINATION_COUNTRY_CD_ = null;
      SET DESTINATION_POSTAL_START_ = null;
      SET DESTINATION_POSTAL_END_ = null;
      SET DESTINATION_PROVINCE_ = null;
      SET DESTINATION_DISTRICT_ = null;
      SET TRANSIT_TM_ = null;
      
      SELECT 
      requested_equipment_type_id into equipmentTypeId 
      FROM shipment where shipment_id = shipmentId ;
      
      -- select equipmentTypeId, SERVICE_CLASS_ ;
      
      delete from shipment_carrier_bid where rate_type_cd='CONTRACT' and shipment_id = shipmentId;
        
      /*  
      INSERT INTO marketplace_db.shipment_carrier_bid
      (carrier_id, shipment_id, quote_status_cd, rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt, quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, comment_txt, create_Dt, create_user_id, update_dt, update_user_id) 
      values
      (81,shipmentId,'P','CONTRACT','Contract Rate',0,equipmentTypeId,100,'USD','2016-06-15 17:22:04',0,'','','2016-06-15 17:22:04','97','2016-06-15 17:22:04','97'),
      (87,shipmentId,'P','CONTRACT','Contract Rate',0,equipmentTypeId,100,'USD','2016-06-15 17:22:04',0,'','','2016-06-15 17:22:04','97','2016-06-15 17:22:04','97');
      */

      /* 
       call abs_rater_utf8.SP_RaterCarrierLane( '' ,
                           '*' , --          CUSTOMER_CD_ VARCHAR(100) ,
                           'MRKTPLC ', --           PARENT_PARTNER_CD_ VARCHAR(100) ,
                            'DD',--          SERVICE_TYPE_ VARCHAR(100) ,
                           '' ,--           CARRIER_CD_ VARCHAR(100) ,
                           '' ,--          TRANSPORT_MODE_CD_ VARCHAR(100) ,
                           '',--           EQUIPMENT_TYPE_CD_ VARCHAR(100) ,
                           'ORIGIN' ,--          ORIGIN_CITY_ VARCHAR(100) ,
                            null ,--         ORIGIN_STATE_ VARCHAR(100) ,
                            null ,--         ORIGIN_COUNTRY_CD_ VARCHAR(100) ,
                            null ,--         ORIGIN_POSTAL_START_ VARCHAR(100) ,
                            null ,--         ORIGIN_POSTAL_END_ VARCHAR(100) ,
                            null ,--         ORIGIN_PROVINCE_ VARCHAR(100) ,
                            null ,--         ORIGIN_DISTRICT_ VARCHAR(100) ,
                            'DESTINATION',--          DESTINATION_CITY_ VARCHAR(100) ,
                            null   ,--       DESTINATION_STATE_ VARCHAR(100) ,
                            null,--          DESTINATION_COUNTRY_CD_ VARCHAR(100) ,
                            null,--          DESTINATION_POSTAL_START_ VARCHAR(100) ,
                            null ,--         DESTINATION_POSTAL_END_ VARCHAR(100) ,
                             null,--         DESTINATION_PROVINCE_ VARCHAR(100) ,
                            '' ,--         DESTINATION_DISTRICT_ VARCHAR(100) ,
                            100          );
                            
                            
                            
                            INSERT INTO marketplace_db.shipment_carrier_bid   
                          (carrier_id, shipment_id, quote_status_cd, rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt, quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, comment_txt, create_Dt, create_user_id, update_dt, update_user_id)   
                          select distinct   c.company_id, 54,'P' as quote_status_cd, 'CONTRACT' as rate_type_cd, 'Contract Rate' as rate_type_desc_txt,  transit_tm,  10,point_to_point_charge_amt, currency_cd, now() as quote_datestamp, now() as quote_expiration_value, null as quote_expiration_uom, '' as comment_txt, now() as create_Dt, 97 as create_user_id, now() as update_dt, 97 as update_user_id 
                          from  abs_rater_utf8.carrier_lane r
                          join marketplace_db.company c on r.carrier_cd=c.company_code 
                          where 
                          cost_type = 'C' 
                          and  CONTRACT_EFFECTIVE_DT <= now()
                          and parent_partner_cd='MRKTPLC' 
                          order by CARRIER_CD asc, TRANSPORT_MODE_CD asc, EQUIPMENT_TYPE_CD asc
        */
        
        
        
        

        SET @strQuery =  CONCAT('INSERT INTO marketplace_db.shipment_carrier_bid   ',
            '(carrier_id, shipment_id, quote_status_cd, rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt, quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, comment_txt, create_Dt, create_user_id, update_dt, update_user_id)    ',
            'select distinct   ',
            'c.company_id, ',
            shipmentId,
            ',',
            '''P'' as quote_status_cd, ',
            '''CONTRACT'' as rate_type_cd, ', 
            '''Contract Rate'' as rate_type_desc_txt,  ',
            'transit_tm,  ',
            equipmentTypeId,
            ',',
            'point_to_point_charge_amt, ',
            'currency_cd, ',
            'now() as quote_datestamp, ',
            '24 as quote_expiration_value, ',
            ''''' as quote_expiration_uom, ',
            ''''' as comment_txt, ',
            'now() as create_Dt, ',
            '97 as create_user_id, ',
            'now() as update_dt, ',
            '97 as update_user_id ',
            'from  abs_rater_utf8.carrier_lane r ',
            'join marketplace_db.company c on r.carrier_cd=c.company_code ',
            'where  cost_type = ''C'' ',
            'and  CONTRACT_EFFECTIVE_DT <= now()')  ;




          IF (SERVICE_CLASS_ IS NOT NULL and SERVICE_CLASS_<>'' and SERVICE_CLASS_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (SERVICE_CLASS =  ''', SERVICE_CLASS_ , '''or SERVICE_CLASS = ''*'') ');
          END IF;

          IF (CUSTOMER_CD_ IS NOT NULL and CUSTOMER_CD_<>'' and CUSTOMER_CD_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (CUSTOMER_CD =  ''', CUSTOMER_CD_ , '''or CUSTOMER_CD = ''*'') ');
          END IF;

          IF  (PARENT_PARTNER_CD_ IS NOT NULL and PARENT_PARTNER_CD_<>'' and PARENT_PARTNER_CD_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (PARENT_PARTNER_CD =  ''', PARENT_PARTNER_CD_ , '''or PARENT_PARTNER_CD = ''*'') ');
          END IF;

          IF  (SERVICE_TYPE_ IS NOT NULL and SERVICE_TYPE_<>''  and SERVICE_TYPE_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (SERVICE_TYPE =  ''', SERVICE_TYPE_ , '''or SERVICE_TYPE = ''*'') ');
          END IF;
            
          IF  (CARRIER_CD_ IS NOT NULL and CARRIER_CD_<>'' and CARRIER_CD_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (CARRIER_CD =  ''', CARRIER_CD_ , '''or CARRIER_CD = ''*'') ');
          END IF;         
           
          IF  (TRANSPORT_MODE_CD_ IS NOT NULL and TRANSPORT_MODE_CD_<>'' and TRANSPORT_MODE_CD_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (TRANSPORT_MODE_CD in  (', TRANSPORT_MODE_CD_ , ') or TRANSPORT_MODE_CD = ''*'') ');
          END IF; 
                   
          IF  (EQUIPMENT_TYPE_CD_ IS NOT NULL and EQUIPMENT_TYPE_CD_<>'' and EQUIPMENT_TYPE_CD_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (EQUIPMENT_TYPE_CD in  (', EQUIPMENT_TYPE_CD_ , ') or EQUIPMENT_TYPE_CD = ''*'') ');
          END IF; 

          IF  (ORIGIN_CITY_ IS NOT NULL and ORIGIN_CITY_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (ORIGIN_CITY =  ''', ORIGIN_CITY_ , '''or ORIGIN_CITY = ''*'') ');
          END IF; 

          IF  (ORIGIN_STATE_ IS NOT NULL and ORIGIN_STATE_<>''  and ORIGIN_STATE_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (ORIGIN_STATE_CD =  ''', ORIGIN_STATE_ , '''or ORIGIN_STATE_CD = ''*'') ');
          END IF; 

          IF  (ORIGIN_COUNTRY_CD_ IS NOT NULL and ORIGIN_COUNTRY_CD_<>'' and ORIGIN_COUNTRY_CD_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (ORIGIN_COUNTRY_CD =  ''', ORIGIN_COUNTRY_CD_ , '''or ORIGIN_COUNTRY_CD = ''*'') ');
          END IF; 

          IF  (ORIGIN_POSTAL_START_ IS NOT NULL and ORIGIN_POSTAL_START_<>'' and ORIGIN_POSTAL_START_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (ORIGIN_POSTAL_START >=  ''', ORIGIN_POSTAL_START_ , '''or ORIGIN_POSTAL_START = ''*'') ');
          END IF; 

          IF  (ORIGIN_POSTAL_END_ IS NOT NULL and ORIGIN_POSTAL_END_<>'' and ORIGIN_POSTAL_END_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (ORIGIN_POSTAL_END >=  ''', ORIGIN_POSTAL_END_ , '''or ORIGIN_POSTAL_END = ''*'') ');
          END IF; 

          IF  (ORIGIN_PROVINCE_ IS NOT NULL and ORIGIN_PROVINCE_<>'' and ORIGIN_PROVINCE_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (ORIGIN_PROVINCE =  ''', ORIGIN_PROVINCE_ , '''or ORIGIN_PROVINCE = ''*'') ');
          END IF;

          IF  (ORIGIN_DISTRICT_ IS NOT NULL and ORIGIN_DISTRICT_<>'' and ORIGIN_DISTRICT_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (ORIGIN_DISTRICT =  ''', ORIGIN_DISTRICT_ , '''or ORIGIN_DISTRICT = ''*'') ');
          END IF;

          IF  (DESTINATION_CITY_ IS NOT NULL and DESTINATION_CITY_<>'' and DESTINATION_CITY_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (DESTINATION_CITY =  ''', DESTINATION_CITY_ , ''' or DESTINATION_CITY = ''*'') ');
          END IF; 

          IF  (DESTINATION_STATE_ IS NOT NULL and DESTINATION_STATE_<>'' and DESTINATION_STATE_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (DESTINATION_STATE_CD =  ''', DESTINATION_STATE_ , '''or DESTINATION_STATE_CD = ''*'') ');
          END IF; 

          IF  (DESTINATION_COUNTRY_CD_ IS NOT NULL and DESTINATION_COUNTRY_CD_<>'' and DESTINATION_COUNTRY_CD_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (DESTINATION_COUNTRY_CD =  ''', DESTINATION_COUNTRY_CD_ , '''or DESTINATION_COUNTRY_CD = ''*'') ');
          END IF; 

          IF  (DESTINATION_POSTAL_START_ IS NOT NULL and DESTINATION_POSTAL_START_<>'' and DESTINATION_POSTAL_START_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (DESTINATION_POSTAL_START >=  ''', DESTINATION_POSTAL_START_ , '''or DESTINATION_POSTAL_START = ''*'') ');
          END IF; 

          IF  (DESTINATION_POSTAL_END_ IS NOT NULL and DESTINATION_POSTAL_END_<>'' and DESTINATION_POSTAL_END_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (DESTINATION_POSTAL_END >=  ''', DESTINATION_POSTAL_END_ , '''or DESTINATION_POSTAL_END = ''*'') ');
          END IF; 

          IF  (DESTINATION_PROVINCE_ IS NOT NULL and DESTINATION_PROVINCE_<>'' and DESTINATION_PROVINCE_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (DESTINATION_PROVINCE =  ''', DESTINATION_PROVINCE_ , '''or DESTINATION_PROVINCE = ''*'') ');
          END IF;

          IF  (DESTINATION_DISTRICT_ IS NOT NULL and DESTINATION_DISTRICT_<>'' and DESTINATION_DISTRICT_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and (DESTINATION_DISTRICT =  ''', DESTINATION_DISTRICT_ , '''or DESTINATION_DISTRICT = ''*'') ');
          END IF;

          IF  (TRANSIT_TM_ IS NOT NULL and TRANSIT_TM_<>'' and TRANSIT_TM_<>'null') THEN
             SET @strQuery = CONCAT(@strQuery, ' and TRANSIT_TM <=  ', TRANSIT_TM_);
          END IF;
          
                   
          SET @strQuery = CONCAT(@strQuery, ' order by point_to_point_charge_amt asc, TRANSIT_TM asc'); 
      
          -- SELECT @strQuery;
         
          PREPARE  stmtp FROM  @strQuery;
          EXECUTE  stmtp;
            
          
           
          END;  
END;
