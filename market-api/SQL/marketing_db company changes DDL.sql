﻿use marketplace_db;
ALTER TABLE `company` add  `Years_Of_Operation` int(11) after branch_address_id;
ALTER TABLE `company` add  `Business_Headline` varchar(1024) after Years_Of_Operation;
ALTER TABLE `company` add `Business_Description` varchar(1024) after Business_Headline;

update company 
   set years_of_operation = (select years_of_operation from company_address a where a.company_address_ID = company.Corporate_Address_ID),
       Business_Headline = (select Business_Headline from company_address a where a.company_address_ID = company.Corporate_Address_ID),
       Business_Description = (select Business_Description from company_address a where a.company_address_ID = company.Corporate_Address_ID);

ALTER TABLE `company_address` drop column `Years_Of_Operation`;
ALTER TABLE `company_address` drop column `Business_Headline`;
ALTER TABLE `company_address` drop column `Business_Description`;
