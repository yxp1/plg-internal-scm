﻿use marketplace_db;
DROP VIEW `vw_carrier_services`;
CREATE VIEW `vw_carrier_services` AS 
select c.company_id, c.company_name, c.company_code, sa.service_area_name, s.service_name
, s.Is_Transportation
, cs.Company_Service_ID, cs.Service_ID 
, cr.Carrier_Region_ID, cr.Service_Area_ID
from `marketplace_db`.`company` `c` 
left join `marketplace_db`.`company_service` `cs` on `cs`.`Company_ID` = `c`.`Company_ID`
left join `marketplace_db`.`service` `s` on `s`.`Service_ID` = `cs`.`Service_ID`
left join `marketplace_db`.`carrier_region` `cr` on `cr`.`Company_ID` = `c`.`Company_ID` 
join `marketplace_db`.`service_area` `sa` on `sa`.`Service_Area_ID` = `cr`.`Service_Area_ID`
where c.is_carrier = 1;
