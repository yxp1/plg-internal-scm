/*
use mysql
-- drop database marketplace_db;
create database marketplace_db;
use marketplace_db;
*/

CREATE TABLE `role` (
  `Role_Id` int(11) NOT NULL,
  `Role_Name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`Role_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into `role` values
(-1,	'Unknown'),
(1,	'Driver'),
(2,	'Consignee'),
(3,	'Shipper')
;


CREATE TABLE `provider` (
  `Provider_Id` int(11) NOT NULL AUTO_INCREMENT,
  `SCAC_CD` varchar(8) DEFAULT NULL,
  `Provider_Name` varchar(256) NOT NULL,
  `vendor_code` varchar(8) NOT NULL,
  PRIMARY KEY (`Provider_Id`),
  UNIQUE KEY `ix_provider_vendor_code` (`vendor_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


insert into `provider`(`Provider_Id`,`vendor_code`,`Provider_Name`) values 
(-1,'UNKNOWN','UNKNOWN')
, (1,'XXL','XXL Summergate Carrier')
, (8,'YYY','YYY')
, (13,'HT','HT Summergate Carrier')
, (14,'TH','TH Summergate Carrier')
, (15,'OE10','OPENPORT1212')
, (16,'OPENPT','OPENPT')
, (17,'KGEX','Kings Express')
, (18,'DM01','DEMO Carrier1')
, (19,'DM02','Demo Carrier 2')
, (20,'DM03','Demo Carrier 3')
, (21,'DM04','DEMO Carrier # 4')
, (22,'USXI','US Xpress')
, (23,'PPXE','Pena, Pena Express')
, (24,'TLIN','Transco Lines')
, (25,'CRPS','CRST Van Expedited')
, (26,'WARD','Ward Trucking')
, (27,'WTVA','Wilson Trucking')
, (28,'ETTL','ET Trucking')
, (29,'ULSE','ULS Express')
, (30,'MART','MARTEN TRANSPORT')
, (31,'LSTR','Landstar Ranger')
, (32,'TSXL','Transtar Express')
, (33,'MLAT','MLA Transports')
, (34,'SETP','Selective Transportation')
, (35,'ODFL','Old Dominon Freight Line')
, (36,'ATSI','ATS')
, (37,'PASC','PTL')
, (38,'HRZA','HORIZON-AIR')
, (39,'DONK','Don''s Trucking')
, (40,'DEMO2','DEMO Carrier2')
, (41,'DEM01','DM01')
, (42,'ACWL','ACW LOGISTICS')
, (43,'MVRK','MAVERICK TRANSPORT')
, (44,'JBET','JB Elite')
, (45,'BBR','PT Bintang Baru')
, (46,'ANTK','Antartika Carrier')
, (47,'CCCC','Cal Cartage Company')
, (48,'BFRC','PT Bafardi Internal Truck Fleet')
, (49,'ATNK','')
, (51,'PTPW','Pioneer Transport')
, (52,'BFRD','')
, (53,'JBHU','JB Hunt')
, (54,'AJTR','AJ Transportation')
, (55,'SLWC','Stream Logistics and Warehousing')
, (56,'IMFF','IMF')
, (57,'USATL','AZ- CFS Atlanta')
, (64,'HLXL','HELIX LOGISTICS')
, (65,'KLTL','KLTL');

update provider set SCAC_CD = vendor_code;


CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(128) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `password_hash` varchar(256) NOT NULL,
  `phone_nbr` varchar(15),
  `Primary_User_Role_Id` int(11) default -1 NOT NULL,
  `Default_Provider_Id` int(11) default -1 NOT NULL,
  `active_Token` varchar(40),
  `token_expiration_dt` datetime,
  `international_country_cd` int(11),
  `actual_mobile_phone_nbr` varchar(12),
  `is_Test` bit(1) DEFAULT 0 NOT NULL,
  `create_user_id` varchar(120) NOT NULL,
  `Create_Dt` datetime NOT NULL,
  `Update_Dt` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ix_user_phone_nbr` (`phone_nbr`),
  UNIQUE KEY `ix_user_email_address` (`email_address`),
  KEY `FK_user_Primary_Role_Id` (`Primary_User_Role_Id`),
  KEY `FK_user_Provider_Id` (`Default_Provider_Id`),
  CONSTRAINT `FK_user_Primary_Role_Id` FOREIGN KEY (`Primary_User_Role_Id`) REFERENCES `role` (`Role_Id`),
  CONSTRAINT `FK_user_Provider_Id` FOREIGN KEY (`Default_Provider_Id`) REFERENCES `provider` (`Provider_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `user_provider` (
  `user_provider_id` int(11) NOT NULL AUTO_INCREMENT,
  `Provider_Id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `Create_Dt` datetime NOT NULL,
  `Update_dt` datetime NOT NULL,
  PRIMARY KEY (`user_provider_id`),
  KEY `IX_user_provider_Provider_Id` (`Provider_Id`),
  KEY `IX_user_provider_user_id` (`user_id`),
  CONSTRAINT `FK_user_provider_Provider_Id` FOREIGN KEY (`Provider_Id`) REFERENCES `provider` (`Provider_Id`),
  CONSTRAINT `FK_user_provider_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `user_session` (
  `user_session_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(40) NOT NULL,
  `token_expiration_dt` datetime DEFAULT NULL,
  `device_name` varchar(128),
	`device_type` varchar(128),
	`identify_key` varchar(128),
	`device_version` varchar(128),
	`app_from` varchar(128), -- ":"Application source",
	`app_id` varchar(128), -- ":"Application ID, it is unique",
	`app_version` varchar(128),
  `Create_Dt` datetime NOT NULL,
  PRIMARY KEY (`user_session_id`),
  KEY `FK_user_session_user_id` (`user_id`),
  CONSTRAINT `FK_user_session_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;







/*
SELECT * FROM `user` u 
WHERE email_address = 'oscar@ef3systems.com' 
and password_hash = 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'

insert into `user` (email_address,
first_name,
last_name,
password_hash,
create_user_id,
Create_Dt,
Update_dt) values ('oscar@ef3systems.com','Oscar','Yu',
'94bdcebe19083ce2a1f959fd02f964c7af4cfc29',
'self', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()
)
;
*/