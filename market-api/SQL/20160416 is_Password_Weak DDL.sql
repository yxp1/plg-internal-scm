use marketplace_db;

ALTER TABLE `user` MODIFY COLUMN `Is_Password_Weak` bit(1) NOT NULL DEFAULT b'1';
