package com.openport.marketplace.repository;


import static org.junit.Assert.assertEquals;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.junit.Test;

public class MailGunTest {

	// @Test
	public void sendMessageTest() throws Exception {
		String sender = "Do-Not-Reply@openport.com <postmaster@openport.com>";
		String sendTo = ""; // supply email address to send to
		String subject = "[TEST ONLY] test sending message please disregard";
		String message = "[TEST ONLY] Your new temporary password is ***";
		
		Response response = MailGun.sendMessage(sender, sendTo, subject, message);
		assertEquals(response.getStatus(), HttpServletResponse.SC_OK);
	}
}
