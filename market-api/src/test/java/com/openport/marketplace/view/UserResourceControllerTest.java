package com.openport.marketplace.view;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.openport.marketplace.InMemoryRestServer;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.User;


// NOTE: Add @Test annotation to the unit test methods

public class UserResourceControllerTest {

    public static UserResource sut = new UserResource();
    public static InMemoryRestServer server;

    @BeforeClass
    public static void beforeClass() throws Exception {
        server = InMemoryRestServer.create(sut);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        server.close();
    }

    // @Test
    public void registerUserTest() throws Exception {
    	// Test if endpoint is accessible without authorization token
    	
    	Company company = new Company();
    	company.setIsShipper(true);
    	company.setCompanyName("OPENPT");
    	
    	User user = new User();
    	user.setFirstName("fname");
    	user.setLastName("lname");
    	user.setPassword("welcome1");
    	user.setUsername("asd6@mail.com");
    	user.setCompany(company);
    	
    	ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        Response response = resteasyClient.target("http://localhost:8080/market-api/users").request().buildPost(Entity.json(user)).invoke();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
    // @Test
    public void forgotPasswordTest() throws Exception {
    	// Test if endpoint is accessible without authorization token
    	
    	User user = new User();
    	user.setFirstName("fname");
    	user.setLastName("lname");
    	user.setUsername("asd6@mail.com");
    	
        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        Response response = resteasyClient.target("http://localhost:8080/market-api/users/forgotPassword").request().buildPost(Entity.json(user)).invoke();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
    // @Test
    public void getVendorsTest() throws Exception {
    	// Test if endpoint is accessible without authorization token
        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        Response response = resteasyClient.target("http://localhost:8080/market-api/shippers/vendorlist/hk").request().buildGet().invoke();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
    // @Test
    public void getLanguagesTest() throws Exception {
    	// Test if endpoint is accessible without authorization token
        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        Response response = resteasyClient.target("http://localhost:8080/market-api/users/languages").request().buildGet().invoke();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
    // @Test
    public void getLabelsTest() throws Exception {
    	// Test if endpoint is accessible without authorization token
        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        Response response = resteasyClient.target("http://localhost:8080/market-api/users/labels/en/114/1.4.0/com.openport.booking").request().buildGet().invoke();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
    // @Test
    public void getLanguageUpdateListTest() throws Exception {
    	// Test if endpoint is accessible without authorization token
        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        Response response = resteasyClient.target("http://localhost:8080/market-api/users/lang/4557/en/1.4.0/com.openport.booking").request().buildGet().invoke();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
}
