package com.openport.marketplace.view;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.openport.marketplace.json.User;
import com.openport.marketplace.repository.MailGun;
import com.openport.marketplace.repository.Notify;


@RunWith(PowerMockRunner.class)
@PrepareForTest({Notify.class, MailGun.class})
public class TruckerResourceTest {
	
	@Before
    public void setUp() {
		PowerMockito.mockStatic(Notify.class);
		PowerMockito.mockStatic(MailGun.class);
    }
	
	//@Test
	public void testNotificationBody() throws Exception {
		
		Mockito.when(Notify.logEmail(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(true);
		Mockito.when(MailGun.sendMessage(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(Response.ok().build());
		
		User user = new User();
		user.setEmailAddress("mail@email.com");
		user.setPassword("pass123");
		
		Whitebox.invokeMethod(new TruckerResource(), "emailNewUser", user);

	}
	
}
