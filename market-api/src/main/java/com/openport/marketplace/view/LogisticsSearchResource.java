package com.openport.marketplace.view;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.openport.marketplace.json.AccessorialCode;
import com.openport.marketplace.json.BCModel;
import com.openport.marketplace.json.ConsolidatedShipment;
import com.openport.marketplace.json.Currency;
import com.openport.marketplace.json.EquipmentType;
import com.openport.marketplace.json.LogisticsColumns;
import com.openport.marketplace.json.PaymentTerm;
import com.openport.marketplace.json.ProofDocument;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.TrackTrace;
import com.openport.marketplace.model.LogisticsTab;
import com.openport.marketplace.model.StatusEvent;
import com.openport.marketplace.parser.DateSimpleFormat;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.marketplace.repository.DbConfigODS;
import com.openport.marketplace.repository.DbConfigRater;
import com.openport.marketplace.repository.DbConfigTMS;
import com.openport.marketplace.repository.DbConfigTmsUtf8;
import com.openport.marketplace.repository.LogisticsRepository;
import com.openport.marketplace.repository.LogisticsSearch;
import com.openport.util.db.ResultSetMapper;


//created by: jero.dungog@openport.com
@Path("logistics")
public class LogisticsSearchResource {

	static final transient Logger log = Logger.getLogger(LogisticsSearchResource.class);
	private final String BCUrl = "http://explore.openport.com/#/tx/";
	@POST // http://localhost:8080/market-api/trucker
	@Path("batchTender") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<LogisticsTab> doBatchTendering(List<LogisticsTab> batchList) {
		
		try {
			for(LogisticsTab lTab : batchList) {
				doTender(lTab.getOrder_nbr(), lTab.getDelivery_nbr(), lTab.getPARENT_PARTNER_CD(),lTab.getPARTNER_CD(), lTab.getSHIPPER_CD(), lTab.getRecommended_carrier(), lTab.getPRO_BILL_NBR(), lTab.getUserId(), lTab.getLegNumber(),lTab.getUserTimezone());
			}
			return batchList;
		} catch (Exception e) {
			log.info("error", e);
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);

		}
		return null;
	}
	
	private void doTender(String poNbr,String releaseNbr,String parentPartnerCd,String partnerCd,String shipperCd,String carrier, String proBillNbr,String userId,String legNbr,String timezone  ) {
		Date dt = new Date();
		if(timezone != null) {
			dt=formatToLocalTime(new Date(), timezone);
		}
			
		String spName = "sp_lc_batch_tendering_update";
		String sql = "call " + spName + " (?,?,?,?,?,?,?,?,?,?)";
		//(poNbr varchar(50), releaseNbr varchar(50),parentPartnerCd varchar(12),partnerCd varchar(12),shipperCd varchar(12),carrier varchar(6),proBillNbr varchar(12), currentDt datetime,userid varchar(128),legNbr varchar(2))
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				java.sql.CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.setString(1, poNbr);
			pstmt.setString(2, releaseNbr);
			pstmt.setString(3, parentPartnerCd);
			pstmt.setString(4, partnerCd);
			pstmt.setString(5, shipperCd);
			pstmt.setString(6, carrier);
			pstmt.setString(7, proBillNbr);
			pstmt.setTimestamp(8, new Timestamp(dt.getTime()));
			pstmt.setString(9, userId);
			pstmt.setString(10, legNbr);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			log.error("Error message: " + proBillNbr + " " +  e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
	}
	
	
	@GET
	@Path("byproductcode") // http://localhost:8080/market-api/logistics/byproductcode
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseProductCodeQueryByProductCode(@QueryParam("accountCode") String accountCode,
			@QueryParam("productCode") String productCode, @DefaultValue("0") @QueryParam("filter") Integer filter) {
		log.info("tmsParameters/accounts/defaultParameters");
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseProductCodeQueryByProductCode(accountCode,
				productCode, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("byconsigneeorder") // http://localhost:8080/market-api/logistics/byconsigneeorder
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByConsigneeOrder(@QueryParam("accountCode") String accountCode,
			@QueryParam("purchaseOrderNumber") String purchaseOrderNumber,
			@DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByConsigneeOrder(accountCode,
				purchaseOrderNumber, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("bydatecreated") // http://localhost:8080/market-api/logistics/bydatecreated
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByDateCreated(@QueryParam("accountCode") String accountCode,
			@QueryParam("date1") String date1, @QueryParam("date2") String date2,
			@DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByDateCreated(accountCode, date1, date2,
				filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByDeliveryShipmentSOReleaseNbr") // http://localhost:8080/market-api/logistics/ByDeliveryShipmentSOReleaseNbr
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByDeliveryShipmentSOReleaseNbr(@QueryParam("accountCode") String accountCode,
			@QueryParam("releaseNbr") String releaseNbr, @DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByDeliveryShipmentSOReleaseNbr(accountCode,
				releaseNbr, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByManualBatchTendering") // http://localhost:8080/market-api/logistics/ByManualBatchTendering
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByManualBatchTendering(@QueryParam("accountCode") String accountCode,
			@QueryParam("date1") String date1, @QueryParam("date2") String date2,
			@DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByManualBatchTendering(accountCode, date1,
				date2, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByPickupDate") // http://localhost:8080/market-api/logistics/ByPickupDate
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByPickupDate(@QueryParam("accountCode") String accountCode,
			@QueryParam("date1") String date1, @QueryParam("date2") String date2,
			/*@QueryParam("pickupDate") String pickupDate,*/ @DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByPickupDate(accountCode, date1, date2, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByPOOrderNbr") // http://localhost:8080/market-api/logistics/ByPOOrderNbr
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByPOOrderNbr(@QueryParam("accountCode") String accountCode,
			@QueryParam("poOrderNbr") String poOrderNbr, @DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByPOOrderNbr(accountCode, poOrderNbr, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByPROBLPLNbr") // http://localhost:8080/market-api/logistics/ByPROBLPLNbr
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByPROBLPLNbr(@QueryParam("accountCode") String accountCode,
			@QueryParam("proBillNbr") String proBillNbr, @DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByPROBLPLNbr(accountCode, proBillNbr, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByStatusNotTendereYet") // http://localhost:8080/market-api/logistics/ByStatusNotTendereYet
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByStatusNotTendereYet(@QueryParam("accountCode") String accountCode,
			@QueryParam("date1") String date1, @QueryParam("date2") String date2,
			@DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByStatusNotTendereYet(accountCode, date1, date2,
				filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByStatusTendered") // http://localhost:8080/market-api/logistics/ByStatusTendered
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByStatusTendered(@QueryParam("accountCode") String accountCode,
			@QueryParam("date1") String date1, @QueryParam("date2") String date2,
			@DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByStatusTendered(accountCode, date1, date2,
				filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}

	@GET
	@Path("ByStatusWaitingForAcknowledgement") // http://localhost:8080/market-api/logistics/ByStatusWaitingForAcknowledgement
	@Produces({ MediaType.APPLICATION_JSON })
	public Response searchBaseQueryByStatusWaitingForAcknowledgement(@QueryParam("accountCode") String accountCode,
			@QueryParam("date1") String date1, @QueryParam("date2") String date2,
			@DefaultValue("0") @QueryParam("filter") Integer filter) {
		LogisticsSearch logisticsSearch = new LogisticsSearch();
		List<LogisticsTab> resultsList = logisticsSearch.searchBaseQueryByStatusWaitingForAcknowledgement(accountCode,
				date1, date2, filter);
		String json = parseToJson(resultsList);

		return Response.ok(json).build();
	}
	
	@GET // http://localhost:8080/market-api/logistics/columns/default
	@Path("columns/default")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<LogisticsColumns> getDefaultColumns() {
		
		List<LogisticsColumns> list = new ArrayList<LogisticsColumns>();
		try {
			LogisticsRepository logistics = new LogisticsRepository();
			list = logistics.getDefaultColumns();
			
		} catch (SQLTimeoutException e) {
			new ServiceException(Response.Status.GATEWAY_TIMEOUT);
		} catch (SQLFeatureNotSupportedException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (JsonParseException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (JsonMappingException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (SQLException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (IOException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (Exception e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
		
		return list;
	}
	
	@GET // http://localhost:8080/market-api/logistics/columns?userId=231
	@Path("columns")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<LogisticsColumns> getUserColumns(@QueryParam("userId") Integer userId) {
		
		List<LogisticsColumns> list = new ArrayList<LogisticsColumns>();
		try {
			LogisticsRepository logistics = new LogisticsRepository();
			list = logistics.getUserColumns(userId);
			
		} catch (SQLTimeoutException e) {
			new ServiceException(Response.Status.GATEWAY_TIMEOUT);
		} catch (SQLFeatureNotSupportedException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (JsonParseException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (JsonMappingException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (SQLException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (IOException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (Exception e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
		
		return list;
	}
	
	@POST // http://localhost:8080/market-api/logistics/columns/insert
	@Path("columns/insert/{userId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void setUserColumns(@PathParam("userId") Integer userId, List<LogisticsColumns> list) {
		try {
			LogisticsRepository logistics = new LogisticsRepository();
			logistics.setUserColumns(userId, list);
			
		} catch (SQLTimeoutException e) {
			new ServiceException(Response.Status.GATEWAY_TIMEOUT);
		} catch (SQLFeatureNotSupportedException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (JsonParseException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (JsonMappingException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (SQLException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (IOException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (Exception e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
	}
	
	@GET // http://localhost:8080/market-api/logistics/track
	@Path("track")
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<TrackTrace> track(@QueryParam("reference") String reference, 
			@QueryParam("defaultTz") String currentTz,
			@QueryParam("currentTz") String convTz, 
			@QueryParam("picturePath") String picturePath,
			@DefaultValue("1") @QueryParam("dataStagingThreshold") Integer dataStagingThreshold) {	
		
		
		// String abs = getPicturePath();
//		log.info("threshold param" + threshold);
//		if (threshold != null) {
//			dataStagingThreshold = threshold;
//		}
//		log.info("threshold " + dataStagingThreshold);
		
		
		currentTz = currentTz.replaceAll("-", "/");
		convTz = convTz.replaceAll("-", "/");
		
		Map<String, Integer> proMap = new HashMap<>();
		Map<Integer, List<ProofDocument>> docMap = new HashMap<>();
		Map<Long,String> bcMap = new HashMap<>();
		List<BCModel> bcList = fetchBCHashValue(reference);
		if(bcList != null && bcList.size() > 0) {
			for(BCModel bcm : bcList) {
				log.info(" " + bcm.getBcHashValue() + " " + bcm.getActivityId());
				bcMap.put(bcm.getActivityId(), bcm.getBcHashValue());
			}
		}
		try {
			String sql = "select distinct\n" + "ac.is_deleted,\n" + "ac.CREATE_USER_ID,\n" + "ac.activity_id,\n"
					+ " td.hh24minute_value,\n" + "td.seconds_value,\n" + "dt.date_value,\n" + "ed.event_desc_txt,\n"
					+ " sd.BOL_NBR,\n" + "sd.PROBILL_NBR,\n" + "pd.po_nbr,\n" + "pd.po_release_nbr,\n" + "ld.loc_txt,\n"
					+ " ld.UN_LOCATION_CD,\n" + "cd.comment_txt,\n" + "doc.relative_path,\n" + "doc.create_dt,\n"
					+ " doc.activity_id, ed.EVENT_CD\n" 
					+ " from activity ac inner join time_dimension td on ac.EVENT_TIME_ID=td.TIME_ID\n"
					+ " inner join date_dimension dt on ac.EVENT_DATE_ID = dt.DATE_ID\n"
					+ " inner join event_dimension ed on ac.event_id=ed.event_id\n"
					+ " inner join shipment_dimension sd ON sd.SHIPMENT_ID = ac.SHIPMENT_ID\n"
					+ " inner join po_dimension pd ON  pd.PURCHASE_ORDER_ID = ac.PURCHASE_ORDER_ID\n"
					+ " inner join location_dimension ld on ld.LOC_ID = ac.EVENT_LOC_ID\n"
					+ " inner join comment_dimension cd ON cd.COMMENT_ID = ac.COMMENT_ID\n"
					+ " left outer join document doc on doc.Activity_ID = ac.Activity_ID\n" + " where (sd.bol_nbr='"
					+ reference + "' or sd.PROBILL_NBR='" + reference + "')\n" + " and ac.is_deleted = 0";
			
			log.info("TNT sql:" + sql);
			
			try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				List<TrackTrace> ttList = new LinkedList<>();
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						TrackTrace tt = new TrackTrace();
						tt.setIs_deleted(rs.getBoolean(1));
						tt.setCreateUserId(rs.getString(2));
						tt.setActivityId(rs.getInt(3));
						String hh24 = rs.getString(4);
						Integer sec = rs.getInt(5);
						String hhsec = "";
						if(bcMap.get(tt.getActivityId() * 1L)!=null) {
							
							tt.setBcTransaction(this.BCUrl + bcMap.get(tt.getActivityId() * 1L));
						
						}
						if (sec < 10) {
							hhsec = hh24 + ":0" + sec;
						} else {
							hhsec = hh24 + ":" + sec;
						}
						
						Date tm = rs.getDate(6);
						Date createDt = new Date();

						tt.setSortDate(formatRealTimeYYYMMddhhmmss(tm, hhsec));
						tm = tt.getSortDate();

						if (!trimString(currentTz).equalsIgnoreCase(trimString(convTz))) {
							log.info("current time" + tm);
							tm = formatToLocalTime(tm, convTz);
							log.info("after time" + tm + " " + convTz);

							createDt = formatToLocalTime(createDt, convTz);
						}

						tt.setEventDt(DateSimpleFormat.formatSimpleDate(tm, "yyyy-MM-dd HH:mm:ss"));
						tt.setEventDesc(rs.getString(7));
						tt.setBolNbr(rs.getString(8));
						tt.setProBillNbr(rs.getString(9));
						tt.setPoNbr(rs.getString(10));
						tt.setPoReleaseNbr(rs.getString(11));
						tt.setLocation(rs.getString(12));
						String unLocCd = rs.getString(13);
						if (unLocCd != null) {
							tt.setLocation(tt.getLocation() + "(" + unLocCd + ")");
						}
						tt.setComments(rs.getString(14));
						String path = rs.getString(15);
						/*Date docDt = rs.getDate(16);*/
						
						tt.setEventCode(rs.getString("EVENT_CD"));
						
						String bcId = "blockchain-link-" + rs.getString("EVENT_CD").toLowerCase();
						tt.setBcTransactionId(bcId);
						

						if (path != null) {
							List<ProofDocument> pdoc = new LinkedList<>();
							if (docMap.get(tt.getActivityId()) != null) {
								pdoc = docMap.get(tt.getActivityId());
							}
							
							ProofDocument pd = new ProofDocument();
							pd.setActivityId(tt.getActivityId());
							pd.setCreateDt(DateSimpleFormat.formatSimpleDate(tm, "yyyy-MM-dd HH:mm:ss"));
							pd.setPath(path);
							// pd.setRootPath(picturePath);
							// pd.setAbsolutePath(pd.getRootPath() +
							// pd.getPath());
							
							pdoc.add(pd);
							
							docMap.put(tt.getActivityId(), pdoc);
						}
						
						if (proMap.get(tt.getBolNbr() + "-" + tt.getActivityId()) == null) {
							ttList.add(tt);
							proMap.put(tt.getBolNbr() + "-" + tt.getActivityId(), 1);
						}
					}
				}

				List<TrackTrace> pojoList = new LinkedList<>();
				if (ttList != null && ttList.size() > 0) {
					for (TrackTrace tl : ttList) {
						tl.setUuid((Long.valueOf(DateSimpleFormat.formatSimpleDate(tl.getSortDate(), "yyyyMMddHHmmss"))
								* dataStagingThreshold) + tl.getActivityId());
						/*List<ProofDocument> pdoc = new LinkedList<>();*/
						if (docMap.get(tl.getActivityId()) != null) {
							tl.setProofDocument(docMap.get(tl.getActivityId()));
							pojoList.add(tl);
						} else {
							pojoList.add(tl);
						}
					}
					
					return pojoList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	
	@POST // http://localhost:8080/market-api/logistics/event
	@Path("event")
	public Integer createEvent(@FormDataParam("proBillNbr") String shipmentNbr, 
			@FormDataParam("application") String application, 
			@FormDataParam("statusCode") String statusCd, 
			@FormDataParam("statusDescription") String statusDesc, 
			@FormDataParam("eventDate") Long eventDt, 
			@FormDataParam("details") String commentTxt, 
			@FormDataParam("userId") String userId) {
		
		// sp_RecordTrackAndTrace_By_ProBillNbr_Return_2 added in checking the event date
		// if event date is (added event) is the latest then update the po_header
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall("call sp_RecordTrackAndTrace_By_ProBillNbr_Return_2(?,?,?,?,?,?,?,?)");) {
			pstmt.setString(1, shipmentNbr);
			pstmt.setString(2, application);
			pstmt.setString(3, statusCd);
			pstmt.setString(4, statusDesc);
			pstmt.setTimestamp(5, new Timestamp(new Long(eventDt)));
			pstmt.setString(6, commentTxt);
			pstmt.setString(7, userId);
			
			pstmt.registerOutParameter(8, java.sql.Types.INTEGER);
			pstmt.executeUpdate();
			
			 return pstmt.getInt(8);
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		 return 0;
	}
	
	@POST // http://localhost:8080/market-api/logistics/document
	@Path("document")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ProofDocument createDocument(ProofDocument docs) {
		return createDocumentByActivityId(docs, docs.getActivityId(), docs.getPath(), docs.getTimezone());
	}
	
	private ProofDocument createDocumentByActivityId(ProofDocument docs, Integer activityId, String path, String timezone) {
		String date = DateSimpleFormat.formatSimpleDate(formatToLocalTime(new Date(), timezone), "yyyy-MM-dd HH:mm:ss");
		String sql = "insert document (activity_id,relative_path,create_dt,tenant_id,partner_id,buyer_id,consignee_id,event_id,event_loc_id,event_date_id,event_time_id,purchase_order_id,"
				+ "booking_id,shipment_id,cargo_load_id,carrier_id,equipment_id,vessel_id,voyage_id,origin_loc_id,destination_loc_id,comment_id,customer_id)"
				+ "values" + "(" + activityId + "," + "'" + path + "'," + "'" + date + "'" + "," + -1 + "," // tenant
																											// id
				+ -1 + "," // partner_cd
				+ -1 + "," // buyer_id
				+ -1 + "," // consignee id
				+ -1 + "," // event id
				+ -1 + "," // evnent loc id
				+ -1 + "," // event date
				+ -1 + "," // event time
				+ -1 + "," // po
				+ -1 + ","// booking id
				+ -1 + "," // shipment id
				+ -1 + "," // cargo load id
				+ -1 + "," // carrier id
				+ -1 + "," // equipent id
				+ -1 + "," // vessel id
				+ -1 + "," // voyage id
				+ -1 + "," // origin loc id
				+ -1 + "," // destination loc id
				+ -1 + "," // comment id
				+ -1 + ")"; // customner
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.executeUpdate();
			if (docs != null) {
				docs.setCreateDt(date);

				return docs;
			} else {
				return new ProofDocument();
			}
		} catch (Exception e) {
			log.info("error", e);
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}
	
	@GET  // http://localhost:8080/market-api/logistics/status
    @Path("status")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public List<StatusEvent> getStatus() {
    	List<StatusEvent> statusList = new ArrayList<StatusEvent>();
    	
		String sql = "select * from tms_utf8_db.status_event;";
		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
	        	while(rs.next()) {
	        		StatusEvent event = new StatusEvent();
	        		event.setApplication(rs.getString("application"));
	        		event.setEventCode(rs.getString("event_cd"));
	        		event.setParentPartnerCode(rs.getString("parent_partner_cd"));
	        		event.setPartnerCode(rs.getString("partner_cd"));
	        		event.setStatus(rs.getString("status"));
	        		event.setEventDescription(rs.getString("event_desc"));
	        		event.setEventLocationCode(rs.getString("event_location_cd"));
	        		
	        		statusList.add(event);
	        	}
	        	
	        	return statusList;
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return null;
   	}
	
	@DELETE // http://localhost:8080/market-api/logistics/trackTrace/{id}/{probill}
	@Path("trackTrace/{id}/{probill}")
	public void delete(@PathParam("id") Integer activityId, @PathParam("probill") String probillNumber) {
		
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall("call sp_delete_activity(?,?)");) {
			pstmt.setString(1, probillNumber);
			pstmt.setInt(2, activityId);
			pstmt.execute();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	@GET  // http://localhost:8080/market-api/logistics/timezone
    @Path("timezone")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public List<StatusEvent> getTimezones() {
    	
		String sql = "select * from timezone_dimension;";
		
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
	        	while(rs.next()) {
	        	}
	        	
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return null;
   	}
	
	@POST // http://localhost:8080/market-api/logistics/postMarketplace
	@Path("postMarketplace")
	@Consumes("application/json")
	public boolean postMarketplace(String data) {
		boolean result = false;
		
		try {
			LogisticsRepository logistics = new LogisticsRepository();
			result = logistics.postMarketplace(data);
			
		} catch (SQLTimeoutException e) {
			new ServiceException(Response.Status.GATEWAY_TIMEOUT);
		} catch (SQLFeatureNotSupportedException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (JsonParseException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (JsonMappingException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (SQLException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (IOException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (Exception e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
		
		return result;
	}
	
	@GET  // http://localhost:8080/market-api/logistics/accessorials
    @Path("accessorials")
	@Consumes("application/json,application/vnd.openport.market.v1+json")
   	@Produces({ MediaType.APPLICATION_JSON })
	public List<AccessorialCode> getMpAccessorial(@QueryParam("parentPartnerCode") String parentPartnerCode) {
		String sql = "select accessorial_cd,accessorial_type,description,customer_cd,parent_partner_cd "
				+ " from accessorial_code where parent_partner_cd='MP' and customer_cd in ('*', ?) order by description";
		
		try (java.sql.Connection connection = DbConfigRater.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, parentPartnerCode);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<AccessorialCode> driverRawMapper = new ResultSetMapper<AccessorialCode>();
				List<AccessorialCode> pojoListObj = driverRawMapper.mapResultSetToObject(rs, AccessorialCode.class);
				if (pojoListObj != null && pojoListObj.size() > 0) {
					return pojoListObj;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return new ArrayList<AccessorialCode>();
	}
	
	@GET  // http://localhost:8080/market-api/logistics/paymentTerms
    @Path("paymentTerms")
   	@Produces({ MediaType.APPLICATION_JSON })
	public List<PaymentTerm> getPaymentTermSelection() {
		String sql = "select payment_term_id, payment_term_cd from marketplace_db.payment_term where payment_term_id > -1;";
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<PaymentTerm> driverRawMapper = new ResultSetMapper<PaymentTerm>();
				List<PaymentTerm> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, PaymentTerm.class);
				if (pojoListObj==null) {
					return new ArrayList<PaymentTerm>();
				} else {
					return pojoListObj;
				}
		     }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return new ArrayList<PaymentTerm>();
	
	}
	
	@GET  // http://localhost:8080/market-api/logistics/currencies
    @Path("currencies")
   	@Produces({ MediaType.APPLICATION_JSON })
	public List<Currency> getCurrencySelection() {
		String sql = "select currency_cd,currency_name  from marketplace_db.currency;";
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Currency> driverRawMapper = new ResultSetMapper<Currency>();
				List<Currency> pojoListObj = driverRawMapper.mapResultSetToObject(rs, Currency.class);
				if (pojoListObj == null) {
					return new ArrayList<Currency>();
				} else {
					return pojoListObj;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return new ArrayList<Currency>();
	}
	
	@GET  // http://localhost:8080/market-api/logistics/equipmentTypes
    @Path("equipmentTypes")
   	@Produces({ MediaType.APPLICATION_JSON })
	public List<EquipmentType> getEquipmentTypeSelection() {
		String sql = "select equipment_type_id,equipment_type_name from marketplace_db.equipment_type where equipment_type_id > -1 order by equipment_type_name;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) 
			{
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EquipmentType> driverRawMapper = new ResultSetMapper<EquipmentType>();
				List<EquipmentType> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, EquipmentType.class);
				if (pojoListObj==null) {
					return new ArrayList<EquipmentType>();
				} else {
					return pojoListObj;
				}
		     }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return new ArrayList<EquipmentType>();
	}
	
	@GET
	@Path("verify/rate")
	public boolean checkLeg(@QueryParam("releaseNumber") String releaseNumber) {
		String sql = "select COUNT(*) AS cnt from tms_utf8_db.po_release_carrier where RELEASE_NBR = ?";
		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setString(1, releaseNumber);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				int count = 0;
				if(rs.next()) {
					log.info("rs " + rs.getString(1));
					if(rs.getString(1).equalsIgnoreCase("0")) {
						return false;
					}
	        		++count;
	        	}
				
				if(count > 0) {
        			return true;
        		}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return false;
	}
	
	@GET
	@Path("shipment/consolidated")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ConsolidatedShipment> getConsoloditedShipments(@QueryParam("deliveryNumber") String parent) {
		List<ConsolidatedShipment> list = new ArrayList<ConsolidatedShipment>();
		
		try {
			if(!parent.isEmpty()) {
				LogisticsRepository logistics = new LogisticsRepository();
				list = logistics.getConsoloditedShipemnts(parent);
			}
		} catch (SQLTimeoutException e) {
			new ServiceException(Response.Status.GATEWAY_TIMEOUT);
		} catch (SQLException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (Exception e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
		
		return list;
	}

	private String parseToJson(List<LogisticsTab> resultsList) {
		JsonArrayBuilder jsonArray = Json.createArrayBuilder();
		for (LogisticsTab logTab : resultsList) {
			JsonObjectBuilder jsonObject = Json.createObjectBuilder();
			jsonObject.add("assigned_carrier", ((logTab.getAssigned_carrier() == null) ? "" : logTab.getAssigned_carrier()));
			jsonObject.add("category", ((logTab.getCategory() == null) ? "" : logTab.getCategory()));
			jsonObject.add("consignee_order_nbr", ((logTab.getConsignee_order_nbr() == null) ? "" : logTab.getConsignee_order_nbr()));
			jsonObject.add("CUSTOMER_CD", ((logTab.getCUSTOMER_CD() == null) ? "" : logTab.getCUSTOMER_CD()));
			jsonObject.add("delivery_nbr", ((logTab.getDelivery_nbr() == null) ? "" : logTab.getDelivery_nbr()));
			jsonObject.add("dest_address", ((logTab.getDest_address() == null) ? "" : logTab.getDest_address()));
			jsonObject.add("dest_city", ((logTab.getDest_city() == null) ? "" : logTab.getDest_city()));
			jsonObject.add("dest_district", ((logTab.getDest_district() == null) ? "" : logTab.getDest_district()));
			jsonObject.add("dest_state_province", ((logTab.getDest_state_province() == null) ? "" : logTab.getDest_state_province()));
			jsonObject.add("destination", ((logTab.getDestination() == null) ? "" : logTab.getDestination()));
			jsonObject.add("driver_phone", ((logTab.getDriver_phone() == null) ? "" : logTab.getDriver_phone()));
			jsonObject.add("equipment_type", ((logTab.getEquipment_type() == null) ? "" : logTab.getEquipment_type()));
			jsonObject.add("order_nbr", ((logTab.getOrder_nbr() == null) ? "" : logTab.getOrder_nbr()));
			jsonObject.add("order_type", ((logTab.getOrder_type() == null) ? "" : logTab.getOrder_type()));
			jsonObject.add("Order_upload_Dt", ((logTab.getOrder_upload_Dt() == null) ? "" : logTab.getOrder_upload_Dt()));
			jsonObject.add("origin", ((logTab.getOrigin() == null) ? "" : logTab.getOrigin()));
			jsonObject.add("PARENT_PARTNER_CD", ((logTab.getPARENT_PARTNER_CD() == null) ? "" : logTab.getPARENT_PARTNER_CD()));
			jsonObject.add("PARTNER_CD", ((logTab.getPARTNER_CD() == null) ? "" : logTab.getPARTNER_CD()));
			jsonObject.add("pickup_dt", ((logTab.getPickup_dt() == null) ? "" : logTab.getPickup_dt()));
			jsonObject.add("PRO_BILL_NBR", ((logTab.getPRO_BILL_NBR() == null) ? "" : logTab.getPRO_BILL_NBR()));
			jsonObject.add("rated_cost", ((logTab.getRated_cost() == null) ? "" : logTab.getRated_cost()));
			jsonObject.add("recommended_carrier", ((logTab.getRecommended_carrier() == null) ? "" : logTab.getRecommended_carrier()));
			jsonObject.add("requested_delivery_dt", ((logTab.getRequested_delivery_dt() == null) ? "" : logTab.getRequested_delivery_dt()));
			jsonObject.add("sales_channel", ((logTab.getSales_channel() == null) ? "" : logTab.getSales_channel()));
			jsonObject.add("SHIPPER_CD", ((logTab.getSHIPPER_CD() == null) ? "" : logTab.getSHIPPER_CD()));
			jsonObject.add("status", ((logTab.getStatus() == null) ? "" : logTab.getStatus()));
			jsonObject.add("total_qty", logTab.getTotal_qty());
			jsonObject.add("transportation", ((logTab.getTransportation() == null) ? "" : logTab.getTransportation()));
			jsonObject.add("uom", ((logTab.getUom() == null) ? "" : logTab.getUom()));
			jsonObject.add("volume", logTab.getVolume());
			jsonObject.add("weight", logTab.getWeight());
			jsonObject.add("selected", false);
			jsonObject.add("legNbr", ((logTab.getLegNumber() == null) ? "" : logTab.getLegNumber()));
			jsonObject.add("productCode", (logTab.getProductCode() == null)  ? "" : logTab.getProductCode());
			jsonObject.add("parent", logTab.getParent());
			jsonObject.add("child", logTab.getChild());
			jsonObject.add("vpo", ((logTab.getVpo() == null) ? "" : logTab.getVpo()));
			jsonObject.add("comment", ((logTab.getComment() == null) ? "" : logTab.getComment()));
			
			jsonArray.add(jsonObject.build());
		}
		StringWriter stringWriter = new StringWriter();
		JsonWriter writer = Json.createWriter(stringWriter);
		writer.writeArray(jsonArray.build());
		writer.close();
		String jsonArrayString = stringWriter.getBuffer().toString();

		return jsonArrayString;
	}
	
	private Date formatToLocalTime(Date date, String timeZoneId) {
		String utc = "";
		DateFormat pstFormat = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
		pstFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		utc = pstFormat.format(date);
		int mins = TimeZone.getTimeZone(timeZoneId).getOffset(new Date().getTime());
		Date localDate = null;
		try {
			SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
			localDate = parser.parse(utc);
			Calendar cal = Calendar.getInstance();
			cal.setTime(localDate);
			cal.add(Calendar.MILLISECOND, mins);
			localDate = cal.getTime();
		} catch (Exception ee) {
		}
		return localDate;

	}
	
	private Date formatRealTimeYYYMMddhhmmss(Date dt, String hhmmssValue) {
		Date retDt = new Date();
		String dt2 = DateSimpleFormat.formatSimpleDate(dt, "yyyy-MM-dd");

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			retDt = df.parse(dt2 + " " + hhmmssValue);
		} catch (ParseException ex) {
		}
		return retDt;
	}
	
	private String trimString(String s) {
		if (s == null) {
			return "";
		} else {
			return s.replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
		}
	}
	private List<BCModel> fetchBCHashValue(String proBillNbr) {

		String sql = "select activity_id,probill_nbr,status_cd,bc_hash_value from data_staging where probill_nbr='"+proBillNbr + "'" + " and bc_hash_value is not null";
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			List<BCModel> aList = new LinkedList<>();
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					BCModel bc = new BCModel();
					bc.setActivityId(rs.getLong(1));
					bc.setProBillNbr(rs.getString(2));
					bc.setStatus(rs.getString(3));
					bc.setBcHashValue(rs.getString(4));
					aList.add(bc);
				}
				
				return aList;
			}
		} catch (Exception e) {
			log.error("error", e);
		}
		
		return null;
	}
}
