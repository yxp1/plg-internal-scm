package com.openport.marketplace.view;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.AnalyticsMenu;
import com.openport.marketplace.json.AnalyticsTracking;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.marketplace.repository.DbConfigTMS;
import com.openport.util.db.ResultSetMapper;

@Path("analytics") // http://localhost:8080/market-api/analytics
public class AnalyticsResource {

	static final transient Logger log = Logger.getLogger(AnalyticsResource.class);
		
	@GET
	@Path("menu/{partner}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<AnalyticsMenu> getMenu(@PathParam("partner") String partner) {
		try {
			String sql = " select partner_cd,menu_label_txt,report_id,report_type_cd from analytics_menu\n"
					+ " where partner_cd=? order by menu_label_txt";
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, partner);
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<AnalyticsMenu> driverRawMapper = new ResultSetMapper<AnalyticsMenu>();
					List<AnalyticsMenu> pojoList = driverRawMapper.mapResultSetToObject(rs, AnalyticsMenu.class);
					// List<Driver> nw_pojoList = new LinkedList<Driver>();
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList;
					}
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;

	}
	
	
	@GET // http://localhost:8080/market-api/analytics/ga/tracking
	@Path("ga/tracking")
	@Produces({ MediaType.APPLICATION_JSON })
	public AnalyticsTracking getTrackingId(@QueryParam("host") String host) {

		try {
			String sql = "select * from ga_tracking ga where ga.host = ?";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, host);

				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<AnalyticsTracking> driverRawMapper = new ResultSetMapper<AnalyticsTracking>();
					List<AnalyticsTracking> pojoList = driverRawMapper.mapResultSetToObject(rs, AnalyticsTracking.class);
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList.get(0);
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}

		return null;
	}

}
