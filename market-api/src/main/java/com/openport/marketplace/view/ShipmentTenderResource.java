/**
 * 
 */
package com.openport.marketplace.view;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.openport.geocoding.ConvertAddressToLatLong;
import com.openport.geocoding.model.GeoLocation;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.CompanyClient;
import com.openport.marketplace.json.Country;
import com.openport.marketplace.json.Currency;
import com.openport.marketplace.json.Dates;
import com.openport.marketplace.json.EntityAddress;
import com.openport.marketplace.json.EquipmentType;
import com.openport.marketplace.json.FileDocumentType;
import com.openport.marketplace.json.PaymentTerm;
import com.openport.marketplace.json.Service;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.Shipment;
import com.openport.marketplace.json.ShipmentAccessorial;
import com.openport.marketplace.json.ShipmentContact;
import com.openport.marketplace.json.ShipmentDetail;
import com.openport.marketplace.json.ShipmentReturn;
import com.openport.marketplace.json.ShipmentService;
import com.openport.marketplace.json.UnitOfMeasure;
import com.openport.marketplace.local.DataHolderLocal;
import com.openport.marketplace.migration.TmsMigration;
import com.openport.marketplace.model.Accessorial;
import com.openport.marketplace.model.Partner;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.marketplace.repository.DbConfigTmsUtf8;
import com.openport.marketplace.repository.PartnerRepository;
import com.openport.marketplace.repository.TenderShipmentRepository;
import com.openport.util.db.ResultSetMapper;

/**
 * @author Relly
 *
 */
@Path("shipmentTenders") // http://localhost:8080/market-api/shipments/tenders
public class ShipmentTenderResource {
	static final transient Logger log = Logger.getLogger(ShipmentTenderResource.class);
	private String UPDATE_ID="";
	
	
	private Integer bidMessageQLoader(String userId,String carrierCd,String owner,String referenceNbr,String poNbr,String releaseNbr) {

		String spName = "sp_marketplace_BID_messageQ_loader";

		String sql = "call " + spName + " (?,?,?,?,?,?)";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.setString(1, userId);
			pstmt.setString(2, carrierCd);
			pstmt.setString(3,  owner);
			pstmt.setString(4, referenceNbr);
			pstmt.setString(5,poNbr);
			pstmt.setString(6, releaseNbr);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}
	
	
	
	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}		
	
		
	@POST // http://localhost:8080/market-api/shipmentTenders/carrierrateshop
	@Path("bidworkqueue/{shipmentId}") 
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ShipmentReturn bidShipment(@PathParam("shipmentId") Integer shipmentId) {
		String userId=null;
		String owner=null;
		String reference="";
		Integer destId=0;
		ShipmentReturn re = new ShipmentReturn();
		String sql = "select a.create_user_id,a.shipment_nbr,b.company_code,a.delivery_address_id from shipment a inner join company b on a.shipper_id=b.company_id where shipment_id="+shipmentId;
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						 userId=rs.getString(1);
						 reference=rs.getString(2);
						 owner=rs.getString(3);
						 destId=rs.getInt(4);
					}
					String countryCd=getCountryCd(destId);
					List<String> carriers = getCarriers(countryCd);
					for(String c : carriers) {
						bidMessageQLoader(userId, c, owner, reference, reference, reference);
					}
					
				
			}
				re.setStatus(Response.Status.ACCEPTED.getStatusCode());
				return re;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		re.setStatus(Response.Status.CONFLICT.getStatusCode());
		return re;
	}		
		
	@POST // http://localhost:8080/market-api/shipmentTenders/carrierrateshop
	@Path("carrierrateshop") 
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ShipmentReturn getContractRates(Shipment shipment) {
		ShipmentReturn re = new ShipmentReturn();
		try {
			re.setShipmentId(shipment.getShipmentId());
			/*
			re.setStatus(Response.Status.CONFLICT.getStatusCode());`
			if (1 == 1){
				return re;
			}
			*/
			String mode="N";
			String sql = "call sp_carrier_rate_shop( "+shipment.getShipmentId()+",'"+ mode+"')";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					pstmt.executeUpdate();
					re.setStatus(Response.Status.ACCEPTED.getStatusCode());
					return re;
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				re.setStatus(Response.Status.CONFLICT.getStatusCode());
				return re;
				// new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		
		
	}
	
	@POST
	@Path("/update")
	@Consumes("application/json")
	public boolean updateShipmentByReleaseNumber(Shipment shipment, 
			@QueryParam("sf") boolean isShipFromChanged, 
			@QueryParam("st") boolean isShipToChanged, 
			@QueryParam("e") boolean isEquipmentChanged, 
			@QueryParam("sd") boolean isShipmentDetialChanged,
			@QueryParam("pd") boolean isPickupDateChanged,
			@QueryParam("dd") boolean isDeliveryDateChanged,
			@QueryParam("code") String accountCode) {
		
		TenderShipmentRepository shipmentRepository = new TenderShipmentRepository();
	    return shipmentRepository.updateShipmentByReleaseNumber(shipment, isShipFromChanged, isShipToChanged, isEquipmentChanged, isShipmentDetialChanged, isPickupDateChanged, isDeliveryDateChanged, accountCode);
	}
	
	@GET
    @Path("/shipment/exists")
    public List<ShipmentDetail> isShipmentExists(@QueryParam("orderNumber") String orderNumber, 
            @QueryParam("releaseNumber") String releaseNumber,
            @QueryParam("code") String accountCode) {
        
        try {
            TenderShipmentRepository shipmentRepository = new TenderShipmentRepository();
            
            return shipmentRepository.isShipmentExists(orderNumber, releaseNumber, accountCode);
        } catch (SQLTimeoutException e) {
            log.error(e);
        } catch (SQLFeatureNotSupportedException e) {
            log.error(e);
        } catch (SQLException e) {
            log.error(e);
        } catch (Exception e) {
            log.error(e);
        }
        
        return null;
    }
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("rateshop/{shipmentId}/{mode}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer getRateShop(@PathParam("shipmentId") Integer shipmentId,@PathParam("mode") String mode) {
	
		try {

			String sql = "call sp_carrier_rate_shop( "+shipmentId+",'"+ mode+"')";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					return pstmt.executeUpdate();
			} catch (Exception e) {
				
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("accessorial/{parent}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Accessorial> getMpAccessorial(@PathParam("parent") String parent) {
		String sql = "select accessorial_cd,accessorial_type,description,customer_cd,parent_partner_cd "
				+ " from accessorial_code where parent_partner_cd='MP' and customer_cd in ('*','"+parent+"') order by description";
		log.info("SQL"+sql);
		try (Connection connection = com.openport.marketplace.repository.DbConfigRater.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) 
			{
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Accessorial> driverRawMapper = new ResultSetMapper<Accessorial>();
				List<Accessorial> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, Accessorial.class);
				if (pojoListObj!=null && pojoListObj.size()>0) {
					return pojoListObj;
				}	
		     }
		} catch (Exception e) {
			// log.error("Error message: " + e, e);
		}
		return new ArrayList<Accessorial>();
	}
	
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("address/{keyword}/{shipperId}/{module}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<EntityAddress> searchAddress(@PathParam("keyword") String keyword,@PathParam("shipperId") Integer shipperId,@PathParam("module") String module) {
		keyword=keyword.replaceAll("'", "''");
		try {
			String sql="";
			if(module.equalsIgnoreCase("Pickup")) {
			sql = "select distinct b.entity_name,b.line1,b.city_locality,b.province,b.country from shipment a inner join entity_address b on a.pickup_address_id=b.entity_address_id "+
                       " where shipper_id="+shipperId + " and entity_name like '%"+ keyword + "%'";
			}else if(module.equalsIgnoreCase("Delivery")) {
				sql = "select distinct b.entity_name,b.line1,b.city_locality,b.province,b.country from shipment a inner join entity_address b on a.delivery_address_id=b.entity_address_id "+
	                       " where shipper_id="+shipperId + " and entity_name like '%"+ keyword + "%'";
					
				
			}
			 log.info("sql"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<EntityAddress> driverRawMapper = new ResultSetMapper<EntityAddress>();
		        	List<EntityAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, EntityAddress.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	

	
	@GET // http://localhost:8080/market-api/truckers
	@Path("alladdress/{shipperId}/{module}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<EntityAddress> searchAllAddress(@PathParam("shipperId") Integer shipperId,@PathParam("module") String module) {
		
		try {
			String sql="";
			if(module.equalsIgnoreCase("Pickup")) {
			sql = "select distinct b.entity_name,b.line1,b.city_locality,b.province,b.country from shipment a inner join entity_address b on a.pickup_address_id=b.entity_address_id "+
                       " where shipper_id="+shipperId;
			}else if(module.equalsIgnoreCase("Delivery")) {
				sql = "select distinct b.entity_name,b.line1,b.city_locality,b.province,b.country from shipment a inner join entity_address b on a.delivery_address_id=b.entity_address_id "+
	                       " where shipper_id="+shipperId;
					
				
			}
			 log.info("sql"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<EntityAddress> driverRawMapper = new ResultSetMapper<EntityAddress>();
		        	List<EntityAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, EntityAddress.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}

	
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("compCountry/{companyAddressId}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public Country fetchCompanyCountry(@PathParam("companyAddressId") Integer companyAddressId) {
		try {
			String sql="";
			sql = "select country_2_cd from company_address a inner join country b on a.country_id=b.country_id and a.company_address_id="+companyAddressId;
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Country> driverRawMapper = new ResultSetMapper<Country>();
		        	List<Country> pojoList = driverRawMapper.mapResultSetToObject(rs, Country.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList.get(0);
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	
	
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("country")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Country> getAllCountry() {
		List<Country> countryList = new ArrayList<Country>();
		try {

			String sql = "SELECT country_id,country_name,country_2_cd FROM `country` where country_id > -1;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Country> driverRawMapper = new ResultSetMapper<Country>();
		        	List<Country> pojoList = driverRawMapper.mapResultSetToObject(rs, Country.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			
		}
		return countryList;
	}
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("formatdate")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Dates> formatDates() {
		
		
		
		List<Dates> obj = new ArrayList<>();
		
		try 
		{
			String dt1 = new com.openport.util.CurrentDateFormatter().formatGivenDate(new Date(),"yyyy-MM-dd 09:00", new Locale("en", "US"));
			String dt2 = new com.openport.util.CurrentDateFormatter().formatGivenDate(new Date(),"yyyy-MM-dd 17:00", new Locale("en", "US"));
			
			DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			
			try {
			  Dates ds1 = new Dates();
			  ds1.setDt(df1.parse(dt1));
			  Dates ds2 = new Dates();
			  ds2.setDt(df1.parse(dt2));
			  obj.add(ds1);
			  obj.add(ds2);
			  return obj;
			}catch(ParseException ex) {}
			
		} catch (Exception e) {
				
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
		}
		
	
		return null;
	}

	
	@GET // http://localhost:8080/market-api/truckers
	@Path("paymentterm")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<PaymentTerm> getPaymentTerms() {
		List<PaymentTerm> termList = new ArrayList<PaymentTerm>();
		try {

			String sql = "SELECT payment_term_id,payment_term_cd FROM `payment_term` where payment_term_id > 0;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<PaymentTerm> driverRawMapper = new ResultSetMapper<PaymentTerm>();
		        	List<PaymentTerm> pojoList = driverRawMapper.mapResultSetToObject(rs, PaymentTerm.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return termList;
	}
	@GET // http://localhost:8080/market-api/truckers
	@Path("currency")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Currency> getCurrencies() {
		List<Currency> currList = new ArrayList<Currency>();
		try {

			String sql = "SELECT currency_cd,country_cd,currency_name FROM `currency`;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Currency> driverRawMapper = new ResultSetMapper<Currency>();
		        	List<Currency> pojoList = driverRawMapper.mapResultSetToObject(rs, Currency.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return currList;
	}
	@GET // http://localhost:8080/market-api/truckers
	@Path("equipmentType")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<EquipmentType> getAllEquipment() {
		List<EquipmentType> equipmentList = new ArrayList<EquipmentType>();
		try {

			String sql = "SELECT equipment_type_id,equipment_type_name,equipment_type_cd FROM `equipment_type` ORDER BY equipment_type_name;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<EquipmentType> driverRawMapper = new ResultSetMapper<EquipmentType>();
		        	List<EquipmentType> pojoList = driverRawMapper.mapResultSetToObject(rs, EquipmentType.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return equipmentList;
	}

	
	@GET // http://localhost:8080/market-api/truckers
	@Path("services")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Service> getAllServices() {
		List<Service> equipmentList = new ArrayList<Service>();
		try {

			String sql = "SELECT service_name,service_id FROM `service` where is_transportation=1 ORDER BY service_name;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Service> driverRawMapper = new ResultSetMapper<Service>();
		        	List<Service> pojoList = driverRawMapper.mapResultSetToObject(rs, Service.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return equipmentList;
	}
	
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("uom")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<UnitOfMeasure> getUom() {
		List<UnitOfMeasure> uomList = new ArrayList<UnitOfMeasure>();
		try {

			String sql = "SELECT uom_cd,uom_description_txt FROM `unit_of_measure` where internal_standard_flg='Y' ORDER BY uom_description_txt;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<UnitOfMeasure> driverRawMapper = new ResultSetMapper<UnitOfMeasure>();
		        	List<UnitOfMeasure> pojoList = driverRawMapper.mapResultSetToObject(rs, UnitOfMeasure.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return uomList;
	
	}
	
	
	private List<ShipmentDetail> getShipmentDetail(Integer shipmentId) {
		String sql = "SELECT shipment_detail_id,shipment_id,qty,unit_of_measure,shipment_wt,wt_uom,shipment_vol,vol_uom,is_hazardous,create_dt,update_dt,"
				+ "stop_po_nbr, stop_release_nbr, product_desc_txt, product_cd, unit_price, goods_value_bill_to_cd from shipment_detail "
				   +" where shipment_id =?";
				
				   
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, shipmentId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentDetail> driverRawMapper = new ResultSetMapper<ShipmentDetail>();
	        	List<ShipmentDetail> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentDetail.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	

	private List<ShipmentService> getShipmentService(Integer shipmentId) {
		String sql = "SELECT shipment_service_id,shipment_id,b.service_id,service_name from shipment_service a inner join service b on a.service_id=b.service_id"
				   +" where shipment_id =?";
				
				   
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, shipmentId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentService> driverRawMapper = new ResultSetMapper<ShipmentService>();
	        	List<ShipmentService> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentService.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	

	private List<ShipmentContact> getShipmentContact(Integer shipmentId) {
		
		String sql = "select contact_type_cd,contact_name,contact_phone_nbr,contact_email_address from shipment_contact "+
				   " where shipment_id =?";
				
				   
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, shipmentId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentContact> driverRawMapper = new ResultSetMapper<ShipmentContact>();
	        	List<ShipmentContact> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentContact.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	private List<Accessorial> getShipmentAccessorial(Integer shipmentId) {
		
		String sql = "select ACCESSORIAL_CD,ACCESSORIAL_DESCRIPTION,ACCESSORIAL_TYPE from shipment_accessorial where shipment_id="+shipmentId;
				   		   
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentAccessorial> driverRawMapper = new ResultSetMapper<ShipmentAccessorial>();
	        	List<ShipmentAccessorial> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentAccessorial.class);
	        	List<Accessorial> accList = new LinkedList<>();
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		for(ShipmentAccessorial sac : pojoList) {
	        			Accessorial acc = new Accessorial();
	        			acc.setAccessorialCd(sac.getAccessorialCd());
	        			acc.setAccessorialType(sac.getAccessorialType());
	        			acc.setDescription(sac.getAccessorialDescription());
	        			acc.setStatus(Boolean.TRUE);
	        			accList.add(acc);
	        		}
	        		return accList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	
	private EntityAddress getAddress(Integer addressId) {
		String sql = "SELECT entity_address_id,entity_name,line1,city_locality,province,country,postal_code from entity_address "
				   +" where entity_address_id =?";
				
				   
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, addressId);
		//	pstmt.setString(2, address.getCityLocality());
	//		pstmt.setString(3, address.getProvince());
	//		pstmt.setString(4, address.getCountry());
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EntityAddress> driverRawMapper = new ResultSetMapper<EntityAddress>();
	        	List<EntityAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, EntityAddress.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	
	
	private List<String> getCarriers(String countryCd) {
		String sql = "select a.company_code from company a inner join company_address b on a.corporate_address_id=b.company_address_id\n"+
" inner join country c on c.country_id=b.Country_Id \n"+
" where c.country_2_cd='"+countryCd + "' and a.Is_Carrier = 1";
			List<String> carrierList = new LinkedList<>();	   
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						carrierList.add(rs.getString(1));	
					}
					return carrierList;
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	private String getCountryCd(Integer deliveryAddressId) {
		String sql = "select country from entity_address  where entity_address_id="+deliveryAddressId;
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						return rs.getString(1);	
					}
				
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentsearch/{shipmentNbr}/{shipperId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public Shipment getShipmentBySearchCriterias(@PathParam("shipmentNbr") String shipmentNbr, @PathParam("shipperId") Integer shipperId) {
	

		String sql = "SELECT shipment_id,shipment_nbr,shipper_id,pickup_address_id,delivery_address_id,requested_equipment_type_id,requested_pickup_dt,requested_delivery_dt,update_dt,payment_term_id,low_end_bid_amt,high_end_bid_amt,target_rate_amt,currency_cd,status_id,origin_wait_tm,origin_wait_tm_uom,dest_wait_tm,dest_wait_tm_uom,commodity_desc_txt,pro_bill_nbr from shipment "
				+ "where shipment_nbr=? and shipper_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, shipmentNbr);
			pstmt.setInt(2, shipperId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		
	        		Shipment shipment= pojoList.get(0);
	        		shipment.setBidRank(0);
	        		shipment.setRateCount(0);
	        		
	        		if(shipment !=null) {
	        		shipment.setPickupAddress(getAddress(shipment.getPickupAddressId()));
	        		shipment.setDeliveryAddress(getAddress(shipment.getDeliveryAddressId()));
	        		shipment.setShipmentDetails(getShipmentDetail(shipment.getShipmentId()));
	        		shipment.setShipmentServices(getShipmentService(shipment.getShipmentId()));
	        		shipment.setShipmentContacts(getShipmentContact(shipment.getShipmentId()));
	        		shipment.setAccessorials(getShipmentAccessorial(shipment.getShipmentId()));
	        		}
	        		return shipment;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		}
	
	@POST // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("validateshipment") 
	@Consumes("application/json") 

	public Shipment validateshipment(Shipment shpt) {

		// TODO get the shipment details in po_header
	    String sql = "SELECT s.shipment_id, s.shipment_nbr, s.shipper_id, s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
	    		+ "s.requested_pickup_dt, s.requested_delivery_dt, s.update_dt, s.payment_term_id, s.low_end_bid_amt, s.high_end_bid_amt, s.target_rate_amt, "
	    		+ "s.currency_cd, s.status_id, s.origin_wait_tm, s.origin_wait_tm_uom, s.dest_wait_tm, s.dest_wait_tm_uom, s.commodity_desc_txt, s.pro_bill_nbr, "
	    		+ "c.SENT_204_FLG, c.BR_FLG, a.LAST_EVENT AS status, a.release_nbr, a.po_nbr "
	    		+ "FROM shipment s "
	    		+ "LEFT JOIN tms_utf8_db.po_header a ON s.shipment_nbr = a.release_nbr "
	    		+ "LEFT JOIN tms_utf8_db.po_release_carrier c ON a.PO_NBR = c.PO_NBR "
	    		+ "AND a.RELEASE_NBR = c.RELEASE_NBR "
	    		+ "AND a.PARTNER_CD = c.PARTNER_CD "
	    		+ "AND a.PARENT_PARTNER_CD = c.PARENT_PARTNER_CD "
	    		+ "AND a.CUSTOMER_CD = c.CUSTOMER_CD "
	    		+ "AND a.SHIPPER_CD = c.SHIPPER_CD "
	    		+ "AND a.EQUIPMENT_SHIPMENT_SEQ_NBR = c.EQUIPMENT_SHIPMENT_SEQ_NBR "
	    		+ "AND c.CHARGE_TYPE_CD = 'C' "
	    		+ "WHERE s.shipment_nbr = ? AND s.shipper_id = ?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, shpt.getShipmentNbr());
			pstmt.setInt(2, shpt.getShipperId());
			
			try (ResultSet rs = pstmt.executeQuery()) {
	        	if(rs.next()) {
	        		
	        		Shipment shipment = new Shipment();
	        		shipment.setBidRank(0);
	        		shipment.setRateCount(0);
	        		shipment.setShipmentId(rs.getInt("shipment_id"));
	        		shipment.setShipmentNbr(rs.getString("shipment_nbr"));
	        		shipment.setShipperId(rs.getInt("shipper_id"));
	        		shipment.setPickupAddressId(rs.getInt("pickup_address_id"));
	        		shipment.setDeliveryAddressId(rs.getInt("delivery_address_id"));
	        		shipment.setRequestedEquipmentTypeId(rs.getInt("requested_equipment_type_id"));
	        		shipment.setRequestedPickupDt(rs.getTimestamp("requested_pickup_dt"));
	        		shipment.setRequestedDeliveryDt(rs.getTimestamp("requested_delivery_dt"));
	        		shipment.setUpdateDt(rs.getTimestamp("update_dt"));
	        		shipment.setPaymentTermId(rs.getInt("payment_term_id"));
	        		shipment.setLowEndBidAmt(rs.getDouble("low_end_bid_amt"));
	        		shipment.setHighEndBidAmt(rs.getDouble("high_end_bid_amt"));
	        		shipment.setTargetRateAmt(rs.getDouble("target_rate_amt"));
	        		shipment.setCurrencyCd(rs.getString("currency_cd"));
	        		shipment.setStatusId(rs.getInt("status_id"));
	        		shipment.setOriginWaitTm(rs.getDouble("origin_wait_tm"));
	        		shipment.setOriginWaitTmUom(rs.getString("origin_wait_tm_uom"));
	        		shipment.setDestWaitTm(rs.getDouble("dest_wait_tm"));
	        		shipment.setDestWaitTmUom(rs.getString("dest_wait_tm_uom"));
	        		shipment.setCommodityDescTxt(rs.getString("commodity_desc_txt"));
	        		shipment.setProBillNbr(rs.getString("pro_bill_nbr"));
	        		shipment.setSent204Flag(rs.getBoolean("SENT_204_FLG"));
                    shipment.setBrFlag(rs.getBoolean("BR_FLG"));
                    shipment.setStatusDescription(rs.getString("status"));
                    shipment.setPurchaseOrderNumber(rs.getString("PO_NBR"));
                    shipment.setReleaseNumber(rs.getString("RELEASE_NBR"));
	        		
	        		if(shipment !=null) {
		        		shipment.setPickupAddress(getAddress(shipment.getPickupAddressId()));
		        		shipment.setDeliveryAddress(getAddress(shipment.getDeliveryAddressId()));
		        		shipment.setShipmentDetails(getShipmentDetail(shipment.getShipmentId()));
		        		shipment.setShipmentServices(getShipmentService(shipment.getShipmentId()));
		        		shipment.setShipmentContacts(getShipmentContact(shipment.getShipmentId()));
		        		shipment.setAccessorials(getShipmentAccessorial(shipment.getShipmentId()));
	        		}
	        		return shipment;
				} else {
				    TenderShipmentRepository tenderShipment = new TenderShipmentRepository();
				    return tenderShipment.getShipmentByReleaseNumber(shpt.getShipmentNbr(), shpt.getAccountCode());
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	
	
	private Date convertToDate(String s) {;
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	Date date=null;
	try {
		date = df.parse(s);
		
	}catch(ParseException ex) {
		
	}

	return date;
	
	
}

	
	
	@POST // http://localhost:8080/market-api/shipmentTenders/newshipment
	@Path("createOrder2") 
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ShipmentReturn createOrder2(Shipment shipment) {
		Integer deleteShipmentId = shipment.getShipmentId();
		//new ServiceException(Response.Status.CONFLICT);
		List<ShipmentContact> scList2 = shipment.getShipmentContacts();
		ShipmentContact pickupContact = new ShipmentContact();
		ShipmentContact deliveryContact = new ShipmentContact();
	
		
		//System.out.println("shipment.getPickupDt():"+shipment.getPickupDt());
		if((shipment.getPickupDt()!=null) && (shipment.getPickupDt().trim().length()==1)){
			shipment = doSetShipmentRequestedPickupDateByDayOfWeek(shipment);
		}
		
		
		ShipmentReturn re = new ShipmentReturn();
		re.setShipmentId(shipment.getShipmentId());
		re.setStatus(Response.Status.ACCEPTED.getStatusCode());
		
		return re;
	
	}
	


	@POST // http://localhost:8080/market-api/shipmentTenders/newshipment
	@Path("createOrder") 
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ShipmentReturn createOrder(Shipment shipment) {
		Integer deleteShipmentId = shipment.getShipmentId();
		String deleteShipmentNumber = shipment.getShipmentNbr();
		ShipmentContact pickupContact = new ShipmentContact();
		ShipmentContact deliveryContact = new ShipmentContact();
		String statusCd="CO";
		String statusDesc="Create Order";
		String goodsValuePartnerName = null;
		
		// shipment.setRequestedDeliveryDt(new Timestamp(convertToDate(shipment.getDeliveryDt()).getTime()));
		// shipment.setRequestedPickupDt(new Timestamp(convertToDate(shipment.getPickupDt()).getTime()));
		
		/*
		if(deleteShipmentId != null && deleteShipmentId > 0 && !validateShipmentUpdateDt(shipment.getUpdateDt())) {
			log.info("skip update");
			return null;
		}
		*/
		
		if((shipment.getPickupDt()!=null) && (shipment.getPickupDt().trim().length()==1)){
			shipment = doSetShipmentRequestedPickupDateByDayOfWeek(shipment);
		}

		
		if(deleteShipmentId != null) {
			deleteShipmentDetail(deleteShipmentId);
			deleteShipmentService(deleteShipmentId);
			deleteShipmentContact(deleteShipmentId);
			deleteShipmentAccessorial(deleteShipmentId);
			deleteShipment(deleteShipmentId);
			statusCd="EO";
			statusDesc="Edit Order";
		}
		
		
		//validate if shipment is locked by user
		shipment.setShipmentId(null);
		if(shipment.getRequestedEquipmentTypeId() == null) {
			shipment.setRequestedEquipmentTypeId(-2);
		}
		
		try {
			//process address
			Integer pickupId = validateAddress(shipment.getPickupAddress()); 
			if(pickupId!=null) {
				shipment.setPickupAddressId(pickupId);
			} else {
				//create EntityAddress
				shipment.setPickupAddressId((insertAddress(shipment.getPickupAddress()).getEntityAddressId()));
			}
			
			boolean destAddressByServiceType=false;
			
			if((shipment.getVendorId()!=null) && (shipment.getVendorId().equalsIgnoreCase("HKSSID"))) {
				destAddressByServiceType=true;
				String serviceNm = "";
				Integer addressId = 0;
				
				if((shipment.getServices()!=null) && (shipment.getServices().get(0).getServiceName()!=null)) {
					serviceNm=shipment.getServices().get(0).getServiceName();
				}
				if(serviceNm.equalsIgnoreCase("DISPOSAL")){
					addressId = 510; // SSID e-Waste Disposal Depot UAT 510
				} else if (serviceNm.equalsIgnoreCase("RECYCLE")) {
					addressId = 510; // NOT SET YET
				} else {
					addressId = 510; 
				}
				
				shipment.setDeliveryAddressId(addressId);
				shipment.setDeliveryAddress(getAddress(addressId));
			}
			
			if (!destAddressByServiceType) {
				Integer deliveryId = validateAddress(shipment.getDeliveryAddress());
				
				if(deliveryId!=null) {
					shipment.setDeliveryAddressId(deliveryId);
				} else {
					//create EntityAddress
					shipment.setDeliveryAddressId( (insertAddress(shipment.getDeliveryAddress()).getEntityAddressId()));
				}
			}
			
			if(shipment.getAssignedProviderId() == null) {
				shipment.setAssignedProviderId(-1);
			}
			
			double totalUnit=0.0;
			double totalUnitWt = 0.0;
			double totalUnitVol = 0.0;
			
			Map<String, String> uomMap = new HashMap<String, String>();
			if(shipment.getShipmentDetails()!=null && shipment.getShipmentDetails().size() > 0) {
				for(ShipmentDetail detail : shipment.getShipmentDetails()) {
					if(detail.getQty() != null) {
						totalUnit+=detail.getQty();
					}
					
					if(detail.getShipmentVol() != null) {
						totalUnitVol+=detail.getShipmentVol();
					}
					
					if(detail.getShipmentWt() != null) {
						totalUnitWt+=detail.getShipmentWt();
					}
					
					uomMap.put(detail.getUnitOfMeasure(),detail.getUnitOfMeasure());
					
					if(goodsValuePartnerName == null) {
						goodsValuePartnerName = detail.getGoodsValueBillToCode();
					}
				}
			}
			
			if(uomMap.size() > 1) {
				shipment.setUnitOfMeasure("MXP");
			} else if(uomMap.size()==1) {
				shipment.setUnitOfMeasure(uomMap.keySet().iterator().next());
			}			
			
			//	shipment.setRequestedEquipmentTypeId(shipment.getEquipmentTypeId());
			
			shipment.setUnitCount(totalUnit);
			shipment.setUnitVol(totalUnitVol);
			shipment.setUnitWt(totalUnitWt);
			
			if(shipment.getProBillNbr() == null || shipment.getProBillNbr().trim().isEmpty()) {
				shipment.setProBillNbr("001-"+getSequenceNbr()); //001 as indicator for Marketplace pro bill
			}
			
			shipment = insertNewShipment(shipment);
			
			List<ShipmentDetail> details = new LinkedList<>();
			
			if(shipment.getShipmentDetails() != null && shipment.getShipmentDetails().size()>0) {
				for(ShipmentDetail detail : shipment.getShipmentDetails()) {
					if((detail.getProductDescTxt() == null)){
						detail.setProductDescTxt(shipment.getCommodityDescTxt());
					}
					detail.setShipmentId(shipment.getShipmentId());
					details.add(detail);
					insertNewShipmentDetail(detail);
				}
			}
			if(shipment.getRequestedEquipmentTypeId()!=null && shipment.getRequestedEquipmentTypeId() > -1) {
			insertEquipment(shipment);
			}
			
			List<ShipmentContact> scList = shipment.getShipmentContacts();
			if(scList!=null && scList.size()>0) {
			for(ShipmentContact sc : scList) {
				if(sc.getContactTypeCd() != null && (sc.getContactTypeCd() == 'P' ||sc.getContactTypeCd() == 'D')) {
					if(sc.getContactTypeCd() == 'P') {
						pickupContact=sc;
					}else if(sc.getContactTypeCd() == 'D') {
						deliveryContact=sc;
					}
				insertShipmentContact(sc,shipment.getShipmentId());
				}
			}
			}
			
			
			if(shipment.getServices()!=null && shipment.getServices().size()>0) {
				for(Service service : shipment.getServices()) {
					service.setShipmentId(shipment.getShipmentId());
					log.info("insert shipment service:"+service.getServiceId()+" shipmentID:"+service.getShipmentId());
					insertServices(service);
				}
			}
			if(shipment.getAccessorials() != null && shipment.getAccessorials().size()>0) {
				for(Accessorial acc : shipment.getAccessorials()) {
					log.info("Accessorial "+acc.getAccessorialCd());
							
					acc.setShipmentId(shipment.getShipmentId());
					if(acc.getAccessorialCd()==null) {
						acc.setAccessorialCd("M"+getSequenceNbr());
						createAccessorial(acc,shipment.getCompanyCd());
					}
					insertShipmentAccessorial(acc);
				}
			}
			/*
			if(deleteShipmentId != null) {
				deleteShipmentDetail(deleteShipmentId);
				deleteShipmentService(deleteShipmentId);
				deleteShipmentContact(deleteShipmentId);
				deleteShipmentAccessorial(deleteShipmentId);
				deleteShipment(deleteShipmentId);
				statusCd="EO";
				statusDesc="Edit Order";
				
			}
			*/
			//

			log.info("shipment pro bill: " + shipment.getProBillNbr());
			log.info("shipment num: " + deleteShipmentNumber);
			
			TmsMigration.migrateMPToTMS(deleteShipmentNumber);

			PartnerRepository partnerRepository = new PartnerRepository();
			Partner partnerBillToParty = partnerRepository.getBillToParty(shipment.getCompanyCd());
			log.info("partnerBillToParty: " + partnerBillToParty.toString());
			
			
			Partner goodsValuePartner = new Partner();
			if(goodsValuePartnerName != null) {
				goodsValuePartner = partnerRepository.getBillToParty(goodsValuePartnerName);
				log.info("goodsValuePartner: " + goodsValuePartner.toString());
			}
			
			Partner parentPartner = DataHolderLocal.get(Partner.class);
			log.info("parentPartner: " + parentPartner.toString());
			
			Company company = DataHolderLocal.get(Company.class);
			String currency = StringUtils.defaultString(company.getCurrencyCd(), "");
			
			TmsMigration.doMapping_MP_Shipment_PoHeader(shipment, parentPartner);
			
			//goods value bill to party
			TmsMigration.doMapping_MP_Shipment_PoReference(shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), "GVBillToParty", goodsValuePartner.getPartnerName(), goodsValuePartner.getLineAddress1() + "," + goodsValuePartner.getLineAddress2(), "", goodsValuePartner.getCity(), goodsValuePartner.getState(), goodsValuePartner.getCountry(), goodsValuePartner.getPostal(), goodsValuePartner.getContactPerson(), "", goodsValuePartner.getEmail(), shipment.getCreateUserId(), 6, goodsValuePartner.getTelephone(), goodsValuePartner.getLocationCode(), goodsValuePartnerName, parentPartner.getParentPartnerCode());
			//bill to party
			TmsMigration.doMapping_MP_Shipment_PoReference(shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), "BillToParty", partnerBillToParty.getPartnerName(), partnerBillToParty.getLineAddress1() + "," + partnerBillToParty.getLineAddress2(), "", partnerBillToParty.getCity(), partnerBillToParty.getState(), partnerBillToParty.getCountry(), partnerBillToParty.getPostal(), partnerBillToParty.getContactPerson(), "", partnerBillToParty.getEmail(), shipment.getCreateUserId(), 5, partnerBillToParty.getTelephone(), partnerBillToParty.getLocationCode(), shipment.getAccountCode(), parentPartner.getParentPartnerCode());
			//shipper
			TmsMigration.doMapping_MP_Shipment_PoReference(shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), "Shipper", shipment.getCorpAddress().getCompanyName(),shipment.getCorpAddress().getAddressLine1(), shipment.getCorpAddress().getAddressLine2(), shipment.getCorpAddress().getCityName(),shipment.getCorpAddress().getProvinceName(), shipment.getCorpAddress().getCountry2Cd(),shipment.getCorpAddress().getPostalCd(),shipment.getCorpAddress().getContactPerson(),shipment.getCorpAddress().getTelephone(),shipment.getCorpAddress().getEmailAddress(),shipment.getCreateUserId(), 4, "", "", shipment.getAccountCode(), parentPartner.getParentPartnerCode());
			//pickup
			TmsMigration.doMapping_MP_Shipment_PoReference(shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), "Pickup", shipment.getPickupAddress().getEntityName(),shipment.getPickupAddress().getLine1(), " ", shipment.getPickupAddress().getCityLocality(),shipment.getPickupAddress().getProvince(),shipment.getPickupAddress().getCountry(), shipment.getPickupAddress().getPostalCode(),pickupContact.getContactName(),pickupContact.getContactPhoneNbr(),pickupContact.getContactEmailAddress(),shipment.getCreateUserId(), 3, "", "", shipment.getAccountCode(), parentPartner.getParentPartnerCode());
			//delivery
			TmsMigration.doMapping_MP_Shipment_PoReference(shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), "Delivery", shipment.getDeliveryAddress().getEntityName(),shipment.getDeliveryAddress().getLine1(), " ", shipment.getDeliveryAddress().getCityLocality(),shipment.getDeliveryAddress().getProvince(), shipment.getDeliveryAddress().getCountry(),shipment.getPickupAddress().getPostalCode(),deliveryContact.getContactName(),deliveryContact.getContactPhoneNbr(),deliveryContact.getContactEmailAddress(),shipment.getCreateUserId(), 2, "", "", null, parentPartner.getParentPartnerCode());
			//consignee
			TmsMigration.doMapping_MP_Shipment_PoReference(shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), "Consignee", shipment.getDeliveryAddress().getEntityName(),shipment.getDeliveryAddress().getLine1(), " ", shipment.getDeliveryAddress().getCityLocality(),shipment.getDeliveryAddress().getProvince(), shipment.getDeliveryAddress().getCountry(),shipment.getDeliveryAddress().getPostalCode(),deliveryContact.getContactName(),deliveryContact.getContactPhoneNbr(),deliveryContact.getContactEmailAddress(),shipment.getCreateUserId(), 1, "", "", null, parentPartner.getParentPartnerCode());
			
			for(int i=0;i<details.size();i++) {
				TmsMigration.doMapping_MP_Shipment_PoLineItem(details.get(i),  shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), shipment.getCreateUserId(), shipment.getCommodityDescTxt(), (i+1), parentPartner.getParentPartnerCode(), currency);
			}
			
			if(shipment.getRequestedEquipmentTypeId()!=null && shipment.getRequestedEquipmentTypeId() > -1) {
				TmsMigration.doMapping_MP_Shipment_PoRequirement(""+shipment.getEquipmentTypeCd(), 1, "", shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(),shipment.getCreateUserId(), parentPartner.getParentPartnerCode());
			}
			Date dEvent = new Date();
			if(shipment.getTimezone()!=null && !shipment.getTimezone().equalsIgnoreCase("Select One")) {
				dEvent=formatToLocalTime(dEvent, shipment.getTimezone());
			}
			TmsMigration.createTrack(shipment.getCompanyCd(),shipment.getProBillNbr(), statusCd, statusDesc, dEvent, "Marketplace Quick Order", shipment.getCreateUserId());
			
			ShipmentReturn re = new ShipmentReturn();
			re.setShipmentId(shipment.getShipmentId());
			re.setStatus(Response.Status.ACCEPTED.getStatusCode());
			return re;
	} catch (Exception ee) {
		log.error("Error message: " ,ee);
		new ServiceException(Response.Status.CONFLICT);
	}
		
    		
	return null;
	
	}
	
	public Date formatToLocalTime(Date date, String timeZoneId) {
		// TODO Auto-generated method stub
			String utc="";
			DateFormat pstFormat = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
			pstFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			utc=pstFormat.format(date);
			int mins=TimeZone.getTimeZone(timeZoneId).getOffset(new Date().getTime());
			Date localDate = null;
			try {
				SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
				localDate = parser.parse(utc);
				Calendar cal = Calendar.getInstance();
				cal.setTime(localDate);
				cal.add(Calendar.MILLISECOND, mins); 
				localDate = cal.getTime();
			} catch (Exception ee) {
			}
			return localDate;
		
		
	}

	
    
	@POST // http://localhost:8080/market-api/shipmentTenders/newshipment
	@Path("newshipment") 
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Shipment createShipment(Shipment shipment) {
		
		UPDATE_ID=shipment.getUpdateUserId();
		List<CompanyClient> clientList = new LinkedList<>();
		List<FileDocumentType> fileDocTypeList = new LinkedList<>();
		List<ShipmentDetail> shipmentDetails = shipment.getShipmentDetails();
		List<ShipmentService> shipmentServices = shipment.getShipmentServices();
		
		try {
			if (shipment.getShipmentId() == null || shipment.getShipmentId() == -1) {
				shipment = insertNewShipment(shipment);
			} else {
				updateShipment(shipment);
			}

		} catch (Exception ee) {
			log.error("Error message: " ,ee);
			new ServiceException(Response.Status.CONFLICT);
		}

		if ((shipment.getShipmentDetails() != null) && (shipment.getShipmentDetails().size() > 0)) {
			for (ShipmentDetail shipmentDetail : shipment.getShipmentDetails()) {
				insertNewShipmentDetail(shipmentDetail);
			}
		}
		
		if ((shipment.getShipmentServices() != null) && (shipment.getShipmentServices().size() > 0)) {
			for (ShipmentService shipmentService : shipment.getShipmentServices()) {
				insertNewShipmentService(shipmentService);
			}
		}
		
		
		return shipment;
	}

	
	
	private Integer validateAddress(EntityAddress address) {
		String sql = "SELECT entity_address_id from entity_address "
				   +" where line1 ='"+address.getLine1() + "'";
				if(address.getEntityName() != null) {
					sql+= " and entity_name='"+doReplaceQuote(address.getEntityName()) + "'";
				}
				if(address.getCityLocality()!=null) {
					sql+= " and city_locality='"+doReplaceQuote(address.getCityLocality()) + "'";
				}
				if(address.getProvince()!=null) {
					sql+=" and province='" + address.getProvince() + "'";
				}
				if(address.getCountry()!=null) {
					sql+=" and country='"+address.getCountry() + "'";
				}
				if(address.getPostalCode()!=null) {
					sql+=" and postal_code='"+address.getPostalCode() + "'";
				}
				
		
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
		//	pstmt.setString(1, address.getLine1());
		//	pstmt.setString(2, address.getCityLocality());
	//		pstmt.setString(3, address.getProvince());
	//		pstmt.setString(4, address.getCountry());
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EntityAddress> driverRawMapper = new ResultSetMapper<EntityAddress>();
	        	List<EntityAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, EntityAddress.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0).getEntityAddressId();
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	
	private  EntityAddress validateAddressLatLong(EntityAddress address) {
		String sql = "SELECT entity_address_id from entity_address "
				   +" where line1 ='"+address.getLine1() + "'";
				
				if(address.getCityLocality()!=null) {
					sql+= " and city_locality='"+doReplaceQuote(address.getCityLocality()) + "'";
				}
				if(address.getProvince()!=null) {
					sql+=" and province='" + address.getProvince() + "'";
				}
				if(address.getCountry()!=null) {
					sql+=" and country='"+address.getCountry() + "'";
				}
				
		
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
		//	pstmt.setString(1, address.getLine1());
		//	pstmt.setString(2, address.getCityLocality());
	//		pstmt.setString(3, address.getProvince());
	//		pstmt.setString(4, address.getCountry());
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EntityAddress> driverRawMapper = new ResultSetMapper<EntityAddress>();
	        	List<EntityAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, EntityAddress.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}
	
	private EntityAddress insertAddress(EntityAddress address) {
		
		System.out.println("address.getLat():"+address.getLat()+" address.getLon():"+address.getLon());
		EntityAddress latlong = validateAddressLatLong(address);
		if(latlong!=null && latlong.getLat()!=null) {
			address.setLat(latlong.getLat());
			address.setLon(latlong.getLon());
		}else if(address.getLat()!=null || address.getLon()!=null) {
			// address.setLat(latlong.getLat());
			// address.setLon(latlong.getLon());
		}else {
			//force to get geo fencing
			Object[] rGeo = getGeoLocation(address.getLine1(), address.getCityLocality(),
					address.getProvince(), null, address.getCountry(),
					address.getPostalCode());
			BigDecimal rLat = null;
			BigDecimal rLon = null;

			
			if (rGeo != null && rGeo.length > 0 && rGeo[0] != null) {
				rLon = new BigDecimal(Double.valueOf("" + rGeo[0]));
				rLat = new BigDecimal(Double.valueOf("" + rGeo[1]));
			}
			if(address.getLon()==null){ 
				address.setLon(rLon);
			}
		
			if(address.getLat()==null){
				address.setLat(rLat);
			}

			
			
		}
		String sql = "INSERT INTO entity_address ("
				+ " entity_name, " //1
				+ " floor_apartment, "  //2
  				+ " building, "  //3
  				+ " house_number, " //4
  				+ " line1, " //5
  				+ " street_name, " //6
  				+ " city_locality, " //7
  				+ " province, " //8
  				+ " country, " //9
  				+ " postal_code, " //10
  				+ " district_name, " //11
  				+ " latitude, " //12
  				+ " longitude, " //13
  				+ " lat_long_status_cd)" //14
  				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{
			pstmt.setString(1, doReplaceQuote(address.getEntityName()));
			pstmt.setString(2, address.getFloorApartment());
			pstmt.setString(3, address.getBuilding());
			pstmt.setString(4, address.getHouseNumber());
			pstmt.setString(5, doReplaceQuote(address.getLine1()));
			pstmt.setString(6, doReplaceQuote(address.getStreetName()));
			pstmt.setString(7, doReplaceQuote(address.getCityLocality()));
			pstmt.setString(8, address.getProvince());
			pstmt.setString(9, address.getCountry());
			pstmt.setString(10, address.getPostalCode());
			pstmt.setString(11, doReplaceQuote(address.getDistrictName()));
			pstmt.setBigDecimal(12, address.getLat());
			pstmt.setBigDecimal(13, address.getLon());
			pstmt.setString(14, address.getLatLongStatusCd());

										
			int id = pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				address.setEntityAddressId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return address;
		
	}
	private String doReplaceQuote(String s) {
		// TODO Auto-generated method stub
		if ((s!=null) && (s.length()>0)){
			s=s.replaceAll("'", "''");
		}
		return s;
	}

	private Shipment insertNewShipment(Shipment shipment) { 
		log.info(shipment.toString());
		String sql = "INSERT INTO shipment ("
				+ " shipment_nbr, " //1
				+ " Shipper_id, "  //2
  				+ " Status_id, "  //3
  				+ " assigned_provider_id, " //4
  				+ " pickup_address_id, " //5
  				+ " delivery_address_id, " //6
  				+ " requested_equipment_type_id, " //7
  				+ " requested_pickup_dt, " //8
  				+ " Requested_Delivery_Dt, " //9
  				+ " Unit_Count, " //10
  				+ " unit_of_measure, " //11
  				+ " Create_Dt, " //
  				+ " create_user_id, " //12
  				+ " Update_dt, " //
  				+ " update_user_id," //13
  				+ " tender_expiration_dt,"//14
  				+ "unit_wt,"//15
  				+ "unit_vol,"//16
  				+ "payment_term_id,"//17
  				+ "low_end_bid_amt,"//18
  				+ "high_end_bid_amt,"//19
  				+ "target_rate_amt,"//20
  				+ "currency_cd,"//21
  				+ "origin_wait_tm,"
  				+ "origin_wait_tm_uom,"
  				+ "dest_wait_tm,"
  				+ "dest_wait_tm_uom,"
  				+ "commodity_desc_txt,"
  				+ "pro_bill_nbr,"
  				+ "partner_cd,"
  				+ "parent_partner_cd,"
  				+ "shipper_cd,"
  				+ "customer_cd,"
  				+ "tender_type_cd"
  				+ ") " 
  				+ " values (?,"
  				+ "?,"//1
  				+ "?,"//2
  				+ "?,"//3
  				+ "?,"//4
  				+ "?,"//5
  				+ "?,"//6
  				+ "?,"//7
  				+ "?,"//8
  				+ "?,"//9
  				+ "?,"//10
  				+ "now(),"
  				+ "?,"//11
  				+ "now(),"
  				+ "?,"//12
  				+ "?,"//13
  				+ "?,"//14
  				+ "?,"//15
  				+ "?,"//16
  				+ "?,"//17
  				+ "?,"//18
  				+ "?,"//19
  				+ "?," //20
  				+ "?,"//21
  				+ "?,"//22
  				+ "?,"//23
  				+ "?,"//24
  				+ "?,"//25
  				+ "?,"//25
  				+ "?,"//25
  				+ "?,"//31
  				+ "?,"//32
  				+ "?,"//33
  				+ "?"//34
  				+ ")";
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{
		//	shipment.setRequestedPickupDt(new Timestamp(shipment.getPickupDt()));
		//	shipment.setRequestedDeliveryDt(new Timestamp(shipment.getDeliveryDt()));
			pstmt.setString(1, shipment.getShipmentNbr());
			pstmt.setInt(2, shipment.getShipperId());
			pstmt.setInt(3, shipment.getStatusId());
			pstmt.setInt(4, shipment.getAssignedProviderId());
			pstmt.setInt(5, shipment.getPickupAddressId() );
			pstmt.setInt(6, shipment.getDeliveryAddressId());
			pstmt.setInt(7, shipment.getRequestedEquipmentTypeId());
			pstmt.setTimestamp(8, shipment.getRequestedPickupDt());
			pstmt.setTimestamp(9, shipment.getRequestedDeliveryDt());
			pstmt.setDouble(10, shipment.getUnitCount());
			pstmt.setString(11, shipment.getUnitOfMeasure());
			pstmt.setString(12, shipment.getCreateUserId());
			pstmt.setString(13, shipment.getUpdateUserId());
			pstmt.setTimestamp(14, shipment.getTenderExpirationDt());
			pstmt.setDouble(15, shipment.getUnitWt());
			pstmt.setDouble(16, shipment.getUnitVol());
			if(shipment.getPaymentTermId()==null) {
				shipment.setPaymentTermId(-1);
			}
			pstmt.setDouble(17, shipment.getPaymentTermId());
			
			if(shipment.getLowEndBidAmt()==null) {
			  shipment.setLowEndBidAmt(0.0); 
			}
			pstmt.setDouble(18, shipment.getLowEndBidAmt());
			
			if(shipment.getHighEndBidAmt()==null) {
				  shipment.setHighEndBidAmt(0.0); 
			}
			pstmt.setDouble(19, shipment.getHighEndBidAmt());
			
			if(shipment.getTargetRateAmt()==null) {
				  shipment.setTargetRateAmt(0.0); 
			}
			pstmt.setDouble(20, shipment.getTargetRateAmt());
			pstmt.setString(21, shipment.getCurrencyCd());
			if(shipment.getOriginWaitTm()==null) {
				shipment.setOriginWaitTm(0.0);
			}
			pstmt.setDouble(22, shipment.getOriginWaitTm());
			pstmt.setString(23, shipment.getOriginWaitTmUom());
			if(shipment.getDestWaitTm()==null) {
				shipment.setDestWaitTm(0.0);
			}
			pstmt.setDouble(24, shipment.getDestWaitTm());
			pstmt.setString(25, shipment.getDestWaitTmUom());
			pstmt.setString(26, shipment.getCommodityDescTxt());
			pstmt.setString(27, shipment.getProBillNbr());
			pstmt.setString(28, shipment.getCompanyCd());
			if((shipment.getVendorId()!=null) && (shipment.getVendorId().length()>0)){
				pstmt.setString(29, shipment.getVendorId());
			}else{
				pstmt.setString(29, shipment.getCompanyCd());
			}
			pstmt.setString(30, shipment.getCompanyCd());
			pstmt.setString(31, shipment.getCompanyCd());
			pstmt.setString(32, shipment.getTenderTypeCd());
			int id = pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				shipment.setShipmentId(rs.getInt(1));
				/*
				String countryCd=getCountryCd(shipment.getDeliveryAddressId());
				List<String> carriers = getCarriers(countryCd);
				for(String c : carriers) {
					bidMessageQLoader(shipment.getCreateUserId(), c, shipment.getCompanyCd(), shipment.getShipmentNbr(), shipment.getShipmentNbr(), shipment.getShipmentNbr());
				}
				*/
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return shipment;
	}
	
	private Integer insertEquipment(Shipment shipment) { 
		String sql = "INSERT INTO shipment_equipment ("
				+ " shipment_id, " //1
				+ " equipment_type_id) "  //2
  				+ " values (?,?)";
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{	
			pstmt.setInt(1, shipment.getShipmentId());
			pstmt.setInt(2, shipment.getRequestedEquipmentTypeId());
			
			int id = pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				 return rs.getInt(1);
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private Integer insertShipmentContact(ShipmentContact sc,Integer shipmentId) { 
		String sql = "INSERT INTO shipment_contact ("
				+ " shipment_id,contact_type_cd, contact_name,contact_phone_nbr,contact_email_address) "  //2
  				+ " values (?,?,?,?,?)";
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{	
			pstmt.setInt(1, shipmentId);
			
			pstmt.setString(2, String.valueOf(sc.getContactTypeCd()));
			pstmt.setString(3,sc.getContactName());
			pstmt.setString(4,sc.getContactPhoneNbr());
			pstmt.setString(5,sc.getContactEmailAddress());
			
			
			int id = pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				 return rs.getInt(1);
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

		
	private void createAccessorial(Accessorial accessorial,String companyCd) {
		String dataSource="";
		String accessorialType="";
		String sql = "insert into accessorial_code " + "(customer_cd,parent_partner_cd,accessorial_cd,accessorial_type,"
				+ "accessorial_data_source,description,last_update,last_update_source) " + 
				
				"values(" + 
				"?," + 
				"?," +
				"?," +
				"?," +
				"?," +
				"?," +
				"?," +
				"?)";
		try (java.sql.Connection connection = com.openport.marketplace.repository.DbConfigRater.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setString(1, companyCd);
			pstmt.setString(2, "MP");
			pstmt.setString(3, accessorial.getAccessorialCd());
			if(accessorial.getAccessorialType().startsWith("RPU")) {
				accessorialType="RPU";
			}else {
				accessorialType=accessorial.getAccessorialType();
			}
			if(accessorial.getAccessorialType().equalsIgnoreCase("RPUH")) {
				dataSource="Hour";
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPUW")) {
				dataSource="Weight";
				
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPUV")) {
				dataSource="Volume";
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPUF")) {
				dataSource="Floor";
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPUS")) {
				dataSource="Stop";
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPUT")) {
				dataSource="Toll";
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPUD")) {
				dataSource="Day";
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPU12HR")) {
				dataSource="12 HR";
			} else if(accessorial.getAccessorialType().equalsIgnoreCase("RPU30MIN")) {
				dataSource="1/2 HR";
				
				
			}else if(accessorial.getAccessorialType().equalsIgnoreCase("FC")) {
				dataSource="Fixed Charge";
				}
			pstmt.setString(4, accessorialType);
			pstmt.setString(5, dataSource);
			
			pstmt.setString(6,accessorial.getDescription().replaceAll("'", "''"));
			pstmt.setTimestamp(7,new Timestamp(new Date().getTime()));
			pstmt.setString(8, "TMS-MP");
			
			
			
			

			int id = pstmt.executeUpdate();
		} catch (Exception e) {
		}
		
	

}
	
	private Integer insertShipmentAccessorial(Accessorial accessorial) { 
		String sql = "INSERT INTO shipment_accessorial ("
				+ " shipment_id,accessorial_description,accessorial_cd,accessorial_type,accessorial_data_source,create_dt) "  //2
  				+ " values (?,?,?,?,?,?)";
		
		log.info("SQL " + sql);
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{	
			pstmt.setInt(1, accessorial.getShipmentId());
			
			pstmt.setString(2, accessorial.getDescription());
			pstmt.setString(3,accessorial.getAccessorialCd());
			pstmt.setString(4,accessorial.getAccessorialType());
			pstmt.setString(5, accessorial.getAccessorialDataSource());
			pstmt.setTimestamp(6, new Timestamp(new Date().getTime()));
				
			
			int id = pstmt.executeUpdate();
			
			return id;

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	
	private Integer insertServices(Service service) { 
		String sql = "INSERT INTO shipment_service ("
				+ " shipment_id, " //1
				+ " service_id) "  //2
  				+ " values (?,?)";
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{	
			pstmt.setInt(1, service.getShipmentId());
			pstmt.setInt(2, service.getServiceId());
			
			int id = pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				 return rs.getInt(1);
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	
	private Shipment updateShipment(Shipment shipment) {
		String sql = 
				" UPDATE shipment SET ("
				+ " shipment_nbr = ?, " //1
				+ " Shipper_id = ?, "  //2
  				+ " Status_id = ?, "  //3
  				+ " assigned_provider_id = ?, " //4
  				+ " pickup_address_id = ?, " //5
  				+ " delivery_address_id = ?, " //6
  				+ " requested_equipment_type_id = ?, " //7
  				+ " requested_pickup_dt = ?, " //8
  				+ " Requested_Delivery_Dt = ?, " //9
  				+ " Unit_Count = ?, " //10
  				+ " unit_of_measure = ?, " //11
  				+ " Create_Dt = ?, " //12
  				+ " create_user_id = ?, " //13
  				+ " Update_dt = ?, " //14
  				+ " update_user_id = ?," //15
  				+ " tender_expiration_dt = ?) " //16
  				+ " WHERE shipment_id = ?"; //17
		
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{
			shipment.setUpdateDt(getCurrentTimeStamp());
			
			pstmt.setString(1, shipment.getShipmentNbr());
			pstmt.setInt(2, shipment.getShipperId());
			pstmt.setInt(3, shipment.getStatusId());
			pstmt.setInt(4, shipment.getAssignedProviderId());
			pstmt.setInt(5, shipment.getPickupAddressId() );
			pstmt.setInt(6, shipment.getDeliveryAddressId());
			pstmt.setInt(7, shipment.getRequestedEquipmentTypeId());
			pstmt.setTimestamp(8, shipment.getRequestedPickupDt());
			pstmt.setTimestamp(9, shipment.getRequestedDeliveryDt());
			pstmt.setDouble(10, shipment.getUnitCount());
			pstmt.setString(11, shipment.getUnitOfMeasure());
			pstmt.setTimestamp(12, shipment.getCreateDt());
			pstmt.setString(13, shipment.getCreateUserId());
			pstmt.setTimestamp(14, shipment.getUpdateDt());
			pstmt.setString(15, shipment.getUpdateUserId());
			pstmt.setTimestamp(16, shipment.getTenderExpirationDt());
			
			pstmt.setInt(17, shipment.getShipmentId());

			int id = pstmt.executeUpdate();

		} catch (Exception e) {
			log.error("Error message: " + sql, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return shipment;
	}
	
	private ShipmentDetail insertNewShipmentDetail(ShipmentDetail shipmentDetail) {
		String sql = "INSERT INTO shipment_detail ("
				+ " shipment_id, " //1
				+ " qty, "  //2
  				+ " unit_of_measure, " //3
  				+ " shipment_wt, " //4
  				+ " wt_uom, " //5
  				+ " shipment_vol, " //6
  				+ " vol_uom, " //7
  				+ " shipment_len, " //8
  				+ " shipment_wth, " //9
  				+ " shipment_ht, " //10
  				+ " dimension_uom, " //11
  				+ " freight_class_cd, " //12
  				+ " is_hazardous, " //13
  				+ " Create_Dt, " //14
  				+ " Update_dt, "
  				+ " product_desc_txt, "
  				+ " product_cd, "
  				+ " unit_price, "
  				+ " goods_value_bill_to_cd "
  				+")" //15
  				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now(),?,?,?,?)";
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{
			shipmentDetail.setCreateDt(getCurrentTimeStamp());
			shipmentDetail.setUpdateDt(getCurrentTimeStamp());
			shipmentDetail.setWtUom("KGS");
			shipmentDetail.setVolUom("CBM");
			shipmentDetail.setFreightClassCd("65");
			
			if(shipmentDetail.getShipmentLen()==null) {
				shipmentDetail.setShipmentLen(0.0);
			}
			
			if(shipmentDetail.getShipmentWth()==null) {
				shipmentDetail.setShipmentWth(0.0);
			}
			
			if(shipmentDetail.getShipmentHt()==null) {
				shipmentDetail.setShipmentHt(0.0);
			}
			
			int index = 1;
			pstmt.setInt(index++, shipmentDetail.getShipmentId());
			pstmt.setDouble(index++, shipmentDetail.getQty());
			pstmt.setString(index++, shipmentDetail.getUnitOfMeasure());
			pstmt.setDouble(index++, shipmentDetail.getShipmentWt());
			pstmt.setString(index++, shipmentDetail.getWtUom());
			pstmt.setDouble(index++, shipmentDetail.getShipmentVol());
			pstmt.setString(index++, shipmentDetail.getVolUom());
			pstmt.setDouble(index++, shipmentDetail.getShipmentLen());
			pstmt.setDouble(index++, shipmentDetail.getShipmentWth());
			pstmt.setDouble(index++, shipmentDetail.getShipmentHt());
			pstmt.setString(index++, shipmentDetail.getDimensionUom());
			pstmt.setString(index++, shipmentDetail.getFreightClassCd());
			pstmt.setBoolean(index++, BooleanUtils.toBooleanDefaultIfNull(shipmentDetail.getIsHazardous(), false));
			pstmt.setString(index++, shipmentDetail.getProductDescTxt());
			pstmt.setString(index++, StringUtils.defaultIfBlank(shipmentDetail.getProductCode(), "FAK"));
			pstmt.setDouble(index++, shipmentDetail.getUnitPrice());
			pstmt.setString(index++, shipmentDetail.getGoodsValueBillToCode());
			
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				shipmentDetail.setShipmentDetailId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return shipmentDetail;
	}	
	
	private ShipmentService insertNewShipmentService(ShipmentService shipmentService) {
		String sql = "INSERT INTO shipment_service ("
				+ " shipment_id, " //1
				+ " service_id)  " //2
  				+ " values (?,?)";
		try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{
			pstmt.setInt(1, shipmentService.getShipmentId());
			pstmt.setInt(2, shipmentService.getServiceId());
			
			int id = pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				shipmentService.setShipmentServiceId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return shipmentService;
	}
	
	public Integer deleteShipmentDetail(Integer shipmentId) {
		
		try {

			String sql = "delete from shipment_detail where shipment_id = "+shipmentId;
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					return pstmt.executeUpdate();
			} catch (Exception e) {
				
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	public Integer deleteShipmentService(Integer shipmentId) {
		
		try {

			String sql = "delete from shipment_service where shipment_id="+shipmentId;;
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					return pstmt.executeUpdate();
			} catch (Exception e) {
				
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
public Integer deleteShipment(Integer shipmentId) {
		
		try {

			String sql = "delete from shipment where shipment_id="+shipmentId;;
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					return pstmt.executeUpdate();
			} catch (Exception e) {
				
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
public Integer deleteShipmentContact(Integer shipmentId) {
	
	try {

		String sql = "delete from shipment_contact where shipment_id="+shipmentId;;
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				return pstmt.executeUpdate();
		} catch (Exception e) {
			
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	} finally {
		log.debug("Done");
	}
	return null;
}
public Integer deleteShipmentAccessorial(Integer shipmentId) {
	
	try {

		String sql = "delete from shipment_accessorial where shipment_id="+shipmentId;;
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				return pstmt.executeUpdate();
		} catch (Exception e) {
			
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	} finally {
		log.debug("Done");
	}
	return null;
}
public boolean validateShipmentUpdateDt(Timestamp updateDt) {
	
		
		String sql = "SELECT shipment_id from shipment "
				+ "where update_Dt=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setTimestamp(1,updateDt);
		
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return true;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		log.info("does not match updateDt:"+updateDt);
		return false;
}
public Integer getSequenceNbr() {

	String sql = "insert into dbseq_shipment (create_tstamp,update_tstamp) values(now(),now())";

	
	try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

	{

		int id = pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs != null && rs.next()) {
			return rs.getInt(1);
		}
	} catch (Exception e) {
		log.error("Error message: " + e, e);
		new ServiceException(Response.Status.CONFLICT);
	}
	return null;

	}

	private Shipment doSetShipmentRequestedPickupDateByDayOfWeek(Shipment shipment) {
		try {
			//System.out.println("shipment.getPickupDt():"+shipment.getPickupDt());
			
			Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
			//getTime() returns the current date in default time zone
			Date date = calendar.getTime();
			//int day = calendar.get(Calendar.DATE);
			int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			int dayOfWeekDelivery = Integer.parseInt(shipment.getPickupDt());
			//System.out.println("date:"+date+" dayOfWeek:"+dayOfWeek+" dayOfWeekDelivery:"+dayOfWeekDelivery);	
			
			int dayToAdd = dayOfWeekDelivery - dayOfWeek ;
			if(dayToAdd < 0){
				dayToAdd = (7 - dayOfWeek) + dayOfWeekDelivery;
			}
			calendar.add(Calendar.DATE, dayToAdd);
			//Date newDate = calendar.getTime();
			//System.out.println(" dayToAdd:"+dayToAdd+" newDate:"+newDate);
			
			shipment.setRequestedPickupDt( new Timestamp(calendar.getTime().getTime()));
			//System.out.println("shipment.getRequestedPickupDt:"+shipment.getRequestedPickupDt());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return shipment;
	}

public Object[] getGeoLocation(String streetAddress, String city, String state, String countryName,
		String countryCd, String postal) {
	Object[] obj = new Object[2];

	GeoLocation geoLoc = new GeoLocation();
	geoLoc.setStreetAddress(streetAddress);
	geoLoc.setCity(city);
	geoLoc.setState(state);
	// geoLoc.setCountryName();
	geoLoc.setCountryCode(countryCd);
	geoLoc.setPostal(postal);
	try {
		ConvertAddressToLatLong catl = new ConvertAddressToLatLong();
		geoLoc = catl.convertAddressToLatLongPrecise(geoLoc);
		if (geoLoc == null || !geoLoc.getStatus().equalsIgnoreCase("ok") || geoLoc.getLongtitude() == null) {
			geoLoc = catl.convertAddressToLatLongNoApproximate(geoLoc);
		}
		if (geoLoc == null || !geoLoc.getStatus().equalsIgnoreCase("ok") || geoLoc.getLongtitude() == null) {
			//log.error("No Geo Location!!!");
		} else {

			String longtitude = geoLoc.getLongtitude();
			String latitude = geoLoc.getLatitude();
			obj[0] = longtitude;
			obj[1] = latitude;

			if (geoLoc.getStatus().equalsIgnoreCase("OVER_QUERY_LIMIT")) {
				log.error("GEO OVER QUERY LIMIT");
			}
		}
	} catch (Exception ee) {

	}
	return obj;
}

}

