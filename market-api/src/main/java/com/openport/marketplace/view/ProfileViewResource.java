package com.openport.marketplace.view;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.CarrierEquipment;
import com.openport.marketplace.json.CarrierProfile;
import com.openport.marketplace.json.CarrierSearch;
import com.openport.marketplace.json.CompanyFile;
import com.openport.marketplace.json.CompanyService;
import com.openport.marketplace.json.CompanyServicesByRegion;
import com.openport.marketplace.json.DocumentType;
import com.openport.marketplace.json.DropDownList;
import com.openport.marketplace.json.Language;
import com.openport.marketplace.json.Profile;
import com.openport.marketplace.json.Service;
import com.openport.marketplace.json.ServiceArea;
import com.openport.marketplace.json.ServiceAreaProfile;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.ServiceProfile;
import com.openport.marketplace.json.Shipment;
import com.openport.marketplace.json.User;
import com.openport.marketplace.json.VwCarrierProfile;
import com.openport.marketplace.migration.TmsMigration;
import com.openport.marketplace.model.CompanyEquipmentList;
import com.openport.marketplace.model.EquipmentType;
import com.openport.marketplace.model.FileDocumentType;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.util.db.ResultSetMapper;


@Path("profileview") // http://localhost:8080/market-api/serviceAreas
public class ProfileViewResource {
	static final transient Logger log = Logger.getLogger(ProfileViewResource.class);

	    
        
    private List<ServiceAreaProfile> getServiceAreas(Integer companyId) {
    	String sql = "select srvcarea.service_area_name  from carrier_region region left join service_area srvcarea  on  srvcarea.Service_Area_ID = region.Service_Area_ID where "+
    			" region.company_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ServiceAreaProfile> driverRawMapper = new ResultSetMapper<ServiceAreaProfile>();
	        	List<ServiceAreaProfile> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceAreaProfile.class);
	        	if(pojoList!=null && pojoList.size()>0) {
	        		return pojoList;
	        	}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
    }
	 	
    private String 	 getCompanyLogo(Integer companyId) {
    	String sql = "select  logo.relative_path from company_file logo  join file_document_type  doctype on doctype.Company_File_ID = logo.Company_File_ID and logo.company_id="+companyId+" and doctype.Document_Type_Id = 0";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				if	(rs.next()) {
					return rs.getString(1);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
    }

	@GET // http://localhost:8080/market-api/serviceAreas/carrierView/84
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/profile/{carrierCode}")
	public CarrierProfile get(@PathParam("carrierCode") Integer companyId) {
			String sql = " SELECT a.company_name,a.business_headline,a.business_description,a.company_code,a.years_of_operation,a.rating_value,\n"+
						 " concat(coalesce(hq.Address_Line_1,''),coalesce(hq.Address_Line_2,''),coalesce(hq.Address_Line_3,'')) company_address,\n"+
						 " hq.city_name,c.province_name,d.country_name "+
						 " FROM company a inner join company b on a.company_id=b.company_id and a.company_id=?"+" left  join company_address hq\n"+
						 " on a.corporate_address_id=hq.company_address_id left join province c on c.province_id=hq.province_id\n"+
                         " left join country d  on d.Country_ID = hq.Country_ID\n";
                       //  " left join   company_file logo on  logo.Company_ID = a.Company_ID   join file_document_type  doctype on doctype.Company_File_ID = logo.Company_File_ID and doctype.Document_Type_Id = 0 ";
				log.info("SQL " + sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyId);
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Profile> driverRawMapper = new ResultSetMapper<Profile>();
		        	List<Profile> pojoList = driverRawMapper.mapResultSetToObject(rs, Profile.class);
		        	if(pojoList!=null && pojoList.size()>0) {
		        		Profile profile = pojoList.get(0);
		        		CarrierProfile cp = new CarrierProfile();
		        		cp.setBusinessDescription(profile.getBusinessDescription());
		        		cp.setBusinessHeadline(profile.getBusinessHeadline());
		        		cp.setCompanyCode(profile.getCompanyCode());
		        		cp.setCompanyName(profile.getCompanyName());
		        		cp.setRating(profile.getRating());
		        		cp.setYearsOfOperation(profile.getYearsOfOperation());
		        		cp.setCompanyAddress(trimString(profile.getCompanyAddress()).replaceAll("Unknown", ""));
		        		cp.setHqCity(profile.getHqCity());
		        		cp.setHqProvince(profile.getHqProvince());
		        		cp.setHqCountry(trimString(profile.getHqCountry()).replaceAll("Specify a Country", ""));
		        		cp.setCompanyLogoPath(getCompanyLogo(companyId));
		        		
		        		
		        		//get Company Address
		        		
		        		/*
		        		List<ServiceAreaProfile> sap = new LinkedList<>();
		        		List<ServiceProfile> sp = new LinkedList<>();
			        	
		        		Set<String> serviceAreaSet = new HashSet<>();
		        	
		        		Set<String> serviceSet = new HashSet<>();
			        	
		        		for(Profile p : pojoList) {
		        			log.info("Service areas " + p.getServiceAreaName() );
		        			serviceAreaSet.add(p.getServiceAreaName());
		        			
		        		}
		        		for(Profile p : pojoList) {
		        			serviceSet.add(p.getServiceName());
		        			
		        		}
		        		
		        		for(String srvcArea : serviceAreaSet) {
		        			log.info(" SRVC " + srvcArea );
				        	ServiceAreaProfile a = new ServiceAreaProfile();
				        	a.setName(srvcArea);
		        			sap.add(a);
		        		}
		        		for(String srvc : serviceSet) {
		        			sp.add(new ServiceProfile(srvc));
		        		}
		        		
		        		cp.setServiceAreas(sap);
		        		*/
		        		
		        		//cp.setServices(sp);
		        		cp.setServiceAreas(getServiceAreas(companyId));
		        		cp.setCompanyFiles(getCompanyFilePhotos(companyId));
		        		cp.setEquipment(getCarrierEquipment(companyId));
		        	
		        		return cp;
		        		
		        	}
		        	
		        	
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
			return null;
		}

	
	
	
	
	
	private List<CarrierEquipment> getCarrierEquipment(Integer companyId) {
		
		
			String sql = 
			" SELECT c.Equipment_Type_Name, a.Equipment_Count "+
			" FROM company_equipment a "+
			" INNER JOIN equipment b on b.Equipment_ID = a.Equipment_ID "+
			" INNER JOIN equipment_type c on b.Equipment_Type_ID = c.Equipment_Type_ID "+
			" INNER JOIN company e on a.Company_ID = e.Company_ID "+
			" WHERE a.Company_ID = ?";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyId);
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CarrierEquipment> driverRawMapper = new ResultSetMapper<CarrierEquipment>();
		        	List<CarrierEquipment> pojoList = driverRawMapper.mapResultSetToObject(rs, CarrierEquipment.class);
		        	if(pojoList!=null && pojoList.size()>0) {
		        		
		        		return pojoList;
		        		
		        	}
		        	
		        	
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
			return null;
		}
	
	
	/**
	 * @param companyId
	 * @return
	 */
	private ArrayList<com.openport.marketplace.json.CompanyFile> getCompanyFilePhotos(Integer companyId) {
		
		ArrayList<com.openport.marketplace.json.CompanyFile> list = new ArrayList<com.openport.marketplace.json.CompanyFile>();
		try {
			String sql = //"SELECT file_document_type_id,company_file_id,document_type_id FROM `file_document_type` where company_file_id in (select company_file_id from company_file where company_id=? and is_legal_document = 0)";
			/*		" SELECT company_file_id, company_id, is_legal_document, document_name, relative_path, " +
					" Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "+
				    " FROM company_file " + 
					" WHERE company_id =  ? " +
					" AND is_legal_document = 0;";
			*/
			" SELECT a.company_file_id, a.company_id, a.is_legal_document, a.document_name, a.relative_path, "+ 
			"        a.Create_User_ID, a.Create_Dt, a.Update_User_ID, a.Update_Dt  "+
			" FROM company_file a "+
			" INNER JOIN file_document_type b on b.company_File_ID = a.company_File_ID "+
			" WHERE a.company_id =  ?  "+
			" AND a.is_legal_document = 0 "+ 
			" AND b.document_type_id BETWEEN 1 and 5; ";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				 PreparedStatement pstmt = connection.prepareStatement(sql);) {
				 pstmt.setInt(1, companyId);
				 try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.CompanyFile fdt = new com.openport.marketplace.model.CompanyFile(rs);

						com.openport.marketplace.json.CompanyFile json = new com.openport.marketplace.json.CompanyFile();
						json.setCompanyFileId(fdt.getCompanyFileId());
						json.setCompanyId(fdt.getCompanyId());
						json.setIsLegalDocument(fdt.getIsLegalDocument());
						json.setDocumentName(fdt.getDocumentName());
						json.setRelativePath(fdt.getRelativePath());
						json.setCreateUserId(fdt.getCreateUserId());
						json.setCreateDt(fdt.getCreateDt());
						json.setUpdateUserId(fdt.getUpdateUserId());
						json.setUpdateDt(fdt.getUpdateDt());
						list.add(json);
					}
					// return ccList;
				 }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
		}
		return list;
	}	
	
	/**
	 * @param companyId
	 * @return
	 */
	private List<ServiceArea> getCarrierServiceAreas(Integer companyId) {
		
		String sql = 
		//	"SELECT a.service_area_id, a.service_area_name, b.carrier_region_id,b.company_id "+
		//	" FROM service_area a LEFT OUTER JOIN carrier_region b ON a.Service_Area_ID = b.service_area_id and b.company_id=?";
		" SELECT c.Service_Area_ID , c.Service_Area_Name, c.Create_User_ID, c.Create_Dt, c.Update_User_ID, c.Update_Dt "+ 
		" FROM carrier_region a "+
		" INNER JOIN company b ON b.Company_ID = a.Company_ID "+
		" INNER JOIN service_area c on c.Service_Area_ID = a.Service_Area_ID "+
		" WHERE b.Company_ID = ?; ";
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			 	pstmt.setInt(1, companyId);
				
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ServiceArea> driverRawMapper = new ResultSetMapper<ServiceArea>();
	        	List<ServiceArea> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceArea.class);
	        	List<ServiceArea> j_pojoList = new LinkedList<>();
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		for(ServiceArea srvc : pojoList) {
	        			log.info("services"+srvc.getStatus() + " " + srvc.getServiceAreaName() + " " + srvc.getCarrierRegionId());
		        		
	        			if(srvc.getCarrierRegionId()==null) {
	        				srvc.setStatus(Boolean.FALSE);
	        				srvc.setCarrierRegionId(-1);
	        			}else {
	        				srvc.setStatus(Boolean.TRUE);
	        			}
	        			j_pojoList.add(srvc);
	        		}
	        		return j_pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}	
	
	private List<User> getCarrierUsers(Integer companyId) {
		List<User> n_pojoList = new ArrayList<User>();
		
		String sql = "select a.user_id,a.email_address,a.first_name,a.last_name,a.phone_nbr,a.create_user_id, a.create_dt,a.update_user_id,a.update_dt,"+
					 "b.company_user_id,b.company_id,b.is_primary_contact,b.is_technical_contact from "+
					 " user a inner join company_user b on a.user_id=b.user_id and b.company_id=?"; 
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
				List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
				if (pojoList!=null && !pojoList.isEmpty()) {
					for(User c : pojoList) {
						n_pojoList.add(c);
					}
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return n_pojoList;
	}
	
	
	/**
	 * @param id
	 * @param DbConfig.getDataSource()
	 * @return
	 */
	private CompanyFile zgetCompanyFileById(Integer id) {
		String sql = 
				" SELECT company_file_id,company_id,is_legal_document,document_name,relative_path " +
			    " FROM company_file " + 
				" WHERE company_file_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyFile> driverRawMapper = new ResultSetMapper<CompanyFile>();
				List<CompanyFile> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyFile.class);
				if (!pojoList.isEmpty()) {
					return pojoList.get(0);
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}
	
	/**
	 * @param id
	 * @param DbConfig.getDataSource()
	 * @return
	 */
	private DocumentType zgetDocumentTypeById(Integer id) {
		String sql = " SELECT document_type_id,description_txt" 
	               + " FROM document_type "
				   + " WHERE document_type_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<DocumentType> driverRawMapper = new ResultSetMapper<DocumentType>();
				List<DocumentType> pojoList = driverRawMapper.mapResultSetToObject(rs, DocumentType.class);
				if (pojoList!=null && !pojoList.isEmpty()) {
					return pojoList.get(0);
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}	
	
	private List<CompanyService> zgetCompanyServiceByCompanyId(Integer companyId) {

		List<CompanyService> service = new ArrayList<CompanyService>();
		
		try {
			String sql = 
					" SELECT company_service_id,company_id,service_id,create_user_id,rating_value "	+ 
			        " FROM company_service where company_id = ? ";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setInt(1, companyId);

				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CompanyService> driverRawMapper = new ResultSetMapper<CompanyService>();
		        	List<CompanyService> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyService.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}else {
						log.info("No Service record!!");
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return null;

	}
	

	@GET  // http://localhost:8080/market-api/serviceAreas/area/JABODETABEK/Customs
	@Path("area/{serviceAreaName}/{serviceName}") 
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyServicesByRegion> zGetAllCarriersForServiceAreaAndService(
			@PathParam("serviceAreaName") String serviceAreaName, 
			@PathParam("serviceName") String serviceName,
			@PathParam("equipmentType") String equipmentType ) 
	{
	
		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();
		
		int i =  1;
		String currServiceArea = "";
		String prevServiceArea = "";
		String currCarrier = "";
		String prevCarrier = "";
		
		boolean d1stTime = true;
		
		try {
			serviceAreaName = ""+serviceAreaName;
			if (serviceAreaName.equalsIgnoreCase("undefined")) {
				serviceAreaName = "*";
			}
			serviceName = ""+serviceName;
			if (serviceName.equalsIgnoreCase("undefined")) {
				serviceName = "*";
			}
			equipmentType = ""+equipmentType;
			if (equipmentType.equalsIgnoreCase("undefined")) {
				equipmentType = "*";
			}
			
			
			//String sql = "SELECT * FROM `vw_carrier_services` ";
			String sql = 
			" SELECT DISTINCT cmpny.company_id AS company_id, "+
			"       cmpny.company_name, "+
			"       cmpny.company_code, "+
			"       sa.service_area_name, "+
            "       s.service_name, "+
            "		et.equipment_type_name, "+
            //"		ce.equipment_count, "+
			"		cs.rating_value "+
			" FROM company cmpny "+
			" JOIN company_address ca ON ca.Company_Address_ID = cmpny.Corporate_Address_ID "+
			" JOIN province p ON p.Province_ID = ca.Province_ID "+
			" JOIN country cntry  ON cntry.Country_ID = p.Country_ID "+
			" LEFT JOIN company_service cs ON cs.Company_ID = cmpny.Company_ID "+
			" LEFT JOIN service s    ON s.Service_ID = cs.Service_ID "+
			" JOIN carrier_region cr ON cr.Company_ID = cmpny.Company_ID "+
			" JOIN service_area sa   ON sa.Service_Area_ID = cr.Service_Area_ID "+
			" LEFT JOIN company_equipment ce on  ce.Company_ID =  cmpny.Company_ID "+
			" LEFT JOIN equipment e on e.equipment_id = ce.equipment_id "+
			" LEFT JOIN equipment_type et on et.equipment_type_id = e.equipment_type_id; ";		
			
			String where1 = null;
			String where2 = null;
			if (!(serviceAreaName.equalsIgnoreCase("*"))) {
				serviceAreaName = "%"+serviceAreaName.replaceAll(" ","%")+"%";
				where1 = " service_area_name LIKE '"+serviceAreaName+"'";
			}
			if (!(serviceName.equalsIgnoreCase("*"))) {
				serviceName     = "%"+serviceName.replaceAll(" ","%")+"%";
				where2 = " service_name LIKE '"+serviceName+"'";
			}
			if (!(equipmentType.equalsIgnoreCase("*"))) {
				equipmentType     = "%"+equipmentType.replaceAll(" ","%")+"%";
				where2 = " equipment_type_name LIKE '"+equipmentType+"'";
			}

			
			String whereClause = "";
			if ((where1 != null) && (where2 != null)) {
				whereClause = " WHERE "+where1 + " AND "+where2;
			}
			if ((where1 == null) && (where2 != null)) {
				whereClause = " WHERE "+where2;
			}
			if ((where1 != null) && (where2 == null)) {
				whereClause = " WHERE "+where1;
			}
			//if both where1 & where2 are null, then no where clause
			
			//sql = sql + whereClause+" ORDER BY company_name, service_area_name, service_name ;";
			sql = sql + whereClause+" ORDER BY service_area_name, company_name, service_name ;";
			log.info("GetAllCarriersForServiceAreaAndService:"+sql);
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				//pstmt.setString(1, serviceAreaName);
				//pstmt.setString(2, serviceName);
		         try (ResultSet rs = pstmt.executeQuery()) {
					 while (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						
						//curr = svc.getCompanyName()+svc.getServiceAreaName();
						currServiceArea = svc.getServiceAreaName();
						currCarrier = svc.getCompanyName();
						if (d1stTime) {
							csr.setSeqNbr(""+i++);
							csr.setCompanyId(svc.getCompanyId());
							csr.setCompanyName(svc.getCompanyName());
							csr.setCompanyCode(svc.getCompanyCode());
							csr.setServiceAreaName(svc.getServiceAreaName());
							csr.setServiceName(svc.getServiceName());
							csr.setRatingValue(svc.getRatingValue());
							
							//curr = svc.getCompanyName()+svc.getServiceAreaName();
							currServiceArea = svc.getServiceAreaName();
							currCarrier = svc.getCompanyName();
							
							prevServiceArea = currServiceArea;
							prevCarrier     = currCarrier;
							d1stTime = false;
						} else {
							if (prevServiceArea.equalsIgnoreCase(currServiceArea)) {
								csr.setCompanyId(svc.getCompanyId());
								//csr.setCompanyName("");
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName("");
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							} else {
								csr.setSeqNbr(""+i++);
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName(svc.getServiceAreaName());
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							}
						}
						prevServiceArea = currServiceArea;		
						prevCarrier     = currCarrier;
						serviceAreas.add(csr);
			        }
					
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}

/*
	@GET  // http://localhost:8080/market-api/serviceAreas/area/JABODETABEK/Customs
	@Path("area/{serviceAreaName}/{serviceName}") 
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyServicesByRegion> GetAllCarriersForServiceAreaAndService(
			@PathParam("serviceAreaName") String serviceAreaName, 
			@PathParam("serviceName") String serviceName,
			@PathParam("equipmentType") String equipmentType ) 
	{
	
		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();
		
		int i =  1;
		String currServiceArea = "";
		String prevServiceArea = "";
		String currCarrier = "";
		String prevCarrier = "";
		
		boolean d1stTime = true;
		
		try {
			serviceAreaName = ""+serviceAreaName;
			if (serviceAreaName.equalsIgnoreCase("undefined")) {
				serviceAreaName = "*";
			}
			serviceName = ""+serviceName;
			if (serviceName.equalsIgnoreCase("undefined")) {
				serviceName = "*";
			}
			
			String sql = "SELECT * FROM `vw_carrier_services` ";
			//String sql = "SELECT * FROM `vw_carrier_services` WHERE service_area_name LIKE '"+serviceAreaName+"' AND service_name LIKE '"+serviceName+"';";
			
			String where1 = null;
			String where2 = null;
			if (!(serviceAreaName.equalsIgnoreCase("*"))) {
				serviceAreaName = "%"+serviceAreaName.replaceAll(" ","%")+"%";
				where1 = " service_area_name LIKE '"+serviceAreaName+"'";
			}
			if (!(serviceName.equalsIgnoreCase("*"))) {
				serviceName     = "%"+serviceName.replaceAll(" ","%")+"%";
				where2 = " service_name LIKE '"+serviceName+"'";
			}
			
			String whereClause = "";
			if ((where1 != null) && (where2 != null)) {
				whereClause = " WHERE "+where1 + " AND "+where2;
			}
			if ((where1 == null) && (where2 != null)) {
				whereClause = " WHERE "+where2;
			}
			if ((where1 != null) && (where2 == null)) {
				whereClause = " WHERE "+where1;
			}
			//if both where1 & where2 are null, then no where clause
			
			//sql = sql + whereClause+" ORDER BY company_name, service_area_name, service_name ;";
			sql = sql + whereClause+" ORDER BY service_area_name, company_name, service_name ;";
			log.info("GetAllCarriersForServiceAreaAndService:"+sql);
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				//pstmt.setString(1, serviceAreaName);
				//pstmt.setString(2, serviceName);
		         try (ResultSet rs = pstmt.executeQuery()) {
					 while (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						
						//curr = svc.getCompanyName()+svc.getServiceAreaName();
						currServiceArea = svc.getServiceAreaName();
						currCarrier = svc.getCompanyName();
						if (d1stTime) {
							csr.setSeqNbr(""+i++);
							csr.setCompanyId(svc.getCompanyId());
							csr.setCompanyName(svc.getCompanyName());
							csr.setCompanyCode(svc.getCompanyCode());
							csr.setServiceAreaName(svc.getServiceAreaName());
							csr.setServiceName(svc.getServiceName());
							csr.setRatingValue(svc.getRatingValue());
							
							//curr = svc.getCompanyName()+svc.getServiceAreaName();
							currServiceArea = svc.getServiceAreaName();
							currCarrier = svc.getCompanyName();
							
							prevServiceArea = currServiceArea;
							prevCarrier     = currCarrier;
							d1stTime = false;
						} else {
							if (prevServiceArea.equalsIgnoreCase(currServiceArea)) {
								csr.setCompanyId(svc.getCompanyId());
								//csr.setCompanyName("");
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName("");
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							} else {
								csr.setSeqNbr(""+i++);
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName(svc.getServiceAreaName());
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							}
						}
						prevServiceArea = currServiceArea;		
						prevCarrier     = currCarrier;
						serviceAreas.add(csr);
			        }
					
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}	
	*/
	private String trimString(String s) {
		// TODO Auto-generated method stub
		if (s == null) {
			return "";
		} else {
			return s.replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
		}
	}
	
}
