package com.openport.marketplace.view;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.CarrierSearch;
import com.openport.marketplace.json.CompanyFile;
import com.openport.marketplace.json.CompanyService;
import com.openport.marketplace.json.CompanyServicesByRegion;
import com.openport.marketplace.json.DocumentType;
import com.openport.marketplace.json.DropDownList;
import com.openport.marketplace.json.Language;
import com.openport.marketplace.json.Service;
import com.openport.marketplace.json.ServiceArea;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.User;
import com.openport.marketplace.json.VwCarrierProfile;
import com.openport.marketplace.model.CompanyEquipmentList;
import com.openport.marketplace.model.EquipmentType;
import com.openport.marketplace.model.FileDocumentType;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.util.db.ResultSetMapper;


@Path("serviceAreas") // http://localhost:8080/market-api/serviceAreas
public class ServiceAreaResource {
	static final transient Logger log = Logger.getLogger(ServiceAreaResource.class);

	
    @GET  // http://localhost:8080/market-api/serviceAreas/services
    @Path ("services")
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Service> zgetAllServices() {
		List<Service> servicesArrayList = new ArrayList<Service>();
		try {
			String sql = " SELECT service_id, service_name, is_transportation, create_user_id, Create_Dt, update_user_id, Update_dt"
					   + " FROM `service` ORDER BY service_name;";
			
			log.info("getAllServices sql:"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				
				Service serviceJSON = new Service();
				serviceJSON.setServiceId(-1);
				serviceJSON.setServiceName("*");
				servicesArrayList.add(serviceJSON);
				
		        try (ResultSet rs = pstmt.executeQuery()) {
		        	 
					while (rs.next()) {
						com.openport.marketplace.model.Service serviceModel = new com.openport.marketplace.model.Service(rs);
						serviceJSON = new Service();
						serviceJSON.setServiceId(serviceModel.getServiceId());
						serviceJSON.setServiceName(serviceModel.getServiceName());
						servicesArrayList.add(serviceJSON);
			        }
					return servicesArrayList;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		return servicesArrayList;
	}
	
    
    @GET  // http://localhost:8080/market-api/serviceAreas/areasDDL
    @Path ("areasDDL")
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<DropDownList> getAllServiceAreasForDropDownList() {
		List<DropDownList> serviceAreaArrayList = new ArrayList<DropDownList>();
		try {
			String sql = " SELECT service_area_id, service_area_name, create_user_id, create_dt, update_user_id, update_dt "
					   + " FROM `service_area` ORDER BY service_area_name;";
			
			log.info("getAllServiceAreasForDropDownList sql:"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				
				DropDownList serviceAreaJSON = new DropDownList();
				//serviceAreaJSON.setName("All Areas/Regions");
				//serviceAreaJSON.setTicked("true");
				//serviceAreaArrayList.add(serviceAreaJSON);
				
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.ServiceArea serviceAreaModel = new com.openport.marketplace.model.ServiceArea(rs);
						serviceAreaJSON = new DropDownList();
						serviceAreaJSON.setName(serviceAreaModel.getServiceAreaName());
						serviceAreaArrayList.add(serviceAreaJSON);						
			        }
					return serviceAreaArrayList;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		return serviceAreaArrayList;
	} 
    
    @GET  // http://localhost:8080/market-api/serviceAreas/servicesDDL
    @Path ("servicesDDL")
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<DropDownList> getAllServicesForDropDownList() {
		List<DropDownList> servicesArrayList = new ArrayList<DropDownList>();
		try {
			String sql = " SELECT service_id, service_name, is_transportation, create_user_id, Create_Dt, update_user_id, Update_dt"
					   + " FROM `service` ORDER BY service_name;";
			
			log.info("getAllServices sql:"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				
				DropDownList serviceJSON = new DropDownList();
				//serviceJSON.setName("All Services");
				//serviceJSON.setTicked("true");
				//servicesArrayList.add(serviceJSON);
				
		        try (ResultSet rs = pstmt.executeQuery()) {
		        	 
					while (rs.next()) {
						com.openport.marketplace.model.Service serviceModel = new com.openport.marketplace.model.Service(rs);
						serviceJSON = new DropDownList();
						serviceJSON.setName(serviceModel.getServiceName());
						servicesArrayList.add(serviceJSON);
			        }
					return servicesArrayList;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		return servicesArrayList;
	}    
    
    
    @GET  // http://localhost:8080/market-api/serviceAreas/allServiceAreas
    @Path ("allServiceAreas")
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ServiceArea> zgetAllServiceAreas() {
		List<ServiceArea> serviceAreaArrayList = new ArrayList<ServiceArea>();
		try {

			String sql = " SELECT service_area_id, service_area_name, create_user_id, create_dt, update_user_id, update_dt "
					   + " FROM `service_area` ORDER BY service_area_name;";
			
			log.info("getAllServiceAreas sql:"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				
				ServiceArea serviceAreaJSON = new ServiceArea();
				serviceAreaJSON.setServiceAreaId(-1);
				serviceAreaJSON.setServiceAreaName("*");
				serviceAreaArrayList.add(serviceAreaJSON);
				
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.ServiceArea serviceAreaModel = new com.openport.marketplace.model.ServiceArea(rs);
						serviceAreaJSON = new ServiceArea();
						serviceAreaJSON.setServiceAreaId(serviceAreaModel.getServiceAreaId());
						serviceAreaJSON.setServiceAreaName(serviceAreaModel.getServiceAreaName());
						//serviceAreaJSON.setCreateUserId(serviceAreaModel.getCreateUserId());
						//serviceAreaJSON.setCreateDt(serviceAreaModel.getCreateDt());
						//serviceAreaJSON.setUpdateUserId(serviceAreaModel.getUpdateUserId());
						//serviceAreaJSON.setUpdateDt(serviceAreaModel.getUpdateDt());
						serviceAreaArrayList.add(serviceAreaJSON);						
			        }
					return serviceAreaArrayList;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		return serviceAreaArrayList;
	}    
    
    
    @GET  // http://localhost:8080/market-api/serviceAreas/area/JABODETABEK
    @Path("area/{serviceAreaName}") 
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyServicesByRegion> zgetAllCarriersForServiceArea(@PathParam("serviceAreaName") String serviceAreaName) {
		
		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();
		
		try {
			int i =  1;
			String curr = "";
			String prev = "";
			boolean d1stTime = true;
			String sql = "SELECT * FROM `vw_carrier_services` WHERE service_area_name = ? "
			     		+ "order by company_name, service_area_name, service_name;";
			
			if ((serviceAreaName != null) && (serviceAreaName.equalsIgnoreCase("*"))) {
				sql = "SELECT * FROM `vw_carrier_services` order by company_name, service_area_name, service_name ";
			}			 
			
			try (
				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				if ((serviceAreaName != null) && (serviceAreaName.equalsIgnoreCase("*"))) {
					
				} else {
					pstmt.setString(1, serviceAreaName);
				}
				
				log.info(sql);
		         try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						
						curr = svc.getCompanyName()+svc.getServiceAreaName();
						if (d1stTime) {
							csr.setSeqNbr(""+i++);
							csr.setCompanyId(svc.getCompanyId());
							csr.setCompanyName(svc.getCompanyName());
							csr.setCompanyCode(svc.getCompanyCode());
							csr.setServiceAreaName(svc.getServiceAreaName());
							csr.setServiceName(svc.getServiceName());
							csr.setRatingValue(svc.getRatingValue());
							
							curr = svc.getCompanyName()+svc.getServiceAreaName();
							prev = curr;
							d1stTime = false;
						} else {
							if (prev.equalsIgnoreCase(curr)) {
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName("");
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName("");
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								curr = svc.getCompanyName()+svc.getServiceAreaName();
							} else {
								csr.setSeqNbr(""+i++);
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName(svc.getCompanyName());
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName(svc.getServiceAreaName());
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								curr = svc.getCompanyName()+svc.getServiceAreaName();
							}
						}
						prev = curr;
						serviceAreas.add(csr);
			        }
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}	
	
    @POST  // http://localhost:8080/market-api/serviceAreas/area/search
    @Path("area/search") 
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyServicesByRegion> Search(CarrierSearch carrierSearch) {

		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();

    	if (carrierSearch == null) {
    		log.info("Search: carrierSearch param is NULL...");
    		return serviceAreas;
    	} 
    	else {//added on  03/15/17 by jero.dungog@openport.com  
    		//the first if will NEVER be true, 
    		//because once the search method is called, the CarrierSearch class will be instantiated.
    		//thus it will never be null, but what the code in the else does is to check if the variables
    		//(AreaList, ServiceList, EquipmentList) of the CarrierSearch class is null or empty.
    		//this way we can really be sure that the user did not select anything in the front end.
    		boolean isAreaListEmpty = (carrierSearch.getAreaList() == null || carrierSearch.getAreaList().isEmpty());
    		boolean isServiceListEmty = (carrierSearch.getServiceList() == null || carrierSearch.getServiceList().isEmpty());
    		boolean isEquipmentListEmpty = (carrierSearch.getEquipmentList() == null || carrierSearch.getEquipmentList().isEmpty());
    		
    		//validation if the three Lists are indeed empty.
    		if(isAreaListEmpty && isServiceListEmty && isEquipmentListEmpty) {
    			log.info("Search: carrierSearch param is NULL...");
    			return serviceAreas;
    		}
    	}
		
		int i =  1;
		String currServiceArea = "";
		String prevServiceArea = "";
		String currCarrier = "";
		String prevCarrier = "";
		
		boolean d1stTime = true;
		
		try {
			//String sql = "SELECT * FROM `vw_carrier_services` ";
			String sql = 
			" SELECT DISTINCT cmpny.company_id AS company_id, "+
			"       cmpny.company_name, "+
			"       cmpny.company_code, "+
			"       sa.service_area_name, "+
            "       s.service_name, "+
            "		et.equipment_type_name, "+
            //"		ce.equipment_count, "+
			"		cs.rating_value "+
			" FROM company cmpny "+
			" JOIN company_address ca ON ca.Company_Address_ID = cmpny.Corporate_Address_ID "+
			" JOIN province p ON p.Province_ID = ca.Province_ID "+
			" JOIN country cntry  ON cntry.Country_ID = p.Country_ID "+
			" LEFT JOIN company_service cs ON cs.Company_ID = cmpny.Company_ID "+
			" LEFT JOIN service s    ON s.Service_ID = cs.Service_ID "+
			" JOIN carrier_region cr ON cr.Company_ID = cmpny.Company_ID "+
			" JOIN service_area sa   ON sa.Service_Area_ID = cr.Service_Area_ID "+
			" LEFT JOIN company_equipment ce on  ce.Company_ID =  cmpny.Company_ID "+
			" LEFT JOIN equipment e on e.equipment_id = ce.equipment_id "+
			" LEFT JOIN equipment_type et on et.equipment_type_id = e.equipment_type_id ";					
			
			//carrierSearch has 3 Lists
			List<DropDownList> areaList = carrierSearch.getAreaList();
			List<DropDownList> serviceList = carrierSearch.getServiceList();
			List<DropDownList> equipmentList = carrierSearch.getEquipmentList();
			
			String areas = "";
			if (areaList != null) {
				DropDownList ddl = new DropDownList();
				for (int a = 0; a  < areaList.size(); a++) {
					ddl = areaList.get(a);
					if (a == 0) {
						areas = "'"+ddl.getName()+"'";
					} else
						areas = areas +",'"+ddl.getName()+"'";
					}
			}
			
			String services = "";
			if ((serviceList != null) && !(serviceList.isEmpty())) {
				DropDownList ddl = new DropDownList();
				for (int a = 0; a  < serviceList.size(); a++) {
					ddl = serviceList.get(a);
					if (a == 0) {
						services = "'"+ddl.getName()+"'";
					} else {
						services = services +",'"+ddl.getName()+"'";
					}
				}
			}

			String equipment = "";
			//if (equipmentList != null) {
			if ((equipmentList != null) && !(equipmentList.isEmpty())) {				
				DropDownList ddl = new DropDownList();
				for (int a = 0; a  < equipmentList.size(); a++) {
					ddl = equipmentList.get(a);
					if (a == 0) {
						equipment = "'"+ddl.getName()+"'";
					} else {
						equipment = equipment +",'"+ddl.getName()+"'";
					}
				}
			}			
			
			
			//commented on 03/15/17 by jero.dungog@openport.com
			//see below comments for reasons on why it was commented out
			//String whereClause = " WHERE "+" service_area_name in ("+areas+") "+" and service_name in ("+services+")";
//			String whereClause = " WHERE "+" service_area_name in ("+areas+") ";
//			if ((serviceList != null) && !(serviceList.isEmpty())) {
//				whereClause = whereClause + " and service_name in ("+services+") ";
//			}
//			
//			//if (equipmentList != null) {
//			if ((equipmentList != null) && !(equipmentList.isEmpty())) {
//				whereClause = whereClause + " and equipment_type_name in ("+equipment+") ";
//			}
			
			//add on 01/15/17 by jero.dungog@openport.com 
			//the above codes where commented out because of the following reasons:
			//1 if the areas variable is empty, it will result to a wrong query.
			//2 if there was no area chosen and only services, the query will still have an "AND" key word after the where
			// clause even though it did not have a preceeding argument. Same is true with equipment variable.
			//3 modifying a String again and again is very expensive in the memory and is a lot slower compared to
			//using StringBuilder.
			StringBuilder whereBuilder = new StringBuilder();
			whereBuilder.append("WHERE ");
			if(areas != null && !areas.isEmpty()) {
				whereBuilder.append("service_area_name in (");
				whereBuilder.append(areas);
				whereBuilder.append(") ");
			}
			if(services != null && !services.isEmpty()) {
				if(whereBuilder.toString().contains("_name")) {
					whereBuilder.append("and ");
				}
				whereBuilder.append("service_name in (");
				whereBuilder.append(services);
				whereBuilder.append(") ");
			}
			if(equipment != null && !equipment.isEmpty()) {
				if(whereBuilder.toString().contains("_name")) {
					whereBuilder.append("and ");
				}
				whereBuilder.append("equipment_type_name in (");
				whereBuilder.append(equipment);
				whereBuilder.append(") ");
			}
			
			String whereClause = whereBuilder.toString();
			
			//v2017-02-08
			if ((equipmentList != null) && (equipmentList.isEmpty())) {
				//modify the SQL so that it won't have repeating result sets
				sql = 
				" SELECT DISTINCT cmpny.company_id AS company_id, "+
				"       cmpny.company_name, "+
				"       cmpny.company_code, "+
				"       sa.service_area_name, "+
				"       s.service_name, "+
				//"       et.equipment_type_name, "+
				"		NULL as equipment_type_name, "+ // so results wont include equipment types
				"		cs.rating_value "+
				" FROM company cmpny "+
				" JOIN company_address ca ON ca.Company_Address_ID = cmpny.Corporate_Address_ID "+
				" JOIN province p ON p.Province_ID = ca.Province_ID "+
				" JOIN country cntry  ON cntry.Country_ID = p.Country_ID "+
				" LEFT JOIN company_service cs ON cs.Company_ID = cmpny.Company_ID "+
				" LEFT JOIN service s    ON s.Service_ID = cs.Service_ID "+
				" JOIN carrier_region cr ON cr.Company_ID = cmpny.Company_ID "+
				" JOIN service_area sa   ON sa.Service_Area_ID = cr.Service_Area_ID "+
				" LEFT JOIN company_equipment ce on  ce.Company_ID =  cmpny.Company_ID "+
				" LEFT JOIN equipment e on e.equipment_id = ce.equipment_id "+
				" LEFT JOIN equipment_type et on et.equipment_type_id = e.equipment_type_id ";
			} 
			//v2017-02-08
			
			sql = sql + whereClause+" ORDER BY service_area_name, company_name, service_name ;";
			log.info("v2 GetAllCarriersForServiceAreaAndService:"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				//pstmt.setString(1, serviceAreaName);
				//pstmt.setString(2, serviceName);
		         try (ResultSet rs = pstmt.executeQuery()) {
					 while (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						
						//curr = svc.getCompanyName()+svc.getServiceAreaName();
						currServiceArea = svc.getServiceAreaName();
						currCarrier = svc.getCompanyName();
						if (d1stTime) {
							csr.setSeqNbr(""+i++);
							csr.setCompanyId(svc.getCompanyId());
							csr.setCompanyName(svc.getCompanyName());
							csr.setCompanyCode(svc.getCompanyCode());
							csr.setServiceAreaName(svc.getServiceAreaName());
							csr.setServiceName(svc.getServiceName());
							csr.setRatingValue(svc.getRatingValue());
							
							//curr = svc.getCompanyName()+svc.getServiceAreaName();
							currServiceArea = svc.getServiceAreaName();
							currCarrier = svc.getCompanyName();
							
							prevServiceArea = currServiceArea;
							prevCarrier     = currCarrier;
							d1stTime = false;
						} else {
							if (prevServiceArea.equalsIgnoreCase(currServiceArea)) {
								csr.setCompanyId(svc.getCompanyId());
								//csr.setCompanyName("");
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName("");
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							} else {
								csr.setSeqNbr(""+i++);
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName(svc.getServiceAreaName());
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							}
						}
						prevServiceArea = currServiceArea;		
						prevCarrier     = currCarrier;
						serviceAreas.add(csr);
			        }
					
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}    
    
    /*
	public List<CompanyServicesByRegion> Search(CarrierSearch carrierSearch) {

		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();

    	if (carrierSearch == null) {
    		log.info("Search: carrierSearch param is NULL...");
    		return serviceAreas;
    	}
		
		int i =  1;
		String currServiceArea = "";
		String prevServiceArea = "";
		String currCarrier = "";
		String prevCarrier = "";
		
		boolean d1stTime = true;
		
		try {
			String sql = "SELECT * FROM `vw_carrier_services` ";
			
			//carrierSearch has 2 Lists
			List<DropDownList> areaList = carrierSearch.getAreaList();
			List<DropDownList> serviceList = carrierSearch.getServiceList();
			
			String areas = "";
			if (areaList != null) {
				DropDownList ddl = new DropDownList();
				for (int a = 0; a  < areaList.size(); a++) {
					ddl = areaList.get(a);
					if (a == 0) {
						areas = "'"+ddl.getName()+"'";
					} else
						areas = areas +",'"+ddl.getName()+"'";
					}
			}
			
			
			String services = "";
			if (serviceList != null) {
				DropDownList ddl = new DropDownList();
				for (int a = 0; a  < serviceList.size(); a++) {
					ddl = serviceList.get(a);
					if (a == 0) {
						services = "'"+ddl.getName()+"'";
					} else {
						services = services +",'"+ddl.getName()+"'";
					}
				}
			}
			
			String whereClause = " WHERE "+" service_area_name in ("+areas+") "+" and service_name in ("+services+")";

			
			sql = sql + whereClause+" ORDER BY service_area_name, company_name, service_name ;";
			log.info("v2 GetAllCarriersForServiceAreaAndService:"+sql);
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				//pstmt.setString(1, serviceAreaName);
				//pstmt.setString(2, serviceName);
		         try (ResultSet rs = pstmt.executeQuery()) {
					 while (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						
						//curr = svc.getCompanyName()+svc.getServiceAreaName();
						currServiceArea = svc.getServiceAreaName();
						currCarrier = svc.getCompanyName();
						if (d1stTime) {
							csr.setSeqNbr(""+i++);
							csr.setCompanyId(svc.getCompanyId());
							csr.setCompanyName(svc.getCompanyName());
							csr.setCompanyCode(svc.getCompanyCode());
							csr.setServiceAreaName(svc.getServiceAreaName());
							csr.setServiceName(svc.getServiceName());
							csr.setRatingValue(svc.getRatingValue());
							
							//curr = svc.getCompanyName()+svc.getServiceAreaName();
							currServiceArea = svc.getServiceAreaName();
							currCarrier = svc.getCompanyName();
							
							prevServiceArea = currServiceArea;
							prevCarrier     = currCarrier;
							d1stTime = false;
						} else {
							if (prevServiceArea.equalsIgnoreCase(currServiceArea)) {
								csr.setCompanyId(svc.getCompanyId());
								//csr.setCompanyName("");
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName("");
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							} else {
								csr.setSeqNbr(""+i++);
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName(svc.getServiceAreaName());
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							}
						}
						prevServiceArea = currServiceArea;		
						prevCarrier     = currCarrier;
						serviceAreas.add(csr);
			        }
					
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}      
    */
    
    
	@GET // http://localhost:8080/market-api/serviceAreas/IDTTUM
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{carrierCode}") 
	public List<CompanyServicesByRegion> zgetCarrierServiceAreas(@PathParam("carrierCode") String carrierCode) {
		
		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();
		
		try {
			String sql = "SELECT * FROM `vw_carrier_services` WHERE company_code = ?;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setString(1, carrierCode);

		         try (ResultSet rs = pstmt.executeQuery()) {
					if (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						csr.setCompanyId(svc.getCompanyId());
						csr.setCompanyName(svc.getCompanyName());
						csr.setCompanyCode(svc.getCompanyCode());
						csr.setServiceAreaName(svc.getServiceAreaName());
						csr.setServiceName(svc.getServiceName());
						serviceAreas.add(csr);
			        }
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}
	
	@GET  // http://localhost:8080/market-api/serviceAreas/equipmentTypesDDL
	@Path ("equipmentTypesDDL")
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<DropDownList> getAllEquipmentTypesForDropDownList() {
		List<DropDownList> equipmentTypeArrayList = new ArrayList<DropDownList>();
		try {
				String sql = " SELECT equipment_type_id, equipment_type_name, create_user_id, Create_Dt, update_user_id, Update_dt"
						   + " FROM `equipment_type` ORDER BY equipment_type_name;";
				
				log.info("getAllEquipmentTypesForDropDownList sql:"+sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
					
				DropDownList equipmentTypeJSON = new DropDownList();
				//serviceJSON.setName("All Services");
				//serviceJSON.setTicked("true");
				//servicesArrayList.add(serviceJSON);
					
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
							com.openport.marketplace.model.EquipmentType equipmentTypeModel = new com.openport.marketplace.model.EquipmentType(rs);
							equipmentTypeJSON = new DropDownList();
							equipmentTypeJSON.setName(equipmentTypeModel.getEquipmentTypeName());
							equipmentTypeArrayList.add(equipmentTypeJSON);
			        }
					return equipmentTypeArrayList;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		return equipmentTypeArrayList;
	} 	
	

	@GET // http://localhost:8080/market-api/serviceAreas/carrierView/84
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/carrierView/{carrierCode}")
	public VwCarrierProfile getCarrierView(@PathParam("carrierCode") Integer companyId) {
		
		List<VwCarrierProfile> carrierProfileRecord = new ArrayList<VwCarrierProfile>();
		
		//ArrayList<EquipmentType> equipmentList = null;
		//ArrayList<FileDocumentType> carrierPicturesList = null;
		//ArrayList<ServiceArea> carrierRegionsList = null;
		//ArrayList<Service> carrierServicesList = null;
		com.openport.marketplace.json.VwCarrierProfile json = new com.openport.marketplace.json.VwCarrierProfile();
		
		try {
			String sql = "SELECT * FROM `vw_carrier_profile` WHERE company_id = ?;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setInt(1, companyId);
				log.info("getCarrierView: carrierCode:"+companyId);
		        try (ResultSet rs = pstmt.executeQuery()) {
					 if (rs.next()) {
						com.openport.marketplace.model.VwCarrierProfile svc = new com.openport.marketplace.model.VwCarrierProfile(rs);
						
						json.setCompanyName(svc.getCompanyName());
						json.setCompanyCode(svc.getCompanyCode());
						
						json.setBusinessHeadline(svc.getBusinessHeadline());
						json.setBusinessDescription(svc.getBusinessDescription());
						
						json.setCompanyAddress(svc.getCompanyAddress());
						json.setHqCity(svc.getHqCity());
						json.setHqProvince(svc.getHqProvince());
						json.setHqCountry(svc.getHqCountry());
						json.setTruckPoolAddress(svc.getTruckPoolAddress());
						json.setBranchAddress(svc.getBranchAddress());
						json.setContactName(svc.getContactName());
						json.setEmailAddress(svc.getEmailAddress());
						json.setPhoneNbr(svc.getPhoneNbr());
						json.setYearsOfOperation(svc.getYearsOfOperation());
						json.setRating(svc.getRating());
						json.setCompanyLogoPath(svc.getCompanyLogoPath());
						
						//carrierProfileRecord.add(json);
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		
		json.setCompanyEquipmentList(getCarrierEquipmentList(companyId));
		ArrayList<com.openport.marketplace.json.CompanyEquipmentList> jEquipmentList = json.getCompanyEquipmentList();
		if (jEquipmentList == null) {
			log.info("jEquipmentList is NULL");
		} else {
			log.info("jEquipmentList size:"+jEquipmentList.size());
			com.openport.marketplace.json.CompanyEquipmentList c = new com.openport.marketplace.json.CompanyEquipmentList();
			for (int i = 0; i < jEquipmentList.size(); i++) {
				c =  (com.openport.marketplace.json.CompanyEquipmentList)jEquipmentList.get(i);
				log.info("getCarrierView:"+c.getEquipmentTypeName());
			}
		}
	
		json.setCarrierPicturesList(getCompanyFilePhotos(companyId));
		json.setCarrierServiceAreasList(getCarrierServiceAreas(companyId));
		json.setCarrierUsersList(getCarrierUsers(companyId));
		List<com.openport.marketplace.json.User> jUserList = json.getCarrierUsersList();
		if (jUserList == null) {
			log.info("jUserList is NULL");
		} else {
			log.info("jUserList size:"+jUserList.size());
			com.openport.marketplace.json.User c = new com.openport.marketplace.json.User();
			for (int i = 0; i < jUserList.size(); i++) {
				c =  (com.openport.marketplace.json.User)jUserList.get(i);
				log.info("getCarrierView Users:"+c.getEmailAddress()+"/"+c.getUsername()+"/"+c.getIsPrimaryContact()+"/"+c.getIsTechnicalContact());
			}
		}		
		
		json.setCertifications("");
		
		carrierProfileRecord.add(json);
		return carrierProfileRecord.get(0);		
		//return null;
	}

	
	@GET // http://localhost:8080/market-api/serviceAreas/carrierView/84
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/profileView/{carrierCode}")
	public VwCarrierProfile getProfileView(@PathParam("carrierCode") Integer companyId) {
		
		List<VwCarrierProfile> carrierProfileRecord = new ArrayList<VwCarrierProfile>();
		
		//ArrayList<EquipmentType> equipmentList = null;
		//ArrayList<FileDocumentType> carrierPicturesList = null;
		//ArrayList<ServiceArea> carrierRegionsList = null;
		//ArrayList<Service> carrierServicesList = null;
		com.openport.marketplace.json.VwCarrierProfile json = new com.openport.marketplace.json.VwCarrierProfile();
		
		try {
			String sql = "SELECT * FROM `vw_carrier_profile` WHERE company_id = ?;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setInt(1, companyId);
				log.info("getCarrierView: carrierCode:"+companyId);
		        try (ResultSet rs = pstmt.executeQuery()) {
					 if (rs.next()) {
						com.openport.marketplace.model.VwCarrierProfile svc = new com.openport.marketplace.model.VwCarrierProfile(rs);
						
						json.setCompanyName(svc.getCompanyName());
						json.setCompanyCode(svc.getCompanyCode());
						
						json.setBusinessHeadline(svc.getBusinessHeadline());
						json.setBusinessDescription(svc.getBusinessDescription());
						
						json.setCompanyAddress(svc.getCompanyAddress());
						json.setHqCity(svc.getHqCity());
						json.setHqProvince(svc.getHqProvince());
						json.setHqCountry(svc.getHqCountry());
						json.setTruckPoolAddress(svc.getTruckPoolAddress());
						json.setBranchAddress(svc.getBranchAddress());
						json.setContactName(svc.getContactName());
						json.setEmailAddress(svc.getEmailAddress());
						json.setPhoneNbr(svc.getPhoneNbr());
						json.setYearsOfOperation(svc.getYearsOfOperation());
						json.setRating(svc.getRating());
						json.setCompanyLogoPath(svc.getCompanyLogoPath());
						
						//carrierProfileRecord.add(json);
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		
		json.setCompanyEquipmentList(getCarrierEquipmentList(companyId));
		ArrayList<com.openport.marketplace.json.CompanyEquipmentList> jEquipmentList = json.getCompanyEquipmentList();
		if (jEquipmentList == null) {
			log.info("jEquipmentList is NULL");
		} else {
			log.info("jEquipmentList size:"+jEquipmentList.size());
			com.openport.marketplace.json.CompanyEquipmentList c = new com.openport.marketplace.json.CompanyEquipmentList();
			for (int i = 0; i < jEquipmentList.size(); i++) {
				c =  (com.openport.marketplace.json.CompanyEquipmentList)jEquipmentList.get(i);
				log.info("getCarrierView:"+c.getEquipmentTypeName());
			}
		}
	
		json.setCarrierPicturesList(getCompanyFilePhotos(companyId));
		json.setCarrierServiceAreasList(getCarrierServiceAreas(companyId));
		json.setCarrierUsersList(getCarrierUsers(companyId));
		List<com.openport.marketplace.json.User> jUserList = json.getCarrierUsersList();
		if (jUserList == null) {
			log.info("jUserList is NULL");
		} else {
			log.info("jUserList size:"+jUserList.size());
			com.openport.marketplace.json.User c = new com.openport.marketplace.json.User();
			for (int i = 0; i < jUserList.size(); i++) {
				c =  (com.openport.marketplace.json.User)jUserList.get(i);
				log.info("getCarrierView Users:"+c.getEmailAddress()+"/"+c.getUsername()+"/"+c.getIsPrimaryContact()+"/"+c.getIsTechnicalContact());
			}
		}		
		
		json.setCertifications("");
		
		carrierProfileRecord.add(json);
		return carrierProfileRecord.get(0);		
		//return null;
	}
	
	
	
	private ArrayList<com.openport.marketplace.json.CompanyEquipmentList> getCarrierEquipmentList(Integer companyId) {
		
		ArrayList<com.openport.marketplace.json.CompanyEquipmentList> equipmentList = new ArrayList<com.openport.marketplace.json.CompanyEquipmentList>();
		
		try {
			String sql = 
			" SELECT a.Company_ID, e.Company_Name, c.Equipment_Type_ID, c.Equipment_Type_Name, a.Equipment_Count "+
			" FROM company_equipment a "+
			" INNER JOIN equipment b on b.Equipment_ID = a.Equipment_ID "+
			" INNER JOIN equipment_type c on b.Equipment_Type_ID = c.Equipment_Type_ID "+
			" INNER JOIN company e on a.Company_ID = e.Company_ID "+
			" WHERE a.Company_ID = ?";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setInt(1, companyId);
				
				log.info("getCarrierEquipmentList:"+sql);
				
		         try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.json.CompanyEquipmentList json = new com.openport.marketplace.json.CompanyEquipmentList();
						com.openport.marketplace.model.CompanyEquipmentList cel = new com.openport.marketplace.model.CompanyEquipmentList(rs);
						json.setCompanyId(cel.getCompanyId());
						json.setCompanyName(cel.getCompanyName());
						json.setEquipmentTypeId(cel.getEquipmentTypeId());
						json.setEquipmentTypeName(cel.getEquipmentTypeName());
						json.setEquipmentCount(cel.getEquipmentCount());
						log.info("getCarrierEquipmentList:"+json.getCompanyName()+"/"+
								json.getEquipmentTypeName()+"/"+json.getEquipmentCount()
								);
						equipmentList.add(json);
					}
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return equipmentList;
	}
	
	
	/**
	 * @param companyId
	 * @return
	 */
	private ArrayList<com.openport.marketplace.json.CompanyFile> getCompanyFilePhotos(Integer companyId) {
		
		ArrayList<com.openport.marketplace.json.CompanyFile> list = new ArrayList<com.openport.marketplace.json.CompanyFile>();
		try {
			String sql = //"SELECT file_document_type_id,company_file_id,document_type_id FROM `file_document_type` where company_file_id in (select company_file_id from company_file where company_id=? and is_legal_document = 0)";
			/*		" SELECT company_file_id, company_id, is_legal_document, document_name, relative_path, " +
					" Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "+
				    " FROM company_file " + 
					" WHERE company_id =  ? " +
					" AND is_legal_document = 0;";
			*/
			" SELECT a.company_file_id, a.company_id, a.is_legal_document, a.document_name, a.relative_path, "+ 
			"        a.Create_User_ID, a.Create_Dt, a.Update_User_ID, a.Update_Dt  "+
			" FROM company_file a "+
			" INNER JOIN file_document_type b on b.company_File_ID = a.company_File_ID "+
			" WHERE a.company_id =  ?  "+
			" AND a.is_legal_document = 0 "+ 
			" AND b.document_type_id BETWEEN 1 and 5; ";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				 PreparedStatement pstmt = connection.prepareStatement(sql);) {
				 pstmt.setInt(1, companyId);
				 try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.CompanyFile fdt = new com.openport.marketplace.model.CompanyFile(rs);

						com.openport.marketplace.json.CompanyFile json = new com.openport.marketplace.json.CompanyFile();
						json.setCompanyFileId(fdt.getCompanyFileId());
						json.setCompanyId(fdt.getCompanyId());
						json.setIsLegalDocument(fdt.getIsLegalDocument());
						json.setDocumentName(fdt.getDocumentName());
						json.setRelativePath(fdt.getRelativePath());
						json.setCreateUserId(fdt.getCreateUserId());
						json.setCreateDt(fdt.getCreateDt());
						json.setUpdateUserId(fdt.getUpdateUserId());
						json.setUpdateDt(fdt.getUpdateDt());
						list.add(json);
					}
					// return ccList;
				 }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
		}
		return list;
	}	
	
	/**
	 * @param companyId
	 * @return
	 */
	private List<ServiceArea> getCarrierServiceAreas(Integer companyId) {
		
		String sql = 
		//	"SELECT a.service_area_id, a.service_area_name, b.carrier_region_id,b.company_id "+
		//	" FROM service_area a LEFT OUTER JOIN carrier_region b ON a.Service_Area_ID = b.service_area_id and b.company_id=?";
		" SELECT c.Service_Area_ID , c.Service_Area_Name, c.Create_User_ID, c.Create_Dt, c.Update_User_ID, c.Update_Dt "+ 
		" FROM carrier_region a "+
		" INNER JOIN company b ON b.Company_ID = a.Company_ID "+
		" INNER JOIN service_area c on c.Service_Area_ID = a.Service_Area_ID "+
		" WHERE b.Company_ID = ?; ";
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			 	pstmt.setInt(1, companyId);
				
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ServiceArea> driverRawMapper = new ResultSetMapper<ServiceArea>();
	        	List<ServiceArea> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceArea.class);
	        	List<ServiceArea> j_pojoList = new LinkedList<>();
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		for(ServiceArea srvc : pojoList) {
	        			log.info("services"+srvc.getStatus() + " " + srvc.getServiceAreaName() + " " + srvc.getCarrierRegionId());
		        		
	        			if(srvc.getCarrierRegionId()==null) {
	        				srvc.setStatus(Boolean.FALSE);
	        				srvc.setCarrierRegionId(-1);
	        			}else {
	        				srvc.setStatus(Boolean.TRUE);
	        			}
	        			j_pojoList.add(srvc);
	        		}
	        		return j_pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}	
	
	private List<User> getCarrierUsers(Integer companyId) {
		List<User> n_pojoList = new ArrayList<User>();
		
		String sql = "select a.user_id,a.email_address,a.first_name,a.last_name,a.phone_nbr,a.create_user_id, a.create_dt,a.update_user_id,a.update_dt,"+
					 "b.company_user_id,b.company_id,b.is_primary_contact,b.is_technical_contact from "+
					 " user a inner join company_user b on a.user_id=b.user_id and b.company_id=?"; 
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
				List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
				if (pojoList!=null && !pojoList.isEmpty()) {
					for(User c : pojoList) {
						n_pojoList.add(c);
					}
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return n_pojoList;
	}
	
	
	/**
	 * @param id
	 * @param DbConfig.getDataSource()
	 * @return
	 */
	private CompanyFile zgetCompanyFileById(Integer id) {
		String sql = 
				" SELECT company_file_id,company_id,is_legal_document,document_name,relative_path " +
			    " FROM company_file " + 
				" WHERE company_file_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyFile> driverRawMapper = new ResultSetMapper<CompanyFile>();
				List<CompanyFile> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyFile.class);
				if (!pojoList.isEmpty()) {
					return pojoList.get(0);
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}
	
	/**
	 * @param id
	 * @param DbConfig.getDataSource()
	 * @return
	 */
	private DocumentType zgetDocumentTypeById(Integer id) {
		String sql = " SELECT document_type_id,description_txt" 
	               + " FROM document_type "
				   + " WHERE document_type_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<DocumentType> driverRawMapper = new ResultSetMapper<DocumentType>();
				List<DocumentType> pojoList = driverRawMapper.mapResultSetToObject(rs, DocumentType.class);
				if (pojoList!=null && !pojoList.isEmpty()) {
					return pojoList.get(0);
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}	
	
	private List<CompanyService> zgetCompanyServiceByCompanyId(Integer companyId) {

		List<CompanyService> service = new ArrayList<CompanyService>();
		
		try {
			String sql = 
					" SELECT company_service_id,company_id,service_id,create_user_id,rating_value "	+ 
			        " FROM company_service where company_id = ? ";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setInt(1, companyId);

				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CompanyService> driverRawMapper = new ResultSetMapper<CompanyService>();
		        	List<CompanyService> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyService.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList;
					}else {
						log.info("No Service record!!");
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return null;

	}
	

	@GET  // http://localhost:8080/market-api/serviceAreas/area/JABODETABEK/Customs
	@Path("area/{serviceAreaName}/{serviceName}") 
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyServicesByRegion> zGetAllCarriersForServiceAreaAndService(
			@PathParam("serviceAreaName") String serviceAreaName, 
			@PathParam("serviceName") String serviceName,
			@PathParam("equipmentType") String equipmentType ) 
	{
	
		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();
		
		int i =  1;
		String currServiceArea = "";
		String prevServiceArea = "";
		String currCarrier = "";
		String prevCarrier = "";
		
		boolean d1stTime = true;
		
		try {
			serviceAreaName = ""+serviceAreaName;
			if (serviceAreaName.equalsIgnoreCase("undefined")) {
				serviceAreaName = "*";
			}
			serviceName = ""+serviceName;
			if (serviceName.equalsIgnoreCase("undefined")) {
				serviceName = "*";
			}
			equipmentType = ""+equipmentType;
			if (equipmentType.equalsIgnoreCase("undefined")) {
				equipmentType = "*";
			}
			
			
			//String sql = "SELECT * FROM `vw_carrier_services` ";
			String sql = 
			" SELECT DISTINCT cmpny.company_id AS company_id, "+
			"       cmpny.company_name, "+
			"       cmpny.company_code, "+
			"       sa.service_area_name, "+
            "       s.service_name, "+
            "		et.equipment_type_name, "+
            //"		ce.equipment_count, "+
			"		cs.rating_value "+
			" FROM company cmpny "+
			" JOIN company_address ca ON ca.Company_Address_ID = cmpny.Corporate_Address_ID "+
			" JOIN province p ON p.Province_ID = ca.Province_ID "+
			" JOIN country cntry  ON cntry.Country_ID = p.Country_ID "+
			" LEFT JOIN company_service cs ON cs.Company_ID = cmpny.Company_ID "+
			" LEFT JOIN service s    ON s.Service_ID = cs.Service_ID "+
			" JOIN carrier_region cr ON cr.Company_ID = cmpny.Company_ID "+
			" JOIN service_area sa   ON sa.Service_Area_ID = cr.Service_Area_ID "+
			" LEFT JOIN company_equipment ce on  ce.Company_ID =  cmpny.Company_ID "+
			" LEFT JOIN equipment e on e.equipment_id = ce.equipment_id "+
			" LEFT JOIN equipment_type et on et.equipment_type_id = e.equipment_type_id; ";		
			
			String where1 = null;
			String where2 = null;
			if (!(serviceAreaName.equalsIgnoreCase("*"))) {
				serviceAreaName = "%"+serviceAreaName.replaceAll(" ","%")+"%";
				where1 = " service_area_name LIKE '"+serviceAreaName+"'";
			}
			if (!(serviceName.equalsIgnoreCase("*"))) {
				serviceName     = "%"+serviceName.replaceAll(" ","%")+"%";
				where2 = " service_name LIKE '"+serviceName+"'";
			}
			if (!(equipmentType.equalsIgnoreCase("*"))) {
				equipmentType     = "%"+equipmentType.replaceAll(" ","%")+"%";
				where2 = " equipment_type_name LIKE '"+equipmentType+"'";
			}

			
			String whereClause = "";
			if ((where1 != null) && (where2 != null)) {
				whereClause = " WHERE "+where1 + " AND "+where2;
			}
			if ((where1 == null) && (where2 != null)) {
				whereClause = " WHERE "+where2;
			}
			if ((where1 != null) && (where2 == null)) {
				whereClause = " WHERE "+where1;
			}
			//if both where1 & where2 are null, then no where clause
			
			//sql = sql + whereClause+" ORDER BY company_name, service_area_name, service_name ;";
			sql = sql + whereClause+" ORDER BY service_area_name, company_name, service_name ;";
			log.info("GetAllCarriersForServiceAreaAndService:"+sql);
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				//pstmt.setString(1, serviceAreaName);
				//pstmt.setString(2, serviceName);
		         try (ResultSet rs = pstmt.executeQuery()) {
					 while (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						
						//curr = svc.getCompanyName()+svc.getServiceAreaName();
						currServiceArea = svc.getServiceAreaName();
						currCarrier = svc.getCompanyName();
						if (d1stTime) {
							csr.setSeqNbr(""+i++);
							csr.setCompanyId(svc.getCompanyId());
							csr.setCompanyName(svc.getCompanyName());
							csr.setCompanyCode(svc.getCompanyCode());
							csr.setServiceAreaName(svc.getServiceAreaName());
							csr.setServiceName(svc.getServiceName());
							csr.setRatingValue(svc.getRatingValue());
							
							//curr = svc.getCompanyName()+svc.getServiceAreaName();
							currServiceArea = svc.getServiceAreaName();
							currCarrier = svc.getCompanyName();
							
							prevServiceArea = currServiceArea;
							prevCarrier     = currCarrier;
							d1stTime = false;
						} else {
							if (prevServiceArea.equalsIgnoreCase(currServiceArea)) {
								csr.setCompanyId(svc.getCompanyId());
								//csr.setCompanyName("");
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName("");
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							} else {
								csr.setSeqNbr(""+i++);
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName(svc.getServiceAreaName());
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							}
						}
						prevServiceArea = currServiceArea;		
						prevCarrier     = currCarrier;
						serviceAreas.add(csr);
			        }
					
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}

/*
	@GET  // http://localhost:8080/market-api/serviceAreas/area/JABODETABEK/Customs
	@Path("area/{serviceAreaName}/{serviceName}") 
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyServicesByRegion> GetAllCarriersForServiceAreaAndService(
			@PathParam("serviceAreaName") String serviceAreaName, 
			@PathParam("serviceName") String serviceName,
			@PathParam("equipmentType") String equipmentType ) 
	{
	
		List<CompanyServicesByRegion> serviceAreas = new ArrayList<CompanyServicesByRegion>();
		
		int i =  1;
		String currServiceArea = "";
		String prevServiceArea = "";
		String currCarrier = "";
		String prevCarrier = "";
		
		boolean d1stTime = true;
		
		try {
			serviceAreaName = ""+serviceAreaName;
			if (serviceAreaName.equalsIgnoreCase("undefined")) {
				serviceAreaName = "*";
			}
			serviceName = ""+serviceName;
			if (serviceName.equalsIgnoreCase("undefined")) {
				serviceName = "*";
			}
			
			String sql = "SELECT * FROM `vw_carrier_services` ";
			//String sql = "SELECT * FROM `vw_carrier_services` WHERE service_area_name LIKE '"+serviceAreaName+"' AND service_name LIKE '"+serviceName+"';";
			
			String where1 = null;
			String where2 = null;
			if (!(serviceAreaName.equalsIgnoreCase("*"))) {
				serviceAreaName = "%"+serviceAreaName.replaceAll(" ","%")+"%";
				where1 = " service_area_name LIKE '"+serviceAreaName+"'";
			}
			if (!(serviceName.equalsIgnoreCase("*"))) {
				serviceName     = "%"+serviceName.replaceAll(" ","%")+"%";
				where2 = " service_name LIKE '"+serviceName+"'";
			}
			
			String whereClause = "";
			if ((where1 != null) && (where2 != null)) {
				whereClause = " WHERE "+where1 + " AND "+where2;
			}
			if ((where1 == null) && (where2 != null)) {
				whereClause = " WHERE "+where2;
			}
			if ((where1 != null) && (where2 == null)) {
				whereClause = " WHERE "+where1;
			}
			//if both where1 & where2 are null, then no where clause
			
			//sql = sql + whereClause+" ORDER BY company_name, service_area_name, service_name ;";
			sql = sql + whereClause+" ORDER BY service_area_name, company_name, service_name ;";
			log.info("GetAllCarriersForServiceAreaAndService:"+sql);
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				//pstmt.setString(1, serviceAreaName);
				//pstmt.setString(2, serviceName);
		         try (ResultSet rs = pstmt.executeQuery()) {
					 while (rs.next()) {
						com.openport.marketplace.model.CompanyServicesByRegion svc = new com.openport.marketplace.model.CompanyServicesByRegion(rs);
						CompanyServicesByRegion csr = new CompanyServicesByRegion();
						
						//curr = svc.getCompanyName()+svc.getServiceAreaName();
						currServiceArea = svc.getServiceAreaName();
						currCarrier = svc.getCompanyName();
						if (d1stTime) {
							csr.setSeqNbr(""+i++);
							csr.setCompanyId(svc.getCompanyId());
							csr.setCompanyName(svc.getCompanyName());
							csr.setCompanyCode(svc.getCompanyCode());
							csr.setServiceAreaName(svc.getServiceAreaName());
							csr.setServiceName(svc.getServiceName());
							csr.setRatingValue(svc.getRatingValue());
							
							//curr = svc.getCompanyName()+svc.getServiceAreaName();
							currServiceArea = svc.getServiceAreaName();
							currCarrier = svc.getCompanyName();
							
							prevServiceArea = currServiceArea;
							prevCarrier     = currCarrier;
							d1stTime = false;
						} else {
							if (prevServiceArea.equalsIgnoreCase(currServiceArea)) {
								csr.setCompanyId(svc.getCompanyId());
								//csr.setCompanyName("");
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName("");
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							} else {
								csr.setSeqNbr(""+i++);
								csr.setCompanyId(svc.getCompanyId());
								csr.setCompanyName(svc.getCompanyName());
								if (prevCarrier.equalsIgnoreCase(currCarrier)) {
									csr.setCompanyName("");
								}
								csr.setCompanyCode(svc.getCompanyCode());
								csr.setServiceAreaName(svc.getServiceAreaName());
								csr.setServiceName(svc.getServiceName());
								csr.setRatingValue(svc.getRatingValue());
								
								//curr = svc.getCompanyName()+svc.getServiceAreaName();
								currServiceArea = svc.getServiceAreaName();
								currCarrier     = svc.getCompanyName();
							}
						}
						prevServiceArea = currServiceArea;		
						prevCarrier     = currCarrier;
						serviceAreas.add(csr);
			        }
					
					return serviceAreas;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return serviceAreas;
	}	
	*/
	
}
