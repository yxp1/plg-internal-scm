package com.openport.marketplace.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.json.JsonArrayBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;

import com.openport.marketplace.json.Carrier;
import com.openport.marketplace.json.LongHaulReward;
import com.openport.marketplace.json.RewardsPointsConfig;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.repository.BidRepository;
import com.openport.marketplace.repository.MaintenanceRepository;
import com.openport.marketplace.helper.AuthenticationHelper;
import com.openport.marketplace.helper.DataResultHelper;


@Path("settings") // http://localhost:8080/tms-api/settings
public class MaintenanceResource {
	static final transient Logger log = Logger.getLogger(MaintenanceResource.class);
	
	private static final int REWARDS_CONVERSION_FLAG = 0;
	private static final int VPO_REWARD_ROLLUP_FLAG = 1;
	private static final int PINGCOUNT_FLAG = 2;

	MaintenanceRepository maintenanceRepository = null;
	AuthenticationHelper authenticationHelper = null;
	
	public MaintenanceResource() {
		maintenanceRepository = new MaintenanceRepository();
		authenticationHelper = new AuthenticationHelper();
	}
	
	/*
	 * fetches all carriers for of company
	 * 
	 * @param String companyCode
	 * @param Integer companyId
	 * @return list of carrier
	 * @url http://localhost:8080/tms-api/settings/carriers/OPENPT
	 */
	@GET
	@Path("carriers/{companyId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Carrier> getShipperCarriers(@HeaderParam("companyCode") String companyCode, 
			@PathParam("companyId") Integer companyId) {
		List<Carrier> returnVal = null;
		try {
			returnVal = maintenanceRepository.getShipperCarriers(companyCode);
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.debug("Done");
		}
		return returnVal;
	}
	
	
	/*
	 * fetches all reward configuration of company
	 * 
	 * @param String companyCode
	 * @param Integer companyId
	 * @return list of reward configurations
	 * @url http://localhost:8080/tms-api/settings/rewards/OPENPT
	 */
	@GET
	@Path("rewards/{companyId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<RewardsPointsConfig> getRewardsConfig(@HeaderParam("companyCode") String companyCode, 
			@PathParam("companyId") Integer companyId) {
		List<RewardsPointsConfig> returnVal = null;
		try {
			returnVal = maintenanceRepository.getRewardsConfig(companyCode);
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.debug("Done");
		}
		return returnVal;
	}
	
	/*
	 * creates new reward configuration based on configFlag: rewardsConversionFlag, vpoRewardRollupFLag, or pingcountFlag
	 * 
	 * @param String currentUserName
	 * @param String companyCode
	 * @param Integer configFlag
	 * @param RewardsPointsConfig rewardsPointsConfig
	 * @return Response on creation status
	 * @url http://localhost:8080/tms-api/settings/rewards/create/0
	 */
	@POST
	@Path("rewards/create/{configFlag}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response createRewardsPointsConfig(@HeaderParam("currentUserName") String currentUserName, 
			@HeaderParam("companyCode") String companyCode, 
			@PathParam("configFlag") Integer configFlag, 
			RewardsPointsConfig rewardsPointsConfig){
		try{			
			RewardsPointsConfig rewardsConfigCheck = null;
			switch(configFlag) {
				case REWARDS_CONVERSION_FLAG:
					rewardsConfigCheck = maintenanceRepository.findRewardsConfigByFields(rewardsPointsConfig);				
					break;
					
				case VPO_REWARD_ROLLUP_FLAG:
					rewardsConfigCheck = maintenanceRepository.findVPORewardRollupByFields(rewardsPointsConfig);
					break;
					
				case PINGCOUNT_FLAG:
					rewardsConfigCheck = maintenanceRepository.findPingCountByFields(rewardsPointsConfig);
					break;					
			}			
			if(rewardsConfigCheck != null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0,"Configuration item already exists")).build();
			
			Integer latest = maintenanceRepository.getLatestSeqNbr(configFlag);
			
			rewardsPointsConfig.setId(++latest);
			rewardsPointsConfig.setShipperCd(companyCode);

			maintenanceRepository.createRewardConfig(currentUserName,rewardsPointsConfig,configFlag);
			return Response.ok(DataResultHelper.createResponseJsonObject(1,"Creating configuration item successful")).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(1,"Creating configuration item failed")).build();
	}
	
	
	/*
	 * updates existing reward configuration
	 * 
	 * @param String currentUserName
	 * @param RewardsPointsConfig rewardsConversionData
	 * @return returns update reward configuration
	 * @url http://localhost:8080/tms-api/settings/rewards/update
	 */
	@POST
	@Path("rewards/update")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateRewardsPointsConfig(@HeaderParam("currentUserName") String currentUserName, 
			RewardsPointsConfig rewardsConversionData){
		try{
			Integer rewardCfgId = rewardsConversionData.getId();
			Integer points = Integer.valueOf(rewardsConversionData.getPoints());

			RewardsPointsConfig rewardsPointsConfig = maintenanceRepository.findRewardsConfigById(rewardCfgId);
			
			if(rewardsPointsConfig== null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "Configuration item not found")).build();
		
			Integer st = maintenanceRepository.updateRewardConfig(currentUserName,rewardCfgId,rewardsConversionData);
			
			if(st==1){
				rewardsConversionData.setPoints(points);
			}
			return Response.ok(rewardsConversionData).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(1,"Updating configuration item failed")).build();
	}
	
	/*
	 * deletes new reward configuration (hard delete)
	 * 
	 * @param RewardsPointsConfig rewardsPointsConfig
	 * @return Response on deletion status
	 * @url http://localhost:8080/tms-api/settings/rewards/delete
	 */
	@POST
	@Path("rewards/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Response deleteRewardsPointsConfig(RewardsPointsConfig rewardsPointsConfig){
		try{
			Integer rewardCfgId = rewardsPointsConfig.getId();
			
			rewardsPointsConfig = maintenanceRepository.findRewardsConfigById(rewardCfgId);
			
			if(rewardsPointsConfig == null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "Configuration item not found")).build();
			
			maintenanceRepository.deleteRewardConfig(rewardCfgId);
			return Response.ok(DataResultHelper.createResponseJsonObject(1,"Deleting configuration item successful")).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(1,"Deleting configuration item failed")).build();
	}
	/**
	 * gets rewards value by currency of company user
	 * 
	 * @param countryCd
	 * @param companyId
	 * @return json response data of rewards_value
	 * @url http://localhost:8080/tms-api/settings/rewards/rewardsValue/224
	 */
	@GET
	@Path("rewards/rewardsValue/{companyId}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getRewardsValue(@HeaderParam("countryCd") String countryCd, 
			@PathParam("companyId") Integer companyId){
		try{
			List<Map<String,Object>> dataResult = maintenanceRepository.getRewardsValue(countryCd);
			
			JsonArrayBuilder JSONresponse = DataResultHelper.parseDateResultToJSON(dataResult);
			
			return Response.ok(JSONresponse.build().toString()).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.debug("Done");
		}
	}
	
	/*
	 * fetches all vpo reward rollup of company
	 * 
	 * @param Integer companyId
	 * @return list of reward configurations
	 * @url http://localhost:8080/tms-api/settings/rewards/OPENPT
	 */
	@GET
	@Path("rewards/vpoRewardRollup/{companyId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<RewardsPointsConfig> getVPORewardsRollup(@HeaderParam("companyCode") String companyCode, 
			@PathParam("companyId") Integer companyId) {
		List<RewardsPointsConfig> returnVal = null;
		try {
			returnVal = maintenanceRepository.getVPORewardsRollup(companyCode);
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.debug("Done");
		}
		return returnVal;
	}

	
	/*
	 * updates existing VPO reward rollup
	 * 
	 * @param RewardsPointsConfig vpoRewardRollup
	 * @return returns update VPO reward rollup
	 * @url http://localhost:8080/tms-api/settings/rewards/vpoRewardRollup/update
	 */
	@POST
	@Path("rewards/vpoRewardRollup/update")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateVPORewardRollup(@HeaderParam("currentUserName") String currentUserName, 
			RewardsPointsConfig vpoRewardRollup){
		try{
			Integer rewardCfgId = vpoRewardRollup.getId();
			maintenanceRepository.updateVPORewardRollup(currentUserName, rewardCfgId, vpoRewardRollup);		
			return Response.ok(vpoRewardRollup).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(1,"Updating configuration item failed")).build();
	}
	
	/*
	 * deletes VPO reward rollup (hard delete)
	 * 
	 * @param RewardsPointsConfig rewardsPointsConfig
	 * @return Response on deletion status
	 * @url http://localhost:8080/tms-api/settings/rewards/vpoRewardRollup/delete
	 */
	@POST
	@Path("rewards/vpoRewardRollup/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Response deleteVPORewardRollup(RewardsPointsConfig rewardsPointsConfig){
		try{
			Integer rewardCfgId = rewardsPointsConfig.getId();
			
			rewardsPointsConfig = maintenanceRepository.findVPORewardRollupById(rewardCfgId);
			
			if(rewardsPointsConfig == null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "Configuration item not found")).build();
			
			maintenanceRepository.deleteVPORewardRollup(rewardCfgId);
			return Response.ok(DataResultHelper.createResponseJsonObject(1,"Deleting configuration item successful")).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(1,"Deleting configuration item failed")).build();
	}
	
	/*
	 * fetches all reward configuration of company
	 * 
	 * @param Integer companyId
	 * @return list of reward configurations
	 * @url http://localhost:8080/tms-api/settings/rewards/pingcount/OPENPT
	 */
	@GET
	@Path("rewards/pingcount/{companyId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<RewardsPointsConfig> getPingcounts(@HeaderParam("companyCode") String companyCode, 
			@PathParam("companyId") Integer companyId) {
		List<RewardsPointsConfig> returnVal = null;
		try {
			returnVal = maintenanceRepository.getPingcounts(companyCode);
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.debug("Done");
		}
		return returnVal;
	}
	
	/*
	 * updates existing pingcount
	 * 
	 * @param RewardsPointsConfig rewardsPointsConfig
	 * @return returns update reward configuration
	 * @url http://localhost:8080/tms-api/settings/rewards/pingcount/update
	 */
	@POST
	@Path("rewards/pingcount/update")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updatePingcount(@HeaderParam("currentUserName") String currentUserName, 
			RewardsPointsConfig rewardsPointsConfig){
		try{
			Integer rewardCfgId = rewardsPointsConfig.getId();
			Integer pingcount = Integer.valueOf(rewardsPointsConfig.getPingcount());
			
			RewardsPointsConfig rewardItem = maintenanceRepository.findPingcountById(rewardCfgId);
			
			if(rewardItem == null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "Configuration item not found")).build();
			
			Integer st = maintenanceRepository.updatePingcount(currentUserName,rewardCfgId, rewardsPointsConfig);
			
			if(st==1){
				rewardsPointsConfig.setPingcount(pingcount);
			}
			return Response.ok(rewardsPointsConfig).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(1,"Updating configuration item failed")).build();
	}
	
	/*
	 * deletes pingcount (hard delete)
	 * 
	 * @param RewardsPointsConfig rewardsPointsConfig
	 * @return Response on deletion status
	 * @url http://localhost:8080/tms-api/settings/rewards/pingcount/delete
	 */
	@POST
	@Path("rewards/pingcount/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Response deletePingcount(RewardsPointsConfig rewardsPointsConfig){
		try{
			Integer rewardCfgId = rewardsPointsConfig.getId();
			
			rewardsPointsConfig = maintenanceRepository.findPingcountById(rewardCfgId);
			
			if(rewardsPointsConfig == null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "Configuration item not found")).build();
			
			maintenanceRepository.deletePingcount(rewardCfgId);
			return Response.ok(DataResultHelper.createResponseJsonObject(1,"Deleting configuration item successful")).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(1,"Deleting configuration item failed")).build();
	}
	
	/**
	 * gets list of payment methods and activation status
	 * 
	 * @return json response of payment method list
	 * @url http://localhost:8080/tms-api/settings/rewards/paymentMethodStatus
	 */
	@GET
	@Path("rewards/paymentMethodStatus")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getPaymentMethodStatus() {
		try{
			List<Map<String,Object>> dataResult = maintenanceRepository.getPaymentMethodStatus();
			JsonArrayBuilder JSONresponse = DataResultHelper.parseDateResultToJSON(dataResult);
			return Response.ok(JSONresponse.build().toString()).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.debug("Done");
		}
	}
	

	
	/**
	 * gets list of Long Haul rewards basing from current user's country code
	 * 
	 * @param countryCd 
	 * @return longHaulList
	 */
	@GET 
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/rewards/longHaul/{countryCd}")
	public List<LongHaulReward> getLongHaulList(@PathParam("countryCd") String countryCd) {
		List<LongHaulReward> longHaulList = new ArrayList<LongHaulReward>();
		try {
			longHaulList = maintenanceRepository.getLongHaulRewards(countryCd);			
		}catch(Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return longHaulList;		
	}	
	
	/**
	 * create new long haul reward entry
	 * 
	 * @param newLongHaul
	 * @return status response
	 */
	@POST
	@Path("rewards/longHaul/create")
	@Produces({MediaType.APPLICATION_JSON})
	public Response createLongHaul(LongHaulReward newLongHaul) {
		try {
			LongHaulReward longHaulCheck = maintenanceRepository.findLongHaulByFields(newLongHaul);
			if(longHaulCheck != null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0,"Long Haul item already exists")).build();
			
			maintenanceRepository.createLongHaul(newLongHaul);
			return Response.ok(DataResultHelper.createResponseJsonObject(1,"Creating new long haul successful")).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(0,"Creating new long haul failed")).build();
	}
	
	/**
	 * updates selected long haul reward item
	 * 
	 * @param longHaulreward
	 * @return response status
	 */
	@POST
	@Path("rewards/longHaul/update")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateLongHaul(LongHaulReward longHaulreward) {
		try {
			LongHaulReward longHaulCheck = maintenanceRepository.findLongHaulByFields(longHaulreward);
			if(longHaulCheck == null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "Long Haul item not found")).build();
			
			maintenanceRepository.updateLongHaulItem(longHaulreward);
			return Response.ok(DataResultHelper.createResponseJsonObject(0,"Updating long haul successful")).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(0,"Updating long haul failed")).build();
	}
	
	/**
	 * removes long haul reward item (hard delete)
	 * 
	 * @param longHaulreward 
	 * @return response status
	 */
	@POST
	@Path("rewards/longHaul/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Response deleteLongHaul(LongHaulReward longHaulreward) {		
		try {
			LongHaulReward longHaulCheck = maintenanceRepository.findLongHaulByFields(longHaulreward);
			if(longHaulCheck == null)
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "Long Haul item not found")).build();
			
			maintenanceRepository.deleteLongHaulItem(longHaulreward);
			return Response.ok(DataResultHelper.createResponseJsonObject(1,"Deleting long haul successful")).build();
		}catch(Exception e){
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(0,"Deleting long haul failed")).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("rewards/longHaul/distance/{origin}/{dest}")
	public Response getDistanceDuration(@PathParam("origin") String origin,
			@PathParam("dest") String dest) {
		
		try {
			String locationApiUrl = maintenanceRepository.getLocationApiUrl(); 
			
			ClientConfig clientConfig = new ClientConfig();
	        Client client = ClientBuilder.newClient(clientConfig);
	        WebTarget target = client.target(locationApiUrl);
	      
	        return target.queryParam("from", origin).queryParam("to", dest).request(MediaType.APPLICATION_JSON).get();

		}catch(Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(0,"Calculating distance and duration failed.")).build();
	}
	
}
