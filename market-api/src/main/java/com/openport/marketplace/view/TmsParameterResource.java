package com.openport.marketplace.view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.openport.marketplace.repository.TmsParameterRepository;

@Path("tmsParameters")
public class TmsParameterResource {

    static final transient Logger log = Logger.getLogger(TmsParameterResource.class);

    private static final String CURRENT_VERSION = "1.0"; 
    private static final String CURRENT_UAT_VERSION = "1.0";

    @GET
    @Path("accounts/defaultParameters") // http://localhost:8080/market-api/tmsParameters/accounts/defaultParameters
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getAllDefaultParameters() {
	log.info("tmsParameters/accounts/defaultParameters");
	String jsonData = TmsParameterRepository.getDefaultParameters();
	return Response.ok(jsonData).build();
    }

    @GET
    @Path("accounts/roles") // http://localhost:8080/market-api/tmsParameters/accounts/roles
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getRoles() {
	log.info("tmsParameters/accounts/roles");
	String jsonArrayOfRoles = TmsParameterRepository.getRoles();
	return Response.ok(jsonArrayOfRoles).build();
    }

    @POST
    @Path("accounts/generateTableQueries")
    @Consumes("application/json,application/vnd.openport.market.v1+json")
    @Produces({ MediaType.APPLICATION_JSON })
	public void generateTableQueries(String tableParameters) throws JsonParseException, JsonMappingException, IOException {
		log.info("tmsParameters/accounts/generateTableQueries");
		
		ObjectMapper mapper = new ObjectMapper();
		
		List<String> ArrayOfTableQueries = TmsParameterRepository.generateTableQueries(
			mapper.readValue(tableParameters, new TypeReference<List<Map<String, Object>>>() {
			}));
		
		TmsParameterRepository.insertQueries(ArrayOfTableQueries);
	}

    @POST
    @Path("accounts/generateQueries")
    @Produces({ MediaType.APPLICATION_JSON })
    public void generateQueries(@FormDataParam("userInfo") String userInfo, @FormDataParam("role") String role, @FormDataParam("userRole") String userRole) throws JsonParseException, JsonMappingException, IOException, SQLException {
		log.info("accounts/generateQueries");
		
		List<String> additionalQueriesList = TmsParameterRepository.generateAdditionalQueries(userInfo, role, userRole);
		 TmsParameterRepository.insertQueries(additionalQueriesList);
	
		ObjectMapper mapper = new ObjectMapper();
		
		Map<String, Object> userInfoMap = mapper.readValue(userInfo, new TypeReference<Map<String, Object>>() {
		});
		
		List<String> userRoleQueries = mapper.readValue(userRole, new TypeReference<List<String>>() {
		});
		
		boolean isAnalytics = false;
		Iterator<String> iter = userRoleQueries.iterator();
		while (iter.hasNext()) {
			if(iter.next().trim().equalsIgnoreCase("Analytics")) {
				isAnalytics = true;
			}
		}
		
		TmsParameterRepository tmsParameterRepository = new TmsParameterRepository();
		if (isAnalytics) {
			tmsParameterRepository.addAnalytics((String) userInfoMap.get("PARTNER_CD"));
		} else {
			tmsParameterRepository.removeAnalytics((String) userInfoMap.get("PARTNER_CD"));
		}
		
		long key = tmsParameterRepository.setShipper((String) userInfoMap.get("PARTNER_CD"), (String) userInfoMap.get("COMPANY_NAME"));
		tmsParameterRepository.setShipperDashboard(key);
    }
    
    @POST
    @Path("accounts/uploader")
	public void setUploader(@FormDataParam("parentPartnerCode") String parentPartnerCode, @FormDataParam("partnerCode") String partnerCode) {
		log.info("tmsParameters/accounts/uploader");
		
		TmsParameterRepository tmsParameterRepository = new TmsParameterRepository();
		tmsParameterRepository.setUploader(parentPartnerCode, partnerCode);
	}
}
