package com.openport.marketplace.view;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.openport.marketplace.json.AccessorialCode;
import com.openport.marketplace.json.Bid;
import com.openport.marketplace.json.Carrier;
import com.openport.marketplace.json.CarrierAccessorial;
import com.openport.marketplace.json.CarrierRate;
import com.openport.marketplace.json.CarrierTransportationLeg;
import com.openport.marketplace.json.CommodityCode;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.CompanyContractTenderLane;
import com.openport.marketplace.json.Contract;
import com.openport.marketplace.json.CorporateAddress;
import com.openport.marketplace.json.Cost;
import com.openport.marketplace.json.Country;
import com.openport.marketplace.json.Equipment;
import com.openport.marketplace.json.Lane;
import com.openport.marketplace.json.PoHeader;
import com.openport.marketplace.json.RatesWorkArea;
import com.openport.marketplace.json.Revenue;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.SpotRate;
import com.openport.marketplace.json.SubmittedArea;
import com.openport.marketplace.parser.DateSimpleFormat;
import com.openport.marketplace.parser.SortObject;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.marketplace.repository.DbConfigODS;
import com.openport.marketplace.repository.DbConfigRater;
import com.openport.marketplace.repository.DbConfigTMS;
import com.openport.marketplace.repository.DbConfigTmsUtf8;
import com.openport.util.DateUtil;
import com.openport.util.db.ResultSetMapper;

@Path("ratermgr") // http://localhost:8080/market-api/truckers
public class RaterResource {
	static final transient Logger log = Logger.getLogger(RaterResource.class);

	private final String LC_STATUS_CD = "SUD";
	private final String LC_STATUS_DESC = "Shipment Updated";
	private String rateCurrency = "USD";

	@GET // http://localhost:8080/market-api/truckers
	@Path("equipment/{id}/{countryCd}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Equipment> fetchEquipment(@PathParam("id") Integer companyId,
			@PathParam("countryCd") String countryCd) {
		log.info("Country " + countryCd);

		return fetchCompanyEquipment(countryCd);
	}

	@GET // http://localhost:8080/market-api/truckers
	@Path("bidlane/{groupId}/{equipmentId}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyContractTenderLane> fetchBidLane(@PathParam("groupId") Integer groupId,
			@PathParam("equipmentId") Integer equipmentId) {
		return fetchContractBid(groupId, equipmentId);
	}

	@POST
	@Path("bidrank")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyContractTenderLane> updateBidRank(List<CompanyContractTenderLane> bids) {
		Set<Integer> ids = new HashSet<>();
		Set<Integer> rateIds = new HashSet();
		Set<Integer> finalIds = new HashSet();
		int maxRateIds = 0;
		int iAwarded = 0;
		for (CompanyContractTenderLane c : bids) {
			
			if (c.getBidRank() == 1) {
				iAwarded=c.getCompanyContractTenderLaneId();
				ids.add(c.getCompanyContractTenderLaneId());
			}else {
				rateIds.add(c.getCompanyContractTenderLaneId());
			}
			if (postBidRank(c) == 0) {
				return null;
			}
		}
		for (Integer i : ids) {
			if (do_MessageQueueByLaneId(i, "A") == 0) {
				return null;
			}else {
				finalIds.add(iAwarded);
				for(Integer s : rateIds) {
					finalIds.add(s);
					
				}
				
			}
		}
		if(finalIds.size() > 0) {
			do_mp_contract_rate_to_rates(finalIds);
		}
		 
		return bids;
	}

	@POST
	@Path("saverank")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyContractTenderLane> saveBidRank(List<CompanyContractTenderLane> bids) {
		Set<Integer> ids = new HashSet<>();
		for (CompanyContractTenderLane c : bids) {
			// if(c.getBidRank() == 1) {
			// ids.add(c.getCompanyContractTenderLaneId());

			if (saveBidRank(c) == 0) {
				return null;
			}
		}
		/*
		 * for(Integer i : ids) { if(do_MessageQueueByLaneId(i, "A") == 0 ) {
		 * return null; } }
		 */
		return bids;
	}

	@GET // http://localhost:8080/market-api/truckers
	@Path("accessorials/{parentCd}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<AccessorialCode> fetchAccessorial(@PathParam("parentCd") String parentCd) {
		log.info("parent " + parentCd);
		List<AccessorialCode> accList = new ArrayList<AccessorialCode>();
		try {

			String sql = "select distinct accessorial_cd,description,accessorial_type from accessorial_code where parent_partner_cd is not null ";
			// if(customerCd != null) {
			// sql+="\n and customer_cd='"+customerCd+"'";
			// }
			if (parentCd != null) {
				sql += "\n and parent_partner_cd='" + parentCd + "'";
			}
			sql += "\norder by description";
			log.info("SQL" + sql);
			try (java.sql.Connection connection = DbConfigRater.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<AccessorialCode> driverRawMapper = new ResultSetMapper<AccessorialCode>();
					List<AccessorialCode> pojoList = driverRawMapper.mapResultSetToObject(rs, AccessorialCode.class);
					// List<Driver> nw_pojoList = new LinkedList<Driver>();
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList;
					}

				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}

	@GET
	@Path("carrierprofile") // http://localhost:8080/market-api/logistics/byproductcode
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Carrier> validateCarrier(@QueryParam("parentPartnerCd") String parentCd,
			@QueryParam("carrierCd") String carrierCd) {
		log.info(parentCd + " " + carrierCd);
		String sql = "select distinct carrier_cd,carrier_name from carrier_profile where parent_partner_cd=? and carrier_cd=? order by carrier_name";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, parentCd);
			pstmt.setString(2, carrierCd);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Carrier> driverRawMapper = new ResultSetMapper<Carrier>();
				List<Carrier> pojoList = driverRawMapper.mapResultSetToObject(rs, Carrier.class);
				// List<Driver> nw_pojoList = new LinkedList<Driver>();
				if (pojoList != null && !pojoList.isEmpty()) {
					return pojoList;
				}

			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	@GET
	@Path("tenderIds") // http://localhost:8080/market-api/logistics/byproductcode
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyContractTenderLane> getTenderIds(@QueryParam("ids") List ids) {
		log.info(ids);
		return fetchContractBidByGroupId(ids);

	}

	/*
	 * @GET
	 * 
	 * @Path("contractrate") //
	 * http://localhost:8080/market-api/logistics/byproductcode
	 * 
	 * @Produces({ MediaType.APPLICATION_JSON }) public Lane
	 * validateContractRate(@QueryParam("rate") RatesWorkArea rwa) { return
	 * validateSingleArea(rwa);
	 * 
	 * }
	 */
	@GET // http://localhost:8080/market-api/truckers
	@Path("carrierprofile/{partnerCd}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Carrier> fetchCarrierProfile(@PathParam("partnerCd") String partnerCd) {

		String sql = "select distinct carrier_cd,carrier_name from carrier_profile where parent_partner_cd=? order by carrier_name";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, partnerCd);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Carrier> driverRawMapper = new ResultSetMapper<Carrier>();
				List<Carrier> pojoList = driverRawMapper.mapResultSetToObject(rs, Carrier.class);
				// List<Driver> nw_pojoList = new LinkedList<Driver>();
				if (pojoList != null && !pojoList.isEmpty()) {
					return pojoList;
				}

			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	@DELETE // http://localhost:8080/market-api/logistics/trackTrace/1208714
	@Path("deletelane")

	public Integer deleteLanes(@QueryParam("ids") List ids) {
		log.info("should go here" + ids.get(0));
		return deleteWorkAreas(ids);
	}

	@DELETE // http://localhost:8080/market-api/logistics/trackTrace/1208714
	@Path("deletetender")

	public Integer deleteTenderAreas(@QueryParam("ids") List<String> ids) {
		if (withdrawTenderByGroupId(ids) == 1) {
			for (String i : ids) {
				if (do_BidMessageQueue(Integer.valueOf(i)) == 0) {
					return 0;
				}
			}
		}
		return 1;
	}

	@GET // http://localhost:8080/market-api/truckers
	@Path("country")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Country> getAllCountry() {
		List<Country> countryList = new ArrayList<Country>();
		try {

			String sql = "SELECT country_cd,country_name FROM country order by country_name";
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Country> driverRawMapper = new ResultSetMapper<Country>();
					List<Country> pojoList = driverRawMapper.mapResultSetToObject(rs, Country.class);
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {

		}
		return countryList;
	}

	@GET // http://localhost:8080/market-api/truckers
	@Path("shipment/{probillNbr}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public SpotRate fetchShipmentByPro(@PathParam("probillNbr") String probill) {
		try {

			String sql = "select a.po_nbr,a.release_nbr,a.PARENT_PARTNER_CD,a.partner_cd,a.SHIPPER_CD,a.CUSTOMER_CD,a.EQUIPMENT_SHIPMENT_SEQ_NBR,"
					+ "(SELECT CASE WHEN COUNT(*) > 0 THEN (SELECT  SUM(aa.TOTAL_WT) FROM tms_utf8_db.po_header AS aa WHERE aa.PO_NBR IN (SELECT  ACTUAL_PO_NBR FROM tms_utf8_db.po_line_item WHERE po_nbr = a.PO_NBR AND a.PO_NBR <> '') "
					+ "AND aa.PO_TYPE_CD = 'LPO' AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD AND aa.PARTNER_CD = a.PARTNER_CD AND aa.CUSTOMER_CD = a.CUSTOMER_CD AND aa.SHIPPER_CD = a.SHIPPER_CD) ELSE a.TOTAL_WT END FROM tms_utf8_db.po_line_item li "
					+ "WHERE a.PO_NBR = li.po_nbr AND a.PO_NBR <> '' AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD AND a.PARTNER_CD = li.PARTNER_CD AND a.SHIPPER_CD = li.SHIPPER_CD AND a.CUSTOMER_CD = li.CUSTOMER_CD AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS total_wt,"
					+ "a.WT_UOM,b.EQUIPMENT_TYPE_CD,b.EQUIPMENT_QTY,b.EQUIPMENT_DESC_TXT,\n"
					+ " c.REFERENCE_TYPE_CD,c.LOCATION_NAME,c.LINE_1_ADDR,c.LINE_2_ADDR,c.CITY_NAME,c.STATE_NAME,c.COUNTRY_CD,c.POSTAL_CD\n"
					+ " from po_header a left outer join po_equipment_requirement b on a.PO_NBR = b.po_nbr and a.RELEASE_NBR = b.release_nbr\n"
					+ " and a.PARENT_PARTNER_CD = b.parent_partner_cd and a.PARTNER_CD = b.partner_cd left outer join po_reference_info c\n"
					+ " on a.PO_NBR = c.po_nbr and a.RELEASE_NBR = c.release_nbr \n"
					+ " and a.PARENT_PARTNER_CD = c.parent_partner_cd and a.PARTNER_CD = c.partner_cd and c.REFERENCE_TYPE_CD  in ('Shipper','Consignee','Pickup','Delivery')\n"
					+ " where a.PRO_BILL_NBR=?";
			log.info("SQL " + sql);
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, probill);
				SpotRate shipper = null;
				SpotRate consignee = null;
				SpotRate pickup = null;
				SpotRate delivery = null;
				SpotRate main = null;
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						SpotRate sp = new SpotRate();
						sp.setPoNbr(rs.getString(1));
						sp.setReleaseNbr(rs.getString(2));
						sp.setParentPartnerCd(rs.getString(3));
						sp.setPartnerCd(rs.getString(4));
						sp.setShipperCd(rs.getString(5));
						sp.setCustomerCd(rs.getString(6));
						sp.setEquipmentShipmentSeqNbr(rs.getInt(7));
						sp.setWeight(rs.getDouble(8));
						sp.setWtUom(rs.getString(9));
						sp.setEquipmentType(rs.getString(10));
						sp.setNoOfEquipment(rs.getInt(11));
						sp.setEquipmentDesc(rs.getString(12));
						sp.setLocationType(rs.getString(13));
						sp.setLocationName(rs.getString(14));
						sp.setLocationAddr1(rs.getString(15));
						sp.setLocationAddr2(rs.getString(16));
						sp.setLocationCity(rs.getString(17));
						sp.setLocationState(rs.getString(18));
						sp.setLocationCountry(rs.getString(19));
						sp.setLocationPostal(rs.getString(20));
						log.info("SPOT " + sp.toString());
						main = sp;
						if (sp.getLocationType() != null && sp.getLocationType().equalsIgnoreCase("Shipper")) {
							shipper = sp;
						} else if (sp.getLocationType() != null && sp.getLocationType().equalsIgnoreCase("Consignee")) {
							consignee = sp;
						} else if (sp.getLocationType() != null && sp.getLocationType().equalsIgnoreCase("Pickup")) {
							pickup = sp;
						} else if (sp.getLocationType() != null && sp.getLocationType().equalsIgnoreCase("Delivery")) {
							delivery = sp;
						}
					}
					if (pickup != null) {

						main.setOriginAddress(
								trimString(pickup.getLocationAddr1()) + trimString(pickup.getLocationAddr2()));
						main.setOriginName(trimString(pickup.getLocationName()));
						main.setOriginCity(trimString(pickup.getLocationCity()));
						main.setOriginState(trimString(pickup.getLocationState()));
						main.setOriginCountry(trimString(pickup.getLocationCountry()));
						main.setOriginPostal(trimString(pickup.getLocationPostal()));

					} else if (shipper != null) {
						main.setOriginAddress(
								trimString(shipper.getLocationAddr1()) + trimString(shipper.getLocationAddr2()));
						main.setOriginName(trimString(shipper.getLocationName()));
						main.setOriginCity(trimString(shipper.getLocationCity()));
						main.setOriginState(trimString(shipper.getLocationState()));
						main.setOriginCountry(trimString(shipper.getLocationCountry()));
						main.setOriginPostal(trimString(shipper.getLocationPostal()));
					}

					if (delivery != null) {
						main.setDestAddress(
								trimString(delivery.getLocationAddr1()) + trimString(delivery.getLocationAddr2()));
						main.setDestName(trimString(delivery.getLocationName()));
						main.setDestCity(trimString(delivery.getLocationCity()));
						main.setDestState(trimString(delivery.getLocationState()));
						main.setDestCountry(trimString(delivery.getLocationCountry()));
						main.setDestPostal(trimString(delivery.getLocationPostal()));
					} else if (consignee != null) {
						main.setDestAddress(
								trimString(consignee.getLocationAddr1()) + trimString(consignee.getLocationAddr2()));
						main.setDestName(trimString(consignee.getLocationName()));
						main.setDestCity(trimString(consignee.getLocationCity()));
						main.setDestState(trimString(consignee.getLocationState()));
						main.setDestCountry(trimString(consignee.getLocationCountry()));
						main.setDestPostal(trimString(consignee.getLocationPostal()));
					}
					log.info("main is " + main.toString());
					return main;
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}

	private String trimString(String s) {
		// TODO Auto-generated method stub
		if (s == null) {
			return "";
		} else if (s.isEmpty()) {
			return "";
		} else {
			return s.replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
		}
	}

	private String trimStringV2(String s) {
		// TODO Auto-generated method stub
		if (s == null) {
			return "";
		} else if (s.isEmpty()) {
			return "";
		} else {
			return s.replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim() + ",";
		}
	}

	private AccessorialCode fetchAccessorialByCode(String code, String parentCd) {

		List<AccessorialCode> accList = new ArrayList<AccessorialCode>();
		try {

			String sql = "select distinct accessorial_cd,description,accessorial_type from accessorial_code where parent_partner_cd is not null ";

			if (parentCd != null) {
				sql += "\n and parent_partner_cd='" + parentCd + "'";
			}
			sql += "\n and accessorial_cd=?";
			sql += "\norder by description";
			log.info("SQL" + sql);
			try (java.sql.Connection connection = DbConfigRater.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, code);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<AccessorialCode> driverRawMapper = new ResultSetMapper<AccessorialCode>();
					List<AccessorialCode> pojoList = driverRawMapper.mapResultSetToObject(rs, AccessorialCode.class);
					// List<Driver> nw_pojoList = new LinkedList<Driver>();
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList.get(0);
					}

				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}

	@POST // http://localhost:8080/market-api/trucker
	@Path("createSingleWorkArea") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public RatesWorkArea createWorkArea(RatesWorkArea rwa) {
		return saveSingleArea(rwa, "P");
	}

	@POST // http://localhost:8080/market-api/trucker
	@Path("submitWorkArea") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<RatesWorkArea> submitWorkArea(List<RatesWorkArea> areas) {
		List<RatesWorkArea> rList = new LinkedList<>();
		for (RatesWorkArea rwa : areas) {
			log.info("Country " + rwa.getCountry());
			RatesWorkArea rw = saveWorkArea(rwa, "U");
			if (rw == null) {
				return null;
			} else {
				rList.add(rw);
			}
		}
		return rList;

	}

	private Integer validateLane(Lane ln) {
		int laneId = getLaneID(ln);
		if (laneId == 0) {
			return -1;
		}
		return laneId;
	}

	private Lane validateSingleArea(RatesWorkArea rwa) {
		try {

			// create Lane Id
			List<Lane> ll = new LinkedList<>();
			Lane ln = rwa.getLane();

			int laneId = getLaneID(rwa.getLane());
			if (laneId == 0) {
				laneId = -1;
			}
			List<Equipment> nEqptSet = new LinkedList();
			List<Equipment> eSet = ln.getEquipmentList();
			for (Equipment e : eSet) {
				if (e.getIsSelected()) {
					// validate
					log.info("lane id :" + laneId + "lane " + ln);
				}
			}
			return ln;

		} catch (Exception e) {
			log.info("error", e);
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);

		}
		return null;
	}

	private boolean isContractExist(String startDt, String endDt, Integer companyId, Integer laneId,
			Integer equipmentTypeId, Integer carrierId, String commodity) {
		String sql = "select company_contract_tender_lane_id from company_contract_tender_lane where STATUS_CD != 'W' AND lane_id=? and COMMODITY_CD=? and shipper_company_id=? "
				+
				// " and equipment_type_id=? and carrier_company_id=? and
				// rate_effective_dt between '"+ startDt + "' and '" + endDt +
				// "'";
				"  and equipment_type_id=? and carrier_company_id=? "+
				" AND ((('"+startDt+"' > rate_effective_dt AND '"+startDt+"' < rate_termination_dt) OR ('"+endDt+"' > rate_effective_dt AND '"+endDt+"' < rate_termination_dt)) "+
				" OR ((rate_effective_dt > '"+startDt+"'  AND rate_effective_dt < '"+endDt+"') OR (rate_termination_dt > '"+startDt+"' AND  rate_termination_dt < '"+endDt+"')) "+
				" OR ( DATE_FORMAT(rate_effective_dt,'%Y-%m-%d') IN ('"+startDt+"','"+endDt+"') OR  DATE_FORMAT(rate_termination_dt,'%Y-%m-%d') IN ('"+startDt+"','"+endDt+"') )) ";
				
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, laneId);
			pstmt.setString(2, commodity);
			pstmt.setInt(3, companyId);
			pstmt.setInt(4, equipmentTypeId);
			pstmt.setInt(5, carrierId);

			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					return true;
				}
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return false;
	}

	private RatesWorkArea saveSingleArea(RatesWorkArea rwa, String status) {

		Set<Integer> seqSet = new HashSet<>();
		Integer groupTenderId = insertDbSeqTender();
		List<CompanyContractTenderLane> tenderLst = new LinkedList<>();
		try {

			// create Lane Id
			List<Lane> ll = new LinkedList<>();
			Lane ln = rwa.getLane();

			int groupId = ln.getGroupTenderId();

			deleteWorkArea(groupId);
			int laneId = getLaneID(rwa.getLane());
			if (laneId == 0) {
				laneId = insertLane(rwa.getLane());
			}

			// validate entries
			List<Equipment> nEqptSet = new LinkedList();
			List<Contract> failedList = new LinkedList<>();
			List<Equipment> eSet = ln.getEquipmentList();
			for (Equipment e : eSet) {
				if (e.getIsSelected()) {

					CompanyContractTenderLane cctl = ln.getCompanyContractTenderLane();
					cctl.setAccessorialAmt(0.0);
					cctl.setBidRank(0);
					cctl.setCommentTxt("");
					cctl.setCreateDt(new Date());
					cctl.setRateAmt(0.0);
					cctl.setStatusCd(status);
					cctl.setCreateDt(new Date());
					cctl.setUpdateDt(cctl.getCreateDt());

					cctl.setUpdateUserId(cctl.getCreateUserId());

					if (validateCommodityCode(cctl.getCommodityCd()) == null) {
						insertCommodityCd(cctl.getCommodityCd());
					}

					cctl.setLaneId(laneId);

					cctl.setEquipmentTypeId(e.getEquipmentTypeId());
					cctl.setCarrierCompanyId(-1);
					if (groupId < 1 && isContractExist(
							DateSimpleFormat.formatSimpleDate(cctl.getRateEffectiveDt(), "yyyy-MM-dd"),
							DateSimpleFormat.formatSimpleDate(cctl.getRateTerminationDt(), "yyyy-MM-dd"),
							cctl.getShipperCompanyId(), laneId, e.getEquipmentTypeId(), -1, cctl.getCommodityCd())) {

						failedList.add(getContract(rwa, e, cctl));
					} else {
						e.setCompanyContractTenderLaneId(insertCompanyContractTenderLane(cctl, groupTenderId, null));
						e.setGroupTenderId(groupTenderId);
						nEqptSet.add(e);
					}

				}
			}
			ln.setEquipmentList(nEqptSet);
			ln.setGroupTenderId(groupTenderId);

			rwa.setLane(ln);
			ll.add(ln);

			rwa.setLanes(ll);
			if (failedList.size() > 0) {
				rwa.setFailed(failedList);
			}
			return rwa;

		} catch (Exception e) {
			log.info("error", e);
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);

		}
		return null;
	}

	private Contract getContract(RatesWorkArea rwa, Equipment e, CompanyContractTenderLane cctl) {
		// TODO Auto-generated method stub
		Contract cc = new Contract();
		cc.setCommodity(cctl.getCommodityCd());
		cc.setStartDt(DateSimpleFormat.formatSimpleDate(cctl.getRateEffectiveDt(), "yyyy-MM-dd"));
		String origin = trimStringV2(rwa.getLane().getOriginCity()) + trimStringV2(rwa.getLane().getOriginProvince())
				+ trimStringV2(rwa.getLane().getOriginCountry()) + trimStringV2(rwa.getLane().getOriginPostalCd());
		String destination = trimStringV2(rwa.getLane().getDestCity()) + trimStringV2(rwa.getLane().getDestProvince())
				+ trimStringV2(rwa.getLane().getDestCountry()) + trimStringV2(rwa.getLane().getDestPostalCd());

		cc.setEquipment(e.getEquipmentName());
		cc.setOrigin(origin.substring(0, origin.length() - 1));
		cc.setDestination(destination.substring(0, destination.length() - 1));
		return cc;
	}

	private RatesWorkArea saveWorkArea(RatesWorkArea rwa, String status) {
		log.info("area " + rwa);
		Set<Integer> seqSet = new HashSet<>();
		Set<Integer> groupTenderIds = new HashSet<>();
		Map<String, Contract> mapContract = new HashMap<>();
		Integer groupTenderId = 0;
		String sKey = "";
		List<Contract> failedList = new LinkedList<>();
		List<CompanyContractTenderLane> tenderLst = new LinkedList<>();
		try {

			// create Lane Id
			List<Lane> ll = new LinkedList<>();
			for (Lane ln : rwa.getLanes()) {
				groupTenderId = insertDbSeqTender();
				int groupId = ln.getGroupTenderId();

				deleteWorkArea(groupId);
				int laneId = getLaneID(rwa.getLane());
				// List<Carrier> carrierList =
				// getCarriers(rwa.getLane().getDestCountry());
				List<Carrier> carrierList = fetchCarriers(rwa.getCountry());

				if (laneId == 0) {
					laneId = insertLane(rwa.getLane());
				}
				List<Equipment> nEqptSet = new LinkedList();
				List<Equipment> eSet = ln.getEquipmentList();
				for (Equipment e : eSet) {
					if (e.getIsSelected()) {

						CompanyContractTenderLane cctl = ln.getCompanyContractTenderLane();
						cctl.setAccessorialAmt(0.0);
						cctl.setBidRank(0);
						cctl.setCommentTxt("");
						cctl.setCreateDt(new Date());
						cctl.setRateAmt(0.0);
						cctl.setStatusCd(status);
						cctl.setCreateDt(new Date());
						cctl.setUpdateDt(cctl.getCreateDt());

						cctl.setUpdateUserId(cctl.getCreateUserId());

						if (validateCommodityCode(cctl.getCommodityCd()) == null) {
							insertCommodityCd(cctl.getCommodityCd());
						}

						cctl.setLaneId(laneId);

						cctl.setEquipmentTypeId(e.getEquipmentTypeId());
						if (carrierList == null || carrierList.size() == 0) {
							cctl.setCarrierCompanyId(-1);
							log.error("No Carrier to bid for destination country " + rwa.getLane().getDestCountry());
							if (groupId < 1
									&& isContractExist(
											DateSimpleFormat.formatSimpleDate(cctl.getRateEffectiveDt(), "yyyy-MM-dd"),
											DateSimpleFormat.formatSimpleDate(cctl.getRateTerminationDt(),
													"yyyy-MM-dd"),
											cctl.getShipperCompanyId(), laneId, e.getEquipmentTypeId(), -1,
											cctl.getCommodityCd())) {
								Contract c = getContract(rwa, e, cctl);
								mapContract.put(
										"" + cctl.getLaneId() + c.getEquipment() + c.getCommodity() + c.getStartDt(),
										c);
								// failedList.add(getContract(rwa,e,cctl));
							} else {
								e.setCompanyContractTenderLaneId(insertCompanyContractTenderLane(cctl, groupTenderId,
										contractGenerator(cctl.getShipperCompanyId(), 0, groupTenderId,
												cctl.getEquipmentTypeId())));
								groupTenderIds.add(groupTenderId);
								e.setGroupTenderId(groupTenderId);
							}
						} else {
							for (Carrier c : carrierList) {
								cctl.setCarrierCompanyId(c.getCompanyId());
								log.info("" + DateSimpleFormat.formatSimpleDate(cctl.getRateEffectiveDt(), "yyyy-MM-dd")
										+ " " + cctl.getShipperCompanyId() + " " + laneId + " " + e.getEquipmentTypeId()
										+ " " + cctl.getCommodityCd());
								if (groupId < 1 && isContractExist(
										DateSimpleFormat.formatSimpleDate(cctl.getRateEffectiveDt(), "yyyy-MM-dd"),
										DateSimpleFormat.formatSimpleDate(cctl.getRateTerminationDt(), "yyyy-MM-dd"),
										cctl.getShipperCompanyId(), laneId, e.getEquipmentTypeId(), c.getCompanyId(),
										cctl.getCommodityCd())) {
									log.info("went here carrier : " + c.getCarrierCd());

									Contract cx = getContract(rwa, e, cctl);
									sKey = "" + cctl.getLaneId() + cx.getEquipment() + cx.getCommodity()
											+ cx.getStartDt();
									log.info("skey" + sKey);
									mapContract.put(sKey, cx);

									// failedList.add(getContract(rwa,e,cctl));
								} else {
									insertCompanyContractTenderLane(cctl, groupTenderId,
											contractGenerator(cctl.getShipperCompanyId(), cctl.getCarrierCompanyId(),
													groupTenderId, cctl.getEquipmentTypeId()));
									groupTenderIds.add(groupTenderId);
								}
							}
						}
						nEqptSet.add(e);
					}
				}
				ln.setEquipmentList(nEqptSet);
				ln.setGroupTenderId(groupTenderId);

				rwa.setLane(ln);
				ll.add(ln);
			}
			rwa.setLanes(ll);
			if (mapContract.size() > 0) {
				// rwa.setFailed(failedList);
				Set<Entry<String, Contract>> eSet = mapContract.entrySet(); // keyword
				for (Entry<String, Contract> e : eSet) {
					failedList.add(e.getValue());

				}
			}
			rwa.setFailed(failedList);
			for (Integer i : groupTenderIds) {
				if (do_BidMessageQueue(i) == 0) {
					return rwa;
				}
			}

			return rwa;

		} catch (Exception e) {
			log.info("error", e);
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);

		}
		return null;
	}

	public String contractGenerator(Integer customerId, Integer carrierId, Integer groupId, Integer equipmentTypeId) {
		return "" + customerId + "-" + carrierId + "-" + groupId + "-" + equipmentTypeId + new Date().getTime();
	}

	public List<Equipment> fetchCompanyEquipment(String countryCd) {
		List<Equipment> eList = new LinkedList<>();

		String sql = "select distinct equipment_type_id,equipment_type from vw_carrier_info where country_cd='"
				+ countryCd + "'";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					Equipment e = new Equipment();
					e.setEquipmentTypeId(rs.getInt(1));
					e.setEquipmentName(rs.getString(2));
					e.setIsSelected(false);
					eList.add(e);
				}
				return eList;
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);

		} finally {
			log.debug("Done");
		}
		return null;
	}

	public List<Carrier> fetchCarriers(String countryCd) {

		String sql = "select distinct company_id,company_code,company_name from vw_carrier_info where country_cd='"
				+ countryCd + "'";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			List<Carrier> carriers = new LinkedList<>();
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					Carrier carrier = new Carrier();
					carrier.setCompanyId(rs.getInt(1));
					carrier.setCarrierCd(rs.getString(2));
					carrier.setCarrierName(rs.getString(3));
					carriers.add(carrier);
				}
				return carriers;
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;

	}

	public Integer insertDbSeqTender() {
		String sql = " insert into dbseq_tender " + "(create_tstamp)" + "VALUES (now())";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	public Integer insertCompanyContractTenderLane(CompanyContractTenderLane cctl, Integer groupTenderId,
			String contractNbr) {
		String sql = " insert into company_contract_tender_lane "
				+ "(SHIPPER_COMPANY_ID, LANE_ID, COMMODITY_CD, RATE_PER_UNIT_CD, EQUIPMENT_TYPE_ID, RATE_EFFECTIVE_DT, RATE_TERMINATION_DT, CONTRACT_NBR, STATUS_CD, CARRIER_COMPANY_ID, BID_RANK, RATE_AMT, ACCESSORIAL_AMT, CURRENCY_CD, COMMENT_TXT, CREATE_DT, CREATE_USER_ID, UPDATE_DT, UPDATE_USER_ID,GROUP_TENDER_ID)"
				+ "VALUES (" + "?," // SHIPPER_COMPANY_ID, "
				+ "?,"// LANE_ID, "
				+ "?,"// COMMODITY_CD', "
				+ "?,"// 'RATE_PER_UNIT_CD', "
				+ "?,"// EQUIPMENT_TYPE_ID, "
				+ "?,"// 'RATE_EFFECTIVE_DT', "
				+ "?,"// RATE_TERMINATION_DT', "
				+ "?,"// 'CONTRACT_NBR', "
				+ "?,"// STATUS_CD', "
				+ "?,"// CARRIER_COMPANY_ID, "
				+ "?,"// BID_RANK,
				+ "?,"// RATE_AMT, "
				+ "?,"// ACCESSORIAL_AMT, "
				+ "?,"// CURRENCY_CD', "
				+ "?,"// COMMENT_TXT', "
				+ "?,"// CREATE_DT', "
				+ "?,"// CREATE_USER_ID', "
				+ "?,"// UPDATE_DT', "
				+ "?,?)";// UPDATE_USER_ID')";
		log.info("SQL " + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			log.info("CCTL : " + cctl.toString());
			pstmt.setInt(1, cctl.getShipperCompanyId());
			pstmt.setInt(2, cctl.getLaneId());
			pstmt.setString(3, cctl.getCommodityCd());
			pstmt.setString(4, cctl.getRatePerUnitCd());
			pstmt.setInt(5, cctl.getEquipmentTypeId());
			pstmt.setTimestamp(6, new Timestamp(cctl.getRateEffectiveDt().getTime()));
			pstmt.setTimestamp(7, new Timestamp(cctl.getRateTerminationDt().getTime()));
			pstmt.setString(8, contractNbr);
			pstmt.setString(9, cctl.getStatusCd());
			pstmt.setInt(10, cctl.getCarrierCompanyId());
			pstmt.setInt(11, cctl.getBidRank());
			pstmt.setDouble(12, cctl.getRateAmt());
			pstmt.setDouble(13, cctl.getAccessorialAmt());
			
			Company comp = fetchCompany(cctl.getShipperCompanyId());
			if(comp != null) {
				pstmt.setString(14, comp.getCurrencyCd());
			}else {
				pstmt.setString(14, cctl.getCurrencyCd());
			}
			pstmt.setString(15, cctl.getCommentTxt());
			pstmt.setTimestamp(16, new Timestamp(new Date().getTime()));
			pstmt.setString(17, cctl.getCreateUserId());
			pstmt.setTimestamp(18, new Timestamp(new Date().getTime()));
			pstmt.setString(19, cctl.getUpdateUserId());
			pstmt.setInt(20, groupTenderId);

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	public Integer insertLane(Lane lane) {
		String sql = "insert into lane (origin_city," + "origin_province," + "origin_country," + "origin_postal_cd,"
				+ "origin_district," + "dest_city," + "dest_province," + "dest_country," + "dest_postal_cd,"
				+ "dest_district," + "create_tstamp," + "update_tstamp) values (" + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?,now(),now())";

		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			pstmt.setString(1, trimString(lane.getOriginCity()));
			pstmt.setString(2, trimString(lane.getOriginProvince()));
			pstmt.setString(3, trimString(lane.getOriginCountry()));
			pstmt.setString(4, trimString(lane.getOriginPostalCd()));
			pstmt.setString(5, trimString(lane.getOriginDistrict()));
			pstmt.setString(6, trimString(lane.getDestCity()));
			pstmt.setString(7, trimString(lane.getDestProvince()));
			pstmt.setString(8, trimString(lane.getDestCountry()));
			pstmt.setString(9, trimString(lane.getDestPostalCd()));
			pstmt.setString(10, trimString(lane.getDestDistrict()));

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;

	}

	public Integer getLaneID(Lane lane) {
		try {

			String sql = "select lane_id from lane where " + "origin_city=? and " + "origin_province=? and "
					+ "origin_country=? and " + "origin_postal_cd=? and " + "origin_district=? and "
					+ "dest_city=? and " + "dest_province=? and " + "dest_country=? and " + "dest_postal_cd=? and "
					+ "dest_district=?";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				pstmt.setString(1, trimString(lane.getOriginCity()));
				pstmt.setString(2, trimString(lane.getOriginProvince()));
				pstmt.setString(3, trimString(lane.getOriginCountry()));
				pstmt.setString(4, trimString(lane.getOriginPostalCd()));
				pstmt.setString(5, trimString(lane.getOriginDistrict()));
				pstmt.setString(6, trimString(lane.getDestCity()));
				pstmt.setString(7, trimString(lane.getDestProvince()));
				pstmt.setString(8, trimString(lane.getDestCountry()));
				pstmt.setString(9, trimString(lane.getDestPostalCd()));
				pstmt.setString(10, trimString(lane.getDestDistrict()));

				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Lane> driverRawMapper = new ResultSetMapper<Lane>();
					List<Lane> pojoList = driverRawMapper.mapResultSetToObject(rs, Lane.class);
					// List<Driver> nw_pojoList = new LinkedList<Driver>();
					if (pojoList != null && !pojoList.isEmpty()) {
						log.info("Lane ID is " + pojoList.get(0).getLaneId());
						return pojoList.get(0).getLaneId();
					}

				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}

		return 0;
	}

	@POST // http://localhost:8080/market-api/trucker
	@Path("saveRates") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CarrierTransportationLeg> saveRates(List<CarrierTransportationLeg> legs) {
		Set<Integer> seqSet = new HashSet<>();
			
			for(CarrierTransportationLeg leg : legs) {
				try {
				CarrierRate revenue = leg.getRevenue().get(0);
			List<CarrierRate> cost = leg.getCost();
			int seqForNewCarrier = 1;	
			for (CarrierRate cr : cost) {

				Math.max(seqForNewCarrier, cr.getCarrierSeqNbr());
			}
			double revTotal = 0.0;
			revenue.setTotalCharge(revenue.getTotalAccessorial() + revenue.getTotalFsc() + revenue.getTotalBase()
					- revenue.getTotalDiscount());

			for (CarrierRate cr : cost) {
				log.info("carrier " + cr.getCarrierCd());
				int seq = cr.getCarrierSeqNbr();

				cr.setTotalCharge(
						cr.getTotalAccessorial() + cr.getTotalFsc() + cr.getTotalBase() - cr.getTotalDiscount());
				if (cr.getIsNewCarrierRate() == null || cr.getIsNewCarrierRate() == Boolean.FALSE) {
					seqSet.add(cr.getCarrierSeqNbr());

					do_CostUpdatePoReleaseStatement(cr, revenue.getTotalCharge(), cr.getTotalCharge(),
							cr.getTotalBase(), cr.getTotalFsc(), cr.getTotalAccessorial(), cr.getTransportLegNbr());
				} else {
					// insert here
					log.info("New Cost and revenue will be created");
					seqForNewCarrier++;
					log.info("new sequence " + seqForNewCarrier);
					seq = seqForNewCarrier;
					seqSet.add(seqForNewCarrier);
					newCostPoReleaseCarrier(cr, seqForNewCarrier, revenue.getTotalCharge());
					newRevenuePoReleaseCarrier(revenue, seqForNewCarrier);
				}
				do_DeleteCostCarrierCharge(cr);
				int lineItemNbr = 1;
				List<CarrierAccessorial> caList = cr.getAccessorials();
				if (caList != null && caList.size() > 0) {
					for (CarrierAccessorial ca : caList) {
						if (ca.getChargeCd() != null && !trimString(ca.getChargeCd()).isEmpty()) {
							if (!ca.getChargeCd().equalsIgnoreCase("FRT") && !ca.getChargeCd().equalsIgnoreCase("FSC")
									&& !ca.getChargeCd().equalsIgnoreCase("DSCFRT")) {
								log.info("cost charge type code..." + cr.getChargeTypeCd() + " sequence" + seq);

								do_InsertCostCarrierCharge(cr, ca, seq, lineItemNbr++);
							}
						}
					}

				}

				log.info("cost frt carrier charge..." + cr.getCarrierCd());
				CarrierAccessorial frt = new CarrierAccessorial();
				frt.setChargeCd("FRT");
				frt.setChargeDescTxt("Freight Charge");
				frt.setLineItemNbr(lineItemNbr++);
				frt.setChargeCcyCd("USD");
				frt.setChargeAmt(cr.getTotalBase());
				do_InsertCostCarrierCharge(cr, frt, seq, lineItemNbr++);

				log.info("cost fsc carrier charge..." + cr.getCarrierCd());
				CarrierAccessorial fsc = new CarrierAccessorial();
				fsc.setChargeCd("FSC");
				fsc.setChargeDescTxt("Fuel Surcharge");
				fsc.setLineItemNbr(lineItemNbr++);
				fsc.setChargeCcyCd("USD");
				fsc.setChargeAmt(cr.getTotalFsc());
				do_InsertCostCarrierCharge(cr, fsc, seq, lineItemNbr++);

				log.info("cost dsc carrier charge..." + cr.getCarrierCd());
				CarrierAccessorial dsc = new CarrierAccessorial();
				dsc.setChargeCd("DSCFRT");
				dsc.setChargeDescTxt("Discount");
				dsc.setLineItemNbr(lineItemNbr++);
				dsc.setChargeCcyCd("USD");
				dsc.setChargeAmt(cr.getTotalDiscount());
				do_InsertCostCarrierCharge(cr, dsc, seq, lineItemNbr++);

				List<PoHeader> hdrList = new LinkedList<>();

				// get
				if (cr.getPoTypeCd().equalsIgnoreCase("VPO")) {
					// get LPOs and delete them
					List<PoHeader> hList = fetch_LPOs(cr.getPoNbr());
					for (PoHeader h : hList) {
						insertDataStaging(h.getProBillNbr(), LC_STATUS_CD, LC_STATUS_DESC, "", cr.getUserId());

						hdrList.add(h);
					}

				}

				if (cr.getCancelFlg() && !cr.getIsCancelFlg()) {

					/**
					 * update po_reference info set sent_204/br flg/reject to
					 * false
					 **/
					do_resetCarrierLoadTenderFlgs(cr.getUserId(), cr.getPoNbr(), cr.getReleaseNbr(),
							cr.getParentPartnerCd(), cr.getPartnerCd(), cr.getShipperCd(), cr.getCarrierCd());
					log.info("cancel current Pro bill " + cr.getProBillNbr());
					insertWorkqueue(cr.getProBillNbr(), cr.getCarrierCd(), cr.getTransportLegNbr(), cr.getPartnerCd(),
							cr.getParentPartnerCd(), "CANCEL");
					do_DeleteDispatcher(cr.getProBillNbr());
					if (hdrList != null && hdrList.size() > 0) {
						for (PoHeader h : hdrList) {
							do_resetCarrierLoadTenderFlgs(cr.getUserId(), h.getPoNbr(), h.getReleaseNbr(),
									h.getParentPartnerCd(), h.getPartnerCd(), h.getShipperCd(), cr.getCarrierCd());
							log.info("cancel pro bill of LPOS " + h.getProBillNbr());
							insertWorkqueue(h.getProBillNbr(), cr.getCarrierCd(), cr.getTransportLegNbr(),
									h.getPartnerCd(), h.getParentPartnerCd(), "CANCEL");
							do_DeleteDispatcher(h.getProBillNbr());
						}
					}

				}
				if (cr.getSent204Flg() && !cr.getIsSent204Flg()) {
					do_updateCancelCarrierLoadTender(cr.getUserId(), cr.getPoNbr(), cr.getReleaseNbr(),
							cr.getParentPartnerCd(), cr.getPartnerCd(), cr.getShipperCd(), cr.getCarrierCd());
					insertWorkqueue(cr.getProBillNbr(), cr.getCarrierCd(), cr.getTransportLegNbr(), cr.getPartnerCd(),
							cr.getParentPartnerCd(), "SEND");

					if (hdrList != null && hdrList.size() > 0) {
						for (PoHeader h : hdrList) {
							do_updateCancelCarrierLoadTender(cr.getUserId(), h.getPoNbr(), h.getReleaseNbr(),
									h.getParentPartnerCd(), h.getPartnerCd(), h.getShipperCd(), cr.getCarrierCd());
							insertWorkqueue(h.getProBillNbr(), cr.getCarrierCd(), cr.getTransportLegNbr(),
									h.getPartnerCd(), h.getParentPartnerCd(), "SEND");
						}

					}

				}
				if (cr.getBrFlg() && !cr.getIsBrFlg()) {
					PoHeader vpoHdr = new PoHeader();
					vpoHdr.setParentPartnerCd(cr.getParentPartnerCd());
					vpoHdr.setPartnerCd(cr.getPartnerCd());
					vpoHdr.setShipperCd(cr.getShipperCd());
					vpoHdr.setCustomerCd(cr.getCustomerCd());
					vpoHdr.setPoNbr(cr.getPoNbr());
					vpoHdr.setReleaseNbr(cr.getReleaseNbr());
					vpoHdr.setPoTypeCd(cr.getPoTypeCd());
					hdrList.add(vpoHdr);
					
					for (PoHeader ph : hdrList) {
						if (ph.getPoTypeCd() != null && ph.getPoTypeCd().equalsIgnoreCase("VPO")) {
							log.info("NORMAL TMS TO DISPATCH" + ph.getPoNbr());
							doTmsLcToDispatcher(ph.getPartnerCd(), ph.getParentPartnerCd(), ph.getShipperCd(),
									ph.getCustomerCd(), ph.getPoNbr(), ph.getReleaseNbr(), ph.getPoNbr(), cr.getCarrierCd(), ph.getPoTypeCd(), cr.getTransportLegNbr());
						} else {
							log.info("VPO TMS TO DISPATCH" + ph.getPoNbr());
							doTmsLcToDispatcher(ph.getPartnerCd(), ph.getParentPartnerCd(), ph.getShipperCd(),
									ph.getCustomerCd(), ph.getPoNbr(), ph.getReleaseNbr(), null, cr.getCarrierCd(), ph.getPoTypeCd(), cr.getTransportLegNbr());
						}
					}

				}

			}
			insertDataStaging(revenue.getProBillNbr(), LC_STATUS_CD, LC_STATUS_DESC, "", revenue.getUserId());
			log.info("revenue update.." + revenue.toString());
			do_RevenueUpdatePoReleaseStatement(revenue, revenue.getTotalCharge(), revenue.getTotalCharge(),
					revenue.getTotalBase(), revenue.getTotalFsc(), revenue.getTotalAccessorial());
			log.info("revenue carrier charge deleted..");
			do_DeleteRevenueCarrierCharge(revenue);
			int lineItemNbr = 1;
			for (Integer sq : seqSet) {
				log.info("Sequence " + sq);
				List<CarrierAccessorial> revList = revenue.getAccessorials();
				for (CarrierAccessorial rv : revList) {
					if (!(trimString(rv.getChargeCd()).isEmpty())) {
						if (!rv.getChargeCd().equalsIgnoreCase("FRT") && !rv.getChargeCd().equalsIgnoreCase("FSC")
								&& !rv.getChargeCd().equalsIgnoreCase("DSCFRT")) {
						log.info("insert revenue carrier charge");
						do_InsertRevenueCarrierCharge(revenue, rv, sq, lineItemNbr++);
					}
					}
				}
				CarrierAccessorial frt = new CarrierAccessorial();
				frt.setChargeCd("FRT");
				frt.setChargeDescTxt("Freight Charge");
				frt.setLineItemNbr(lineItemNbr++);
				frt.setChargeCcyCd("USD");
				frt.setChargeAmt(revenue.getTotalBase());
				do_InsertRevenueCarrierCharge(revenue, frt, sq, lineItemNbr++);

				CarrierAccessorial fsc = new CarrierAccessorial();
				fsc.setChargeCd("FSC");
				fsc.setChargeDescTxt("Fuel Surcharge");
				fsc.setLineItemNbr(lineItemNbr++);
				fsc.setChargeCcyCd("USD");
				fsc.setChargeAmt(revenue.getTotalFsc());
				do_InsertRevenueCarrierCharge(revenue, fsc, sq, lineItemNbr++);

				CarrierAccessorial dsc = new CarrierAccessorial();
				dsc.setChargeCd("DSCFRT");
				dsc.setChargeDescTxt("Discount");
				dsc.setLineItemNbr(lineItemNbr++);
				dsc.setChargeCcyCd("USD");
				dsc.setChargeAmt(revenue.getTotalDiscount());
				do_InsertRevenueCarrierCharge(revenue, dsc, sq, lineItemNbr++);

			}

			
		} catch (Exception e) {
			log.info("error", e);
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);

		}
		}
		return legs;
	}

	@POST // http://localhost:8080/market-api/trucker
	@Path("saveCarrierProfile") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Carrier saveCarrierProfile(Carrier carrier) {

		String sql = "insert into carrier_profile (" + "parent_partner_cd," + "partner_cd," + "carrier_name,"
				+ "carrier_cd," + "carrier_contact_person," + "carrier_email_address," + "carrier_telephone_nbr)"
				+ " values (" + "?," + "?," + "?," + "?," + "?," + "?," + "?)";
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			pstmt.setString(1, carrier.getParentPartnerCd());// no pdf file
			pstmt.setString(2, "*");
			pstmt.setString(3, carrier.getCarrierName());
			pstmt.setString(4, carrier.getCarrierCd());
			pstmt.setString(5, carrier.getCarrierContactPerson());
			pstmt.setString(6, carrier.getCarrierEmailAddress());
			pstmt.setString(7, carrier.getCarrierTelephoneNbr());
			pstmt.executeUpdate();
			return carrier;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	/**
	 * steps in saving rates **
	 * 
	 * Cost 1. Update Cost PO Release carrier 2. If Cancel Then return flags to
	 * available 3. If br flag then direct dispatch 4. If Send Flag call rodel
	 * 5. Re-create carrier_charge
	 * 
	 * Revenue 1. Update Cost PO Release carrier 2. If Cancel Then return flags
	 * to available 3. If br flag then direct dispatch 4. If Send Flag call
	 * rodel 5. Re-create carrier_charge
	 *
	 * 
	 * 
	 * 
	 * 
	 */

	public Integer createCostPoReleaseCarrier(Cost cost, SpotRate sp, Integer legId, Double revenueAmt,
			Double totalAccessorialAmt, Integer carrierSeqNbr, String chargeTypeCd) {

		String sql = "insert into po_release_carrier (" + "carrier_cd," // 1
				+ "carrier_seq_nbr,"// 2
				+ "charge_type_cd," // 3
				+ "equipment_type_cd,"// 4
				+ "transport_leg_nbr," // 5
				+ "transport_leg_id,"// 6
				+ "accept_990_flg,"// 7
				+ "active_flg," // 8
				+ "cancel_990_flg,"// 9
				+ "create_tstamp,"// 10
				+ "create_user_id,"// 11
				+ "customer_cd,"// 12
				+ "contract_number,"// 13
				+ "distance_miles_value,"// 14
				+ "equipment_shipment_seq_nbr,"// 15
				+ "equipment_total_qty,"// 16
				+ "frt_charge_amt,"// 17
				+ "fsc_charge_amt,"// 18
				+ "fsc_type_cd,"// 19
				+ "parent_partner_cd,"// 20
				+ "partner_cd,"// 21
				+ "po_nbr,"// 22
				+ "preconfirmed_carrier_flg,"// 23
				+ "rate_type_cd,"// 24
				+ "reject_990_flg,"// 25
				+ "release_nbr,"// 26
				+ "route_info_txt,"// 27
				+ "route_type_cd,"// 28
				+ "sent_204_flg,"// 29
				+ "shipper_cd,"// 30
				+ "tender_requested_qty,"// 31
				+ "terminal_handling_cost_amt,"// 32
				+ "total_revenue_amt,"// 33
				+ "total_revenue_usd_amt,"// 34
				+ "service_provider_type_cd,"// 35
				+ "total_customs_brokerage_cost_amt,"// 36
				+ "total_storage_fee_cost_amt,"// 37
				+ "carrier_mark_cd,"// 38
				+ "nvo_carrier_cd,"// 39
				+ "transit_days_dur,"// 40
				+ "transhipment_days_dur,"// 41
				+ "transhipment_loc_name,"// 42
				+ "transport_mode_cd,"// 43
				+ "update_tstamp,"// 44
				+ "update_user_id,"// 45
				+ "transport_amt_ccy_cd,"// 46
				+ "revenue_ccy_cd,"// 47
				+ "total_accessorial_cost_amt,"// 48
				+ "total_transport_amt) values ("// 49

				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?,"

				+ "?)";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			log.info("equipment " + sp);
			pstmt.setString(1, cost.getCarrierCd()); // carrier code
			pstmt.setInt(2, carrierSeqNbr); // default to 1 carrier seq number
			pstmt.setString(3, chargeTypeCd); // charge type
			pstmt.setString(4, sp.getEquipmentType());// equipment type code
			pstmt.setInt(5, sp.getLegNbr());// leg number
			pstmt.setInt(6, legId); // leg id
			pstmt.setString(7, "N");// accept 990 flg
			pstmt.setString(8, "Y");// active flg
			pstmt.setString(9, "N");// cancel 990 flg
			pstmt.setTimestamp(10, new Timestamp(new Date().getTime()));// create
																		// tstamp
			pstmt.setString(11, sp.getUserId());// create user id
			pstmt.setString(12, sp.getCustomerCd());// customer code
			pstmt.setString(13, cost.getContractNumber());// contract number
			pstmt.setDouble(14, cost.getMileage()); // distance_miles_value
			pstmt.setInt(15, sp.getEquipmentShipmentSeqNbr());// equipment_shipment_seq_nbr
			pstmt.setInt(16, sp.getNoOfEquipment());// equipment_total_qty
			pstmt.setDouble(17, cost.getBaseFreight());// base freight
			pstmt.setDouble(18, cost.getFsc());// fsc charge amt
			pstmt.setString(19, "FSC"); // fsc type code
			pstmt.setString(20, sp.getParentPartnerCd()); // parent partner code
			pstmt.setString(21, sp.getPartnerCd()); // partner code
			pstmt.setString(22, sp.getPoNbr()); // po number
			pstmt.setString(23, "N");// pre confirmed carrier flag
			pstmt.setString(24, "R");// rate type code
			pstmt.setString(25, "N");// reject_990_flg
			pstmt.setString(26, sp.getReleaseNbr()); // release number
			pstmt.setString(27, sp.getOriginCity()); // route info text
			pstmt.setString(28, "R"); // route type code
			pstmt.setString(29, "N"); // sent 204 flg
			pstmt.setString(30, sp.getShipperCd());// shipper code
			pstmt.setInt(31, 1);// shipper code
			pstmt.setDouble(32, 0.0);
			pstmt.setDouble(33, revenueAmt);
			pstmt.setDouble(34, revenueAmt);
			pstmt.setString(35, "DD");
			pstmt.setDouble(36, 0.0);
			pstmt.setDouble(37, 0.0);

			pstmt.setString(38, sp.getCarrier()); // carrier mark cd

			pstmt.setString(39, sp.getParentPartnerCd());// nvo carrier code
			pstmt.setInt(40, cost.getTransitDay()); // transit days
			pstmt.setInt(41, 1); // transhipmetn days dur default to 1
			pstmt.setString(42, "");// transhipment_loc_name
			pstmt.setString(43, "DRAY");// transhipment mode code
			pstmt.setTimestamp(44, new Timestamp(new Date().getTime()));// update
																		// tstamp
			pstmt.setString(45, sp.getUserId());// update user id
			if (sp.getCurrencyCd() == null) {
				pstmt.setString(46, "USD");
				pstmt.setString(47, "USD");
			} else {
				pstmt.setString(46, sp.getCurrencyCd());
				pstmt.setString(47, sp.getCurrencyCd());
			}
			pstmt.setDouble(48, totalAccessorialAmt);
			pstmt.setDouble(49, cost.getTotalCharge());

			int id = pstmt.executeUpdate();
			return id;

		} catch (Exception e) {
			log.error("error", e);
		}

		return 0;
	}

	public Integer newCostPoReleaseCarrier(CarrierRate cost, Integer carrierSeqNbr, Double revenueAmt) {

		String sql = "insert into po_release_carrier (" + "carrier_cd," // 1
				+ "carrier_seq_nbr,"// 2
				+ "charge_type_cd," // 3
				+ "equipment_type_cd,"// 4
				+ "transport_leg_nbr," // 5
				+ "transport_leg_id,"// 6
				+ "accept_990_flg,"// 7
				+ "active_flg," // 8
				+ "cancel_990_flg,"// 9
				+ "create_tstamp,"// 10
				+ "create_user_id,"// 11
				+ "customer_cd,"// 12
				+ "contract_number,"// 13
				+ "distance_miles_value,"// 14
				+ "equipment_shipment_seq_nbr,"// 15
				+ "equipment_total_qty,"// 16
				+ "frt_charge_amt,"// 17
				+ "fsc_charge_amt,"// 18
				+ "fsc_type_cd,"// 19
				+ "parent_partner_cd,"// 20
				+ "partner_cd,"// 21
				+ "po_nbr,"// 22
				+ "preconfirmed_carrier_flg,"// 23
				+ "rate_type_cd,"// 24
				+ "reject_990_flg,"// 25
				+ "release_nbr,"// 26
				+ "route_info_txt,"// 27
				+ "route_type_cd,"// 28
				+ "sent_204_flg,"// 29
				+ "shipper_cd,"// 30
				+ "tender_requested_qty,"// 31
				+ "terminal_handling_cost_amt,"// 32
				+ "total_revenue_amt,"// 33
				+ "total_revenue_usd_amt,"// 34
				+ "service_provider_type_cd,"// 35
				+ "total_customs_brokerage_cost_amt,"// 36
				+ "total_storage_fee_cost_amt,"// 37
				+ "carrier_mark_cd,"// 38
				+ "nvo_carrier_cd,"// 39
				+ "transit_days_dur,"// 40
				+ "transhipment_days_dur,"// 41
				+ "transhipment_loc_name,"// 42
				+ "transport_mode_cd,"// 43
				+ "update_tstamp,"// 44
				+ "update_user_id,"// 45
				+ "transport_amt_ccy_cd,"// 46
				+ "revenue_ccy_cd,"// 47
				+ "total_accessorial_cost_amt,"// 48
				+ "total_transport_amt) values ("// 49

				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?,"

				+ "?)";
		log.info("SQL " + sql);
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

			pstmt.setString(1, cost.getCarrierCd()); // carrier code
			pstmt.setInt(2, carrierSeqNbr); // default to 1 carrier seq number
			pstmt.setString(3, "C"); // charge type
			pstmt.setString(4, cost.getEquipmentTypeCd());// equipment type code
			pstmt.setInt(5, cost.getTransportLegNbr());// leg number
			pstmt.setInt(6, cost.getTransportLegId()); // leg id
			pstmt.setString(7, "N");// accept 990 flg
			pstmt.setString(8, "Y");// active flg
			pstmt.setString(9, "N");// cancel 990 flg
			pstmt.setTimestamp(10, new Timestamp(new Date().getTime()));// create
																		// tstamp
			pstmt.setString(11, cost.getUserId());// create user id
			pstmt.setString(12, cost.getCustomerCd());// customer code
			pstmt.setString(13, "");// contract number
			pstmt.setDouble(14, 0.0); // distance_miles_value
			pstmt.setInt(15, cost.getEquipmentShipmentSeqNbr());// equipment_shipment_seq_nbr
			pstmt.setInt(16, 1);// equipment_total_qty
			pstmt.setDouble(17, cost.getTotalBase());// base freight
			pstmt.setDouble(18, cost.getTotalFsc());// fsc charge amt
			pstmt.setString(19, "FSC"); // fsc type code
			pstmt.setString(20, cost.getParentPartnerCd()); // parent partner
															// code
			pstmt.setString(21, cost.getPartnerCd()); // partner code
			pstmt.setString(22, cost.getPoNbr()); // po number
			pstmt.setString(23, "N");// pre confirmed carrier flag
			pstmt.setString(24, "R");// rate type code
			pstmt.setString(25, "N");// reject_990_flg
			pstmt.setString(26, cost.getReleaseNbr()); // release number
			pstmt.setString(27, cost.getPickupCityName()); // route info text
			pstmt.setString(28, "R"); // route type code
			pstmt.setString(29, "N"); // sent 204 flg
			pstmt.setString(30, cost.getShipperCd());// shipper code
			pstmt.setInt(31, 1);// shipper code
			pstmt.setDouble(32, 0.0);
			pstmt.setDouble(33, revenueAmt);
			pstmt.setDouble(34, revenueAmt);
			pstmt.setString(35, "DD");
			pstmt.setDouble(36, 0.0);
			pstmt.setDouble(37, 0.0);

			pstmt.setString(38, cost.getCarrierCd()); // carrier mark cd

			pstmt.setString(39, cost.getParentPartnerCd());// nvo carrier code
			pstmt.setInt(40, 1); // transit days
			pstmt.setInt(41, 1); // transhipmetn days dur default to 1
			pstmt.setString(42, "");// transhipment_loc_name
			pstmt.setString(43, "DRAY");// transhipment mode code
			pstmt.setTimestamp(44, new Timestamp(new Date().getTime()));// update
																		// tstamp
			pstmt.setString(45, cost.getUserId());// update user id
			if (cost.getCurrencyCd() == null) {
				pstmt.setString(46, "USD");
				pstmt.setString(47, "USD");
			} else {
				pstmt.setString(46, cost.getCurrencyCd());
				pstmt.setString(47, cost.getCurrencyCd());
			}
			pstmt.setDouble(48, cost.getTotalAccessorial());
			pstmt.setDouble(49, cost.getTotalCharge());

			int id = pstmt.executeUpdate();
			return id;

		} catch (Exception e) {
			log.error("error", e);
		}

		return 0;
	}

	public Integer newRevenuePoReleaseCarrier(CarrierRate rev, Integer carrierSeqNbr) {

		String sql = "insert into po_release_carrier (" + "carrier_cd," // 1
				+ "carrier_seq_nbr,"// 2
				+ "charge_type_cd," // 3
				+ "equipment_type_cd,"// 4
				+ "transport_leg_nbr," // 5
				+ "transport_leg_id,"// 6
				+ "accept_990_flg,"// 7
				+ "active_flg," // 8
				+ "cancel_990_flg,"// 9
				+ "create_tstamp,"// 10
				+ "create_user_id,"// 11
				+ "customer_cd,"// 12
				+ "contract_number,"// 13
				+ "distance_miles_value,"// 14
				+ "equipment_shipment_seq_nbr,"// 15
				+ "equipment_total_qty,"// 16
				+ "frt_charge_amt,"// 17
				+ "fsc_charge_amt,"// 18
				+ "fsc_type_cd,"// 19
				+ "parent_partner_cd,"// 20
				+ "partner_cd,"// 21
				+ "po_nbr,"// 22
				+ "preconfirmed_carrier_flg,"// 23
				+ "rate_type_cd,"// 24
				+ "reject_990_flg,"// 25
				+ "release_nbr,"// 26
				+ "route_info_txt,"// 27
				+ "route_type_cd,"// 28
				+ "sent_204_flg,"// 29
				+ "shipper_cd,"// 30
				+ "tender_requested_qty,"// 31
				+ "terminal_handling_cost_amt,"// 32
				+ "total_revenue_amt,"// 33
				+ "total_revenue_usd_amt,"// 34
				+ "service_provider_type_cd,"// 35
				+ "total_customs_brokerage_cost_amt,"// 36
				+ "total_storage_fee_cost_amt,"// 37
				+ "carrier_mark_cd,"// 38
				+ "nvo_carrier_cd,"// 39
				+ "transit_days_dur,"// 40
				+ "transhipment_days_dur,"// 41
				+ "transhipment_loc_name,"// 42
				+ "transport_mode_cd,"// 43
				+ "update_tstamp,"// 44
				+ "update_user_id,"// 45
				+ "transport_amt_ccy_cd,"// 46
				+ "revenue_ccy_cd,"// 47
				+ "total_accessorial_cost_amt,"// 48
				+ "total_transport_amt) values ("// 49

				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?,"

				+ "?)";
		log.info("SQL " + sql);
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

			pstmt.setString(1, rev.getCarrierCd()); // carrier code
			pstmt.setInt(2, carrierSeqNbr); // default to 1 carrier seq number
			pstmt.setString(3, "R"); // charge type
			pstmt.setString(4, rev.getEquipmentTypeCd());// equipment type code
			pstmt.setInt(5, rev.getTransportLegNbr());// leg number
			pstmt.setInt(6, rev.getTransportLegId()); // leg id
			pstmt.setString(7, "N");// accept 990 flg
			pstmt.setString(8, "Y");// active flg
			pstmt.setString(9, "N");// cancel 990 flg
			pstmt.setTimestamp(10, new Timestamp(new Date().getTime()));// create
																		// tstamp
			pstmt.setString(11, rev.getUserId());// create user id
			pstmt.setString(12, rev.getCustomerCd());// customer code
			pstmt.setString(13, "");// contract number
			pstmt.setDouble(14, 0.0); // distance_miles_value
			pstmt.setInt(15, rev.getEquipmentShipmentSeqNbr());// equipment_shipment_seq_nbr
			pstmt.setInt(16, 1);// equipment_total_qty
			pstmt.setDouble(17, rev.getTotalBase());// base freight
			pstmt.setDouble(18, rev.getTotalFsc());// fsc charge amt
			pstmt.setString(19, "FSC"); // fsc type code
			pstmt.setString(20, rev.getParentPartnerCd()); // parent partner
															// code
			pstmt.setString(21, rev.getPartnerCd()); // partner code
			pstmt.setString(22, rev.getPoNbr()); // po number
			pstmt.setString(23, "N");// pre confirmed carrier flag
			pstmt.setString(24, "R");// rate type code
			pstmt.setString(25, "N");// reject_990_flg
			pstmt.setString(26, rev.getReleaseNbr()); // release number
			pstmt.setString(27, rev.getPickupCityName()); // route info text
			pstmt.setString(28, "R"); // route type code
			pstmt.setString(29, "N"); // sent 204 flg
			pstmt.setString(30, rev.getShipperCd());// shipper code
			pstmt.setInt(31, 1);// shipper code
			pstmt.setDouble(32, 0.0);
			pstmt.setDouble(33, rev.getTotalCharge());
			pstmt.setDouble(34, rev.getTotalCharge());
			pstmt.setString(35, "DD");
			pstmt.setDouble(36, 0.0);
			pstmt.setDouble(37, 0.0);

			pstmt.setString(38, rev.getCarrierCd()); // carrier mark cd

			pstmt.setString(39, rev.getParentPartnerCd());// nvo carrier code
			pstmt.setInt(40, 1); // transit days
			pstmt.setInt(41, 1); // transhipmetn days dur default to 1
			pstmt.setString(42, "");// transhipment_loc_name
			pstmt.setString(43, "DRAY");// transhipment mode code
			pstmt.setTimestamp(44, new Timestamp(new Date().getTime()));// update
																		// tstamp
			pstmt.setString(45, rev.getUserId());// update user id
			if (rev.getCurrencyCd() == null) {
				pstmt.setString(46, "USD");
				pstmt.setString(47, "USD");
			} else {
				pstmt.setString(46, rev.getCurrencyCd());
				pstmt.setString(47, rev.getCurrencyCd());
			}
			pstmt.setDouble(48, rev.getTotalAccessorial());
			pstmt.setDouble(49, rev.getTotalCharge());

			int id = pstmt.executeUpdate();
			return id;

		} catch (Exception e) {
			log.error("error", e);
		}

		return 0;
	}

	public Integer createRevenuePoReleaseCarrier(Revenue cost, SpotRate sp, Integer legId, Double revenueAmt,
			Double totalAccessorialAmt, Integer carrierSeqNbr, String chargeTypeCd, String carrierCd) {

		String sql = "insert into po_release_carrier (" + "carrier_cd," // 1
				+ "carrier_seq_nbr,"// 2
				+ "charge_type_cd," // 3
				+ "equipment_type_cd,"// 4
				+ "transport_leg_nbr," // 5
				+ "transport_leg_id,"// 6
				+ "accept_990_flg,"// 7
				+ "active_flg," // 8
				+ "cancel_990_flg,"// 9
				+ "create_tstamp,"// 10
				+ "create_user_id,"// 11
				+ "customer_cd,"// 12
				+ "contract_number,"// 13
				+ "distance_miles_value,"// 14
				+ "equipment_shipment_seq_nbr,"// 15
				+ "equipment_total_qty,"// 16
				+ "frt_charge_amt,"// 17
				+ "fsc_charge_amt,"// 18
				+ "fsc_type_cd,"// 19
				+ "parent_partner_cd,"// 20
				+ "partner_cd,"// 21
				+ "po_nbr,"// 22
				+ "preconfirmed_carrier_flg,"// 23
				+ "rate_type_cd,"// 24
				+ "reject_990_flg,"// 25
				+ "release_nbr,"// 26
				+ "route_info_txt,"// 27
				+ "route_type_cd,"// 28
				+ "sent_204_flg,"// 29
				+ "shipper_cd,"// 30
				+ "tender_requested_qty,"// 31
				+ "terminal_handling_cost_amt,"// 32
				+ "total_revenue_amt,"// 33
				+ "total_revenue_usd_amt,"// 34
				+ "service_provider_type_cd,"// 35
				+ "total_customs_brokerage_cost_amt,"// 36
				+ "total_storage_fee_cost_amt,"// 37
				+ "carrier_mark_cd,"// 38
				+ "nvo_carrier_cd,"// 39
				+ "transit_days_dur,"// 40
				+ "transhipment_days_dur,"// 41
				+ "transhipment_loc_name,"// 42
				+ "transport_mode_cd,"// 43
				+ "update_tstamp,"// 44
				+ "update_user_id,"// 45
				+ "transport_amt_ccy_cd,"// 46
				+ "revenue_ccy_cd,"// 47
				+ "total_accessorial_cost_amt,"// 48
				+ "total_transport_amt) values ("// 49

				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?,"

				+ "?)";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

			pstmt.setString(1, carrierCd); // carrier code
			pstmt.setInt(2, carrierSeqNbr); // default to 1 carrier seq number
			pstmt.setString(3, chargeTypeCd); // charge type
			pstmt.setString(4, sp.getEquipmentType());// equipment type code
			pstmt.setInt(5, sp.getLegNbr());// leg number
			pstmt.setInt(6, legId); // leg id
			pstmt.setString(7, "N");// accept 990 flg
			pstmt.setString(8, "Y");// active flg
			pstmt.setString(9, "N");// cancel 990 flg
			pstmt.setTimestamp(10, new Timestamp(new Date().getTime()));// create
																		// tstamp
			pstmt.setString(11, sp.getUserId());// create user id
			pstmt.setString(12, sp.getCustomerCd());// customer code
			pstmt.setString(13, cost.getContractNumber());// contract number
			pstmt.setDouble(14, cost.getMileage()); // distance_miles_value
			pstmt.setInt(15, sp.getEquipmentShipmentSeqNbr());// equipment_shipment_seq_nbr
			pstmt.setInt(16, sp.getNoOfEquipment());// equipment_total_qty
			pstmt.setDouble(17, cost.getBaseFreight());// base freight
			pstmt.setDouble(18, cost.getFsc());// fsc charge amt
			pstmt.setString(19, "FSC"); // fsc type code
			pstmt.setString(20, sp.getParentPartnerCd()); // parent partner code
			pstmt.setString(21, sp.getPartnerCd()); // partner code
			pstmt.setString(22, sp.getPoNbr()); // po number
			pstmt.setString(23, "N");// pre confirmed carrier flag
			pstmt.setString(24, "R");// rate type code
			pstmt.setString(25, "N");// reject_990_flg
			pstmt.setString(26, sp.getReleaseNbr()); // release number
			pstmt.setString(27, sp.getOriginCity()); // route info text
			pstmt.setString(28, "R"); // route type code
			pstmt.setString(29, "N"); // sent 204 flg
			pstmt.setString(30, sp.getShipperCd());// shipper code
			pstmt.setInt(31, 1);// shipper code
			pstmt.setDouble(32, 0.0);
			pstmt.setDouble(33, revenueAmt);
			pstmt.setDouble(34, revenueAmt);
			pstmt.setString(35, "DD");
			pstmt.setDouble(36, 0.0);
			pstmt.setDouble(37, 0.0);

			pstmt.setString(38, sp.getCarrier()); // carrier mark cd

			pstmt.setString(39, sp.getParentPartnerCd());// nvo carrier code
			pstmt.setInt(40, cost.getTransitDay()); // transit days
			pstmt.setInt(41, 1); // transhipmetn days dur default to 1
			pstmt.setString(42, "");// transhipment_loc_name
			pstmt.setString(43, "DRAY");// transhipment mode code
			pstmt.setTimestamp(44, new Timestamp(new Date().getTime()));// update
																		// tstamp
			pstmt.setString(45, sp.getUserId());// update user id
			if (sp.getCurrencyCd() == null) {
				pstmt.setString(46, "USD");
				pstmt.setString(47, "USD");
			} else {
				pstmt.setString(46, sp.getCurrencyCd());
				pstmt.setString(47, sp.getCurrencyCd());
			}
			pstmt.setDouble(48, totalAccessorialAmt);
			pstmt.setDouble(49, cost.getTotalCharge());

			int id = pstmt.executeUpdate();
			return id;

		} catch (Exception e) {
			log.error("error", e);
		}

		return 0;
	}

	public Integer createCarrierAccessorialCharge(CarrierAccessorial acc, SpotRate sp, Integer legId,
			Integer carrierSeqNbr, Integer lineItemNbr, String chargeTypeCd, String carrierCd) {

		String sql = "insert into carrier_accessorial_charge (" + "carrier_cd," + "carrier_seq_nbr," + "charge_type_cd,"
				+ "equipment_type_cd," + "line_item_nbr," + "transport_leg_id," + "charge_amt," + "charge_ccy_cd,"
				+ "charge_cd," + "charge_desc_txt," + "create_tstamp," + "create_user_id," + "customer_cd,"
				+ "equipment_shipment_seq_nbr," + "exchange_rt," + "item_cnt," + "parent_partner_cd," + "partner_cd,"
				+ "po_nbr," + "print_cd," + "rate," + "release_nbr," + "shipper_cd," + "transport_leg_nbr,"
				+ "update_tstamp," + "update_user_id," + "usd_charge_amt," + "wt," + "wt_uom) values(" + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?)";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

			pstmt.setString(1, carrierCd);// "carrier_cd,"
			pstmt.setInt(2, carrierSeqNbr); // carrier_seq_nbr,
			pstmt.setString(3, chargeTypeCd);// charge_type_cd,
			pstmt.setString(4, sp.getEquipmentType());// equipment_type_cd
			pstmt.setInt(5, lineItemNbr);// line_item_nbr
			pstmt.setInt(6, legId);// transport_leg_id

			pstmt.setDouble(7, acc.getChargeAmt());
			pstmt.setString(8, sp.getCurrencyCd());
			pstmt.setString(9, acc.getChargeCd());
			pstmt.setString(10, acc.getChargeDescTxt());
			pstmt.setTimestamp(11, new Timestamp(new Date().getTime()));
			pstmt.setString(12, sp.getUserId());
			pstmt.setString(13, sp.getCustomerCd());
			pstmt.setInt(14, sp.getEquipmentShipmentSeqNbr());
			pstmt.setDouble(15, 0.0);
			pstmt.setDouble(16, 1.0);
			pstmt.setString(17, sp.getParentPartnerCd());
			pstmt.setString(18, sp.getPartnerCd());
			pstmt.setString(19, sp.getPoNbr());
			pstmt.setString(20, "PRINT");
			pstmt.setDouble(21, sp.getRtWt());
			pstmt.setString(22, sp.getReleaseNbr());
			pstmt.setString(23, sp.getShipperCd());
			pstmt.setInt(24, sp.getLegNbr());
			pstmt.setTimestamp(25, new Timestamp(new Date().getTime()));
			pstmt.setString(26, sp.getUserId());
			pstmt.setDouble(27, acc.getChargeAmt());
			pstmt.setDouble(28, sp.getWeight());
			pstmt.setString(29, sp.getWtUom());

			int id = pstmt.executeUpdate();
			return id;

		} catch (Exception e) {
			log.error("error", e);
		}

		return 0;
	}

	public Integer createLegEquipment(SpotRate sp, Integer legId) {
		String sql = "insert into leg_equipment_type (" + "transport_leg_id," + "equipment_type_cd," + "active_flg,"
				+ "actual_carrier_cd," + "equipment_desc_txt," + "equipment_qty," + "equipment_type_seq_nbr) values ("
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?)";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			pstmt.setInt(1, legId);
			pstmt.setString(2, sp.getEquipmentType());
			pstmt.setString(3, "Y");
			pstmt.setString(4, sp.getParentPartnerCd());
			pstmt.setString(5, sp.getEquipmentDesc());
			pstmt.setInt(6, sp.getNoOfEquipment());
			pstmt.setInt(7, 1);
			int id = pstmt.executeUpdate();
			return id;

		} catch (Exception e) {
			log.error("error", e);
		}

		return 0;
	}

	public Integer createCarrierCharge(CarrierAccessorial acc, SpotRate sp, Integer legId, Integer itemNbr,
			Integer carrierSeq, String chargeTypeCd, String carrierCd, Double rtWt) {
		log.info("spot rate " + sp);
		String sql = "insert into carrier_charge (" + "carrier_cd," // 1
				+ "charge_type_cd,"// 2
				+ "equipment_type_cd,"// 3
				+ "line_item_nbr,"// 4
				+ "transport_leg_id,"// 5
				+ "carrier_seq_nbr,"// 6
				+ "transport_leg_nbr,"// 7
				+ "charge_amt,"// 8
				+ "active_flg,"// 9
				+ "charge_ccy_cd,"// 10
				+ "charge_desc_txt,"// 11
				+ "create_tstamp,"// 12
				+ "create_user_id,"// 13
				+ "print_cd,"// 14
				+ "exchange_rt,"// 15
				+ "rate,"// 16
				+ "reference_value,"// 17
				+ "customer_cd,"// 18
				+ "parent_partner_cd,"// 19
				+ "shipper_cd,"// 20
				+ "po_nbr,"// 21
				+ "release_nbr,"// 22
				+ "equipment_shipment_seq_nbr,"// 23
				+ "partner_cd,charge_cd) values ("// 24
				+ "?,"// 1
				+ "?,"// 2
				+ "?,"// 3
				+ "?,"// 4
				+ "?,"// 5
				+ "?,"// 6
				+ "?,"// 7
				+ "?,"// 8
				+ "?,"// 9
				+ "?,"// 10
				+ "?,"// 11
				+ "?,"// 12
				+ "?,"// 13
				+ "?,"// 14
				+ "?,"// 15
				+ "?,"// 16
				+ "?,"// 17
				+ "?,"// 18
				+ "?,"// 19
				+ "?,"// 20
				+ "?,"// 21
				+ "?,"// 22
				+ "?,"// 23
				+ "?,?)";// 24

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			pstmt.setString(1, carrierCd);
			pstmt.setString(2, chargeTypeCd);
			pstmt.setString(3, sp.getEquipmentType());
			pstmt.setInt(4, itemNbr);
			pstmt.setInt(5, legId);
			pstmt.setInt(6, carrierSeq);
			pstmt.setInt(7, sp.getLegNbr());
			if (acc.getChargeAmt() != null) {
				pstmt.setDouble(8, acc.getChargeAmt());
			} else {
				pstmt.setDouble(8, 0.0);

			}
			pstmt.setString(9, "Y");
			pstmt.setString(10, sp.getCurrencyCd());
			pstmt.setString(11, acc.getChargeDescTxt());
			pstmt.setTimestamp(12, new Timestamp(new Date().getTime()));
			pstmt.setString(13, sp.getUserId());
			if (acc.getChargeAmt() != null && acc.getChargeAmt() > 0.0) {
				pstmt.setString(14, acc.getChargeCd());
			} else {
				pstmt.setString(14, "DNP");
			}
			pstmt.setDouble(15, sp.getExchangeRt());
			pstmt.setDouble(16, rtWt);
			pstmt.setString(17, "");
			pstmt.setString(18, sp.getCustomerCd());
			pstmt.setString(19, sp.getParentPartnerCd());
			pstmt.setString(20, sp.getShipperCd());
			pstmt.setString(21, sp.getPoNbr());
			pstmt.setString(22, sp.getReleaseNbr());
			pstmt.setInt(23, sp.getEquipmentShipmentSeqNbr());
			pstmt.setString(24, sp.getPartnerCd());
			pstmt.setString(25, acc.getChargeCd());
			int id = pstmt.executeUpdate();
			return id;

		} catch (Exception e) {
			log.error("error", e);
		}

		return 0;
	}

	public Integer updatePoHeader(SpotRate sp) {
		String sql = "update tms_utf8_db.po_header set last_event='Spot Rate',last_event_dt=now(),update_tstamp=now() "
				+ " where parent_partner_cd=? and partner_cd=? and shipper_cd=? and customer_cd=? and po_nbr=? and release_nbr=?";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			pstmt.setString(1, sp.getParentPartnerCd());
			pstmt.setString(2, sp.getPartnerCd());
			pstmt.setString(3, sp.getShipperCd());
			pstmt.setString(4, sp.getCustomerCd());

			pstmt.setString(5, sp.getPoNbr());
			pstmt.setString(6, sp.getReleaseNbr());
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			log.error("Error", e);
		}

		return 0;

	}

	public Integer createTransportLeg(SpotRate sp) {

		String sql = "insert into transportation_leg (" + "transport_leg_nbr," + "partner_cd," + "parent_partner_cd,"
				+ "shipper_cd," + "customer_cd," + "po_nbr," + "release_nbr," + "equipment_shipment_seq_nbr,"
				+ "create_tstamp," + "create_user_id," + "update_tstamp," + "update_user_id," + "leg_completed_flg,"
				+ "pickup_city_name," + "pickup_state_cd," + "pickup_country_cd," + "pickup_location_name,"
				+ "pickup_postal_cd," + "delivery_city_name," + "delivery_state_cd," + "delivery_country_cd,"
				+ "delivery_location_name," + "delivery_postal_cd," + "preferred_carrier_cd, delivery_line_1_addr) values("

				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?)";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			pstmt.setInt(1, sp.getLegNbr());
			pstmt.setString(2, sp.getPartnerCd());
			pstmt.setString(3, sp.getParentPartnerCd());
			pstmt.setString(4, sp.getShipperCd());
			pstmt.setString(5, sp.getCustomerCd());

			pstmt.setString(6, sp.getPoNbr());
			pstmt.setString(7, sp.getReleaseNbr());
			pstmt.setInt(8, sp.getEquipmentShipmentSeqNbr());
			pstmt.setTimestamp(9, new Timestamp(new Date().getTime()));
			pstmt.setString(10, sp.getUserId());
			pstmt.setTimestamp(11, new Timestamp(new Date().getTime()));
			pstmt.setString(12, sp.getUserId());
			pstmt.setString(13, "Y");
			pstmt.setString(14, sp.getOriginCity());
			pstmt.setString(15, sp.getOriginState());
			if(trimString(sp.getOriginCountry()).equalsIgnoreCase("Select")) {
				pstmt.setString(16, "");
			}else {
				
				pstmt.setString(16, sp.getOriginCountry());
			}
			pstmt.setString(17, sp.getOriginName());
			pstmt.setString(18, sp.getOriginPostal());
			pstmt.setString(19, sp.getDestCity());
			pstmt.setString(20, sp.getDestState());
			if(trimString(sp.getDestCountry()).equalsIgnoreCase("Select")) {
			pstmt.setString(21, "");
			}else {
				pstmt.setString(21, sp.getDestCountry());
			}
			pstmt.setString(22, sp.getDestName());
			pstmt.setString(23, sp.getDestPostal());
			pstmt.setString(24, sp.getCarrier());
			pstmt.setString(25, sp.getDestAddress());
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			log.error("Error", e);
		}

		return 0;
	}
	/*
	 * private List<SpotRate> testData() { // LEGS100 po // 20160615A release
	 * List<SpotRate> spList = new LinkedList<>(); // revenue SpotRate rv = new
	 * SpotRate(); List<AccessorialCharge> acList = new LinkedList<>();
	 * AccessorialCharge ac1 = new AccessorialCharge(); ac1.setAmount("200.00");
	 * ac1.setCode("STORAGE"); ac1.setDescription("Storage Fee");
	 * acList.add(ac1); AccessorialCharge ac2 = new AccessorialCharge();
	 * ac2.setAmount("300.00"); ac2.setCode("DETENTION"); ac2.setDescription(
	 * "Detention Fee"); acList.add(ac2);
	 * 
	 * rv.setAccessorial(acList); rv.setBaseFreight(1000.00);
	 * rv.setCarrier("NRVV"); rv.setCarrierName("Nargel Velasco");
	 * rv.setChargeTypeCd("R"); rv.setContractReference("Q1234");
	 * rv.setCurrencyCd("USD"); rv.setCustomerCd("OPENPT"); rv.setDestCity(
	 * "New Delhi"); rv.setDestCountry("IN"); rv.setDestName("Kataria Traders");
	 * rv.setDestPostal("11018"); rv.setDestState("New Delhi State");
	 * rv.setEquipmentDesc("TRUCKLOAD"); rv.setEquipmentShipmentSeqNbr(1);
	 * rv.setEquipmentType("TL"); rv.setExchangeRt(0.0); rv.setFsc(150.00);
	 * rv.setFuelSurchargeType("FC"); rv.setLegNbr(1); rv.setMarginPct(0.00);
	 * rv.setMileage(100.00); rv.setNoOfEquipment(1); rv.setOriginCity(
	 * "New Delhi"); rv.setOriginCountry("IN"); rv.setOriginName(
	 * "Kataria Traders"); rv.setOriginPostal("11018"); rv.setOriginState(
	 * "New Delhi State"); rv.setParentPartnerCd("OPENPT");
	 * rv.setPartnerCd("OPENPT"); rv.setPoNbr("LEGS100");
	 * rv.setRateReference("SPOT"); rv.setRateType("R");
	 * rv.setReleaseNbr("20160615A"); rv.setRtWt(1.0);
	 * rv.setShipperCd("OPENPT"); rv.setTotalFreightCharge(1000.00);
	 * rv.setTransitDays(1); rv.setUserId("NRV"); rv.setWeight(12390.00);
	 * rv.setWtUom("KGS"); spList.add(rv);
	 * 
	 * rv = new SpotRate(); acList = new LinkedList<>(); ac1 = new
	 * AccessorialCharge(); ac1.setAmount("200.00"); ac1.setCode("STORAGE");
	 * ac1.setDescription("Storage Fee"); acList.add(ac1); ac2 = new
	 * AccessorialCharge(); ac2.setAmount("300.00"); ac2.setCode("DETENTION");
	 * ac2.setDescription("Detention Fee"); acList.add(ac2);
	 * 
	 * rv.setAccessorial(acList); rv.setBaseFreight(1000.00);
	 * rv.setCarrier("NRVC"); rv.setCarrierName("Nargel Velasco C");
	 * rv.setChargeTypeCd("C"); rv.setContractReference("Q1234");
	 * rv.setCurrencyCd("USD"); rv.setCustomerCd("OPENPT"); rv.setDestCity(
	 * "New Delhi"); rv.setDestCountry("IN"); rv.setDestName("Kataria Traders");
	 * rv.setDestPostal("11018"); rv.setDestState("New Delhi State");
	 * rv.setEquipmentDesc("TRUCKLOAD"); rv.setEquipmentShipmentSeqNbr(1);
	 * rv.setEquipmentType("TL"); rv.setExchangeRt(0.0); rv.setFsc(150.00);
	 * rv.setFuelSurchargeType("FC"); rv.setLegNbr(1); rv.setMarginPct(0.00);
	 * rv.setMileage(100.00); rv.setNoOfEquipment(1); rv.setOriginCity(
	 * "New Delhi"); rv.setOriginCountry("IN"); rv.setOriginName(
	 * "Kataria Traders"); rv.setOriginPostal("11018"); rv.setOriginState(
	 * "New Delhi State"); rv.setParentPartnerCd("OPENPT");
	 * rv.setPartnerCd("OPENPT"); rv.setPoNbr("LEGS100");
	 * rv.setRateReference("SPOT"); rv.setRateType("R");
	 * rv.setReleaseNbr("20160615A"); rv.setRtWt(1.0);
	 * rv.setShipperCd("OPENPT"); rv.setTotalFreightCharge(1000.00);
	 * rv.setTransitDays(1); rv.setUserId("NRV"); rv.setWeight(12390.00);
	 * rv.setWtUom("KGS"); spList.add(rv);
	 * 
	 * rv = new SpotRate(); acList = new LinkedList<>(); ac1 = new
	 * AccessorialCharge(); ac1.setAmount("200.00"); ac1.setCode("STORAGE");
	 * ac1.setDescription("Storage Fee"); acList.add(ac1); ac2 = new
	 * AccessorialCharge(); ac2.setAmount("300.00"); ac2.setCode("DETENTION");
	 * ac2.setDescription("Detention Fee"); acList.add(ac2);
	 * 
	 * rv.setAccessorial(acList); rv.setBaseFreight(1000.00);
	 * rv.setCarrier("NRVD"); rv.setCarrierName("Nargel Velasco D");
	 * rv.setChargeTypeCd("C"); rv.setContractReference("Q1234");
	 * rv.setCurrencyCd("USD"); rv.setCustomerCd("OPENPT"); rv.setDestCity(
	 * "New Delhi"); rv.setDestCountry("IN"); rv.setDestName("Kataria Traders");
	 * rv.setDestPostal("11018"); rv.setDestState("New Delhi State");
	 * rv.setEquipmentDesc("TRUCKLOAD"); rv.setEquipmentShipmentSeqNbr(1);
	 * rv.setEquipmentType("TL"); rv.setExchangeRt(0.0); rv.setFsc(150.00);
	 * rv.setFuelSurchargeType("FC"); rv.setLegNbr(1); rv.setMarginPct(0.00);
	 * rv.setMileage(100.00); rv.setNoOfEquipment(1); rv.setOriginCity(
	 * "New Delhi"); rv.setOriginCountry("IN"); rv.setOriginName(
	 * "Kataria Traders"); rv.setOriginPostal("11018"); rv.setOriginState(
	 * "New Delhi State"); rv.setParentPartnerCd("OPENPT");
	 * rv.setPartnerCd("OPENPT"); rv.setPoNbr("LEGS100");
	 * rv.setRateReference("SPOT"); rv.setRateType("R");
	 * rv.setReleaseNbr("20160615A"); rv.setRtWt(1.0);
	 * rv.setShipperCd("OPENPT"); rv.setTotalFreightCharge(1000.00);
	 * rv.setTransitDays(1); rv.setUserId("NRV"); rv.setWeight(12390.00);
	 * rv.setWtUom("KGS"); spList.add(rv);
	 * 
	 * return spList; }
	 */

	@POST // http://localhost:8080/market-api/trucker
	@Path("spotRates") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SpotRate> spotRate(List<SpotRate> spList) {
		log.info("spList size :" + spList.size());    
		for (SpotRate sp : spList) {

			Set<Integer> seqSet = new HashSet<>();

			try {

				// do cost rate first because each rate should be combined with
				// revenue
				int carrierSeq = 1;
				Integer pId = updatePoHeader(sp);
				Integer tId = createTrackAndTrace(sp.getProBillNbr(), "SR", "Spot Rate", sp.getUserId());
				Integer transportLegId = createTransportLeg(sp);
				
				Integer retLeg = createLegEquipment(sp, transportLegId);
				List<Cost> crs = sp.getCosts();
				for (Cost cost : crs) {

					List<CarrierAccessorial> cAccList = cost.getAccessorials();

					// process po relesae carrier
					double totalAccessorial = 0.0;
					for (CarrierAccessorial cAcc : cAccList) {
						if (cAcc.getChargeAmt() != null && !cAcc.getChargeCd().equalsIgnoreCase("FRT")
								&& !cAcc.getChargeCd().equalsIgnoreCase("FSC")
								&& !cAcc.getChargeCd().equalsIgnoreCase("DSCFRT")) {
							totalAccessorial = Double.valueOf(cAcc.getChargeAmt());
						}
					}

					createCostPoReleaseCarrier(cost, sp, transportLegId, cost.getTotalCharge(), totalAccessorial,
							carrierSeq, "C");
					int lineItemNbrAcc = 1;
					int lineItemNbrAc = 1;

					// FRT
					CarrierAccessorial frtCost = new CarrierAccessorial();
					if (cost.getBaseFreight() == null) {
						frtCost.setChargeAmt(0.0);
					} else {
						frtCost.setChargeAmt(cost.getBaseFreight());

					}
					frtCost.setChargeCd("FRT");
					frtCost.setChargeDescTxt("Freight");

					createCarrierCharge(frtCost, sp, transportLegId, lineItemNbrAc++, carrierSeq, "C",
							cost.getCarrierCd(), cost.getRtWt());

					// FSC
					CarrierAccessorial fscCost = new CarrierAccessorial();
					if (cost.getFsc() == null) {
						fscCost.setChargeAmt(0.0);
					} else {
						fscCost.setChargeAmt(cost.getFsc());

					}

					fscCost.setChargeCd("FSC");
					fscCost.setChargeDescTxt("Fuel Surcharge");

					createCarrierCharge(fscCost, sp, transportLegId, lineItemNbrAc++, carrierSeq, "C",
							cost.getCarrierCd(), cost.getRtWt());
					// DSC
					CarrierAccessorial dscCost = new CarrierAccessorial();
					if (cost.getDsc() == null) {
						dscCost.setChargeAmt(0.0);
					} else {
						dscCost.setChargeAmt(cost.getDsc());

					}

					dscCost.setChargeCd("DSCFRT");
					dscCost.setChargeDescTxt("Discount");

					createCarrierCharge(dscCost, sp, transportLegId, lineItemNbrAc++, carrierSeq, "C",
							cost.getCarrierCd(), cost.getRtWt());

					for (CarrierAccessorial cAcc : cAccList) {
						if (!trimString(cAcc.getChargeCd()).isEmpty()) {
							createCarrierAccessorialCharge(cAcc, sp, transportLegId, carrierSeq, lineItemNbrAcc++, "C",
									cost.getCarrierCd());
							createCarrierCharge(cAcc, sp, transportLegId, lineItemNbrAc++, carrierSeq, "C",
									cost.getCarrierCd(), cost.getRtWt());
						}
					}

					// create Revenue

					// transportLegId = createTransportLeg(revenueRates.get(0));
					// retLeg = createLegEquipment(revenueRates.get(0),
					// transportLegId);
					List<CarrierAccessorial> revAccList = sp.getRevenue().getAccessorials();

					// process po relesae carrier
					totalAccessorial = 0.0;
					for (CarrierAccessorial revAcc : revAccList) {
						if (revAcc.getChargeAmt() != null && !revAcc.getChargeCd().equalsIgnoreCase("FRT")
								&& !revAcc.getChargeCd().equalsIgnoreCase("FSC")
								&& !revAcc.getChargeCd().equalsIgnoreCase("DSCFRT")) {
							totalAccessorial = revAcc.getChargeAmt();
						}
					}
					createRevenuePoReleaseCarrier(sp.getRevenue(), sp, transportLegId, sp.getRevenue().getTotalCharge(),
							totalAccessorial, carrierSeq, "R", sp.getRevenue().getCarrierCd());
					lineItemNbrAcc = 1;
					lineItemNbrAc = 1;

					// FRT
					CarrierAccessorial frtRev = new CarrierAccessorial();
					if (sp.getRevenue().getBaseFreight() == null) {
						frtRev.setChargeAmt(0.0);
					} else {
						frtRev.setChargeAmt(sp.getRevenue().getBaseFreight());

					}
					frtRev.setChargeCd("FRT");
					frtRev.setChargeDescTxt("Freight");

					createCarrierCharge(frtRev, sp, transportLegId, lineItemNbrAc++, carrierSeq, "R",
							sp.getRevenue().getCarrierCd(), sp.getRevenue().getRtWt());

					// FSC
					CarrierAccessorial fscRev = new CarrierAccessorial();
					if (sp.getRevenue().getFsc() == null) {
						fscRev.setChargeAmt(0.0);
					} else {
						fscRev.setChargeAmt(sp.getRevenue().getFsc());

					}

					fscRev.setChargeCd("FSC");
					fscRev.setChargeDescTxt("Fuel Surcharge");
					createCarrierCharge(fscRev, sp, transportLegId, lineItemNbrAc++, carrierSeq, "R",
							sp.getRevenue().getCarrierCd(), sp.getRevenue().getRtWt());
					// DSC
					CarrierAccessorial dscRev = new CarrierAccessorial();
					if (sp.getRevenue().getDsc() == null) {
						dscRev.setChargeAmt(0.0);
					} else {
						dscRev.setChargeAmt(sp.getRevenue().getDsc());

					}

					dscRev.setChargeCd("DSCFRT");
					dscRev.setChargeDescTxt("Discount");

					createCarrierCharge(dscRev, sp, transportLegId, lineItemNbrAc++, carrierSeq, "R",
							sp.getRevenue().getCarrierCd(), sp.getRevenue().getRtWt());

					for (CarrierAccessorial revAcc : revAccList) {

						createCarrierAccessorialCharge(revAcc, sp, transportLegId, carrierSeq, lineItemNbrAcc++, "R",
								sp.getRevenue().getCarrierCd());
						createCarrierCharge(revAcc, sp, transportLegId, lineItemNbrAc++, carrierSeq, "R",
								sp.getRevenue().getCarrierCd(), sp.getRevenue().getRtWt());
					}
					carrierSeq++;
				}
				
			} catch (Exception e) {
				log.info("error", e);
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);

			}
			
		}
		
		return spList;
	}

	private Integer createTrackAndTrace(String probillNbr, String eventCd, String eventDesc, String userId) {
		log.info("Pro bill " + probillNbr);
		String sql = "insert into ods_utf8_db.data_staging (" + "activity_id," + "probill_nbr," + "application,"
				+ "status_cd," + "status_desc," + "event_dt," + "comment_txt," + "create_user_id," + "create_dt,"
				+ "update_dt," + "data_source_cd) " + "values(" + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?)";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, -1);
			pstmt.setString(2, probillNbr);
			pstmt.setString(3, "TMS");
			pstmt.setString(4, eventCd);
			pstmt.setString(5, eventDesc);
			pstmt.setTimestamp(6, new Timestamp(new Date().getTime()));
			pstmt.setString(7, "Spot Rate");
			pstmt.setString(8, userId);
			pstmt.setTimestamp(9, new Timestamp(new Date().getTime()));
			pstmt.setTimestamp(10, new Timestamp(new Date().getTime()));
			pstmt.setString(11, "MP");

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	/**
	 * steps in saving rates **
	 * 
	 * Cost 1. Update Cost PO Release carrier 2. If Cancel Then return flags to
	 * available 3. If br flag then direct dispatch 4. If Send Flag call rodel
	 * 5. Re-create carrier_charge
	 * 
	 * Revenue 1. Update Cost PO Release carrier 2. If Cancel Then return flags
	 * to available 3. If br flag then direct dispatch 4. If Send Flag call
	 * rodel 5. Re-create carrier_charge
	 *
	 * 
	 * 
	 * 
	 * 
	 */
	@GET // http://localhost:8080/market-api/truckers
	@Path("workarea/{companyId}/{countryCd}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<RatesWorkArea> fetchWorkArea(@PathParam("companyId") Integer companyId,
			@PathParam("countryCd") String countryCd) {
		log.info("Country Code : " + countryCd);
		return fetchWorkAreaByCompanyId(companyId, countryCd);
	}

	@GET // http://localhost:8080/market-api/truckers
	@Path("submitted/{companyId}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SubmittedArea> fetchSubmitted(@PathParam("companyId") Integer companyId) {
		return fetchSubmittedByCompanyId(companyId, null, null);
	}

	@GET // http://localhost:8080/market-api/truckers
	@Path("submittedbyrange") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SubmittedArea> fetchSubmitted(@QueryParam("company") Integer companyId,
			@QueryParam("dStart") String dStart, @QueryParam("dEnd") String dEnd) {

		log.info("Company " + companyId + " dStart " + dStart + " dEnd " + dEnd);
		return fetchSubmittedByCompanyId(companyId, dStart, dEnd);
	}

	@GET // http://localhost:8080/market-api/truckers
	@Path("rates/{probill}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CarrierTransportationLeg> fetchRates(@PathParam("probill") String probill) {
		List<CarrierRate> rates = new ArrayList<CarrierRate>();
		Object[] legs = new Object[3];
		List<CarrierTransportationLeg> transportationList = new LinkedList<>();
		int maxLeg = 1; // how many legs
		try {

			String sql = "select a.po_nbr," + "a.release_nbr," + "a.pro_bill_nbr," + "a.parent_partner_cd,"
					+ "a.partner_cd," + "a.shipper_cd," + "b.transport_leg_nbr," + "c.CHARGE_TYPE_CD," + "c.carrier_cd,"
					+ "c.carrier_seq_nbr," + "c.SENT_204_FLG," + "c.ACCEPT_990_FLG," + "c.REJECT_990_FLG," + "c.br_flg,"
					+ "d.carrier_cd," + "d.carrier_seq_nbr," + "d.charge_cd," + "d.charge_desc_txt," + "d.charge_amt,"
					+ "d.charge_ccy_cd," + "d.charge_type_cd," + "d.line_item_nbr," + "c.cancel_990_flg,"
					+ "a.customer_cd,"
					+ "a.equipment_shipment_seq_nbr,b.DELIVERY_CITY_NAME,b.DELIVERY_STATE_CD,b.DELIVERY_COUNTRY_CD,b.PICKUP_CITY_NAME,b.PICKUP_STATE_CD,b.PICKUP_COUNTRY_CD,"
					+ "(SELECT CASE WHEN COUNT(*) > 0 THEN (SELECT  SUM(aa.TOTAL_WT) FROM tms_utf8_db.po_header AS aa WHERE aa.PO_NBR IN (SELECT  ACTUAL_PO_NBR FROM tms_utf8_db.po_line_item WHERE po_nbr = a.PO_NBR AND a.PO_NBR <> '') "
					+ "AND aa.PO_TYPE_CD = 'LPO' AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD AND aa.PARTNER_CD = a.PARTNER_CD AND aa.CUSTOMER_CD = a.CUSTOMER_CD AND aa.SHIPPER_CD = a.SHIPPER_CD) ELSE a.TOTAL_WT END FROM tms_utf8_db.po_line_item li "
					+ "WHERE a.PO_NBR = li.po_nbr AND a.PO_NBR <> '' AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD AND a.PARTNER_CD = li.PARTNER_CD AND a.SHIPPER_CD = li.SHIPPER_CD AND a.CUSTOMER_CD = li.CUSTOMER_CD AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS total_wt,"
					+ "a.wt_uom,"
					+ "c.sent_204_tstamp,c.cancel_990_tstamp,b.transport_leg_id,a.equipment_shipment_seq_nbr,c.equipment_type_cd,a.customer_cd,a.po_type_cd,d.rate "
					+ " from po_header a inner join transportation_leg b on  a.PARENT_PARTNER_CD = b.PARENT_PARTNER_CD and "
					+ "b.PARTNER_CD = a.PARTNER_CD AND b.SHIPPER_CD = a.SHIPPER_CD AND b.CUSTOMER_CD = a.CUSTOMER_CD AND b.PO_NBR = a.PO_NBR AND b.RELEASE_NBR = a.RELEASE_NBR AND b.EQUIPMENT_SHIPMENT_SEQ_NBR = a.EQUIPMENT_SHIPMENT_SEQ_NBR\n"
					+ "and a.pro_bill_Nbr='" + probill
					+ "' inner join po_release_carrier c on b.TRANSPORT_LEG_ID = c.transport_leg_id left outer join carrier_charge d on c.TRANSPORT_LEG_ID = d.transport_leg_id and c.CARRIER_SEQ_NBR = d.CARRIER_SEQ_NBR and d.carrier_cd=c.carrier_cd and c.CHARGE_TYPE_CD = d.CHARGE_TYPE_CD\n"
					+ "order by b.transport_leg_nbr,c.CARRIER_SEQ_NBR,c.carrier_cd,c.charge_type_cd";
			log.info("sql " + sql);
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						CarrierRate carrier = new CarrierRate();
						carrier.setPoNbr(rs.getString(1));
						carrier.setReleaseNbr(rs.getString(2));
						carrier.setProBillNbr(rs.getString(3));
						carrier.setParentPartnerCd(rs.getString(4));
						carrier.setPartnerCd(rs.getString(5));
						carrier.setShipperCd(rs.getString(6));
						carrier.setTransportLegNbr(rs.getInt(7));
						carrier.setChargeTypeCd(rs.getString(8));
						carrier.setCarrierCd(rs.getString(9));
						carrier.setCarrierSeqNbr(rs.getInt(10));
						carrier.setSent204Flg(rs.getBoolean(11));
						carrier.setAccept990Flg(rs.getBoolean(12));
						carrier.setReject990Flg(rs.getBoolean(13));
						carrier.setBrFlg(rs.getBoolean(14));
						carrier.setAccessorialCarrierCd(rs.getString(15));
						carrier.setAccessorialCarrierSeqNbr(rs.getInt(16));
						carrier.setAccessorialChargeCd(rs.getString(17));
						carrier.setAccessorialChargeDescTxt(rs.getString(18));
						carrier.setAccessorialChargeAmt(rs.getDouble(19));
						carrier.setAccessorialChargeCcyCd(rs.getString(20));
						carrier.setAccessorialChargeTypeCd(rs.getString(21));
						carrier.setAccessorialLineItemNbr(rs.getInt(22));
						carrier.setCancelFlg(rs.getBoolean(23));
						carrier.setCustomerCd(rs.getString(24));
						carrier.setEquipmentShipmentSeqNbr(rs.getInt(25));
						carrier.setDeliveryCityName(rs.getString(26));
						carrier.setDeliveryState(rs.getString(27));
						carrier.setDeliveryCountry(rs.getString(28));
						carrier.setPickupCityName(rs.getString(29));
						carrier.setPickupState(rs.getString(30));
						carrier.setPickupCountry(rs.getString(31));
						carrier.setTotalWt(rs.getDouble(32));
						carrier.setWtUom(rs.getString(33));
						carrier.setSent204Tstamp(rs.getTimestamp(34));
						carrier.setCancelTstamp(rs.getTimestamp(35));
						carrier.setTransportLegId(rs.getInt(36));
						carrier.setEquipmentShipmentSeqNbr(rs.getInt(37));
						carrier.setEquipmentTypeCd(rs.getString(38));
						carrier.setCustomerCd(rs.getString(39));
						carrier.setPoTypeCd(rs.getString(40));
						carrier.setRtWt(rs.getDouble(41));
						if (carrier.getBrFlg()) {
							carrier.setIsBrFlg(true);
						} else {
							carrier.setIsBrFlg(false);
						}
						if (carrier.getCancelFlg()) {
							carrier.setIsCancelFlg(true);
						} else {
							carrier.setIsCancelFlg(false);
						}
						if (carrier.getAccept990Flg()) {
							carrier.setIsAccept990Flg(true);
						} else {
							carrier.setIsAccept990Flg(false);

						}
						if (carrier.getSent204Flg()) {
							carrier.setIsSent204Flg(true);
						} else {
							carrier.setIsSent204Flg(false);
						}
						if (carrier.getReject990Flg()) {
							carrier.setIsReject990Flg(true);
						} else {
							carrier.setIsReject990Flg(false);
						}

						maxLeg = Math.max(maxLeg, carrier.getTransportLegNbr());
						rates.add(carrier);
					}

					// separate legs
					legs = new Object[maxLeg];
					for (int i = 0; i < maxLeg; i++) {
						List<CarrierRate> list = new LinkedList<>();
						for (CarrierRate cr : rates) {
							if (cr.getTransportLegNbr() == i + 1) {
								list.add(cr);
							}
						}
						legs[i] = list;
					}

					// get rates

					for (int x = 0; x < legs.length; x++) {
						List<CarrierRate> leg = (List<CarrierRate>) legs[x]; // manipulate
																				// carrier
																				// rate

						// Revenue for each leg
						Map<String, List<CarrierRate>> revMap = new HashMap<>();
						for (CarrierRate crRevenue : leg) {
							if (crRevenue.getChargeTypeCd().equalsIgnoreCase("R")) {
								String revKeyword = crRevenue.getChargeTypeCd() + "-" + crRevenue.getCarrierCd() + "-"
										+ crRevenue.getCarrierSeqNbr();
								List<CarrierRate> tempList = new LinkedList<>();
								if (revMap.get(revKeyword) != null) {
									tempList = revMap.get(revKeyword);
								}
								tempList.add(crRevenue);
								revMap.put(revKeyword, tempList);

							}
						}
						Map<String, List<CarrierRate>> costMap = new HashMap<>();

						// cost for each leg
						for (CarrierRate crCost : leg) {
							if (crCost.getChargeTypeCd().equalsIgnoreCase("C")) {
								String costKeyword = crCost.getChargeTypeCd() + "-" + crCost.getCarrierCd();
								List<CarrierRate> tempList = new LinkedList<>();
								if (costMap.get(costKeyword) != null) {
									tempList = costMap.get(costKeyword);
								}
								tempList.add(crCost);
								costMap.put(costKeyword, tempList);

							}
						}

						List<CarrierRate> revenueRates = new LinkedList();
						List<CarrierRate> costRates = new LinkedList();

						// manipulate Revenue

						Set<Entry<String, List<CarrierRate>>> eSet = revMap.entrySet(); // keyword
																						// with
																						// accessorial
																						// charge
						double rtWt = 0.0;
						double baseRt = 0.0;
						double discount = 0.0;
						double fsc = 0.0;
						double totalAcc = 0.0;
						double totalCharge = 0.0;
						for (Entry<String, List<CarrierRate>> e : eSet) {
							List<CarrierRate> lRc = e.getValue();
							List<CarrierAccessorial> revAccessorial = new LinkedList();
							CarrierRate lrc1 = new CarrierRate();

							for (CarrierRate fRev : lRc) {
								CarrierAccessorial lrAcc = new CarrierAccessorial();
								lrc1.setRtWt(fRev.getRtWt());
								lrc1.setAccept990Flg(fRev.getAccept990Flg());
								lrc1.setBrFlg(fRev.getBrFlg());
								lrc1.setCarrierCd(fRev.getCarrierCd());
								lrc1.setCarrierSeqNbr(fRev.getCarrierSeqNbr());
								lrc1.setChargeTypeCd(fRev.getChargeTypeCd());
								lrc1.setParentPartnerCd(fRev.getParentPartnerCd());
								lrc1.setPartnerCd(fRev.getPartnerCd());
								lrc1.setShipperCd(fRev.getShipperCd());
								lrc1.setPoNbr(fRev.getPoNbr());
								lrc1.setEquipmentShipmentSeqNbr(fRev.getEquipmentShipmentSeqNbr());
								lrc1.setReleaseNbr(fRev.getReleaseNbr());
								lrc1.setSent204Flg(fRev.getSent204Flg());
								lrc1.setTransportLegNbr(fRev.getTransportLegNbr());
								lrc1.setTransportLegId(fRev.getTransportLegId());
								lrc1.setCancelFlg(fRev.getCancelFlg());
								lrc1.setReject990Flg(fRev.getReject990Flg());
								lrc1.setEquipmentTypeCd(fRev.getEquipmentTypeCd());
								lrc1.setCustomerCd(fRev.getCustomerCd());
								lrc1.setIsAccept990Flg(fRev.getIsAccept990Flg());
								lrc1.setIsBrFlg(fRev.getIsBrFlg());
								lrc1.setIsCancelFlg(fRev.getIsCancelFlg());
								lrc1.setIsReject990Flg(fRev.getIsReject990Flg());
								lrc1.setIsSent204Flg(fRev.getIsSent204Flg());
								lrc1.setPoTypeCd(fRev.getPoTypeCd());
								lrc1.setProBillNbr(fRev.getProBillNbr());

								// foreign exchange API must be called
								lrc1.setExchangeRt(1.0);

								if (fRev.getTotalWt() != null) {
									lrc1.setWeight(""
											+ new BigDecimal(fRev.getTotalWt()).setScale(2, BigDecimal.ROUND_HALF_UP));
									if (fRev.getWtUom() != null) {
										lrc1.setWeight(lrc1.getWeight() + " " + fRev.getWtUom());
									}
								}

								if (lrc1.getRtWt() != null) {
									rtWt = lrc1.getRtWt();
								}

								totalCharge += fRev.getAccessorialChargeAmt();
								if (fRev.getAccessorialChargeCd() != null
										&& fRev.getAccessorialChargeCd().equalsIgnoreCase("FRT")) {
									baseRt = fRev.getAccessorialChargeAmt();
								} else if (fRev.getAccessorialChargeCd() != null
										&& fRev.getAccessorialChargeCd().equalsIgnoreCase("DSCFRT")) {
									discount = fRev.getAccessorialChargeAmt();
								} else if (fRev.getAccessorialChargeCd() != null
										&& fRev.getAccessorialChargeCd().equalsIgnoreCase("FSC")) {
									fsc = fRev.getAccessorialChargeAmt();
								} else {
									totalAcc += fRev.getAccessorialChargeAmt();
								}

								lrAcc.setChargeAmt(fRev.getAccessorialChargeAmt());
								lrAcc.setChargeCcyCd(fRev.getAccessorialChargeCcyCd());
								lrAcc.setChargeCd(fRev.getAccessorialChargeCd());
								lrAcc.setChargeDescTxt(fRev.getAccessorialChargeDescTxt());
								lrAcc.setLineItemNbr(fRev.getAccessorialLineItemNbr());
								revAccessorial.add(lrAcc);
							}
							lrc1.setTotalBase(baseRt);
							lrc1.setTotalDiscount(discount);
							lrc1.setTotalAccessorial(totalAcc);
							lrc1.setTotalBase(baseRt);
							lrc1.setTotalFsc(fsc);
							lrc1.setTotalCharge(totalCharge);
							lrc1.setAccessorials(revAccessorial);
							revenueRates.add(lrc1);
							break; // only one record for revenue
						}

						// manipulate cost
						Set<Entry<String, List<CarrierRate>>> ecSet = costMap.entrySet(); // keyword
																							// with
																							// accessorial
																							// charge
						CarrierRate lcc1 = new CarrierRate();

						for (Entry<String, List<CarrierRate>> e : ecSet) {
							List<CarrierRate> lCc = e.getValue();
							List<CarrierAccessorial> costAccessorial = new LinkedList();
							CarrierRate lrc1 = new CarrierRate();
							rtWt = 0.0;
							baseRt = 0.0;
							discount = 0.0;
							fsc = 0.0;
							totalAcc = 0.0;
							totalCharge = 0.0;
							for (CarrierRate cRev : lCc) {
								CarrierAccessorial lrAcc = new CarrierAccessorial();
								lrc1.setRtWt(cRev.getRtWt());
								lrc1.setAccept990Flg(cRev.getAccept990Flg());
								lrc1.setBrFlg(cRev.getBrFlg());
								lrc1.setCarrierCd(cRev.getCarrierCd());
								lrc1.setCarrierSeqNbr(cRev.getCarrierSeqNbr());
								lrc1.setChargeTypeCd(cRev.getChargeTypeCd());
								lrc1.setParentPartnerCd(cRev.getParentPartnerCd());
								lrc1.setPartnerCd(cRev.getPartnerCd());
								lrc1.setShipperCd(cRev.getShipperCd());
								lrc1.setPoNbr(cRev.getPoNbr());
								lrc1.setReleaseNbr(cRev.getReleaseNbr());
								lrc1.setSent204Flg(cRev.getSent204Flg());
								lrc1.setTransportLegNbr(cRev.getTransportLegNbr());
								lrc1.setTransportLegId(cRev.getTransportLegId());
								lrc1.setCancelFlg(cRev.getCancelFlg());
								lrc1.setReject990Flg(cRev.getReject990Flg());
								lrc1.setEquipmentShipmentSeqNbr(cRev.getEquipmentShipmentSeqNbr());
								lrc1.setExchangeRt(1.0);
								lrc1.setIsAccept990Flg(cRev.getIsAccept990Flg());
								lrc1.setIsBrFlg(cRev.getIsBrFlg());
								lrc1.setIsCancelFlg(cRev.getIsCancelFlg());
								lrc1.setIsReject990Flg(cRev.getIsReject990Flg());
								lrc1.setIsSent204Flg(cRev.getIsSent204Flg());
								lrc1.setEquipmentTypeCd(cRev.getEquipmentTypeCd());
								lrc1.setCustomerCd(cRev.getCustomerCd());
								lrc1.setPoTypeCd(cRev.getPoTypeCd());
								lrc1.setProBillNbr(cRev.getProBillNbr());
								if (cRev.getTotalWt() != null) {
									lrc1.setWeight(""
											+ new BigDecimal(cRev.getTotalWt()).setScale(2, BigDecimal.ROUND_HALF_UP));
									if (cRev.getWtUom() != null) {
										lrc1.setWeight(lrc1.getWeight() + " " + cRev.getWtUom());
									}
								}

								if (lrc1.getRtWt() != null) {
									rtWt = lrc1.getRtWt();
								}

								totalCharge += cRev.getAccessorialChargeAmt();
								if (cRev.getAccessorialChargeCd() != null
										&& cRev.getAccessorialChargeCd().equalsIgnoreCase("FRT")) {
									baseRt = cRev.getAccessorialChargeAmt();
								} else if (cRev.getAccessorialChargeCd() != null
										&& cRev.getAccessorialChargeCd().equalsIgnoreCase("DSCFRT")) {
									discount = cRev.getAccessorialChargeAmt();
								} else if (cRev.getAccessorialChargeCd() != null
										&& cRev.getAccessorialChargeCd().equalsIgnoreCase("FSC")) {
									fsc = cRev.getAccessorialChargeAmt();
								} else {
									totalAcc += cRev.getAccessorialChargeAmt();
								}

								lrAcc.setChargeAmt(cRev.getAccessorialChargeAmt());
								lrAcc.setChargeCcyCd(cRev.getAccessorialChargeCcyCd());
								lrAcc.setChargeCd(cRev.getAccessorialChargeCd());
								lrAcc.setChargeDescTxt(cRev.getAccessorialChargeDescTxt());
								lrAcc.setLineItemNbr(cRev.getAccessorialLineItemNbr());
								costAccessorial.add(lrAcc);
							}
							lrc1.setTotalBase(baseRt);
							lrc1.setTotalDiscount(discount);
							lrc1.setTotalAccessorial(totalAcc);
							lrc1.setTotalBase(baseRt);
							lrc1.setTotalFsc(fsc);

							lrc1.setTotalCharge(totalCharge);

							lrc1.setAccessorials(costAccessorial);
							costRates.add(lrc1);

						}

						CarrierTransportationLeg transportation = new CarrierTransportationLeg();
						transportation.setRevenue(revenueRates);
						transportation.setCost(costRates);
						transportation.setLegNbr(leg.get(0).getTransportLegNbr());

						String legRoute = "";
						String origin = leg.get(0).getPickupCityName();
						if (leg.get(0).getPickupState() != null) {
							origin += "," + leg.get(0).getPickupState();
						}
						String destination = leg.get(0).getDeliveryCityName();
						if (leg.get(0).getDeliveryState() != null) {
							destination += "," + leg.get(0).getDeliveryState();
						}
						transportation.setOrigin(origin);
						transportation.setDestination(destination);
						transportationList.add(transportation);

					}
					log.info("transportation List " + transportationList);
					return transportationList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	/*
	 * private void do_CarrierRates(List<CarrierRate> rates) { List<CarrierRate>
	 * costRates = new LinkedList<>(); CarrierRate revRate = new CarrierRate();
	 * Set<Integer> seqSet = new HashSet<>(); for (CarrierRate c : rates) { if
	 * (c.getChargeTypeCd().equalsIgnoreCase("C")) {
	 * seqSet.add(c.getCarrierSeqNbr()); costRates.add(c); } if
	 * (c.getChargeTypeCd().equalsIgnoreCase("R")) { revRate = c; }
	 * 
	 * }
	 * 
	 * updateRevenuePoReleaseCarrier(revRate);
	 * 
	 * updateRevenueCarrierCharge(revRate, seqSet);
	 * 
	 * for (CarrierRate crRate : costRates) {
	 * updateCostPoReleaseCarrier(crRate);
	 * 
	 * updateCostCarrierCharge(crRate);
	 * 
	 * }
	 * 
	 * }
	 */
	/*
	 * not needed private void updateRevenueCarrierCharge(CarrierRate revRate,
	 * Set<Integer> seqSet) { // delete all Revenue carrier charge
	 * do_DeleteRevenueCarrierCharge(revRate);
	 * 
	 * for (Integer i : seqSet) { List<CarrierAccessorial> crSet =
	 * revRate.getAccessorials(); int iLineNbr = 1; for (CarrierAccessorial cr :
	 * crSet) { do_InsertRevenueCarrierCharge(revRate, cr, i, iLineNbr++); }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * private void updateCostCarrierCharge(CarrierRate rate) { // delete all
	 * Revenue carrier charge do_DeleteCostCarrierCharge(rate);
	 * 
	 * List<CarrierAccessorial> crSet = rate.getAccessorials(); int iLineNbr =
	 * 1; for (CarrierAccessorial cr : crSet) {
	 * do_InsertRevenueCarrierCharge(rate, cr, rate.getCarrierSeqNbr(),
	 * iLineNbr++); }
	 * 
	 * }
	 */
	/*
	 * private void updateCostPoReleaseCarrier(CarrierRate rate) {
	 * List<CarrierAccessorial> cList = rate.getAccessorials(); Double totalFrt
	 * = 0.0; Double totalFsc = 0.0; Double totalAcc = 0.0; Double totalDisc =
	 * 0.0; Double totalAmt = 0.0; for (CarrierAccessorial acc : cList) { if
	 * (acc.getChargeCd().equalsIgnoreCase("FRT")) { totalFrt =
	 * acc.getChargeAmt(); } else if (acc.getChargeCd().equalsIgnoreCase("FSC"))
	 * { totalFsc = acc.getChargeAmt(); } else if
	 * (acc.getChargeCd().equalsIgnoreCase("DSCFRT")) { totalDisc = 0.0; } else
	 * { totalAcc += acc.getChargeAmt(); } } totalAmt = totalFrt + totalFsc +
	 * totalAcc - (Math.abs(totalDisc));
	 * do_RevenueUpdatePoReleaseStatement(rate, totalAmt, totalFrt, totalFsc,
	 * totalAcc);
	 * 
	 * }
	 * 
	 * private void updateRevenuePoReleaseCarrier(CarrierRate rate) {
	 * List<CarrierAccessorial> cList = rate.getAccessorials(); Double totalFrt
	 * = 0.0; Double totalFsc = 0.0; Double totalAcc = 0.0; Double totalDisc =
	 * 0.0; Double totalAmt = 0.0; for (CarrierAccessorial acc : cList) { if
	 * (acc.getChargeCd().equalsIgnoreCase("FRT")) { totalFrt =
	 * acc.getChargeAmt(); } else if (acc.getChargeCd().equalsIgnoreCase("FSC"))
	 * { totalFsc = acc.getChargeAmt(); } else if
	 * (acc.getChargeCd().equalsIgnoreCase("DSCFRT")) { totalDisc = 0.0; } else
	 * { totalAcc += acc.getChargeAmt(); } } totalAmt = totalFrt + totalFsc +
	 * totalAcc - (Math.abs(totalDisc));
	 * do_RevenueUpdatePoReleaseStatement(rate, totalAmt, totalFrt, totalFsc,
	 * totalAcc);
	 * 
	 * }
	 */

	private String validateCommodityCode(String code) {

		String sql = "select commodity_cd from commodity_code where commodity_cd=?";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, code);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CommodityCode> driverRawMapper = new ResultSetMapper<CommodityCode>();
				List<CommodityCode> pojoList = driverRawMapper.mapResultSetToObject(rs, CommodityCode.class);
				// List<Driver> nw_pojoList = new LinkedList<Driver>();
				if (pojoList != null && !pojoList.isEmpty()) {
					return code;
				}

			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	private List<Equipment> getCompEquipment(String countryCd) {
		List<Equipment> eList = new LinkedList<>();
		String sql = "select distinct equipment_type_id,equipment_type from vw_carrier_info where country_cd='"
				+ countryCd + "'";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					Equipment e = new Equipment();
					e.setEquipmentTypeId(rs.getInt(1));
					e.setEquipmentName(rs.getString(2));
					eList.add(e);
				}
				return eList;
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	private List<PoHeader> fetch_LPOs(String vpoNbr) {

		String sql = "select distinct a.parent_partner_cd,a.partner_cd,a.shipper_cd,a.customer_cd,a.actual_po_nbr,a.actual_po_release_nbr,(select b.pro_bill_nbr from po_header b where a.parent_partner_cd=b.parent_partner_cd and a.partner_cd=b.partner_cd and a.actual_po_Nbr=b.po_nbr and a.actual_po_release_nbr=b.release_nbr) 'pro_bill_nbr' from po_line_item a where po_nbr=?";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, vpoNbr);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<PoHeader> driverRawMapper = new ResultSetMapper<PoHeader>();
				List<PoHeader> pojoList = driverRawMapper.mapResultSetToObject(rs, PoHeader.class);
				// List<Driver> nw_pojoList = new LinkedList<Driver>();
				if (pojoList != null && !pojoList.isEmpty()) {
					return pojoList;
				}

			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	private Integer do_RevenueUpdatePoReleaseStatement(CarrierRate rate, Double totalRevenue, Double totalAmt,
			Double totalFrt, Double totalFsc, Double totalAcc) {

		String sql = "update po_release_carrier  set " + "total_revenue_amt=?," + "" + "total_transport_amt=?,"
				+ "fsc_charge_amt=?," + "frt_charge_amt=?," + "total_accessorial_cost_amt=?," + "update_tstamp=now(),"
				+ "update_user_id=?"

				+ " where po_nbr=? and release_nbr=? and parent_partner_cd=? and partner_cd=? and shipper_cd=? and carrier_cd=? and charge_type_cd='R'";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setDouble(1, new BigDecimal(totalRevenue).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(2, new BigDecimal(totalAmt).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(3, new BigDecimal(totalFsc).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(4, new BigDecimal(totalFrt).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(5, new BigDecimal(totalAcc).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setString(6, rate.getUserId());

			pstmt.setString(7, rate.getPoNbr());
			pstmt.setString(8, rate.getReleaseNbr());
			pstmt.setString(9, rate.getParentPartnerCd());
			pstmt.setString(10, rate.getPartnerCd());
			pstmt.setString(11, rate.getShipperCd());
			pstmt.setString(12, rate.getCarrierCd());

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer do_CostUpdatePoReleaseStatement(CarrierRate rate, Double totalRevenue, Double totalAmt,
			Double totalFrt, Double totalFsc, Double totalAcc, int legNumber) {
		log.info("rates " + rate);
		log.info("total base " + totalFrt);
		if (rate.getCancelTstamp() == null) {
			rate.setCancelTstamp(new Timestamp(new Date().getTime()));
		}
		if (rate.getSent204Tstamp() == null) {
			rate.setSent204Tstamp(new Timestamp(new Date().getTime()));
		}

		String sql = "update po_release_carrier  set " + "total_revenue_amt=?," + "" + "total_transport_amt=?,"
				+ "fsc_charge_amt=?," + "frt_charge_amt=?," + "total_accessorial_cost_amt=?," + "update_tstamp=now(),"
				+ "update_user_id=?," + "sent_204_flg=?," + "sent_204_tstamp=?," + "cancel_990_flg=?,"
				+ "cancel_990_tstamp=?," + "br_flg=?,"

				+ "reject_990_flg=?," + "accept_990_flg=?"
				+ " where po_nbr=? and release_nbr=? and parent_partner_cd=? and partner_cd=? and shipper_cd=? and carrier_cd=? and charge_type_cd='C' and transport_leg_nbr = ?";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setDouble(1, new BigDecimal(totalRevenue).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(2, new BigDecimal(totalAmt).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(3, new BigDecimal(totalFsc).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(4, new BigDecimal(totalFrt).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setDouble(5, new BigDecimal(totalAcc).setScale(2, java.math.BigDecimal.ROUND_UP).doubleValue());
			pstmt.setString(6, rate.getUserId());
			pstmt.setString(7, rate.getSent204Flg() ? "Y" : "N");
			pstmt.setTimestamp(8, rate.getSent204Tstamp());
			pstmt.setString(9, rate.getCancelFlg() ? "Y" : "N");
			pstmt.setTimestamp(10, rate.getCancelTstamp());
			pstmt.setString(11, rate.getBrFlg() ? "Y" : "N");

			pstmt.setString(12, rate.getReject990Flg() ? "Y" : "N");
			pstmt.setString(13, rate.getAccept990Flg() ? "Y" : "N");

			pstmt.setString(14, rate.getPoNbr());
			pstmt.setString(15, rate.getReleaseNbr());
			pstmt.setString(16, rate.getParentPartnerCd());
			pstmt.setString(17, rate.getPartnerCd());
			pstmt.setString(18, rate.getShipperCd());
			pstmt.setString(19, rate.getCarrierCd());
			pstmt.setInt(20,  legNumber);

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer do_updateCancelCarrierLoadTender(String userId, String poNbr, String releaseNbr,
			String parentPartnerCd, String partnerCd, String shipperCd, String carrierCd) {
		String sql = "update po_release_carrier  set " + "update_tstamp=now()," + "update_user_id=?,"
				+ "sent_204_flg='Y'," + "sent_204_tstamp=now()," + "cancel_990_flg='N'," + "cancel_990_tstamp=null,"
				+ "br_flg='N'," + "reject_990_flg='N'," + "received_990_tstamp=null," + "assign_204_tstamp=null,"
				+ "accept_990_flg='N'"

				+ " where po_nbr=? and release_nbr=? and parent_partner_cd=? and partner_cd=? and shipper_cd=? and carrier_cd=? and charge_type_cd='C'";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, userId);
			pstmt.setString(2, poNbr);
			pstmt.setString(3, releaseNbr);
			pstmt.setString(4, parentPartnerCd);
			pstmt.setString(5, partnerCd);
			pstmt.setString(6, shipperCd);
			pstmt.setString(7, carrierCd);

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer do_resetCarrierLoadTenderFlgs(String userId, String poNbr, String releaseNbr,
			String parentPartnerCd, String partnerCd, String shipperCd, String carrierCd) {

		String sql = "update po_release_carrier  set " + "update_tstamp=now()," + "update_user_id=?,"
				+ "sent_204_flg='N'," + "sent_204_tstamp=null," + "cancel_990_flg='Y'," + "cancel_990_tstamp=now(),"
				+ "br_flg='N'," + "reject_990_flg='N'," + "received_990_tstamp=null," + "assign_204_tstamp=null,"
				+ "accept_990_flg='N'"

				+ " where po_nbr=? and release_nbr=? and parent_partner_cd=? and partner_cd=? and shipper_cd=? and carrier_cd=? and charge_type_cd='C'";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, userId);
			pstmt.setString(2, poNbr);
			pstmt.setString(3, releaseNbr);
			pstmt.setString(4, parentPartnerCd);
			pstmt.setString(5, partnerCd);
			pstmt.setString(6, shipperCd);
			pstmt.setString(7, carrierCd);
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	/*
	 * private Integer do_CarrierCancelUpdatePoReleaseStatement(String
	 * userId,String poNbr,String releaseNbr,String parentPartnerCd,String
	 * partnerCd,String shipperCd,String carrierCd) {
	 * 
	 * 
	 * 
	 * 
	 * String sql = "update po_release_carrier  set " + "update_tstamp=now()," +
	 * "update_user_id=?," + "sent_204_flg='N'," + "sent_204_tstamp=null," +
	 * "cancel_990_flg='Y'," + "cancel_990_tstamp=now()," + "br_flg='N'," +
	 * "reject_990_flg='N'," + "accept_990_flg='N'," + "assign_204_tstamp=null"
	 * +
	 * " where po_nbr=? and release_nbr=? and parent_partner_cd=? and partner_cd=? and shipper_cd=? and carrier_cd=? and charge_type_cd='C'"
	 * ;
	 * 
	 * 
	 * try (java.sql.Connection connection =
	 * DbConfigTMS.getDataSource().getConnection(); PreparedStatement pstmt =
	 * connection.prepareStatement(sql);) { pstmt.setString(1, userId);
	 * pstmt.setString(2, poNbr); pstmt.setString(3, releaseNbr);
	 * pstmt.setString(4, parentPartnerCd); pstmt.setString(5, partnerCd);
	 * pstmt.setString(6, shipperCd); pstmt.setString(7, carrierCd);
	 * 
	 * pstmt.executeUpdate(); return 1; } catch (Exception e) { log.error(
	 * "Error message: " + e, e); new
	 * ServiceException(Response.Status.CONFLICT); } return 0; }
	 */
	private Integer do_DeleteRevenueCarrierCharge(CarrierRate rate) {
		String sql = "delete from carrier_charge where po_nbr=? and release_nbr=? and parent_partner_cd=? and partner_cd=? and shipper_cd=? and carrier_cd=? and charge_type_cd='R' and transport_leg_nbr=?";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, rate.getPoNbr());
			pstmt.setString(2, rate.getReleaseNbr());
			pstmt.setString(3, rate.getParentPartnerCd());
			pstmt.setString(4, rate.getPartnerCd());
			pstmt.setString(5, rate.getShipperCd());
			pstmt.setString(6, rate.getCarrierCd());
			pstmt.setInt(7, rate.getTransportLegNbr());

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private Integer insertCommodityCd(String code) {
		String sql = "insert into commodity_code (commodity_cd,commodity_desc_txt,category_cd) values(?,?,'*')";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, code);
			pstmt.setString(2, code);

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private Integer do_DeleteCostCarrierCharge(CarrierRate rate) {
		log.info("rate " + rate.toString());
		String sql = "delete from carrier_charge where po_nbr=? and release_nbr=? and parent_partner_cd=? and partner_cd=? and shipper_cd=? and carrier_cd=? and carrier_seq_nbr=? and charge_type_cd='C' and transport_leg_nbr=?";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, rate.getPoNbr());
			pstmt.setString(2, rate.getReleaseNbr());
			pstmt.setString(3, rate.getParentPartnerCd());
			pstmt.setString(4, rate.getPartnerCd());
			pstmt.setString(5, rate.getShipperCd());
			pstmt.setString(6, rate.getCarrierCd());
			pstmt.setInt(7, rate.getCarrierSeqNbr());
			pstmt.setInt(8, rate.getTransportLegNbr());

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private Integer deleteWorkArea(int id) {
		String sql = "delete from company_contract_tender_lane where group_tender_id=" + id;

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private Integer deleteWorkAreas(List<String> ids) {
		// String ins = "";
		// for(String id : ids) {
		// ins+=id+",";
		// }
		// ins=ins.substring(0,ins.length()-1);
		String sql = "delete from company_contract_tender_lane where group_tender_id in ("
				+ StringUtils.join(ids.toArray(), ",") + ")";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private Integer do_InsertRevenueCarrierCharge(CarrierRate rate, CarrierAccessorial cracc, Integer carrierSeqNbr,
			Integer lineItemNbr) {
		
		log.info("Leg number " + rate.getTransportLegNbr() + " rate type" + rate.getChargeTypeCd() + " "+rate.getTransportLegId() + " " + rate.getCarrierCd());
		if (cracc.getLineItemNbr() < 0) {
			AccessorialCode code = fetchAccessorialByCode(cracc.getChargeCd(), rate.getParentPartnerCd());
			if (code != null) {
				cracc.setChargeDescTxt(code.getDescription());
			}
		}
		String sql = "insert into carrier_charge (" + "carrier_cd," + "charge_type_cd," + "equipment_type_cd,"
				+ "line_item_nbr," + "transport_leg_id," + "carrier_seq_nbr," + "transport_leg_nbr," + "charge_amt,"
				+ "usd_charge_amt," + "charge_ccy_cd," + "charge_cd," + "charge_desc_txt," + "create_tstamp,"
				+ "create_user_id," + "print_cd," + "exchange_rt," + "rate," + "reference_value," + "customer_cd,"
				+ "parent_partner_cd," + "partner_cd," + "shipper_cd," + "po_nbr," + "release_nbr,"
				+ "equipment_shipment_seq_nbr) values (" + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?)";
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			pstmt.setString(1, rate.getCarrierCd()); // carrier code
			pstmt.setString(2, rate.getChargeTypeCd()); // charge type
			pstmt.setString(3, rate.getEquipmentTypeCd()); // equipment type
															// code
			pstmt.setInt(4, lineItemNbr); // line item number
			pstmt.setInt(5, rate.getTransportLegId()); // leg id
			pstmt.setInt(6, carrierSeqNbr); // carrier sequence numbe
			pstmt.setInt(7, rate.getTransportLegNbr()); // transport leg number
			if (trimString(cracc.getChargeCd()).equalsIgnoreCase("FRT")) {
				pstmt.setDouble(8, rate.getTotalBase());
				cracc.setChargeAmt(rate.getTotalBase());
			} else if (trimString(cracc.getChargeCd()).equalsIgnoreCase("FSC")) {
				pstmt.setDouble(8, rate.getTotalFsc());
				cracc.setChargeAmt(rate.getTotalFsc());
			} else if (trimString(cracc.getChargeCd()).equalsIgnoreCase("DSCFRT")) {
				pstmt.setDouble(8, rate.getTotalDiscount());
				cracc.setChargeAmt(rate.getTotalDiscount());
			} else if (!trimString(cracc.getChargeCd()).isEmpty()) {
				pstmt.setDouble(8, cracc.getChargeAmt()); // charge amount
			} else {
				pstmt.setDouble(8, 0.0); // charge amount
			}
			pstmt.setDouble(9, (rate.getExchangeRt() * cracc.getChargeAmt())); // us
																				// charge
			pstmt.setString(10, cracc.getChargeCcyCd()); // currency
			pstmt.setString(11, cracc.getChargeCd()); // accessorial charge code
			pstmt.setString(12, cracc.getChargeDescTxt()); // accessorial charge
															// description
			pstmt.setTimestamp(13, new Timestamp(new Date().getTime())); // create
																			// tstamp
			pstmt.setString(14, rate.getUserId()); // user id

			if (cracc.getChargeAmt() <= 0) {
				pstmt.setString(15, "DNP"); // print code

			} else {
				pstmt.setString(15, "");
			}
			pstmt.setDouble(16, rate.getExchangeRt()); // exchange rt

			pstmt.setDouble(17, rate.getRtWt()); // rate
			pstmt.setString(18, ""); // reference value
			pstmt.setString(19, rate.getCustomerCd());// customer cde
			pstmt.setString(20, rate.getParentPartnerCd());// customer cde
			pstmt.setString(21, rate.getPartnerCd());// customer cde
			pstmt.setString(22, rate.getShipperCd());// customer cde
			pstmt.setString(23, rate.getPoNbr());// customer cde
			pstmt.setString(24, rate.getReleaseNbr());// customer cde
			pstmt.setInt(25, rate.getEquipmentShipmentSeqNbr());// customer cde

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer do_InsertCostCarrierCharge(CarrierRate rate, CarrierAccessorial cracc, Integer carrierSeqNbr,
			Integer lineItemNbr) {
		if (cracc.getLineItemNbr() < 0) {
			AccessorialCode code = fetchAccessorialByCode(cracc.getChargeCd(), rate.getParentPartnerCd());
			if (code != null) {
				cracc.setChargeDescTxt(code.getDescription());
			}
		}
		String sql = "insert into carrier_charge (" + "carrier_cd," + "charge_type_cd," + "equipment_type_cd,"
				+ "line_item_nbr," + "transport_leg_id," + "carrier_seq_nbr," + "transport_leg_nbr," + "charge_amt,"
				+ "usd_charge_amt," + "charge_ccy_cd," + "charge_cd," + "charge_desc_txt," + "create_tstamp,"
				+ "create_user_id," + "print_cd," + "exchange_rt," + "rate," + "reference_value," + "customer_cd,"
				+ "parent_partner_cd," + "partner_cd," + "shipper_cd," + "po_nbr," + "release_nbr,"
				+ "equipment_shipment_seq_nbr) values (" + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?)";
		log.info("SQL :" + sql);
		log.info("Rate to be inserted " + rate.toString());
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			pstmt.setString(1, rate.getCarrierCd()); // carrier code
			pstmt.setString(2, rate.getChargeTypeCd()); // charge type
			pstmt.setString(3, rate.getEquipmentTypeCd()); // equipment type
															// code
			pstmt.setInt(4, lineItemNbr); // line item number
			pstmt.setInt(5, rate.getTransportLegId()); // leg id
			pstmt.setInt(6, carrierSeqNbr); // carrier sequence numbe
			pstmt.setInt(7, rate.getTransportLegNbr()); // transport leg number
			if (cracc.getChargeCd().equalsIgnoreCase("FRT")) {
				pstmt.setDouble(8, rate.getTotalBase());
				cracc.setChargeAmt(rate.getTotalBase());
			} else if (cracc.getChargeCd().equalsIgnoreCase("FSC")) {
				pstmt.setDouble(8, rate.getTotalFsc());
				cracc.setChargeAmt(rate.getTotalFsc());
			} else if (cracc.getChargeCd().equalsIgnoreCase("DSCFRT")) {
				pstmt.setDouble(8, rate.getTotalDiscount());
				cracc.setChargeAmt(rate.getTotalDiscount());
			} else {
				pstmt.setDouble(8, cracc.getChargeAmt()); // charge amount
			}
			pstmt.setDouble(9, (rate.getExchangeRt() * cracc.getChargeAmt())); // us
																				// charge
			pstmt.setString(10, cracc.getChargeCcyCd()); // currency
			pstmt.setString(11, cracc.getChargeCd()); // accessorial charge code
			pstmt.setString(12, cracc.getChargeDescTxt()); // accessorial charge
															// description
			pstmt.setTimestamp(13, new Timestamp(new Date().getTime())); // create
																			// tstamp
			pstmt.setString(14, rate.getUserId()); // user id

			if (cracc.getChargeAmt() <= 0) {
				pstmt.setString(15, "DNP"); // print code

			} else {
				pstmt.setString(15, "");
			}
			pstmt.setDouble(16, rate.getExchangeRt()); // exchange rt

			pstmt.setDouble(17, rate.getRtWt()); // rate
			pstmt.setString(18, ""); // reference value
			pstmt.setString(19, rate.getCustomerCd());// customer cde
			pstmt.setString(20, rate.getParentPartnerCd());// customer cde
			pstmt.setString(21, rate.getPartnerCd());// customer cde
			pstmt.setString(22, rate.getShipperCd());// customer cde
			pstmt.setString(23, rate.getPoNbr());// customer cde
			pstmt.setString(24, rate.getReleaseNbr());// customer cde
			pstmt.setInt(25, rate.getEquipmentShipmentSeqNbr());// customer cde

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer insertDataStaging(String proBillNbr, String statusCd, String statusDesc, String commentTxt,
			String userId) {
		Timestamp tstamp = new Timestamp(new Date().getTime());
		String sql = "insert into data_staging (" + "activity_id," + "probill_nbr," + "application," + "status_cd,"
				+ "status_desc," + "event_dt," + "comment_txt," + "create_user_id," + "create_dt,"
				+ "update_dt) values (" + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?)";
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, -1);
			pstmt.setString(2, proBillNbr);
			pstmt.setString(3, "TMS");
			pstmt.setString(4, statusCd);
			pstmt.setString(5, statusDesc);
			pstmt.setTimestamp(6, tstamp);
			pstmt.setString(7, commentTxt);
			pstmt.setString(8, userId);
			pstmt.setTimestamp(9, tstamp);
			pstmt.setTimestamp(10, tstamp);

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer insertWorkqueue(String proBillNbr, String carrierCd, Integer legNbr, String partnerCd,
			String parentPartnerCd, String module) {
		String sql = "insert into messaging_workqueue (" + "reference_data," + "reference_nbr," + "notify_partner_cd,"
				+ "input_1_value," + "transaction_type_cd," + "request_status_cd," + "request_tstamp,"
				+ "processor_error_flg,"

				+ "create_user_id," + "create_tstamp," + "reference_type_cd," + "transaction_version_nbr,"
				+ "transaction_processor_name," + "data_format_type_cd," + "response_expected_flg,"
				+ "response_timeout_value," + "update_tstamp," + "internal_event_cd," + "external_event_cd,"
				+ "customer_cd," + "transmit_or_receive_cd," + "priority_cd," + "transmission_method_cd,"
				+ "response_timed_out_flg,processor_error_cd) values ("

				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,"
				+ "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?)";
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			pstmt.setString(1, "");// no pdf file
			pstmt.setString(2, proBillNbr);
			pstmt.setString(3, carrierCd);
			pstmt.setString(4, "" + legNbr);
			if (module.equalsIgnoreCase("CANCEL")) {

				pstmt.setString(5, "204CEML");
			} else {
				pstmt.setString(5, "204EML");
			}
			pstmt.setString(6, "P");

			pstmt.setTimestamp(7, new Timestamp(new Date().getTime()));
			pstmt.setString(8, "N");
			pstmt.setString(9, "PO-MANAGER");
			pstmt.setTimestamp(10, new Timestamp(new Date().getTime()));
			pstmt.setString(11, "PROBILL");
			pstmt.setString(12, "4010");
			pstmt.setString(13, "PO");
			pstmt.setString(14, "EML");
			pstmt.setString(15, "Y");
			pstmt.setInt(16, 1044);
			pstmt.setTimestamp(17, new Timestamp(new Date().getTime()));
			pstmt.setString(18, "LOAD_TENDER");
			pstmt.setString(19, "204");
			if (parentPartnerCd.equalsIgnoreCase("KLTL")) {
				pstmt.setString(20, "KLTL");
			} else {
				pstmt.setString(20, partnerCd);
			}

			pstmt.setString(21, "T");
			pstmt.setInt(22, 1);
			pstmt.setString(23, "EML");
			pstmt.setString(24, "N");
			pstmt.setInt(25, 0);

			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer do_MessageQueueByLaneId(int id, String status) {

		String spName = "marketplace_db.sp_contact_bid_msgqueue_by_lane";

		String sql = "call " + spName + " (?,?)";
		log.info("SQL SP " + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.setInt(1, id);
			pstmt.setString(2, status);
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	
	private Integer do_mp_contract_rate_to_rates(Set<Integer> ids) {

		String spName = "marketplace_db.sp_mp_contract_to_rater";

		String sql = "call " + spName + " ('"+ StringUtils.join(ids.toArray(), ",") + "')";
		log.info("SQL SP " + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}
	
	private Integer do_BidMessageQueue(int groupTenderId) {

		String spName = "marketplace_db.sp_contact_bid_msgqueue";

		String sql = "call " + spName + " (?)";
		log.info("SQL SP " + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.setInt(1, groupTenderId);
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private Integer do_TMSToDispatcher(String partnerCd, String parentPartnerCd, String shipperCd, String customerCd,
			String poNbr, String releaseNbr, String vpoPoNbr, String carrierCd) {

		String spName = "marketplace_db.sp_TMS_to_Dispatcher";

		String sql = "call " + spName + " (?,?,?,?,?,?,?,?)";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.setString(1, partnerCd);
			pstmt.setString(2, parentPartnerCd);
			pstmt.setString(3, shipperCd);
			pstmt.setString(4, customerCd);
			pstmt.setString(5, poNbr);
			pstmt.setString(6, releaseNbr);
			pstmt.setString(7, vpoPoNbr);
			pstmt.setString(8, carrierCd);
			pstmt.executeUpdate();

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}
	
	private Integer doTmsLcToDispatcher(String partnerCd, String parentPartnerCd, String shipperCd, String customerCd,
			String poNbr, String releaseNbr, String vpoPoNbr, String carrierCd, String poTypeCd, int legNumber) {

		String spName = "marketplace_db.sp_TMS_LC_to_DispatcherQ_V3";

		String sql = "call " + spName + " (?,?,?,?,?,?,?,?,?,?)";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.setString(1, partnerCd);
			pstmt.setString(2, parentPartnerCd);
			pstmt.setString(3, shipperCd);
			pstmt.setString(4, customerCd);
			pstmt.setString(5, poNbr);
			pstmt.setString(6, releaseNbr);
			pstmt.setString(7, vpoPoNbr);
			pstmt.setString(8, carrierCd);
			pstmt.setString(9, poTypeCd);
			pstmt.setInt(10, legNumber);
			pstmt.executeUpdate();

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private List<Carrier> getCarriers(String country) {

		String sql = "select a.company_id,a.company_code,a.company_name from company a inner join company_address b on a.corporate_address_id=b.Company_Address_ID inner join "
				+ " country c on c.Country_ID = b.Country_ID " + " where a.is_carrier=1 and c.country_2_cd='" + country
				+ "'";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			List<Carrier> carriers = new LinkedList<>();
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					Carrier carrier = new Carrier();
					carrier.setCompanyId(rs.getInt(1));
					carrier.setCarrierCd(rs.getString(2));
					carrier.setCarrierName(rs.getString(3));
					carriers.add(carrier);
				}
				return carriers;
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;

	}

	private Integer do_DeleteDispatcher(String probillNbr) {

		String spName = "staging_db.sp_shipment_cancellation";

		String sql = "call " + spName + " (?)";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				CallableStatement pstmt = connection.prepareCall(sql);) {
			pstmt.setString(1, probillNbr);
			pstmt.executeUpdate();

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;

	}

	private List<RatesWorkArea> fetchWorkAreaByCompanyId(int companyId, String countryCd) {
		log.info("Country Cd" + countryCd);
		String sql = "select company_contract_tender_lane_id,group_tender_id,commodity_cd,a.lane_id,b.origin_city,b.origin_province,b.origin_postal_cd,"
				+ " b.origin_country,b.dest_city,b.dest_province,b.dest_postal_cd,b.dest_country,c.Equipment_Type_Name,a.Equipment_Type_ID,a.RATE_EFFECTIVE_DT,a.rate_termination_dt,a.rate_per_unit_cd"
				+ " from company_contract_tender_lane a inner join lane b on a.LANE_ID=b.lane_id inner join equipment_type c ON c.equipment_Type_ID = a.EQUIPMENT_TYPE_ID "
				+ " where status_cd='P' and shipper_company_id=" + companyId
				+ " order by a.rate_effective_dt,group_tender_id ";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			Map<Integer, CompanyContractTenderLane> mapCctl = new HashMap<>();
			Map<Integer, Lane> mapLane = new HashMap<>();
			Map<Integer, List<Equipment>> mapEqpt = new HashMap<>();
			List<RatesWorkArea> wList = new LinkedList<>();
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					CompanyContractTenderLane cctl = new CompanyContractTenderLane();
					Lane ln = new Lane();
					Equipment et = new Equipment();
					cctl.setCompanyContractTenderLaneId(rs.getInt(1));
					cctl.setGroupTenderId(rs.getInt(2));
					cctl.setCommodityCd(rs.getString(3));
					cctl.setLaneId(rs.getInt(4));
					ln.setLaneId(cctl.getLaneId());
					ln.setOriginCity(rs.getString(5));
					ln.setOriginProvince(rs.getString(6));
					ln.setOriginPostalCd(rs.getString(7));
					ln.setOriginCountry(rs.getString(8));
					ln.setDestCity(rs.getString(9));
					ln.setDestProvince(rs.getString(10));
					ln.setDestPostalCd(rs.getString(11));
					ln.setDestCountry(rs.getString(12));
					ln.setIsSelected(false);
					et.setEquipmentName(rs.getString(13));
					et.setEquipmentTypeId(rs.getInt(14));
					et.setCompanyContractTenderLaneId(cctl.getCompanyContractTenderLaneId());
					et.setIsSelected(true);
					et.setGroupTenderId(cctl.getGroupTenderId());

					ln.setGroupTenderId(cctl.getGroupTenderId());
					cctl.setRateEffectiveDt(rs.getDate(15));
					cctl.setRateTerminationDt(rs.getDate(16));

					cctl.setRatePerUnitCd(rs.getString(17));
					mapCctl.put(cctl.getGroupTenderId(), cctl);
					mapLane.put(cctl.getGroupTenderId(), ln);
					List<Equipment> eqptList = new LinkedList<>();
					if (mapEqpt.get(cctl.getGroupTenderId()) != null) {
						eqptList = mapEqpt.get(cctl.getGroupTenderId());
					}
					eqptList.add(et);
					mapEqpt.put(cctl.getGroupTenderId(), eqptList);

				}
				Set<Entry<Integer, CompanyContractTenderLane>> cctlSet = mapCctl.entrySet();
				for (Entry<Integer, CompanyContractTenderLane> c : cctlSet) {
					CompanyContractTenderLane cc = c.getValue();
					RatesWorkArea rwa = new RatesWorkArea();
					List<Lane> lns = new LinkedList<>();
					rwa.setSortByDate(DateUtil.formatRealTimeYYYMMddhhmm(cc.getRateTerminationDt(), "00:00"));
					rwa.setEndDt(DateSimpleFormat.formatSimpleDate(cc.getRateTerminationDt(), "MM/dd/yyyy"));
					rwa.setStartDt(DateSimpleFormat.formatSimpleDate(cc.getRateEffectiveDt(), "MM/dd/yyyy"));
					List<Equipment> eList = mapEqpt.get(c.getValue().getGroupTenderId());
					Lane ln = mapLane.get(c.getValue().getGroupTenderId());
					ln.setCompanyContractTenderLane(cc);

					// include not been checked
					// current compa equipment
					Map<Integer, Boolean> mapCurrentEquipment = new HashMap();
					for (Equipment e : eList) {
						mapCurrentEquipment.put(e.getEquipmentTypeId(), true);
					}
					List<Equipment> newEqpt = new LinkedList<>();
					newEqpt.addAll(eList);
					log.info("Get current equipment" + countryCd);
					List<Equipment> compEqpt = fetchCompanyEquipment(countryCd);
					if (compEqpt != null && compEqpt.size() > 0) {
						for (Equipment ee : compEqpt) {
							// ee.setEquipmentName(ee.getEquipmentTypeName());
							if (mapCurrentEquipment.get(ee.getEquipmentTypeId()) == null) {
								newEqpt.add(ee);
							}
						}
					}
					ln.setEquipmentList(newEqpt);
					lns.add(ln);
					rwa.setLanes(lns);

					wList.add(rwa);
				}
				// sort by dates
				List<RatesWorkArea> ucList = new LinkedList<>();
				List<SortObject> sortList = new LinkedList<>();
				for (RatesWorkArea u : wList) {
					SortObject sObj = new SortObject();
					sObj.setdDt(u.getSortByDate());
					sObj.setObj(u);
					sObj.setType("date");
					sortList.add(sObj);
				}
				Collections.sort(sortList);// apply

				if (sortList != null && sortList.size() > 0) {
					for (SortObject i : sortList) {
						ucList.add((RatesWorkArea) i.getObj());
					}
				}
				// combine all rate effective + termination
				wList = new LinkedList<>();
				Map<String, List<Lane>> mapDates = new HashMap<>();
				Map<String, RatesWorkArea> mapArea = new HashMap<>();
				for (RatesWorkArea rwa : ucList) {
					String key = "" + rwa.getStartDt() + rwa.getEndDt();
					mapArea.put(key, rwa);
					List<Lane> areaList = new LinkedList<>();
					if (mapDates.get(key) != null) {
						areaList = mapDates.get(key);
					}
					areaList.addAll(rwa.getLanes());
					mapDates.put(key, areaList);

				}
				// display All
				wList = new LinkedList<>();
				Set<Entry<String, RatesWorkArea>> areaSet = mapArea.entrySet();
				for (Entry<String, RatesWorkArea> a : areaSet) {

					RatesWorkArea rwa = a.getValue();
					log.info("sorty " + rwa.getSortByDate());
					rwa.setLanes(mapDates.get(a.getKey()));
					wList.add(rwa);
				}

				// final sorting
				// sort by dates
				ucList = new LinkedList<>();
				sortList = new LinkedList<>();
				for (RatesWorkArea u : wList) {
					SortObject sObj = new SortObject();
					sObj.setdDt(u.getSortByDate());
					sObj.setObj(u);
					sObj.setType("date");
					sortList.add(sObj);
				}
				Collections.sort(sortList);// apply

				if (sortList != null && sortList.size() > 0) {
					for (SortObject i : sortList) {
						ucList.add((RatesWorkArea) i.getObj());
					}
				}

				return ucList;
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	private Integer postBidRank(CompanyContractTenderLane rank) {

		String status = "E";
		if (rank.getBidRank() == 1) {
			status = "A";
		}
		String sql = "update company_contract_tender_lane set status_cd='" + status + "',bid_rank=" + rank.getBidRank()
				+ ",update_user_id=?,update_dt=now() where company_contract_tender_lane_id="
				+ rank.getCompanyContractTenderLaneId();
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, rank.getUpdateUserId());
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer saveBidRank(CompanyContractTenderLane rank) {

		String sql = "update company_contract_tender_lane set bid_rank=" + rank.getBidRank()
				+ ",update_user_id=?,update_dt=now() where company_contract_tender_lane_id="
				+ rank.getCompanyContractTenderLaneId();
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, rank.getUpdateUserId());
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private Integer withdrawTenderByGroupId(List<String> ids) {
		// String ins = "";
		// for(String id : ids) {
		// ins+=id+",";
		// }
		// ins=ins.substring(0,ins.length()-1);
		String sql = "update company_contract_tender_lane set status_cd='W' where group_tender_id in ("
				+ StringUtils.join(ids.toArray(), ",") + ")";
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return 0;
	}

	private List<SubmittedArea> fetchSubmittedByCompanyId(int companyId, String dStart, String dEnd) {
		String sql = "";
		if (dStart == null) {
			sql = "select company_contract_tender_lane_id,group_tender_id,commodity_cd,a.lane_id,b.origin_city,b.origin_province,b.origin_postal_cd,"
					+ " b.origin_country,b.dest_city,b.dest_province,b.dest_postal_cd,b.dest_country,c.Equipment_Type_Name,a.Equipment_Type_ID,a.RATE_EFFECTIVE_DT,a.rate_termination_dt,a.rate_per_unit_cd,a.rate_amt,a.status_cd,a.carrier_company_id,d.company_name"
					+ " from company_contract_tender_lane a inner join lane b on a.LANE_ID=b.lane_id inner join equipment_type c ON c.equipment_Type_ID = a.EQUIPMENT_TYPE_ID inner join company d on d.company_id = a.carrier_company_id "
					+ " where status_cd in ('U','A','E') and shipper_company_id=" + companyId
					+ " order by a.rate_effective_dt,group_tender_id ";
		} else {
			sql = "select company_contract_tender_lane_id,group_tender_id,commodity_cd,a.lane_id,b.origin_city,b.origin_province,b.origin_postal_cd,"
					+ " b.origin_country,b.dest_city,b.dest_province,b.dest_postal_cd,b.dest_country,c.Equipment_Type_Name,a.Equipment_Type_ID,a.RATE_EFFECTIVE_DT,a.rate_termination_dt,a.rate_per_unit_cd,a.rate_amt,a.status_cd,a.carrier_company_id,d.company_name"
					+ " from company_contract_tender_lane a inner join lane b on a.LANE_ID=b.lane_id inner join equipment_type c ON c.equipment_Type_ID = a.EQUIPMENT_TYPE_ID inner join company d on d.company_id = a.carrier_company_id "
					+ " where status_cd in ('U','A','E') and shipper_company_id=" + companyId
					+ " and rate_effective_dt between '" + dStart + " 00:00' and '" + dEnd + " 23:59'"
					+ " order by a.rate_effective_dt,group_tender_id ";
		}
		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			Map<Integer, CompanyContractTenderLane> mapCctl = new HashMap<>();
			Map<Integer, Lane> mapLane = new HashMap<>();
			Map<Integer, List<Equipment>> mapEqpt = new HashMap<>();
			List<RatesWorkArea> wList = new LinkedList<>();
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					CompanyContractTenderLane cctl = new CompanyContractTenderLane();
					Lane ln = new Lane();
					Equipment et = new Equipment();
					cctl.setCompanyContractTenderLaneId(rs.getInt(1));
					cctl.setGroupTenderId(rs.getInt(2));
					cctl.setCommodityCd(rs.getString(3));
					cctl.setLaneId(rs.getInt(4));
					ln.setLaneId(cctl.getLaneId());
					ln.setOriginCity(rs.getString(5));
					ln.setOriginProvince(rs.getString(6));
					ln.setOriginPostalCd(rs.getString(7));
					ln.setOriginCountry(rs.getString(8));
					ln.setDestCity(rs.getString(9));
					ln.setDestProvince(rs.getString(10));
					ln.setDestPostalCd(rs.getString(11));
					ln.setDestCountry(rs.getString(12));
					ln.setIsSelected(false);
					et.setEquipmentName(rs.getString(13));
					et.setEquipmentTypeId(rs.getInt(14));

					et.setCompanyContractTenderLaneId(cctl.getCompanyContractTenderLaneId());
					et.setIsSelected(true);
					et.setGroupTenderId(cctl.getGroupTenderId());
					et.setCarrierId(cctl.getCarrierCompanyId());
					et.setStatus(cctl.getStatusCd());

					ln.setCommodityCd(cctl.getCommodityCd());
					ln.setGroupTenderId(cctl.getGroupTenderId());
					cctl.setRateEffectiveDt(rs.getDate(15));
					cctl.setRateTerminationDt(rs.getDate(16));

					cctl.setRatePerUnitCd(rs.getString(17));
					cctl.setRateAmt(rs.getDouble(18));
					cctl.setStatusCd(rs.getString(19));
					cctl.setCarrierCompanyId(rs.getInt(20));
					cctl.setCompanyName(rs.getString(21));

					et.setRateAmt(cctl.getRateAmt());
					et.setStatus(cctl.getStatusCd());
					et.setCarrierId(cctl.getCarrierCompanyId());
					et.setCarrierName(cctl.getCompanyName());
					mapCctl.put(cctl.getGroupTenderId(), cctl);
					mapLane.put(cctl.getGroupTenderId(), ln);
					List<Equipment> eqptList = new LinkedList<>();
					if (mapEqpt.get(cctl.getGroupTenderId()) != null) {
						eqptList = mapEqpt.get(cctl.getGroupTenderId());
					}
					eqptList.add(et);
					mapEqpt.put(cctl.getGroupTenderId(), eqptList);

				}
				Set<Entry<Integer, CompanyContractTenderLane>> cctlSet = mapCctl.entrySet();
				for (Entry<Integer, CompanyContractTenderLane> c : cctlSet) {
					CompanyContractTenderLane cc = c.getValue();
					RatesWorkArea rwa = new RatesWorkArea();
					List<Lane> lns = new LinkedList<>();
					rwa.setSortByDate(DateUtil.formatRealTimeYYYMMddhhmm(cc.getRateTerminationDt(), "00:00"));
					rwa.setEndDt(DateSimpleFormat.formatSimpleDate(cc.getRateTerminationDt(), "MM/dd/yyyy"));
					rwa.setStartDt(DateSimpleFormat.formatSimpleDate(cc.getRateEffectiveDt(), "MM/dd/yyyy"));
					List<Equipment> eList = mapEqpt.get(c.getValue().getGroupTenderId());
					Lane ln = mapLane.get(c.getValue().getGroupTenderId());
					ln.setCompanyContractTenderLane(cc);

					// include not been checked
					// current compa equipment
					ln.setEquipmentList(eList);
					lns.add(ln);
					rwa.setLanes(lns);

					wList.add(rwa);

				}

				// sort by dates
				List<RatesWorkArea> ucList = new LinkedList<>();
				List<SortObject> sortList = new LinkedList<>();
				for (RatesWorkArea u : wList) {
					SortObject sObj = new SortObject();
					sObj.setdDt(u.getSortByDate());
					sObj.setObj(u);
					sObj.setType("date");
					sortList.add(sObj);
				}
				Collections.sort(sortList);// apply

				if (sortList != null && sortList.size() > 0) {
					for (SortObject i : sortList) {
						ucList.add((RatesWorkArea) i.getObj());
					}
				}
				// combine all rate effective + termination
				wList = new LinkedList<>();
				Map<String, List<Lane>> mapDates = new HashMap<>();
				Map<String, RatesWorkArea> mapArea = new HashMap<>();
				for (RatesWorkArea rwa : ucList) {
					String key = "" + rwa.getStartDt() + rwa.getEndDt();
					mapArea.put(key, rwa);
					List<Lane> areaList = new LinkedList<>();
					if (mapDates.get(key) != null) {
						areaList = mapDates.get(key);
					}
					areaList.addAll(rwa.getLanes());
					mapDates.put(key, areaList);

				}
				// display All
				wList = new LinkedList<>();
				Set<Entry<String, RatesWorkArea>> areaSet = mapArea.entrySet();
				for (Entry<String, RatesWorkArea> a : areaSet) {

					RatesWorkArea rwa = a.getValue();
					rwa.setLanes(mapDates.get(a.getKey()));
					wList.add(rwa);
				}

				// final sorting
				// sort by dates
				ucList = new LinkedList<>();
				sortList = new LinkedList<>();
				for (RatesWorkArea u : wList) {
					SortObject sObj = new SortObject();
					sObj.setdDt(u.getSortByDate());
					sObj.setObj(u);
					sObj.setType("date");
					sortList.add(sObj);
				}
				Collections.sort(sortList);// apply

				if (sortList != null && sortList.size() > 0) {
					for (SortObject i : sortList) {
						log.info(((RatesWorkArea) i.getObj()).toString());
						ucList.add((RatesWorkArea) i.getObj());
					}
				}

				// modify it to fit screen
				List<SubmittedArea> sList = new LinkedList<>();

				for (RatesWorkArea _rwa : ucList) {
					SubmittedArea s = new SubmittedArea(); // use it as a
															// template to
															// display submitted
					s.setStartDt(_rwa.getStartDt());
					s.setEndDt(_rwa.getEndDt());
					List<Lane> _lanes = _rwa.getLanes();
					log.info("how many lanes " + _lanes.size());
					List<Bid> bidList = new LinkedList<>();

					for (Lane _lane : _lanes) { // this are the lanes
						// _lane.get
						log.info("Current _lane" + _lane.toString());
						Map<String, List<Equipment>> mapEquipmentType = new HashMap<>();
						List<Equipment> _eList = _lane.getEquipmentList();
						for (Equipment _e : _eList) {
							List<Equipment> eTemp = new LinkedList<>();
							if (mapEquipmentType.get(_e.getEquipmentName()) != null) {
								eTemp = mapEquipmentType.get(_e.getEquipmentName());
							}
							eTemp.add(_e);
							mapEquipmentType.put(_e.getEquipmentName(), eTemp);
						}
						// format
						Set<Entry<String, List<Equipment>>> eSet = mapEquipmentType.entrySet();
						int cnt = 1;

						for (Entry<String, List<Equipment>> a : eSet) {
							double minRate = 0.0;
							double maxRate = 0.0;
							Bid bid = new Bid();
							log.info("key equipment " + a.getKey());
							List<Equipment> eBidEquipment = a.getValue();
							log.info("equipment " + eBidEquipment.toString());
							bid.setRows(mapEquipmentType.size());
							if (cnt == 1) {
								bid.setDisplay(true);
								cnt = 2;
							} else {
								bid.setDisplay(false);
							}
							bid.setGroupTenderId(_lane.getGroupTenderId());
							bid.setOrigin(trimStringV2(_lane.getOriginCity()) + trimStringV2(_lane.getOriginProvince())
									+ trimStringV2(_lane.getOriginCountry()) + trimStringV2(_lane.getOriginPostalCd()));
							bid.setOrigin(bid.getOrigin().substring(0, bid.getOrigin().length() - 1));
							bid.setDestination(trimStringV2(_lane.getDestCity()) + trimStringV2(_lane.getDestProvince())
									+ trimStringV2(_lane.getDestCountry()) + trimStringV2(_lane.getDestPostalCd()));
							bid.setDestination(bid.getDestination().substring(0, bid.getDestination().length() - 1));
							bid.setCommodity(_lane.getCommodityCd());

							bid.setEquipmentType(a.getKey());
							bid.setRatingUnit("FC");
							int cntx = 0;
							String awarded = "";
							List<Double> sortAmt = new LinkedList<>();
							sortAmt.add(0.0);// in
							for (Equipment ee : eBidEquipment) {
								bid.setEquipmentTypeId(ee.getEquipmentTypeId());
								if (ee.getRateAmt() != null && ee.getRateAmt() > 0) {
									sortAmt.add(ee.getRateAmt());
									++cntx;
								}
								if (ee.getStatus().equalsIgnoreCase("A")) {
									awarded = "" + ee.getCarrierName();
								}
							}

							sortList = new LinkedList<>();
							for (Double u : sortAmt) {
								SortObject sObj = new SortObject();
								sObj.setdDouble(u);
								sObj.setObj(u);
								sObj.setType("double");
								sortList.add(sObj);
							}
							Collections.sort(sortList);// apply
							if (sortList != null && sortList.size() > 0) {
								if (sortList.size() == 1) {
									minRate = (Double) sortList.get(0).getObj();
									maxRate = (Double) sortList.get(0).getObj();
								} else if (sortList.size() == 2) {
									minRate = (Double) sortList.get(0).getObj();
									maxRate = (Double) sortList.get(1).getObj();
								} else {
									minRate = (Double) sortList.get(1).getObj();
									maxRate = (Double) sortList.get(sortList.size() - 1).getObj();
								}
							}

							// resort it by double
							bid.setRate("" + new BigDecimal(minRate).setScale(2, BigDecimal.ROUND_UP) + " - "
									+ new BigDecimal(maxRate).setScale(2, BigDecimal.ROUND_UP));
							bid.setCnt(cntx);
							if (!awarded.isEmpty()) {
								bid.setRateAwarded("" + awarded);
							} else {
								bid.setRateAwarded(null);
							}
							log.info("equipment " + a.getKey() + " " + bid.toString());
							bidList.add(bid);

							// rwa.setLanes(mapDates.get(a.getKey()));
							// wList.add(rwa);
						}
						log.info("number of bid List" + " " + bidList.size());

					}
					s.setBids(bidList);
					sList.add(s);
				}

				return sList;
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	private List<CompanyContractTenderLane> fetchContractBid(int groupId, int equipmentId) {

		String sql = "select company_contract_tender_lane_id,bid_rank,rate_amt,a.carrier_company_id,b.company_name,a.equipment_type_id,a.status_cd"
				+ " from company_contract_tender_lane a inner join company b on b.company_id = a.carrier_company_id "
				+ " where  group_tender_id=" + groupId + " and equipment_type_id=" + equipmentId
				+ " order by rate_amt,bid_rank";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			List<CompanyContractTenderLane> wList = new LinkedList<>();
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					CompanyContractTenderLane cctl = new CompanyContractTenderLane();
					cctl.setCompanyContractTenderLaneId(rs.getInt(1));
					cctl.setBidRank(rs.getInt(2));
					cctl.setRateAmt(rs.getDouble(3));
					cctl.setCarrierCompanyId(rs.getInt(4));
					cctl.setCompanyName(rs.getString(5));
					cctl.setEquipmentTypeId(rs.getInt(6));
					cctl.setStatusCd(rs.getString(7));
					wList.add(cctl);
				}

			}
			return wList;

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}
	
	private Company fetchCompany(int companyId) {

		String sql = "select currency_cd from company where company_id="+companyId;
				

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			List<CompanyContractTenderLane> wList = new LinkedList<>();
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
	        	List<Company> pojoList = driverRawMapper.mapResultSetToObject(rs, Company.class);
	        	
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		
	        		return pojoList.get(0);
	        	}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	private List<CompanyContractTenderLane> fetchContractBidByGroupId(List<String> grpList) {
		List<CompanyContractTenderLane> lst = new LinkedList<>();
		String ins = "";
		for (String id : grpList) {
			ins += id + ",";
		}

		ins = ins.substring(0, ins.length() - 1);

		String sql = "select distinct company_contract_tender_lane_id " + " from company_contract_tender_lane "
				+ " where  group_tender_id in (" + ins + ")";

		log.info("SQL :" + sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					CompanyContractTenderLane l = new CompanyContractTenderLane();
					l.setCompanyContractTenderLaneId(rs.getInt(1));
					lst.add(l);

				}

			}
			log.info(lst);
			return lst;

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

}
