package com.openport.marketplace.view;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.DataSources;
import com.openport.marketplace.json.Gmt;
import com.openport.marketplace.json.ReportSchedule;
import com.openport.marketplace.json.ReportWizardData;
import com.openport.marketplace.json.ReportWizardSql;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.TmsParameter;
import com.openport.marketplace.parser.ReportGenerator;
import com.openport.marketplace.parser.ReportWizardParser;
import com.openport.marketplace.repository.DbConfigTMS;
import com.openport.util.db.ResultSetMapper;

@Path("reportwizard") // http://localhost:8080/tms-api/users
public class ReportResource {

	static final transient Logger log = Logger.getLogger(ReportResource.class);

	
	@POST // http://localhost:8080/market-api/shipmentTenders/newshipment
	@Path("runreport")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ReportWizardData runReport(ReportWizardData data) {
		log.info("run report...");
		data.setFilename(createFilename(data.getReportFormat(), data.getDataSource(), data.getUserName()));
		
	//	ReportGenerator generator = new ReportGenerator();
		ReportGenerator.createSQLScript(data);
	//	data.setSqlScript(generator.reportSQL);
		if(!new File(data.getReportLocalPath()+data.getFilename()).isFile()) {
			data.setIsFileCreated(false);
		}else {
			data.setIsFileCreated(true);
		}
		return data;
	}
	@POST // http://localhost:8080/market-api/shipmentTenders/newshipment
	@Path("deletereport")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ReportWizardData deleteReport(ReportWizardData data) {
		String sql = "delete from report_wizard_sql where report_name=? and report_db_source_name=? and user_id=?";
		log.info(sql);
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setString(1, data.getReportName());
			pstmt.setString(2, data.getDataSource());
			pstmt.setString(3, data.getUserName());
			int id = pstmt.executeUpdate();	
			deleteReportScheduler(data.getReportName(),data.getUserName());
			return data;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	@POST // http://localhost:8080/market-api/shipmentTenders/newshipment
	@Path("deleteschedule")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ReportSchedule deleteSchedule(ReportSchedule schedule) {
		
		try {
		if(deleteReportScheduler(schedule.getReportName(), schedule.getReportOwnerUserId()) != null) {
			return schedule;
		}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	@GET
	@Path("datasources/{parent}/{partner}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<DataSources> getDataSources(@PathParam("parent") String parent, @PathParam("partner") String partner) {
		try {
			String sql = " select report_data_group_name,report_db_source_name from report_wizard_data_source\n"
					+ " where partner_cd=? and parent_partner_cd=? order by report_data_group_name";
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, partner);
				pstmt.setString(2, parent);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<DataSources> driverRawMapper = new ResultSetMapper<DataSources>();
					List<DataSources> pojoList = driverRawMapper.mapResultSetToObject(rs, DataSources.class);
					// List<Driver> nw_pojoList = new LinkedList<Driver>();
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList;
					}
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;

	}

	@GET
	@Path("availablesources/{sourcename}/{userid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ReportWizardSql> getAvailableSources(@PathParam("sourcename") String sourcename,
			@PathParam("userid") String userId) {
		try {
			String sql = " select report_name,user_id,report_sql_txt,report_output_format,time_zone_country,time_zone_offset from report_wizard_sql \n"
					+ " where report_db_source_name=? and user_id=? order by report_name";
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, sourcename);
				pstmt.setString(2, userId);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<ReportWizardSql> driverRawMapper = new ResultSetMapper<ReportWizardSql>();
					List<ReportWizardSql> pojoList = driverRawMapper.mapResultSetToObject(rs, ReportWizardSql.class);
					List<ReportWizardSql> nw_pojoList = new LinkedList<ReportWizardSql>();
					if (pojoList != null && !pojoList.isEmpty()) {

						for (ReportWizardSql rw : pojoList) {

							rw.setUserColumns(ReportWizardParser.getDistinctColumn(rw.getReportSqlTxt().toUpperCase()));
							rw.setUserSort(ReportWizardParser.getSorting(rw.getReportSqlTxt().toUpperCase()));
							rw.setUserCriteria(ReportWizardParser.getCriteria(rw.getReportSqlTxt().toUpperCase()));
							rw.setReportSchedule(ReportGenerator.getReportSchedule(rw.getReportName(),userId));
							nw_pojoList.add(rw);
						}
						return nw_pojoList;
					}
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;

	}
	@GET
	@Path("gmt")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Gmt> getGmt() {
		try {
			String sql = " select country,pacific from gmt order by country";
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Gmt> driverRawMapper = new ResultSetMapper<Gmt>();
					List<Gmt> pojoList = driverRawMapper.mapResultSetToObject(rs, Gmt.class);
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList;
					}
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;

	}
	@GET
	@Path("availablecolumns/{sourcename}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<TmsParameter> getAvailableColumns(@PathParam("sourcename") String sourcename,
			@PathParam("userid") String partner) {
		try {
			String sql = "select parameter_seq_nbr,parameter_name,parameter_1_value,parameter_2_value from tms_parameter  "
					+ " WHERE parameter_Cd = 'REPORT_COLS' " + " AND program_Id = 'REPORT_WIZARD' "
					+ " AND parameter_3_Value = '" + sourcename + "' " + " ORDER BY parameter_name";
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<TmsParameter> driverRawMapper = new ResultSetMapper<TmsParameter>();
					List<TmsParameter> pojoList = driverRawMapper.mapResultSetToObject(rs, TmsParameter.class);
					List<TmsParameter> nw_pojoList = new LinkedList<TmsParameter>();
					if (pojoList != null && !pojoList.isEmpty()) {
						for (TmsParameter tm : pojoList) {
							tm.setParameterId("COL_PARAM_" + tm.getParameter1Value());

							nw_pojoList.add(tm);
						}
						return nw_pojoList;
					}

				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;

	}
	
	private Integer deleteReportScheduler(String reportName,String userId) {
	String sql = "delete from report_schedule where report_name=? and  report_owner_user_id=?";
	log.info(sql);
	try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

	{
		pstmt.setString(1, reportName);
		pstmt.setString(2, userId);
		int id = pstmt.executeUpdate();	
		return id;

	} catch (Exception e) {
		log.error("Error message: " + e, e);
		new ServiceException(Response.Status.CONFLICT);
	}
	return null;
	}
	private String createFilename(String formatType,String datasource,String username) {
		if(formatType.equalsIgnoreCase("CSV")) {
			return username+"_"+datasource+"_"+new Date().getTime() + ".csv";
		}else if(formatType.equalsIgnoreCase("XLS")) {
			return username+"_"+datasource+"_"+new Date().getTime() + ".xls";
		}
		return null;
	}
	/*
	public ReportSchedule getReportSchedule(String reportName,String userId) {
			
		try {
			String sql = " select * from report_schedule \n"
					+ " where report_name=? and report_owner_user_id=?";
			try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, reportName);
				pstmt.setString(2, userId);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<ReportSchedule> driverRawMapper = new ResultSetMapper<ReportSchedule>();
					List<ReportSchedule> pojoList = driverRawMapper.mapResultSetToObject(rs, ReportSchedule.class);
					if (pojoList != null && !pojoList.isEmpty()) {
						for (ReportSchedule rsched : pojoList) {
							rsched.setStartTm(new CurrentDateFormatter().formatGivenDate(rsched.getStartSchedTm(), "h:m a", new Locale("EN_US")));
							return rsched;
						}
					}
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;

	}
	*/

}
