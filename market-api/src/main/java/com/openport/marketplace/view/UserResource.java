package com.openport.marketplace.view;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.openport.marketplace.auth.annotation.NoAuthorization;
import com.openport.marketplace.repository.TmsParameterRepository;
import org.apache.log4j.Logger;

import com.openport.marketplace.helper.JwtTokenHelper;
import com.openport.marketplace.helper.TemporaryPassword;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.Label;
import com.openport.marketplace.json.Language;
import com.openport.marketplace.json.LanguagePack;
import com.openport.marketplace.json.Password;
import com.openport.marketplace.json.Role;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.Token;
import com.openport.marketplace.json.User;
import com.openport.marketplace.json.UserLoginRequest;
import com.openport.marketplace.json.UserRole;
import com.openport.marketplace.json.UserStatus;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.marketplace.repository.Notify;
import com.openport.marketplace.repository.UserRepository;
import com.openport.util.db.ResultSetMapper;
import com.openport.util.password.MySqlPassword;

@Path("users") // http://localhost:8080/market-api/users
public class UserResource {

	static final transient Logger log = Logger.getLogger(UserResource.class);
	
	
	private static final String CURRENT_VERSION = "1.0";
	private static final String CURRENT_UAT_VERSION = "1.0";

	private static final String OPEN_TM_CONFIG_CD = "OPEN_TM_CONFIG";
	private static final String OPEN_TM_AUTH_TIMEOUT_NAME = "OPEN_TM_AUTH_TIMEOUT";
	private static final long OPEN_TM_DEFAULT_TIMEOUT = 24 * 60; //timu unit used is minute this will be 1 hour
	
	
	@GET
	@Path("labels/{langCd}/{langVer}/{appVersion}/{appId}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	@NoAuthorization
	public UserStatus getLabelsApp(@PathParam("langCd") String langCd
			,@PathParam("langVer") int langVer
			, @PathParam("appVersion") String appVersion, @PathParam("appId") String appId
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("labels/{langCd}/{langVer}/{appVersion}/{appId}");
		try {
	    	sts.setRefreshSeconds(null);
	    	sts.setStatus(200);
			sts = doFindLabels(sts, langCd, langVer); // pass zero on langVer to list all labels
			checkVersion(langCd, appVersion, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	
	
	
	@GET
	@Path("lang/{langVer}/{langCd}/{appVersion}/{appId}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	@NoAuthorization
	public UserStatus lang(@PathParam("langCd") String langCd
			,@PathParam("langVer") int langVer
			,@PathParam("appVersion") String appVersion
			,@PathParam("appId") String appId
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
	    	sts.setStatus(200);
			sts = checkLanguage(sts, langVer, ""); // all updates no filter on language
			checkVersion(langCd, appVersion, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	

	
	@GET
	@Path("languages")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	@NoAuthorization
	public UserStatus lang() {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
	    	sts.setStatus(200);
	    	List<Language> languageList = new ArrayList();
	    	languageList = doGetLanguageList();
	    	sts.setLanguages(languageList);
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	
	
	@GET
	@Path("lang/{langCd}/{langVer}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus getLangLabel(@PathParam("langCd") String langCd
			,@PathParam("langVer") int langVer
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.debug("lang/{langCd}/{langVer}");
		try {
	    	sts.setRefreshSeconds(null);
	    	sts.setStatus(200);
			sts = checkLanguage(sts, langVer , langCd);
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	

	
	
	@GET
	@Path("labels/{langCd}/{langVer}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus getLabels(@PathParam("langCd") String langCd
			,@PathParam("langVer") int langVer
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
	    	sts.setStatus(200);
			sts = checkLanguage(sts, langVer , langCd);
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	
		
	
	@GET
	@Path("labels/{langVer}") // http://localhost:8080/market-api/shippers/labels/zh/123/4.5.0/com.openport.delivery.uat
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus lang(@PathParam("langVer") int langVer) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
	    	sts.setStatus(200);
			sts = checkLanguage(sts, langVer, "");
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }

	@POST
	@Path("authenticate") // http://localhost:8080/market-api/users/authenticate
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
    @NoAuthorization
	public UserStatus authenticate(UserLoginRequest userLogin) {
		log.info("User:"+userLogin.getUsername()+ " - start ");
		UserStatus sts = new UserStatus();
		try {
			if (userLogin.getUsername() == null || userLogin.getUsername().trim().length() == 0 || userLogin.getPassword() == null
					|| userLogin.getPassword().trim().length() == 0) {
				new ServiceException(Response.Status.FORBIDDEN);
			} else {
				
				User usr1 = UserRepository.findUser(userLogin);
				if (usr1!=null) {
	        		usr1.setPassword(null);
					usr1.setCompany(getCompanyByUserId(usr1.getUserId()));
					sts.setUser(usr1);
//					ShipperResource shpr = new ShipperResource();
//					Company cmpny = shpr.getCompanyById(companyID) {

					Token token = new Token();
					token.setAccessToken(JwtTokenHelper.createToken(usr1, TmsParameterRepository.getLong( OPEN_TM_CONFIG_CD, OPEN_TM_AUTH_TIMEOUT_NAME , OPEN_TM_DEFAULT_TIMEOUT)));
					
					sts.setToken(token);
					sts.setSuccess("true");
					return sts;
				} else{
					new ServiceException(Status.FORBIDDEN);
				}
			}
		} finally {
			log.info("User:"+userLogin.getUsername()+" - Done");
		}
//		new ServiceException(Response.Status.FORBIDDEN);
		return sts;
	}

	public Company getCompanyByUserId(int userID) {
		return UserRepository.getCompanyByUserId(userID);
	}

	
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{userId}") // // http://localhost:8080/market-api/users/1234
	public User getUserByUserId(@PathParam("userId") int userId) {
		if (userId == 0) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} else {
			return getUserByUserId(userId);
		}
		return new User();
	}


	

	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{emailAddress}/email") // http://localhost:8080/market-api/users/test@info.com
	public User getUserByEmail(@PathParam("emailAddress") String emailAddress) {
		if (emailAddress == null || emailAddress.trim().length() == 0) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} else {
    		return UserRepository.getUserByEmail(emailAddress);
		}
		return new User();
	}


    @GET  // http://localhost:8080/market-api/users
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<User> getAllUsers() {
		List<User> shps = UserRepository.getAllUsers();			  
		return shps;
	}

    
	// {"firstName":"testtest","lastName":"test","username":"test","password":"test"}
	@POST  // http://localhost:8080/market-api/users
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	@NoAuthorization
	public UserStatus create(User usr) {
		UserStatus sts = new UserStatus();
		log.info("Start");
		try {
			boolean isExistingUser = UserRepository.isExistingUser(usr);
			if (isExistingUser) {
				Company company = UserRepository.findCompanyByEmail(usr);
				sts.setCompanyName(company.getCompanyName());
				sts.setCompanyCode(company.getCompanyCode());
        		sts.setStatus(Response.Status.CONFLICT.getStatusCode());
        		sts.setMessage("Email already in the system");
        		sts.setUser(usr);
        		throw new ServiceException(Status.CONFLICT, "Email already in the system");
        		// return sts;
			}
			
	
			Company company = UserRepository.findCompanyByName(usr);
			if (company != null) {
				log.info("company is not null!");
        		if (usr.getCompany().getCompanyCode()==null) {
    				log.warn(usr.getCompany().getCompanyName()+" already in the system, you must provide company code");
    				sts.setCompanyName(company.getCompanyName());
    				sts.setCompanyCode(company.getCompanyCode());
	        		sts.setStatus(Response.Status.EXPECTATION_FAILED.getStatusCode());
	        		sts.setMessage(usr.getCompany().getCompanyName()+" already in the system, you must provide company code");
	        		sts.setUser(usr);
	        		//throw new ServiceException(Status.EXPECTATION_FAILED, usr.getCompany().getCompanyName()+" already in the system, you must provide company code");
	        		throw new ServiceException(Status.EXPECTATION_FAILED, 
	        				usr.getCompany().getCompanyName()+" already in the system, you must provide company code",
	        				usr.getCompany().getCompanyName(),
	        				company.getCompanyCode()
	        				);
	        		// return sts;
        		} else {
        			log.info("Existing Company found");
        			if (!company.getCompanyCode().equalsIgnoreCase(usr.getCompany().getCompanyCode())) {
        				sts.setCompanyName(company.getCompanyName());
        				sts.setCompanyCode(company.getCompanyCode());
		        		sts.setStatus(Response.Status.NOT_ACCEPTABLE.getStatusCode());
		        		sts.setMessage(usr.getCompany().getCompanyName()+" already in the system & your company code does not match");
	    				log.warn(usr.getCompany().getCompanyName()+" already in the system & your company code does not match");
		        		sts.setUser(usr);
		        		//throw new ServiceException(Status.NOT_ACCEPTABLE, usr.getCompany().getCompanyName()+" already in the system, you must provide company code");
		        		throw new ServiceException(Status.EXPECTATION_FAILED, 
		        				usr.getCompany().getCompanyName()+" already in the system & your company code does not match",
		        				usr.getCompany().getCompanyName(),
		        				company.getCompanyCode()
		        				);
		        		// return sts;
        			}
        			if (company.isCarrier() != usr.getCompany().isCarrier() || company.isShipper() != usr.getCompany().isShipper()) {
        				sts.setCompanyName(company.getCompanyName());
        				sts.setCompanyCode(company.getCompanyCode());
		        		sts.setStatus(Response.Status.NOT_FOUND.getStatusCode());
		        		sts.setMessage(usr.getCompany().getCompanyName()+" already in the system & company type does not match");
		        		sts.setUser(usr);
	    				log.warn(usr.getCompany().getCompanyName()+" already in the system & company type does not match");
		        		//throw new ServiceException(Status.NOT_FOUND, usr.getCompany().getCompanyName()+" already in the system & company type does not match");
	    				throw new ServiceException(Status.EXPECTATION_FAILED, 
		        				usr.getCompany().getCompanyName()+" already in the system & company type does not match",
		        				usr.getCompany().getCompanyName(),
		        				company.getCompanyCode()
		        				);
		        		// return sts;
        			}
        		}
        		
        		// Company UPDATE
        		usr.setCompanyId(company.getCompanyId());
        		sts = UserRepository.createUser(usr, sts);
				sts.setMessage("User added to company "+usr.getCompany().getCompanyName());
				UserRepository.createCompanyUser(sts.getUser().getUserId(), (company == null), company.getCompanyId());
        		
			}else{
				// Company name does NOT exist
				int companyId = UserRepository.createCompany(usr, company);
				usr.setCompanyId(companyId);
				sts = UserRepository.createUser(usr, sts);
				if ((usr.getCompany().getCompanyCode()!=null) && (usr.getCompany().getCompanyCode().trim().length()>0)){
					sts.setMessage("New company "+usr.getCompany().getCompanyName()+ " created. Company code "+usr.getCompany().getCompanyCode()+ " ignored.");
				}else{
					sts.setMessage("New company "+usr.getCompany().getCompanyName()+ " created.");
				}
				UserRepository.createCompanyUser(sts.getUser().getUserId(), (company == null), companyId);
			}
			
			return sts;
		} catch(WebApplicationException e) { // ServiceException is already caught by WebApplicationException
			throw e;
		} catch (Exception e) {
			log.error(e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done");
		}
		return sts;
	}

	private Date futureExpiration() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 7);
		Date expires = cal.getTime();
		return expires;
	}

	private Token generateToken() {
		Token tkn = new Token();
		tkn.setAccessToken(MySqlPassword.generateToken());
		tkn.setExpires(futureExpiration());
		return tkn;
	}

	@POST
	@Path("logout") // // http://localhost:8080/marketplace/user/logout
	@Consumes("application/json,application/vnd.openport.password.v1+json") // MediaType.APPLICATION_JSON
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus logout(Token token) {
		UserStatus sts = new UserStatus();
		sts.setStatus(200);
		sts.setMessage("Ok");
//		DispatchDao dDao = new DispatchDao();
//		dDao.invalidateToken(token.getAccessToken());
		return sts;
	}

	@POST
	@Path("logout") // // http://localhost:8080/marketplace/user/logout
	@Consumes("application/vnd.openport.delivery.v2+json") // MediaType.APPLICATION_JSON
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus logoutV2(@HeaderParam("x-openport-token") String token) {
		UserStatus sts = new UserStatus();
		sts.setStatus(200);
		sts.setMessage("Ok");
//		DispatchDao dDao = new DispatchDao();
//		dDao.invalidateToken(token);
		return sts;
	}


	@POST
	@Path("retrievePassword") // // http://localhost:8080/marketplace/user/retrievePassword
	@Consumes("application/vnd.openport.password.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus retrievePasswordV1(Password token) {
		UserStatus sts = new UserStatus();
		if (token == null) {
			log.error("Password parameter missing");
			new ServiceException(Response.Status.UNAUTHORIZED);
		} else if (token.getAccessToken() == null || token.getAccessToken().trim().length()==0) {
			log.error("AccessToken parameter missing");
			new ServiceException(Response.Status.FORBIDDEN);
		} else if (token.getEmailAddress() == null || token.getEmailAddress().trim().length()==0) {
			log.error("EmailAddress parameter missing");
			new ServiceException(Response.Status.FORBIDDEN);
		} else if (token.getPhoneNumber() == null || token.getPhoneNumber().trim().length()==0) {
			log.error("PhoneNumber parameter missing");
			new ServiceException(Response.Status.FORBIDDEN);
		}  else {
			try {
				log.info("Updating password for MobileUser with accessToken:"+token.getAccessToken());
//				DispatchDao dDao = new DispatchDao();
//				String accessToken = token.getAccessToken();
//				String emailAddr = token.getEmailAddress();
//				String phoneNbr = token.getPhoneNumber();
//				String tmpPassword = UUID.randomUUID().toString().substring(0,  5).toLowerCase();
//				MobileUser mobileUser = dDao.retrieveMobileUserPassword(accessToken, emailAddr, phoneNbr, tmpPassword);
//				if (mobileUser == null) {
//					new ServiceException(Response.Status.FORBIDDEN);
//				} else{
//					log.info("Emailing updated password for MobileUser with accessToken:"+token.getAccessToken()+" to "+mobileUser.getEmailAddress());
//					emailPasswordChangeNotification(mobileUser.getEmailAddress(), "OpenPort Mobile App – Password Reset", "Your new password is "+tmpPassword);
//					sts.setStatus(200);
//					sts.setMessage("New password emailed to "+mobileUser.getEmailAddress());
//					log.info("New password sent for MobileUser with accessToken:"+token.getAccessToken()+" to "+mobileUser.getEmailAddress());
//				}
			} catch (Exception ex) {
				new ServiceException(Response.Status.BAD_REQUEST);
			}
		}
		return sts;
	}

	@POST
	@Path("retrievePassword") // // http://localhost:8080/marketplace/user/retrievePassword
	@Consumes("application/vnd.openport.delivery.v2+json") // MediaType.APPLICATION_JSON
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus retrievePasswordV2(@HeaderParam("x-openport-token") String token, Password password) {
		UserStatus sts = new UserStatus();
		if (password == null) {
			log.error("Password parameter missing");
			new ServiceException(Response.Status.UNAUTHORIZED);
		} else if (token == null || token.trim().length()==0) {
			log.error("AccessToken parameter missing");
			new ServiceException(Response.Status.FORBIDDEN);
		} else if (password.getEmailAddress() == null || password.getEmailAddress().trim().length()==0) {
			log.error("EmailAddress parameter missing");
			new ServiceException(Response.Status.FORBIDDEN);
		} else if (password.getPhoneNumber() == null || password.getPhoneNumber().trim().length()==0) {
			log.error("PhoneNumber parameter missing");
			new ServiceException(Response.Status.FORBIDDEN);
		}  else {
			password.setAccessToken(token);
			try {
				log.info("Updating password for MobileUser with accessToken:"+password.getAccessToken());
//				DispatchDao dDao = new DispatchDao();
//				String accessToken = password.getAccessToken();
//				String emailAddr = password.getEmailAddress();
//				String phoneNbr = password.getPhoneNumber();
//				String tmpPassword = UUID.randomUUID().toString().substring(0,  5).toLowerCase();
//				MobileUser mobileUser = dDao.retrieveMobileUserPassword(accessToken, emailAddr, phoneNbr, tmpPassword);
//				if (mobileUser == null) {
//					new ServiceException(Response.Status.FORBIDDEN);
//				} else{
//					log.info("Emailing updated password for MobileUser with accessToken:"+password.getAccessToken()+" to "+mobileUser.getEmailAddress());
//					emailPasswordChangeNotification(mobileUser.getEmailAddress(), "OpenPort Mobile App – Password Reset", "Your new password is "+tmpPassword);
//					sts.setStatus(200);
//					sts.setMessage("New password emailed to "+mobileUser.getEmailAddress());
//					log.info("New password sent for MobileUser with accessToken:"+password.getAccessToken()+" to "+mobileUser.getEmailAddress());
//				}
			} catch (Exception ex) {
				new ServiceException(Response.Status.BAD_REQUEST);
			}
		}
		return sts;
	}

	@POST
	@Path("forgotPassword") // // http://localhost:8080/marketplace/user/forgotPassword
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@NoAuthorization
	public UserStatus forgotPassword(User token) {
		UserStatus sts = new UserStatus();
		sts.setStatus(200);
		sts.setMessage("Success");
		if (token == null) {
			log.error("Password parameter missing");
			new ServiceException(Response.Status.UNAUTHORIZED);
		} else {
			String sendTo = token.getUsername();
			if (sendTo == null || sendTo.trim().length()==0) {
				log.error("EmailAddress parameter missing");
				new ServiceException(Response.Status.FORBIDDEN);
			} else if (token.getFirstName() == null || token.getFirstName().isEmpty()) {
				log.error("First name parameter missing");
				new ServiceException(Response.Status.FORBIDDEN);
			} else if (token.getLastName() == null || token.getLastName().isEmpty()) {
				log.error("Last name parameter missing");
				new ServiceException(Response.Status.FORBIDDEN);
			}  else {
				try {
					String tmpPassword = TemporaryPassword.generate(7);
					if (UserRepository.updateTempPassword(sendTo, token.getFirstName(), token.getLastName(), tmpPassword)) {
						String message = "Your new temporary password is "+tmpPassword;
						Response response = Notify.sendEmail(sendTo, "Password Reset", message);
						if (response.getStatus() != Response.Status.OK.getStatusCode()) {
							//sts.setStatus(response.getStatus());
							sts.setStatus(Response.Status.EXPECTATION_FAILED.getStatusCode()); //417
							sts.setMessage("Unable to send password");
							return sts;
						}
					}else{
						//log.info("NOT UPDATED");
						sts.setStatus(Response.Status.NOT_FOUND.getStatusCode()); // 404
						sts.setMessage("Unable to set password");
						return sts;
					}
				} catch (Exception ex) {
					log.error("Unable to reset password for "+sendTo, ex);
					// new ServiceException(Response.Status.BAD_REQUEST);
				}
			}
		}
		return sts;
	}

	

	@PUT
	@Path("{userId}") // // http://localhost:8080/market-api/users/1234
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus update(@PathParam("userId") int userId, Password usr) {
		UserStatus sts = new UserStatus();
		sts.setStatus(200);
		sts.setMessage("Success");
		log.info("Start");
		if (userId == 0) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} else {
			UserRepository.update(userId, usr, sts);
		}
		return sts;
	}


	public UserStatus checkLanguage(UserStatus sts, int langVer, String langCd) {
		//return dDao.checkLanguage(sts, langVer);


		int latestVer = 0;
		long currentLanguageId = -1;
		try {
			log.debug("langVer:"+langVer+" langCd:"+langCd);
			latestVer=doGetMaxLabelIdFromLabel();

			log.debug(latestVer);
			
		
			if (langVer>=latestVer) {
				return sts; // nothing to do
			}

    		LanguagePack languageUpdate = new LanguagePack(latestVer);
    		sts.setUpdateLanguage(languageUpdate);
    		
	    	List<Language> languagePacks = new LinkedList<Language>();
	    	languageUpdate.setLanguagePack(languagePacks);

	    	
	    	HashMap<Integer, Language> languages = new HashMap<Integer, Language>();
	    	List<Language> languageList = new ArrayList();
	    	languageList = doGetLanguages();
	    	/*
	    	if ((langCd !=null) && (langCd.trim().length()>0)){
	    		languageList = doGetLanguagesByCd(langCd);
	    		//languageList = doGetLanguages();
	    	}else{
	    		languageList = doGetLanguages();
	    	}
	    	*/
	    	log.debug("languageList size:"+languageList.size());
	    	for (Language l: languageList){
	    		int id = l.getLanguageId();
				String cd = l.getLanguageCd();
				String txt = l.getLanguageName();
				log.debug("language:"+id+" "+cd+" "+txt);
				languages.put(id, new Language(cd,txt));
				// languages.put(id, l);
	    	}
	    	
	    	log.debug("langVer:"+langVer);
	    	List<Label> labelsList = new ArrayList();
	    	
	    	if ((langCd !=null) && (langCd.trim().length()>0)){
	    		//labelsList = doGetLabels(langVer);
	    		labelsList = doGetLabelsByLanguageAndVer(langCd, langVer);
	    	}else{
	    		labelsList = doGetLabels(langVer);
	    	}
	    	
	    	//labelsList = doGetLabels(langVer);
	    	log.debug(" size:"+labelsList.size());
	    	if ( (labelsList==null) || (labelsList.size()==0)) {
	    		log.debug("returning");
				return sts;
			}
	    	
	    	List<Label> labels = null;
	    	List<Label> labels2 = null;
	    	//log.debug("looping label");
	    	try {
	    		int langId =0;
	    		for (Label lbl: labelsList) {
	    			try {
	    				//log.debug("1");
				    	
				    	langId = lbl.getLanguageId();
				    	String cd = lbl.getLabelCd();
				    	String txt = lbl.getLabelTxt();
				    	log.debug(" langId:"+langId+" cd:"+cd+" txt:"+txt);
			            // log.debug("\t" + langId+"\t" + cd+"\t" + txt);
			            if (langId != currentLanguageId) {
			            	//log.debug("langId:"+langId+" currentLanguageId:"+currentLanguageId);
			            	Language language = languages.get(langId);
			     	    	labels = language.getData();
			     	    	//log.debug("labels size:"+labels.size());
							languagePacks.add(language);
			     	    	currentLanguageId = langId;
			            }
			        	labels.add(new Label(cd, txt));
			        	//log.debug("labels size:"+labels.size());
			        	Language language2 = languages.get(langId);
			        	labels2 = language2.getData();
		     	    	//log.debug("labels2 size:"+labels2.size());
					} catch (Exception ex) {
						log.error(ex,ex);
					}
			    	
			    }
	    		
	    		//Language language = languages.get(langId);
	  	    	//labels = language.getData();
	 			//languagePacks.add(language);
			} catch (Exception ex) {
				log.error(ex,ex);
			}
		    
		} catch (Exception ex) {
			log.error(ex,ex);
		} 
		return sts;
	
		
		
	}
	


	
	private UserStatus doFindLabels(UserStatus sts, String langCd, int langVer) {
		// TODO Auto-generated method stub
		
		String sql = 	"SELECT Label_CD, Label_Txt, label_id "
				+ " FROM marketplace_db.label "
				+ " where Label_ID >= ? "
				+ " and exists (select language_id from marketplace_db.language where language_cd = ? "
								+ " and language.language_id = label.language_id)"
				+ " ORDER BY label_id;";
		
		log.debug("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1,langVer);
				pstmt.setString(2, langCd);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Label> driverRawMapper = new ResultSetMapper<Label>();
	        	List<Label> pojoList = driverRawMapper.mapResultSetToObject(rs, Label.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		for (Label lbl: pojoList){
	        			sts.setLangVer(lbl.getLabelId());
						sts.addLabel(lbl);
	        		}
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return sts;
	}
	
	
	private List<Label> doGetLabelsByLanguageAndVer(String langCd, int langVer) {
		// TODO Auto-generated method stub
		
		String sql = 	"SELECT  "
				+" language_id, "
				+" label_id, Label_CD, label_txt "
				+" create_dt, create_user_id "
				+ " FROM marketplace_db.label "
				+ " where Label_ID >= ? "
				+ " and exists (select language_id from marketplace_db.language where language_cd = ? "
								+ " and language.language_id = label.language_id)"
				+ " ORDER BY label_id;";
		
		log.debug("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1,langVer);
				pstmt.setString(2, langCd);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Label> driverRawMapper = new ResultSetMapper<Label>();
	        	List<Label> pojoList = driverRawMapper.mapResultSetToObject(rs, Label.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return null;
	}
	
	private List<Label> doGetLabels(long langVer) {
		// TODO Auto-generated method stub
		
		String sql = 	" select "
				+" language_id, "
				+" label_id, Label_CD, label_txt "
				+" create_dt, create_user_id "
				+ " from marketplace_db.label "
				+ " where label_id > ? "
				+ " order by language_id, label_id";
		
		log.debug("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setLong(1, langVer);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Label> driverRawMapper = new ResultSetMapper<Label>();
	        	List<Label> pojoList = driverRawMapper.mapResultSetToObject(rs, Label.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return null;
	}



	private List<Language> doGetLanguages() {
		String sql = " select language_id, language_cd, language_name "	
				//" select language_id,language_name, "
				// +" Iso_639_2_CD, "
				//+" language_cd, create_user_id,create_dt,update_user_id,update_dt "
				+" from  marketplace_db.language ; ";
				
		log.debug("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Language> driverRawMapper = new ResultSetMapper<Language>();
	        	List<Language> pojoList = driverRawMapper.mapResultSetToObject(rs, Language.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<Language> doGetLanguagesByCd(String langCd) {
		String sql = " select language_id, language_cd, language_name "	
				//" select language_id,language_name, "
				// +" Iso_639_2_CD, "
				//+" language_cd, create_user_id,create_dt,update_user_id,update_dt "
				+" from  marketplace_db.language  "
				+" where language_cd = ? ;";
				
		log.debug("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, langCd);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Language> driverRawMapper = new ResultSetMapper<Language>();
	        	List<Language> pojoList = driverRawMapper.mapResultSetToObject(rs, Language.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	private List<Language> doGetLanguageList() {
		String sql = " select  language_cd, language_name "	
				//" select language_id,language_name, "
				// +" Iso_639_2_CD, "
				//+" language_cd, create_user_id,create_dt,update_user_id,update_dt "
				+" from  marketplace_db.language ; ";
				
		log.debug("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Language> driverRawMapper = new ResultSetMapper<Language>();
	        	List<Language> pojoList = driverRawMapper.mapResultSetToObject(rs, Language.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	private int doGetMaxLabelIdFromLabel() {
		String sql = 	" select max(label_id) as label_id,  '' as label_cd,'' as label_txt,now() as create_dt,'' as create_user_id  from marketplace_db.label ; ";
		
		log.debug("sql:"+sql);
		
	
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Label> driverRawMapper = new ResultSetMapper<Label>();
	        	List<Label> pojoList = driverRawMapper.mapResultSetToObject(rs, Label.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		Label lbl = pojoList.get(0);
	        		return lbl.getLabelId();
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return 0;
	}
	
	private void checkVersion(String langCd, String appVersion, UserStatus sts, String appId) {
		if (appId!=null && appId.equalsIgnoreCase("com.openport.marketplace.uat")) {
			if (appVersion.compareTo(CURRENT_UAT_VERSION) < 0) {
				if (langCd.equalsIgnoreCase("id")) {
					sts.setMessage("Versi terbaru (" + CURRENT_UAT_VERSION + ") telah tersedia, silahkan unduh dan instal dari https://www.pgyer.com/X4b5");
				} else if (langCd.equalsIgnoreCase("zh")) {
					sts.setMessage("Openpor物流手机APP（安卓系统）已更新至最新版本" + CURRENT_UAT_VERSION + "， 请通过https://www.pgyer.com/X4b5链接下载安装");
				} else if (langCd.equalsIgnoreCase("zh-Hant")) {
					sts.setMessage("Openpor物流手機APP（安卓系統）已更新至最新版本" + CURRENT_UAT_VERSION + "， 請通過https://www.pgyer.com/X4b5鏈接下載安裝");
				} else if (langCd.equalsIgnoreCase("ur")) {
					sts.setMessage("A new version (" + CURRENT_UAT_VERSION + ") is available, please download & install from https://www.pgyer.com/X4b5");
				} else {
					sts.setMessage("A new version (" + CURRENT_UAT_VERSION + ") is available, please download & install from https://www.pgyer.com/X4b5");
				}
			}
		} else if (appId!=null && appId.toLowerCase().startsWith("com.openport.marketplace")) { // if (appId.equalsIgnoreCase("com.openport.delivery.uat")) {
			if (appVersion.compareTo(CURRENT_VERSION) < 0) {
				if (langCd.equalsIgnoreCase("id")) {
					sts.setMessage("Versi terbaru (" + CURRENT_VERSION + ") telah tersedia, silahkan unduh dan instal dari https://goo.gl/aDSMdF");
				} else if (langCd.equalsIgnoreCase("zh")) {
					sts.setMessage("Openpor物流手机APP（安卓系统）已更新至最新版本" + CURRENT_VERSION + "， 请通过http://www.pgyer.com/ldXH链接下载安装");
				} else if (langCd.equalsIgnoreCase("zh-Hant")) {
					sts.setMessage("Openpor物流手機APP（安卓系統）已更新至最新版本" + CURRENT_VERSION + "， 請通過http://www.pgyer.com/ldXH鏈接下載安裝");
				} else if (langCd.equalsIgnoreCase("ur")) {
					sts.setMessage("A new version (" + CURRENT_VERSION + ") is available, please download & install from https://goo.gl/aDSMdF");
				} else {
					sts.setMessage("A new version (" + CURRENT_VERSION + ") is available, please download & install from https://goo.gl/aDSMdF");
				}
			}
		}
	}
	
	@GET
	@Path("role/{emailAddress}")// http://localhost:8080/market-api/users/role/png@png.com
	@Produces({ MediaType.APPLICATION_JSON })
	public UserRole getUserRole(@PathParam("emailAddress") String emailAddress) {
		log.info("users/role/{username}");
		
		return UserRepository.getUserRole(emailAddress);
    }
	
	@POST
	@Path("/role/set") // http://localhost:8080/market-api/users/role/set
	@Consumes(MediaType.APPLICATION_JSON)
	public void setUserRole(UserRole userRole) {
		log.info("users/role/set");
		
		userRole.setCreateDt(new Timestamp(new Date().getTime()));
		userRole.setUpdateDt(new Timestamp(new Date().getTime()));
		
		UserRepository.setUserRole(userRole);
	}
	
	@POST
	@Path("/role/update") // http://localhost:8080/market-api/users/role/update
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateUserRole(UserRole userRole) {
		log.info("users/role/update");
		
		userRole.setUpdateDt(new Timestamp(new Date().getTime()));
		
		UserRepository.updateUserRole(userRole);
	}
	
	@GET
	@Path("role/allUsers")// http://localhost:8080/market-api/users/role/allUsers
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllUserRole() {
		log.info("users/role/allUsers");
		
		String jsonData = UserRepository.getAllUserRole();
		
        return Response.ok(jsonData).build();
    }
	
	@GET
	@Path("roles")// http://localhost:8080/market-api/users/roles
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Role> getRoles() {
		log.info("users/roles");
		
		return UserRepository.getRoles();
    }
	
}
