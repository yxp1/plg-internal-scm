package com.openport.marketplace.view;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mysql.jdbc.NotUpdatable;
import com.openport.marketplace.helper.TemporaryPassword;
import com.openport.marketplace.json.CarrierRegion;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.CompanyAddress;
import com.openport.marketplace.json.CompanyBranch;
import com.openport.marketplace.json.CompanyClient;
import com.openport.marketplace.json.CompanyEquipment;
import com.openport.marketplace.json.CompanyFile;
import com.openport.marketplace.json.CompanyService;
import com.openport.marketplace.json.CompanyUser;
import com.openport.marketplace.json.CorporateAddress;
import com.openport.marketplace.json.Country;
import com.openport.marketplace.json.DocumentType;
import com.openport.marketplace.json.Equipment;
import com.openport.marketplace.json.EquipmentType;
import com.openport.marketplace.json.FileDocumentType;
import com.openport.marketplace.json.HttpStatus;
import com.openport.marketplace.json.Language;
import com.openport.marketplace.json.Photo;
import com.openport.marketplace.json.Service;
import com.openport.marketplace.json.ServiceArea;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.Timezone;
import com.openport.marketplace.json.User;
import com.openport.marketplace.json.UserLanguage;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.marketplace.repository.MailGun;
import com.openport.marketplace.repository.Notify;
import com.openport.util.db.ResultSetMapper;
import com.openport.util.password.MySqlPassword;

@Path("truckers") // http://localhost:8080/market-api/truckers
public class TruckerResource {
	private int UPDATE_ID=0;
	static final transient Logger log = Logger.getLogger(TruckerResource.class);
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("corpAddress/{companyAddressId}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public CorporateAddress fetchCorpAddress(@PathParam("companyAddressId") Integer companyAddressId) {
		try {
			String sql="";
			sql = "select country_2_cd,b.country_id,a.address_line_1,a.address_line_2,a.address_line_3,a.city_name,a.province_id,a.postal_cd,c.province_name,d.company_name from company_address a inner join country b on a.country_id=b.country_id and a.company_address_id="+companyAddressId + " left join province c on a.province_id=c.province_id left join company d on a.company_address_id=d.corporate_address_id";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CorporateAddress> driverRawMapper = new ResultSetMapper<CorporateAddress>();
		        	List<CorporateAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, CorporateAddress.class);
		        	CorporateAddress corp =new CorporateAddress();
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		
		        		corp =  pojoList.get(0);
		        		if(corp.getAddressLine2() != null) {
		        			corp.setAddressLine1(corp.getAddressLine1() + " " + corp.getAddressLine2());
		        		}
		        		if(corp.getAddressLine3() != null) {
		        			corp.setAddressLine1(corp.getAddressLine1() + " " + corp.getAddressLine3());
		        		}
		        		return corp;
		        	}
		        	
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	@GET // http://localhost:8080/market-api/truckers
	@Path("compCountry/{companyAddressId}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public Country fetchCompanyCountry(@PathParam("companyAddressId") Integer companyAddressId) {
		try {
			String sql="";
			sql = "select country_2_cd,b.country_id from company_address a inner join country b on a.country_id=b.country_id and a.company_address_id="+companyAddressId;
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Country> driverRawMapper = new ResultSetMapper<Country>();
		        	List<Country> pojoList = driverRawMapper.mapResultSetToObject(rs, Country.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList.get(0);
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	
	@GET // http://localhost:8080/market-api/truckers
	@Path("validateregion/{region}/{country}") // mode is N (new) or U (update)
	@Produces({ MediaType.APPLICATION_JSON })
	public ServiceArea validateRegion(@PathParam("region") String region,@PathParam("country") Integer country) {
		try {
			String sql="";
			sql = "select country_id from service_area where service_area_name='"+region + "' and country_id="+country;
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<ServiceArea> driverRawMapper = new ResultSetMapper<ServiceArea>();
		        	List<ServiceArea> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceArea.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList.get(0);
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	
	@GET // http://localhost:8080/market-api/truckers
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Country> getAllCountry() {
		List<Country> countryList = new ArrayList<Country>();
		try {

			String sql = "SELECT * FROM `country`;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.Country usr = new com.openport.marketplace.model.Country(rs);
						Country cntry = new Country();
						cntry.setCountryId(usr.getCountryId());
						cntry.setCountryName(usr.getCountryName());
						cntry.setCountry2Cd(usr.getCountry2Cd());
						cntry.setCountry3Cd(usr.getCountry3Cd());
						countryList.add(cntry);
					}
					return countryList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return countryList;
	}

	@GET
	@Path("users/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<User> getUsersByCompanyId(@PathParam("companyId") Integer companyId) {
		
	//	return getCompanyUser(companyId);
		return getCompanyUsers(companyId);
		
	}	

	@GET
	@Path("validateuser/{userId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public int getUsersByEmailAddress(@PathParam("userId") String userId) {
		
	//	return getCompanyUser(companyId);
		return validateUserId(userId);
		
	}	

	@GET
	@Path("profiles/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public Company getCompanyProfileById(@PathParam("companyId") Integer companyId) {
		try {
		Company company =doGetCompanyById(companyId);
		company.setClients(getClients(company.getCompanyId()));
		company.setFileDocTypes(getFileDocumentTypeLegalDocument(company.getCompanyId()));
		return company;
		
		}catch(Exception ee) {
			log.error("Error company ID"+companyId,ee);
		}
		
		return null;
	}
	
	@GET
	@Path("photos/{companyId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<FileDocumentType> getPhotosDocument(@PathParam("companyId") Integer companyId) {
	try {
		return getFileDocumentTypePhotos(companyId);
	}catch(Exception ee) {
		log.error("Error file document type",ee);
		
	}
	return null;
	}
	
	@GET
	@Path("legaldocuments/{companyId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<FileDocumentType> getLegalDocument(@PathParam("companyId") Integer companyId) {
	try {
		return getFileDocumentTypeLegalDocument(companyId);
	}catch(Exception ee) {
		log.error("Error file document type",ee);
		
	}
	return null;
	}
	
	
	@GET
	@Path("companyAdress/{corporateAddressId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public CompanyAddress getCompanyAddressById(@PathParam("corporateAddressId") Integer corporateAddressId) {
		try {
			return doGetCompanyAddress(corporateAddressId);
					
		}catch(Exception ee) {
			log.error("Error corporate address ID"+corporateAddressId,ee);
		}
		
		return null;
	}
	
	
	@GET
	@Path("companyuser/{companyUserId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<User> getCompanyUserByCompanyId(@PathParam("companyUserId") Integer companyId) {
		try {
			//return getCompanyUser(companyId);
				return getCompanyUsers(companyId);	
		}catch(Exception ee) {
			log.error("Error company user "+companyId,ee);
		}
		
		return null;
	}
	
	@GET
	@Path("companyServices/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyService> getCompanyServices(@PathParam("companyId") Integer companyId) {
		try {
			return getCompanyServiceModel(companyId);
					
		}catch(Exception ee) {
			log.error("Error company user "+companyId,ee);
		}
		
		return null;
	}
	
	@GET
	@Path("currentservices/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyService> getCurrentServices(@PathParam("companyId") Integer companyId) {
		try {
			return getCompanyServiceModel(companyId);
				
					
		}catch(Exception ee) {
			log.error("Error company user "+companyId,ee);
		}
		
		return null;
	}
	
	@GET
	@Path("currentservicearea/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CarrierRegion> getCurrentServiceArea(@PathParam("companyId") Integer companyId) {
		try {
			return getCarrierRegionModel(companyId);
				
					
		}catch(Exception ee) {
			log.error("Error company user "+companyId,ee);
		}
		
		return null;
	}
	
	@GET
	@Path("currentequipment/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyEquipment> getCurrentEquipment(@PathParam("companyId") Integer companyId) {
		try {
			return getCompanyEquipmentModel(companyId);
				
					
		}catch(Exception ee) {
			log.error("Error company user "+companyId,ee);
		}
		
		return null;
	}
	@GET
	@Path("carrierregion/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CarrierRegion> getCarrierRegions(@PathParam("companyId") Integer companyId) {
		try {
			return getCarrierRegionModel(companyId);
					
		}catch(Exception ee) {
			log.error("Error company user "+companyId,ee);
		}
		
		return null;
	}
	
	@GET
	@Path("companyequipments/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyEquipment> getEquipments(@PathParam("companyId") Integer companyId) {
		try {
			return getCompanyEquipmentModel(companyId);
					
		}catch(Exception ee) {
			log.error("Error company user "+companyId,ee);
		}
		
		return null;
	}
	
	
	@GET
	@Path("companyAddress/{corporateAddressId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public CompanyAddress getCorporateAddress(@PathParam("corporateAddressId") Integer corporateAddressId) {
		try {
			return getCompanyAddressById(corporateAddressId);
					
		}catch(Exception ee) {
			log.error("Error company address",ee);
		}
		
		return null;
	}
	

	@GET
	@Path("company/{companyId}") // //
	@Produces({ MediaType.APPLICATION_JSON })
	public Company getCompanyById(@PathParam("companyId") Integer companyId) {
		Company company = new Company();
		try {
		

		CompanyAddress corporateAddress = new CompanyAddress();
  		List<CompanyClient> clients = new ArrayList();
  		List<CompanyBranch> companyBranchPojoList = new ArrayList();
  		List<CompanyBranch> pojoList =  new ArrayList();
		if (companyId < -1 ) {
			corporateAddress = new CompanyAddress();
			corporateAddress.setAddressLine1(null);
			corporateAddress.setAddressLine2(null);
			corporateAddress.setAddressLine3(null);
			corporateAddress.setCityName(null);
			corporateAddress.setPostalCd(null);
			corporateAddress.setProvinceName(null);
			corporateAddress.setCountryName(null);
			company.setCorporateAddress(corporateAddress);
			company.setClients(clients);
			//return company;
			//new ServiceException(Response.Status.BAD_REQUEST);
		} else {
			
			company = new Company();
			int companyAddressId = -1;
			int branchAddressId = -1;
			int countryId = -1 ;
			int provinceId = -1 ;
			
			try {
		
				
				company = doGetCompanyById(companyId);
				companyAddressId = company.getCorporateAddressId();
			
				
				// COMPANY ADDRESS 
	  			corporateAddress = doGetCompanyAddress(companyAddressId);
	  			company.setCorporateAddress(corporateAddress);
	  			
	  			

				
				//** additional Objects **/
	  			company.setClients(getClients(company.getCompanyId()));
	  			company.setFileDocTypes(getFileDocumentTypeByCompanyId(company.getCompanyId()));
				company.setCompanyUsers(getCompanyUser(company.getCompanyId()));
				company.setCarrierRegions(getCarrierRegionModel(company.getCompanyId()));
				company.setCompanyServices(getCompanyServiceModel(company.getCompanyId()));
				company.setCompanyEquipments(getCompanyEquipmentModel(companyId));
			} finally {
				log.debug("Done");
			}
		}

		}catch(Exception ee) {
			log.error("Error for company ID"+companyId,ee);
		}
	
		return company;
	}

	private List<CompanyEquipment> getCompanyEquipmentModel(Integer companyId) {
		List<CompanyEquipment> j_list = new ArrayList<>();
		List<CompanyEquipment> m_list = getCompanyEquipmentByCompanyId(companyId);
		if(m_list!=null && m_list.size()>0) {
			for(CompanyEquipment ce : m_list) {
				Equipment e = getEquipmentById(ce.getEquipmentId());
				if(e.getEquipmentTypeId() >0 ){
					EquipmentType et = getEquipmentTypeById(e.getEquipmentTypeId());
					e.setEquipmentType(et);
					ce.setEquipment(e);
					j_list.add(ce);
				}
				
			}
			return j_list;
		}
		return null;
	}


	private List<EquipmentType> getEquipmentType() { 
		List<EquipmentType> serviceList = new ArrayList<EquipmentType>();
		String sql = "SELECT equipment_type_id,equipment_type_name "
				+ "      FROM equipment_type where equipment_type_id > 0";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EquipmentType> driverRawMapper = new ResultSetMapper<EquipmentType>();
	        	List<EquipmentType> pojoList = driverRawMapper.mapResultSetToObject(rs, EquipmentType.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		}
	
	private EquipmentType getEquipmentTypeById(Integer id) { 
		List<EquipmentType> serviceList = new ArrayList<EquipmentType>();
		String sql = "SELECT equipment_type_id,equipment_type_name "
				+ "      FROM equipment_type where equipment_type_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EquipmentType> driverRawMapper = new ResultSetMapper<EquipmentType>();
	        	List<EquipmentType> pojoList = driverRawMapper.mapResultSetToObject(rs, EquipmentType.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		}


	
	private CompanyEquipment getCompanyEquipmentyCompanyEquipmentId(Integer companyEquipmentId) { 
		List<CompanyEquipment> serviceList = new ArrayList<CompanyEquipment>();
		String sql = "SELECT company_equipment_id,company_id,equipment_id,equipment_count "
				+ "      FROM company_equipment where company_equipment_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyEquipmentId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyEquipment> driverRawMapper = new ResultSetMapper<CompanyEquipment>();
	        	List<CompanyEquipment> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyEquipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		}

	
	
	private List<CompanyEquipment> getCompanyEquipmentByCompanyId(Integer companyId) { 
		List<CompanyEquipment> serviceList = new ArrayList<CompanyEquipment>();
		String sql = "SELECT company_equipment_id,company_id,equipment_id,equipment_count "
				+ "      FROM company_equipment where company_id=? and company_equipment_id > 0";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyEquipment> driverRawMapper = new ResultSetMapper<CompanyEquipment>();
	        	List<CompanyEquipment> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyEquipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}else {
					log.info("No Service record!!");
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		}

	
	
	private Equipment getEquipmentById(Integer id) { 
	List<Equipment> serviceList = new ArrayList<Equipment>();
	String sql = "SELECT equipment_type_id,equipment_name "
			+ "      FROM equipment where equipment_id=?";
	try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql);) {
		pstmt.setInt(1, id);
		try (ResultSet rs = pstmt.executeQuery()) {
			ResultSetMapper<Equipment> driverRawMapper = new ResultSetMapper<Equipment>();
        	List<Equipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Equipment.class);
        	if (pojoList!=null && !pojoList.isEmpty()) {
        		return pojoList.get(0);
			}else {
				log.info("No Service record!!");
			}
		}
	} catch (Exception e) {
		log.error("Error message: " + e, e);
		new ServiceException(Response.Status.CONFLICT);
	}
	return null;
	}
	

	
	
	private List<CompanyService> getCompanyServiceModel(Integer companyId) {
		List<CompanyService> j_cs = new ArrayList<>();
		List<CompanyService> m_cs = getCompanyServiceByCompanyId(companyId);
		if(m_cs!=null && m_cs.size()>0) {
			for(CompanyService cs : m_cs) {
				Service s = getServiceById(cs.getServiceId());
				cs.setService(s);
				j_cs.add(cs);
				
			}
			return j_cs;
		}
		return null;
	}
	
	private Service getServiceById(Integer id) { 
	List<Service> serviceList = new ArrayList<Service>();
	String sql = "SELECT service_id,service_name,is_transportation "
			+ "      FROM service where service_id=?";
	try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql);) {
		pstmt.setInt(1, id);
		try (ResultSet rs = pstmt.executeQuery()) {
			ResultSetMapper<Service> driverRawMapper = new ResultSetMapper<Service>();
        	List<Service> pojoList = driverRawMapper.mapResultSetToObject(rs, Service.class);
        	if (!pojoList.isEmpty()) {
        		return pojoList.get(0);
			}else {
				log.info("No Service record!!");
			}
		}
	} catch (Exception e) {
		log.error("Error message: " + e, e);
		new ServiceException(Response.Status.CONFLICT);
	}
	return null;
	}
	
	private List<CarrierRegion> getCarrierRegionModel(Integer companyId) {
		List<CarrierRegion> j_region = new LinkedList<>();
		List<CarrierRegion> m_region = getCarrierRegionByCompanyId(companyId);
		if(m_region!=null && m_region.size()>0){
		for(CarrierRegion j_cr : m_region) {
			ServiceArea srvc = getServiceAreaById(j_cr.getServiceAreaId());
			srvc.setCompanyId(companyId);
			j_cr.setServiceArea(srvc);
			j_region.add(j_cr);
		}
		}
		return j_region;
	}
	
	@GET
	@Path("documents/{documentId}") // // http://localhost:8080/market-api/users/1234
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<DocumentType> getDocumentType(@PathParam("documents") String docs) {
		List<DocumentType> docList = new ArrayList<DocumentType>();
		try {
			String sql = "SELECT * FROM `document_type`";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.DocumentType doc = new com.openport.marketplace.model.DocumentType(
								rs);
						DocumentType dt = new DocumentType();
						dt.setDescriptionTxt(doc.getDescriptionTxt());
						dt.setDocumentTypeId(doc.getDocumentTypeId());
						docList.add(dt);
					}
					return docList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}

		return new ArrayList<DocumentType>();
	}

	
	@GET
	@Path("transportservices/{companyId}") // // http://localhost:8080/market-api/users/1234
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Service> getTransportationServices(@PathParam("companyId") Integer companyId) {
		log.info("Servics" + companyId);
		String sql=" SELECT a.service_id, a.service_name, b.company_service_id,b.company_id,is_transportation "+
				  " FROM service a "+
			       " LEFT OUTER JOIN company_service b "+
			        "  ON a.service_id = b.service_id and b.company_id=?";
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Service> driverRawMapper = new ResultSetMapper<Service>();
	        	List<Service> pojoList = driverRawMapper.mapResultSetToObject(rs, Service.class);
	        	List<Service> j_pojoList = new LinkedList<>();
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		for(Service js : pojoList) {
	        			log.info("Service Name :"+js.getServiceName());
	        			if(js.getCompanyServiceId()!=null) {
	        				js.setStatus(Boolean.TRUE);
	        				
	        			}else {
	        				js.setStatus(Boolean.FALSE);
	        				js.setCompanyServiceId(-1);
	        			}
	        			j_pojoList.add(js);
	        		}
	        		return j_pojoList;
				}else {
					// log.info("No Service record!!");
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
return null;
	}
	
	@GET
	@Path("services/{serviceId}") // // http://localhost:8080/market-api/users/1234
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Service> getServices(@PathParam("services") String servc) {
		List<Service> serviceList = new ArrayList<Service>();
		String sql = "SELECT service_id,service_name,is_transportation "
				+ "      FROM service";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Service> driverRawMapper = new ResultSetMapper<Service>();
	        	List<Service> pojoList = driverRawMapper.mapResultSetToObject(rs, Service.class);
	        	if (!pojoList.isEmpty()) {
	        		return pojoList;
				}else {
					// log.info("No Service record!!");
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
return null;
	}
	
	@GET
	@Path("equipmenttypes/{equipmentId}") // // http://localhost:8080/market-api/users/1234
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<EquipmentType> getEquipmentTypesObjects(@PathParam("equipmenttypes") String servc) {
		try {
			return getEquipmentType();
		
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
return null;
	}
	
	
	
	@GET
	@Path("regions/{companyId}") // // http://localhost:8080/market-api/users/1234
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ServiceArea> getRegions(@PathParam("companyId") Integer companyId) {
		log.info("company Id"+companyId);
		if(companyId==-1) {
return		getServiceArea();
		}else {
		return	getServiceArea(companyId);
		}

	}
	private List<ServiceArea> getServiceArea() {
		List<ServiceArea> serviceList = new ArrayList<ServiceArea>();
		String sql = "SELECT a.service_area_id,a.service_area_name,b.country_id,b.country_name,b.country_2_cd "
				+ "      FROM service_area a inner join country b on a.country_id=b.country_id order by b.country,a.service_area_name";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ServiceArea> driverRawMapper = new ResultSetMapper<ServiceArea>();
	        	List<ServiceArea> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceArea.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	
	private List<ServiceArea> getServiceArea(Integer companyId) {
		List<ServiceArea> serviceList = new ArrayList<ServiceArea>();
		String sql = "SELECT a.service_area_id, a.service_area_name, b.carrier_region_id,b.company_id,a.country_Id,d.country_Name,d.country_2_Cd "+
					" FROM service_area a inner join country d on a.country_id=d.country_id LEFT OUTER JOIN carrier_region b ON a.Service_Area_ID = b.service_area_id and b.company_id=? order by d.country_name,a.service_area_name";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ServiceArea> driverRawMapper = new ResultSetMapper<ServiceArea>();
	        	List<ServiceArea> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceArea.class);
	        	List<ServiceArea> j_pojoList = new LinkedList<>();
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		for(ServiceArea srvc : pojoList) {
	        			log.info("services"+srvc.getStatus() + " " + srvc.getServiceAreaName() + " " + srvc.getCarrierRegionId());
		        		
	        			if(srvc.getCarrierRegionId()==null) {
	        				srvc.setStatus(Boolean.FALSE);
	        				srvc.setCarrierRegionId(-1);
	        			}else {
	        				srvc.setStatus(Boolean.TRUE);
	        			}
	        			j_pojoList.add(srvc);
	        		}
	        		return j_pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	
	private ServiceArea validateServiceArea(String serviceAreaName) {
		String sql = "SELECT service_area_id,service_area_name "
				+ "      FROM service_area where service_area_name=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, serviceAreaName);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ServiceArea> driverRawMapper = new ResultSetMapper<ServiceArea>();
	        	List<ServiceArea> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceArea.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private Service validateService(String serviceName) {
		String sql = "SELECT service_id,service_name "
				+ "      FROM service where service_name=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, serviceName);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Service> driverRawMapper = new ResultSetMapper<Service>();
	        	List<Service> pojoList = driverRawMapper.mapResultSetToObject(rs, Service.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private CarrierRegion validateCarrierRegion(Integer companyId,Integer serviceAreaId) {
		String sql = "SELECT service_area_id,company_id "
				+ "      FROM carrier_region where company_id=? and service_Area_Id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			pstmt.setInt(2,serviceAreaId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CarrierRegion> driverRawMapper = new ResultSetMapper<CarrierRegion>();
	        	List<CarrierRegion> pojoList = driverRawMapper.mapResultSetToObject(rs, CarrierRegion.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}else {
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private CompanyService validateCompanyService(Integer companyId,Integer serviceId) {
		String sql = "SELECT service_id,company_id "
				+ "      FROM company_service where company_id=? and service_Id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			pstmt.setInt(2,serviceId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyService> driverRawMapper = new ResultSetMapper<CompanyService>();
	        	List<CompanyService> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyService.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}else {
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	
	private ServiceArea getServiceAreaById(Integer id) {
		List<ServiceArea> serviceList = new ArrayList<ServiceArea>();
		String sql = "SELECT service_area_id,service_area_name "
				+ "      FROM service_area where service_area_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, id);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ServiceArea> driverRawMapper = new ResultSetMapper<ServiceArea>();
	        	List<ServiceArea> pojoList = driverRawMapper.mapResultSetToObject(rs, ServiceArea.class);
	        	if (!pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	
	
	@GET
	@Path("{languages}") // // http://localhost:8080/market-api/users/1234
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })

	public List<Language> getLanguages(@PathParam("languages") String language) {
		List<Language> langList = new ArrayList<Language>();
		try {
			String sql = "SELECT language_id,language_name FROM `language`";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Language> driverRawMapper = new ResultSetMapper<Language>();
		        	List<Language> pojoList = driverRawMapper.mapResultSetToObject(rs, Language.class);
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		for(Language l : pojoList) {
		        			l.setStatus(Boolean.FALSE);
		        			langList.add(l);
		        		}
		        		return langList;
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}

		return null;
	}
	
	private List<CarrierRegion> getCarrierRegionByCompanyId(Integer companyId) {
		List<CarrierRegion> region = new ArrayList<CarrierRegion>();
		String sql = "SELECT carrier_region_id,company_id,service_area_id,create_user_id "
				+ "      FROM carrier_region where company_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CarrierRegion> driverRawMapper = new ResultSetMapper<CarrierRegion>();
	        	List<CarrierRegion> pojoList = driverRawMapper.mapResultSetToObject(rs, CarrierRegion.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	private CarrierRegion getCarrierRegionByServiceAreaAndCompany(Integer companyId,Integer serviceAreaId) {
		List<CarrierRegion> region = new ArrayList<CarrierRegion>();
		String sql = "SELECT carrier_region_id,company_id,service_area_id,create_user_id "
				+ "      FROM carrier_region where company_id=? and service_area_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			pstmt.setInt(2, serviceAreaId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CarrierRegion> driverRawMapper = new ResultSetMapper<CarrierRegion>();
	        	List<CarrierRegion> pojoList = driverRawMapper.mapResultSetToObject(rs, CarrierRegion.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	private CompanyService getCompanyServiceByServiceAndCompany(Integer companyId,Integer serviceId) {
		List<CompanyService> cs = new ArrayList<CompanyService>();
		String sql = "SELECT company_service_id,company_id,service_id,create_user_id "
				+ "      FROM company_service where company_id=? and service_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			pstmt.setInt(2, serviceId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyService> driverRawMapper = new ResultSetMapper<CompanyService>();
	        	List<CompanyService> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyService.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<CompanyService> getCompanyServiceByCompanyId(Integer companyId) {
		List<CompanyService> service = new ArrayList<CompanyService>();
		String sql = "SELECT company_service_id,company_id,service_id,create_user_id,rating_value "
				+ "      FROM company_service where company_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, companyId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyService> driverRawMapper = new ResultSetMapper<CompanyService>();
	        	List<CompanyService> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyService.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	private CompanyUser insertCompanyUser(CompanyUser cu) {
		
			User u = cu.getUser();
			u=insertNewUser(u);
			List<UserLanguage> j_uList = new ArrayList<UserLanguage>();
			List<UserLanguage> uList =u.getUserLanguages();
			if(uList!=null && uList.size()>0){
			for(UserLanguage f_usrLang : uList) {
				f_usrLang.setUserId(u.getUserId());
				f_usrLang=insertUserLanguage(f_usrLang);
				j_uList.add(f_usrLang);
			}
			}
			cu.setUserId(u.getUserId());
			cu=insertNewCompanyUser(cu);
		//	log.error("test password for "+u.getFirstName() + " "+u.getPassword());
			return cu;
			
		}
	@POST // http://localhost:8080/market-api/trucker
	@Path("insertcompanyuser") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public User insertCompanyUserModel(User cu) {
		
		User u=insertNewUser(cu);
		
		List<Language> j_uList = new ArrayList<Language>();
		List<Language> uList =u.getLanguages();
		if(uList!=null && uList.size()>0){
			for(Language f_usrLang : uList) {
				if(f_usrLang.getStatus()== Boolean.TRUE) {
				f_usrLang.setUserId(u.getUserId());
				f_usrLang.setUpdateUserId(u.getUserId());
				f_usrLang=insertLanguage(f_usrLang);
				j_uList.add(f_usrLang);
				}
			}
		}

		cu=insertNewCompanyUser2(cu);
		updateWizardSetNumber(cu.getCompanyId(), 3);
	//	log.error("test password for "+u.getFirstName() + " "+u.getPassword());
		emailNewUser(cu);
		cu.setPassword("");
		return cu;
		
	}
	
	
	@POST // http://localhost:8080/market-api/trucker
	@Path("deletephoto") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Photo deletePhoto(Photo p) {
	
		if(p.getRelativePath()!=null) {
		 String sql = " delete from file_document_type  where company_file_id in (select b.company_file_id from company_file b where company_id=? and relative_path=?)";

		 try (

					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

			{
				pstmt.setInt(1,p.getCompanyId());
				pstmt.setString(2,p.getRelativePath());
				
				int id = pstmt.executeUpdate();
				return deleteCompanyFile(p);
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		}
		return null;
		
	}
	private Photo deleteCompanyFile(Photo p) {
		
		 String sql = " delete from company_file  where company_id=? and relative_path=?";

		 try (

					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

			{
				pstmt.setInt(1,p.getCompanyId());
				pstmt.setString(2,p.getRelativePath());
				
				int id = pstmt.executeUpdate();
				return p;
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
			
		return null;
		
	}
	private void emailNewUser(User cu) {
		/*
		String html="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\n" +
		"<html>\n"+
		"<head>\n"+
		"</head>\n"+
		"<body bgcolor=\"#ffffff\">\n"+
		"<h1 align=\"center\">&nbsp;</h1>\n"+

		"<h3>&nbsp;Registration Successful.</h3>\n"+

		"<h5>This message is an automated reply to your registration request.</h5>\n"+

		"<h5>You can now access the marketplace using the login information below:</h5>\n"+

		"<h4>User ID : "+cu.getEmailAddress()+"</h4>\n"+
		"<h4>Temporary Password : "+cu.getPassword()+"</h4>\n"+
		"<h2>&nbsp;</h2>\n"+

		"<h5>You will need your Temporary Password above to login for the first time. Please, change your password upon initial login.</h5>\n"+
		"</body>\n"+
		"</html>";
		*/
		String html="Registration successful\n"+
					"This message is automated reply to your registration request.\n"+
					"You can access the OpenTM using the login information below:\n"+
					"USER ID :"+ cu.getEmailAddress() + "\n"+
					"Temporary Password:"+cu.getPassword() +"\n"+
					"You will need your Temporary Password above to login for the first time. Please, change your password upon initial login.";
		
		Notify.sendEmail(cu.getEmailAddress(), "OpenTM Registration", html);
	}
	private User insertNewCompanyUser2(User compUsr) {
		String sql = "insert into company_user "
				+ " (company_id,user_id,is_primary_contact,is_technical_contact,create_user_id,create_dt,update_user_id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,compUsr.getCompanyId());
			pstmt.setInt(2,compUsr.getUserId());
			pstmt.setBoolean(3, compUsr.getIsPrimaryContact());
			pstmt.setBoolean(4, compUsr.getIsTechnicalContact());
			pstmt.setInt(5,compUsr.getCreateUserId());
			pstmt.setTimestamp(6,getCurrentTimeStamp());
			pstmt.setInt(7,compUsr.getCreateUserId());
			pstmt.setTimestamp(8,getCurrentTimeStamp());
	
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				compUsr.setCompanyUserId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return compUsr;
	}


	@POST // http://localhost:8080/market-api/trucker
	@Path("updatecompanyuser") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public User updateCompanyUserModel(User cu) {
		
		cu=setUpdateUser(cu);
		deleteUserLangaugeByUserId(cu.getUserId());
		List<Language> j_uList = new ArrayList<Language>();
		List<Language> uList =cu.getLanguages();
		if(uList!=null && uList.size()>0){
		for(Language l : uList) {
			if(l.getStatus()==Boolean.TRUE) {
			l.setUserId(cu.getUserId());
			l.setUpdateUserId(cu.getUserId());
			insertLanguage(l);
			j_uList.add(l);
			}
		}
		}
		setUpdateCompanyUser_v2(cu);
		updateWizardSetNumber(cu.getCompanyId(), 3);
		return cu;
		
	}
	@GET
	@Path("validateuseremail/{emailAddress}") // //
									// http://localhost:8080/market-api/users/1234
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public User validateUserEmail(@PathParam("emailAddress") String emailAddress) {
		return validateUserEmailAddress(emailAddress);

	}
	
	@GET
	@Path("validateuserphone/{phoneNbr}") // //
									// http://localhost:8080/market-api/users/1234
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public User validateUserPhone(@PathParam("phoneNbr") String phoneNbr) {
		return validateUserPhoneNbr(phoneNbr);

	}
	
	@GET
	@Path("clients/{companyId}") // //
									// http://localhost:8080/market-api/users/1234
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyClient> getClients(@PathParam("companyId") Integer companyId) {
		return getClientsByCompanyId(companyId);

	}

	/**
	 * @param fileId
	 * @return
	 */
	private List<FileDocumentType> getFileDocumentTypeById(Integer fileId) {
			List<FileDocumentType> list = new ArrayList<FileDocumentType>();
		try {
			String sql = "SELECT file_document_type_id,company_file_id,document_type_id FROM `file_document_type` where file_document_type_id=?";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, fileId);
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.FileDocumentType fdt = new com.openport.marketplace.model.FileDocumentType(
								rs);

						FileDocumentType j_fdt = new FileDocumentType();
						j_fdt.setFileDocumentTypeId(fdt.getFileDocumentTypeId());
						j_fdt.setCompanyFile(getCompanyFileById(fdt.getCompanyFileId()));
						j_fdt.setDocumentType(getDocumentTypeById(fdt.getDocumentTypeId()));

						list.add(j_fdt);

					}
					// return ccList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return list;
	}

	/**
	 * @param companyId
	 * @return
	 */
	private List<FileDocumentType> getFileDocumentTypeByCompanyId(Integer companyId) {
			List<FileDocumentType> list = new ArrayList<FileDocumentType>();
		try {
			String sql = "SELECT file_document_type_id,company_file_id,document_type_id FROM `file_document_type` where company_file_id in (select company_file_id from company_file where company_id=?)";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyId);
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.FileDocumentType fdt = new com.openport.marketplace.model.FileDocumentType(
								rs);

						FileDocumentType j_fdt = new FileDocumentType();
						j_fdt.setFileDocumentTypeId(fdt.getFileDocumentTypeId());
						j_fdt.setCompanyFile(getCompanyFileById(fdt.getCompanyFileId()));
						j_fdt.setDocumentType(getDocumentTypeById(fdt.getDocumentTypeId()));

						list.add(j_fdt);

					}
					// return ccList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return list;
	}

	/**
	 * @param companyId
	 * @return
	 */
	private List<FileDocumentType> getFileDocumentTypePhotos(Integer companyId) {
		List<FileDocumentType> list = new ArrayList<FileDocumentType>();
		try {
			String sql = "SELECT file_document_type_id,company_file_id,document_type_id FROM `file_document_type` where company_file_id in (select company_file_id from company_file where company_id=? and is_legal_document = 0)";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyId);
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.FileDocumentType fdt = new com.openport.marketplace.model.FileDocumentType(
								rs);

						FileDocumentType j_fdt = new FileDocumentType();
						j_fdt.setFileDocumentTypeId(fdt.getFileDocumentTypeId());
						j_fdt.setCompanyFile(getCompanyFileById(fdt.getCompanyFileId()));
						j_fdt.setDocumentType(getDocumentTypeById(fdt.getDocumentTypeId()));

						list.add(j_fdt);

					}
					// return ccList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
		}
		return list;
	}
	
	/**
	 * @param companyId
	 * @return
	 */
	private List<FileDocumentType> getFileDocumentTypeLegalDocument(Integer companyId) {
		List<FileDocumentType> list = new ArrayList<FileDocumentType>();
		try {
			String sql = "SELECT file_document_type_id,company_file_id,document_type_id FROM `file_document_type` where  company_file_id in (select company_file_id from company_file where company_id=? and is_legal_document=1)";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyId);
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.FileDocumentType fdt = new com.openport.marketplace.model.FileDocumentType(
								rs);

						FileDocumentType j_fdt = new FileDocumentType();
						j_fdt.setFileDocumentTypeId(fdt.getFileDocumentTypeId());
						j_fdt.setCompanyFile(getCompanyFileById(fdt.getCompanyFileId()));
						j_fdt.setDocumentType(getDocumentTypeById(fdt.getDocumentTypeId()));

						list.add(j_fdt);

					}
					// return ccList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return list;
	}


	
	private List<CompanyClient> getClientsByCompanyId(Integer companyId) {
		List<CompanyClient> ccList = new ArrayList<CompanyClient>();
		try {
			String sql = "SELECT * FROM `company_client` where company_id=?";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyId);
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.CompanyClient j_cc = new com.openport.marketplace.model.CompanyClient(
								rs);
						CompanyClient cc = new CompanyClient();
						cc.setCompanyClientId(j_cc.getCompanyClientId());
						cc.setClientName(j_cc.getClientName());
						ccList.add(cc);
					}
					// return ccList;
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
		}
		return ccList;
	}

	@POST // http://localhost:8080/market-api/truckers
	@Path("equipment") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public CompanyEquipment createCompanyEquipment(CompanyEquipment equipment) {
		CompanyEquipment ce=null;
		try {
			ce= insertNewCompanyEquipment(equipment, equipment.getCompanyId());
			updateWizardSetNumber(equipment.getCompanyId(), 5);

			return ce;
	} catch (Exception ee) {
		log.error("Error message: " ,ee);
		new ServiceException(Response.Status.CONFLICT);
	}
	return null;
	}	
	
	
	
	
		
	
	
	@POST // http://localhost:8080/market-api/truckers
	@Path("profile") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Company newProfile(Company company) {
		UPDATE_ID=company.getUpdateUserId();
		List<CompanyClient> clientList = new LinkedList<>();
		List<FileDocumentType> fileDocTypeList = new LinkedList<>();
		List<CompanyUser> cList = new LinkedList<>();
		List<CompanyService> csList = new LinkedList<>();
		List<CarrierRegion> crList = new LinkedList<>();
		List<CompanyEquipment> ceList = new LinkedList<>();
		
		List<CompanyClient> compClients = company.getClients();
		List<FileDocumentType> fileDocType = company.getFileDocTypes();
		List<CompanyUser> companyUser = company.getCompanyUsers();
		List<CompanyService> companyServices = company.getCompanyServices();
		List<CarrierRegion> carrierRegions = company.getCarrierRegions();
		List<CompanyEquipment> companyEquipments = company.getCompanyEquipments();
		
		
		
		try {
			if (company.getCompanyId() == null || company.getCompanyId() == -1) {
				company = insertNewCompany(company);
			} else {
				updateProfileCompany(company);
			}
			/* Client Setup */

			if (compClients != null && compClients.size() > 0) {
				for (CompanyClient cc : compClients) {
					if (cc.getCompanyClientId() == -1) {
						clientList = insertCompanyClient(company, clientList, cc);
					} else {
						clientList = updateCompanyClient(company, clientList, cc);
					}
				} // for Clients
			}

			/* Company File */

			if (fileDocType != null && fileDocType.size() > 0) {
				for (FileDocumentType fdt : fileDocType) {
					CompanyFile cf = fdt.getCompanyFile();
					DocumentType dt = fdt.getDocumentType();
					if (cf.getCompanyFileId() == -1) {
						cf = insertCompanyFile(company, cf);
					} else {
						cf = updateCompanyFile(company, cf);
					}

					if (fdt.getFileDocumentTypeId() == null || fdt.getFileDocumentTypeId() == -1) {
						// new FileDocumentType
						fileDocTypeList.add(insertFileDocumentType(fdt, dt, cf));

					} else {
						fdt.setCompanyFile(cf);
						fdt.setDocumentType(dt);
						fileDocTypeList.add(fdt);
					}
				} 
			}
		
			//Company User	
			if(companyUser!=null && companyUser.size()>0) {
				for(CompanyUser cu : companyUser) {
					log.info(cu.toString());
					if(cu.getCompanyUserId()==-1) {
						
						cList.add(insertCompanyUser(cu));
					}
				}
			}
			
			//Company Service
			if(companyServices!=null && companyServices.size()>0) {
				for(CompanyService ccs : companyServices) {
					if(ccs.getCompanyServiceId()==-1) {
						csList.add(insertNewCompanyService(ccs,company.getCompanyId()));
					}else if(ccs.getServiceId()==-1) {//delete
						log.info("Delete company Services");
						setDeleteCompanyService(ccs);
					}else if(ccs.getService()!=null){
						csList.add(updateCompanyService(ccs,company.getCompanyId()));
					}
				}
			}
			
			

			//Carrier Region
			if(carrierRegions!=null && carrierRegions.size()>0) {
				for(CarrierRegion crs : carrierRegions) {
					if(crs.getCarrierRegionId()==-1) {
						crList.add(insertNewCarrierRegion(crs,company.getCompanyId()));
					}else if(crs.getServiceAreaId()==-1){
						setDeleteCarrierRegion(crs);
					}else if(crs.getServiceArea()!=null) {
						crList.add(updateCarrierRegion(crs,company.getCompanyId()));
					}
				}
			}
			
			
			//Company Equipment
			if(companyEquipments!=null && companyEquipments.size()>0) {
				for(CompanyEquipment ce : companyEquipments) {
					if(ce.getCompanyEquipmentId()==-1) {
						ceList.add(insertNewCompanyEquipment(ce,company.getCompanyId()));
					}else if(ce.getEquipmentId()==-1){
						setDeleteEquipment(ce.getEquipment().getEquipmentId());
						setDeleteCompanyEquipment(ce.getCompanyEquipmentId());
						
					}else if(ce.getEquipment()!=null) {
						setUpdateEquipment(ce.getEquipment());
						setUpdateCompanyEquipment(ce, company.getCompanyId());
					}
				}
			}
			
			

		} catch (Exception ee) {
			log.error("Error message: " ,ee);
			new ServiceException(Response.Status.CONFLICT);
		}
		company.setClients(clientList);
		company.setFileDocTypes(fileDocTypeList);
		company.setCompanyUsers(cList);
		company.setCompanyServices(csList);
		company.setCarrierRegions(crList);
		company.setCompanyEquipments(ceList);

		
		
		return company;
	}
	
	
	@POST // http://localhost:8080/market-api/truckers
	@Path("companyprofile") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Company createCompanyProfile(Company company) {
		
		UPDATE_ID=company.getUpdateUserId();
		List<CompanyClient> clientList = new LinkedList<>();
		List<FileDocumentType> fileDocTypeList = new LinkedList<>();
		List<CompanyClient> compClients = company.getClients();
		List<FileDocumentType> fileDocType = company.getFileDocTypes();
		
		
		
		try {
			if (company.getCompanyId() == null || company.getCompanyId() == -1) {
				company = insertNewCompany(company);
			} else {
				updateProfileCompany(company);
			}
			/* Client Setup */

			if (compClients != null && compClients.size() > 0) {
				for (CompanyClient cc : compClients) {
					if (cc.getCompanyClientId() == -1) {
						clientList = insertCompanyClient(company, clientList, cc);
					} else {
						clientList = updateCompanyClient(company, clientList, cc);
					}
				} 
			}

			/* Company File */

			if (fileDocType != null && fileDocType.size() > 0) {
				for (FileDocumentType fdt : fileDocType) {
					CompanyFile cf = fdt.getCompanyFile();
					DocumentType dt = fdt.getDocumentType();
					if (cf.getCompanyFileId() == -1) {
						log.info("Insert..."+cf.getRelativePath());
						log.info("Insert legal document..."+cf.getIsLegalDocument());
						log.info("Insert legal document..."+cf.isLegalDocument());
						
						cf = insertCompanyFile(company, cf);
					} else {
						cf = updateCompanyFile(company, cf);
					}

					if (fdt.getFileDocumentTypeId() == null || fdt.getFileDocumentTypeId() == -1) {
						// new FileDocumentType
						log.info("insert new ...");
						fileDocTypeList.add(insertFileDocumentType(fdt, dt, cf));

					} else {
						fdt.setCompanyFile(cf);
						fdt.setDocumentType(dt);
						fileDocTypeList.add(fdt);
					}
				} 
			}
		

		} catch (Exception ee) {
			log.error("Error message: " ,ee);
			new ServiceException(Response.Status.CONFLICT);
		}
		company.setClients(clientList);
		company.setFileDocTypes(fileDocTypeList);
		if(company.getHasPod() !=null && company.getWizardStepNbr()> 6) {
			updateWizardSetNumber(company.getCompanyId(), company.getWizardStepNbr());
			
		}else if(company.getHasPod() !=null) {
			updateWizardSetNumber(company.getCompanyId(), 6);
		}else {
			updateWizardSetNumber(company.getCompanyId(), 0);
		}
		return company;
	}
	private int updateWizardSetNumber(Integer companyId, Integer currentTab) {
		String sql = "update company set wizard_step_nbr= "+currentTab
				+ " where company_id=? and "+ currentTab + " > wizard_step_nbr";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,companyId);
			int id = pstmt.executeUpdate();
			return id;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return 0;
	
		
	}
	private Equipment setUpdateEquipment(Equipment eq) {
		String sql = "update equipment set equipment_type_id=?,equipment_name=?,update_user_id=?,update_dt=? "
				+ " where equipment_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,eq.getEquipmentId());
			pstmt.setString(2,eq.getEquipmentName());
			pstmt.setInt(3,UPDATE_ID);
			pstmt.setTimestamp(4,getCurrentTimeStamp());
		
			pstmt.setInt(5,eq.getEquipmentId());
			int id = pstmt.executeUpdate();
			return eq;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}

	private CompanyEquipment setUpdateCompanyEquipment(CompanyEquipment ce,Integer companyId) {
		String sql = "update company_equipment set company_id=?,equipment_id=?,equipment_count=?,update_user_id=?,update_dt=? "
				+ " where company_equipment_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,companyId);
			pstmt.setInt(2,ce.getEquipmentId());
			pstmt.setDouble(3,ce.getEquipmentCount());
			pstmt.setInt(4,UPDATE_ID);
			pstmt.setTimestamp(5,getCurrentTimeStamp());
			pstmt.setInt(6,ce.getCompanyEquipmentId());
	
			int id = pstmt.executeUpdate();
			return ce;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}

	
	
	private CompanyService insertNewCompanyService(CompanyService cs,Integer companyId) {
		CompanyService j_cs=setNewCompanyService(cs, companyId);
		Service j_s = getServiceById(j_cs.getServiceId());
		j_cs.setService(j_s);
		return j_cs;
	}

	
	/** method of company equipment **/
	
	
	private Equipment insertNewEquipment(Equipment e) {
		
		String sql = "insert into equipment "
				+ " (equipment_type_id,equipment_name,create_user_id,create_dt,update_user_id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,e.getEquipmentTypeId());
			pstmt.setString(2,e.getEquipmentName());
			pstmt.setInt(3,e.getCreateUserId());
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,e.getCreateUserId());
			pstmt.setTimestamp(6,getCurrentTimeStamp());
	
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				e.setEquipmentId(rs.getInt(1));
			}
			return e;
		} catch (Exception ee) {
			log.error("Error message: " + ee, ee);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}

	private int setDeleteEquipment(Integer eId) {
		String sql = "delete FROM equipment  "
				+ " where equipment_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,eId);
				int id = pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return 0;
		
	}
	private int setDeleteCompanyEquipment(Integer eId) {
		String sql = "delete FROM company_equipment  "
				+ " where company_equipment_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,eId);
				int id = pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return 0;
		
	}
	
	public CompanyEquipment insertNewCompanyEquipment(CompanyEquipment ce,Integer companyId) {
		Equipment e = insertNewEquipment(ce.getEquipment());
		ce.setEquipmentId(e.getEquipmentId());
		ce.setEquipment(e);
		CompanyEquipment j_ce=setNewCompanyEquipment(ce, companyId);
		//Equipment j_s = getServiceAreaById(j_cs.getServiceAreaId());
		//j_cs.setServiceArea(j_s);
		return j_ce;
	}
	
	private CompanyEquipment setNewCompanyEquipment(CompanyEquipment ce, Integer companyId) {
		String sql = "insert into company_equipment "
				+ " (company_id,equipment_id,equipment_count,create_user_id,create_dt,update_user_id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,companyId);
			pstmt.setInt(2,ce.getEquipmentId());
			pstmt.setDouble(3,ce.getEquipmentCount());
			pstmt.setInt(4,ce.getCreateUserId());
			pstmt.setTimestamp(5,getCurrentTimeStamp());
			pstmt.setInt(6,ce.getCreateUserId());
			pstmt.setTimestamp(7,getCurrentTimeStamp());
	
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				ce.setCompanyEquipmentId(rs.getInt(1));
			}
			return ce;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	
	
	private CarrierRegion insertNewCarrierRegion(CarrierRegion cr,Integer companyId) {
		CarrierRegion j_cs=setNewCarrierRegion(cr, companyId);
		ServiceArea j_s = getServiceAreaById(j_cs.getServiceAreaId());
		j_cs.setServiceArea(j_s);
		return j_cs;
	}
	
	private CarrierRegion updateCarrierRegion(CarrierRegion cr,Integer companyId) {
		CarrierRegion j_cs=setUpdateCarrierRegion(cr, companyId);
		ServiceArea j_s = getServiceAreaById(j_cs.getServiceAreaId());
		j_cs.setServiceArea(j_s);
		return j_cs;
	}

	private CompanyService updateCompanyService(CompanyService cs,Integer companyId) {
		CompanyService j_cs=setUpdateCompanyService(cs, companyId);
		Service j_s = getServiceById(j_cs.getServiceId());
		j_cs.setService(j_s);
		return j_cs;
	}
	
	private int setDeleteCompanyService(CompanyService cs) {
		String sql = "delete FROM company_service  "
				+ " where company_service_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,cs.getCompanyServiceId());
				int id = pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return 0;
		
	}
	
	
	private int setDeleteCarrierRegion(CarrierRegion cs) {
		String sql = "delete FROM carrier_region  "
				+ " where carrier_region_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,cs.getCarrierRegionId());
				int id = pstmt.executeUpdate();
			return 1;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return 0;
		
	}

	
	private CompanyService setUpdateCompanyService(CompanyService cs, Integer companyId) {
		String sql = "update company_service set company_id=?,service_id=?,update_user_id=?,update_dt=? "
				+ " where company_service_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,companyId);
			pstmt.setInt(2,cs.getServiceId());
			pstmt.setInt(3,UPDATE_ID);
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,cs.getCompanyServiceId());
	
			int id = pstmt.executeUpdate();
			return cs;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}

	private CarrierRegion setUpdateCarrierRegion(CarrierRegion cr, Integer companyId) {
		String sql = "update carrier_region set company_id=?,service_area_id=?,update_user_id=?,update_dt=? "
				+ " where carrier_region_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,companyId);
			pstmt.setInt(2,cr.getServiceAreaId());
			pstmt.setInt(3,UPDATE_ID);
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,cr.getCarrierRegionId());
	
			int id = pstmt.executeUpdate();
			return cr;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	

	
	private ServiceArea setNewServiceArea(ServiceArea srvc) {
		String sql = "insert into service_area (service_area_name,create_user_id,create_dt,update_user_id,update_dt,country_id) "
				+" VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{
				pstmt.setString(1,srvc.getServiceAreaName());
				pstmt.setInt(2,srvc.getCreateUserId());
				pstmt.setTimestamp(3,getCurrentTimeStamp());
				pstmt.setInt(4,srvc.getCreateUserId());
				pstmt.setTimestamp(5,getCurrentTimeStamp());
				pstmt.setInt(6,srvc.getCountryId());
				
				int id = pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs != null && rs.next()) {
					srvc.setServiceAreaId(rs.getInt(1));
				}
		
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return srvc;
		
	}
	

	
	private Service setNewService(Service srvc) {
		String sql = "insert into service (service_name,create_user_id,create_dt,update_user_id,update_dt,is_transportation) "
				+" VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
		{
				pstmt.setString(1,srvc.getServiceName());
				pstmt.setInt(2,srvc.getCreateUserId());
				pstmt.setTimestamp(3,getCurrentTimeStamp());
				pstmt.setInt(4,srvc.getCreateUserId());
				pstmt.setTimestamp(5,getCurrentTimeStamp());
				
				pstmt.setBoolean(6, srvc.getIsTransportation());
				
		
				int id = pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs != null && rs.next()) {
					srvc.setServiceId(rs.getInt(1));
				}
		
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return srvc;
		
	}
	
	private CarrierRegion setNewCarrierRegion(CarrierRegion cr, Integer companyId) {
		log.info("Company ID " +companyId);
		String sql = "insert into carrier_region "
				+ " (company_id,service_area_id,create_user_id,create_dt,update_user_id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,companyId);
			pstmt.setInt(2,cr.getServiceAreaId());
			pstmt.setInt(3,cr.getCreateUserId());
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,cr.getCreateUserId());
			pstmt.setTimestamp(6,getCurrentTimeStamp());
	
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				cr.setCarrierRegionId(rs.getInt(1));
				log.info("Carrier Info"+cr.getCarrierRegionId());
			}
			
			return cr;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	
	
	private CompanyService setNewCompanyService(CompanyService cs, Integer companyId) {
		String sql = "insert into company_service "
				+ " (company_id,service_id,create_user_id,create_dt,update_user_id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,companyId);
			pstmt.setInt(2,cs.getServiceId());
			pstmt.setInt(3,cs.getCreateUserId());
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,cs.getCreateUserId());
			pstmt.setTimestamp(6,getCurrentTimeStamp());
	
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				cs.setCompanyServiceId(rs.getInt(1));
			}
			return cs;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	
	
	private CompanyUser insertNewCompanyUser(CompanyUser compUsr) {
		String sql = "insert into company_user "
				+ " (company_id,user_id,is_primary_contact,is_technical_contact,create_user_id,create_dt,update_user_id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,compUsr.getCompanyId());
			pstmt.setInt(2,compUsr.getUserId());
			pstmt.setBoolean(3, compUsr.getIsPrimaryContact());
			pstmt.setBoolean(4, compUsr.getIsTechnicalContact());
			pstmt.setInt(5,compUsr.getCreateUserId());
			pstmt.setTimestamp(6,getCurrentTimeStamp());
			pstmt.setInt(7,compUsr.getCreateUserId());
			pstmt.setTimestamp(8,getCurrentTimeStamp());
	
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				compUsr.setCompanyUserId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return compUsr;
	}
	
	private Service insertService(Service service) {
		String sql = "insert into service "
				+ " (service_name,is_transportation,create_user_id,create_dt,update_user_id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setString(1,service.getServiceName());
			pstmt.setBoolean(2,service.isTransportation());
			pstmt.setInt(3,service.getCreateUserId());
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,service.getCreateUserId());
			pstmt.setTimestamp(6,getCurrentTimeStamp());
		
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				service.setServiceId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return service;
	}
	private User validateUserEmailAddress(String emailAddress) {
		try {
			String sql="select email_address from user where email_address=?";
			log.info("SQL " + sql);
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					pstmt.setString(1, emailAddress);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
		        	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
		        	
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		log.info("RESULT " + pojoList.get(0));
		        		return pojoList.get(0);
		        	}
		        	
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	private User validateUserPhoneNbr(String phoneNbr) {
		try {
			String sql="select phone_nbr from user where phone_nbr=?";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
					pstmt.setString(1, phoneNbr);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
		        	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
		        	
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		return pojoList.get(0);
		        	}
		        	
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;
	}
	
	
	private User insertNewUser(User usr) {
		String sql = "insert into user "
				+ " (email_address,first_name,last_name,password_hash,phone_nbr,primary_user_role_id,is_active,is_internal,create_user_id,create_dt,update_user_id,update_Dt,is_password_weak "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?,?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			usr.setPassword(TemporaryPassword.generate());
			String passwordHash = MySqlPassword.passwordHash(usr.getPassword());
			pstmt.setString(1,usr.getEmailAddress());
			pstmt.setString(2,usr.getFirstName());
			pstmt.setString(3, usr.getLastName());
			pstmt.setString(4, passwordHash);
			pstmt.setString(5, usr.getPhoneNumber());
			pstmt.setInt(6, 1);
			pstmt.setBoolean(7, true);
			pstmt.setBoolean(8, true);
			pstmt.setInt(9,usr.getUserId());
			pstmt.setTimestamp(10,getCurrentTimeStamp());
			pstmt.setInt(11,usr.getUserId());
			pstmt.setTimestamp(12,getCurrentTimeStamp());
			pstmt.setBoolean(13, true);
	

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				usr.setUserId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return usr;
	}
	
	private UserLanguage insertUserLanguage(UserLanguage usrLang) {
		String sql = "insert into user_language "
				+ " (user_id,language_id,create_user_id,create_Dt,update_User_Id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,usrLang.getUserId());
			pstmt.setInt(2,usrLang.getLanguageId());
			pstmt.setInt(3, usrLang.getCreateUserId());
			pstmt.setTimestamp(4, getCurrentTimeStamp());
			pstmt.setInt(5, usrLang.getCreateUserId());
			pstmt.setTimestamp(6, getCurrentTimeStamp());
			
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				usrLang.setUserLanguageId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);;
			new ServiceException(Response.Status.CONFLICT);
		}
		return usrLang;
	}
	
	private Language insertLanguage(Language lang) {
		String sql = "insert into user_language "
				+ " (user_id,language_id,create_user_id,create_Dt,update_User_Id,update_Dt "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1,lang.getUserId());
			pstmt.setInt(2,lang.getLanguageId());
			pstmt.setInt(3, lang.getUserId());
			pstmt.setTimestamp(4, getCurrentTimeStamp());
			pstmt.setInt(5, lang.getUpdateUserId());
			pstmt.setTimestamp(6, getCurrentTimeStamp());
			
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				lang.setUserLanguageId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);;
			new ServiceException(Response.Status.CONFLICT);
		}
		return lang;
	}
	
	private Company insertNewCompany(Company company) {
		String sql = "insert into company "
				+ " (company_Name,company_code,create_User_Id,create_Dt,update_User_Id,update_Dt,years_of_operation  "
				+ " corporate_address_id,truck_pool_address_id,branch_address_id,Wizard_Step_Nbr,is_shipper, is_carrier, hasPod,business_headline,business_description,timezone_id "
				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			company.setCreateDt(getCurrentTimeStamp());
			company.setUpdateDt(getCurrentTimeStamp());
			company.setCreateUserId(11);
			company.setUpdateUserId(11);
			company.setWizardStepNbr(company.getWizardStepNbr());
			company.isCarrier(true);
			pstmt.setString(1, company.getCompanyName());
			pstmt.setString(2, trimString(company.getCompanyCode()));
			pstmt.setInt(3, company.getCreateUserId());
			pstmt.setTimestamp(4, company.getCreateDt());
			pstmt.setInt(5, company.getUpdateUserId());
			pstmt.setTimestamp(6, company.getUpdateDt());
			pstmt.setInt(7, company.getYearsOfOperation());
			pstmt.setInt(8, -1);
			pstmt.setInt(9, -1);
			pstmt.setInt(10, -1);
			pstmt.setInt(11, company.getWizardStepNbr());
			pstmt.setInt(12, 0);
			pstmt.setInt(13, 1);
			if(company.getHasPod()!=null && company.getHasPod()==Boolean.TRUE) {
				pstmt.setInt(14, 1);
			}else {
				pstmt.setInt(14, 0);
			}
			pstmt.setString(15, company.getBusinessHeadline());
			pstmt.setString(16, company.getBusinessDescription());
			pstmt.setString(17, company.getTimezoneId());
			// pstmt.setString(14,
			// trimString(company.getBusinessDescription()));
			// pstmt.setString(15,
			// trimString(company.getBusinessHeadline()));

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				company.setCompanyId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return company;
	}

	private Company updateProfileCompany(Company company) {
		int wizardStepNbr = 0;
		String sql = "UPDATE `company` SET " + " update_User_Id = ? " + " ,update_Dt = ? " + " ,Wizard_Step_Nbr = ? "
				+ " ,years_of_operation = ? " + " ,company_name = ? " + " ,company_code = ?,corporate_address_id=?,has_pod=?,business_headline=?,business_description=?,timezone_id=?"
				+ " WHERE  company_id = ? ";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			company.setUpdateDt(getCurrentTimeStamp());
					pstmt.setInt(1, company.getUpdateUserId());
			pstmt.setTimestamp(2, company.getUpdateDt());
			pstmt.setInt(3, company.getWizardStepNbr());
			pstmt.setInt(4, company.getYearsOfOperation());
			pstmt.setString(5, company.getCompanyName());
			pstmt.setString(6, company.getCompanyCode());
			pstmt.setInt(7, company.getCorporateAddressId());
			if(company.getHasPod()!=null && company.getHasPod()==Boolean.TRUE) {
				pstmt.setInt(8, 1);
			}else {
				pstmt.setInt(8, 0);
			}
		
			pstmt.setString(9, company.getBusinessHeadline());
			pstmt.setString(10, company.getBusinessDescription());
			pstmt.setString(11, company.getTimezoneId());
			
			
			pstmt.setInt(12, company.getCompanyId());
		

			// pstmt.setString(14,
			// trimString(company.getBusinessDescription()));
			// pstmt.setString(15,
			// trimString(company.getBusinessHeadline()));

			int id = pstmt.executeUpdate();

		} catch (Exception e) {
			log.error("Error message: " + sql, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return company;
	}

	/**
	 * @param id
	 * @param DbConfig.getDataSource()
	 * @return
	 */
	private DocumentType getDocumentTypeById(Integer id) {
		String sql = "SELECT document_type_id,description_txt" + "      FROM document_type "
				+ "     WHERE document_type_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<DocumentType> driverRawMapper = new ResultSetMapper<DocumentType>();
				List<DocumentType> pojoList = driverRawMapper.mapResultSetToObject(rs, DocumentType.class);
				if (pojoList!=null && !pojoList.isEmpty()) {
					return pojoList.get(0);
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	/**
	 * @param id
	 * @param DbConfig.getDataSource()
	 * @return
	 */
	private User getUser(Integer id) {
		
		String sql = "SELECT user_ID,email_address,first_name,last_name,phone_nbr,primary_user_role_id,is_Test,is_Active,is_Internal,create_user_id,create_dt,update_dt " // password_hash,
				+ "      FROM user " 
				+ "     WHERE user_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
	        	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	
	private Language getLanguage(Integer id) {
		String sql = "SELECT language_id,language_name"
				+ "      FROM language " + "     WHERE Language_ID =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					com.openport.marketplace.model.Language lang = new com.openport.marketplace.model.Language(rs);
					Language l = new Language();
					l.setLanguageName(lang.getLanguageName());
					l.setLanguageId(lang.getLanguageId());
					return l;
				
				}
			}
			

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
		
	}
	private List<UserLanguage> getUserLanguage(Integer id) {
		String sql = "SELECT user_language_id,language_id"
				+ "      FROM user_language " + "     WHERE user_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);
			try (ResultSet rs = pstmt.executeQuery()) {
				List<UserLanguage> userLanguages = new ArrayList<UserLanguage>();
				while (rs.next()) {
					com.openport.marketplace.model.UserLanguage lang = new com.openport.marketplace.model.UserLanguage(rs);
					UserLanguage ul = new UserLanguage();
					ul.setUserLanguageId(lang.getUserLanguageId());
					ul.setLanguage(getLanguage(lang.getLanguageId()));
					userLanguages.add(ul);
					
				
				}
				return userLanguages;
			}
			

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	
	private List<Language> getUserLanguages(Integer userId) {
		String sql = " SELECT b.language_name,b.language_id,a.user_language_id,a.user_id  FROM user_language a inner join language b on a.language_id=b.language_id and a.user_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, userId);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<Language> driverRawMapper = new ResultSetMapper<Language>();
		        	List<Language> pojoList = driverRawMapper.mapResultSetToObject(rs, Language.class);
		        	List<Language> j_pojoList = new LinkedList<>();
			        
		        	if (pojoList!=null && !pojoList.isEmpty()) {
		        		for(Language l : pojoList) {
		        			l.setStatus(Boolean.TRUE);
		        			j_pojoList.add(l);
		        		}
		        		return j_pojoList;
					}
				}
				
			
			

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	
	private Integer getProvince(String provinceName,Integer countryId,Integer createUserId) {
		// check if country already exists
		int provinceId=-1;
		String sql="";
		sql = "SELECT * FROM `province` WHERE province_name = ? and country_id = ? ;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			pstmt.setString(1, provinceName);
			pstmt.setInt(2, countryId);
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					com.openport.marketplace.model.Province prvnc = new com.openport.marketplace.model.Province(rs);
					provinceId = prvnc.getProvinceId();
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		// if not exists, then insert
		if (provinceId == -1) {

			sql = "insert into province "
					+ " (province_Name,country_Id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
					+ " (?,?,?,?,?,?);";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

				pstmt.setString(1, provinceName);
				pstmt.setInt(2, countryId);
				pstmt.setInt(3, createUserId);
				pstmt.setTimestamp(4, getCurrentTimeStamp());
				pstmt.setInt(5, createUserId);
				pstmt.setTimestamp(6, getCurrentTimeStamp());

				int id = pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs != null && rs.next()) {
					provinceId = rs.getInt(1);
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} else {
			// log.info("provinceId exists:" + provinceId);
		}
		return provinceId;

	}
	private List<CompanyUser> getCompanyUser(Integer id) {
		List<CompanyUser> n_pojoList = new ArrayList<CompanyUser>();
		
		String sql = "SELECT company_user_id,company_id,user_id,is_primary_contact,is_technical_contact,create_dt,update_user_id,update_dt "
				+ "      FROM company_user " + "     WHERE company_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyUser> driverRawMapper = new ResultSetMapper<CompanyUser>();
				List<CompanyUser> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyUser.class);
				
				if (pojoList!=null && !pojoList.isEmpty()) {
					for(CompanyUser c : pojoList) {
						User cUser = getUser(c.getUserId());
						
						if(cUser!=null) {
							c.setUser(cUser);
						List<UserLanguage> uList = getUserLanguage(cUser.getUserId());
						if(!uList.isEmpty()) {
							cUser.setUserLanguages(uList);
							
						}
						n_pojoList.add(c);
						}
					}
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return n_pojoList;
	}
	
	
	private List<User> getCompanyUsers(Integer id) {
		List<User> n_pojoList = new ArrayList<User>();
		
		String sql = "select a.user_id,a.email_address,a.first_name,a.last_name,a.phone_nbr,a.create_user_id, a.create_dt,a.update_user_id,a.update_dt,"+
					 "b.company_user_id,b.company_id,b.is_primary_contact,b.is_technical_contact from "+
					 " user a inner join company_user b on a.user_id=b.user_id and b.company_id=?"; 
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
				List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
				if (pojoList!=null && !pojoList.isEmpty()) {
					for(User c : pojoList) {
						log.info("Primary"+c.getUserId());
						List<Language> uList = getUserLanguages(c.getUserId());
						if(uList!=null && !uList.isEmpty()) {
							c.setLanguages(uList);
						}
						n_pojoList.add(c);
					}
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return n_pojoList;
	}
	
	private Integer validateUserId(String userId) {
		List<User> n_pojoList = new ArrayList<User>();
		
		String sql = "select a.email_address from user where email_address=?"; 
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, userId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
				List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
				if (pojoList!=null && !pojoList.isEmpty()) {
						return 1; 
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return 0;
	}
	
	
	
	
	/**
	 * @param id
	 * @param DbConfig.getDataSource()
	 * @return
	 */
	private CompanyFile getCompanyFileById(Integer id) {
		String sql = "SELECT company_file_id,company_id,is_legal_document,document_name,relative_path"
				+ "      FROM company_file " + "     WHERE company_file_id =  ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyFile> driverRawMapper = new ResultSetMapper<CompanyFile>();
				List<CompanyFile> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyFile.class);
				if (!pojoList.isEmpty()) {
					return pojoList.get(0);
				}

			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return null;
	}

	/**
	 * @param company
	 * @param clientList
	 * @param DbConfig.getDataSource()
	 * @param dt
	 *            Insert Company Client
	 */
	private List<CompanyClient> insertCompanyClient(Company company, List<CompanyClient> clientList,
			CompanyClient dt) {
		String sql = "insert into company_client "
				+ " (company_ID,client_name,create_User_Id,create_Dt,update_User_Id,update_Dt "

				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			if (company.getCreateDt() == null) {
				company.setCreateDt(getCurrentTimeStamp());
			}
			pstmt.setInt(1, company.getCompanyId());
			pstmt.setString(2, trimString(dt.getClientName()));
			pstmt.setInt(3, company.getCreateUserId());
			pstmt.setTimestamp(4, company.getCreateDt());
			pstmt.setInt(5, company.getCreateUserId());
			pstmt.setTimestamp(6, getCurrentTimeStamp());
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				dt.setCompanyClientId(rs.getInt(1));
			}
			clientList.add(dt);

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return clientList;
	}

	/**
	 * @param company
	 * @param clientList
	 * @param DbConfig.getDataSource()
	 * @param dt
	 *            Insert Company Client
	 */
	private List<CompanyClient> updateCompanyClient(Company company, List<CompanyClient> clientList,
			CompanyClient cc) {
		if (cc.getCompanyClientId() > -1) {// insert instead

			String sql = "UPDATE `company_client` SET " + " update_User_Id = ? " + " ,update_Dt = ? "
					+ " ,client_name = ? " +

					// " ,business_Headline = ? "+
					// " ,business_Description = ? "+
					" WHERE  company_client_id = ? ";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, company.getUpdateUserId());
				pstmt.setTimestamp(2, getCurrentTimeStamp());
				pstmt.setString(3, cc.getClientName());
				pstmt.setInt(4, cc.getCompanyClientId());
				// pstmt.setString(5,
				// trimString(company.getBusinessHeadline()));
				// pstmt.setString(6,
				// trimString(company.getBusinessDescription()));
				int iRslt = pstmt.executeUpdate();

				if (iRslt < 1) {
					log.error("Faild to update company file ID:" + cc.getCompanyClientId());
				}
				clientList.add(cc);

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		}
		return clientList;

	}

	private CompanyFile insertCompanyFile(Company company, CompanyFile cf) {
		String sql = "insert into company_file "
				+ " (company_ID,create_Dt,create_User_Id,document_name,Is_Legal_Document,relative_path,update_dt,update_user_id "

				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			int isLegal=0;
			if(cf.getDocumentIndicator()!=null && cf.getDocumentIndicator()==1) {
				isLegal=1;
			}
		//	UUID relativePath = UUID.randomUUID();
			pstmt.setInt(1, company.getCompanyId());
			pstmt.setTimestamp(2, company.getCreateDt());
			pstmt.setInt(3, company.getCreateUserId());
			pstmt.setString(4, cf.getDocumentName());
			pstmt.setInt(5, isLegal);
			pstmt.setString(6, cf.getRelativePath());
			pstmt.setTimestamp(7, getCurrentTimeStamp());
			pstmt.setInt(8, company.getUpdateUserId());

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				cf.setCompanyFileId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return cf;
	}

	
	private CompanyAddress insertCompanyAddress(CompanyAddress address) {
	
		String sql = "insert into company_address "
				+ " (address_line_1,address_line_2,address_line_3,city_name,province_id,country_id,postal_cd,create_user_id,create_dt,update_user_id,update_dt "

				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?,?,now(),?,now());";
		int provinceId=getProvinceModel(address.getProvinceName(), address.getCountryId());

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			
		//	UUID relativePath = UUID.randomUUID();
			pstmt.setString(1, address.getAddressLine1());
			pstmt.setString(2, address.getAddressLine2());
			pstmt.setString(3, address.getAddressLine3());
			pstmt.setString(4, address.getCityName());
			pstmt.setInt(5, provinceId);
			pstmt.setInt(6, address.getCountryId());
			pstmt.setString(7, address.getPostalCd());
			pstmt.setInt(8, address.getCreateUserId());
			pstmt.setInt(9, address.getCreateUserId());
			
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				address.setCompanyAddressId(rs.getInt(1));
				address=getCompanyAddressById(address.getCompanyAddressId());
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return address;
	}

	
	private FileDocumentType insertFileDocumentType(FileDocumentType fdt, DocumentType dt, CompanyFile cf) {
		String sql = "insert into file_document_type " + " (company_file_id,document_type_id "

		// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1, cf.getCompanyFileId());
			pstmt.setInt(2, dt.getDocumentTypeId());

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				fdt.setFileDocumentTypeId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return fdt;
	}

	private CompanyFile updateCompanyFile(Company company, CompanyFile cf) {
		String sql = "UPDATE `company_file` SET " + " update_User_Id = ? " + " ,update_Dt = ? " + " ,document_name = ? "
				+ " ,relative_path = ? " +

				// " ,business_Headline = ? "+
				// " ,business_Description = ? "+
				" WHERE  company_file_id = ? ";

		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, company.getUpdateUserId());
			pstmt.setTimestamp(2, getCurrentTimeStamp());
			pstmt.setString(3, cf.getDocumentName());
			pstmt.setString(4, cf.getRelativePath());
			pstmt.setInt(5, cf.getCompanyFileId());
			// pstmt.setString(5, trimString(company.getBusinessHeadline()));
			// pstmt.setString(6, trimString(company.getBusinessDescription()));
			int iRslt = pstmt.executeUpdate();

			if (iRslt < 1) {
				log.error("Faild to update company file ID:" + cf.getCompanyFileId());
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return cf;
	}

	
	private CompanyAddress updateCompanyAddress(CompanyAddress address) {
		String sql = "UPDATE `company_address` SET " + " address_Line_1 = ?,address_Line_2 = ? ,address_Line_3 = ?,city_Name=?,province_Id=?,country_id=?,postal_cd=?,update_user_id=?,"
				+ "update_dt=now() "
				+ " WHERE  company_address_id = ?  and update_dt=?";
		
		int provinceId=getProvinceModel(address.getProvinceName(), address.getCountryId());

		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, address.getAddressLine1());
			pstmt.setString(2,address.getAddressLine2());
			pstmt.setString(3, address.getAddressLine3());
			pstmt.setString(4, address.getCityName());
			pstmt.setInt(5, provinceId);
			pstmt.setInt(6, address.getCountryId());
			pstmt.setString(7, address.getPostalCd());
			pstmt.setInt(8, address.getUpdateUserId());
			pstmt.setInt(9, address.getCompanyAddressId());
			pstmt.setTimestamp(10, address.getUpdateDt());
			
			// pstmt.setString(5, trimString(company.getBusinessHeadline()));
			// pstmt.setString(6, trimString(company.getBusinessDescription()));
			int iRslt = pstmt.executeUpdate();
			
			
			return doGetCompanyAddress(address.getCompanyAddressId());
		 // 	return address;
			
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return address;
	}

	
	@POST // http://localhost:8080/market-api/trucker
	@Path("updatecompanyequipmentonly") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public CompanyEquipment updateCompanyEquipmentCount(CompanyEquipment ce) {
		try {
			updateWizardSetNumber(ce.getCompanyId(), 5);
		return setUpdateCompanyEquipment(ce,ce.getCompanyId());
	
		
	} catch (Exception e) {
		log.error("Error message: " + e, e);
		new ServiceException(Response.Status.CONFLICT);
	}
		return null;
	}
	
	@POST // http://localhost:8080/market-api/trucker
	@Path("insertservice") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Service createInsertStatementService(Service service) {
		try {
			
			return insertService(service);
	
		
	} catch (Exception e) {
		log.error("Error message: " + e, e);
		new ServiceException(Response.Status.CONFLICT);
	}
		return null;
	}
	
	
	
	private User setUpdateUser(User usr) {
	
		String sql = "update user set email_address=?,phone_nbr=?,first_name=?,last_name=?,update_user_id=?,update_dt=now() "
				+ " where user_id=? and update_dt=?";
				// +" ,businessHeadline,businessDescription "
		log.info("user sql " + sql);
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setString(1,usr.getEmailAddress());
			pstmt.setString(2,usr.getPhoneNumber());
			pstmt.setString(3,usr.getFirstName());
			pstmt.setString(4,usr.getLastName());
		
			pstmt.setInt(5,usr.getUpdateUserId());
			pstmt.setInt(6,usr.getUserId());
			pstmt.setTimestamp(7,usr.getUpdateDt());
			int id = pstmt.executeUpdate();
			return usr;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	private CompanyUser setUpdateCompanyUser(CompanyUser usr) {
		String sql = "update company_user set is_primary_contact=?,is_technical_contact=?,update_user_id=?,update_dt=? "
				+ " where user_id=? ";
				// +" ,businessHeadline,businessDescription "
			log.info("SQL "+sql);
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setBoolean(1,usr.getIsTechnicalContact());
			pstmt.setBoolean(2,usr.getIsPrimaryContact());
			pstmt.setInt(3,usr.getUpdateUserId());
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,usr.getUserId());
			int id = pstmt.executeUpdate();
			return usr;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}

	
	private User setUpdateCompanyUser_v2(User usr) {
		String sql = "update company_user set is_primary_contact=?,is_technical_contact=?,update_user_id=?,update_dt=? "
				+ " where user_id=? ";
				// +" ,businessHeadline,businessDescription "
			
		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setBoolean(1,usr.getIsPrimaryContact());
			
			pstmt.setBoolean(2,usr.getIsTechnicalContact());
			pstmt.setInt(3,usr.getUpdateUserId());
			pstmt.setTimestamp(4,getCurrentTimeStamp());
			pstmt.setInt(5,usr.getUserId());
			int id = pstmt.executeUpdate();
			return usr;
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		
		return null;
	}
	
	/**
	 * @param companyFileId
	 * @return
	 */
	public Boolean deleteUserLangauge(Integer userLanguageId) {
		

		String sql = "DELETE FROM `user_langauge` WHERE " + " user_language_id = ? ";

		try {
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);)
			{
				pstmt.setInt(1, userLanguageId);

				int id = pstmt.executeUpdate();

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return true;

	}
	/**
	 * @param companyFileId
	 * @return
	 */
	public Boolean deleteUserLangaugeByUserId(Integer userId) {
		

		String sql = "DELETE FROM `user_language` WHERE " + " user_id = ? ";

		try {
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);)
			{
				pstmt.setInt(1, userId);

				int id = pstmt.executeUpdate();

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return true;

	}
	
	@POST // http://localhost:8080/market-api/trucker
	@Path("updatecompanyaddress") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public CompanyAddress updateCompanyAddressModel(CompanyAddress address) {
		log.info("address"+address.getCompanyAddressId());
		try {
			return updateCompanyAddress(address);
		}catch(Exception ee) {
			log.error("Error updating address"+address.getCompanyAddressId(),ee);
		}
		return address;
	}
	
	
	@POST // http://localhost:8080/market-api/truckers
	@Path("insertcompanyservices") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CompanyService> insertCompanyServicesModel(Company company){
		List<CompanyService> compServices = new LinkedList();
		List<CompanyService> companyServices = company.getCompanyServices();
		try {
		if(companyServices!=null && companyServices.size()>0) {
			for(CompanyService ccs : companyServices) {
				if(ccs.getCompanyServiceId()==-1) {
					compServices.add(insertNewCompanyService(ccs,company.getCompanyId()));
				}else if(ccs.getServiceId()==-1) {//delete
					setDeleteCompanyService(ccs);
				}else if(ccs.getService()!=null){
					compServices.add(updateCompanyService(ccs,company.getCompanyId()));
				}
			}
		}
		updateWizardSetNumber(company.getCompanyId(),4);
	}catch(Exception ee) {
		log.error("Error inserting address",ee);
	}
	return compServices;
		
	
	}
	
	private CompanyFile insertCompanyFile(CompanyFile cf) {
		String sql = "insert into company_file "
				+ " (company_ID,create_Dt,create_User_Id,document_name,Is_Legal_Document,relative_path,update_dt,update_user_id "

				// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?,?,?,?,?,?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			
		//	UUID relativePath = UUID.randomUUID();
			pstmt.setInt(1, cf.getCompanyId());
			pstmt.setTimestamp(2, getCurrentTimeStamp());
			pstmt.setInt(3, cf.getCreateUserId());
			pstmt.setString(4, cf.getDocumentName());
			pstmt.setBoolean(5, cf.isLegalDocument());
			pstmt.setString(6, cf.getRelativePath());
			pstmt.setTimestamp(7, getCurrentTimeStamp());
			pstmt.setInt(8, cf.getUpdateUserId());

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				cf.setCompanyFileId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return cf;
	}
	
	private CompanyFile updateCompanyFile(CompanyFile cf) {
		String sql = "UPDATE `company_file` SET " + " update_User_Id = ? " + " ,update_Dt = ? " + " ,document_name = ? "
				+ " ,relative_path = ? " +

				// " ,business_Headline = ? "+
				// " ,business_Description = ? "+
				" WHERE  company_file_id = ? ";

		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, cf.getUpdateUserId());
			pstmt.setTimestamp(2, getCurrentTimeStamp());
			pstmt.setString(3, cf.getDocumentName());
			pstmt.setString(4, cf.getRelativePath());
			pstmt.setInt(5, cf.getCompanyFileId());
			// pstmt.setString(5, trimString(company.getBusinessHeadline()));
			// pstmt.setString(6, trimString(company.getBusinessDescription()));
			int iRslt = pstmt.executeUpdate();

			if (iRslt < 1) {
				log.error("Faild to update company file ID:" + cf.getCompanyFileId());
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return cf;
	}
	
	@POST
	@Path("createfiledocumenttype")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<FileDocumentType> createFileDocumentType(List<FileDocumentType> fdtList){
		Integer companyId=0;
		List<FileDocumentType> j_fdt = new LinkedList<>();
		try {
		if (fdtList != null && fdtList.size() > 0) {
			companyId=fdtList.get(0).getCompanyFile().getCompanyId();
			for (FileDocumentType fdt : fdtList) {
				CompanyFile cf = fdt.getCompanyFile();
			
				DocumentType dt = fdt.getDocumentType();
				if (cf.getCompanyFileId() == -1) {
					cf = insertCompanyFile(cf);
				} else {
					cf = updateCompanyFile(cf);
				}

				if (fdt.getFileDocumentTypeId() == null || fdt.getFileDocumentTypeId() == -1) {
					// new FileDocumentType
					j_fdt.add(insertFileDocumentType(fdt, dt, cf));

				} else {
					fdt.setCompanyFile(cf);
					fdt.setDocumentType(dt);
					j_fdt.add(fdt);
				}
			} 
			updateWizardSetNumber(companyId, 2);
			if(!j_fdt.isEmpty()) {
				return j_fdt;
			}
		}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return fdtList;
	}
	
	@POST // http://localhost:8080/market-api/truckers
	@Path("serviceareas") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public ServiceArea createCompanyServiceAreaModel(ServiceArea area){
		log.info("area company ID"+area.getCompanyId() + " " + area.getStatus());

		try {
		updateWizardSetNumber(area.getCompanyId(), 4);
		//return createCarrierRegion(company);
		
		return validateAndCreateServiceArea(area);
		}catch(Exception ee) {
			log.error("Error inserting address",ee);
		}
		return null;
	
	}

	private ServiceArea validateAndCreateServiceArea(ServiceArea area) {
		if(validateCarrierRegion(area.getCompanyId(), area.getServiceAreaId())!= null) {
			return null;
		}
		
		ServiceArea srvc=validateServiceArea(area.getServiceAreaName());
		
		
		
		if(srvc!=null) {
			log.info("srvc"+srvc.getServiceAreaId());
			srvc.setCompanyId(area.getCompanyId());
			area=srvc;
		}else {
			area = setNewServiceArea(area);
		}
		CarrierRegion cr = new CarrierRegion();
		cr.setCompanyId(area.getCompanyId());
		cr.setServiceAreaId(area.getServiceAreaId());
		cr.setServiceArea(area);
		cr.setCreateUserId(area.getCreateUserId());
		cr.setUpdateUserId(area.getUpdateUserId());
		cr = setNewCarrierRegion(cr, area.getCompanyId());
		area.setCarrierRegionId(cr.getCarrierRegionId());
		area.setStatus(Boolean.TRUE);
		return area;
	}
	
	
	private Service validateAndCreateService(Service srvcs) {
		if(validateCompanyService(srvcs.getCompanyId(), srvcs.getServiceId())!= null) {
			return null;
		}
		
		Service sv=validateService(srvcs.getServiceName());
		
		
		
		if(sv!=null) {
			sv.setCompanyId(srvcs.getCompanyId());
			srvcs=sv;
			
		}else {
			srvcs = setNewService(srvcs);
		}
		CompanyService cr = new CompanyService();
		cr.setCompanyId(srvcs.getCompanyId());
		cr.setServiceId(srvcs.getServiceId());
		cr.setCreateUserId(srvcs.getCreateUserId());
		cr.setUpdateUserId(srvcs.getUpdateUserId());
		cr = setNewCompanyService(cr, srvcs.getCompanyId());
		srvcs.setCompanyServiceId(cr.getCompanyServiceId());
		srvcs.setStatus(Boolean.TRUE);
		return srvcs;
	}
	
	
	
	@POST // http://localhost:8080/market-api/truckers
	@Path("deleteserviceareas") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public CarrierRegion deleteCompanyServiceAreaModel(ServiceArea area){
		log.info("area"+area.getStatus());
		try {
		updateWizardSetNumber(area.getCompanyId(), 4);
		//return createCarrierRegion(company);
		CarrierRegion cr = getCarrierRegionByServiceAreaAndCompany(area.getCompanyId(),area.getServiceAreaId());
		setDeleteCarrierRegion(cr);
		return cr;
		}catch(Exception ee) {
			log.error("Error deleting address",ee);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	
	}
	@POST // http://localhost:8080/market-api/truckers
	@Path("setserviceareas") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ServiceArea> setServiceAreas(List<ServiceArea> area){
		int companyId=-2;
		if(area!=null && !area.isEmpty()) {
			updateWizardSetNumber(area.get(0).getCompanyId(), 4);
			companyId=area.get(0).getCompanyId();
			for(ServiceArea a : area) {
				if(a.getStatus()==Boolean.FALSE && a.getCarrierRegionId()==-1) {
					//do nothinng
					log.info("Do nothing for service"+a.getServiceAreaName());
				}else if(a.getStatus()==Boolean.FALSE && a.getCarrierRegionId()!=-1) {
					//delete
					log.info("delete..."+a.getServiceAreaName());
					CarrierRegion cr = getCarrierRegionByServiceAreaAndCompany(a.getCompanyId(),a.getServiceAreaId());
					setDeleteCarrierRegion(cr);
					
				}else {
					//create
					log.info("validate and ..."+a.getServiceAreaName());
					
					validateAndCreateServiceArea(a);
				}
			}
		}
			return getServiceArea(companyId);

	
	}
	
	
	@POST // http://localhost:8080/market-api/truckers
	@Path("setservices") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Service> setServices(List<Service> srvcs){
		log.info("");
		int companyId=-2;
		if(srvcs!=null && !srvcs.isEmpty()) {
			updateWizardSetNumber(srvcs.get(0).getCompanyId(), 4);
			companyId=srvcs.get(0).getCompanyId();
			for(Service s : srvcs) {
				log.info("service " +s.getServiceName());
				if(s.getStatus()==Boolean.FALSE && s.getCompanyServiceId()==-1) {
				}else if(s.getStatus()==Boolean.FALSE && s.getCompanyServiceId()!=-1) {
				
					CompanyService cs = getCompanyServiceByServiceAndCompany(s.getCompanyId(),s.getServiceId());
					setDeleteCompanyService(cs);
					
				}else {
					//create
					
					validateAndCreateService(s);
				}
			}
		}
			return getTransportationServices(companyId);

	
	}

	
		
	@POST // http://localhost:8080/market-api/truckers
	@Path("insertcompanyservicearea") // http://localhost:8080/market-api/truckers/profile
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CarrierRegion> insertCompanyServiceAreaModel(Company company){
		updateWizardSetNumber(company.getCompanyId(), 4);
		return createCarrierRegion(company);
		
	
	}

	private List<CarrierRegion> createCarrierRegion(Company company) {
		List<CarrierRegion> region = new LinkedList();
		List<CarrierRegion> compRegion = company.getCarrierRegions();
		try {
		if(compRegion!=null && compRegion.size()>0) {
			for(CarrierRegion ccs : compRegion) {
				if(ccs.getCarrierRegionId()==-1) {
					region.add(insertNewCarrierRegion(ccs,company.getCompanyId()));
				}else if(ccs.getServiceAreaId()==-1) {//delete
					setDeleteCarrierRegion(ccs);
				}else if(ccs.getServiceArea()!=null){
					region.add(updateCarrierRegion(ccs,company.getCompanyId()));
				}
			}
		}
	}catch(Exception ee) {
		log.error("Error inserting address",ee);
	}
	return region;
	}
	
	
	
	@POST // http://localhost:8080/market-api/trucker
	@Path("insertcompanyaddress") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Company insertCompanyAddressModel(Company company) {
		try {
			updateWizardSetNumber(company.getCompanyId(), 1);
			CompanyAddress address = company.getCorporateAddress();
			if(address.getProvinceId()==-1) {
				//get province id
				address.setProvinceId(getProvince(address.getProvinceName(),address.getCountryId(), address.getCreateUserId()));
			}
			address=insertCompanyAddress(address);
			company.setCorporateAddressId(address.getCompanyAddressId());
			company.setCorporateAddress(address);
			company.setCorporateAddressId(address.getCompanyAddressId());
			updateProfileCompany(company);
			
			
		}catch(Exception ee) {
			log.error("Error inserting address",ee);
		}
		return company;
	}
	
	
	@POST // http://localhost:8080/market-api/trucker
	@Path("upsertcompanyaddress") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Company upsertCompanyAddress(Company company) {
		try {
			// company
			updateWizardSetNumber(company.getCompanyId(),1);
			String companyName = trimString(company.getCompanyName());
			String companyCode = trimString(company.getCompanyCode());

			// company address
			String province = null;
			String cityName = null;
			String line1Address = null;
			String line2Address = null;
			String postalCd = null;

			// insert child table first
			// country
			String countryName = null;
			if (company.getCorporateAddress() != null) {
				countryName = trimString(company.getCorporateAddress().getCountryName());
				province = trimString(company.getCorporateAddress().getProvinceName());
				cityName = trimString(company.getCorporateAddress().getCityName());
				line1Address = trimString(company.getCorporateAddress().getAddressLine1());
				line2Address = trimString(company.getCorporateAddress().getAddressLine2());
				postalCd = trimString(company.getCorporateAddress().getPostalCd());
			}

			int companyAddressId = -1;

			// COUNTRY

			int countryId = -1;
			// check if country already exists
			String sql = "SELECT * FROM `country` WHERE country_name = ?;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				pstmt.setString(1, company.getCorporateAddress().getCountryName());
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.Country cntry = new com.openport.marketplace.model.Country(rs);
						countryId = cntry.getCountryId();
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}

			// if not exists, then insert
			if (countryId == -1) {

				sql = "insert into country " + " (country_Name) VALUES " + " (?);";

				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

					pstmt.setString(1, countryName);

					int id = pstmt.executeUpdate();
					ResultSet rs = pstmt.getGeneratedKeys();
					if (rs != null && rs.next()) {
						countryId = rs.getInt(1);
					}
				} catch (Exception e) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);

				}
			} else {
			//	log.info("countryId exists:" + countryId);
			}

			// PROVINCE
			int provinceId = -1;

			// check if country already exists
			sql = "SELECT * FROM `province` WHERE province_name = ? and country_id = ? ;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				pstmt.setString(1, province);
				pstmt.setInt(2, countryId);
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.Province prvnc = new com.openport.marketplace.model.Province(rs);
						provinceId = prvnc.getProvinceId();
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}

			// if not exists, then insert
			if (provinceId == -1) {

				sql = "insert into province "
						+ " (province_Name,country_Id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
						+ " (?,?,?,?,?,?);";

				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

					pstmt.setString(1, province);
					pstmt.setInt(2, countryId);
					pstmt.setInt(3, 11);
					pstmt.setTimestamp(4, getCurrentTimeStamp());
					pstmt.setInt(5, 11);
					pstmt.setTimestamp(6, getCurrentTimeStamp());

					int id = pstmt.executeUpdate();
					ResultSet rs = pstmt.getGeneratedKeys();
					if (rs != null && rs.next()) {
						provinceId = rs.getInt(1);
					}
				} catch (Exception e) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
			} else {
			//	log.info("provinceId exists:" + provinceId);
			}

			// INSERT ADDRESS with CHECK
			sql = "SELECT * FROM `company_address` WHERE " + " address_Line_1 = ? " + " and city_Name = ? "
					+ " and province_Id = ? " + " and country_Id = ? " + " and postal_Cd = ? ;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				pstmt.setString(1, line1Address);
				pstmt.setString(2, cityName);
				pstmt.setInt(3, provinceId);
				pstmt.setInt(4, countryId);
				pstmt.setString(5, postalCd);

				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.CompanyAddress cmpnyAddress = new com.openport.marketplace.model.CompanyAddress(
								rs);
						companyAddressId = cmpnyAddress.getCompanyAddressId();
					}
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}

			// if not exists, then insert
			if (companyAddressId == -1) {

				sql = "insert into company_address "
						+ " (address_Line_1,address_Line_2,city_Name,province_Id,country_Id,postal_Cd,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
						+ " (?,?,?,?,?,?,?,?,?,?);";

				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

					pstmt.setString(1, line1Address);
					pstmt.setString(2, line2Address);
					pstmt.setString(3, cityName);
					pstmt.setInt(4, provinceId);
					pstmt.setInt(5, countryId);
					pstmt.setString(6, postalCd);
					pstmt.setInt(7, 11);
					pstmt.setTimestamp(8, getCurrentTimeStamp());
					pstmt.setInt(9, 11);
					pstmt.setTimestamp(10, getCurrentTimeStamp());

					int id = pstmt.executeUpdate();
					ResultSet rs = pstmt.getGeneratedKeys();
					if (rs != null && rs.next()) {
						companyAddressId = rs.getInt(1);
					}
				} catch (Exception e) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
			} else {
			//	log.info("companyAddressId exists:" + companyAddressId);
			}

			sql = "UPDATE `company` SET corporate_address_id = ? WHERE  company_name = ? ";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, companyAddressId);
				pstmt.setString(2, companyName);
				int iRslt = pstmt.executeUpdate();

				if (iRslt < 1) {
					log.error(
							"Faild to update corporate_address_id:" + companyAddressId + " for Company:" + companyName);
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}

			int wizardStepNbr = company.getWizardStepNbr();
			sql = "UPDATE `company` SET " + "wizard_step_nbr = ? " +
			// " ,business_Headline = ? "+
			// " ,business_Description = ? "+
					" WHERE  company_name = ? ";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, wizardStepNbr);
				pstmt.setString(2, companyName);
				// pstmt.setString(5,
				// trimString(company.getBusinessHeadline()));
				// pstmt.setString(6,
				// trimString(company.getBusinessDescription()));
				int iRslt = pstmt.executeUpdate();

				if (iRslt < 1) {
					log.error("Faild to update wizard_step_nbr:" + wizardStepNbr + " for Company:" + companyName);
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}

		/*
			log.info("countryName:" + countryName);
			log.info("province:" + province);
			log.info("cityName:" + cityName);
			log.info("line1Address:" + line1Address);
			log.info("line2Address:" + line2Address);
			log.info("postalCd:" + postalCd);

			log.info("companyName:" + companyName);
			log.info("companyCode:" + companyCode);
			log.info("provinceId:" + provinceId);
			log.info("companyAddressId:" + companyAddressId);
			log.info("countryId:" + countryId);
			*/
		} finally {
		//	log.debug("Done");
		}

		return company;
	}

	@POST
	@Path("document/{documentId}") // http://localhost:8080/market-api/truckers/document
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public HttpStatus deleteDocument(@PathParam("documentId") Integer documentId) {
		try {
			List<FileDocumentType> fList = getFileDocumentTypeById(documentId);
			if (fList != null && fList.size() > 0 && fList.get(0).getCompanyFile() != null) {
				int companyFileId = fList.get(0).getCompanyFile().getCompanyFileId();
				deleteFileDocumentType(fList.get(0).getFileDocumentTypeId());

				if (deleteCompanyFile(companyFileId)) {
					return new HttpStatus(Response.Status.OK);
				}
			}
		} catch (Exception ee) {
			log.error("Error Document ID" + documentId, ee);
		} finally {
		//	log.info("DONE");
		}
		return new HttpStatus(Response.Status.CONFLICT);
	}
	
	
	
	
	@POST
	@Path("asset/{assetId}") // http://localhost:8080/market-api/truckers/document
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public HttpStatus deleteCompanyEquipmentData(@PathParam("assetId") Integer assetId) {
		try {
			CompanyEquipment fList = getCompanyEquipmentyCompanyEquipmentId(assetId);
			if (fList != null) {
				int equipmentId = fList.getEquipmentId();
				setDeleteCompanyEquipment(assetId);
				setDeleteEquipment(equipmentId);
						return new HttpStatus(Response.Status.OK);
				
			}
		} catch (Exception ee) {
			log.error("Error delete company equipment" + assetId, ee);
		} finally {
			// log.info("DONE");
		}
		return new HttpStatus(Response.Status.CONFLICT);
	}
	

	@POST // http://localhost:8080/market-api/trucker
	@Path("client/{clientId}") // http://localhost:8080/market-api/shippers/trucker
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public HttpStatus deleteClient(@PathParam("clientId") Integer clientId) {
			String sql = "DELETE FROM `company_client` WHERE " + " company_client_id = ? ";

		try {
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);)
			{
				pstmt.setInt(1, clientId);

				if (pstmt.executeUpdate()>0) {
					return new HttpStatus(Response.Status.OK);
				}

			} catch (Exception e) {

				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);

		}

		return new HttpStatus(Response.Status.CONFLICT);

	}

	/**
	 * @param companyFileId
	 * @return
	 */
	public Boolean deleteCompanyFile(Integer companyFileId) {
		int wizardStepNbr = 0;

		String sql = "DELETE FROM `company_file` WHERE " + " company_file_id = ? ";

		try {
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);)
			{
				pstmt.setInt(1, companyFileId);

				int id = pstmt.executeUpdate();

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return true;

	}
	public Integer getProvinceModel(String provinceName,Integer countryId) {
		// PROVINCE
		int provinceId = -1;

		// check if country already exists
		String sql = "SELECT * FROM `province` WHERE province_name = ? and country_id = ? ;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			pstmt.setString(1, provinceName);
			pstmt.setInt(2, countryId);
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					com.openport.marketplace.model.Province prvnc = new com.openport.marketplace.model.Province(rs);
					provinceId = prvnc.getProvinceId();
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		// if not exists, then insert
		if (provinceId == -1) {

			sql = "insert into province "
					+ " (province_Name,country_Id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
					+ " (?,?,?,?,?,?);";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

				pstmt.setString(1, provinceName);
				pstmt.setInt(2, countryId);
				pstmt.setInt(3, 11);
				pstmt.setTimestamp(4, getCurrentTimeStamp());
				pstmt.setInt(5, 11);
				pstmt.setTimestamp(6, getCurrentTimeStamp());

				int id = pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs != null && rs.next()) {
					provinceId = rs.getInt(1);
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
			//return provinceId;
		}
		return provinceId;
	}
	@GET
	@Path("timezone")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Timezone> getTimezone() {
	try {
		List<String> tz = listTimezone();
		List<Timezone> timez = new LinkedList<>();
		for(String s : tz) {
			Timezone t = new Timezone();
			t.setName(s);
			timez.add(t);
		}
		return timez;
	}catch(Exception ee) {
		log.error("Error file document type",ee);
		
	}
	return null;
	}
	
	public Boolean deleteFileDocumentType(Integer fileDocumentTypeId) {
		int wizardStepNbr = 0;

		String sql = "DELETE FROM `file_document_type` WHERE " + " file_document_type_id = ? ";

		try {
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);)
			{
				pstmt.setInt(1, fileDocumentTypeId);

				int id = pstmt.executeUpdate();

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return true;

	}

	private String trimString(String s) {
		// TODO Auto-generated method stub
		if (s == null) {
			return "";
		} else {
			return s.replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
		}
	}

	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}
	private Company doGetCompanyById(int companyId){
		Company company  = new Company();
		try {
			String sql = 	" SELECT Company_ID, Company_Name, Is_Carrier, Is_Shipper, Company_Code, Corporate_Address_ID, Truck_Pool_Address_ID, Branch_Address_ID, "+
							" Create_User_ID, Create_Dt, Update_User_ID, Update_Dt, Wizard_Step_Nbr, Years_of_operation, Business_Headline, Business_Description,has_Pod,timezone_id "+
							" FROM marketplace_db.company " +
							" WHERE company_Id  = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
					pstmt.setInt(1, companyId);
					try (ResultSet rs = pstmt.executeQuery()) {
						ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
						List<Company> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, Company.class);
						if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
							company = pojoListObj.get(0);
						}
						
				     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return company;
	}
	
	
private CompanyAddress doGetCompanyAddress(int companyAddressId){
		
		
		CompanyAddress companyAddress  = new CompanyAddress();
		
		try {
			
			String sql = 	" SELECT Company_Address_ID, Address_Line_1, Address_Line_2, Address_Line_3, City_Name, c.Province_ID, c.Country_ID, "+
				" Postal_Cd, Years_Of_Operation, Business_Headline, Business_Description, c.Create_User_ID, c.Create_Dt, c.Update_User_ID, c.Update_Dt, "+ 
				" province_name, country_name "+
				" FROM marketplace_db.company_address c "+
				" left JOIN marketplace_db.province on c.province_id = province.province_id " +
				" left JOIN marketplace_db.country  on c.country_id = country.country_id " +
				" WHERE company_Address_Id  = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setInt(1, companyAddressId);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CompanyAddress> driverRawMapper = new ResultSetMapper<CompanyAddress>();
					List<CompanyAddress> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, CompanyAddress.class);
					if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
						companyAddress = pojoListObj.get(0);
					}
					
			     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return companyAddress;
	}
   

	private List<CompanyClient> doGetCompanyClients(int companyId){
		
		
		List<CompanyClient> clientList = new ArrayList(); 
		
		try {
			
			String sql = 	" SELECT Company_Client_ID, Company_ID, Client_Name, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "+
							" FROM marketplace_db.company_client " +
							" WHERE Company_ID  = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setInt(1, companyId);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CompanyClient> driverRawMapper = new ResultSetMapper<CompanyClient>();
					clientList  = driverRawMapper.mapResultSetToObject(rs, CompanyClient.class);
			     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return clientList;
	}
	
	public List<String> listTimezone() {
		 List<String> tzlist = new LinkedList();
		 tzlist.add("ACT");
		 tzlist.add("AET");
		 tzlist.add("Africa/Abidjan");
		 tzlist.add("Africa/Accra");
		 tzlist.add("Africa/Addis_Ababa");
		 tzlist.add("Africa/Algiers");
		 tzlist.add("Africa/Asmara");
		 tzlist.add("Africa/Asmera");
		 tzlist.add("Africa/Bamako");
		 tzlist.add("Africa/Bangui");
		 tzlist.add("Africa/Banjul");
		 tzlist.add("Africa/Bissau");
		 tzlist.add("Africa/Blantyre");
		 tzlist.add("Africa/Brazzaville");
		 tzlist.add("Africa/Bujumbura");
		 tzlist.add("Africa/Cairo");
		 tzlist.add("Africa/Casablanca");
		 tzlist.add("Africa/Ceuta");
		 tzlist.add("Africa/Conakry");
		 tzlist.add("Africa/Dakar");
		 tzlist.add("Africa/Dar_es_Salaam");
		 tzlist.add("Africa/Djibouti");
		 tzlist.add("Africa/Douala");
		 tzlist.add("Africa/El_Aaiun");
		 tzlist.add("Africa/Freetown");
		 tzlist.add("Africa/Gaborone");
		 tzlist.add("Africa/Harare");
		 tzlist.add("Africa/Johannesburg");
		 tzlist.add("Africa/Kampala");
		 tzlist.add("Africa/Khartoum");
		 tzlist.add("Africa/Kigali");
		 tzlist.add("Africa/Kinshasa");
		 tzlist.add("Africa/Lagos");
		 tzlist.add("Africa/Libreville");
		 tzlist.add("Africa/Lome");
		 tzlist.add("Africa/Luanda");
		 tzlist.add("Africa/Lubumbashi");
		 tzlist.add("Africa/Lusaka");
		 tzlist.add("Africa/Malabo");
		 tzlist.add("Africa/Maputo");
		 tzlist.add("Africa/Maseru");
		 tzlist.add("Africa/Mbabane");
		 tzlist.add("Africa/Mogadishu");
		 tzlist.add("Africa/Monrovia");
		 tzlist.add("Africa/Nairobi");
		 tzlist.add("Africa/Ndjamena");
		 tzlist.add("Africa/Niamey");
		 tzlist.add("Africa/Nouakchott");
		 tzlist.add("Africa/Ouagadougou");
		 tzlist.add("Africa/Porto-Novo");
		 tzlist.add("Africa/Sao_Tome");
		 tzlist.add("Africa/Timbuktu");
		 tzlist.add("Africa/Tripoli");
		 tzlist.add("Africa/Tunis");
		 tzlist.add("Africa/Windhoek");
		 tzlist.add("AGT");
		 tzlist.add("America/Adak");
		 tzlist.add("America/Anchorage");
		 tzlist.add("America/Anguilla");
		 tzlist.add("America/Antigua");
		 tzlist.add("America/Araguaina");
		 tzlist.add("America/Argentina/Buenos_Aires");
		 tzlist.add("America/Argentina/Catamarca");
		 tzlist.add("America/Argentina/ComodRivadavia");
		 tzlist.add("America/Argentina/Cordoba");
		 tzlist.add("America/Argentina/Jujuy");
		 tzlist.add("America/Argentina/La_Rioja");
		 tzlist.add("America/Argentina/Mendoza");
		 tzlist.add("America/Argentina/Rio_Gallegos");
		 tzlist.add("America/Argentina/San_Juan");
		 tzlist.add("America/Argentina/Tucuman");
		 tzlist.add("America/Argentina/Ushuaia");
		 tzlist.add("America/Aruba");
		 tzlist.add("America/Asuncion");
		 tzlist.add("America/Atikokan");
		 tzlist.add("America/Atka");
		 tzlist.add("America/Bahia");
		 tzlist.add("America/Barbados");
		 tzlist.add("America/Belem");
		 tzlist.add("America/Belize");
		 tzlist.add("America/Blanc-Sablon");
		 tzlist.add("America/Boa_Vista");
		 tzlist.add("America/Bogota");
		 tzlist.add("America/Boise");
		 tzlist.add("America/Buenos_Aires");
		 tzlist.add("America/Cambridge_Bay");
		 tzlist.add("America/Campo_Grande");
		 tzlist.add("America/Cancun");
		 tzlist.add("America/Caracas");
		 tzlist.add("America/Catamarca");
		 tzlist.add("America/Cayenne");
		 tzlist.add("America/Cayman");
		 tzlist.add("America/Chicago");
		 tzlist.add("America/Chihuahua");
		 tzlist.add("America/Coral_Harbour");
		 tzlist.add("America/Cordoba");
		 tzlist.add("America/Costa_Rica");
		 tzlist.add("America/Cuiaba");
		 tzlist.add("America/Curacao");
		 tzlist.add("America/Danmarkshavn");
		 tzlist.add("America/Dawson");
		 tzlist.add("America/Dawson_Creek");
		 tzlist.add("America/Denver");
		 tzlist.add("America/Detroit");
		 tzlist.add("America/Dominica");
		 tzlist.add("America/Edmonton");
		 tzlist.add("America/Eirunepe");
		 tzlist.add("America/El_Salvador");
		 tzlist.add("America/Ensenada");
		 tzlist.add("America/Fort_Wayne");
		 tzlist.add("America/Fortaleza");
		 tzlist.add("America/Glace_Bay");
		 tzlist.add("America/Godthab");
		 tzlist.add("America/Goose_Bay");
		 tzlist.add("America/Grand_Turk");
		 tzlist.add("America/Grenada");
		 tzlist.add("America/Guadeloupe");
		 tzlist.add("America/Guatemala");
		 tzlist.add("America/Guayaquil");
		 tzlist.add("America/Guyana");
		 tzlist.add("America/Halifax");
		 tzlist.add("America/Havana");
		 tzlist.add("America/Hermosillo");
		 tzlist.add("America/Indiana/Indianapolis");
		 tzlist.add("America/Indiana/Knox");
		 tzlist.add("America/Indiana/Marengo");
		 tzlist.add("America/Indiana/Petersburg");
		 tzlist.add("America/Indiana/Tell_City");
		 tzlist.add("America/Indiana/Vevay");
		 tzlist.add("America/Indiana/Vincennes");
		 tzlist.add("America/Indiana/Winamac");
		 tzlist.add("America/Indianapolis");
		 tzlist.add("America/Inuvik");
		 tzlist.add("America/Iqaluit");
		 tzlist.add("America/Jamaica");
		 tzlist.add("America/Jujuy");
		 tzlist.add("America/Juneau");
		 tzlist.add("America/Kentucky/Louisville");
		 tzlist.add("America/Kentucky/Monticello");
		 tzlist.add("America/Knox_IN");
		 tzlist.add("America/La_Paz");
		 tzlist.add("America/Lima");
		 tzlist.add("America/Los_Angeles");
		 tzlist.add("America/Louisville");
		 tzlist.add("America/Maceio");
		 tzlist.add("America/Managua");
		 tzlist.add("America/Manaus");
		 tzlist.add("America/Martinique");
		 tzlist.add("America/Mazatlan");
		 tzlist.add("America/Mendoza");
		 tzlist.add("America/Menominee");
		 tzlist.add("America/Merida");
		 tzlist.add("America/Mexico_City");
		 tzlist.add("America/Miquelon");
		 tzlist.add("America/Moncton");
		 tzlist.add("America/Monterrey");
		 tzlist.add("America/Montevideo");
		 tzlist.add("America/Montreal");
		 tzlist.add("America/Montserrat");
		 tzlist.add("America/Nassau");
		 tzlist.add("America/New_York");
		 tzlist.add("America/Nipigon");
		 tzlist.add("America/Nome");
		 tzlist.add("America/Noronha");
		 tzlist.add("America/North_Dakota/Center");
		 tzlist.add("America/North_Dakota/New_Salem");
		 tzlist.add("America/Panama");
		 tzlist.add("America/Pangnirtung");
		 tzlist.add("America/Paramaribo");
		 tzlist.add("America/Phoenix");
		 tzlist.add("America/Port_of_Spain");
		 tzlist.add("America/Port-au-Prince");
		 tzlist.add("America/Porto_Acre");
		 tzlist.add("America/Porto_Velho");
		 tzlist.add("America/Puerto_Rico");
		 tzlist.add("America/Rainy_River");
		 tzlist.add("America/Rankin_Inlet");
		 tzlist.add("America/Recife");
		 tzlist.add("America/Regina");
		 tzlist.add("America/Resolute");
		 tzlist.add("America/Rio_Branco");
		 tzlist.add("America/Rosario");
		 tzlist.add("America/Santiago");
		 tzlist.add("America/Santo_Domingo");
		 tzlist.add("America/Sao_Paulo");
		 tzlist.add("America/Scoresbysund");
		 tzlist.add("America/Shiprock");
		 tzlist.add("America/St_Johns");
		 tzlist.add("America/St_Kitts");
		 tzlist.add("America/St_Lucia");
		 tzlist.add("America/St_Thomas");
		 tzlist.add("America/St_Vincent");
		 tzlist.add("America/Swift_Current");
		 tzlist.add("America/Tegucigalpa");
		 tzlist.add("America/Thule");
		 tzlist.add("America/Thunder_Bay");
		 tzlist.add("America/Tijuana");
		 tzlist.add("America/Toronto");
		 tzlist.add("America/Tortola");
		 tzlist.add("America/Vancouver");
		 tzlist.add("America/Virgin");
		 tzlist.add("America/Whitehorse");
		 tzlist.add("America/Winnipeg");
		 tzlist.add("America/Yakutat");
		 tzlist.add("America/Yellowknife");
		 tzlist.add("Antarctica/Casey");
		 tzlist.add("Antarctica/Davis");
		 tzlist.add("Antarctica/DumontDUrville");
		 tzlist.add("Antarctica/Mawson");
		 tzlist.add("Antarctica/McMurdo");
		 tzlist.add("Antarctica/Palmer");
		 tzlist.add("Antarctica/Rothera");
		 tzlist.add("Antarctica/South_Pole");
		 tzlist.add("Antarctica/Syowa");
		 tzlist.add("Antarctica/Vostok");
		 tzlist.add("Arctic/Longyearbyen");
		 tzlist.add("ART");
		 tzlist.add("Asia/Aden");
		 tzlist.add("Asia/Almaty");
		 tzlist.add("Asia/Amman");
		 tzlist.add("Asia/Anadyr");
		 tzlist.add("Asia/Aqtau");
		 tzlist.add("Asia/Aqtobe");
		 tzlist.add("Asia/Ashgabat");
		 tzlist.add("Asia/Ashkhabad");
		 tzlist.add("Asia/Baghdad");
		 tzlist.add("Asia/Bahrain");
		 tzlist.add("Asia/Baku");
		 tzlist.add("Asia/Bangkok");
		 tzlist.add("Asia/Beirut");
		 tzlist.add("Asia/Bishkek");
		 tzlist.add("Asia/Brunei");
		 tzlist.add("Asia/Calcutta");
		 tzlist.add("Asia/Choibalsan");
		 tzlist.add("Asia/Chongqing");
		 tzlist.add("Asia/Chungking");
		 tzlist.add("Asia/Colombo");
		 tzlist.add("Asia/Dacca");
		 tzlist.add("Asia/Damascus");
		 tzlist.add("Asia/Dhaka");
		 tzlist.add("Asia/Dili");
		 tzlist.add("Asia/Dubai");
		 tzlist.add("Asia/Dushanbe");
		 tzlist.add("Asia/Gaza");
		 tzlist.add("Asia/Harbin");
		 tzlist.add("Asia/Hong_Kong");
		 tzlist.add("Asia/Hovd");
		 tzlist.add("Asia/Irkutsk");
		 tzlist.add("Asia/Istanbul");
		 tzlist.add("Asia/Jakarta");
		 tzlist.add("Asia/Jayapura");
		 tzlist.add("Asia/Jerusalem");
		 tzlist.add("Asia/Kabul");
		 tzlist.add("Asia/Kolkata");
		 
		 tzlist.add("Asia/Kamchatka");
		 tzlist.add("Asia/Karachi");
		 tzlist.add("Asia/Kashgar");
		 tzlist.add("Asia/Katmandu");
		 tzlist.add("Asia/Krasnoyarsk");
		 tzlist.add("Asia/Kuala_Lumpur");
		 tzlist.add("Asia/Kuching");
		 tzlist.add("Asia/Kuwait");
		 tzlist.add("Asia/Macao");
		 tzlist.add("Asia/Macau");
		 tzlist.add("Asia/Magadan");
		 tzlist.add("Asia/Makassar");
		 tzlist.add("Asia/Manila");
		 
		 tzlist.add("Asia/Muscat");
		 tzlist.add("Asia/Mumbai");
		 
		
		 tzlist.add("Asia/Nicosia");
		 tzlist.add("Asia/Novosibirsk");
		 tzlist.add("Asia/Omsk");
		 tzlist.add("Asia/Oral");
		 tzlist.add("Asia/Phnom_Penh");
		 tzlist.add("Asia/Pontianak");
		 tzlist.add("Asia/Pyongyang");
		 tzlist.add("Asia/Qatar");
		 tzlist.add("Asia/Qyzylorda");
		 tzlist.add("Asia/Rangoon");
		 tzlist.add("Asia/Riyadh");
		 tzlist.add("Asia/Riyadh87");
		 tzlist.add("Asia/Riyadh88");
		 tzlist.add("Asia/Riyadh89");
		 tzlist.add("Asia/Saigon");
		 tzlist.add("Asia/Sakhalin");
		 tzlist.add("Asia/Samarkand");
		 tzlist.add("Asia/Seoul");
		 tzlist.add("Asia/Shanghai");
		 tzlist.add("Asia/Singapore");
		 tzlist.add("Asia/Taipei");
		 tzlist.add("Asia/Tashkent");
		 tzlist.add("Asia/Tbilisi");
		 tzlist.add("Asia/Tehran");
		 tzlist.add("Asia/Tel_Aviv");
		 tzlist.add("Asia/Thimbu");
		 tzlist.add("Asia/Thimphu");
		 tzlist.add("Asia/Tokyo");
		 tzlist.add("Asia/Ujung_Pandang");
		 tzlist.add("Asia/Ulaanbaatar");
		 tzlist.add("Asia/Ulan_Bator");
		 tzlist.add("Asia/Urumqi");
		 tzlist.add("Asia/Vientiane");
		 tzlist.add("Asia/Vladivostok");
		 tzlist.add("Asia/Yakutsk");
		 tzlist.add("Asia/Yekaterinburg");
		 tzlist.add("Asia/Yerevan");
		 tzlist.add("AST");
		 tzlist.add("Atlantic/Azores");
		 tzlist.add("Atlantic/Bermuda");
		 tzlist.add("Atlantic/Canary");
		 tzlist.add("Atlantic/Cape_Verde");
		 tzlist.add("Atlantic/Faeroe");
		 tzlist.add("Atlantic/Faroe");
		 tzlist.add("Atlantic/Jan_Mayen");
		 tzlist.add("Atlantic/Madeira");
		 tzlist.add("Atlantic/Reykjavik");
		 tzlist.add("Atlantic/South_Georgia");
		 tzlist.add("Atlantic/St_Helena");
		 tzlist.add("Atlantic/Stanley");
		 tzlist.add("Australia/ACT");
		 tzlist.add("Australia/Adelaide");
		 tzlist.add("Australia/Brisbane");
		 tzlist.add("Australia/Broken_Hill");
		 tzlist.add("Australia/Canberra");
		 tzlist.add("Australia/Currie");
		 tzlist.add("Australia/Darwin");
		 tzlist.add("Australia/Eucla");
		 tzlist.add("Australia/Hobart");
		 tzlist.add("Australia/LHI");
		 tzlist.add("Australia/Lindeman");
		 tzlist.add("Australia/Lord_Howe");
		 tzlist.add("Australia/Melbourne");
		 tzlist.add("Australia/North");
		 tzlist.add("Australia/NSW");
		 tzlist.add("Australia/Perth");
		 tzlist.add("Australia/Queensland");
		 tzlist.add("Australia/South");
		 tzlist.add("Australia/Sydney");
		 tzlist.add("Australia/Tasmania");
		 tzlist.add("Australia/Victoria");
		 tzlist.add("Australia/West");
		 tzlist.add("Australia/Yancowinna");
		 tzlist.add("BET");
		 tzlist.add("Brazil/Acre");
		 tzlist.add("Brazil/DeNoronha");
		 tzlist.add("Brazil/East");
		 tzlist.add("Brazil/West");
		 tzlist.add("BST");
		 tzlist.add("Canada/Atlantic");
		 tzlist.add("Canada/Central");
		 tzlist.add("Canada/Eastern");
		 tzlist.add("Canada/East-Saskatchewan");
		 tzlist.add("Canada/Mountain");
		 tzlist.add("Canada/Newfoundland");
		 tzlist.add("Canada/Pacific");
		 tzlist.add("Canada/Saskatchewan");
		 tzlist.add("Canada/Yukon");
		 tzlist.add("CAT");
		 tzlist.add("CET");
		 tzlist.add("Chile/Continental");
		 tzlist.add("Chile/EasterIsland");
		 tzlist.add("CNT");
		 tzlist.add("CST");
		 tzlist.add("CST6CDT");
		 tzlist.add("CTT");
		 tzlist.add("Cuba");
		 tzlist.add("EAT");
		 tzlist.add("ECT");
		 tzlist.add("EET");
		 tzlist.add("Egypt");
		 tzlist.add("Eire");
		 tzlist.add("EST");
		 tzlist.add("EST5EDT");
		 tzlist.add("Etc/GMT");
		 tzlist.add("Etc/GMT+0");
		 tzlist.add("Etc/GMT+1");
		 tzlist.add("Etc/GMT+10");
		 tzlist.add("Etc/GMT+11");
		 tzlist.add("Etc/GMT+12");
		 tzlist.add("Etc/GMT+2");
		 tzlist.add("Etc/GMT+3");
		 tzlist.add("Etc/GMT+4");
		 tzlist.add("Etc/GMT+5");
		 tzlist.add("Etc/GMT+6");
		 tzlist.add("Etc/GMT+7");
		 tzlist.add("Etc/GMT+8");
		 tzlist.add("Etc/GMT+9");
		 tzlist.add("Etc/GMT0");
		 tzlist.add("Etc/GMT-0");
		 tzlist.add("Etc/GMT-1");
		 tzlist.add("Etc/GMT-10");
		 tzlist.add("Etc/GMT-11");
		 tzlist.add("Etc/GMT-12");
		 tzlist.add("Etc/GMT-13");
		 tzlist.add("Etc/GMT-14");
		 tzlist.add("Etc/GMT-2");
		 tzlist.add("Etc/GMT-3");
		 tzlist.add("Etc/GMT-4");
		 tzlist.add("Etc/GMT-5");
		 tzlist.add("Etc/GMT-6");
		 tzlist.add("Etc/GMT-7");
		 tzlist.add("Etc/GMT-8");
		 tzlist.add("Etc/GMT-9");
		 tzlist.add("Etc/Greenwich");
		 tzlist.add("Etc/UCT");
		 tzlist.add("Etc/Universal");
		 tzlist.add("Etc/UTC");
		 tzlist.add("Etc/Zulu");
		 tzlist.add("Europe/Amsterdam");
		 tzlist.add("Europe/Andorra");
		 tzlist.add("Europe/Athens");
		 tzlist.add("Europe/Belfast");
		 tzlist.add("Europe/Belgrade");
		 tzlist.add("Europe/Berlin");
		 tzlist.add("Europe/Bratislava");
		 tzlist.add("Europe/Brussels");
		 tzlist.add("Europe/Bucharest");
		 tzlist.add("Europe/Budapest");
		 tzlist.add("Europe/Chisinau");
		 tzlist.add("Europe/Copenhagen");
		 tzlist.add("Europe/Dublin");
		 tzlist.add("Europe/Gibraltar");
		 tzlist.add("Europe/Guernsey");
		 tzlist.add("Europe/Helsinki");
		 tzlist.add("Europe/Isle_of_Man");
		 tzlist.add("Europe/Istanbul");
		 tzlist.add("Europe/Jersey");
		 tzlist.add("Europe/Kaliningrad");
		 tzlist.add("Europe/Kiev");
		 tzlist.add("Europe/Lisbon");
		 tzlist.add("Europe/Ljubljana");
		 tzlist.add("Europe/London");
		 tzlist.add("Europe/Luxembourg");
		 tzlist.add("Europe/Madrid");
		 tzlist.add("Europe/Malta");
		 tzlist.add("Europe/Mariehamn");
		 tzlist.add("Europe/Minsk");
		 tzlist.add("Europe/Monaco");
		 tzlist.add("Europe/Moscow");
		 tzlist.add("Europe/Nicosia");
		 tzlist.add("Europe/Oslo");
		 tzlist.add("Europe/Paris");
		 tzlist.add("Europe/Podgorica");
		 tzlist.add("Europe/Prague");
		 tzlist.add("Europe/Riga");
		 tzlist.add("Europe/Rome");
		 tzlist.add("Europe/Samara");
		 tzlist.add("Europe/San_Marino");
		 tzlist.add("Europe/Sarajevo");
		 tzlist.add("Europe/Simferopol");
		 tzlist.add("Europe/Skopje");
		 tzlist.add("Europe/Sofia");
		 tzlist.add("Europe/Stockholm");
		 tzlist.add("Europe/Tallinn");
		 tzlist.add("Europe/Tirane");
		 tzlist.add("Europe/Tiraspol");
		 tzlist.add("Europe/Uzhgorod");
		 tzlist.add("Europe/Vaduz");
		 tzlist.add("Europe/Vatican");
		 tzlist.add("Europe/Vienna");
		 tzlist.add("Europe/Vilnius");
		 tzlist.add("Europe/Volgograd");
		 tzlist.add("Europe/Warsaw");
		 tzlist.add("Europe/Zagreb");
		 tzlist.add("Europe/Zaporozhye");
		 tzlist.add("Europe/Zurich");
		 tzlist.add("GB");
		 tzlist.add("GB-Eire");
		 tzlist.add("GMT");
		 tzlist.add("GMT0");
		 tzlist.add("Greenwich");
		 tzlist.add("Hongkong");
		 tzlist.add("HST");
		 tzlist.add("Iceland");
		 tzlist.add("IET");
		 tzlist.add("Indian/Antananarivo");
		 tzlist.add("Indian/Chagos");
		 tzlist.add("Indian/Christmas");
		 tzlist.add("Indian/Cocos");
		 tzlist.add("Indian/Comoro");
		 tzlist.add("Indian/Kerguelen");
		 tzlist.add("Indian/Mahe");
		 tzlist.add("Indian/Maldives");
		 tzlist.add("Indian/Mauritius");
		 tzlist.add("Indian/Mayotte");
		 tzlist.add("Indian/Reunion");
		 tzlist.add("Iran");
		 tzlist.add("Israel");
		 tzlist.add("IST");
		 tzlist.add("Jamaica");
		 tzlist.add("Japan");
		 tzlist.add("JST");
		 tzlist.add("Kwajalein");
		 tzlist.add("Libya");
		 tzlist.add("MET");
		 tzlist.add("Mexico/BajaNorte");
		 tzlist.add("Mexico/BajaSur");
		 tzlist.add("Mexico/General");
		 tzlist.add("Mideast/Riyadh87");
		 tzlist.add("Mideast/Riyadh88");
		 tzlist.add("Mideast/Riyadh89");
		 tzlist.add("MIT");
		 tzlist.add("MST");
		 tzlist.add("MST7MDT");
		 tzlist.add("Navajo");
		 tzlist.add("NET");
		 tzlist.add("NST");
		 tzlist.add("NZ");
		 tzlist.add("NZ-CHAT");
		 tzlist.add("Pacific/Apia");
		 tzlist.add("Pacific/Auckland");
		 tzlist.add("Pacific/Chatham");
		 tzlist.add("Pacific/Easter");
		 tzlist.add("Pacific/Efate");
		 tzlist.add("Pacific/Enderbury");
		 tzlist.add("Pacific/Fakaofo");
		 tzlist.add("Pacific/Fiji");
		 tzlist.add("Pacific/Funafuti");
		 tzlist.add("Pacific/Galapagos");
		 tzlist.add("Pacific/Gambier");
		 tzlist.add("Pacific/Guadalcanal");
		 tzlist.add("Pacific/Guam");
		 tzlist.add("Pacific/Honolulu");
		 tzlist.add("Pacific/Johnston");
		 tzlist.add("Pacific/Kiritimati");
		 tzlist.add("Pacific/Kosrae");
		 tzlist.add("Pacific/Kwajalein");
		 tzlist.add("Pacific/Majuro");
		 tzlist.add("Pacific/Marquesas");
		 tzlist.add("Pacific/Midway");
		 tzlist.add("Pacific/Nauru");
		 tzlist.add("Pacific/Niue");
		 tzlist.add("Pacific/Norfolk");
		 tzlist.add("Pacific/Noumea");
		 tzlist.add("Pacific/Pago_Pago");
		 tzlist.add("Pacific/Palau");
		 tzlist.add("Pacific/Pitcairn");
		 tzlist.add("Pacific/Ponape");
		 tzlist.add("Pacific/Port_Moresby");
		 tzlist.add("Pacific/Rarotonga");
		 tzlist.add("Pacific/Saipan");
		 tzlist.add("Pacific/Samoa");
		 tzlist.add("Pacific/Tahiti");
		 tzlist.add("Pacific/Tarawa");
		 tzlist.add("Pacific/Tongatapu");
		 tzlist.add("Pacific/Truk");
		 tzlist.add("Pacific/Wake");
		 tzlist.add("Pacific/Wallis");
		 tzlist.add("Pacific/Yap");
		 tzlist.add("PLT");
		 tzlist.add("PNT");
		 tzlist.add("Poland");
		 tzlist.add("Portugal");
		 tzlist.add("PRC");
		 tzlist.add("PRT");
		 tzlist.add("PST");
		 tzlist.add("PST8PDT");
		 tzlist.add("ROK");
		 tzlist.add("Singapore");
		 tzlist.add("SST");
		 tzlist.add("SystemV/AST4");
		 tzlist.add("SystemV/AST4ADT");
		 tzlist.add("SystemV/CST6");
		 tzlist.add("SystemV/CST6CDT");
		 tzlist.add("SystemV/EST5");
		 tzlist.add("SystemV/EST5EDT");
		 tzlist.add("SystemV/HST10");
		 tzlist.add("SystemV/MST7");
		 tzlist.add("SystemV/MST7MDT");
		 tzlist.add("SystemV/PST8");
		 tzlist.add("SystemV/PST8PDT");
		 tzlist.add("SystemV/YST9");
		 tzlist.add("SystemV/YST9YDT");
		 tzlist.add("Turkey");
		 tzlist.add("UCT");
		 tzlist.add("Universal");
		 tzlist.add("US/Alaska");
		 tzlist.add("US/Aleutian");
		 tzlist.add("US/Arizona");
		 tzlist.add("US/Central");
		 tzlist.add("US/Eastern");
		 tzlist.add("US/East-Indiana");
		 tzlist.add("US/Hawaii");
		 tzlist.add("US/Indiana-Starke");
		 tzlist.add("US/Michigan");
		 tzlist.add("US/Mountain");
		 tzlist.add("US/Pacific");
		 tzlist.add("US/Pacific-New");
		 tzlist.add("US/Samoa");
		 tzlist.add("UTC");
		 tzlist.add("VST");
		 tzlist.add("WET");
		 tzlist.add("W-SU");
		 tzlist.add("Zulu");
		 return tzlist;
	}
}
