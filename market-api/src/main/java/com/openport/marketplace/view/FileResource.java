package com.openport.marketplace.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.UserStatus;
import com.openport.marketplace.repository.DbConfigODS;

@Path("files") // http://localhost:8080/market-api/files
public class FileResource {

	private static String isUat = System.getProperty("is.uat");
	private static final String ROOT_FOLDER = "/opt/openport"
				+(((isUat == null) || (isUat.isEmpty()) 
						|| isUat.equalsIgnoreCase("false")
						|| isUat.equalsIgnoreCase("no")
						|| isUat.equalsIgnoreCase("0"))?"":"-uat")
				+"/data/DOCUMENTS/";
	
	private static final String TRACK_TACE_ROOT_FOLDER = "/opt/openport/data/PICTURES/";
	
	//private static final String ROOT_FOLDER="C:/Users/nargv2015/test/";
	static final transient Logger log = Logger.getLogger(FileResource.class);
	// private TmsDeliveryRepository tmsDeliveryRepository = new TmsDeliveryRepositoryStub();


	@POST
	@Path("upload") // http://localhost:8080/market-api/files/upload
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus uploadFile(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {
		UserStatus sts = new UserStatus();
		sts.setStatus(Response.Status.OK.getStatusCode());
		sts.setMessage(Response.Status.OK.getReasonPhrase());
		String filename = "null";
		if (fileDetail!=null&&fileDetail.getFileName()!=null) {
			filename = UUID.randomUUID().toString() + "_" + fileDetail.getFileName();
		}
		
		try {
			saveFile(uploadedInputStream, ROOT_FOLDER + filename);
			sts.setFileId(filename);
			return sts;
		} catch (IOException e) {
			log.error(e + " saving filename:"+filename, e);
			new ServiceException(Response.Status.INTERNAL_SERVER_ERROR, e.toString());
		}

		new ServiceException(Response.Status.FORBIDDEN);
		return null;
	}

	private void saveFile(InputStream uploadedInputStream, String fileLocation)
			throws FileNotFoundException, IOException {
		FileOutputStream out = new FileOutputStream(new File(fileLocation));
		int read = 0;
		byte[] bytes = new byte[1024];

		out = new FileOutputStream(new File(fileLocation));
		while ((read = uploadedInputStream.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}
		out.flush();
		out.close();
	}

	@GET
	@Path("{relativePath}/{filename}") // http://localhost:8080/market-api/files/32de53c1-21a6-430d-a55d-f92279810e07_1448949868112.jpg/1448949868112.jpg
	public Response downloadFile(@PathParam("relativePath") int relativePath, 
			@PathParam("filename") int filename) {
		log.info("start download of fileId:"+relativePath+" as :"+filename);

		try {
			StreamingOutput fileStream =  new StreamingOutput()
	        {
	            @Override
	            public void write(java.io.OutputStream output) throws IOException, WebApplicationException
	            {
	                try
	                {
	                	log.info("Downloading "+ROOT_FOLDER+relativePath);
	                    java.nio.file.Path path = Paths.get(ROOT_FOLDER+relativePath);
	                    byte[] data = Files.readAllBytes(path);
	                    output.write(data);
	                    output.flush();
	                }
	                catch (Exception e)
	                {
	                	log.error("Unable to read "+ROOT_FOLDER+relativePath, e);
	                    // throw new WebApplicationException("File Not Found !!");
	    				new ServiceException(Response.Status.CONFLICT);
	                }
	            }
	        };
	        return Response
	                .ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
	                .header("content-disposition","attachment; filename="+relativePath)
	                .build();
		} catch( WebApplicationException ex) {
        	return Response.status(Status.CONFLICT).build();
		}
	}

	@GET
	@Path("{relativePath}") // http://localhost:8080/market-api/files/32de53c1-21a6-430d-a55d-f92279810e07_1448949868112.jpg
	public Response displayFile(@PathParam("relativePath") String relativePath) {
		log.info("start download of relativePath:"+relativePath+"; ROOT_FOLDER:"+ROOT_FOLDER);
		
		if (relativePath == null || relativePath.trim().length()==0) {
			log.error("Unable to download relativePath:"+relativePath+"; ROOT_FOLDER:"+ROOT_FOLDER);
			new ServiceException(Response.Status.NOT_FOUND);
		} else {
			try {
				StreamingOutput fileStream =  new StreamingOutput()
		        {
		            @Override
		            public void write(java.io.OutputStream output) throws IOException, WebApplicationException
		            {
		                try
		                {
		                	log.info("Downloading "+ROOT_FOLDER+relativePath);
		                    java.nio.file.Path path = Paths.get(ROOT_FOLDER+relativePath);
		                    byte[] data = Files.readAllBytes(path);
		                    output.write(data);
		                    output.flush();
		                }
		                catch (Exception e)
		                {
		                	log.error("Unable to read "+ROOT_FOLDER+relativePath, e);
		                    // throw new WebApplicationException("File Not Found !!");
		    				new ServiceException(Response.Status.CONFLICT);
		                }
		            }
		        };
		        return Response
		                .ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
		                .header("content-disposition","inline; filename="+relativePath)
		                .build();
			} catch( WebApplicationException ex) {
            	return Response.status(Status.CONFLICT).build();
			}
		}
		log.error("should never get here. relativePath:"+relativePath+"; ROOT_FOLDER:"+ROOT_FOLDER);
		new ServiceException(Response.Status.FORBIDDEN);
		return null;
	}
	
	@POST // http://localhost:8080/market-api/files/upload/trackAndTrace
	@Path("upload/trackAndTrace")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<UserStatus> uploadFile(@FormDataParam("file") List<FormDataBodyPart> bodyParts, @FormDataParam("file") FormDataContentDisposition fileDetail) {
		List<UserStatus> userStatus = new ArrayList<UserStatus>();
		for(int i = 0; i < bodyParts.size(); i++) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
			
			String fileName = bodyParts.get(i).getContentDisposition().getFileName();
			String extension = fileName.substring(fileName.lastIndexOf("."), fileName.length());
			String hashFileName = UUID.randomUUID().toString() + extension;
			
			try {
				java.nio.file.Path path = FileSystems.getDefault().getPath(TRACK_TACE_ROOT_FOLDER + hashFileName);
				Files.copy(bodyPartEntity.getInputStream(), path);
				
				UserStatus sts = new UserStatus();
				sts.setStatus(Response.Status.OK.getStatusCode());
				sts.setMessage(Response.Status.OK.getReasonPhrase());
				sts.setFileId(hashFileName);
				
				userStatus.add(sts);
				log.info("Track and Trace saving file: " + hashFileName);
			} catch (IOException e) {
				log.error(e + " Track and Trace saving file: " + hashFileName, e);
			}
		}
		
		return userStatus;
	}
	
	@GET
	@Path("list") // http://localhost:8080/market-api/files/list
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getFileNames(@QueryParam("activityId") int id) {
		String jsonData = "";
		String sql = "SELECT Document_Id AS id, Relative_Path AS path, Create_Dt AS create_date, Activity_ID AS activity_id FROM document WHERE Activity_ID = ?";
		
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);
			
			JsonArrayBuilder jsonArray = Json.createArrayBuilder();
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				JsonObjectBuilder jsonObject = Json.createObjectBuilder();
				jsonObject.add("id", rs.getInt("id"));
				jsonObject.add("path", rs.getString("path"));
				jsonObject.add("create_date", rs.getString("create_date"));
				jsonObject.add("activity_id", rs.getInt("activity_id"));
				
				jsonArray.add(jsonObject.build());
			}
			
	        StringWriter stringWriter = new StringWriter();
	        JsonWriter writer = Json.createWriter(stringWriter);
	        writer.writeArray(jsonArray.build());
	        writer.close();
	        
	        jsonData = stringWriter.getBuffer().toString();
			
		} catch (Exception e) {
			log.info("error", e);
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return Response.ok(jsonData).build();
	}
	
	@GET
    @Path("/download/zip/{activityId}")
    @Produces("application/zip")
    public Response downloadZippedFile(@PathParam("activityId") Integer id) {
    	
    	List<String> srcFiles = new ArrayList<String>();
    	String sql = "SELECT Document_Id AS id, Relative_Path AS path, Create_Dt AS create_date, Activity_ID AS activity_id FROM document WHERE Activity_ID = ?";
		
		String zippedFileName = "ProofOfDelivery-" + id + ".zip";

		File zippedFile = null;
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		FileInputStream fis = null;
    	
    	try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, id);
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				srcFiles.add(rs.getString("path"));
			}
			
			zippedFile = new File(TRACK_TACE_ROOT_FOLDER + zippedFileName);
			if(zippedFile.exists()) {
				zippedFile.delete();
				zippedFile = new File(TRACK_TACE_ROOT_FOLDER + zippedFileName);
			}

			fos = new FileOutputStream(TRACK_TACE_ROOT_FOLDER + zippedFileName);
			zos = new ZipOutputStream(fos);
			
			for (int i = 0; i < srcFiles.size(); i++) {

				File srcFile = new File(TRACK_TACE_ROOT_FOLDER + srcFiles.get(i));
				fis = new FileInputStream(srcFile);
				
				zos.putNextEntry(new ZipEntry(srcFile.getName()));

				byte[] buffer = new byte[(int) srcFile.length()];
				int length;
				while ((length = fis.read(buffer)) >= 0) {
					zos.write(buffer, 0, length);
				}
			}
			
		    ResponseBuilder responseBuilder = Response.ok((Object) zippedFile, MediaType.APPLICATION_OCTET_STREAM);
	        responseBuilder.header("Content-Disposition", "attachment; filename=\"" +  zippedFileName + "\"");
	        
	        return responseBuilder.build();
	        
    	} catch (FileNotFoundException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} catch (Exception e) {
			log.error(e);
		} finally {
			try {
				if(fis != null) {
					fis.close();
				}
				
				if(zos != null) {
					zos.closeEntry();
					zos.close();
				}

				if(fos != null) {
					fos.close();
				}
				
				if(zippedFile != null) {
					new Thread() {
						public void run() {
							try {
								Thread.sleep(60000);
							} catch (InterruptedException e) {
								log.error(e);
							}
							
							File fileDelete = new File(TRACK_TACE_ROOT_FOLDER + zippedFileName);
							if(fileDelete.exists()) {
								fileDelete.delete();
							}
						}
					}.start();
				}
			} catch (IOException e) {
				log.error(e);
			}
		}
    	
		return Response.status(Status.CONFLICT).build();
	}

}
