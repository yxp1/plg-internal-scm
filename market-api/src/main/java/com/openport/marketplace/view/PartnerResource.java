package com.openport.marketplace.view;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.model.GoodsValueBillToParty;
import com.openport.marketplace.repository.PartnerRepository;


@Path("partner")
public class PartnerResource {

	static final transient Logger log = Logger.getLogger(PartnerResource.class);
	
	@GET
	@Path("getLogoPath") // http://localhost:8080/market-api/partner/getLogoPath
	public String getLogoPath(@QueryParam("accountCode") String accountCode) {
		log.info("Getting logo for " + accountCode);
		
		String url = null;
		try {
			PartnerRepository partnerRepository = new PartnerRepository();
			url = partnerRepository.getLogoPath(accountCode);
		} catch (SQLTimeoutException e) {
			new ServiceException(Response.Status.GATEWAY_TIMEOUT);
		} catch (SQLFeatureNotSupportedException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (JsonParseException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (JsonMappingException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (SQLException e) {
			new ServiceException(Response.Status.BAD_GATEWAY);
		} catch (IOException e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		} catch (Exception e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
		
		return url;
	}
	
	@GET
	@Path("getGVBillTo") // http://localhost:8080/market-api/partner/getGVBillTo
	public List<GoodsValueBillToParty> getGoodsValueBillToPartyList(@QueryParam("accountCode") String accountCode) {
		log.info("Getting logo for " + accountCode);
		
		if(StringUtils.isEmpty(accountCode)) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
		
		List<GoodsValueBillToParty> partners = new ArrayList<>();
		
		try {
			PartnerRepository partnerRepository = new PartnerRepository();
			partners = partnerRepository.getGoodsValueBillToList(accountCode);
		} catch (Exception e) {
			new ServiceException(Response.Status.BAD_REQUEST);
		}
		
		return partners;
	}
}
