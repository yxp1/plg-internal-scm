package com.openport.marketplace.view;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.openport.marketplace.helper.DataResultHelper;
import com.openport.marketplace.helper.ExportRatesHelper;
import com.openport.marketplace.repository.BidRepository;
import com.openport.marketplace.repository.ExportRateRepository;
import com.openport.wb.mp.xmlmapping.XmlMappingParser;
import com.openport.wb.writer.ExcelWriter;

@Path("export")
public class ExportRateResource {
	
	static final transient Logger log = Logger.getLogger(ExportRateResource.class);

	@POST
	@Path("/contractRatesRequest")
	@Produces({MediaType.APPLICATION_OCTET_STREAM})
	public Response exportRequestRates(List<String>tenderIds, @Context UriInfo ui){
		
		ExportRateRepository rpo = new ExportRateRepository();
		try{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			List<Map<String,Object>> clusterData = rpo.executeExportRatesQuery(tenderIds);
			ExcelWriter ew = new ExcelWriter( new XmlMappingParser(ExportRatesHelper.getTemplatePath(ui),
					"StandardTemplateRateSchedule.xml"));
			ew.getExcelTemplateMapping().getSheetAt(0).getClusterAt(0).setClusterDataList(clusterData);
			ew.writeData(baos);
			
			String filename = ExportRatesHelper.generateName("contract_rates_workarea",
					ew.getExcelTemplateMapping().getSourceFileExtension());
			System.out.println("Exported data to: "+filename);
			 return Response.ok(baos.toByteArray(), MediaType.APPLICATION_OCTET_STREAM)
		                .header("content-disposition", "attachment;filename=\"" + filename + "\"").build();

		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@POST
	@Path("/biddingTenderLaneRatesShipper")
	@Produces({MediaType.APPLICATION_OCTET_STREAM})
	public Response exportBiddingTenderLaneRatesShipper(@HeaderParam("x-openport-token") String token,
			List<String>tenderIds, @Context UriInfo ui){
		try{
			BidRepository bidRpo = new BidRepository();
			Map<String,Object> userInfo = bidRpo.getUserInfo(token);
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			
			Integer companyId = (Integer)userInfo.get("Company_ID");
			List<Map<String,Object>> clusterData = bidRpo.getContractsByTenderLaneIdByCompany(companyId,tenderIds);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ExcelWriter ew = new ExcelWriter( new XmlMappingParser(ExportRatesHelper.getTemplatePath(ui),
					"BiddingTemplateRateMapping.xml"));
			ew.getExcelTemplateMapping().getSheetAt(0).getClusterAt(0).setClusterDataList(clusterData);
			ew.writeData(baos);
			
			String filename = ExportRatesHelper.generateName("contract_bidding_rates",
					ew.getExcelTemplateMapping().getSourceFileExtension());
			System.out.println("Exported data to: "+filename);
    		return Response.ok(baos.toByteArray(), MediaType.APPLICATION_OCTET_STREAM)
		                .header("content-disposition", "attachment;filename=\"" + filename + "\"").build();

		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@POST
	@Path("/biddingTenderLaneRates")
	@Produces({MediaType.APPLICATION_OCTET_STREAM})
	public Response exportBiddingTenderLaneRates(@HeaderParam("x-openport-token") String token,
			List<String>tenderIds, @Context UriInfo ui){
		try{
			BidRepository bidRpo = new BidRepository();
			Map<String,Object> userInfo = bidRpo.getUserInfo(token);
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			
			Integer companyId = (Integer)userInfo.get("Company_ID");
			Boolean isShipper = (Boolean)userInfo.get("Is_Shipper");
			Boolean isCarrer = (Boolean)userInfo.get("Is_Carrier");
			List<Map<String,Object>> clusterData = bidRpo.getContractsByTenderLaneId(companyId,tenderIds,isShipper,isCarrer);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ExcelWriter ew = new ExcelWriter( new XmlMappingParser(ExportRatesHelper.getTemplatePath(ui),
					"BiddingTemplateRateMapping.xml"));
			ew.getExcelTemplateMapping().getSheetAt(0).getClusterAt(0).setClusterDataList(clusterData);
			ew.writeData(baos);
			
			String filename = ExportRatesHelper.generateName("contract_bidding_rates",
					ew.getExcelTemplateMapping().getSourceFileExtension());
			//System.out.println("Exported data to: "+filename);
    		return Response.ok(baos.toByteArray(), MediaType.APPLICATION_OCTET_STREAM)
		                .header("content-disposition", "attachment;filename=\"" + filename + "\"").build();

		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	@GET
	@Path("/biddingTenderLaneRates")
	@Produces({MediaType.APPLICATION_OCTET_STREAM})
	public Response getBiddingTenderLaneRates(@QueryParam("token") String token,
			@QueryParam ("tenderIds") String tenderIdsConcat, @Context UriInfo ui){
		try{
			List<String>tenderIds =  Arrays.asList(tenderIdsConcat.split(","));
			
			BidRepository bidRpo = new BidRepository();
			Map<String,Object> userInfo = bidRpo.getUserInfo(token);
			
			if(tenderIds.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "There are no tender ids to fetch")).build();
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			
			Integer companyId = (Integer)userInfo.get("Company_ID");
			Boolean isShipper = (Boolean)userInfo.get("Is_Shipper");
			Boolean isCarrer = (Boolean)userInfo.get("Is_Carrier");
			List<Map<String,Object>> clusterData = bidRpo.getContractsByTenderLaneId(companyId,tenderIds,isShipper,isCarrer);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ExcelWriter ew = new ExcelWriter( new XmlMappingParser(ExportRatesHelper.getTemplatePath(ui),
					"BiddingTemplateRateMapping.xml"));
			ew.getExcelTemplateMapping().getSheetAt(0).getClusterAt(0).setClusterDataList(clusterData);
			ew.writeData(baos);
			
			String filename = ExportRatesHelper.generateName("contract_bidding_rates",
					ew.getExcelTemplateMapping().getSourceFileExtension());
			//System.out.println("Exported data to: "+filename);
    		return Response.ok(baos.toByteArray(), MediaType.APPLICATION_OCTET_STREAM)
		                .header("content-disposition", "attachment;filename=\"" + filename + "\"").build();

		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	@GET
	@Path("/contractTenderLanes")
	@Produces({MediaType.APPLICATION_OCTET_STREAM})
	public Response getContractTenderLanes(@QueryParam("token") String token,
			@QueryParam ("tenderIds") String tenderIdsConcat, @Context UriInfo ui){
		try{
			List<String>tenderIds =  Arrays.asList(tenderIdsConcat.split(","));
			
			BidRepository bidRpo = new BidRepository();
			Map<String,Object> userInfo = bidRpo.getUserInfo(token);
			
			if(tenderIds.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "There are no tender ids to fetch")).build();
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			
			Integer companyId = (Integer)userInfo.get("Company_ID");
			Boolean isShipper = (Boolean)userInfo.get("Is_Shipper");
			Boolean isCarrer = (Boolean)userInfo.get("Is_Carrier");
			List<Map<String,Object>> clusterData = bidRpo.getContractTendersByTenderLaneId(companyId,tenderIds,isShipper,isCarrer);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ExcelWriter ew = new ExcelWriter( new XmlMappingParser(ExportRatesHelper.getTemplatePath(ui),
					"TenderTemplateRateMapping.xml"));
			ew.getExcelTemplateMapping().getSheetAt(0).getClusterAt(0).setClusterDataList(clusterData);
			ew.writeData(baos);
			
			String filename = ExportRatesHelper.generateName("contract_tender_lanes",
					ew.getExcelTemplateMapping().getSourceFileExtension());
			//System.out.println("Exported data to: "+filename);
    		return Response.ok(baos.toByteArray(), MediaType.APPLICATION_OCTET_STREAM)
		                .header("content-disposition", "attachment;filename=\"" + filename + "\"").build();

		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
}
