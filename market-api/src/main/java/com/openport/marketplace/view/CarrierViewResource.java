package com.openport.marketplace.view;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.VwCarrierProfile;
import com.openport.marketplace.repository.DbConfigMarketPlace;


@Path("carrierView") // http://localhost:8080/market-api/carrierView/IDTTUM
public class CarrierViewResource {
	static final transient Logger log = Logger.getLogger(CarrierViewResource.class);

	@GET // http://localhost:8080/market-api/carrierView/IDTTUM
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{carrierCode}")
	public List<VwCarrierProfile> getCarrierView(@PathParam("carrierCode") String carrierCode) {
		
		List<VwCarrierProfile> carrierProfileRecord = new ArrayList<VwCarrierProfile>();
		
		try {
			String sql = "SELECT * FROM `vw_carrier_profile` WHERE company_code = ?;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setString(1, carrierCode);

		         try (ResultSet rs = pstmt.executeQuery()) {
					if (rs.next()) {
						com.openport.marketplace.model.VwCarrierProfile svc = new com.openport.marketplace.model.VwCarrierProfile(rs);
						VwCarrierProfile csr = new VwCarrierProfile();
						csr.setCompanyName(svc.getCompanyName());
						csr.setCompanyCode(svc.getCompanyCode());
						csr.setCompanyAddress(svc.getCompanyAddress());
						csr.setHqCity(svc.getHqCity());
						csr.setHqProvince(svc.getHqProvince());
						csr.setHqCountry(svc.getHqCountry());
						csr.setTruckPoolAddress(svc.getTruckPoolAddress());
						csr.setBranchAddress(svc.getBranchAddress());
						csr.setContactName(svc.getContactName());
						csr.setEmailAddress(svc.getEmailAddress());
						csr.setRating(svc.getRating());
						
						
						carrierProfileRecord.add(csr);
			        }
					return carrierProfileRecord;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return carrierProfileRecord;
	}	
	
	
}
