package com.openport.marketplace.view;

import java.io.InputStream;
import java.sql.Date;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.json.JsonArrayBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.openport.marketplace.helper.DataResultHelper;
import com.openport.marketplace.helper.ExportRatesHelper;
import com.openport.marketplace.json.CompanyContractTenderLane;
import com.openport.marketplace.repository.BidRepository;
import com.openport.wb.contants.WorkBookConstants;
import com.openport.wb.mp.xmlmapping.XmlMappingParser;
import com.openport.wb.reader.ExcelReader;
import com.openport.wb.reader.IExcelReader;
import com.openport.wb.writer.ExcelWriter;

@Path("bid")
public class BidResource {
	
	static final transient Logger log = Logger.getLogger(BidResource.class);
	@GET
	@Path("rpu")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getRatePerUnit(){
		log.info("---  BEGIN REQUEST ---");
		try{
			BidRepository rpo = new BidRepository();
			List<Map<String,Object>> dataResult = rpo.getRatePerUnit();
			JsonArrayBuilder JSONresponse = DataResultHelper.parseDateResultToJSON(dataResult);
			return Response.ok(JSONresponse.build().toString()).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.info("---  END REQUEST ---");
		}
	}
	
	@GET
	@Path("shippers")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getShippers(@HeaderParam("x-openport-token") String token){
		log.info("---  BEGIN REQUEST ---");
		try{

			BidRepository rpo = new BidRepository();
			Map<String,Object> userInfo = rpo.getUserInfo(token);
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			
			Integer userCompanyId = (Integer)userInfo.get("Company_ID");
			List<Map<String,Object>> dataResult = rpo.getShippers2(userCompanyId);
			JsonArrayBuilder JSONresponse = DataResultHelper.parseDateResultToJSON(dataResult);
			return Response.ok(JSONresponse.build().toString()).build();
		}catch(Exception e){
			e.printStackTrace();
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.info("---  END REQUEST ---");
		}
	}
	
	@GET
	@Path("contracts")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getContractDetails(@HeaderParam("x-openport-token") String token,
			@QueryParam("status") Integer status,
			@QueryParam("shipperId") Integer shipperId,
			@QueryParam("contractNbr") String contractNbr,
			@QueryParam("from") String from,
			@QueryParam("to") String to
			){
		log.info("---  BEGIN REQUEST ---");
		try{
			BidRepository rpo = new BidRepository();
			Map<String,Object> userInfo = rpo.getUserInfo(token);
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			
			Integer userCompanyId = (Integer)userInfo.get("Company_ID");
			
			if(from != null && to != null){
				if(!DataResultHelper.isValidDate(from) || !DataResultHelper.isValidDate(to)){
					throw new ParseException("Invalid Date format", 0);
				}
			}
			
			List<Map<String,Object>> contracts = rpo.getContracts2(userCompanyId,  status, shipperId, contractNbr, from,to);

			JsonArrayBuilder JSONresponse = DataResultHelper.parseDateResultToJSON(contracts);
			return Response.ok(JSONresponse.build().toString()).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.info("---  END REQUEST ---");
		}
	}
	
	@POST
	@Path("submitTenderLane")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response submitContractItem(@HeaderParam("x-openport-token") String token, 
			List<CompanyContractTenderLane> companyContractTenderLaneList){
		log.info("---  BEGIN REQUEST ---");
		try{
			BidRepository rpo = new BidRepository();
			Map<String,Object> userInfo = rpo.getUserInfo(token);
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			Integer userId = (Integer)userInfo.get("User_ID");
			
			for(CompanyContractTenderLane companyContractTenderLane : companyContractTenderLaneList){
				rpo.submitTenderLaneRate(companyContractTenderLane,userId);
			}

			return Response.ok(DataResultHelper.createResponseJsonObject(1,"submit item successful")).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.info("---  END REQUEST ---");
		}
	}
	
	@POST
	@Path("editTenderLane")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response editContractItem(@HeaderParam("x-openport-token") String token, 
			CompanyContractTenderLane companyContractTenderLane){
		log.info("---  BEGIN REQUEST ---");
		try{
			BidRepository rpo = new BidRepository();
			Map<String,Object> userInfo = rpo.getUserInfo(token);
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			Integer userId = (Integer)userInfo.get("User_ID");
			
			Map<String,Object> tenderLane = rpo.findTenderLaneById(companyContractTenderLane.getCompanyContractTenderLaneId());
			
			if(!tenderLane.isEmpty()){
				String statusCd = (String)tenderLane.get("STATUS_CD");
				if(statusCd.equals("U")){
					rpo.editTenderLaneRate(companyContractTenderLane,userId);
					return Response.ok(DataResultHelper.createResponseJsonObject(1,"submit item successful")).build();
				}else {
					return Response.ok(DataResultHelper.createResponseJsonObject(0,statusCd)).build();
				}
			}	
			return Response.ok(DataResultHelper.createResponseJsonObject(0,"cannot find tender lane")).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.info("---  END REQUEST ---");
		}
	}
	
	@POST
	@Path("removeTenderLane")
	@Produces({MediaType.APPLICATION_JSON})
	public Response removeContractItem(@HeaderParam("x-openport-token") String token, 
			List<String> contractTenderLaneIds){
		log.info("---  BEGIN REQUEST ---");
		try{
			BidRepository rpo = new BidRepository();
			Map<String,Object> userInfo = rpo.getUserInfo(token);
			
			if(userInfo.isEmpty())
				return Response.ok(DataResultHelper.createResponseJsonObject(0, "User token not found")).build();
			Integer userId = (Integer)userInfo.get("User_ID");

			rpo.removeTenderLaneRate(contractTenderLaneIds,userId);
			return Response.ok(DataResultHelper.createResponseJsonObject(1,"removing item successful")).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}finally{
			log.info("---  END REQUEST ---");
		}
	}
	
	@POST
	@Path("/uploadContractBids")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadContractBids(@FormDataParam("file") InputStream is, 
			@Context UriInfo ui) {
		log.info("---  BEGIN REQUEST ---");
		try{
			BidRepository rpo = new BidRepository();
			ExcelReader er = new ExcelReader( new XmlMappingParser(ExportRatesHelper.getTemplatePath(ui),
					"BiddingTemplateRateMapping.xml"));
			List<Map<String,Object>> error = new LinkedList<Map<String,Object>>();
	        List<Map<String,Object>> result = er.read(is);
	        
	        int rowIndex =1;
	        for(Map<String,Object> rowData : result){ 
	        	String contractNbr = String.valueOf(rowData.get("CONTRACT_NBR"));
	        	Double rateAmt = Double.valueOf(String.valueOf(rowData.get("rate_amt")));
	        	
	        	Map<String,Object> tenderLane = rpo.findTenderLaneByContractNbr(contractNbr);
	        	Map<String,Object> errorItem = new LinkedHashMap<String,Object>();
	        	
	        	if(tenderLane.isEmpty()){
	        		errorItem.put("rowIndex", rowIndex);
	        		errorItem.put("contractNbr", contractNbr);
	        		errorItem.put("message", "does not exist");
	        	}else if(!(String.valueOf(tenderLane.get("STATUS_CD")).equals("U") && 
	        				Double.valueOf(String.valueOf(tenderLane.get("RATE_AMT"))) <= 0)){
	        		errorItem.put("rowIndex", rowIndex);
	        		errorItem.put("contractNbr", contractNbr);
        			errorItem.put("message", "needs to be pending");
	        	}else{
	        		rpo.updateBidByContractNbr(contractNbr,rateAmt);
	        	}
	        	
	        	if(!errorItem.isEmpty())
	        		error.add(errorItem);
	        	
	        	rowIndex++;
	        }
	        return Response.ok(DataResultHelper.parseDateResultToJSON(error).build().toString()).build();
		}catch(Exception e){
			log.info("error", e);
			log.error("Error message: " + e, e);
//			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			return Response.ok(DataResultHelper.createResponseJsonObject(0,e.getMessage())).build();
		}finally{
			log.info("---  END REQUEST ---");
		}
	}
}
