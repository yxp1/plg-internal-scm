package com.openport.marketplace.view;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.UserStatus;
import com.openport.marketplace.repository.DbConfigMarketPlace;


@Path("adminView")
public class AdminViewResource {
	static final transient Logger log = Logger.getLogger(AdminViewResource.class);

	@GET // http://localhost:8080/market-api/adminView/rating/OpenPort
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/rating/{companyName}")
	public List<Company> getCompanyRating(@PathParam("companyName") String companyName) {
		
		List<Company> companyList = new ArrayList<Company>();
		
		try {
			String sql = "select * from marketplace_db.Company where Is_Carrier = 1 and Company_Name like '%" + companyName + "%'";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {

		         try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						Company company = new Company();
						company.setCompanyId(rs.getInt("company_id"));
						company.setCompanyName(rs.getString("company_name"));
						company.setCompanyCode(rs.getString("company_code"));
						company.setRatingValue(rs.getInt("rating_value"));
						
						companyList.add(company);
			        }
					return companyList;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return companyList;
	}	
	
	@GET // http://localhost:8080/market-api/adminView/companyNames
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/companyNames")
	public List<Company> getCompanyNames() {
		
		List<Company> companyList = new ArrayList<Company>();
		
		try {
			String sql = "select * from marketplace_db.Company where Is_Carrier = 1";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {

		         try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						Company company = new Company();
						company.setCompanyId(rs.getInt("company_id"));
						company.setCompanyName(rs.getString("company_name"));
						company.setCompanyCode(rs.getString("company_code"));
						
						companyList.add(company);
			        }
					return companyList;
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return companyList;
	}
	
	@POST // http://localhost:8080/market-api/adminView/rating/update?companyCode=OP4PL&ratingValue=2
	@Path("/rating/update")
	public void updateCompanyRating(@FormDataParam("companyCode") String companyCode, @FormDataParam("ratingValue") Integer ratingValue) {
		try {
			String sql = "update marketplace_db.Company set RATING_VALUE = " + ratingValue + " where Is_Carrier = 1 and Company_Code = '" + companyCode + "'";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				
					pstmt.execute();
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}		
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
	}
	
	
}
