package com.openport.marketplace.view;



import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.auth.annotation.NoAuthorization;
import com.openport.marketplace.json.AccessorialCode;
import com.openport.marketplace.json.CarrierBidStats;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.CompanyAddress;
import com.openport.marketplace.json.CompanyBranch;
import com.openport.marketplace.json.CompanyClient;
import com.openport.marketplace.json.CompanyFile;
import com.openport.marketplace.json.Country;
import com.openport.marketplace.json.DocumentType;
import com.openport.marketplace.json.EntityAddress;
import com.openport.marketplace.json.EquipmentType;
import com.openport.marketplace.json.FileDocumentType;
import com.openport.marketplace.json.Ids;
import com.openport.marketplace.json.Label;
import com.openport.marketplace.json.Language;
import com.openport.marketplace.json.LanguagePack;
import com.openport.marketplace.json.PaymentTerm;
import com.openport.marketplace.json.Province;
import com.openport.marketplace.json.Service;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.Shipment;
import com.openport.marketplace.json.ShipmentAccessorial;
import com.openport.marketplace.json.ShipmentBooking;
import com.openport.marketplace.json.ShipmentCarrierBid;
import com.openport.marketplace.json.ShipmentCarrierBidDetail;
import com.openport.marketplace.json.ShipmentDetail;
import com.openport.marketplace.json.ShipmentDetails;
import com.openport.marketplace.json.ShipmentInfo;
import com.openport.marketplace.json.ShipmentReturn;
import com.openport.marketplace.json.ShipmentSearch;
import com.openport.marketplace.json.ShipperStats;
import com.openport.marketplace.json.Status;
import com.openport.marketplace.json.Token;
import com.openport.marketplace.json.UserStatus;
import com.openport.marketplace.json.Vendors;
import com.openport.marketplace.json.VwCarrierProfile;
import com.openport.marketplace.model.Accessorial;
import com.openport.marketplace.repository.DbConfigMarketPlace;
import com.openport.util.db.ResultSetMapper;
import com.openport.util.password.MySqlPassword;

@Path("shippers") // http://localhost:8080/market-api/shippers
public class ShipperResource {

	static final transient Logger log = Logger.getLogger(ShipperResource.class);
	private static final String CURRENT_VERSION = "1.0";
	private static final String CURRENT_UAT_VERSION = "1.0";
	private String sqlShipmentAllInfo = " SELECT "
			+" s.shipment_id, s.shipment_nbr, s.Shipper_Id, "
			+" s.Status_Id, s.assigned_provider_Id, "
			+" s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
			+" s.requested_pickup_dt, s.Requested_Delivery_Dt, "
			+" s.Unit_Count, s.unit_of_measure, "
			+" s.tender_expiration_dt ,  s.payment_term_id ,s.high_end_bid_amt, s.low_end_bid_amt, s.target_rate_amt, s.avg_bid_amt ,s.currency_cd, s.commodity_desc_txt , "
			+" s.create_dt,s.create_User_Id, "
			+" s.origin_service_area_id, "
			+" s.origin_wait_tm, "
			+" s.origin_wait_tm_uom, "
			+" s.parent_partner_cd, "
			+" s.pro_bill_nbr, "
			+" s.tender_type_cd, "
			+" s.update_dt, "
			+" s.update_User_Id, "
			+" s.unit_vol, "
			+" s.unit_wt, "
			+" s.dest_wait_tm, "
			+" s.dest_wait_tm_uom  , "
			+" s.shipment_type_cd  , "
			+" s.stops_cnt  , "
			+" s.destination_service_area_id  , "
			+" p.entity_address_id  as pickup_entity_address_id  , "
			+" p.entity_name  as pickup_entity_name  , "
			+" p.floor_apartment  as pickup_floor_apartment  , "
			+" p.building  as pickup_building  , "
			+" p.house_number  as pickup_house_number  , "
			+" p.line1  as pickup_line1  , "
			+" p.street_name  as pickup_street_name  , "
			+" p.city_locality  as pickup_city_locality  , "
			+" p.province  as pickup_province  , "
			+" p.country  as pickup_country  , "
			+" p.postal_code  as pickup_postal_code  , "
			+" p.district_name  as pickup_district_name  , "
			+" p.is_Dc as pickup_is_Dc, "
			+" p.lat_Long_Status_Cd as pickup_lat_Long_Status_Cd, "
			+" p.latitude as pickup_latitude, "
			+" p.longitude as pickup_longitude, "
			+" d.entity_address_id  as delivery_entity_address_id  , "
			+" d.entity_name  as delivery_entity_name  , "
			+" d.floor_apartment  as delivery_floor_apartment  , "
			+" d.building  as delivery_building  , "
			+" d.house_number  as delivery_house_number  , "
			+" d.line1  as delivery_line1  , "
			+" d.street_name  as delivery_street_name  , "
			+" d.city_locality  as delivery_city_locality  , "
			+" d.province  as delivery_province  , "
			+" d.country  as delivery_country  , "
			+" d.postal_code  as delivery_postal_code  , "
			+" d.district_name  as delivery_district_name  , "
			+" d.is_Dc as delivery_is_Dc, "
			+" d.lat_Long_Status_Cd as delivery_lat_Long_Status_Cd, "
			+" d.latitude as delivery_latitude, "
			+" d.longitude as delivery_longitude, "
			+" e.Equipment_Type_ID, e.Equipment_Type_Name, "
			+" a.ACCESSORIAL_DESCRIPTION, a.ACCESSORIAL_CD, a.ACCESSORIAL_TYPE, "
			+" a.ACCESSORIAL_AMT , "
			+" t.Payment_Term_ID, t.Payment_Term_Cd, t.Payment_Term_Name,    "
			+" sd.shipment_detail_id,   "
			+" sd.qty, sd.unit_of_measure as unit_of_measure_dtl, "
			+" sd.shipment_wt, sd.wt_uom, sd.shipment_vol, sd.vol_uom,   "
			+" sd.shipment_len, sd.shipment_wth, sd.shipment_ht, "
			+" sd.dimension_uom, sd.freight_class_cd, sd.is_hazardous, "
			+" sd.product_desc_txt, "
			+" sd.stop_nbr ,"
			+" sd.stop_location_name ,"
			+" sd.stop_location_addr ,"
			+" sd.stop_location_city_name ,"
			+" sd.stop_location_province_name ,"
			+" sd.stop_location_country_cd ,"
			+" sd.stop_location_postal_cd ,"
			+" sd.stop_po_Nbr ,"
			+" sd.stop_release_nbr ,"
			+" coalesce(b.shipment_carrier_bid_id,-1) as shipment_carrier_bid_id, b.carrier_id,"
			+" b.quote_status_cd, b.rate_type_cd, b.rate_type_desc_txt, b.transit_tm, b.equipment_type_id, "
			+" b.quote_amt,   "
			+" b.quote_currency_cd, b.quote_datestamp, b.quote_expiration_value, b.quote_expiration_uom, b.comment_txt, "
			+" coalesce(c.company_name,'') as carrier_name , "
			+" coalesce(c.company_code,'') as carrier_cd , "
			+" coalesce( c.rating_value,0) as carrier_rating , "
			+" IF( coalesce(s.tender_expiration_dt,'2000-01-01') < now(), 'Y','N') as bid_ended , "
			+" coalesce( b.bid_rank,0) as bid_rank, "
			+" b.carrier_id as carrier_id_bid_detail, "
			+" sa.accessorial_description as accessorial_description_bid_detail, "
			+" sa.accessorial_cd as accessorial_cd_bid_detail, "
			+" sa.accessorial_type as accessorial_type_bid_detail, "
			+" sa.accessorial_data_source as accessorial_data_source_bid_detail, "
			+" 0 as accessorial_amt_bid_detail, "
			+" '' as accessorial_rate_comment_bid_detail, "
			+" 1 as shipper_defined_flg "
			+" FROM shipment s  "
			+" LEFT JOIN shipment_carrier_bid b on s.shipment_id=b.shipment_id "
			+" LEFT JOIN (select * from shipment_accessorial)  sa on sa.shipment_id=s.shipment_id "
			+" LEFT JOIN company c on c.company_id=b.carrier_id  "
			+" LEFT JOIN equipment_type e on e.equipment_type_id=s.requested_equipment_type_id "
			+" LEFT JOIN shipment_accessorial a on  a.shipment_id=s.shipment_id "
			+" LEFT JOIN Payment_Term t on  t.payment_term_id=s.payment_term_id "
			+" LEFT JOIN shipment_detail sd on sd.shipment_id=s.shipment_id "
			+" JOIN (select * from entity_address) as  p ON p.entity_address_id = s.pickup_address_id "
			+" JOIN (select * from entity_address) as d ON d.entity_address_id = s.delivery_address_id ";

	@GET
	@Path("test") // // http://localhost:8080/delivery/shipment/test
	//@Consumes("application/json") // MediaType.APPLICATION_JSON
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus testKeyValuePairArray() {
		UserStatus sts = new UserStatus();
		sts.setStatus(1);
		System.out.println("good");
		return sts;
	}
	
	@POST
	@Path("logoupld")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public FileDocumentType upsertFileDocumentType(FileDocumentType fdt){
		Integer companyId=0;
		try {
			companyId=fdt.getCompanyFile().getCompanyId();
			CompanyFile cf = fdt.getCompanyFile();
			int iRslt = doUpdateCompanyFileRelativePath(cf);

			if(iRslt<1){
				DocumentType dt = fdt.getDocumentType();
				if (cf.getCompanyFileId() == -1) {
					cf = insertCompanyFile(cf);
				} else {
					cf = updateCompanyFile(cf);
				}

				if (fdt.getFileDocumentTypeId() == null || fdt.getFileDocumentTypeId() == -1) {
					// new FileDocumentType
					insertFileDocumentType(fdt, dt, cf);

				} else {
					fdt.setCompanyFile(cf);
					fdt.setDocumentType(dt);
				}
			}
			

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return fdt;
	}
	
	
	private int doUpdateCompanyFileRelativePath(CompanyFile cf) {
		int iRslt = 0;
		String sql = "UPDATE "+ 
				" company_file cf "+ 
				" join file_document_type fdt on  cf.company_file_id=fdt.company_file_id "+ 
				" join document_type dt on dt.document_type_id = fdt.document_type_id "+
				" set cf.relative_path = ? , "+
				" cf.document_name = ?  "+
				" where  "+
				" cf.company_id = ? and "+
				" dt.document_type_id=0 ";

 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
 				pstmt.setString(1, cf.getRelativePath());
 				pstmt.setString(2, cf.getDocumentName());
 				pstmt.setInt(3, cf.getCompanyId());
 				iRslt = pstmt.executeUpdate();

			
	 			return iRslt;
				
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
 		return iRslt;
	}


	@GET
	@Path("lang/{lang}/{langVer}/{appVersion}/{appId}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus lang(@PathParam("lang") String lang
			,@PathParam("langVer") int langVer
			,@PathParam("appVersion") String appVersion
			,@PathParam("appId") String appId
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
			sts = checkLanguage(sts, langVer);
			checkVersion(lang, appVersion, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	

	
	@GET
	@Path("languages")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus lang() {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
	    	
	    	List<Language> languageList = new ArrayList();
	    	languageList = doGetLanguageList();
	    	sts.setLanguages(languageList);
	    	
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	
	
	
	@GET
	@Path("labels/{langCd}/{langVer}/{appVersion}/{appId}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus getLabelsApp(@PathParam("langCd") String lang
			,@PathParam("langVer") int langVer
			, @PathParam("appVersion") String appVersion, @PathParam("appId") String appId
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
			sts = checkLanguage(sts, langVer);
			checkVersion(lang, appVersion, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	
	@GET
	@Path("labels/{langCd}/{langVer}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus getLabels(@PathParam("langCd") String lang
			,@PathParam("langVer") int langVer
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
			sts = checkLanguage(sts, langVer);
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	
	@GET
	@Path("lang/{lang}/{langVer}")// http://localhost:8080/market-api/shippers/lang
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus getLangLabel(@PathParam("lang") String lang
			,@PathParam("langVer") int langVer
			) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
			sts = checkLanguage(sts, langVer);
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	
	
	
	@GET
	@Path("labels/{langVer}") // http://localhost:8080/market-api/shippers/labels/zh/123/4.5.0/com.openport.delivery.uat
	// @Path("lang/{langVer}/{lang}/{version}/{appId}") // // http://localhost:8080/delivery/user/lang/com.openport.delivery
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus lang(@PathParam("langVer") int langVer) {
    	UserStatus sts = new UserStatus();
		Date startDt = new Date();
		log.info("start");
		try {
	    	sts.setRefreshSeconds(null);
			sts = checkLanguage(sts, langVer);
			//checkVersion(lang, version, sts, appId);
		} finally {
			/* 
			double seconds = getElapsedTime(startDt);
			if (seconds>3.0) {
				log.error("Done - slow API:"+seconds+" seconds for langVer#"+langVer);
			} else {
				log.info("Done in "+seconds+" seconds for langVer#"+langVer);
			}
			*/
		}
		return sts;
    }
	


	@GET // http://localhost:8080/market-api/serviceAreas/carrierView/84
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/carrierdetail/{carrierCode}")
	public VwCarrierProfile getCarrierView(@PathParam("carrierCode") Integer companyId) {
		
		// List<VwCarrierProfile> carrierProfileRecord = new ArrayList<VwCarrierProfile>();
		
		VwCarrierProfile carrierProfile = new VwCarrierProfile();
		
		try {
			String sql = "SELECT * FROM `vw_carrier_profile2` WHERE company_id = ?;";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setInt(1, companyId);
				//log.info("getCarrierView: carrierCode:"+companyId);
				
				try (ResultSet rs = pstmt.executeQuery()) {
					 if (rs.next()) {
						com.openport.marketplace.model.VwCarrierProfile svc = new com.openport.marketplace.model.VwCarrierProfile(rs);
						
						//log.info("getCarrierView: carrierCode:"+companyId);
						carrierProfile.setCompanyId(companyId);	
						carrierProfile.setCompanyName(svc.getCompanyName());
						carrierProfile.setCompanyCode(svc.getCompanyCode());
						
						carrierProfile.setBusinessHeadline(svc.getBusinessHeadline());
						carrierProfile.setBusinessDescription(svc.getBusinessDescription());
						
						carrierProfile.setCompanyAddress(svc.getCompanyAddress());
						carrierProfile.setHqProvince(svc.getHqProvince());
						carrierProfile.setHqCountry(svc.getHqCountry());
						carrierProfile.setTruckPoolAddress(svc.getTruckPoolAddress());
						carrierProfile.setBranchAddress(svc.getBranchAddress());
						carrierProfile.setContactName(svc.getContactName());
						carrierProfile.setEmailAddress(svc.getEmailAddress());
						carrierProfile.setPhoneNbr(svc.getPhoneNbr());
						carrierProfile.setYearsOfOperation(svc.getYearsOfOperation());
						carrierProfile.setCompanyLogoPath(svc.getCompanyLogoPath());
						
						//carrierProfileRecord.add(json);
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.CONFLICT);
			//new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		//log.info("getCarrierView: carrierCode:"+carrierProfile);
		carrierProfile.setCertifications("");
		try {
			DocumentType documentType = doGetCompanyCertification(companyId);
			if((documentType!=null) && (documentType.getDescriptionTxt()!=null)){
				carrierProfile.setCertifications(documentType.getDescriptionTxt());
			}else{
				carrierProfile.setCertifications("");
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//log.info("getCertifications:"+carrierProfile.getCertifications());
		/*
		carrierProfile.setCompanyEquipmentList(getCarrierEquipmentList(companyId));
		ArrayList<com.openport.marketplace.carrierProfile.CompanyEquipmentList> jEquipmentList = carrierProfile.getCompanyEquipmentList();
		if (jEquipmentList == null) {
			log.info("jEquipmentList is NULL");
		} else {
			log.info("jEquipmentList size:"+jEquipmentList.size());
			com.openport.marketplace.carrierProfile.CompanyEquipmentList c = new com.openport.marketplace.carrierProfile.CompanyEquipmentList();
			for (int i = 0; i < jEquipmentList.size(); i++) {
				c =  (com.openport.marketplace.carrierProfile.CompanyEquipmentList)jEquipmentList.get(i);
				log.info("getCarrierView:"+c.getEquipmentTypeName());
			}
		}
	
		carrierProfile.setCarrierPicturesList(getCompanyFilePhotos(companyId));
		carrierProfile.setCarrierServiceAreasList(getCarrierServiceAreas(companyId));
		carrierProfile.setCarrierUsersList(getCarrierUsers(companyId));
		List<com.openport.marketplace.carrierProfile.User> jUserList = carrierProfile.getCarrierUsersList();
		if (jUserList == null) {
			log.info("jUserList is NULL");
		} else {
			log.info("jUserList size:"+jUserList.size());
			com.openport.marketplace.carrierProfile.User c = new com.openport.marketplace.carrierProfile.User();
			for (int i = 0; i < jUserList.size(); i++) {
				c =  (com.openport.marketplace.carrierProfile.User)jUserList.get(i);
				log.info("getCarrierView Users:"+c.getEmailAddress()+"/"+c.getUsername()+"/"+c.getIsPrimaryContact()+"/"+c.getIsTechnicalContact());
			}
		}		
		
		carrierProfileRecord.add(carrierProfile);
		return carrierProfileRecord.get(0);		
		*/
		//return null;
		//log.info("END");
		return carrierProfile;
	}
	
	@PUT  // http://localhost:8080/market-api/shippers
 	@Path("updateCompanyCorporateAddressId")  // http://localhost:8080/market-api/shippers/upsertcompany
 	@Consumes("application/json,application/vnd.openport.market.v1+json")
 	@Produces({ MediaType.APPLICATION_JSON })
 	public Status updateCorporateAddressId(Ids id) {
		try {
			String sql = "update company set corporate_address_id ="+id.getCorporateAddressId() + 
					" WHERE  company_id =  "+id.getCompanyId();
					
			log.info("sql " + sql);
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
			int iRslt = pstmt.executeUpdate();
			Status s = new Status();
			s.setStatusCd("200");
			return s;
				} catch (Exception e) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
	  return null;
	}
	
	@POST  // http://localhost:8080/market-api/shippers
 	@Path("insertcarrierbid")  // http://localhost:8080/market-api/shippers/upsertcompany
 	@Consumes("application/json,application/vnd.openport.market.v1+json")
 	@Produces({ MediaType.APPLICATION_JSON })
 	public ShipmentCarrierBid upsertCarrierBid(ShipmentCarrierBid shipmentCarrierBid) {
 		UserStatus sts = new UserStatus();
 		try {
 			
 			boolean isSuccess = true;
 			
 			String commentTxt = "";
 			
 			if (shipmentCarrierBid.getCommentTxt()!=null){
 	 			commentTxt = shipmentCarrierBid.getCommentTxt().replace("'", "''");
 	 			shipmentCarrierBid.setCommentTxt(commentTxt);
 			}
 			
 			List<ShipmentCarrierBidDetail> shipmentCarrierBidDetailList = shipmentCarrierBid.getShipmentCarrierBidDetails();
 			System.out.println("shipmentCarrierBidDetailList size:"+shipmentCarrierBidDetailList.size());
 			
 			//int shipmentCarrierBidId = doUpsertShipmentCarrierBidObject(shipmentCarrierBid);
 			ShipmentCarrierBid shipmentCarrierBidObj = doUpsertShipmentCarrierBidObject(shipmentCarrierBid);
 			int shipmentCarrierBidId = -1;
 			if(shipmentCarrierBidObj!=null){
 				shipmentCarrierBidId = shipmentCarrierBidObj.getShipmentCarrierBidId();
 	 			shipmentCarrierBid.setShipmentCarrierBidId(shipmentCarrierBidId);

 			}
 			System.out.println("shipmentCarrierBidId:"+shipmentCarrierBidId);
 			
 			if(shipmentCarrierBidId<0){
 				System.out.println("shipmentCarrierBidId is <0:"+shipmentCarrierBidId);
 				new ServiceException(Response.Status.BAD_REQUEST);
 			}else{
 				doDeleteShipmentCarrierBidDetails(shipmentCarrierBidId,shipmentCarrierBid.getCarrierId());
 				doUpsertShipmentCarrierBidDetailObject(shipmentCarrierBid.getShipmentCarrierBidDetails(),
 						shipmentCarrierBidObj.getShipmentId(),
 						shipmentCarrierBidId,
 						shipmentCarrierBid.getCarrierId(),
 						shipmentCarrierBid.getQuoteCurrencyCd());
 			}
 			//new ServiceException(Response.Status.OK);
 		 		
 		} catch (Exception e1) {
 			log.error("Error message: " + e1, e1);
 			new ServiceException(Response.Status.BAD_REQUEST);
 			//new ServiceException(Response.Status.BAD_GATEWAY);
 		}
 		
 		return shipmentCarrierBid;
 	}
	
	
	
	private void doDeleteShipmentCarrierBidDetails(int shipmentCarrierBidId, int carrierId) {
			try {
				String sql = "DELETE FROM SHIPMENT_CARRIER_BID_DETAIL "+
						" WHERE  shipment_carrier_bid_id = ? "+
						" and carrier_id = ?";
			
				try (
						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql);) 
					{
				pstmt.setInt(1, shipmentCarrierBidId);
				pstmt.setInt(2, carrierId);
				int iRslt = pstmt.executeUpdate();
				
				if(iRslt<1){
					log.error("Faild to delete SHIPMENT_CARRIER_BID_DETAIL shipment_carrier_bid_id:"+shipmentCarrierBidId+" carrier_id:"+carrierId);
				}
				
				} catch (Exception e) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
	}



	private void doUpsertShipmentCarrierBidDetailObject(List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails,
					int shipmentId,
					int shipmentCarrierBidId,  
					int carrierId, 
					String currencyCd) {
		try {
			for(ShipmentCarrierBidDetail bidDetail: shipmentCarrierBidDetails){
				try {
					
					String sql = "call sp_carrier_bid_detail_insert( "
										+"'"+shipmentId+"',"
										+"'"+currencyCd+"',"
										+"'"+bidDetail.getAccessorialCd()+"',"
										+"'"+bidDetail.getAccessorialDataSource()+"',"
										+"'"+bidDetail.getAccessorialDescription()+"',"
										+"'"+bidDetail.getAccessorialRateComment()+"',"
										+"'"+bidDetail.getAccessorialType()+"',"
										+"'"+bidDetail.getAccessorialAmt()+"',"
										+"'"+carrierId+"',"
										+"'"+shipmentCarrierBidId+"'"
										+")";
					try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
							PreparedStatement pstmt = connection.prepareStatement(sql);) {
							System.out.println("sql:"+sql);
							pstmt.executeUpdate();
					} catch (Exception e) {
						
						log.error("Error message: " + e, e);
						new ServiceException(Response.Status.CONFLICT);
					}
			 	} catch (Exception e) {
			 		log.error(e,e);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
			
	}



	@POST  // http://localhost:8080/market-api/shippers
 	@Path("bookshipment")  // http://localhost:8080/market-api/shippers/upsertcompany
 	@Consumes("application/json,application/vnd.openport.market.v1+json")
 	@Produces({ MediaType.APPLICATION_JSON })
 	public ShipmentReturn bookShipment(ShipmentBooking shipmentBooking) {
 		UserStatus sts = new UserStatus();
 		ShipmentReturn re = new ShipmentReturn();
 		try {
 			System.out.println("Book shipment ####");
 			boolean isSuccess = true;
 			
 			//company
 			int shipmentId= shipmentBooking.getShipmentId();
 			int shipmentCarrierBidId = shipmentBooking.getShipmentCarrierBidId();
 			int assignedProviderId = shipmentBooking.getAssignedProviderId();
 			String carrierCd = shipmentBooking.getCarrierCd();
 			int statusId = 30 ;
 			
 			re.setShipmentId(shipmentId);

 			
 			String sql = "UPDATE `shipment` SET "+
				" status_id = ? "+
				" ,assigned_provider_id = ? "+
				" WHERE  shipment_id = ? "; 

 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
 				pstmt.setInt(1, statusId);
 				pstmt.setInt(2, assignedProviderId);
 				pstmt.setInt(3, shipmentId);
				
				int iRslt = pstmt.executeUpdate();
				
				if(iRslt<1){
					log.error("Faild to update shipment id "+shipmentId+ " to status_id:"+statusId);
				}
				
				String sqlSp = "call sp_MP_to_TMS_loader("+shipmentId+")";
	 			try {
	 				doCallStoredProcUpdate(sqlSp);
				} catch (Exception e) {
					log.error("Failed to execute sql: " +sqlSp+" >>> "+ e, e);
				}
	 			re.setStatus(Response.Status.ACCEPTED.getStatusCode());
	 			
	 			return re;
				
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				re.setStatus(Response.Status.CONFLICT.getStatusCode());
				new ServiceException(Response.Status.CONFLICT);
			}	
 		
 		} catch (Exception e1) {
 			log.error("Error message: " + e1, e1);
 			re.setStatus(Response.Status.CONFLICT.getStatusCode());
 			new ServiceException(Response.Status.CONFLICT);
 			//new ServiceException(Response.Status.BAD_GATEWAY);
 		}
 		
 		return re;
 	}
	
	
	
	@POST  // http://localhost:8080/market-api/shippers
 	@Path("updateshipmentbidenddate")  // http://localhost:8080/market-api/shippers/updateshipmentbidenddate
 	// @Consumes("application/json,application/vnd.openport.market.v1+json")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
 	public ShipmentReturn updateShipmentBidTerminationDate(Shipment shipment) {
		ShipmentReturn re = new ShipmentReturn();
 		try {
 			
 			re.setShipmentId(shipment.getShipmentId());
 			// new ServiceException(Response.Status.CONFLICT);
 			//company
 			Timestamp expiryDate = shipment.getTenderExpirationDt();
 			int statusId = 15 ;
 			//statusId = 20 ;
 			if( shipment.getTenderExpirationDt().before(getCurrentTimeStamp())){
 				statusId = 20;
 			}
 			
 			
			String sql = "UPDATE `shipment` SET "+
				" tender_expiration_dt = ? "+
				" , status_id = ? "+
				" WHERE  shipment_id = ? "; 

 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
 				pstmt.setTimestamp(1, expiryDate);
 				pstmt.setInt(2, statusId);
 				pstmt.setInt(3, shipment.getShipmentId());
				int iRslt = pstmt.executeUpdate();
				
				
				if(iRslt<1){
					log.error("Faild to update shipment tender_expiration_dt:"+expiryDate);
				}else{
					re.setStatus(Response.Status.ACCEPTED.getStatusCode());
				}
				System.out.println("shipment status:"+re.getStatus());
				return re;
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	

 		
 		} catch (Exception e1) {
 			log.error("Error message: " + e1, e1);
 			System.out.println("nag error e1:"+e1);
 			new ServiceException(Response.Status.CONFLICT);
 		}
 		return re;
 	}
	
	@GET // http://localhost:8080/market-api/shippers/vendorlist
	@Path("vendorlist/{countryCd}")
	@Produces({ MediaType.APPLICATION_JSON })
	@NoAuthorization
	public Vendors getVendorListBkgApp(@PathParam("countryCd") String countryCd) {
		Vendors vendorList = new Vendors();
		List<Company> companyList = new ArrayList();
		try {
				//companyList=doGetCompanyVendorListByCountry(countryCd);
				companyList=doGetCompanySetVendorListByCountry(countryCd);
				if((companyList == null || (companyList.size()<1))){
					System.out.println("vendorList is null");
					// new ServiceException(Response.Status.NOT_FOUND);
					vendorList.setStatus(Response.Status.NOT_FOUND.getStatusCode());
					companyList = new ArrayList();
				}else{
					vendorList.setStatus(Response.Status.OK.getStatusCode());
				}
				vendorList.setCompanies(companyList);
			} catch (Exception e) {
				log.error("Error message2: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		return vendorList;
	}
	
	
	@GET // http://localhost:8080/market-api/shippers/vendorlist
	@Path("vendorlist2/{countryCd}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Company> getVendorListBkgApp2(@PathParam("countryCd") String countryCd) {
		List<Company> companyList = new ArrayList();
		try {
				//companyList=doGetCompanyVendorListByCountry(countryCd);
				companyList=doGetCompanySetVendorListByCountry(countryCd);
				if((companyList == null || (companyList.size()<1))){
					System.out.println("vendorList is null");
					companyList = new ArrayList<Company>() ;
				}
			} catch (Exception e) {
				log.error("Error message2: " + e, e);
				companyList = new ArrayList<Company>() ;
				new ServiceException(Response.Status.CONFLICT);
			}
		return companyList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipmentcarrierbid
	@Path("shipmentcarrierbidwithstatus/{shipmentid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ShipmentCarrierBid> getShipmentCarrierBidByShipmentIdWithStatus(@PathParam("shipmentid") int shipmentid) {
		List<ShipmentCarrierBid> shipmentCarrierBidList = new ArrayList();
		try {
			shipmentCarrierBidList=getShipmentCarrierBidListByShipmentId(shipmentid);
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return shipmentCarrierBidList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipmentcarrierbid
	@Path("shipmentcarrierbid/{shipmentid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ShipmentCarrierBid> getShipmentCarrierBidByShipmentId(@PathParam("shipmentid") int shipmentid) {

  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<ShipmentCarrierBid> shipmentCarrierBidList = new ArrayList();
		try {
	  		//shipmentCarrierBidList = doGetShipmentCarrierBids(shipmentid);
	  		List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shipmentid);
			if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
				
				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shipmentid);
				
				
				for (ShipmentCarrierBid scb: shipmentCarrierBids){
					if(scb.getShipmentCarrierBidId()>0){
						
						List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
																		scb.getShipmentCarrierBidId(),
																		scb.getCarrierId(),
																		shipmentAccessorialsList
																		);
						
						if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
							scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
						}
						shipmentCarrierBidList.add(scb);
					}
				}
			}
	  		/*
	  		if (shipmentCarrierBidList.get(0).getShipmentCarrierBidId() == -1){
	  			new ServiceException(Response.Status.CONFLICT);
	  		}else if (shipmentCarrierBidList.size()==0){
	  			new ServiceException(Response.Status.CONFLICT);
	  		}
	  		*/
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return shipmentCarrierBidList;
	}
	
	
	
	
	@GET // http://localhost:8080/market-api/shippers/companycertificates
	@Path("companycertificates/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public DocumentType getCompanyCertifications(@PathParam("companyid") int companyid) {
  		
		DocumentType documentType = doGetCompanyCertification(companyid);
		return documentType;
	}
	
	
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipmentpaymentterm/{paymenttermid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public PaymentTerm getShipmentPaymentTermById(@PathParam("paymenttermid") int paymenttermid) {
  		
  		CompanyAddress corporateAddress = new CompanyAddress();
  		PaymentTerm paymentTerm = doGetPaymentTerm(paymenttermid);
		return paymentTerm;
	}
	

	
	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentsearch/{origin}/{destination}/{pickupDateTxt}/{deliveryDateTxt}/{referenceNbr}/{statusId}/{companyId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentBySearchCriterias(@PathParam("origin") String origin, 
			@PathParam("destination") String destination,
			@PathParam("pickupDateTxt") String pickupDateTxt,
			@PathParam("deliveryDateTxt") String deliveryDateTxt,
			@PathParam("referenceNbr") String referenceNbr,
			@PathParam("statusId") String statusId,
			@PathParam("companyId") String companyId
			) {
		System.out.println(""+origin+"-"+destination);
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		ShipmentSearch shipmentSearch = new ShipmentSearch();
  		shipmentSearch.setOriginTxt(origin);
  		shipmentSearch.setDestinationTxt(destination);
  		shipmentSearch.setPickupDateTxt(pickupDateTxt);
  		shipmentSearch.setPickupDateTxt2("all");
  		shipmentSearch.setDeliveryDateTxt(deliveryDateTxt);
  		shipmentSearch.setDeliveryDateTxt2("all");
  		shipmentSearch.setReferenceNbr(referenceNbr);
  		shipmentSearch.setStatusId(doGetIntValue(statusId));
  		shipmentSearch.setCompanyId(doGetIntValue(companyId));
  		
  		/*
  		String sp="sp_marketplace_shipments_all_data_temp";
		String sql = 	 " call " + sp + " (?) ";
		log.debug(sql+" companyId:"+companyId);
		List<ShipmentInfo> shipmentList = doGetShipmentsAllData(doGetIntValue(companyId),sql);
		shipmentRateList =  doSetShipmentInfoListToShipmentObj(shipmentList);
  		*/
  		
  		shipmentRateList = doGetShipmentListUsingSearchObj(shipmentSearch,false);
  		//shipmentRateList = doGetShipmentListBySearchCriteria(shipmentSearch);
  		
		return shipmentRateList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentsearchwapp/{origin}/{destination}/{pickupDateTxt}/{deliveryDateTxt}/{referenceNbr}/{statusId}/{companyId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentBySearchCriteriasWapp(@PathParam("origin") String origin, 
			@PathParam("destination") String destination,
			@PathParam("pickupDateTxt") String pickupDateTxt,
			@PathParam("deliveryDateTxt") String deliveryDateTxt,
			@PathParam("referenceNbr") String referenceNbr,
			@PathParam("statusId") String statusId,
			@PathParam("companyId") String companyId
			) {
		System.out.println(""+origin+"-"+destination);
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		ShipmentSearch shipmentSearch = new ShipmentSearch();
  		shipmentSearch.setOriginTxt(origin);
  		shipmentSearch.setDestinationTxt(destination);
  		shipmentSearch.setPickupDateTxt(pickupDateTxt);
  		shipmentSearch.setPickupDateTxt2("all");
  		shipmentSearch.setDeliveryDateTxt(deliveryDateTxt);
  		shipmentSearch.setDeliveryDateTxt2("all");
  		shipmentSearch.setReferenceNbr(referenceNbr);
  		shipmentSearch.setStatusId(doGetIntValue(statusId));
  		shipmentSearch.setCompanyId(doGetIntValue(companyId));
  		
  		/*
  		String sp="sp_marketplace_shipments_all_data_temp";
		String sql = 	 " call " + sp + " (?) ";
		log.debug(sql+" companyId:"+companyId);
		List<ShipmentInfo> shipmentList = doGetShipmentsAllData(doGetIntValue(companyId),sql);
		shipmentRateList =  doSetShipmentInfoListToShipmentObj(shipmentList);
  		*/
  		
  		shipmentRateList = doGetShipmentListUsingSearchObj(shipmentSearch,true);
  		//shipmentRateList = doGetShipmentListBySearchCriteria(shipmentSearch);
  		
		return shipmentRateList;
	}
	
	
	
	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentsearchdtrng/{origin}/{destination}/{pickupDateTxt1}/{pickupDateTxt2}/{deliveryDateTxt1}/{deliveryDateTxt2}/{referenceNbr}/{statusId}/{companyId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentBySearchCriteriasDateRanges(@PathParam("origin") String origin, 
			@PathParam("destination") String destination,
			@PathParam("pickupDateTxt1") String pickupDateTxt1,
			@PathParam("pickupDateTxt2") String pickupDateTxt2,
			@PathParam("deliveryDateTxt1") String deliveryDateTxt1,
			@PathParam("deliveryDateTxt2") String deliveryDateTxt2,
			@PathParam("referenceNbr") String referenceNbr,
			@PathParam("statusId") String statusId,
			@PathParam("companyId") String companyId
			) {
		System.out.println(""+origin+"-"+destination+"-"+pickupDateTxt1+"-"+pickupDateTxt2);
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		ShipmentSearch shipmentSearch = new ShipmentSearch();
  		shipmentSearch.setOriginTxt(origin);
  		shipmentSearch.setDestinationTxt(destination);
  		shipmentSearch.setPickupDateTxt(pickupDateTxt1);
  		shipmentSearch.setPickupDateTxt2(pickupDateTxt2);
  		shipmentSearch.setDeliveryDateTxt(deliveryDateTxt1);
  		shipmentSearch.setDeliveryDateTxt2(deliveryDateTxt2);
  		shipmentSearch.setReferenceNbr(referenceNbr);
  		shipmentSearch.setStatusId(doGetIntValue(statusId));
  		shipmentSearch.setCompanyId(doGetIntValue(companyId));
  		
  		shipmentRateList = doGetShipmentListUsingSearchObj(shipmentSearch,false);
  		//shipmentRateList = doGetShipmentListBySearchCriteriaDateRange(shipmentSearch);
		return shipmentRateList;
	}
	
	
	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentforbidsearch/{origin}/{destination}/{pickupDateTxt}/{deliveryDateTxt}/{referenceNbr}/{statusId}/{companyId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentForBidBySearchCriterias(@PathParam("origin") String origin, 
			@PathParam("destination") String destination,
			@PathParam("pickupDateTxt") String pickupDateTxt,
			@PathParam("deliveryDateTxt") String deliveryDateTxt,
			@PathParam("referenceNbr") String referenceNbr,
			@PathParam("statusId") String statusId,
			@PathParam("companyId") String companyId
			) {
		log.error(""+origin+"-"+destination);
		System.out.println(""+origin+"-"+destination);
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		ShipmentSearch shipmentSearch = new ShipmentSearch();
  		shipmentSearch.setOriginTxt(origin);
  		shipmentSearch.setDestinationTxt(destination);
  		shipmentSearch.setPickupDateTxt(pickupDateTxt);
  		shipmentSearch.setDeliveryDateTxt(deliveryDateTxt);
  		shipmentSearch.setReferenceNbr(referenceNbr);
  		shipmentSearch.setStatusId(doGetIntValue(statusId));
  		shipmentSearch.setCompanyId(doGetIntValue(companyId));
  		
  		//shipmentRateList = doGetShipmentForBidListBySearchCriteria(shipmentSearch);
  		shipmentRateList = doGetShipmentListUsingSearchObjCarrier(shipmentSearch,true);
  		
		return shipmentRateList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentpairforbidsearch/{origin}/{destination}/{pickupDateTxt}/{deliveryDateTxt}/{origin2}/{destination2}/{pickupDateTxt2}/{deliveryDateTxt2}/{companyId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentPairForBidBySearchCriterias(
			@PathParam("origin") String origin, 
			@PathParam("destination") String destination,
			@PathParam("pickupDateTxt") String pickupDateTxt,
			@PathParam("deliveryDateTxt") String deliveryDateTxt,
			@PathParam("origin2") String origin2, 
			@PathParam("destination2") String destination2,
			@PathParam("pickupDateTxt2") String pickupDateTxt2,
			@PathParam("deliveryDateTxt2") String deliveryDateTxt2,
			@PathParam("companyId") String companyId
			) {
		System.out.println(""+origin+"-"+destination);
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		ShipmentSearch shipmentSearch = new ShipmentSearch();
  		shipmentSearch.setOriginTxt(origin);
  		shipmentSearch.setDestinationTxt(destination);
  		shipmentSearch.setPickupDateTxt(pickupDateTxt);
  		shipmentSearch.setDeliveryDateTxt(deliveryDateTxt);
  		shipmentSearch.setOriginTxt2(origin2);
  		shipmentSearch.setDestinationTxt2(destination2);
  		shipmentSearch.setPickupDateTxt2(pickupDateTxt2);
  		shipmentSearch.setDeliveryDateTxt2(deliveryDateTxt2);

  		
  		shipmentSearch.setCompanyId(doGetIntValue(companyId));
  		
  		//shipmentRateList = doGetShipmentPairForBidListBySearchCriteria(shipmentSearch);

  		//rodel 20170309 add compact json return option
  		shipmentRateList = doGetShipmentPairListUsingSearchObjCarrier(shipmentSearch, true);
  		
		return shipmentRateList;
	}
	
	private void doDisplayShipmentSearchObject(ShipmentSearch shipmentSearch) {
		// TODO Auto-generated method stub
		
	}

	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentbackhaulsearch/{origin}/{pickupDateTxt}/{deliveryDateTxt}/{companyId}/{userId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentForBidWithBackhaul(
			@PathParam("origin") String origin, 
			@PathParam("pickupDateTxt") String pickupDateTxt,
			@PathParam("deliveryDateTxt") String deliveryDateTxt,
			@PathParam("companyId") String companyId,
			@PathParam("userId") String userId
			) {
		CompanyAddress corporateAddress = new CompanyAddress(); 
  		List<Shipment> shipmentRateList = new ArrayList();
  		ShipmentSearch shipmentSearch = new ShipmentSearch();
  		shipmentSearch.setOriginTxt(origin);
  		shipmentSearch.setPickupDateTxt(pickupDateTxt);
  		shipmentSearch.setDeliveryDateTxt(deliveryDateTxt);
  		shipmentSearch.setCompanyId(doGetIntValue(companyId));
  		
  		String spName = "marketplace_db.sp_marketplace_carrier_active_shipment_with_backhaul";
  		shipmentRateList = doGetShipmentWithBackhaulForBidListBySearchCriteria(shipmentSearch, userId, spName );
		return shipmentRateList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentbackhaulsearchall/{origin}/{pickupDateTxt}/{deliveryDateTxt}/{companyId}/{userId}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentForBidWithBackhaulAll(
			@PathParam("origin") String origin, 
			@PathParam("pickupDateTxt") String pickupDateTxt,
			@PathParam("deliveryDateTxt") String deliveryDateTxt,
			@PathParam("companyId") String companyId,
			@PathParam("userId") String userId
			) {
		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		ShipmentSearch shipmentSearch = new ShipmentSearch();
  		shipmentSearch.setOriginTxt(origin);
  		shipmentSearch.setPickupDateTxt(pickupDateTxt);
  		shipmentSearch.setDeliveryDateTxt(deliveryDateTxt);
  		shipmentSearch.setCompanyId(doGetIntValue(companyId));
  		
  		String spName = "marketplace_db.sp_marketplace_carrier_active_shipment_with_backhaul_all";
  		shipmentRateList = doGetShipmentWithBackhaulForBidListBySearchCriteria(shipmentSearch, userId, spName );
		return shipmentRateList;
	}
	
	private int doGetIntValue(String sInt) {
		int rslt =0;
		try {
			rslt = Integer.parseInt(sInt);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return rslt;
	}


	@GET // http://localhost:8080/market-api/shippers/shipmentsrch
	@Path("shipmentsrch") 
	@Consumes("application/json,application/vnd.openport.market.v1+json")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentBySearchCriteria(ShipmentSearch shipmentSearch) {
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		
  		shipmentRateList = doGetShipmentListUsingSearchObj(shipmentSearch,false);
  		//shipmentRateList = doGetShipmentListBySearchCriteria(shipmentSearch);
		return shipmentRateList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipmentsv1/{shippercompanyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentsAndRates(@PathParam("shippercompanyid") int companyId) {
  		
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		shipmentRateList = doGetShipmentAndRate(companyId);
		return shipmentRateList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipments/{shippercompanyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentsAndRatesV2(@PathParam("shippercompanyid") int companyId) {
  		
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		List<ShipmentInfo> shipmentList = new ArrayList();
  		
  		shipmentRateList = doGetShipmentAndRateV2(companyId);
  		System.out.println("shipmentRateList size:"+shipmentRateList.size());
  		/*
  		for(Shipment s: shipmentRateList){
  			//EntityAddress e = s.getPickupAddress();
  			System.out.println("pickup:"+e.getLine1());
  			//EntityAddress d = s.getDeliveryAddress();
  			System.out.println("delivery:"+d.getLine1());
  			//doDisplayShipmentCarrierBids(s.getShipmentCarrierBids());
  		}
  		*/
  		
		return shipmentRateList;
	}
	
	private void doDisplayShipmentCarrierBids(List<ShipmentCarrierBid> shipmentCarrierBids) {
		if ((shipmentCarrierBids!=null) && (shipmentCarrierBids.size()>0)){
			
		}else{
			System.out.println("No shipmentCarrierBid");
		}
		
	}


	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipmentsforbid/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentsForBid(@PathParam("companyid") int companyId) {
  		
  		//CompanyAddress corporateAddress = new CompanyAddress();
  		//List<Shipment> shipmentRateList = new ArrayList();
		//shipmentRateList = doGetShipmentForBid(companyId);
		//return shipmentRateList;
		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_carrier_active_shipment_all_data",true);
		return shipmentList;
  		
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("vendorshipments/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getVenndorShipments(@PathParam("companyid") int companyId) {
  		
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
		shipmentRateList = doGetVendorShipments(companyId);
  		
		return shipmentRateList;
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("carrierbiddetailsearch/{shipmentid}/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })

	public ShipmentCarrierBid getCarrierBidOnShipment(@PathParam("shipmentid") String shipmentid, 
			@PathParam("companyid") String companyid) {
  		
  		ShipmentCarrierBid shipmentCarrierBid = new ShipmentCarrierBid();
  		shipmentCarrierBid = doGetShipmentCarrierBid(doGetIntValue(shipmentid),doGetIntValue(companyid));
		return shipmentCarrierBid;
	}
	
	
	
	private ShipmentCarrierBid doGetShipmentCarrierBid(int shipmentid, int companyid) {
		log.info("get "+shipmentid + " " + companyid);
		try {
			ShipmentCarrierBid shipmentCarrierBid =  doGetShipmentCarrierBidByCarrierId(shipmentid,companyid);
			log.info("carrier Bid "+ shipmentCarrierBid);
			if(shipmentCarrierBid != null && shipmentCarrierBid.getShipmentCarrierBidId()<=0) {
				log.info("Should go here");
				shipmentCarrierBid =  doGetShipmentCarrierBidByNoCarrierId(shipmentid);
			}else if(shipmentCarrierBid == null) {
				log.info("it is null");
				shipmentCarrierBid =  doGetShipmentCarrierBidByNoCarrierId(shipmentid);
			}
			
			if(shipmentCarrierBid!=null){
				if(shipmentCarrierBid.getShipmentCarrierBidId()>0){
					
					List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsNoDefault(
							shipmentCarrierBid.getShipmentCarrierBidId()
							);
					
					if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
						shipmentCarrierBid.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
					}else {
						//
						
						List<Accessorial> accList = getShipmentAccessorial(shipmentid);
						
						if(accList!=null && accList.size()> 0) {
							List<ShipmentCarrierBidDetail> bidList = new LinkedList<>();
							for(Accessorial acc : accList) {
								ShipmentCarrierBidDetail scb = new ShipmentCarrierBidDetail();
								log.info("acc.getAccessorialDataSource " + acc.getAccessorialDataSource());
								scb.setAccessorialCd(acc.getAccessorialCd());
								scb.setAccessorialDescription(acc.getDescription());
								scb.setAccessorialAmt(acc.getAccessorialAmt());
								if(acc.getAccessorialDataSource()!=null && (acc.getAccessorialDataSource().equalsIgnoreCase("MP")||acc.getAccessorialDataSource().equalsIgnoreCase("TMS-MP"))) {
									scb.setAccessorialDataSource(acc.getAccessorialType());
								}else {
								scb.setAccessorialDataSource(acc.getAccessorialDataSource());
								}
								
								bidList.add(scb);
								
							}
							shipmentCarrierBid.setShipmentCarrierBidDetails(bidList);
						}
						
					}
				}else {
					List<Accessorial> accList = getShipmentAccessorial(shipmentid);
					
					if(accList!=null && accList.size()> 0) {
						List<ShipmentCarrierBidDetail> bidList = new LinkedList<>();
						for(Accessorial acc : accList) {
							ShipmentCarrierBidDetail scb = new ShipmentCarrierBidDetail();
							log.info("acc.getAccessorialDataSource " + acc.getAccessorialDataSource());
							scb.setAccessorialCd(acc.getAccessorialCd());
							scb.setAccessorialDescription(acc.getDescription());
							scb.setAccessorialAmt(acc.getAccessorialAmt());
							if(acc.getAccessorialDataSource()!=null && (acc.getAccessorialDataSource().equalsIgnoreCase("MP")||acc.getAccessorialDataSource().equalsIgnoreCase("TMS-MP"))) {
								scb.setAccessorialDataSource(acc.getAccessorialType());
							}else {
							scb.setAccessorialDataSource(acc.getAccessorialDataSource());
							}
							
							bidList.add(scb);
							
						}
						shipmentCarrierBid.setShipmentCarrierBidDetails(bidList);
					}
				}
			}
			return shipmentCarrierBid;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

private List<Accessorial> getShipmentAccessorial(Integer shipmentId) {
		
		String sql = "select ACCESSORIAL_CD,ACCESSORIAL_DESCRIPTION,ACCESSORIAL_TYPE,ACCESSORIAL_AMT,ACCESSORIAL_DATA_SOURCE from shipment_accessorial where shipment_id="+shipmentId;
				   		   
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentAccessorial> driverRawMapper = new ResultSetMapper<ShipmentAccessorial>();
	        	List<ShipmentAccessorial> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentAccessorial.class);
	        	List<Accessorial> accList = new LinkedList<>();
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		for(ShipmentAccessorial sac : pojoList) {
	        			Accessorial acc = new Accessorial();
	        			acc.setAccessorialCd(sac.getAccessorialCd());
	        			acc.setAccessorialType(sac.getAccessorialType());
	        			acc.setDescription(sac.getAccessorialDescription());
	        			acc.setAccessorialDataSource(sac.getAccessorialDataSource());
	        			acc.setAccessorialAmt(sac.getAccessorialAmt());
	        			acc.setStatus(Boolean.TRUE);
	        			accList.add(acc);
	        		}
	        		return accList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
		
	}


	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipperactivebids/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipperActiveBids(@PathParam("companyid") int companyId) {
  		
  		/*
		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		
  		List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_shipper_active_bids");
  		System.out.println(shipmentList.size());
  		if ((shipmentList!=null) && (shipmentList.size()>0)){
  			shipmentRateList = doGetShipmentMyBids(companyId,shipmentList,false);
  		}
		return shipmentRateList;
		*/
		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_shipper_active_bids_all_data",true);
		return shipmentList;
	}
	
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipperpendingbooking/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipperPendingBooking(@PathParam("companyid") int companyId) {
  		
		/*
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		
  		List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_shipper_pending_bookings");
  		System.out.println(shipmentList.size());
  		if ((shipmentList!=null) && (shipmentList.size()>0)){
  			shipmentRateList = doGetShipmentMyBids(companyId,shipmentList,false);
  		}
		return shipmentRateList;
		*/
		
		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_shipper_pending_bookings_all_data",true);
		return shipmentList;
		
	}
	
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipperbookedshipments/{companyid}") 
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipperBookedShipments(@PathParam("companyid") int companyId) {
  		
		/*
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		
  		List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_shipper_booked_shipments");
  		System.out.println(shipmentList.size());
  		if ((shipmentList!=null) && (shipmentList.size()>0)){
  			shipmentRateList = doGetShipmentMyBids(companyId,shipmentList,false);
  		}
		return shipmentRateList;
		*/
		
		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_shipper_booked_shipments_all_data",true);
		return shipmentList;
		
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipments
   	@Path("shipperstats/{companyid}")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public ShipperStats getShipperStats(@PathParam("companyid") int companyId) {
     	
     	ShipperStats shipperStats = doGetShipperStat(companyId);
     	System.out.println("companyId:"+companyId+" active:"+shipperStats.getMyActiveBidsCount()+" pending:"+shipperStats.getMyPendingBookingCount()+" booked:"+shipperStats.getMyBookedShipment());
   		return shipperStats;
   	}
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipmentsmybids/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentsMyBids(@PathParam("companyid") int companyId) {
  		
		/*
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		
  		List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_carrier_bids");
  		System.out.println(shipmentList.size());
  		if ((shipmentList!=null) && (shipmentList.size()>0)){
  			shipmentRateList = doGetShipmentMyBids(companyId,shipmentList,true);
  		}
		return shipmentRateList;
		*/
		//List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_carrier_bids_all_data",true);
		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyIdCarrierOrShipper(companyId,"sp_marketplace_carrier_bids_all_data",false, true);
		
		return shipmentList;
		
	}
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipmentsmyactivebids/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentsMyActiveBids(@PathParam("companyid") int companyId) {
  		
		/*
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();

  		List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_carrier_active_bids");
  		if ((shipmentList!=null) && (shipmentList.size()>0)){
  			shipmentRateList = doGetShipmentMyBids(companyId,shipmentList,true);
  		}
  		System.out.println(shipmentList.size());
		return shipmentRateList;
		*/
		
		
		//List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_carrier_active_bids_all_data",true);
		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyIdCarrierOrShipper(companyId,"sp_marketplace_carrier_active_bids_all_data",false, true);
		return shipmentList;
	}
	

	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipmentsmybidsallactive/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentsAllActiveBids(@PathParam("companyid") int companyId) {
  		// This list all shipments available for bidding
		/*
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();

  		List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_carrier_all_active_shipment");
  		System.out.println(shipmentList.size());
  		if ((shipmentList!=null) && (shipmentList.size()>0)){
  			shipmentRateList = doGetShipmentMyBids(companyId,shipmentList,true);
  		}

		return shipmentRateList;
		*/
		//List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_carrier_active_shipment_all_data",false);
		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyIdCarrierOrShipper(companyId,"sp_marketplace_carrier_active_shipment_all_data",false, true);
		
		return shipmentList;
	}
	
	
	
	
	@GET // http://localhost:8080/market-api/shippers/shipments
	@Path("shipmentsmybidswon/{companyid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Shipment> getShipmentsMyBidsWon(@PathParam("companyid") int companyId) {
  		
		/*
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<Shipment> shipmentRateList = new ArrayList();
  		List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_carrier_bids_won");
  		System.out.println(shipmentList.size());
  		if ((shipmentList!=null) && (shipmentList.size()>0)){
  			shipmentRateList = doGetShipmentMyBids(companyId,shipmentList,true );
  		}
  		*/
  		//List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyId(companyId,"sp_marketplace_carrier_bids_won_all_data",true);
  		List<Shipment> shipmentList = doGetShipmentGroupTypeByCompanyIdCarrierOrShipper(companyId,"sp_marketplace_carrier_bids_won_all_data",false, true);
		return shipmentList;
	}
	

    @GET // http://localhost:8080/market-api/shippers/shipments
   	@Path("shipmentsmybidstat/{companyid}")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public CarrierBidStats getShipmentsMyBidStat(@PathParam("companyid") int companyId) {
     	
    	
     	CarrierBidStats carrierBidStats = doGetShipmentCarrierBidStat(companyId);
     	System.out.println("companyId:"+companyId+" length:"+carrierBidStats.getMyBidsActiveCount()+" bids:"+carrierBidStats.getMyBidsCount());
   		return carrierBidStats;
   	}

	
    private CarrierBidStats doGetShipmentCarrierBidStat(int companyId) {
		// TODO Auto-generated method stub
    	// 
		String sql = 	 " call sp_marketplace_carrier_bid_stat(?)";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CarrierBidStats> driverRawMapper = new ResultSetMapper<CarrierBidStats>();
	        	List<CarrierBidStats> pojoList = driverRawMapper.mapResultSetToObject(rs, CarrierBidStats.class);
	        	System.out.println("size:"+pojoList.size());
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		CarrierBidStats cbs = pojoList.get(0); 
	        		System.out.println("shipmentCount:"+cbs.getActiveShipmentsCount());
	        		return cbs;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
    
    
    private ShipperStats doGetShipperStat(int companyId) {
		// TODO Auto-generated method stub
    	// 
		String sql = " call sp_marketplace_shipper_stat(?)";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipperStats> driverRawMapper = new ResultSetMapper<ShipperStats>();
	        	List<ShipperStats> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipperStats.class);
	        	System.out.println("size:"+pojoList.size());
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		ShipperStats cbs = pojoList.get(0); 
	        		System.out.println("shipmentCount:"+cbs.getMyActiveBidsCount());
	        		return cbs;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}



	@GET  // http://localhost:8080/market-api/shippers
    @Path("country")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public List<Country> getAllCountry() {
   		List<Country> countryList = new ArrayList<Country>();
   		try {
   			String sql = "SELECT * FROM `country`;";
   			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
   					PreparedStatement pstmt = connection.prepareStatement(sql );) {
   				
   		         try (ResultSet rs = pstmt.executeQuery()) {
   					while (rs.next()) {
   						com.openport.marketplace.model.Country usr = new com.openport.marketplace.model.Country(rs);
   						Country cntry = new Country();
   						cntry.setCountryId(usr.getCountryId());
   						cntry.setCountryName(usr.getCountryName());
   						cntry.setCountry2Cd(usr.getCountry2Cd());
   						cntry.setCountry3Cd(usr.getCountry3Cd());
   						countryList.add(cntry);
   			        }
   					
   		         }
   			} catch (Exception e) {
   				log.error("Error message: " + e, e);
   				new ServiceException(Response.Status.CONFLICT);
   			}			  
   		} catch (Exception e1) {
   			log.error("Error message: " + e1, e1);
   			new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		return countryList;
   	}
	
	@GET  // http://localhost:8080/market-api/shippers
    @Path("accessorialcode")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public List<AccessorialCode> getAccessorialCodes() {
		
		String sql = 	" SELECT CUSTOMER_CD, PARENT_PARTNER_CD, ACCESSORIAL_CD, "
						+ " ACCESSORIAL_TYPE, ACCESSORIAL_DATA_SOURCE, DESCRIPTION, SIZE_UOM, "
						+ " LAST_UPDATE, LAST_UPDATE_SOURCE "
						+ " FROM abs_rater_utf8.accessorial_code where parent_partner_cd='MP'";
		
		System.out.println("accessorialCode:"+sql);
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<AccessorialCode> driverRawMapper = new ResultSetMapper<AccessorialCode>();
	        	List<AccessorialCode> pojoList = driverRawMapper.mapResultSetToObject(rs, AccessorialCode.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
	
			
		return null;
		
		
		
   	}
    
    @GET  // http://localhost:8080/market-api/shippers
    @Path("status")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public List<Status> getStatus() {
    	List<Status> statusList = new ArrayList<Status>();
		String sql = 	" SELECT Status_Id, Status_Cd, Description_Txt FROM marketplace_db.status; ";
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Status> driverRawMapper = new ResultSetMapper<Status>();
	        	List<Status> pojoList = driverRawMapper.mapResultSetToObject(rs, Status.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
   	}
    
    
    // {"firstName":"testtest","lastName":"test","username":"test","password":"test"}
 	@POST  // http://localhost:8080/market-api/shippers
 	@Path("upsertcompany")  // http://localhost:8080/market-api/shippers/upsertcompany
 	@Consumes("application/json,application/vnd.openport.market.v1+json")
 	@Produces({ MediaType.APPLICATION_JSON })
 	public Company upsertCompany(Company company) {
 		UserStatus sts = new UserStatus();
 		try {
 			log.info("CREATE COMPANY ..." + company.getCompanyCode());
 			boolean isSuccess = true;
 			company.setStatus(0);
 			company.setMessage("Ok");
 			//company
 			String companyName = blankWhenNull(company.getCompanyName());
 			String companyCode = blankWhenNull(company.getCompanyCode());
 			String businessHeadline = blankWhenNull(company.getBusinessHeadline());
 			String businessDescription = blankWhenNull(company.getBusinessDescription());
 			String timezoneId = blankWhenNull(company.getTimezoneId());
 			Integer yearsOfOperation = zeroWhenNull(company.getYearsOfOperation());
 			int wizardStepNbr = company.getWizardStepNbr();
 			
 			
 			/*
 			 *
 			 * check if company code and company id is unique
 			 * if companyId+companyCode matches then ok
 			 * 
 			 * if companyId+companyCode DOES NOT match, then check if companyCode already exists. 
 			 * If exists then fail (msg = code is already in use) if NOT exist then continue update
 			 * 
 			 */
 			
 			/*
 			Company companyRecord = doGetCompanyObjectById(company.getCompanyId());
 			if(!companyRecord.getCompanyCode().trim().equalsIgnoreCase(companyCode.trim())){
 				Company companyRecord = doGetCompanyObjectById(company.getCompanyId());
 			}
 			*/
 			
 			Company companyRecord = doGetCompanyObjectByCode(companyCode);
 			log.info("companyRecord.getCompanyId():"+companyRecord.getCompanyId()+"company.getCompanyId():"+company.getCompanyId());
 			if(companyRecord.getCompanyId() !=null){
 				// companyCode already exists. 
 				Company cmpnyZero = new Company();
 	 			cmpnyZero.setStatus(-1);
 	 			cmpnyZero.setMessage("Company code "+companyCode+" is already in use by different company id.");
 				return cmpnyZero;
 			}
 			
 			if (company.getWizardStepNbr()>wizardStepNbr){
 				wizardStepNbr = company.getWizardStepNbr();
 				company.setWizardStepNbr(wizardStepNbr);
 			}
 			
 			log.info("Company exists:");
			String sql = "UPDATE `company` SET "+
				" update_User_Id = ? "+
				" ,update_Dt = ? "+
				" ,years_of_operation = ? "+
				" ,wizard_step_nbr = ? "+
				" ,company_name = ? "+
				" ,business_Headline = ? "+
				" ,business_Description = ? "+
				" ,timezone_id = ? "+
				" ,company_code = ? "+
				" WHERE  company_id = ? "; 

 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				pstmt.setInt(1, 3);
				pstmt.setTimestamp(2, getCurrentTimeStamp());
				pstmt.setInt(3, yearsOfOperation);
				pstmt.setInt(4, wizardStepNbr);
				pstmt.setString(5, companyName);
				pstmt.setString(6, businessHeadline);
				pstmt.setString(7, businessDescription);
				pstmt.setString(8, timezoneId);
				pstmt.setString(9, companyCode);
				
				pstmt.setInt(10, company.getCompanyId());
				//pstmt.setString(5, blankWhenNull(company.getBusinessHeadline()));
				//pstmt.setString(6,  blankWhenNull(company.getBusinessDescription()));
				int iRslt = pstmt.executeUpdate();
				
				if(iRslt<1){
					log.error("Faild to update yearsOfOperation:"+yearsOfOperation+" for Company:"+companyName);
				}
				
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	

 		
 		} catch (Exception e1) {
 			log.error("Error message: " + e1, e1);
 			Company cmpnyZero = new Company();
 			cmpnyZero.setStatus(-1);
 			cmpnyZero.setMessage("Fail");
 			return cmpnyZero;
 			//new ServiceException(Response.Status.CONFLICT);
 		}
 		
 		return company;
 	}

// {"firstName":"testtest","lastName":"test","username":"test","password":"test"}
  	@POST  // http://localhost:8080/market-api/shippers
  	@Path("upsertcompanyaddress")  // http://localhost:8080/market-api/shippers/upsertcompany
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public CompanyAddress upsertCorporateAddress(CompanyAddress companyAddress) {
  		UserStatus sts = new UserStatus();
  		try {
  			log.info("CREATE COMPANY ..." + companyAddress.getCompanyAddressId());
  			boolean isSuccess = true;
  			
  			if(companyAddress==null){
  				isSuccess = false;
  				return companyAddress;
  			}
  			
  			//company

  			
  			if(companyAddress.getCompanyAddressId() > 0) {
  				log.debug("companyAddress.getCompanyAddressId():" + companyAddress.getCompanyAddressId());
  				return updateCompanyAddress(companyAddress);
  			}
  			
  			int companyAddressId=-1;
  			int countryId=companyAddress.getCountryId();
  			
  			
  			companyAddress = doUpsertCompanyAddressObjectByGeo( 
  					companyAddress,
					11);
  			/*
 			int provinceId = -1;
 			if((company.getCorporateAddress().getProvinceName()!=null) && (company.getCorporateAddress().getProvinceName().trim().length()>0)){
 				provinceId=doUpsertProvinceByCountryAndProvinceNameObject(company.getCorporateAddress().getProvinceName(), countryId, 11);
 			}
 			CompanyAddress companyAddress = doUpsertCompanyAddressObject(companyId, company.getCorporateAddress(), provinceId, countryId, 11);
 			*/
 		} catch (Exception e1) {
  			log.error("Error message: " + e1, e1);
  			new ServiceException(Response.Status.BAD_GATEWAY);
  		}
  		
  		return companyAddress;
  	}
  	
  	@POST  // http://localhost:8080/market-api/shippers
  	@Path("updatecmpnybranch")  // http://localhost:8080/market-api/shippers/upsertcompany
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public List<CompanyBranch> UpdateCompanyBranch(CompanyBranch companyBranch) {
  		List<CompanyBranch> companyBranchList = new ArrayList();
   		try {
   			log.info("CREATE COMPANY ...");
   			boolean isSuccess = true;
   			//company
  			int companyId = companyBranch.getCompanyId();
  			// need to check for 
  			CompanyAddress companyAddress = doUpsertCompanyAddressObjectByGeo(  
  					companyBranch.getBranchAddress(),
					11);
  			doUpdateCompanyBranchAddressIdAndBranchName(companyBranch.getBranchName(),companyBranch.getCompanyBranchId());
  			
  			// need to change input as list
  			//doUpdateCompanyBranchAddressIdAndBranchName(companyBranch.getBranchName(),companyBranch.getCompanyBranchId(), companyAddress.getCompanyAddressId());
  			
  			int wizardStepNbr = 4;
  			doUpdateWizardStepNbr(wizardStepNbr, companyId);
  			
  			companyBranchList = doGetCompanyBranchesObject(companyId);
  			
   		
   		} catch (Exception e) {
   			log.error("Error message: " + e, e);
   			new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		
   		return companyBranchList;
   	}
  	
  	@POST  // http://localhost:8080/market-api/shippers
  	@Path("updatewizardstepnbr")  // http://localhost:8080/market-api/shippers/upsertcompany
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public Company UpdateWizardStepNbr(Company company) {
  		//List<CompanyBranch> companyBranchList = new ArrayList();
   		try {
   			log.info("CREATE COMPANY ...");
   			boolean isSuccess = true;
   			//company
  			int companyId = company.getCompanyId();
  			
  			company.setStatus(0);
  			if(company.getCorporateAddressId()==null){
  				doUpdateCompanyWizardStepNbr(company);
  			}else{
  				doUpdateCompanyWizardCorporateAddressId(company);	
  			}
  			//companyBranchList = doGetCompanyBranchesObject(companyId);
   		} catch (Exception e) {
   			log.error("Error message: " + e, e);
   			company.setStatus(-1);
   			return company;
   			//new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		
   		return company;
   	}
  	
  	
  	@POST  // http://localhost:8080/market-api/shippers
  	@Path("updatewizardstepnbrmapp")  // http://localhost:8080/market-api/shippers/upsertcompany
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public Company UpdateWizardStepNbrWithCheckOnCompanyAddress(Company company) {
  		//List<CompanyBranch> companyBranchList = new ArrayList();
   		try {
   			log.info("CREATE COMPANY ...");
   			boolean isSuccess = true;
   			//company
  			int companyId = company.getCompanyId();
  			
  			String companyCd= company.getCompanyCode();
  			int wizardStepNbr =  company.getWizardStepNbr();
  			
  			company.setStatus(0);
  			if(company.getCorporateAddressId()==null){
  				doUpdateCompanyWizardStepNbr(company);
  			}else if ((company.getCorporateAddressId()!=null) && (company.getCorporateAddressId()>-1)){
  				company.setWizardStepNbr(2);
  				doUpdateCompanyWizardCorporateAddressId(company);	
  			}else if ((company.getCorporateAddressId()!=null) && (company.getCorporateAddressId()<0)){
  				doUpdateCompanyWizardStepNbr(company);
  			}else{
  				doUpdateCompanyWizardStepNbr(company);
  			}
  			//companyBranchList = doGetCompanyBranchesObject(companyId);
   		} catch (Exception e) {
   			log.error("Error message: " + e, e);
   			company.setStatus(-1);
   			return company;
   			//new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		
   		
   		
   		return company;
   	}
  	
  
  	
  	
  	
  	@POST  // http://localhost:8080/market-api/shippers
  	@Path("donothing")  // http://localhost:8080/market-api/shippers/upsertcompany
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public Company DoNothingHere(Company company) {
  		List<CompanyBranch> companyBranchList = new ArrayList();
   		try {
   			log.info("DO NOTHING ...");
   			doGetCompanyObjectById(-1);
   		} catch (Exception e) {
   			log.error("Error message: " + e, e);
   			new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		return company;
   	}


	@POST  // http://localhost:8080/market-api/shippers
  	@Path("createcmpnybranch")  // http://localhost:8080/market-api/shippers/upsertcompany
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public List<CompanyBranch> CreateCompanyBranch(CompanyBranch companyBranch) {
  		List<CompanyBranch> companyBranchList = new ArrayList();
   		try {
   			log.info("CREATE COMPANY ...");
   			boolean isSuccess = true;
   			//company
  			int companyId = companyBranch.getCompanyId();

  			CompanyAddress companyAddress = doUpsertCompanyAddressObjectByGeo( 
  					companyBranch.getBranchAddress(),
					11);
  			
  			doCreateCompanyBranch(companyBranch,companyId,companyAddress.getCompanyAddressId(),11);
  			// doUpdateCompanyBranchAddressId(companyBranch.getCompanyBranchId(), companyAddress.getCompanyAddressId());
  			
  			int wizardStepNbr = 4;
  			doUpdateWizardStepNbr(wizardStepNbr, companyId);
  			
  			companyBranchList = doGetCompanyBranchesObject(companyId);
  			
   		
   		} catch (Exception e) {
   			log.error("Error message: " + e, e);
   			new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		
   		return companyBranchList;
   	}
  	
  	
  	private int doCreateCompanyBranch(CompanyBranch companyBranch, int companyId, int companyAddressId, int userId) {
		// SELECT Company_Branch_ID,  
  		// FROM marketplace_db.company_branch;
  		int companyBranchId = -1;
		try {
				String sql = "insert into company_branch "
						+" (Branch_Name, Is_Truck_Pool, Company_ID, Company_Address_ID, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt) VALUES "
						+" (?,?,?,?,?,?,?,?);";
				
				try (
						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
					{
 					pstmt.setString(1, companyBranch.getBranchName());
 					pstmt.setInt(2, 0);
 					pstmt.setInt(3, companyId);
 					pstmt.setInt(4, companyAddressId);
 					pstmt.setInt(5, userId);
 					pstmt.setTimestamp(6, getCurrentTimeStamp());
 					pstmt.setInt(7, userId);
 					pstmt.setTimestamp(8, getCurrentTimeStamp());
 					
 					int id = pstmt.executeUpdate();
 					ResultSet rs = pstmt.getGeneratedKeys();
 					if (rs != null && rs.next()) {
 						 companyBranchId = rs.getInt(1);
 					}
				} catch (Exception e) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return companyBranchId;
	}


	@POST  // http://localhost:8080/market-api/shippers
  	@Path("deletecmpnybranch")  // http://localhost:8080/market-api/shippers/upsertcompany
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public List<CompanyBranch> DeleteCompanyBranch(CompanyBranch companyBranch) {
  		List<CompanyBranch> companyBranchList = new ArrayList();
   		try {
   			log.info("CREATE COMPANY ...");
   			boolean isSuccess = true;
   			//company
  			int companyId = companyBranch.getCompanyId();
  			doDeleteCompanyBranch(companyBranch.getCompanyBranchId());
  			
  			int wizardStepNbr = 4;
  			doUpdateWizardStepNbr(wizardStepNbr, companyId);
  			companyBranchList = doGetCompanyBranchesObject(companyId);
  			
   		
   		} catch (Exception e) {
   			log.error("Error message: " + e, e);
   			new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		
   		return companyBranchList;
   	}

	/*
	// {"firstName":"testtest","lastName":"test","username":"test","password":"test"}
   	@POST  // http://localhost:8080/market-api/shippers
   	@Path("upsertcompanyclient")  // http://localhost:8080/market-api/shippers/upsertcompany
   	@Consumes("application/json,application/vnd.openport.market.v1+json")
   	@Produces({ MediaType.APPLICATION_JSON })
   	public Company upsertCompanyClientV1(Company company) {
   		UserStatus sts = new UserStatus();
   		try {
   			log.info("CREATE COMPANY ...");
   			boolean isSuccess = true;
   			//company
  			String companyName = blankWhenNull(company.getCompanyName());
  			String companyCode = blankWhenNull(company.getCompanyCode());
  			int companyId = company.getCompanyId();
  			
  			
  			List<CompanyClient> clientList = new ArrayList();
  			clientList = company.getClients();
  			if (clientList!=null){
  				for(CompanyClient cc: clientList){
  					log.info("clientName:"+cc.getClientName());
  				}
  			}
  			//clientList=null; //for testing
  			
  			
  			int clientId = -1;
  			if (clientList!=null){
  				
  	 			String sql = "DELETE FROM  COMPANY_CLIENT WHERE company_id = ?  ;";
		  			try (
		  					java.sql.Connection connection = DbConfig.getDataSource().getConnection();
		 					PreparedStatement pstmt = connection.prepareStatement(sql);) 
		  				{
		  				pstmt.setInt(1, companyId);
		  				pstmt.executeUpdate();
		  			
		 			} catch (Exception e) {
		 				log.error("Error message: " + e, e);
		 				new ServiceException(Response.Status.CONFLICT);
		 			}	
  	 			List lClientNameList = new ArrayList();
  				for(CompanyClient cc: clientList){
  					log.info("clientName:"+cc.getClientName());
  					if(lClientNameList.contains(cc.getClientName().trim())){
  						continue;
  					}
  					lClientNameList.add(cc.getClientName().trim());
  					clientId = -1;
  					// check if client already exists
  		  			// if not exists, then insert
  		  			//if(clientId==-1){
	  				sql = "insert into company_client "
	  						+" (client_Name,company_id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
	  						+" (?,?,?,?,?,?);";
	  				
	  				try (
	  						java.sql.Connection connection = DbConfig.getDataSource().getConnection();
	  						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
	  					{
	  					
	 	 					pstmt.setString(1, cc.getClientName());
	 	 					pstmt.setInt(2, companyId);
	 	 					pstmt.setInt(3, 11);
	 	 					pstmt.setTimestamp(4, getCurrentTimeStamp());
	 	 					pstmt.setInt(5, 11);
	 	 					pstmt.setTimestamp(6, getCurrentTimeStamp());
	 	 					
	 	 					int id = pstmt.executeUpdate();
	  				} catch (Exception e) {
	  					isSuccess = false;
	  					log.error("Error message: " + e, e);
	  					new ServiceException(Response.Status.CONFLICT);
	  				}
  		  			//}else{
  		  			//	log.info("Client exists:");
  		  			//}
  				}
  			}else{
  				log.info("No Client list");
  			}
  			
  			int wizardStepNbr = company.getWizardStepNbr();
  			doUpdateWizardStepNbr(wizardStepNbr, companyId);
  			
  			
   			
   		
   		} catch (Exception e1) {
   			log.error("Error message: " + e1, e1);
   			new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		
   		return company;
   	}
  	*/
   	
   	@POST  // http://localhost:8080/market-api/shippers
   	@Path("upsertcompanyclient") 
   	@Consumes("application/json,application/vnd.openport.market.v1+json")
   	//@Produces({ MediaType.APPLICATION_JSON })
   	public void upsertCompanyClient(List<CompanyClient> clientList) {
   	//public void upsertCompanyClient(@PathParam("clientList") List<CompanyClient> clientList, @PathParam("wizardStepNbr") String wizardStepNbr){ 
   		UserStatus sts = new UserStatus();
   		try {
   			log.info("CREATE COMPANY ...");
   			boolean isSuccess = true;

  			int companyId = -1;
  			
  			if ((clientList!=null) && (clientList.size()>0)){
  				companyId = clientList.get(0).getCompanyId();
  			}
  			//clientList=null; //for testing
  			
  			
  			int clientId = -1;
  			if (clientList!=null){
  				
  	 			
  	 			String sql = "DELETE FROM  COMPANY_CLIENT WHERE company_id = ?  ;";
		  			try (
		  					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
		 					PreparedStatement pstmt = connection.prepareStatement(sql);) 
		  				{
		  				pstmt.setInt(1, companyId);
		  				pstmt.executeUpdate();
		  				/*
		 				
		 				pstmt.setString(2, cc.getClientName());
		 		        try (ResultSet rs = pstmt.executeQuery()) {
		 					while (rs.next()) {
		 						com.openport.marketplace.model.CompanyClient client = new com.openport.marketplace.model.CompanyClient(rs);
		 						clientId = client.getCompanyClientId();
		 			        }
		 		         }
		 		         */
		 			} catch (Exception e) {
		 				log.error("Error message: " + e, e);
		 				new ServiceException(Response.Status.CONFLICT);
		 			}	
  	 			List lClientNameList = new ArrayList();
  				for(CompanyClient cc: clientList){
  					log.info("clientName:"+cc.getClientName());
  					if(lClientNameList.contains(cc.getClientName().trim())){
  						continue;
  					}
  					lClientNameList.add(cc.getClientName().trim());
  					clientId = -1;
  					// check if client already exists
  		  			// if not exists, then insert
  		  			//if(clientId==-1){
	  				sql = "insert into company_client "
	  						+" (client_Name,company_id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
	  						+" (?,?,?,?,?,?);";
	  				
	  				try (
	  						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
	  						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
	  					{
	  					
	 	 					pstmt.setString(1, cc.getClientName());
	 	 					pstmt.setInt(2, companyId);
	 	 					pstmt.setInt(3, 11);
	 	 					pstmt.setTimestamp(4, getCurrentTimeStamp());
	 	 					pstmt.setInt(5, 11);
	 	 					pstmt.setTimestamp(6, getCurrentTimeStamp());
	 	 					
	 	 					int id = pstmt.executeUpdate();
	  				} catch (Exception e) {
	  					isSuccess = false;
	  					log.error("Error message: " + e, e);
	  					new ServiceException(Response.Status.CONFLICT);
	  				}
  		  			//}else{
  		  			//	log.info("Client exists:");
  		  			//}
  				}
  			}else{
  				log.info("No Client list");
  			}
  			
  			//doUpdateWizardStepNbr(wizardStepNbr, companyId);
   			
   		
   		} catch (Exception e1) {
   			log.error("Error message: " + e1, e1);
   			new ServiceException(Response.Status.BAD_GATEWAY);
   		}
   		
   		//return clientList;
   	}
   	


	@GET
	@Path("{companyid}") // // http://localhost:8080/market-api/users/1234
	@Produces({ MediaType.APPLICATION_JSON })
	public Company getCompanyById(@PathParam("companyid") int companyID) {
  		Company company = new Company();
  		CompanyAddress corporateAddress = new CompanyAddress();
  		List<CompanyClient> clients = new ArrayList();
  		List<CompanyBranch> companyBranchPojoList = new ArrayList();
  		List<CompanyBranch> pojoList =  new ArrayList();
		if (companyID < -1 ) {
			corporateAddress = new CompanyAddress();
			corporateAddress.setAddressLine1(null);
			corporateAddress.setAddressLine2(null);
			corporateAddress.setAddressLine3(null);
			corporateAddress.setCityName(null);
			corporateAddress.setPostalCd(null);
			corporateAddress.setProvinceName(null);
			corporateAddress.setCountryName(null);
			company.setCorporateAddress(corporateAddress);
			company.setClients(clients);
			//return company;
			//new ServiceException(Response.Status.BAD_REQUEST);
		} else {
			
			company = new Company();
			int companyAddressId = -1;
			int branchAddressId = -1;
			int countryId = -1 ;
			int provinceId = -1 ;
			
			try {
				company = doGetCompanyObjectById(companyID);
				companyAddressId = company.getCorporateAddressId();
				
				// COMPANY ADDRESS 
	  			corporateAddress = doGetCompanyAddress(companyAddressId);
	  			company.setCorporateAddress(corporateAddress);
	  			
	  			
	  			// CLIENTS
	  			clients = doGetCompanyClientsObject(companyID);
				company.setClients(clients);
				
				
				// BRANCHES
				companyBranchPojoList = doGetCompanyBranchesObject(companyID);
				company.setCompanyBranches(companyBranchPojoList);
	  			
	  			
			} catch (Exception e1) {
				log.error("Error message: " + e1, e1);
				new ServiceException(Response.Status.BAD_GATEWAY);
			}
		}
		

		
		return company;
	}
   	
 	
    
    // {"firstName":"testtest","lastName":"test","username":"test","password":"test"}
 	@POST  // http://localhost:8080/market-api/shippers
 	@Path("upsert")  // http://localhost:8080/market-api/shippers/upsert
 	@Consumes("application/json,application/vnd.openport.market.v1+json")
 	@Produces({ MediaType.APPLICATION_JSON })
 	public Company upsert(Company company) {
 		UserStatus sts = new UserStatus();
 		try {
 			log.info("upsert ...");
 			boolean isSuccess = true;
 			
 			// province
 			String province= null;
 			
 			//company address
 			String cityName = null;
 			String line1Address = null;
 			String line2Address= null;
 			String postalCd = null;
 			BigDecimal latitude = null;
 			BigDecimal longtitude = null;
 			
 			// insert child table first
 			// country 
 			String countryName = null;
 			if(company.getCorporateAddress()!=null){
	 			countryName = blankWhenNull(company.getCorporateAddress().getCountryName());
	 			province = blankWhenNull(company.getCorporateAddress().getProvinceName());
	 			cityName = blankWhenNull(company.getCorporateAddress().getCityName());
	 			line1Address = blankWhenNull(company.getCorporateAddress().getAddressLine1());
	 			line2Address = blankWhenNull(company.getCorporateAddress().getAddressLine2());
	 			postalCd = blankWhenNull(company.getCorporateAddress().getPostalCd());
	 			latitude = company.getCorporateAddress().getLat();
	 			longtitude = company.getCorporateAddress().getLon();
 			}
 			
 			//company
 			String companyName = blankWhenNull(company.getCompanyName());
 			String companyCode = blankWhenNull(company.getCompanyCode());
 			
 			
 			/*
 			 * 
 			 *  COUNTRY
 			 *  
 			 *  
 			 */
 			
 			int countryId=0;
 			// check if country already exists
 			String sql = "SELECT * FROM `country` WHERE country_name = ?;";
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql );) 
 				{
				
				pstmt.setString(1, company.getCorporateAddress().getCountryName());
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.Country cntry = new com.openport.marketplace.model.Country(rs);
						countryId = cntry.getCountryId();
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
 			
 			// if not exists, then insert
 			if(countryId==0){
 				
 				sql = "insert into country "
 						+" (country_Name,create_dt,update_dt,create_User_Id,update_User_Id) VALUES "
 						+" (?,?,?,?,?);";
 				
 				try (
 						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
 					{
 					
	 					pstmt.setString(1, countryName);
	 					pstmt.setTimestamp(2, getCurrentTimeStamp());
	 					pstmt.setTimestamp(3, getCurrentTimeStamp());
	 					pstmt.setInt(4, 11);
	 					pstmt.setInt(5, 11);
	 					
	 					int id = pstmt.executeUpdate();
	 					ResultSet rs = pstmt.getGeneratedKeys();
	 					if (rs != null && rs.next()) {
	 						countryId = rs.getInt(1);
	 					}
 				} catch (Exception e) {
 					isSuccess = false;
 					log.error("Error message: " + e, e);
 					new ServiceException(Response.Status.CONFLICT);
 					
 				}
 			}else{
 				log.info("countryId exists:"+countryId);
 			}
 			
 			/*
 			 * 
 			 * 
 			 *  PROVINCE
 			 * 
 			 */
 			int provinceId=0;
 			// check if country already exists
 			sql = "SELECT * FROM `province` WHERE province_name = ? and country_id = ? ;";
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setString(1, province);
				pstmt.setInt(2, countryId);
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.Province prvnc = new com.openport.marketplace.model.Province(rs);
						provinceId = prvnc.getProvinceId();
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
 			
 			// if not exists, then insert
 			if(provinceId==0){
 				
 				sql = "insert into province "
 						+" (province_Name,country_Id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
 						+" (?,?,?,?,?,?);";
 				
 				try (
 						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
 					{
 					
	 					pstmt.setString(1, province);
	 					pstmt.setInt(2, countryId);
	 					pstmt.setInt(3, 11);
	 					pstmt.setTimestamp(4, getCurrentTimeStamp());
	 					pstmt.setInt(5, 11);
	 					pstmt.setTimestamp(6, getCurrentTimeStamp());
	 					
	 					int id = pstmt.executeUpdate();
	 					ResultSet rs = pstmt.getGeneratedKeys();
	 					if (rs != null && rs.next()) {
	 						provinceId = rs.getInt(1);
	 					}
 				} catch (Exception e) {
 					isSuccess = false;
 					log.error("Error message: " + e, e);
 					new ServiceException(Response.Status.CONFLICT);
 				}
 			}else{
 				log.info("provinceId exists:"+provinceId);
 			}
 			
 			
 		
 			
 			
 			/*
 			 * 
 			 * 
 			 *  COMPANY ADDRESS 
 			 * 
 			 */
 			int companyAddressId=0;
 			// check if country already exists
 			sql = "SELECT * FROM `company_address` WHERE "
 				  +" address_Line_1 = ? "
 				  +" and city_Name = ? "
 				  +" and province_Id = ? "
 				  +" and country_Id = ? "
 				  +" and postal_Cd = ? ;";
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setString(1, line1Address);
				pstmt.setString(2, cityName);
				pstmt.setInt(3, provinceId);
				pstmt.setInt(4, countryId);
				pstmt.setString(5, postalCd);
				
				
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.CompanyAddress cmpnyAddress = new com.openport.marketplace.model.CompanyAddress(rs);
						companyAddressId = cmpnyAddress.getCompanyAddressId();
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
 			
 			// if not exists, then insert
 			if(companyAddressId==0){
 				
 				sql = "insert into company_address "
 						+" (address_Line_1,address_Line_2,city_Name,province_Id,country_Id,postal_Cd,create_User_Id,create_Dt,update_User_Id,update_Dt,company_Address_Id,latitude, longtitude) VALUES "
 						+" (?,?,?,?,?,?,?,?,?,?,?,?,?);";
 				
 				try (
 						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
 					{
 					
	 					pstmt.setString(1, line1Address);
	 					pstmt.setString(2, line2Address);
	 					pstmt.setString(3, cityName);
	 					pstmt.setInt(4, provinceId);
	 					pstmt.setInt(5, countryId);
	 					pstmt.setString(6, postalCd);
	 					pstmt.setInt(7, 11);
	 					pstmt.setTimestamp(8, getCurrentTimeStamp());
	 					pstmt.setInt(9, 11);
	 					pstmt.setTimestamp(10, getCurrentTimeStamp());
	 					pstmt.setInt(11, 3);
	 					pstmt.setBigDecimal(12, latitude);
	 					pstmt.setBigDecimal(13, longtitude);
	 					
	 					int id = pstmt.executeUpdate();
	 					ResultSet rs = pstmt.getGeneratedKeys();
	 					if (rs != null && rs.next()) {
	 						companyAddressId = rs.getInt(1);
	 					}
 				} catch (Exception e) {
 					isSuccess = false;
 					log.error("Error message: " + e, e);
 					new ServiceException(Response.Status.CONFLICT);
 				}
 			}else{
 				log.info("companyAddressId exists:"+companyAddressId);
 			}
 			
 			
 			if(isSuccess){
 				log.info("Success child table data. now insert ");
 			}else{
 				log.info("NOT Success child table data.");
 			}
 		
 		
 			
 			log.info("companyName:"+companyName);
 			log.info("companyCode:"+companyCode);
 			log.info("line1Address:"+line1Address);
 			log.info("cityName:"+cityName);
 			log.info("province:"+province);
 			log.info("provinceId:"+provinceId);
 			log.info("postalCd:"+postalCd);
 			log.info("countryName:"+countryName);
 			log.info("countryId:"+countryId);
 			
 		
 		} catch (Exception e1) {
 			log.error("Error message: " + e1, e1);
 			new ServiceException(Response.Status.BAD_GATEWAY);
 		}
 		
 		return company;
 	}
 	
 	
 	@POST  // http://localhost:8080/market-api/shippers
 	@Consumes("application/json,application/vnd.openport.market.v1+json")
 	@Produces({ MediaType.APPLICATION_JSON })
 	public Company select(Company company) {
 		UserStatus sts = new UserStatus();
 		try {
 			log.info("select ...");
 			if(company==null){
 				log.info("no object ...");
 				return company;
 			}
 			
 			boolean isSuccess = true;
 			
 			// province
 			String province= null;
 			
 			//company address
 			String cityName = null;
 			String line1Address = null;
 			String line2Address= null;
 			String postalCd = null;
 			
 			// insert child table first
 			// country 
 			List<CompanyClient> clientList = new ArrayList();
 			String countryName = null;
 			if(company.getCorporateAddress()!=null){
	 			countryName = blankWhenNull(company.getCorporateAddress().getCountryName());
	 			province = blankWhenNull(company.getCorporateAddress().getProvinceName());
	 			cityName = blankWhenNull(company.getCorporateAddress().getCityName());
	 			line1Address = blankWhenNull(company.getCorporateAddress().getAddressLine1());
	 			line2Address = blankWhenNull(company.getCorporateAddress().getAddressLine2());
	 			postalCd = blankWhenNull(company.getCorporateAddress().getPostalCd());
	 			clientList = company.getClients();
	 			
 			}
 			
 			
 			//company
 			String companyName = blankWhenNull(company.getCompanyName());
 			String companyCode = blankWhenNull(company.getCompanyCode());
 			
 			
 			log.info("companyName:"+companyName);
 			log.info("companyCode:"+companyCode);
 			log.info("line1Address:"+line1Address);
 			log.info("cityName:"+cityName);
 			log.info("province:"+province);
 			log.info("postalCd:"+postalCd);
 			log.info("countryName:"+countryName);
 			
 			if (clientList!=null){
 				for(CompanyClient cc: clientList){
 					log.info("clientName:"+cc.getClientName());
 				}
 			}else{
 				log.info("No Client list");
 			}
 			
 			
 			
 			if(
 					(countryName==null)
 					|| (province==null)
 				){
 				log.info("no object countryName province...");
 				return company;
 			}
 			

 			/*
 			 * 
 			 *  COUNTRY
 			 *  
 			 *  
 			 */
 			
 			int countryId=0;
 			// check if country already exists
 			String sql = "SELECT * FROM `country` WHERE country_name = ?;";
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql );) 
 				{
				
				pstmt.setString(1, company.getCorporateAddress().getCountryName());
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.Country cntry = new com.openport.marketplace.model.Country(rs);
						countryId = cntry.getCountryId();
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
 			
 			// if not exists, then insert
 			if(countryId==0){
 				
 				
 			}else{
 				log.info("countryId exists:"+countryId);
 			}
 			
 			/*
 			 * 
 			 * 
 			 *  PROVINCE
 			 * 
 			 */
 			int provinceId=0;
 			// check if country already exists
 			sql = "SELECT * FROM `province` WHERE province_name = ? and country_id = ? ;";
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setString(1, province);
				pstmt.setInt(2, countryId);
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.Province prvnc = new com.openport.marketplace.model.Province(rs);
						provinceId = prvnc.getProvinceId();
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
 			
 			// if not exists, then insert
 			if(provinceId==0){
 				
 				
 			}else{
 				log.info("provinceId exists:"+provinceId);
 			}
 			
 			
 		
 			
 			
 			/*
 			 * 
 			 * 
 			 *  COMPANY ADDRESS 
 			 * 
 			 */
 			int companyAddressId=0;
 			// check if country already exists
 			sql = "SELECT * FROM `company_address` WHERE "
 				  +" address_Line_1 = ? "
 				  +" and city_Name = ? "
 				  +" and province_Id = ? "
 				  +" and country_Id = ? "
 				  +" and postal_Cd = ? ;";
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setString(1, line1Address);
				pstmt.setString(2, cityName);
				pstmt.setInt(3, provinceId);
				pstmt.setInt(4, countryId);
				pstmt.setString(5, postalCd);
				
				
		        try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						com.openport.marketplace.model.CompanyAddress cmpnyAddress = new com.openport.marketplace.model.CompanyAddress(rs);
						companyAddressId = cmpnyAddress.getCompanyAddressId();
			        }
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	
 			
 			// if not exists, then insert
 			if(companyAddressId==0){
 				
 				
 			}else{
 				log.info("companyAddressId exists:"+companyAddressId);
 			}
 			
 			
 			if(isSuccess){
 				log.info("Success child table data. now insert ");
 			}else{
 				log.info("NOT Success child table data.");
 			}
 		
 		
 			
 			log.info("companyName:"+companyName);
 			log.info("companyCode:"+companyCode);
 			log.info("line1Address:"+line1Address);
 			log.info("cityName:"+cityName);
 			log.info("province:"+province);
 			log.info("provinceId:"+provinceId);
 			log.info("postalCd:"+postalCd);
 			log.info("countryName:"+countryName);
 			log.info("countryId:"+countryId);
 			
 		
 		} catch (Exception e1) {
 			log.error("Error message: " + e1, e1);
 			new ServiceException(Response.Status.BAD_GATEWAY);
 		}
 		
 		return company;
 	}
 	
 	
 // {"firstName":"testtest","lastName":"test","username":"test","password":"test"}
  	@POST  // http://localhost:8080/market-api/shippers
  	@Path("create")  // http://localhost:8080/market-api/shippers/upsert
  	@Consumes("application/json,application/vnd.openport.market.v1+json")
  	@Produces({ MediaType.APPLICATION_JSON })
  	public Company create(Company company) {
  		UserStatus sts = new UserStatus();
  		try {
  			log.info("create ...");
  			boolean isSuccess = true;
  			
  			// province
  			String province= null;
  			
  			//company address
  			String cityName = null;
  			String line1Address = null;
  			String line2Address= null;
  			String postalCd = null;
  			BigDecimal latitude = null;
  			BigDecimal longtitude = null;
  			
  			// insert child table first
  			// country 
  			String countryName = null;
  			if(company.getCorporateAddress()!=null){
 	 			countryName = blankWhenNull(company.getCorporateAddress().getCountryName());
 	 			province = blankWhenNull(company.getCorporateAddress().getProvinceName());
 	 			cityName = blankWhenNull(company.getCorporateAddress().getCityName());
 	 			line1Address = blankWhenNull(company.getCorporateAddress().getAddressLine1());
 	 			line2Address = blankWhenNull(company.getCorporateAddress().getAddressLine2());
 	 			postalCd = blankWhenNull(company.getCorporateAddress().getPostalCd());
 	 			latitude = company.getCorporateAddress().getLat();
 	 			longtitude = company.getCorporateAddress().getLon();
  			}
  			
  			//company
  			String companyName = blankWhenNull(company.getCompanyName());
  			String companyCode = blankWhenNull(company.getCompanyCode());
  			
  			
  			/*
  			 * 
  			 *  COUNTRY
  			 *  
  			 *  
  			 */
  			
  			int countryId=0;
  			// check if country already exists
  			String sql = "SELECT * FROM `country` WHERE country_name = ?;";
  			try (
  					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 					PreparedStatement pstmt = connection.prepareStatement(sql );) 
  				{
 				
 				pstmt.setString(1, company.getCorporateAddress().getCountryName());
 		        try (ResultSet rs = pstmt.executeQuery()) {
 					while (rs.next()) {
 						com.openport.marketplace.model.Country cntry = new com.openport.marketplace.model.Country(rs);
 						countryId = cntry.getCountryId();
 			        }
 		         }
 			} catch (Exception e) {
 				log.error("Error message: " + e, e);
 				new ServiceException(Response.Status.CONFLICT);
 			}	
  			
  			// if not exists, then insert
  			if(countryId==0){
  				
  				sql = "insert into country "
  						+" (country_Name,create_dt,update_dt,create_User_Id,update_User_Id) VALUES "
  						+" (?,?,?,?,?);";
  				
  				try (
  						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
  						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
  					{
  					
 	 					pstmt.setString(1, countryName);
 	 					pstmt.setTimestamp(2, getCurrentTimeStamp());
 	 					pstmt.setTimestamp(3, getCurrentTimeStamp());
 	 					pstmt.setInt(4, 11);
 	 					pstmt.setInt(5, 11);
 	 					
 	 					int id = pstmt.executeUpdate();
 	 					ResultSet rs = pstmt.getGeneratedKeys();
 	 					if (rs != null && rs.next()) {
 	 						countryId = rs.getInt(1);
 	 					}
  				} catch (Exception e) {
  					isSuccess = false;
  					log.error("Error message: " + e, e);
  					new ServiceException(Response.Status.CONFLICT);
  					
  				}
  			}else{
  				log.info("countryId exists:"+countryId);
  			}
  			
  			/*
  			 * 
  			 * 
  			 *  PROVINCE
  			 * 
  			 */
  			int provinceId=0;
  			// check if country already exists
  			sql = "SELECT * FROM `province` WHERE province_name = ? and country_id = ? ;";
  			try (
  					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 					PreparedStatement pstmt = connection.prepareStatement(sql);) 
  				{
 				
 				pstmt.setString(1, province);
 				pstmt.setInt(2, countryId);
 		        try (ResultSet rs = pstmt.executeQuery()) {
 					while (rs.next()) {
 						com.openport.marketplace.model.Province prvnc = new com.openport.marketplace.model.Province(rs);
 						provinceId = prvnc.getProvinceId();
 			        }
 		         }
 			} catch (Exception e) {
 				log.error("Error message: " + e, e);
 				new ServiceException(Response.Status.CONFLICT);
 			}	
  			
  			// if not exists, then insert
  			if(provinceId==0){
  				
  				sql = "insert into province "
  						+" (province_Name,country_Id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
  						+" (?,?,?,?,?,?);";
  				
  				try (
  						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
  						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
  					{
  					
 	 					pstmt.setString(1, province);
 	 					pstmt.setInt(2, countryId);
 	 					pstmt.setInt(3, 11);
 	 					pstmt.setTimestamp(4, getCurrentTimeStamp());
 	 					pstmt.setInt(5, 11);
 	 					pstmt.setTimestamp(6, getCurrentTimeStamp());
 	 					
 	 					int id = pstmt.executeUpdate();
 	 					ResultSet rs = pstmt.getGeneratedKeys();
 	 					if (rs != null && rs.next()) {
 	 						provinceId = rs.getInt(1);
 	 					}
  				} catch (Exception e) {
  					isSuccess = false;
  					log.error("Error message: " + e, e);
  					new ServiceException(Response.Status.CONFLICT);
  				}
  			}else{
  				log.info("provinceId exists:"+provinceId);
  			}
  			
  			
  		
  			
  			
  			/*
  			 * 
  			 * 
  			 *  COMPANY ADDRESS 
  			 * 
  			 */
  			int companyAddressId=0;
  			// check if country already exists
  			sql = "SELECT * FROM `company_address` WHERE "
  				  +" address_Line_1 = ? "
  				  +" and city_Name = ? "
  				  +" and province_Id = ? "
  				  +" and country_Id = ? "
  				  +" and postal_Cd = ? ;";
  			try (
  					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 					PreparedStatement pstmt = connection.prepareStatement(sql);) 
  				{
 				
 				pstmt.setString(1, line1Address);
 				pstmt.setString(2, cityName);
 				pstmt.setInt(3, provinceId);
 				pstmt.setInt(4, countryId);
 				pstmt.setString(5, postalCd);
 				
 				
 		        try (ResultSet rs = pstmt.executeQuery()) {
 					while (rs.next()) {
 						com.openport.marketplace.model.CompanyAddress cmpnyAddress = new com.openport.marketplace.model.CompanyAddress(rs);
 						companyAddressId = cmpnyAddress.getCompanyAddressId();
 			        }
 		         }
 			} catch (Exception e) {
 				log.error("Error message: " + e, e);
 				new ServiceException(Response.Status.CONFLICT);
 			}	
  			
  			// if not exists, then insert
  			if(companyAddressId==0){
  				
  				sql = "insert into company_address "
  						+" (address_Line_1,address_Line_2,city_Name,province_Id,country_Id,postal_Cd,create_User_Id,create_Dt,update_User_Id,update_Dt,company_Address_Id ,latitude, longtitude) VALUES "
  						+" (?,?,?,?,?,?,?,?,?,?,?);";
  				
  				try (
  						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
  						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
  					{
  					
 	 					pstmt.setString(1, line1Address);
 	 					pstmt.setString(2, line2Address);
 	 					pstmt.setString(3, cityName);
 	 					pstmt.setInt(4, provinceId);
 	 					pstmt.setInt(5, countryId);
 	 					pstmt.setString(6, postalCd);
 	 					pstmt.setInt(7, 11);
 	 					pstmt.setTimestamp(8, getCurrentTimeStamp());
 	 					pstmt.setInt(9, 11);
 	 					pstmt.setTimestamp(10, getCurrentTimeStamp());
 	 					pstmt.setInt(11, 3);
 	 				  	pstmt.setBigDecimal(12, latitude);
	 					pstmt.setBigDecimal(13, longtitude);
 	 					
 	 					int id = pstmt.executeUpdate();
 	 					ResultSet rs = pstmt.getGeneratedKeys();
 	 					if (rs != null && rs.next()) {
 	 						companyAddressId = rs.getInt(1);
 	 					}
  				} catch (Exception e) {
  					isSuccess = false;
  					log.error("Error message: " + e, e);
  					new ServiceException(Response.Status.CONFLICT);
  				}
  			}else{
  				log.info("companyAddressId exists:"+companyAddressId);
  			}
  			
  			
  			if(isSuccess){
  				log.info("Success child table data. now insert ");
  			}else{
  				log.info("NOT Success child table data.");
  			}
  		
  		
  			
  			log.info("companyName:"+companyName);
  			log.info("companyCode:"+companyCode);
  			log.info("line1Address:"+line1Address);
  			log.info("cityName:"+cityName);
  			log.info("province:"+province);
  			log.info("provinceId:"+provinceId);
  			log.info("postalCd:"+postalCd);
  			log.info("countryName:"+countryName);
  			log.info("countryId:"+countryId);
  			
  			
  			
  			
  			
  		
  		} catch (Exception e1) {
  			log.error("Error message: " + e1, e1);
  			new ServiceException(Response.Status.BAD_GATEWAY);
  		}
  		return company;
  	}
  	
  	
  	
  	@GET // http://localhost:8080/market-api/serviceAreas/carrierView/84
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/shipperView/{shipperId}")
	public VwCarrierProfile getCarrierView(@PathParam("carrierCode") String carrierCode) {
		
		List<VwCarrierProfile> carrierProfileRecord = new ArrayList<VwCarrierProfile>();
		
		try {
			String sql = "SELECT * FROM `vw_carrier_profile` WHERE company_id = ?;";
	 
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setString(1, carrierCode);

		         try (ResultSet rs = pstmt.executeQuery()) {
					if (rs.next()) {
						com.openport.marketplace.model.VwCarrierProfile svc = new com.openport.marketplace.model.VwCarrierProfile(rs);
						VwCarrierProfile csr = new VwCarrierProfile();
						csr.setCompanyName(svc.getCompanyName());
						csr.setCompanyCode(svc.getCompanyCode());
						
						csr.setBusinessHeadline(svc.getBusinessHeadline());
						csr.setBusinessDescription(svc.getBusinessDescription());
						
						csr.setCompanyAddress(svc.getCompanyAddress());
						csr.setHqProvince(svc.getHqProvince());
						csr.setHqCountry(svc.getHqCountry());
						csr.setTruckPoolAddress(svc.getTruckPoolAddress());
						csr.setBranchAddress(svc.getBranchAddress());
						csr.setContactName(svc.getContactName());
						csr.setEmailAddress(svc.getEmailAddress());
						csr.setPhoneNbr(svc.getPhoneNbr());
						csr.setYearsOfOperation(svc.getYearsOfOperation());
						csr.setCompanyLogoPath(svc.getCompanyLogoPath());
						carrierProfileRecord.add(csr);
			        }
					
					if  (carrierProfileRecord != null && !carrierProfileRecord.isEmpty()) {
					
						return carrierProfileRecord.get(0);
					}
		         }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}			  
		} catch (Exception e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.BAD_GATEWAY);
		}
		
		return null;
	}
  	
  	
  	
  	
  	private CompanyAddress doGetCompanyAddress(int companyAddressId){
		
		
		CompanyAddress companyAddress  = new CompanyAddress();
		
		try {
			
			String sql = 	" SELECT Company_Address_ID, Address_Line_1, Address_Line_2, Address_Line_3, City_Name, c.Province_ID, c.Country_ID, "+
				" Postal_Cd, Years_Of_Operation, Business_Headline, Business_Description, c.Create_User_ID, c.Create_Dt, c.Update_User_ID, c.Update_Dt, "+ 
				" province_name, country_name, latitude, longtitude "+
				" FROM marketplace_db.company_address c "+
				" JOIN marketplace_db.province on c.province_id = province.province_id " +
				" JOIN marketplace_db.country  on c.country_id = country.country_id " +
				" WHERE company_Address_Id  = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setInt(1, companyAddressId);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CompanyAddress> driverRawMapper = new ResultSetMapper<CompanyAddress>();
					List<CompanyAddress> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, CompanyAddress.class);
					if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
						companyAddress = pojoListObj.get(0);
					}
					
			     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return companyAddress;
	}
  	
 	
  	private Company doGetCompanyObjectById(int companyId){
		Company company  = new Company();
		try {
			String sql = 	" SELECT Company_ID, Company_Name, Is_Carrier, Is_Shipper, Company_Code, Corporate_Address_ID, Truck_Pool_Address_ID, Branch_Address_ID, "+
							" Create_User_ID, Create_Dt, Update_User_ID, Update_Dt, Wizard_Step_Nbr, Years_of_operation, Business_Headline, Business_Description,timezone_id "+
							" FROM marketplace_db.company " +
							" WHERE company_Id  = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
					pstmt.setInt(1, companyId);
					try (ResultSet rs = pstmt.executeQuery()) {
						ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
						List<Company> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, Company.class);
						if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
							company = pojoListObj.get(0);
						}
						
				     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return company;
	}
  	
  	private Company doGetCompanyObjectByCode(String companyCode){
		Company company  = new Company();
		try {
			String sql = 	" SELECT Company_ID, Company_Name, Is_Carrier, Is_Shipper, Company_Code, Corporate_Address_ID, Truck_Pool_Address_ID, Branch_Address_ID, "+
							" Create_User_ID, Create_Dt, Update_User_ID, Update_Dt, Wizard_Step_Nbr, Years_of_operation, Business_Headline, Business_Description,timezone_id "+
							" FROM marketplace_db.company " +
							" WHERE Company_Code  = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
					pstmt.setString(1, companyCode);
					try (ResultSet rs = pstmt.executeQuery()) {
						ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
						List<Company> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, Company.class);
						if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
							company = pojoListObj.get(0);
						}
						
				     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return company;
	}
  	
  	
  	private List<Shipment> doGetShipmentAndRate(Integer companyId) {
  		List<Shipment> shipmentListFinal = new ArrayList();
  		//new ServiceException(Response.Status.CONFLICT);
		List<Shipment> shipmentList = doGetShipments(companyId);
		boolean isTest=true;
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress); 
				
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);
				
				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}

				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}
			

				
				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				
	
				
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
							}
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							
							bidCount++;
							
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
						}
						
						shipmentCarrierBidList.add(scb);
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}
					
					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentList;
	}

  	
  	private List<Shipment> doGetShipmentListUsingSearchObj(ShipmentSearch ss, boolean isCompact) {
  		
  		String sp="sp_marketplace_shipments_search";
		List<ShipmentInfo> shipmentList = doGetShipmentsAllDataUsingShipmentSearchObj(ss,sp);
		List<Shipment> shipmentListFinal=	doSetShipmentInfoListToShipmentObj(shipmentList,isCompact);
		return shipmentListFinal;
	}
  	
  	private List<Shipment> doGetShipmentListUsingSearchObjCarrier(ShipmentSearch ss, boolean isCompact) {
  		
  		String sp="sp_marketplace_shipments_search_carrier";
		List<ShipmentInfo> shipmentList = doGetShipmentsAllDataUsingShipmentSearchObj(ss,sp);
		List<Shipment> shipmentListFinal=	doSetShipmentInfoListToShipmentObj(shipmentList,isCompact);
		return shipmentListFinal;
	}
  	
  	
  	private List<Shipment> doGetShipmentPairListUsingSearchObjCarrier(ShipmentSearch ss, boolean isCompact) {
  		
  		String sp="sp_marketplace_shipmentpair_search_carrier";
		List<ShipmentInfo> shipmentList = doGetShipmentsAllDataUsingShipmentSearchObjV2(ss,sp);
		//List<ShipmentInfo> shipmentList = doGetShipmentsPairAllDataUsingShipmentSearchObjV1(ss);
		List<Shipment> shipmentListFinal=	doSetShipmentInfoListToShipmentObj(shipmentList,isCompact);
		return shipmentListFinal;
	}
  	
  	
  	
  	private List<ShipmentInfo> doGetShipmentsPairAllDataUsingShipmentSearchObj(ShipmentSearch shipmentSearch) {
		
		List<ShipmentInfo> shipments = new ArrayList<ShipmentInfo>();
		String sql = 	sqlShipmentAllInfo
						+" WHERE "
						+" s.tender_expiration_dt  is not null "
						+" and (s.tender_expiration_dt > now() or b.carrier_id = ?  ) "
						//+" and 1 = ? "
						+" AND "
						+" ( "
						+" 	( " // opening the group of left side of the OR argument
						+" 		( "
						+"			p.city_Locality like ? "
						+"			or p.line1 like ? "
						+"			or p.street_name like ? "
						+"			or p.province like ? "
						+"			or p.country like ? "
						+"			or p.entity_name like ? "
						+"			)"
						+" 		and ( "
						+"			d.city_Locality like ? "
						+"			or d.line1 like ? "
						+"			or d.street_name like ? "
						+"			or d.province like ? "
						+"			or d.country like ? "
						+"			or d.entity_name like ? "
						+"			)"
		
						// match country of carrier
						+" and "
						+"  ("
						+" 		(	d.country in " 
						+" 				(	select distinct ctry.country_2_cd "
						+" 					from company c join carrier_region cr  on c.company_id=cr.company_id "
						+" 					join service_area sa on cr.service_area_id=sa.service_area_id "
						+" 					join country ctry on sa.Country_ID = ctry.Country_ID "
						+"   				where c.company_id = ? "
						+"  			) "
						/*
						+" 				(	select ctr.country_2_cd "
						+"   				from company c "
						+"   				join company_address a on c.corporate_address_id = a.company_address_id "
						+"   				join country ctr on ctr.country_id=a.country_id "
						+"   				where c.company_id = ? "
						+"  			) "
						*/
						+"  	) "
						+"  	or "
						+"  	( b.carrier_id = ? ) "
						+"  )";
								
		
						//+"	)"
						//+"  or "
						//+"	 ( "
						//+"	 ) "
						//+" )";
						//+" and s.shipment_nbr like ?";
		
		String originTxt = "%";
		if ((shipmentSearch.getOriginTxt()!=null) && (!shipmentSearch.getOriginTxt().equalsIgnoreCase("all"))){
			originTxt = "%" + shipmentSearch.getOriginTxt() + "%";
		};

		String destinationTxt = "%";
		if ((shipmentSearch.getDestinationTxt()!=null)  && (!shipmentSearch.getDestinationTxt().equalsIgnoreCase("all"))){
			destinationTxt = "%" + shipmentSearch.getDestinationTxt() + "%";
		}
		
		String pickupDateVal = "1";
		String pickupDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getPickupDateTxt()!=null)  && (!shipmentSearch.getPickupDateTxt().equalsIgnoreCase("all"))){
			pickupDateSqlWerCondition = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) = ?) ";
			pickupDateVal = shipmentSearch.getPickupDateTxt();
		}
		sql = sql + pickupDateSqlWerCondition ;
		
		String deliveryDateVal = "1";
		String deliveryDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getDeliveryDateTxt()!=null)  && (!shipmentSearch.getDeliveryDateTxt().equalsIgnoreCase("all"))){
			deliveryDateSqlWerCondition = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) = ?) ";
			deliveryDateVal = shipmentSearch.getDeliveryDateTxt();
		}
		sql = sql + deliveryDateSqlWerCondition ;
		
		
		
		String originTxt2 = "%";
		if ((shipmentSearch.getOriginTxt2()!=null) && (!shipmentSearch.getOriginTxt2().equalsIgnoreCase("all"))){
			originTxt2 = "%" + shipmentSearch.getOriginTxt2() + "%";
		};

		String destinationTxt2 = "%";
		if ((shipmentSearch.getDestinationTxt2()!=null)  && (!shipmentSearch.getDestinationTxt2().equalsIgnoreCase("all"))){
			destinationTxt2 = "%" + shipmentSearch.getDestinationTxt2() + "%";
		}
		
		/*
		String statusId = "1";
		String statusSqlWerCondition = " and 1 = ? ";
		if (shipmentSearch.getStatusId()>0){
			statusSqlWerCondition = " and s.status_id = ? ";
			statusId = ""+shipmentSearch.getStatusId();
		}
		sql = sql + statusSqlWerCondition ;
		*/
		
		sql = 	sql  
				+"	)" // closing the group of left side of the OR argument
			  	+"  or "
			  	+" 	( " // opening the group of right side of the OR argument
				+" 		( "
				+"			p.city_Locality like ? "
				+"			or p.line1 like ? "
				+"			or p.street_name like ? "
				+"			or p.province like ? "
				+"			or p.country like ? "
				+"			or p.entity_name like ? "
				+"			)"
				+" 		and ( "
				+"			d.city_Locality like ? "
				+"			or d.line1 like ? "
				+"			or d.street_name like ? "
				+"			or d.province like ? "
				+"			or d.country like ? "
				+"			or d.entity_name like ? "
				+"			)"

				// match country of carrier
				+" and "
				+"  ("
				+" 		(	d.country in " 
				+" 				(	select distinct ctry.country_2_cd "
				+" 					from company c join carrier_region cr  on c.company_id=cr.company_id "
				+" 					join service_area sa on cr.service_area_id=sa.service_area_id "
				+" 					join country ctry on sa.Country_ID = ctry.Country_ID "
				+"   				where c.company_id = ? "
				+"  			) "
				/*
				+" 				(	select ctr.country_2_cd "
				+"   				from company c "
				+"   				join company_address a on c.corporate_address_id = a.company_address_id "
				+"   				join country ctr on ctr.country_id=a.country_id "
				+"   				where c.company_id = ? "
				+"  			) "
				*/
				+"  	) "
				+"  	or "
				+"  	( b.carrier_id = ? ) "
				+"  )";
						
		String pickupDateVal2 = "1";
		String pickupDateSqlWerCondition2 = " and 1 = ? ";
		if ((shipmentSearch.getPickupDateTxt2()!=null)  && (!shipmentSearch.getPickupDateTxt2().equalsIgnoreCase("all"))){
			pickupDateSqlWerCondition2 = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) = ?) ";
			pickupDateVal2 = shipmentSearch.getPickupDateTxt();
		}
		sql = sql + pickupDateSqlWerCondition2 ;
		
		String deliveryDateVal2 = "1";
		String deliveryDateSqlWerCondition2 = " and 1 = ? ";
		if ((shipmentSearch.getDeliveryDateTxt2()!=null)  && (!shipmentSearch.getDeliveryDateTxt2().equalsIgnoreCase("all"))){
			deliveryDateSqlWerCondition2 = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) = ?) ";
			deliveryDateVal2 = shipmentSearch.getDeliveryDateTxt();
		}
		sql = sql + deliveryDateSqlWerCondition2 ;
		
		sql = 	sql  
				+"	)" // closing the group of right side of the OR argument
				+")"; // closing the AND where the OR argument belongs
		
		
			
		
		
		
		String shipmentRefTxt = "%";
		if ((shipmentSearch.getReferenceNbr()!=null) && (!shipmentSearch.getReferenceNbr().equalsIgnoreCase("all"))){
			shipmentRefTxt = "%" + shipmentSearch.getReferenceNbr() + "%";
		};
		
		
		System.out.println("PairShipmentSearch:"+sql);

		
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			
			pstmt.setInt(1, shipmentSearch.getCompanyId());
			//pstmt.setInt(1, 1);
			pstmt.setString(2, originTxt);
			pstmt.setString(3, originTxt);
			pstmt.setString(4, originTxt);
			pstmt.setString(5, originTxt);
			pstmt.setString(6, originTxt);
			pstmt.setString(7, originTxt);
			pstmt.setString(8, destinationTxt);
			pstmt.setString(9, destinationTxt);
			pstmt.setString(10, destinationTxt);
			pstmt.setString(11, destinationTxt);
			pstmt.setString(12, destinationTxt);
			pstmt.setString(13, destinationTxt);
			

			pstmt.setInt(14, shipmentSearch.getCompanyId());
			pstmt.setInt(15, shipmentSearch.getCompanyId());
			
			pstmt.setString(16, pickupDateVal);
			pstmt.setString(17, deliveryDateVal);
			
			
			pstmt.setString(18, originTxt2);
			pstmt.setString(19, originTxt2);
			pstmt.setString(20, originTxt2);
			pstmt.setString(21, originTxt2);
			pstmt.setString(22, originTxt2);
			pstmt.setString(23, originTxt2);
			pstmt.setString(24, destinationTxt2);
			pstmt.setString(25, destinationTxt2);
			pstmt.setString(26, destinationTxt2);
			pstmt.setString(27, destinationTxt2);
			pstmt.setString(28, destinationTxt2);
			pstmt.setString(29, destinationTxt2);
			
			pstmt.setInt(30, shipmentSearch.getCompanyId());
			pstmt.setInt(31, shipmentSearch.getCompanyId());
			
			
			pstmt.setString(32, pickupDateVal2);
			pstmt.setString(33, deliveryDateVal2);
			//pstmt.setString(30, statusId);
			
			
			
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentInfo> driverRawMapper = new ResultSetMapper<ShipmentInfo>();
	        	List<ShipmentInfo> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentInfo.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		System.out.println("ShipmentInfoListSize:"+pojoList.size());
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	private List<Shipment> doGetShipmentAndRateV2(Integer companyId) {
  		String sp="sp_marketplace_shipments_all_data";
		String sql = 	 " call " + sp + " (?) ";
		log.debug(sql+" companyId:"+companyId);
		List<ShipmentInfo> shipmentList = doGetShipmentsAllData(companyId,sql);
		List<Shipment> shipmentListFinal =  doSetShipmentInfoListToShipmentObj(shipmentList,false);
		return shipmentListFinal;
	}
  	
  	private List<Shipment> doSetShipmentInfoListToShipmentObj(List<ShipmentInfo> shipmentList, boolean isCompact
  			// , int companyId, boolean isCarrier 
  			) {
  		List<Shipment> shipmentListFinal = new ArrayList();
  		try {
	  		
	  		//new ServiceException(Response.Status.CONFLICT);
  			List shipmentServiceIdList =  new ArrayList();
			List shipmentProcessedStartRecord =  new ArrayList();
			List shipmentDetailIdList =  new ArrayList();
			List shipmentCarrierBidIdList =  new ArrayList();
			List shipmentCarrierBidDetailAccessorialCdList = new ArrayList();
			Shipment shipmentObj = new Shipment();
			boolean hasBegun = false;
			if(shipmentList!=null && shipmentList.size()>0){
				for(ShipmentInfo shpt : shipmentList) {
					
					 /*
					 System.out.println("shipmentNbr:"+shpt.getShipmentNbr());
					 System.out.println("getShipmentId:"+shpt.getShipmentId());
					 System.out.println("shpt.getPickupLine1():"+shpt.getPickupLine1());
					 System.out.println("shpt.getPickupEntityName():"+shpt.getPickupEntityName());
					 System.out.println("shpt.getPickupEntityAddressId():"+shpt.getPickupEntityAddressId());
					 
					 System.out.println("shpt.getDeliveryLine1():"+shpt.getDeliveryLine1());
					 System.out.println("shpt.getDeliveryEntityName():"+shpt.getDeliveryEntityName());
					 System.out.println("shpt.getDeliveryEntityAddressId():"+shpt.getDeliveryEntityAddressId());
					 
					 System.out.println("shpt.getShipmentCarrierBidId():"+shpt.getShipmentCarrierBidId());
					 System.out.println("shpt.getQuoteAmt():"+shpt.getQuoteAmt());
					
		
					
					 System.out.println("shpt.getLine1():"+shpt.getLine1());
					 System.out.println("shpt.getEntityName():"+shpt.getEntityName());
					 System.out.println("shpt.getEntityAddressId():"+shpt.getEntityAddressId());
					 
					 System.out.println("shpt.getEquipmentTypeId():"+shpt.getEquipmentTypeId());
					 System.out.println("shpt.getShipmentDetailId():"+shpt.getShipmentDetailId());
					 System.out.println("shpt.getPaymentTermId():"+shpt.getPaymentTermId());
					 */
					 
					
						
					if(!shipmentProcessedStartRecord.contains(shpt.getShipmentId())){
						System.out.println("new Shipment:"+shpt.getShipmentId());
						if(hasBegun){
							shipmentListFinal.add(shipmentObj);
							System.out.println("shipmentListFinal.add:"+shipmentObj.getShipmentId());
						}else{
							hasBegun=true;
						}
						shipmentObj = new Shipment();
						shipmentObj = doSetShipmentNonRepeatingInformation(shpt,shipmentObj,isCompact);
						shipmentCarrierBidDetailAccessorialCdList = new ArrayList();
						System.out.println("shipmentNbr:"+shipmentObj.getShipmentNbr());
						//System.out.println("shpt.getPickupLine1():"+shpt.getPickupLine1());
						//System.out.println("shpt.getDeliveryLine1():"+shpt.getDeliveryLine1()+" uid:"+shpt.getDeliveryEntityAddressId());
						//EntityAddress e = shipmentObj.getPickupAddress();
			  			//System.out.println("pickup:"+e.getLine1());
			  			//EntityAddress d = shipmentObj.getDeliveryAddress();
			  			//System.out.println("delivery:"+d.getLine1());
						shipmentProcessedStartRecord.add(shpt.getShipmentId());
					}
					
					System.out.println("here doSetShipmentDetailObjData");
					if ((shpt.getShipmentDetailId()!=null) && (!shipmentDetailIdList.contains(shpt.getShipmentDetailId()))){
						System.out.println("here doSetShipmentDetailObjData pasok si.getUnitOfMeasureDtl():"+shpt.getUnitOfMeasure());
						shipmentDetailIdList.add(shpt.getShipmentDetailId());
						shipmentObj=doSetShipmentDetailObjData(shpt,shipmentObj,isCompact);
					}
					
					
					if (shpt.getServiceId()!=null){
						String currServiceFlg = shipmentObj.getShipmentId()+""+shpt.getServiceId();
						if ((shpt.getServiceId()!=null) && (!shipmentServiceIdList.contains(currServiceFlg))){
							shipmentServiceIdList.add(currServiceFlg);
							shipmentObj=doSetShipmentServiceObjData(shpt,shipmentObj,isCompact);
						}
					}
					
					/*
					// REPLACED 20170219O
					if ((shpt.getShipmentCarrierBidId()!=null) && (!shipmentCarrierBidIdList.contains(shpt.getShipmentCarrierBidId()))){
						shipmentCarrierBidIdList.add(shpt.getShipmentCarrierBidId());
						if(isCarrier){
							if (shpt.getCarrierId()	 == companyId){
								doSetShipmentCarrierBidObjData(shpt,shipmentObj);
							}
	
						}else{
							doSetShipmentCarrierBidObjData(shpt,shipmentObj);
						}
					}
					*/
					if ((shpt.getShipmentCarrierBidId()!=null) 
							&& (shpt.getShipmentCarrierBidId()>-1) 
							&& (!shipmentCarrierBidIdList.contains(shpt.getShipmentCarrierBidId()))){
						shipmentCarrierBidIdList.add(shpt.getShipmentCarrierBidId());
						shipmentObj = doSetShipmentCarrierBidObjData(shpt,shipmentObj,isCompact);
					}
					
					
					
					if (
							    (!isCompact)
								&& (shpt.getShipmentCarrierBidId()!=null) 
								&& (shipmentObj.getShipmentCarrierBids()!=null)
								&& (shipmentObj.getShipmentCarrierBids().size()>0)
								&& (shpt.getAccessorialCdBidDetail()!=null) 
								&& (!shipmentCarrierBidDetailAccessorialCdList.contains(shpt.getAccessorialCdBidDetail()))
							 ){
								shipmentCarrierBidDetailAccessorialCdList.add(shpt.getAccessorialCdBidDetail());
								doSetShipmentCarrierBidDetailObjData(shpt,shipmentObj);
					}
				}
				
			}else{
				return shipmentListFinal;
			}
			if(shipmentObj!=null){
				shipmentListFinal.add(shipmentObj);
			}
		System.out.println("list size:"+shipmentListFinal.size());
  		} catch (Exception e) {
			log.error(e,e);
		}
		return shipmentListFinal;
	}
  	
  	
  	private List<Shipment> doSetShipmentInfoListToShipmentObjCarrierOrShipper(List<ShipmentInfo> shipmentList, boolean isCompact
  			, boolean isCarrier
  			, int companyId 
  			) {
  		List<Shipment> shipmentListFinal = new ArrayList();
  		try {
	  		
	  		//new ServiceException(Response.Status.CONFLICT);
			List shipmentProcessedStartRecord =  new ArrayList();
			List shipmentDetailIdList =  new ArrayList();
			List shipmentCarrierBidIdList =  new ArrayList();
			List shipmentCarrierBidDetailAccessorialCdList = new ArrayList();
			Shipment shipmentObj = new Shipment();
			boolean hasBegun = false;
			if(shipmentList!=null && shipmentList.size()>0){
				for(ShipmentInfo shpt : shipmentList) {
					
					 System.out.println("shipmentNbr:"+shpt.getShipmentNbr());
					 System.out.println("getShipmentId:"+shpt.getShipmentId());
					 System.out.println("shpt.getPickupLine1():"+shpt.getPickupLine1());
					 System.out.println("shpt.getPickupEntityName():"+shpt.getPickupEntityName());
					 System.out.println("shpt.getPickupEntityAddressId():"+shpt.getPickupEntityAddressId());
					 
					 System.out.println("shpt.getDeliveryLine1():"+shpt.getDeliveryLine1());
					 System.out.println("shpt.getDeliveryEntityName():"+shpt.getDeliveryEntityName());
					 System.out.println("shpt.getDeliveryEntityAddressId():"+shpt.getDeliveryEntityAddressId());
					 
					 System.out.println("shpt.getShipmentCarrierBidId():"+shpt.getShipmentCarrierBidId());
					 System.out.println("shpt.getQuoteAmt():"+shpt.getQuoteAmt());
					
		
					 /*
					 System.out.println("shpt.getLine1():"+shpt.getLine1());
					 System.out.println("shpt.getEntityName():"+shpt.getEntityName());
					 System.out.println("shpt.getEntityAddressId():"+shpt.getEntityAddressId());
					 
					 System.out.println("shpt.getEquipmentTypeId():"+shpt.getEquipmentTypeId());
					 System.out.println("shpt.getShipmentDetailId():"+shpt.getShipmentDetailId());
					 System.out.println("shpt.getPaymentTermId():"+shpt.getPaymentTermId());
					 */
					 
					
						
					if(!shipmentProcessedStartRecord.contains(shpt.getShipmentId())){
						System.out.println("new Shipment:"+shpt.getShipmentId());
						if(hasBegun){
							shipmentListFinal.add(shipmentObj);
							System.out.println("shipmentListFinal.add:"+shipmentObj.getShipmentId());
						}else{
							hasBegun=true;
						}
						shipmentObj = new Shipment();
						shipmentObj = doSetShipmentNonRepeatingInformation(shpt,shipmentObj,isCompact);
						shipmentCarrierBidDetailAccessorialCdList = new ArrayList();
						System.out.println("shipmentNbr:"+shipmentObj.getShipmentNbr());
						System.out.println("shpt.getPickupLine1():"+shpt.getPickupLine1());
						System.out.println("shpt.getDeliveryLine1():"+shpt.getDeliveryLine1()+" uid:"+shpt.getDeliveryEntityAddressId());
						//EntityAddress e = shipmentObj.getPickupAddress();
			  			//System.out.println("pickup:"+e.getLine1());
			  			//EntityAddress d = shipmentObj.getDeliveryAddress();
			  			//System.out.println("delivery:"+d.getLine1());
						shipmentProcessedStartRecord.add(shpt.getShipmentId());
					}
					
					System.out.println("here doSetShipmentDetailObjData");
					if ((shpt.getShipmentDetailId()!=null) && (!shipmentDetailIdList.contains(shpt.getShipmentDetailId()))){
						shipmentDetailIdList.add(shpt.getShipmentDetailId());
						doSetShipmentDetailObjData(shpt,shipmentObj,isCompact);
						
					}
					
					/*
					// REPLACED 20170219O
					if ((shpt.getShipmentCarrierBidId()!=null) && (!shipmentCarrierBidIdList.contains(shpt.getShipmentCarrierBidId()))){
						shipmentCarrierBidIdList.add(shpt.getShipmentCarrierBidId());
						if(isCarrier){
							if (shpt.getCarrierId()	 == companyId){
								doSetShipmentCarrierBidObjData(shpt,shipmentObj);
							}
	
						}else{
							doSetShipmentCarrierBidObjData(shpt,shipmentObj);
						}
					}
					*/
					if ((shpt.getShipmentCarrierBidId()!=null) 
							&& (shpt.getShipmentCarrierBidId()>-1) 
							&& (!shipmentCarrierBidIdList.contains(shpt.getShipmentCarrierBidId()))){
						
						// added 20170320
						// need this to show the bid amt on carrier's view
						if(isCarrier){
							if (shpt.getCarrierId()	 == companyId){
								shipmentObj = doSetShipmentCarrierBidObjData(shpt,shipmentObj,isCompact);
								shipmentObj.setCarrierBid(shipmentObj.getShipmentCarrierBids().get(0).getQuoteAmt());
								shipmentObj.setBidRank(shipmentObj.getShipmentCarrierBids().get(0).getBidRank());
							}
	
						}else{
							shipmentObj = doSetShipmentCarrierBidObjData(shpt,shipmentObj,isCompact);
						}
						
						shipmentCarrierBidIdList.add(shpt.getShipmentCarrierBidId());
						//shipmentObj = doSetShipmentCarrierBidObjData(shpt,shipmentObj,isCompact);
					}
					
					
					
					if (
							    (!isCompact)
								&& (shpt.getShipmentCarrierBidId()!=null) 
								&& (shipmentObj.getShipmentCarrierBids()!=null)
								&& (shipmentObj.getShipmentCarrierBids().size()>0)
								&& (shpt.getAccessorialCdBidDetail()!=null) 
								&& (!shipmentCarrierBidDetailAccessorialCdList.contains(shpt.getAccessorialCdBidDetail()))
							 ){
								shipmentCarrierBidDetailAccessorialCdList.add(shpt.getAccessorialCdBidDetail());
								doSetShipmentCarrierBidDetailObjData(shpt,shipmentObj);
					}
				}
				
			}else{
				return shipmentListFinal;
			}
			if(shipmentObj!=null){
				shipmentListFinal.add(shipmentObj);
			}
		System.out.println("list size:"+shipmentListFinal.size());
  		} catch (Exception e) {
			log.error(e,e);
		}
		return shipmentListFinal;
	}
  	
  	
  	private Shipment doSetShipmentCarrierBidDetailObjData(ShipmentInfo si, Shipment s) {
  		
  		System.out.println("here doSetShipmentCarrierBidDetailObjData");
		try {
			boolean withRecord =false;
			ShipmentCarrierBid scb = s.getShipmentCarrierBids().get(s.getShipmentCarrierBids().size()-1);
			List<ShipmentCarrierBidDetail> sdCb = new ArrayList();
			if(( scb.getShipmentCarrierBidDetails()!=null) && ( scb.getShipmentCarrierBidDetails().size()>0)){
				sdCb = scb.getShipmentCarrierBidDetails();
			}
			
			ShipmentCarrierBidDetail x = new ShipmentCarrierBidDetail();
			//x.setIsShipperDefined(si.getIsShipperDefined());
			//x.setCreateDt(si.getCreateDt());
			//x.setUpdateDt(si.getUpdateDt());
			x.setAccessorialAmt(si.getAccessorialAmtBidDetail());
			x.setCarrierId(si.getCarrierIdBidDetail());
			//x.setShipmentCarrierBidId(si.getShipmentCarrierBidIdBidDetail());
			//x.setShipmentId(si.getShipmentIdBidDetail());
			x.setShipperDefinedFlg(si.getShipperDefinedFlgBidDetail());
			//x.setAccessorialCcy(si.getAccessorialCcyBidDetail());
			x.setAccessorialCd(si.getAccessorialCdBidDetail());
			x.setAccessorialDataSource(si.getAccessorialDataSourceBidDetail());
			x.setAccessorialDescription(si.getAccessorialDescriptionBidDetail());
			x.setAccessorialRateComment(si.getAccessorialRateCommentBidDetail());
			x.setAccessorialType(si.getAccessorialType());
			x.setShipperDefined(true);

			sdCb.add(x);
			System.out.println("here doSetShipmentCarrierBidDetailObjData setting carrierBidDetails ");
			s.getShipmentCarrierBids().get(s.getShipmentCarrierBids().size()-1).setShipmentCarrierBidDetails(sdCb);
			
		}catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		
		

		return s;
	
  		
  		
  	}


	private Shipment doSetShipmentNonRepeatingInformation(ShipmentInfo shpt, Shipment shipmentObj, boolean isCompact) {
  		
  		try {
  			System.out.println("here doSetShipmentObjData");
  	  		shipmentObj = doSetShipmentObjData(shpt,shipmentObj);
  	  		System.out.println("here doSetEntityAddressObjData");
  	  		shipmentObj = doSetEntityAddressObjData(shpt,shipmentObj);
  	  		System.out.println("here doSetEquipmentObjData");
  	  		shipmentObj = doSetEquipmentObjData(shpt,shipmentObj);
  	  		System.out.println("here doSetPaymentTermObjData");
  	  		shipmentObj = doSetPaymentTermObjData(shpt,shipmentObj);
			
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
  		
		return shipmentObj;
	}


	private Shipment doSetPaymentTermObjData(ShipmentInfo si, Shipment s) {
		try {
			PaymentTerm x = new PaymentTerm();
			x.setPaymentTermCd(si.getPaymentTermCd());
			x.setPaymentTerm(si.getPaymentTermName());
			s.setPaymentTerm(x);
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		

		return s;
	}


	private Shipment doSetEquipmentObjData(ShipmentInfo si, Shipment s) {
		try {
			EquipmentType e = new EquipmentType();
			e.setEquipmentTypeId(si.getEquipmentTypeId());
			e.setEquipmentTypeName(si.getEquipmentTypeName());
			s.setEquipmentType(e);
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		

		return s;
	}


	private Shipment doSetEntityAddressObjData(ShipmentInfo si, Shipment s) {
		// PICKUP
		try {
			EntityAddress pu = new EntityAddress();
			//pu.setEntityAddressId(si.getPickupEntityAddressId());
			//pu.setLine1(si.getPickupLine1());
			pu.setLat(si.getPickupLat());
			pu.setLon(si.getPickupLon());
			// pu.setIsDc(si.getPickupIsDc());
			pu.setEntityAddressId(si.getPickupEntityAddressId());
			pu.setBuilding(si.getPickupBuilding());
			pu.setCityLocality(si.getPickupCityLocality());
			pu.setCountry(si.getPickupCountry());
			pu.setEntityName(si.getPickupEntityName());
			pu.setFloorApartment(si.getPickupFloorApartment());
			pu.setHouseNumber(si.getPickupHouseNumber());
			pu.setLatLongStatusCd(si.getPickupLatLongStatusCd());
			pu.setLine1(si.getPickupLine1());
			pu.setDistrictName(si.getPickupDistrictName());
			pu.setPostalCode(si.getPickupPostalCode());
			pu.setProvince(si.getPickupProvince());
			pu.setStreetName(si.getPickupStreetName());
			System.out.println("doSetEntityAddressObjData >>> si.getPickupLine1():"+si.getPickupLine1());
			 
			s.setPickupAddress(pu);
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		
		// DELIVERY
		try {
			EntityAddress d = new EntityAddress();
			//d.setEntityAddressId(si.getDeliveryEntityAddressId());
			//d.setLine1(si.getDeliveryLine1());
			int ctr=0;
			d.setLat(si.getDeliveryLat());
			d.setLon(si.getDeliveryLon());
			d.setEntityAddressId(si.getDeliveryEntityAddressId());
			d.setBuilding(si.getDeliveryBuilding());
			d.setCityLocality(si.getDeliveryCityLocality());
			d.setCountry(si.getDeliveryCountry());
			d.setEntityName(si.getDeliveryEntityName());
			d.setFloorApartment(si.getDeliveryFloorApartment());
			d.setHouseNumber(si.getDeliveryHouseNumber());			
			d.setLatLongStatusCd(si.getDeliveryLatLongStatusCd());			
			d.setLine1(si.getDeliveryLine1());			
			d.setDistrictName(si.getDeliveryDistrictName());		
			// d.setIsDc(si.getDeliveryIsDc());
			d.setPostalCode(si.getDeliveryPostalCode());
			d.setProvince(si.getDeliveryProvince());
			d.setStreetName(si.getDeliveryStreetName());
			// System.out.println("doSetEntityAddressObjData >>> si.getDeliveryLine1():"+si.getDeliveryLine1());
			s.setDeliveryAddress(d);
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}

		return s;
	}


	private Shipment doSetShipmentObjData(ShipmentInfo si, Shipment s) {
		
		try {
			/*
			s.setAvgBidAmt(si.getAvgBidAmt());
			s.setDestWaitTm(si.getDestWaitTm());
			s.setHighEndBidAmt(si.getHighEndBidAmt());
			s.setLowEndBidAmt(si.getLowEndBidAmt());
			s.setOriginWaitTm(si.getOriginWaitTm());
			s.setTargetRateAmt(si.getTargetRateAmt());
			s.setUnitCount(si.getUnitCount());
			s.setUnitVol(si.getUnitVol());
			s.setUnitWt(si.getUnitWt());
			s.setAssignedProviderId(si.getAssignedProviderId());
			s.setDeliveryAddressId(si.getDeliveryAddressId());
			s.setDestinationServiceAreaId(si.getDestinationServiceAreaId());
			s.setOriginServiceAreaId(si.getOriginServiceAreaId());
			s.setPaymentTermId(si.getPaymentTermId());
			s.setPickupAddressId(si.getPickupAddressId());
			//s.setRateCount(si.getRateCount());
			s.setRequestedEquipmentTypeId(si.getRequestedEquipmentTypeId());
			s.setShipmentId(si.getShipmentId());
			s.setShipperId(si.getShipperId());
			s.setStatusId(si.getStatusId());
			s.setCommodityDescTxt(si.getCommodityDescTxt());
			s.setCreateUserId(si.getCreateUserId());
			s.setCurrencyCd(si.getCurrencyCd());
			s.setDestWaitTmUom(si.getDestWaitTmUom());
			s.setOriginWaitTmUom(si.getOriginWaitTmUom());
			s.setProBillNbr(si.getProBillNbr());
			s.setShipmentNbr(si.getShipmentNbr());
			s.setTenderTypeCd(si.getTenderTypeCd());
			s.setUnitOfMeasure(si.getUnitOfMeasure());
			s.setUpdateUserId(si.getUpdateUserId());
			s.setVendorId(si.getVendorId());
			s.setCreateDt(si.getCreateDt());
			s.setRequestedDeliveryDt(si.getRequestedDeliveryDt());
			s.setRequestedPickupDt(si.getRequestedPickupDt());
			s.setTenderExpirationDt(si.getTenderExpirationDt());
			s.setUpdateDt(si.getUpdateDt());
			*/
			//s.setRateCount(si.getRateCount());
			s.setAssignedProviderId(-1);
			// s.setAvgBidAmt(si.getAvgBidAmt());
			s.setCommodityDescTxt(si.getCommodityDescTxt());
			s.setCreateDt(si.getCreateDt());
			s.setCreateUserId(si.getCreateUserId());
			s.setCurrencyCd(si.getCurrencyCd());
			s.setDeliveryAddressId(si.getDeliveryAddressId());
			//s.setDestinationServiceAreaId(si.getDestinationServiceAreaId());
			//s.setDestWaitTm(si.getDestWaitTm());
			//s.setDestWaitTmUom(si.getDestWaitTmUom());
			s.setHighEndBidAmt(si.getHighEndBidAmt());
			s.setLowEndBidAmt(si.getLowEndBidAmt());
			//s.setOriginServiceAreaId(si.getOriginServiceAreaId());
			//s.setOriginWaitTm(si.getOriginWaitTm());
			//s.setOriginWaitTmUom(si.getOriginWaitTmUom());
			s.setPaymentTermId(si.getPaymentTermId());
			s.setPickupAddressId(si.getPickupAddressId());
			s.setProBillNbr(si.getProBillNbr());
			s.setRequestedDeliveryDt(si.getRequestedDeliveryDt());
			s.setRequestedEquipmentTypeId(si.getRequestedEquipmentTypeId());
			s.setRequestedPickupDt(si.getRequestedPickupDt());
			
			if(si.getRequestedPickupDt()!=null){
				Calendar c = Calendar.getInstance();
				c.setTime(si.getRequestedPickupDt());
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				s.setPickupDt(""+dayOfWeek);
			}
			
			s.setShipmentId(si.getShipmentId());
			s.setShipmentNbr(si.getShipmentNbr());
			s.setShipperId(si.getShipperId());
			s.setStatusId(si.getStatusId());
			s.setTargetRateAmt(si.getTargetRateAmt());
			s.setTenderExpirationDt(si.getTenderExpirationDt());
			//s.setTenderTypeCd(si.getTenderTypeCd());
			s.setUnitCount(si.getUnitCount());
			s.setUnitOfMeasure(si.getUnitOfMeasure());
			//s.setUnitVol(si.getUnitVol());
			//s.setUnitWt(si.getUnitWt());
			s.setUpdateDt(si.getUpdateDt());
			s.setUpdateUserId(si.getUpdateUserId());
			//s.setVendorId(si.getVendorId());

			s.setRateCount(0);
			s.setRateType("");
			s.setBidRank(0);
			s.setSi(si.getSi());
			s.setShptType(si.getShptType());
			s.setStopCnt(si.getStopCnt());

			/*
			System.out.println("si.getCommodityDescTxt():"+si.getCommodityDescTxt());
			System.out.println("si.getCreateDt():"+si.getCreateDt());
			System.out.println("si.getCreateUserId():"+si.getCreateUserId());
			System.out.println("si.getCurrencyCd():"+si.getCurrencyCd());
			System.out.println("si.getDeliveryAddressId():"+si.getDeliveryAddressId());
			System.out.println("si.getHighEndBidAmt():"+si.getHighEndBidAmt());
			System.out.println("si.getLowEndBidAmt():"+si.getLowEndBidAmt());
			System.out.println("si.getPaymentTermId():"+si.getPaymentTermId());
			System.out.println("si.getPickupAddressId():"+si.getPickupAddressId());
			System.out.println("si.getProBillNbr():"+si.getProBillNbr());
			System.out.println("si.getRequestedDeliveryDt():"+si.getRequestedDeliveryDt());
			System.out.println("si.getRequestedEquipmentTypeId():"+si.getRequestedEquipmentTypeId());
			System.out.println("si.getRequestedPickupDt():"+si.getRequestedPickupDt());
			System.out.println("si.getShipmentId():"+si.getShipmentId());
			System.out.println("si.getShipmentNbr():"+si.getShipmentNbr());
			System.out.println("si.getShipperId():"+si.getShipperId());
			System.out.println("si.getStatusId():"+si.getStatusId());
			System.out.println("si.getTargetRateAmt():"+si.getTargetRateAmt());
			System.out.println("si.getTenderExpirationDt():"+si.getTenderExpirationDt());
			System.out.println("si.getCurrencyCd():"+si.getUnitCount());
			System.out.println("si.getUnitOfMeasure():"+si.getUnitOfMeasure());
			System.out.println("si.getUpdateDt():"+si.getUpdateDt());
			System.out.println("si.getUpdateUserId():"+si.getUpdateUserId());
			*/
			
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		
		

		return s;
	}
	
	
	private Shipment doSetShipmentDetailObjData(ShipmentInfo si, Shipment s, boolean isCompact) {
		
		try {
			List<ShipmentDetail> sdList = new ArrayList();
			if(s.getShipmentDetails()!=null){
				sdList = s.getShipmentDetails();
			}
			
			ShipmentDetail x = new ShipmentDetail();
			x.setQty(si.getQty());
			x.setQtyUom(si.getUnitOfMeasureDtl());
			x.setShipmentHt(si.getShipmentHt());
			x.setShipmentLen(si.getShipmentLen());
			x.setShipmentVol(si.getShipmentVol());
			x.setShipmentWt(si.getShipmentWt());
			x.setShipmentWth(si.getShipmentWth());
			x.setShipmentDetailId(si.getShipmentDetailId());
			x.setDimensionUom(si.getDimensionUom());
			x.setFreightClassCd(si.getFreightClassCd());
			x.setProductDescTxt(si.getProductDescTxt());
			x.setUnitOfMeasure(si.getUnitOfMeasureDtl());
			x.setVolUom(si.getVolUom());
			x.setWtUom(si.getWtUom());
			x.setIsHazardous(si.getIsHazardous());
			x.setUnitOfMeasure(si.getUnitOfMeasureDtl());
			x.setStopNbr(si.getStopNbr());
			x.setStopLocName(si.getStopLocName());
			x.setStopAddr(si.getStopAddr());
			x.setStopCity(si.getStopCity());
			x.setStopProvince(si.getStopProvince());
			x.setStopCountry(si.getStopCountry());
			x.setStopPostal(si.getStopPostal());
			x.setStopPonbr(si.getStopPostal());
			x.setStopRlsnbr(si.getStopRlsnbr());
			
			
			System.out.println("x.getQtyUom():"+x.getQtyUom());
			
			if(!isCompact){
				sdList.add(x);
				s.setShipmentDetails(sdList);
			}
			
			if(s.getShipmentDetail()==null){
				s.setShipmentDetail(x);
			}

			
			
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		

		return s;
	}
	
	private Shipment doSetShipmentServiceObjData(ShipmentInfo si, Shipment s, boolean isCompact) {
		
		try {
			List<Service> sList = new ArrayList();
			if(s.getServices()!=null){
				sList = s.getServices();
			}
			
			Service x = new Service();
			x.setServiceId(si.getServiceId());
			x.setServiceName(si.getServiceName());
			
			if(!isCompact){
				sList.add(x);
				s.setServices(sList);
			}
			

			
			
		} catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		

		return s;
	}
	
	
	
	
	private Shipment doSetShipmentCarrierBidObjData(ShipmentInfo si, Shipment s, boolean compact) {
		
		try {
			boolean withRecord =false;
			List<ShipmentCarrierBid> sdCb = new ArrayList();
			if(s.getShipmentCarrierBids()!=null){
				System.out.println("here s.getShipmentCarrierBids() != null:");
				sdCb = s.getShipmentCarrierBids();
				withRecord =true;
			}
			
			ShipmentCarrierBid x = new ShipmentCarrierBid();
			x.setQuoteDatestamp(si.getQuoteDatestamp());
			x.setQuoteAmt(si.getQuoteAmt());
			x.setTransitTm(si.getTransitTm());
			x.setCarrierId(si.getCarrierId());
			x.setQuoteExpirationValue(si.getQuoteExpirationValue());
			x.setBidRank(si.getBidRank());
			x.setShipmentCarrierBidId(si.getShipmentCarrierBidId());
			x.setBidEnded(si.getBidEnded());
			x.setCarrierCd(si.getCarrierCd());
			x.setCarrierName(si.getCarrierName());
			x.setCarrierRating(si.getCarrierRating());
			x.setCommentTxt(si.getCommentTxt());
			x.setQuoteCurrencyCd(si.getQuoteCurrencyCd());
			x.setQuoteExpirationUom(si.getQuoteExpirationUom());
			x.setQuoteStatusCd(si.getQuoteStatusCd());
			x.setRateTypeCd(si.getRateTypeCd());
			x.setRateTypeDescTxt(si.getRateTypeDescTxt());
			
			if(!compact){
				sdCb.add(x);
				s.setShipmentCarrierBids(sdCb);
			}
			
			Double lowestCost = s.getLowestCost();
			String lowestCostCurrency = s.getLowestCostCurrency();
			
			if((lowestCost==null) || (lowestCost>x.getQuoteAmt())){
				lowestCost = x.getQuoteAmt();
				lowestCostCurrency = x.getQuoteCurrencyCd();
			}
			
			List rateTypeList = new ArrayList();
			if ((x.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(x.getRateTypeDescTxt()))){
				rateTypeList.add(x.getRateTypeDescTxt());
			}
			
			String rateType = "In Process"; 
			if(rateTypeList.size()>0){
				for (Object r: rateTypeList){
					if (rateType.trim().length()>0){
						rateType = rateType + "," + r ;
					}else{
						rateType = r.toString();
					}
				}
			}
			
			
			int bidCount = 1;
			try {
				bidCount = s.getRateCount() + 1;
			} catch (Exception e) {
				System.out.println(e);
			}
			
			s.setLowestCostCurrency(lowestCostCurrency);
			s.setRateType(rateType);
			s.setLowestCost(lowestCost);
			s.setRateCount(bidCount);
			s.setCarrierBid(si.getQuoteAmt());
		}catch (Exception e) {
			log.error(e,e);
			System.out.println(e);
		}
		
		

		return s;
	}


	private List<ShipmentInfo> doGetShipmentsAllData(Integer companyId, String sql) {
		// TODO Auto-generated method stub
  			
  			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
  					PreparedStatement pstmt = connection.prepareStatement(sql);) {

				pstmt.setInt(1, companyId);
  				
  				try (ResultSet rs = pstmt.executeQuery()) {
  					ResultSetMapper<ShipmentInfo> driverRawMapper = new ResultSetMapper<ShipmentInfo>();
  		        	List<ShipmentInfo> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentInfo.class);
  		        	if (pojoList!=null && !pojoList.isEmpty()) {
  		        		return pojoList;
  					}
  				}
  			} catch (Exception e) {
  				log.error("Error message: " + e, e);
  				new ServiceException(Response.Status.CONFLICT);
  			}
  			return new ArrayList();
	}
	
	private List<ShipmentInfo> doGetShipmentsAllDataUsingShipmentSearchObj(ShipmentSearch ss, String sp) {
		// TODO Auto-generated method stub
  			
			/*
			 *  call sp_marketplace_shipments_search (  
                                      'all', -- origin VARCHAR(100) ,
                                      'all', -- destination VARCHAR(100) ,
                                      'all', -- pickupDateTxt1 VARCHAR(100) ,
                                      'all', -- pickupDateTxt2 VARCHAR(100) ,
									  'all', -- deliveryDateTxt1 VARCHAR(100) ,
                                      'all', -- deliveryDateTxt2 VARCHAR(100) ,
                                      'all', -- referenceNbr VARCHAR(100) ,
                                      '-2', -- statusId VARCHAR(100) ,
                                      '116' -- companyId VARCHAR(100) 
                                      );
			 */
		    
		    String sql = 	 " call " + sp + " (?,?,?,?,?,?,?,?,?) ";
  			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
  					PreparedStatement pstmt = connection.prepareStatement(sql);) {

  				
  				String originText =  allIfNull(ss.getOriginTxt());
  				String destinationTxt =  allIfNull(ss.getDestinationTxt());
  				String pickupDateTxt =  allIfNull(ss.getPickupDateTxt());
  				String pickupDateTxt2 =  allIfNull(ss.getPickupDateTxt2());
  				String deliveryDateTxt =  allIfNull(ss.getDeliveryDateTxt());
  				String deliveryDateTxt2 =  allIfNull(ss.getDeliveryDateTxt2());
  				String referenceNbr =  allIfNull(ss.getReferenceNbr());
  				int statusId =  dfltIfNull(ss.getStatusId(),-2);
  				int companyId =  dfltIfNull(ss.getCompanyId(),-1);
  				
				pstmt.setString(1, originText);
				pstmt.setString(2, destinationTxt);
				pstmt.setString(3, pickupDateTxt);
				pstmt.setString(4, pickupDateTxt2);
				pstmt.setString(5, deliveryDateTxt);
				pstmt.setString(6, deliveryDateTxt2);
				pstmt.setString(7, referenceNbr);
				pstmt.setInt(8, statusId);
				pstmt.setInt(9, companyId);
  				
  				try (ResultSet rs = pstmt.executeQuery()) {
  					ResultSetMapper<ShipmentInfo> driverRawMapper = new ResultSetMapper<ShipmentInfo>();
  		        	List<ShipmentInfo> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentInfo.class);
  		        	if (pojoList!=null && !pojoList.isEmpty()) {
  		        		System.out.println("ShipmentInfoListSize:"+pojoList.size());
  		        		return pojoList;
  					}
  				}
  			} catch (Exception e) {
  				log.error("Error message: " + e, e);
  				new ServiceException(Response.Status.CONFLICT);
  			}
  			return new ArrayList();
	}
	
	
	private List<ShipmentInfo> doGetShipmentsAllDataUsingShipmentSearchObjV2(ShipmentSearch ss, String sp) {
		// TODO Auto-generated method stub
  			
			/*
			 *  call sp_marketplace_shipments_search (  
                                      'all', -- origin VARCHAR(100) ,
                                      'all', -- destination VARCHAR(100) ,
                                      'all', -- pickupDateTxt1 VARCHAR(100) ,
                                      'all', -- pickupDateTxt2 VARCHAR(100) ,
									  'all', -- deliveryDateTxt1 VARCHAR(100) ,
                                      'all', -- deliveryDateTxt2 VARCHAR(100) ,
                                      'all', -- referenceNbr VARCHAR(100) ,
                                      '-2', -- statusId VARCHAR(100) ,
                                      '116' -- companyId VARCHAR(100) 
                                      );
                                      
                  sp_marketplace_shipmentpair_search_carrier`(  
                                      'all', -- origin VARCHAR(100) ,
                                      'all', -- destination VARCHAR(100) ,
                                      'all', -- pickupDateTxt1 VARCHAR(100) ,
									  'all', -- deliveryDateTxt1 VARCHAR(100) ,
									  'all', -- origin2 VARCHAR(100) ,
                                      'all', -- destination2 VARCHAR(100),
                                      'all', -- pickupDateTxt2 VARCHAR(100) ,
                                      'all', -- deliveryDateTxt2 VARCHAR(100) ,
                                       'all', -- referenceNbr VARCHAR(100) ,
                                      '-2', -- statusId VARCHAR(100) ,
                                      '206 --companyId VARCHAR(100) 
                                      )
                                      
			 */
		    
		    String sql = 	 " call " + sp + " (?,?,?,?,?,?,?,?,?,?,?) ";
  			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
  					PreparedStatement pstmt = connection.prepareStatement(sql);) {

  				
  				String originText =  allIfNull(ss.getOriginTxt());
  				String destinationTxt =  allIfNull(ss.getDestinationTxt());
  				String originText2 =  allIfNull(ss.getOriginTxt2());
  				String destinationTxt2 =  allIfNull(ss.getDestinationTxt2());
  				String pickupDateTxt =  allIfNull(ss.getPickupDateTxt());
  				String pickupDateTxt2 =  allIfNull(ss.getPickupDateTxt2());
  				String deliveryDateTxt =  allIfNull(ss.getDeliveryDateTxt());
  				String deliveryDateTxt2 =  allIfNull(ss.getDeliveryDateTxt2());
  				String referenceNbr =  allIfNull(ss.getReferenceNbr());
  				int statusId =  dfltIfNull(ss.getStatusId(),-2);
  				int companyId =  dfltIfNull(ss.getCompanyId(),-1);
  				
  				int ctr = 1;
  				
				pstmt.setString(ctr++, originText);
				pstmt.setString(ctr++, destinationTxt);
				pstmt.setString(ctr++, pickupDateTxt);
				pstmt.setString(ctr++, deliveryDateTxt);
				pstmt.setString(ctr++, originText2);
				pstmt.setString(ctr++, destinationTxt2);
				pstmt.setString(ctr++, pickupDateTxt2);
				pstmt.setString(ctr++, deliveryDateTxt2);
				pstmt.setString(ctr++, referenceNbr);
				pstmt.setInt(ctr++, statusId);
				pstmt.setInt(ctr++, companyId);
				
  				
  				try (ResultSet rs = pstmt.executeQuery()) {
  					ResultSetMapper<ShipmentInfo> driverRawMapper = new ResultSetMapper<ShipmentInfo>();
  		        	List<ShipmentInfo> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentInfo.class);
  		        	if (pojoList!=null && !pojoList.isEmpty()) {
  		        		System.out.println("ShipmentInfoListSize:"+pojoList.size());
  		        		return pojoList;
  					}
  				}
  			} catch (Exception e) {
  				log.error("Error message: " + e, e);
  				new ServiceException(Response.Status.CONFLICT);
  			}
  			return new ArrayList();
	}
	


	private int dfltIfNull(Integer i, Integer dflt) {
		if(i==null){
			i = dflt;
		}
		return i;
	}


	private String allIfNull(String s) {
		if ((s==null) || (s.trim().length()==0)){
			s="all";
		}
		return s;
	}
	
	private int zeroIfNull(String s) {
		int i=0;
		try {
			i = Integer.parseInt(s);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return i;
	}


	private List<ShipmentCarrierBidDetail> doGetShipmentCarrierBidDetailsNoDefault(Integer shipmentCarrierBidId) {
  		
  		List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = new ArrayList<ShipmentCarrierBidDetail>();
		String sql = 	" SELECT shipment_carrier_bid_id, carrier_id, shipment_id, accessorial_description, "
						+" accessorial_cd, accessorial_type, accessorial_data_source, accessorial_amt, "
						+" accessorial_ccy, accessorial_rate_comment, create_dt, update_dt, "
						+" shipper_defined_flg"
						+" FROM shipment_carrier_bid_detail where shipment_carrier_bid_id=?";
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentCarrierBidId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentCarrierBidDetail> driverRawMapper = new ResultSetMapper<ShipmentCarrierBidDetail>();
	        	List<ShipmentCarrierBidDetail> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentCarrierBidDetail.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
	
			
		return null;
	}
		
	private List<ShipmentCarrierBidDetail> doGetShipmentCarrierBidDetailsWithDefault(Integer shipmentCarrierBidId, 
			Integer carrierId,
			List<ShipmentAccessorial> shipmentAccessorialsList) {
  		
  		List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = new ArrayList<ShipmentCarrierBidDetail>();
		String sql = 	" SELECT shipment_carrier_bid_id, carrier_id, shipment_id, accessorial_description, "
						+" accessorial_cd, accessorial_type, accessorial_data_source, accessorial_amt, "
						+" accessorial_ccy, accessorial_rate_comment, create_dt, update_dt, "
						+" shipper_defined_flg "
						+" FROM shipment_carrier_bid_detail where shipment_carrier_bid_id=?";
		
		System.out.println("sql from doGetShipmentCarrierBidDetailsWithDefault:"+sql+" shipmentCarrierBidId:"+shipmentCarrierBidId);
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentCarrierBidId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentCarrierBidDetail> driverRawMapper = new ResultSetMapper<ShipmentCarrierBidDetail>();
	        	List<ShipmentCarrierBidDetail> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentCarrierBidDetail.class);
	        	if ((pojoList!=null) && (!pojoList.isEmpty()) && (pojoList.size()>0)) {
	        		System.out.println("pojoList is null");
	        		return pojoList;
				}else if ((shipmentAccessorialsList!=null) && (shipmentAccessorialsList.size()>0)){
					System.out.println("shipmentAccessorialsList is not null "+shipmentAccessorialsList.size());
					shipmentCarrierBidDetails = new ArrayList();
					for(ShipmentAccessorial sa : shipmentAccessorialsList){
						ShipmentCarrierBidDetail scbd = new ShipmentCarrierBidDetail();
						scbd.setAccessorialCd(sa.getAccessorialCd());
						scbd.setAccessorialDataSource(sa.getAccessorialDataSource());
						scbd.setAccessorialDescription(sa.getAccessorialDescription());
						scbd.setAccessorialType(sa.getAccessorialType());
						scbd.setAccessorialAmt(0.0);
						scbd.setAccessorialRateComment("");
						scbd.setCarrierId(carrierId);
						scbd.setShipperDefined(true);
						
						shipmentCarrierBidDetails.add(scbd);
					}
					return shipmentCarrierBidDetails;
				}
					
				
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		
		System.out.println("return  null");	
		return null;
	}



	private List<Shipment> doGetShipmentForBid(Integer companyId) {
  		List<Shipment> shipmentListFinal = new ArrayList();
		List<Shipment> shipmentList = doGetShipmentsForBidList(companyId);
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				
				shpt.setBidRank(0);
				
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);

				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}
				
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					//shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}
				
				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
							}
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							bidCount++;
							
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							
							
							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
							
							if(companyId==scb.getCarrierId()){
								shpt.setBidRank(scb.getBidRank());
								shpt.setCarrierBid(scb.getQuoteAmt());
								shpt.setCarrierBidType(scb.getRateTypeCd());
								shipmentCarrierBidList.add(scb); // carrier should see only his bid
							}
							
							//shipmentCarrierBidList.add(scb);
						}
						
						// shipmentCarrierBidList.add(scb);
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}
					
					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentList;
	}
	
	private List<Shipment> doGetVendorShipments(Integer companyId) {
  		List<Shipment> shipmentListFinal = new ArrayList();
		List<Shipment> shipmentList = doGetVendorShipmentsList(companyId);
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				
				shpt.setBidRank(0);
				
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);

				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}
				
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					//shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}
				
				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
							}
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							bidCount++;
							
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							
							
							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
							
							if(companyId==scb.getCarrierId()){
								shpt.setBidRank(scb.getBidRank());
								shpt.setCarrierBid(scb.getQuoteAmt());
								shpt.setCarrierBidType(scb.getRateTypeCd());
								shipmentCarrierBidList.add(scb); // carrier should see only his bid
							}
							
							//shipmentCarrierBidList.add(scb);
						}
						
						// shipmentCarrierBidList.add(scb);
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}
					
					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentList;
	}
  	
	private List<Shipment> doGetShipmentGroupTypeByCompanyId(Integer companyId,  String spName, boolean isCompact) {
		// List<Shipment> shipmentList = doGetShipmentsForBidList(companyId);
  		//List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_carrier_bids_won");
		//sp_marketplace_shipments_all_data
		List<Shipment> shipmentListFinal = new ArrayList();
		try {
			String sql = 	 " call "+spName+"(?)";
			List<ShipmentInfo> shipmentList = doGetShipmentsAllData(companyId,sql);
			if ((shipmentList!=null) && (shipmentList.size()>0)){
				System.out.println("shipmentList:"+shipmentList.size());
				shipmentListFinal = doSetShipmentInfoListToShipmentObj(shipmentList ,isCompact);
			}
			  
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return shipmentListFinal;
  		
	}
	
	private List<Shipment> doGetShipmentGroupTypeByCompanyIdCarrierOrShipper(Integer companyId,  String spName, boolean isCompact, boolean isCarrier) {
		// List<Shipment> shipmentList = doGetShipmentsForBidList(companyId);
  		//List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_carrier_bids_won");
		//sp_marketplace_shipments_all_data
		List<Shipment> shipmentListFinal = new ArrayList();
		try {
			String sql = 	 " call "+spName+"(?)";
			List<ShipmentInfo> shipmentList = doGetShipmentsAllData(companyId,sql);
			if ((shipmentList!=null) && (shipmentList.size()>0)){
				System.out.println("shipmentList:"+shipmentList.size());
				shipmentListFinal = doSetShipmentInfoListToShipmentObjCarrierOrShipper(shipmentList ,isCompact,isCarrier,companyId);
			}
			  
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return shipmentListFinal;
  		
	}
  	
	private List<Shipment> doGetShipmentMyBidsV2_(Integer companyId,  String spName) {
		// List<Shipment> shipmentList = doGetShipmentsForBidList(companyId);
  		//List<Shipment> shipmentList = doGetShipmentMyBids(companyId,"sp_marketplace_carrier_bids_won");
		//sp_marketplace_shipments_all_data
		List<Shipment> shipmentListFinal = new ArrayList();
		try {
			String sql = 	 " call "+spName+"(?)";
			List<ShipmentInfo> shipmentList = doGetShipmentsAllData(companyId,sql);
			if ((shipmentList!=null) && (shipmentList.size()>0)){
				System.out.println("shipmentList:"+shipmentList.size());
				shipmentListFinal = doSetShipmentInfoListToShipmentObj(shipmentList ,false);
			}
			  
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return shipmentListFinal;
  		
	}
	
  	private List<Shipment> doGetShipmentMyBids(Integer companyId, List<Shipment> shipmentList, boolean isCarrier) {
  		List<Shipment> shipmentListFinal = new ArrayList();
		// List<Shipment> shipmentList = doGetShipmentsForBidList(companyId);
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				
				shpt.setBidRank(0);
				
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);

				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}
				
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					//shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}
				
				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
							}
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							bidCount++;
							
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							
							
							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							
							
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}

							if(isCarrier){
								if (scb.getCarrierId() == companyId){
									shpt.setCarrierBid(scb.getQuoteAmt());
									shpt.setCarrierBidType(scb.getRateTypeCd());
									shpt.setBidRank(scb.getBidRank());
									shipmentCarrierBidList.add(scb);
								}
							}else{
								shipmentCarrierBidList.add(scb);
							}
							
						}
						
						// shipmentCarrierBidList.add(scb);
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}
					
					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentList;
	}
  	
  	
 	
  	private List<Shipment> doGetShipmentListBySearchCriteriaV1(ShipmentSearch shipmentSearch) {
  		List<Shipment> shipmentListFinal = new ArrayList();
		List<Shipment> shipmentList = doGetShipmentsByLocationV1(shipmentSearch);
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);
				
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}

				Double sumCost = 0.0;
				Double highEndBidAmt = 0.0;
				Double avgBidAmt = 0.0;

				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				String costCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
								costCurrency = lowestCostCurrency;
							}
							
							if((highEndBidAmt==null) || (highEndBidAmt<scb.getQuoteAmt())){
								highEndBidAmt = scb.getQuoteAmt();
								costCurrency = scb.getQuoteCurrencyCd();
							}
							
							sumCost = sumCost + scb.getQuoteAmt();
							
							
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							bidCount++;
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
							
							shipmentCarrierBidList.add(scb);
							
						}
						
						
						
						//shipmentCarrierBidList.add(scb);
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}

					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				
				avgBidAmt =  sumCost / bidCount ;
				
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				// shpt.setAvgBidAmt(avgBidAmt);  already on sp on carrier bid upsert
				shpt.setHighEndBidAmt(highEndBidAmt);
				shpt.setShipmentCarrierBids(shipmentCarrierBids);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentListFinal;
	}
  	
  	
  	private List<Shipment> doGetShipmentListBySearchCriteriaDateRange(ShipmentSearch shipmentSearch) {
  		List<ShipmentInfo> shipmentList = doGetShipmentsByLocationAndDateRange(shipmentSearch);
		List<Shipment> shipmentListFinal =  doSetShipmentInfoListToShipmentObj(shipmentList,false);
		return shipmentListFinal;
  		
	}
  	
  	private List<Shipment> doGetShipmentForBidListBySearchCriteria(ShipmentSearch shipmentSearch) {
  		List<Shipment> shipmentListFinal = new ArrayList();
		List<Shipment> shipmentList = doGetShipmentsForBidByLocation(shipmentSearch);
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);

				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}
				
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}
				
				String costCurrency = "";
				Double sumCost = 0.0;
				Double highEndBidAmt = 0.0;
				Double avgBidAmt = 0.0;
				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
							}
							
							if((highEndBidAmt==null) || (highEndBidAmt<scb.getQuoteAmt())){
								highEndBidAmt = scb.getQuoteAmt();
								costCurrency = scb.getQuoteCurrencyCd();
							}

							sumCost = sumCost + scb.getQuoteAmt();
							
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							bidCount++;
							
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							
							
						
							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
							
							if (scb.getCarrierId() == shipmentSearch.getCompanyId()){
								shpt.setCarrierBid(scb.getQuoteAmt());
								shpt.setCarrierBidType(scb.getRateTypeCd());
								shpt.setBidRank(scb.getBidRank());
								shipmentCarrierBidList.add(scb); // carrier must not see other carrier bid
							}
							
							//shipmentCarrierBidList.add(scb);
							
						}
						//shipmentCarrierBidList.add(scb);
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}

					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				avgBidAmt =  sumCost / bidCount ;
				// shpt.setAvgBidAmt(avgBidAmt); already on sp on carrier bid upsert
				shpt.setHighEndBidAmt(highEndBidAmt);
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				shpt.setShipmentCarrierBids(shipmentCarrierBids);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentListFinal;
	}
  	
  	private List<Shipment> doGetShipmentPairForBidListBySearchCriteria(ShipmentSearch shipmentSearch) {
  		List<Shipment> shipmentListFinal = new ArrayList();
		List<Shipment> shipmentList = doGetShipmentsPairForBidByLocation(shipmentSearch);
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				shpt.setDeliveryAddress(deliveryAddress);

				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);

				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}
				
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}
				
				String costCurrency = "";
				Double sumCost = 0.0;
				Double highEndBidAmt = 0.0;
				Double avgBidAmt = 0.0;
				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
							}
							
							if((highEndBidAmt==null) || (highEndBidAmt<scb.getQuoteAmt())){
								highEndBidAmt = scb.getQuoteAmt();
								costCurrency = scb.getQuoteCurrencyCd();
							}

							sumCost = sumCost + scb.getQuoteAmt();
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							bidCount++;
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							

							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							
							
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
							
							if (scb.getCarrierId() == shipmentSearch.getCompanyId()){
								shpt.setBidRank(scb.getBidRank());
								shpt.setCarrierBid(scb.getQuoteAmt());
								shpt.setCarrierBidType(scb.getRateTypeCd());
								shipmentCarrierBidList.add(scb); // carrier must not see other carrier's bid
							}
							//shipmentCarrierBidList.add(scb);
							
						}
						
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}

					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				avgBidAmt =  sumCost / bidCount ;
				// shpt.setAvgBidAmt(avgBidAmt); already on sp on carrier bid upsert
				shpt.setHighEndBidAmt(highEndBidAmt);
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				shpt.setShipmentCarrierBids(shipmentCarrierBids);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentListFinal;
	}
  	
  	
  	private List<Shipment> doGetShipmentWithBackhaulForBidListBySearchCriteria(ShipmentSearch shipmentSearch, String userId, String spName) {
  		List<Shipment> shipmentListFinal = new ArrayList();
		List<Shipment> shipmentList = doGetShipmentsBackhaulForBidByLocation(shipmentSearch,userId, spName );
		if(shipmentList!=null && shipmentList.size()>0){
			for(Shipment shpt : shipmentList) {
				EntityAddress pickupAddress = doGetEntityAddress(shpt.getPickupAddressId());
				shpt.setPickupAddress(pickupAddress);
				
				EntityAddress deliveryAddress = doGetEntityAddress(shpt.getDeliveryAddressId());
				shpt.setDeliveryAddress(deliveryAddress);
				
				List<ShipmentDetail> shipmentDetailsList =  doGetShipmentDetails(shpt.getShipmentId());
				shpt.setDeliveryAddress(deliveryAddress);

				List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shpt.getShipmentId());
				shpt.setShipmentAccessorials(shipmentAccessorialsList);
				
				EquipmentType equipmentType = doGetEquipmentType(shpt.getRequestedEquipmentTypeId());
				shpt.setEquipmentType(equipmentType);

				if(shpt.getPaymentTermId()!=null){
					PaymentTerm paymentTerm = doGetPaymentTerm(shpt.getPaymentTermId());
					shpt.setPaymentTerm(paymentTerm);
				}
				
				if((shipmentDetailsList!=null) &&(shipmentDetailsList.size()>0)){
					shpt.setShipmentDetails(shipmentDetailsList);
					shpt.setShipmentDetail(shipmentDetailsList.get(0));
				}
				
				String costCurrency = "";
				Double sumCost = 0.0;
				Double highEndBidAmt = 0.0;
				Double avgBidAmt = 0.0;
				Double lowestCost = 0.0;
				int bidCount = 0;
				String rateType = "In Process"; 
				String lowestCostCurrency = "";
				List rateTypeList = new ArrayList();
				List<ShipmentCarrierBid>  shipmentCarrierBidList = new ArrayList();
				List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shpt.getShipmentId());
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					//shpt.setShipmentCarrierBids(shipmentCarrierBids);
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							if((lowestCost==null) || (lowestCost>scb.getQuoteAmt())){
								lowestCost = scb.getQuoteAmt();
								lowestCostCurrency = scb.getQuoteCurrencyCd();
							}
							
							if((highEndBidAmt==null) || (highEndBidAmt<scb.getQuoteAmt())){
								highEndBidAmt = scb.getQuoteAmt();
								costCurrency = scb.getQuoteCurrencyCd();
							}

							sumCost = sumCost + scb.getQuoteAmt();
							/*
							if (scb.getRateTypeCd().equalsIgnoreCase("B")){
								bidCount++;
							}
							*/
							bidCount++;
							if ((scb.getRateTypeDescTxt()!=null) && (!rateTypeList.contains(scb.getRateTypeDescTxt()))){
								rateTypeList.add(scb.getRateTypeDescTxt());
							}
							

							
							Company company = doGetCompanyObjectById(scb.getCarrierId());
							scb.setCarrierCd(company.getCompanyName());
							
							
							
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
									scb.getShipmentCarrierBidId(),
									scb.getCarrierId(),
									shipmentAccessorialsList
									);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
							
							if (scb.getCarrierId() == shipmentSearch.getCompanyId()){
								shpt.setBidRank(scb.getBidRank());
								shpt.setCarrierBid(scb.getQuoteAmt());
								shpt.setCarrierBidType(scb.getRateTypeCd());
								shipmentCarrierBidList.add(scb); // carrier must not see other carrier's bid
							}
							//shipmentCarrierBidList.add(scb);
							
						}
						
					}
					
					if(rateTypeList.size()>0){
						for (Object r: rateTypeList){
							if (rateType.trim().length()>0){
								rateType = rateType + "," + r ;
							}else{
								rateType = r.toString();
							}
						}
							
					}

					shpt.setShipmentCarrierBids(shipmentCarrierBidList);
				}
				
				avgBidAmt =  sumCost / bidCount ;
				// shpt.setAvgBidAmt(avgBidAmt); already on sp on carrier bid upsert
				shpt.setHighEndBidAmt(highEndBidAmt);
				shpt.setLowestCostCurrency(lowestCostCurrency);
				shpt.setRateType(rateType);
				shpt.setLowestCost(lowestCost);
				shpt.setRateCount(bidCount);
				shpt.setShipmentCarrierBids(shipmentCarrierBids);
				shipmentListFinal.add(shpt);
			}
		}
		return shipmentListFinal;
	}
 
	private List<Shipment> doGetShipments(int companyId){
		
		//companyId = 83;
		
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	" SELECT s.shipment_id, s.shipment_nbr, s.Shipper_Id, s.Status_Id, s.assigned_provider_Id, "
						+" s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
						+" s.requested_pickup_dt, s.Requested_Delivery_Dt, "
						+" s.Unit_Count, s.unit_of_measure, s.Create_Dt, s.create_user_id, s.Update_dt, s.update_user_id, s.tender_expiration_dt "
						+" ,s.payment_term_id "
						+" , s.high_end_bid_amt, s.low_end_bid_amt, s.target_rate_amt, s.avg_bid_amt "
						+" ,s.currency_cd"
						//+" , coalesce( b.bid_rank,0) as bid_rank"
						+" , 0 as bid_rank"
						+" , commodity_desc_txt"
						+" , pro_bill_nbr"
						+" , Shipping_Instruction_Txt"
						+" , s.shipment_type_cd "
						+" , s.stops_cnt "
						+" FROM shipment s "
						//+" LEFT JOIN shipment_carrier_bid b on s.shipment_id = b.shipment_id "
						+" where s.status_id <= 30 "+
						//" and  DATE_ADD(s.Requested_Delivery_Dt, INTERVAL 45 DAY) > now()  "+
						"  and  s.Requested_Delivery_Dt between now() and DATE_ADD(now(), INTERVAL 45 DAY) "+
						" and s.shipper_id=? ";
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<Shipment> doGetShipmentMyBids(int companyId, String spMyBids){
		
		
		
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	 " call "+spMyBids+"(?)";
		System.out.println(sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return new ArrayList();
	}
	
	private List<Shipment> doGetShipmentsForBidList(int companyId){
		
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	 " SELECT distinct s.shipment_id, s.shipment_nbr, s.Shipper_Id, s.Status_Id, s.assigned_provider_Id, "
						+" s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
						+" s.requested_pickup_dt, s.Requested_Delivery_Dt, "
						+" s.Unit_Count, s.unit_of_measure, s.Create_Dt, s.create_user_id, s.Update_dt, s.update_user_id, "+
						"  s.tender_expiration_dt "
						+" , s.payment_term_id "
						+" , s.high_end_bid_amt, s.low_end_bid_amt, s.target_rate_amt, s.avg_bid_amt "
						+" , s.currency_cd"
						//+" , coalesce( b.bid_rank,0) as bid_rank"
						+" , 0 as bid_rank"
						+" , commodity_desc_txt"
						+" , pro_bill_nbr"
						+" , Shipping_Instruction_Txt"
						+" , s.shipment_type_cd "
						+" , s.stops_cnt "
						+" FROM shipment s "
						+" JOIN entity_address d on d.entity_address_id = s.delivery_address_id " 
						+" LEFT JOIN shipment_carrier_bid b on b.shipment_id = s.shipment_id "
						+" where  "
						+" tender_expiration_dt is not null "
						+" and tender_expiration_dt > now()"
						// added to handle BOOKING APP
						//+" (s.tender_expiration_dt is not null  or s.tender_type_cd='BKG') "
						//+" and (s.tender_expiration_dt > now() or s.tender_type_cd='BKG') "
						//+" and (( s.tender_type_cd='BKG' and s.Requested_Delivery_Dt > now()) or coalesce(s.tender_type_cd,'')='') "
						+" and "
						+"  ("
						+" 		(	d.country in " 
						+" 				(	select distinct ctry.country_2_cd "
						+" 					from company c join carrier_region cr  on c.company_id=cr.company_id "
						+" 					join service_area sa on cr.service_area_id=sa.service_area_id "
						+" 					join country ctry on sa.Country_ID = ctry.Country_ID "
						+"   				where c.company_id = ? "
						+"  			) "
						/*
						+" 				(	select ctr.country_2_cd "
						+"   				from company c "
						+"   				join company_address a on c.corporate_address_id = a.company_address_id "
						+"   				join country ctr on ctr.country_id=a.country_id "
						+"   				where c.company_id = ? "
						+"  			) "
						*/
						+"  	) "
						+"  	or "
						+"  	( b.carrier_id = ? ) "
						+"  )";
		
						// +" and ( tender_expiration_dt > now() or (s.status_id < 30 and carrier_id = ? )  ) ";
						//+" and (tender_expiration_dt > now() or carrier_id = ?  ) ";
						//s.status_id=15
						//+" where shipper_id=?";
		System.out.println(sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, companyId);
			pstmt.setInt(2, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<Shipment> doGetVendorShipmentsList(Integer companyId){
		
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	 " SELECT distinct s.shipment_id, s.shipment_nbr, s.Shipper_Id, s.Status_Id, s.assigned_provider_Id, "
						+" s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
						+" s.requested_pickup_dt, s.Requested_Delivery_Dt, "
						+" s.Unit_Count, s.unit_of_measure, s.Create_Dt, s.create_user_id, s.Update_dt, s.update_user_id, "+
						"  s.tender_expiration_dt "
						+" , s.payment_term_id "
						+" , s.high_end_bid_amt, s.low_end_bid_amt, s.target_rate_amt, s.avg_bid_amt "
						+" , s.currency_cd"
						//+" , coalesce( b.bid_rank,0) as bid_rank"
						+" , 0 as bid_rank"
						+" , commodity_desc_txt"
						+" , pro_bill_nbr"
						+" , Shipping_Instruction_Txt"
						+" , s.shipment_type_cd "
						+" , s.stops_cnt "
						+" FROM shipment s "
						+" JOIN company c ON c.Company_code = s.parent_partner_cd "
						+" JOIN entity_address d on d.entity_address_id = s.delivery_address_id " 
						+" LEFT JOIN shipment_carrier_bid b on b.shipment_id = s.shipment_id "
						+" where  "
						+" s.tender_type_cd='BKG'"
						+" and s.assigned_provider_Id = ? ";
						//+" and (c.Company_ID = ? or s.assigned_provider_Id = ?) ";
					log.info(sql);
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, companyId);
			// pstmt.setInt(2, companyId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	private List<ShipmentInfo> doGetShipmentsByLocationAndDateRange(ShipmentSearch shipmentSearch){
		//shipmentSearch.setCompanyId(83);
		List<Shipment> shipments = new ArrayList<Shipment>();
		
		String sqlFilter =	" WHERE s.shipper_id  = ? "
						+" and ( "
						+"			p.city_Locality like ? "
						+"			or p.line1 like ? "
						+"			or p.street_name like ? "
						+"			or p.province like ? "
						+"			or p.country like ? "
						+"			or p.entity_name like ? "
						+"		)"
						+" and ( "
						+"			d.city_Locality like ? "
						+"			or d.line1 like ? "
						+"			or d.street_name like ? "
						+"			or d.province like ? "
						+"			or d.country like ? "
						+"			or d.entity_name like ? "
						+"		)"
						+" and s.shipment_nbr like ?";
			
		String originTxt = "%";
		if ((shipmentSearch.getOriginTxt()!=null) && (!shipmentSearch.getOriginTxt().equalsIgnoreCase("all"))){
			originTxt = "%" + shipmentSearch.getOriginTxt() + "%";
		};

		String destinationTxt = "%";
		if ((shipmentSearch.getDestinationTxt()!=null)  && (!shipmentSearch.getDestinationTxt().equalsIgnoreCase("all"))){
			destinationTxt = "%" + shipmentSearch.getDestinationTxt() + "%";
		}
		
		String shipmentRefTxt = "%";
		if ((shipmentSearch.getReferenceNbr()!=null) && (!shipmentSearch.getReferenceNbr().equalsIgnoreCase("all"))){
			shipmentRefTxt = "%" + shipmentSearch.getReferenceNbr() + "%";
		};
		
		String pickupDateVal = "1";
		String pickupDateVal2 = "1";
		String pickupDateSqlWerCondition = " and 1 = ? and 1 = ? ";
		boolean isWithPickupDt1 = false;
		if ((shipmentSearch.getPickupDateTxt()!=null)  && (!shipmentSearch.getPickupDateTxt().equalsIgnoreCase("all"))){
			isWithPickupDt1 = true;
		}
		boolean isWithPickupDt2 = false;
		if ((shipmentSearch.getPickupDateTxt2()!=null)  && (!shipmentSearch.getPickupDateTxt2().equalsIgnoreCase("all"))){
			isWithPickupDt2 = true;
		}
		
		if ((isWithPickupDt1) && (isWithPickupDt2)){
			pickupDateSqlWerCondition = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) >= ?) ";
			pickupDateSqlWerCondition = pickupDateSqlWerCondition + " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) <= ?) ";
			pickupDateVal = shipmentSearch.getPickupDateTxt();
			pickupDateVal2 = shipmentSearch.getPickupDateTxt2();
			
		}else if(isWithPickupDt1){
			pickupDateSqlWerCondition = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) >= ?) ";
			pickupDateSqlWerCondition = pickupDateSqlWerCondition + " and 1 = ?  ";
			pickupDateVal = shipmentSearch.getPickupDateTxt();
		}else if(isWithPickupDt2){
			pickupDateSqlWerCondition = " and 1 = ? ";
			pickupDateSqlWerCondition = pickupDateSqlWerCondition + " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) <= ?) ";
			pickupDateVal2 = shipmentSearch.getPickupDateTxt2(); 
			
		}
		
		sqlFilter = sqlFilter + pickupDateSqlWerCondition ;
		
		
		
		String deliveryDateVal = "1";
		String deliveryDateVal2 = "1";
		String deliveryDateSqlWerCondition = " and 1 = ? and 1 = ? ";
		boolean isWithDeliveryDt1 = false;
		if ((shipmentSearch.getDeliveryDateTxt()!=null)  && (!shipmentSearch.getDeliveryDateTxt().equalsIgnoreCase("all"))){
			isWithDeliveryDt1 = true;
		}
		boolean isWithDeliveryDt2 = false;
		if ((shipmentSearch.getDeliveryDateTxt2()!=null)  && (!shipmentSearch.getDeliveryDateTxt2().equalsIgnoreCase("all"))){
			isWithDeliveryDt2 = true;
		}
		
		if ((isWithDeliveryDt1) && (isWithDeliveryDt2)){
			deliveryDateSqlWerCondition = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) >= ?) ";
			deliveryDateSqlWerCondition = deliveryDateSqlWerCondition + " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) <= ?) ";
			deliveryDateVal = shipmentSearch.getDeliveryDateTxt();
			deliveryDateVal2 = shipmentSearch.getDeliveryDateTxt2();
			
		}else if(isWithDeliveryDt1){
			deliveryDateSqlWerCondition = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) >= ?) ";
			deliveryDateSqlWerCondition = deliveryDateSqlWerCondition + " and 1 = ?  ";
			deliveryDateVal = shipmentSearch.getDeliveryDateTxt();
		}else if(isWithDeliveryDt2){
			deliveryDateSqlWerCondition = " and 1 = ? ";
			deliveryDateSqlWerCondition = deliveryDateSqlWerCondition + " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) <= ?) ";
			deliveryDateVal2 = shipmentSearch.getDeliveryDateTxt2(); 
			
		}
		sqlFilter = sqlFilter + deliveryDateSqlWerCondition ;
		
		String statusId = "1";
		String statusSqlWerCondition = " and 1 = ? ";
		if (shipmentSearch.getStatusId()>-1){
			statusSqlWerCondition = " and s.status_id = ? ";
			statusId = ""+shipmentSearch.getStatusId();
		}
		sqlFilter = sqlFilter + statusSqlWerCondition ;
		String sql = this.sqlShipmentAllInfo + sqlFilter;
		System.out.println(statusSqlWerCondition);
		System.out.println(statusId);
		System.out.println(sql);
		
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			
			pstmt.setInt(1, shipmentSearch.getCompanyId());
			pstmt.setString(2, originTxt);
			pstmt.setString(3, originTxt);
			pstmt.setString(4, originTxt);
			pstmt.setString(5, originTxt);
			pstmt.setString(6, originTxt);
			pstmt.setString(7, originTxt);
			pstmt.setString(8, destinationTxt);
			pstmt.setString(9, destinationTxt);
			pstmt.setString(10, destinationTxt);
			pstmt.setString(11, destinationTxt);
			pstmt.setString(12, destinationTxt);
			pstmt.setString(13, destinationTxt);
			pstmt.setString(14, shipmentRefTxt);
			pstmt.setString(15, pickupDateVal);
			pstmt.setString(16, pickupDateVal2);
			pstmt.setString(17, deliveryDateVal);
			pstmt.setString(18, deliveryDateVal2);
			pstmt.setString(19, statusId);
			
			log.debug(shipmentSearch.getCompanyId()+","+originTxt+","+destinationTxt+","+shipmentRefTxt+","+pickupDateVal+","+pickupDateVal2+","
			+deliveryDateVal+","+deliveryDateVal2+","+statusId);
			
			
			
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentInfo> driverRawMapper = new ResultSetMapper<ShipmentInfo>();
	        	List<ShipmentInfo> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentInfo.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<ShipmentInfo> doGetShipmentsByLocationAllData(ShipmentSearch shipmentSearch){
		//shipmentSearch.setCompanyId(83);

	   String sqlFilter =" WHERE s.shipper_id  = ? "
						+" and ( "
						+"			p.city_Locality like ? "
						+"			or p.line1 like ? "
						+"			or p.street_name like ? "
						+"			or p.province like ? "
						+"			or p.country like ? "
						+"			or p.entity_name like ? "
						+"		)"
						+" and ( "
						+"			d.city_Locality like ? "
						+"			or d.line1 like ? "
						+"			or d.street_name like ? "
						+"			or d.province like ? "
						+"			or d.country like ? "
						+"			or d.entity_name like ? "
						+"		)"
						+" and s.shipment_nbr like ?";
			
		String originTxt = "%";
		if ((shipmentSearch.getOriginTxt()!=null) && (!shipmentSearch.getOriginTxt().equalsIgnoreCase("all"))){
			originTxt = "%" + shipmentSearch.getOriginTxt() + "%";
		};

		String destinationTxt = "%";
		if ((shipmentSearch.getDestinationTxt()!=null)  && (!shipmentSearch.getDestinationTxt().equalsIgnoreCase("all"))){
			destinationTxt = "%" + shipmentSearch.getDestinationTxt() + "%";
		}
		
		String shipmentRefTxt = "%";
		if ((shipmentSearch.getReferenceNbr()!=null) && (!shipmentSearch.getReferenceNbr().equalsIgnoreCase("all"))){
			shipmentRefTxt = "%" + shipmentSearch.getReferenceNbr() + "%";
		};
		
		String pickupDateVal = "1";
		String pickupDateSqlWerCondition = " and 1 = ? ";
		
		if ((shipmentSearch.getPickupDateTxt()!=null)  && (!shipmentSearch.getPickupDateTxt().equalsIgnoreCase("all"))){
			pickupDateSqlWerCondition = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) = ?) ";
			pickupDateVal = shipmentSearch.getPickupDateTxt();
		}
		sqlFilter = sqlFilter + pickupDateSqlWerCondition ;
		
		
		
		String deliveryDateVal = "1";
		String deliveryDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getDeliveryDateTxt()!=null)  && (!shipmentSearch.getDeliveryDateTxt().equalsIgnoreCase("all"))){
			deliveryDateSqlWerCondition = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) = ?) ";
			deliveryDateVal = shipmentSearch.getDeliveryDateTxt();
		}
		sqlFilter = sqlFilter + deliveryDateSqlWerCondition ;

		String statusId = "1";
		String statusSqlWerCondition = " and 1 = ? ";
		if (shipmentSearch.getStatusId()>-1){
			statusSqlWerCondition = " and s.status_id = ? ";
			statusId = ""+shipmentSearch.getStatusId();
		}
		sqlFilter = sqlFilter + statusSqlWerCondition ;
		
		String sql = this.sqlShipmentAllInfo + sqlFilter;
		
		System.out.println(statusSqlWerCondition);
		System.out.println(statusId);
		System.out.println(sql);
		
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			
			pstmt.setInt(1, shipmentSearch.getCompanyId());
			pstmt.setString(2, originTxt);
			pstmt.setString(3, originTxt);
			pstmt.setString(4, originTxt);
			pstmt.setString(5, originTxt);
			pstmt.setString(6, originTxt);
			pstmt.setString(7, originTxt);
			pstmt.setString(8, destinationTxt);
			pstmt.setString(9, destinationTxt);
			pstmt.setString(10, destinationTxt);
			pstmt.setString(11, destinationTxt);
			pstmt.setString(12, destinationTxt);
			pstmt.setString(13, destinationTxt);
			pstmt.setString(14, shipmentRefTxt);
			pstmt.setString(15, pickupDateVal);
			pstmt.setString(16, deliveryDateVal);
			pstmt.setString(17, statusId);
			
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentInfo> driverRawMapper = new ResultSetMapper<ShipmentInfo>();
	        	List<ShipmentInfo> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentInfo.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	
	private List<Shipment> doGetShipmentsByLocationV1(ShipmentSearch shipmentSearch){
		//shipmentSearch.setCompanyId(83);
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sqlSelect = 	" SELECT s.shipment_id, s.shipment_nbr, s.Shipper_Id, s.Status_Id, s.assigned_provider_Id, "
						+" s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
						+" s.requested_pickup_dt, s.Requested_Delivery_Dt, "
						+" s.Unit_Count, s.unit_of_measure, s.Create_Dt, s.create_user_id, s.Update_dt, s.update_user_id, s.tender_expiration_dt "
						+" , s.payment_term_id "
						+" , s.high_end_bid_amt, s.low_end_bid_amt, s.target_rate_amt, s.avg_bid_amt "
						+" , s.currency_cd"
						//+" , coalesce( b.bid_rank,0) as bid_rank"
						+" , 0 as bid_rank"
						+" , commodity_desc_txt"
						+" , pro_bill_nbr"
						+" , Shipping_Instruction_Txt"
						+" , s.shipment_type_cd "
						+" , s.stops_cnt "
						+" FROM shipment s "
						+" JOIN entity_address p on p.entity_address_id = s.pickup_address_id "
						+" JOIN entity_address d on d.entity_address_id = s.delivery_address_id " ;
						//+" LEFT JOIN shipment_carrier_bid b on s.shipment_id=b.shipment_id "
						
		sqlSelect = 	this.sqlShipmentAllInfo ;
						
	  String sqlFilter =" WHERE s.shipper_id  = ? "
						+" and ( "
						+"			p.city_Locality like ? "
						+"			or p.line1 like ? "
						+"			or p.street_name like ? "
						+"			or p.province like ? "
						+"			or p.country like ? "
						+"			or p.entity_name like ? "
						+"		)"
						+" and ( "
						+"			d.city_Locality like ? "
						+"			or d.line1 like ? "
						+"			or d.street_name like ? "
						+"			or d.province like ? "
						+"			or d.country like ? "
						+"			or d.entity_name like ? "
						+"		)"
						+" and s.shipment_nbr like ?";
			
		String originTxt = "%";
		if ((shipmentSearch.getOriginTxt()!=null) && (!shipmentSearch.getOriginTxt().equalsIgnoreCase("all"))){
			originTxt = "%" + shipmentSearch.getOriginTxt() + "%";
		};

		String destinationTxt = "%";
		if ((shipmentSearch.getDestinationTxt()!=null)  && (!shipmentSearch.getDestinationTxt().equalsIgnoreCase("all"))){
			destinationTxt = "%" + shipmentSearch.getDestinationTxt() + "%";
		}
		
		String shipmentRefTxt = "%";
		if ((shipmentSearch.getReferenceNbr()!=null) && (!shipmentSearch.getReferenceNbr().equalsIgnoreCase("all"))){
			shipmentRefTxt = "%" + shipmentSearch.getReferenceNbr() + "%";
		};
		
		String pickupDateVal = "1";
		String pickupDateSqlWerCondition = " and 1 = ? ";
		
		if ((shipmentSearch.getPickupDateTxt()!=null)  && (!shipmentSearch.getPickupDateTxt().equalsIgnoreCase("all"))){
			pickupDateSqlWerCondition = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) = ?) ";
			pickupDateVal = shipmentSearch.getPickupDateTxt();
		}
		sqlFilter = sqlFilter + pickupDateSqlWerCondition ;
		
		
		
		String deliveryDateVal = "1";
		String deliveryDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getDeliveryDateTxt()!=null)  && (!shipmentSearch.getDeliveryDateTxt().equalsIgnoreCase("all"))){
			deliveryDateSqlWerCondition = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) = ?) ";
			deliveryDateVal = shipmentSearch.getDeliveryDateTxt();
		}
		sqlFilter = sqlFilter + deliveryDateSqlWerCondition ;

		String statusId = "1";
		String statusSqlWerCondition = " and 1 = ? ";
		if (shipmentSearch.getStatusId()>-1){
			statusSqlWerCondition = " and s.status_id = ? ";
			statusId = ""+shipmentSearch.getStatusId();
		}
		sqlFilter = sqlFilter + statusSqlWerCondition ;
		
		
		String sql = sqlSelect + sqlFilter;
		
		System.out.println(statusSqlWerCondition);
		System.out.println(statusId);
		System.out.println(sql);
		
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			
			pstmt.setInt(1, shipmentSearch.getCompanyId());
			pstmt.setString(2, originTxt);
			pstmt.setString(3, originTxt);
			pstmt.setString(4, originTxt);
			pstmt.setString(5, originTxt);
			pstmt.setString(6, originTxt);
			pstmt.setString(7, originTxt);
			pstmt.setString(8, destinationTxt);
			pstmt.setString(9, destinationTxt);
			pstmt.setString(10, destinationTxt);
			pstmt.setString(11, destinationTxt);
			pstmt.setString(12, destinationTxt);
			pstmt.setString(13, destinationTxt);
			pstmt.setString(14, shipmentRefTxt);
			pstmt.setString(15, pickupDateVal);
			pstmt.setString(16, deliveryDateVal);
			pstmt.setString(17, statusId);
			
			
			
			
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	
	
	private List<Shipment> doGetShipmentsForBidByLocation(ShipmentSearch shipmentSearch){
		
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	" SELECT distinct s.shipment_id, s.shipment_nbr, s.Shipper_Id, s.Status_Id, s.assigned_provider_Id, "
						+" s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
						+" s.requested_pickup_dt, s.Requested_Delivery_Dt, "
						+" s.Unit_Count, s.unit_of_measure, s.Create_Dt, s.create_user_id, s.Update_dt, s.update_user_id, s.tender_expiration_dt "
						+" , s.payment_term_id "
						+" , s.high_end_bid_amt, s.low_end_bid_amt, s.target_rate_amt, s.avg_bid_amt "
						+" , s.currency_cd"
						//+" , coalesce( b.bid_rank,0) as bid_rank"
						+" , 0 as bid_rank"
						+" , commodity_desc_txt"
						+" , pro_bill_nbr"
						+" , Shipping_Instruction_Txt"
						+" , s.shipment_type_cd "
						+" , s.stops_cnt "
						+" FROM shipment s "
						+" JOIN entity_address p on p.entity_address_id = s.pickup_address_id "
						+" JOIN entity_address d on d.entity_address_id = s.delivery_address_id " 
						+" LEFT JOIN shipment_carrier_bid b on b.shipment_id = s.shipment_id "
						+" WHERE "
						+" s.tender_expiration_dt  is not null "
						+" and (s.tender_expiration_dt > now() or b.carrier_id = ?  ) "
						//+" and 1 = ? "
						+" and ( "
						+"			p.city_Locality like ? "
						+"			or p.line1 like ? "
						+"			or p.street_name like ? "
						+"			or p.province like ? "
						+"			or p.country like ? "
						+"			or p.entity_name like ? "
						+"		)"
						+" and ( "
						+"			d.city_Locality like ? "
						+"			or d.line1 like ? "
						+"			or d.street_name like ? "
						+"			or d.province like ? "
						+"			or d.country like ? "
						+"			or d.entity_name like ? "
						+"		)"
						+" and s.shipment_nbr like ?"
						
						+" and "
						+"  ("
						+" 		(	d.country in "
						+" 				(	select distinct ctry.country_2_cd "
						+" 					from company c join carrier_region cr  on c.company_id=cr.company_id "
						+" 					join service_area sa on cr.service_area_id=sa.service_area_id "
						+" 					join country ctry on sa.Country_ID = ctry.Country_ID "
						+"   				where c.company_id = ? "
						+"  			) "
						/*
						+" 				(	select ctr.country_2_cd "
						+"   				from company c "
						+"   				join company_address a on c.corporate_address_id = a.company_address_id "
						+"   				join country ctr on ctr.country_id=a.country_id "
						+"   				where c.company_id = ? "
						+"  			) "
						*/
						+"  	) "
						+"  	or "
						+"  	( b.carrier_id = ? ) "
						+"  )";
			
		String originTxt = "%";
		if ((shipmentSearch.getOriginTxt()!=null) && (!shipmentSearch.getOriginTxt().equalsIgnoreCase("all"))){
			originTxt = "%" + shipmentSearch.getOriginTxt() + "%";
		};

		String destinationTxt = "%";
		if ((shipmentSearch.getDestinationTxt()!=null)  && (!shipmentSearch.getDestinationTxt().equalsIgnoreCase("all"))){
			destinationTxt = "%" + shipmentSearch.getDestinationTxt() + "%";
		}
		
		String shipmentRefTxt = "%";
		if ((shipmentSearch.getReferenceNbr()!=null) && (!shipmentSearch.getReferenceNbr().equalsIgnoreCase("all"))){
			shipmentRefTxt = "%" + shipmentSearch.getReferenceNbr() + "%";
		};
		
		String pickupDateVal = "1";
		String pickupDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getPickupDateTxt()!=null)  && (!shipmentSearch.getPickupDateTxt().equalsIgnoreCase("all"))){
			pickupDateSqlWerCondition = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) = ?) ";
			pickupDateVal = shipmentSearch.getPickupDateTxt();
		}
		sql = sql + pickupDateSqlWerCondition ;
		
		String deliveryDateVal = "1";
		String deliveryDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getDeliveryDateTxt()!=null)  && (!shipmentSearch.getDeliveryDateTxt().equalsIgnoreCase("all"))){
			deliveryDateSqlWerCondition = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) = ?) ";
			deliveryDateVal = shipmentSearch.getDeliveryDateTxt();
		}
		sql = sql + deliveryDateSqlWerCondition ;

		String statusId = "1";
		String statusSqlWerCondition = " and 1 = ? ";
		if (shipmentSearch.getStatusId()>0){
			statusSqlWerCondition = " and s.status_id = ? ";
			statusId = ""+shipmentSearch.getStatusId();
		}
		sql = sql + statusSqlWerCondition ;
		
		System.out.println("statusId:"+statusId+" sql for bid -->"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			
			pstmt.setInt(1, shipmentSearch.getCompanyId());
			//pstmt.setInt(1, 1);
			pstmt.setString(2, originTxt);
			pstmt.setString(3, originTxt);
			pstmt.setString(4, originTxt);
			pstmt.setString(5, originTxt);
			pstmt.setString(6, originTxt);
			pstmt.setString(7, originTxt);
			pstmt.setString(8, destinationTxt);
			pstmt.setString(9, destinationTxt);
			pstmt.setString(10, destinationTxt);
			pstmt.setString(11, destinationTxt);
			pstmt.setString(12, destinationTxt);
			pstmt.setString(13, destinationTxt);
			pstmt.setString(14, shipmentRefTxt);
			pstmt.setInt(15, shipmentSearch.getCompanyId());
			pstmt.setInt(16, shipmentSearch.getCompanyId());
			pstmt.setString(17, pickupDateVal);
			pstmt.setString(18, deliveryDateVal);
			pstmt.setString(19, statusId);
			
			
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	

	private List<Shipment> doGetShipmentsBackhaulForBidByLocation(ShipmentSearch shipmentSearch,  String userId, String sp){
		List<Shipment> shipments = new ArrayList<Shipment>();
		//String sql = 	 " call marketplace_db.sp_marketplace_carrier_active_shipment_with_backhaul (?, ?, ?, ?, ?)";
		String sql = 	 " call " + sp + " (?, ?, ?, ?, ?)";
		System.out.println(sql+" userId:"+userId+" city:"+shipmentSearch.getOriginTxt()+" company:"+shipmentSearch.getCompanyId());
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setString(1, userId);
			pstmt.setString(2, shipmentSearch.getOriginTxt());
			pstmt.setString(3, shipmentSearch.getPickupDateTxt());
			pstmt.setString(4, shipmentSearch.getDeliveryDateTxt());
			pstmt.setInt(5, shipmentSearch.getCompanyId());
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return new ArrayList();
	}
	
	private List<Shipment> doGetShipmentsPairForBidByLocation(ShipmentSearch shipmentSearch){
		
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	" SELECT distinct s.shipment_id, s.shipment_nbr, s.Shipper_Id, s.Status_Id, s.assigned_provider_Id, "
						+" s.pickup_address_id, s.delivery_address_id, s.requested_equipment_type_id, "
						+" s.requested_pickup_dt, s.Requested_Delivery_Dt, "
						+" s.Unit_Count, s.unit_of_measure, s.Create_Dt, s.create_user_id, s.Update_dt, s.update_user_id, s.tender_expiration_dt "
						+" , s.payment_term_id "
						+" , s.high_end_bid_amt, s.low_end_bid_amt, s.target_rate_amt , s.avg_bid_amt"
						+" , s.currency_cd"
						//+" , coalesce( b.bid_rank,0) as bid_rank"
						+" , 0 as bid_rank"
						+" , commodity_desc_txt"
						+" , pro_bill_nbr"
						+" , Shipping_Instruction_Txt"
						+" , s.shipment_type_cd "
						+" , s.stops_cnt "
						+" FROM shipment s "
						+" JOIN entity_address p on p.entity_address_id = s.pickup_address_id "
						+" JOIN entity_address d on d.entity_address_id = s.delivery_address_id " 
						+" LEFT JOIN shipment_carrier_bid b on b.shipment_id = s.shipment_id "
						+" WHERE "
						+" s.tender_expiration_dt  is not null "
						+" and (s.tender_expiration_dt > now() or b.carrier_id = ?  ) "
						//+" and 1 = ? "
						+" AND "
						+" ( "
						+" 	( " // opening the group of left side of the OR argument
						+" 		( "
						+"			p.city_Locality like ? "
						+"			or p.line1 like ? "
						+"			or p.street_name like ? "
						+"			or p.province like ? "
						+"			or p.country like ? "
						+"			or p.entity_name like ? "
						+"			)"
						+" 		and ( "
						+"			d.city_Locality like ? "
						+"			or d.line1 like ? "
						+"			or d.street_name like ? "
						+"			or d.province like ? "
						+"			or d.country like ? "
						+"			or d.entity_name like ? "
						+"			)"
		
						// match country of carrier
						+" and "
						+"  ("
						+" 		(	d.country in " 
						+" 				(	select distinct ctry.country_2_cd "
						+" 					from company c join carrier_region cr  on c.company_id=cr.company_id "
						+" 					join service_area sa on cr.service_area_id=sa.service_area_id "
						+" 					join country ctry on sa.Country_ID = ctry.Country_ID "
						+"   				where c.company_id = ? "
						+"  			) "
						/*
						+" 				(	select ctr.country_2_cd "
						+"   				from company c "
						+"   				join company_address a on c.corporate_address_id = a.company_address_id "
						+"   				join country ctr on ctr.country_id=a.country_id "
						+"   				where c.company_id = ? "
						+"  			) "
						*/
						+"  	) "
						+"  	or "
						+"  	( b.carrier_id = ? ) "
						+"  )";
								
		
						//+"	)"
						//+"  or "
						//+"	 ( "
						//+"	 ) "
						//+" )";
						//+" and s.shipment_nbr like ?";
		
		String originTxt = "%";
		if ((shipmentSearch.getOriginTxt()!=null) && (!shipmentSearch.getOriginTxt().equalsIgnoreCase("all"))){
			originTxt = "%" + shipmentSearch.getOriginTxt() + "%";
		};

		String destinationTxt = "%";
		if ((shipmentSearch.getDestinationTxt()!=null)  && (!shipmentSearch.getDestinationTxt().equalsIgnoreCase("all"))){
			destinationTxt = "%" + shipmentSearch.getDestinationTxt() + "%";
		}
		
		String pickupDateVal = "1";
		String pickupDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getPickupDateTxt()!=null)  && (!shipmentSearch.getPickupDateTxt().equalsIgnoreCase("all"))){
			pickupDateSqlWerCondition = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) = ?) ";
			pickupDateVal = shipmentSearch.getPickupDateTxt();
		}
		sql = sql + pickupDateSqlWerCondition ;
		
		String deliveryDateVal = "1";
		String deliveryDateSqlWerCondition = " and 1 = ? ";
		if ((shipmentSearch.getDeliveryDateTxt()!=null)  && (!shipmentSearch.getDeliveryDateTxt().equalsIgnoreCase("all"))){
			deliveryDateSqlWerCondition = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) = ?) ";
			deliveryDateVal = shipmentSearch.getDeliveryDateTxt();
		}
		sql = sql + deliveryDateSqlWerCondition ;
		
		
		
		String originTxt2 = "%";
		if ((shipmentSearch.getOriginTxt2()!=null) && (!shipmentSearch.getOriginTxt2().equalsIgnoreCase("all"))){
			originTxt2 = "%" + shipmentSearch.getOriginTxt2() + "%";
		};

		String destinationTxt2 = "%";
		if ((shipmentSearch.getDestinationTxt2()!=null)  && (!shipmentSearch.getDestinationTxt2().equalsIgnoreCase("all"))){
			destinationTxt2 = "%" + shipmentSearch.getDestinationTxt2() + "%";
		}
		
		/*
		String statusId = "1";
		String statusSqlWerCondition = " and 1 = ? ";
		if (shipmentSearch.getStatusId()>0){
			statusSqlWerCondition = " and s.status_id = ? ";
			statusId = ""+shipmentSearch.getStatusId();
		}
		sql = sql + statusSqlWerCondition ;
		*/
		
		sql = 	sql  
				+"	)" // closing the group of left side of the OR argument
			  	+"  or "
			  	+" 	( " // opening the group of right side of the OR argument
				+" 		( "
				+"			p.city_Locality like ? "
				+"			or p.line1 like ? "
				+"			or p.street_name like ? "
				+"			or p.province like ? "
				+"			or p.country like ? "
				+"			or p.entity_name like ? "
				+"			)"
				+" 		and ( "
				+"			d.city_Locality like ? "
				+"			or d.line1 like ? "
				+"			or d.street_name like ? "
				+"			or d.province like ? "
				+"			or d.country like ? "
				+"			or d.entity_name like ? "
				+"			)"

				// match country of carrier
				+" and "
				+"  ("
				+" 		(	d.country in " 
				+" 				(	select distinct ctry.country_2_cd "
				+" 					from company c join carrier_region cr  on c.company_id=cr.company_id "
				+" 					join service_area sa on cr.service_area_id=sa.service_area_id "
				+" 					join country ctry on sa.Country_ID = ctry.Country_ID "
				+"   				where c.company_id = ? "
				+"  			) "
				/*
				+" 				(	select ctr.country_2_cd "
				+"   				from company c "
				+"   				join company_address a on c.corporate_address_id = a.company_address_id "
				+"   				join country ctr on ctr.country_id=a.country_id "
				+"   				where c.company_id = ? "
				+"  			) "
				*/
				+"  	) "
				+"  	or "
				+"  	( b.carrier_id = ? ) "
				+"  )";
						
		String pickupDateVal2 = "1";
		String pickupDateSqlWerCondition2 = " and 1 = ? ";
		if ((shipmentSearch.getPickupDateTxt2()!=null)  && (!shipmentSearch.getPickupDateTxt2().equalsIgnoreCase("all"))){
			pickupDateSqlWerCondition2 = " and ((DATE_FORMAT(s.requested_pickup_dt,'%Y-%m-%d')) = ?) ";
			pickupDateVal2 = shipmentSearch.getPickupDateTxt();
		}
		sql = sql + pickupDateSqlWerCondition2 ;
		
		String deliveryDateVal2 = "1";
		String deliveryDateSqlWerCondition2 = " and 1 = ? ";
		if ((shipmentSearch.getDeliveryDateTxt2()!=null)  && (!shipmentSearch.getDeliveryDateTxt2().equalsIgnoreCase("all"))){
			deliveryDateSqlWerCondition2 = " and ((DATE_FORMAT(s.Requested_Delivery_Dt,'%Y-%m-%d')) = ?) ";
			deliveryDateVal2 = shipmentSearch.getDeliveryDateTxt();
		}
		sql = sql + deliveryDateSqlWerCondition2 ;
		
		sql = 	sql  
				+"	)" // closing the group of right side of the OR argument
				+")"; // closing the AND where the OR argument belongs
		
		
			
		
		
		
		String shipmentRefTxt = "%";
		if ((shipmentSearch.getReferenceNbr()!=null) && (!shipmentSearch.getReferenceNbr().equalsIgnoreCase("all"))){
			shipmentRefTxt = "%" + shipmentSearch.getReferenceNbr() + "%";
		};
		
		
		System.out.println("PairShipmentSearch:"+sql);

		
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			
			pstmt.setInt(1, shipmentSearch.getCompanyId());
			//pstmt.setInt(1, 1);
			pstmt.setString(2, originTxt);
			pstmt.setString(3, originTxt);
			pstmt.setString(4, originTxt);
			pstmt.setString(5, originTxt);
			pstmt.setString(6, originTxt);
			pstmt.setString(7, originTxt);
			pstmt.setString(8, destinationTxt);
			pstmt.setString(9, destinationTxt);
			pstmt.setString(10, destinationTxt);
			pstmt.setString(11, destinationTxt);
			pstmt.setString(12, destinationTxt);
			pstmt.setString(13, destinationTxt);
			

			pstmt.setInt(14, shipmentSearch.getCompanyId());
			pstmt.setInt(15, shipmentSearch.getCompanyId());
			
			pstmt.setString(16, pickupDateVal);
			pstmt.setString(17, deliveryDateVal);
			
			
			pstmt.setString(18, originTxt2);
			pstmt.setString(19, originTxt2);
			pstmt.setString(20, originTxt2);
			pstmt.setString(21, originTxt2);
			pstmt.setString(22, originTxt2);
			pstmt.setString(23, originTxt2);
			pstmt.setString(24, destinationTxt2);
			pstmt.setString(25, destinationTxt2);
			pstmt.setString(26, destinationTxt2);
			pstmt.setString(27, destinationTxt2);
			pstmt.setString(28, destinationTxt2);
			pstmt.setString(29, destinationTxt2);
			
			pstmt.setInt(30, shipmentSearch.getCompanyId());
			pstmt.setInt(31, shipmentSearch.getCompanyId());
			
			
			pstmt.setString(32, pickupDateVal2);
			pstmt.setString(33, deliveryDateVal2);
			//pstmt.setString(30, statusId);
			
			
			
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Shipment> driverRawMapper = new ResultSetMapper<Shipment>();
	        	List<Shipment> pojoList = driverRawMapper.mapResultSetToObject(rs, Shipment.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private EntityAddress doGetEntityAddress(int entityAddressId){
		
		EntityAddress entityAddress = null;
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	" SELECT entity_address_id, entity_name, floor_apartment, "
						+" building, house_number, line1, street_name, city_locality, province, country, postal_code, "
						+" district_name, latitude, longitude, lat_long_status_cd, Is_DC "
						+" FROM entity_address where entity_address_id=?";
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, entityAddressId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EntityAddress> driverRawMapper = new ResultSetMapper<EntityAddress>();
	        	List<EntityAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, EntityAddress.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		entityAddress = pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return entityAddress;
	}
	
	private EquipmentType doGetEquipmentType(int equipmentTypeId){
		
		EquipmentType equipmentType = null;
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	" SELECT Equipment_Type_ID, Equipment_Type_Name, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "
						+" FROM equipment_type where equipment_type_id=?";
		
				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, equipmentTypeId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<EquipmentType> driverRawMapper = new ResultSetMapper<EquipmentType>();
	        	List<EquipmentType> pojoList = driverRawMapper.mapResultSetToObject(rs, EquipmentType.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		equipmentType = pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return equipmentType;
	}
	
	
	private PaymentTerm doGetPaymentTerm(int paymentTermId){
		
		PaymentTerm paymentTerm = null;
		List<Shipment> shipments = new ArrayList<Shipment>();
		String sql = 	" SELECT Payment_Term_ID, Payment_Term_Cd, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt, Payment_Term_Name "
						+" FROM marketplace_db.payment_term where payment_term_id=?";
		
				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, paymentTermId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<PaymentTerm> driverRawMapper = new ResultSetMapper<PaymentTerm>();
	        	List<PaymentTerm> pojoList = driverRawMapper.mapResultSetToObject(rs, PaymentTerm.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		paymentTerm = pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return paymentTerm;
	}
	
	
	private List<ShipmentCarrierBid> doGetShipmentCarrierBids(int shipmentId){
		
		List<ShipmentCarrierBid> shipments = new ArrayList<ShipmentCarrierBid>();
		String sql = 	" SELECT coalesce(b.shipment_carrier_bid_id,-1) as shipment_carrier_bid_id, carrier_id, b.shipment_id, quote_status_cd, "
						+" rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt,  "
						+" quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, comment_txt, b.create_Dt, "
						+" b.create_user_id, b.update_dt, b.update_user_id  "
						+" ,coalesce(company_name,'') as carrier_name "
						+" ,coalesce(company_code,'') as carrier_cd "
						+" ,coalesce( c.rating_value,0) as carrier_rating "
						+" ,IF( coalesce(st.tender_expiration_dt,'2000-01-01') < now(), 'Y','N') as bid_ended "
						+" , st.status_id "
						+" , st.payment_term_id "
						+" , st.high_end_bid_amt, st.low_end_bid_amt, st.target_rate_amt, st.avg_bid_amt "
						+" , st.currency_cd"
						+" , coalesce( b.bid_rank,0) as bid_rank"
						+" FROM shipment st "
						+" LEFT JOIN shipment_carrier_bid b on st.shipment_id=b.shipment_id "
						+" LEFT JOIN company c on c.company_id=b.carrier_id "
						//+" LEFT JOIN company_service s on s.company_id = b.carrier_id "
						+" where st.shipment_id=?"
						+" order by quote_amt, transit_tm ";
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentCarrierBid> driverRawMapper = new ResultSetMapper<ShipmentCarrierBid>();
	        	List<ShipmentCarrierBid> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentCarrierBid.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private ShipmentCarrierBid doGetShipmentCarrierBidByCarrierId(int shipmentId, int carrierId){
		
		List<ShipmentCarrierBid> shipments = new ArrayList<ShipmentCarrierBid>();
		String sql = 	" SELECT coalesce(b.shipment_carrier_bid_id,-1) as shipment_carrier_bid_id, carrier_id, b.shipment_id, quote_status_cd, "
						+" rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt,  "
						+" quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, "
						+" coalesce(comment_txt,'') as comment_txt, b.create_Dt, "
						+" b.create_user_id, b.update_dt, b.update_user_id  "
						+" ,coalesce(company_name,'') as carrier_name "
						+" ,coalesce(company_code,'') as carrier_cd "
						+" ,coalesce( c.rating_value,0) as carrier_rating "
						+" ,IF( coalesce(st.tender_expiration_dt,'2000-01-01') < now(), 'Y','N') as bid_ended "
						+" , st.status_id "
						+" , st.payment_term_id "
						+" , st.high_end_bid_amt, st.low_end_bid_amt, st.target_rate_amt, st.avg_bid_amt "
						+" , st.currency_cd"
						+" , coalesce( b.bid_rank,0) as bid_rank"
						+" FROM shipment st "
						+" LEFT JOIN shipment_carrier_bid b on st.shipment_id=b.shipment_id "
						+" LEFT JOIN company c on c.company_id=b.carrier_id "
						//+" LEFT JOIN company_service s on s.company_id = b.carrier_id "
						+" where st.shipment_id=?"
						+" and b.carrier_id = ? "
						+" order by quote_amt, transit_tm ";
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentId);
			pstmt.setInt(2, carrierId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentCarrierBid> driverRawMapper = new ResultSetMapper<ShipmentCarrierBid>();
	        	List<ShipmentCarrierBid> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentCarrierBid.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
private ShipmentCarrierBid doGetShipmentCarrierBidByNoCarrierId(int shipmentId){
		
		List<ShipmentCarrierBid> shipments = new ArrayList<ShipmentCarrierBid>();
		String sql = 	" SELECT coalesce(b.shipment_carrier_bid_id,-1) as shipment_carrier_bid_id, carrier_id, b.shipment_id, quote_status_cd, "
						+" rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt,  "
						+" quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, "
						+" coalesce(comment_txt,'') as comment_txt, b.create_Dt, "
						+" b.create_user_id, b.update_dt, b.update_user_id  "
						+" ,coalesce(company_name,'') as carrier_name "
						+" ,coalesce(company_code,'') as carrier_cd "
						+" ,coalesce( c.rating_value,0) as carrier_rating "
						+" ,IF( coalesce(st.tender_expiration_dt,'2000-01-01') < now(), 'Y','N') as bid_ended "
						+" , st.status_id "
						+" , st.payment_term_id "
						+" , st.high_end_bid_amt, st.low_end_bid_amt, st.target_rate_amt, st.avg_bid_amt "
						+" , st.currency_cd"
						+" , coalesce( b.bid_rank,0) as bid_rank"
						+" FROM shipment st "
						+" LEFT JOIN shipment_carrier_bid b on st.shipment_id=b.shipment_id "
						+" LEFT JOIN company c on c.company_id=b.carrier_id "
						//+" LEFT JOIN company_service s on s.company_id = b.carrier_id "
						+" where st.shipment_id=?"
						
						+" order by quote_amt, transit_tm ";
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentId);

			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentCarrierBid> driverRawMapper = new ResultSetMapper<ShipmentCarrierBid>();
	        	List<ShipmentCarrierBid> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentCarrierBid.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList.get(0);
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	private List<ShipmentCarrierBid> doGetShipmentInfoDetails(int shipmentId){
		
		List<ShipmentDetails> shipments = new ArrayList<ShipmentDetails>();
		String sql = 	" SELECT coalesce(b.shipment_carrier_bid_id,-1) as shipment_carrier_bid_id, carrier_id, b.shipment_id, quote_status_cd, "
						+" rate_type_cd, rate_type_desc_txt, transit_tm, equipment_type_id, quote_amt,  "
						+" quote_currency_cd, quote_datestamp, quote_expiration_value, quote_expiration_uom, comment_txt, b.create_Dt, "
						+" b.create_user_id, b.update_dt, b.update_user_id  "
						+" ,coalesce(company_name,'') as carrier_name "
						+" ,coalesce(company_code,'') as carrier_cd "
						+" ,c.rating_value as carrier_rating "
						+" ,IF( coalesce(st.tender_expiration_dt,'2000-01-01') < now(), 'Y','N') as bid_ended "
						+" , st.status_id "
						+" , st.payment_term_id "
						+" , st.high_end_bid_amt, st.low_end_bid_amt, st.target_rate_amt "
						+" , st.currency_cd"
						+" , coalesce( b.bid_rank,0) as bid_rank"
						+" FROM shipment st "
						+" LEFT JOIN shipment_carrier_bid b on st.shipment_id=b.shipment_id "
						+" LEFT JOIN company c on c.company_id=b.carrier_id "
						//+" LEFT JOIN company_service s on s.company_id = b.carrier_id "
						+" where st.shipment_id=?";
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentCarrierBid> driverRawMapper = new ResultSetMapper<ShipmentCarrierBid>();
	        	List<ShipmentCarrierBid> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentCarrierBid.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<ShipmentDetail> doGetShipmentDetails(int shipmentId){
		
		List<ShipmentDetail> shipments = new ArrayList<ShipmentDetail>();
		String sql = 	" SELECT shipment_detail_id, shipment_id, qty, unit_of_measure, "
						+" shipment_wt, wt_uom, shipment_vol, vol_uom,  "
						+" shipment_len, shipment_wth, shipment_ht, "
						+" dimension_uom, freight_class_cd, is_hazardous, create_dt, update_dt  "
						+" ,stop_nbr "
						+" ,stop_location_name "
						+" ,stop_location_addr "
						+" ,stop_location_city_name "
						+" ,stop_location_province_name "
						+" ,stop_location_country_cd "
						+" ,stop_location_postal_cd "
						+" ,sd.stop_po_Nbr "
						+" ,sd.stop_release_nbr "
						+" FROM shipment_detail where shipment_id=?";
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentDetail> driverRawMapper = new ResultSetMapper<ShipmentDetail>();
	        	List<ShipmentDetail> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentDetail.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<ShipmentAccessorial> doGetShipmentAccessorials(int shipmentId){
		
		List<ShipmentDetail> shipments = new ArrayList<ShipmentDetail>();
		String sql = 	" SELECT shipment_id, ACCESSORIAL_DESCRIPTION, ACCESSORIAL_CD, "
						+ " ACCESSORIAL_TYPE, "
						+ " if(accessorial_type='FC','Fix Charge',accessorial_data_source) as ACCESSORIAL_DATA_SOURCE, "
						+ " create_dt, update_dt, " 
						+ " ACCESSORIAL_AMT "
						+" FROM shipment_accessorial where shipment_id=?";
				
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			pstmt.setInt(1, shipmentId);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ShipmentAccessorial> driverRawMapper = new ResultSetMapper<ShipmentAccessorial>();
	        	List<ShipmentAccessorial> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentAccessorial.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
   

	private List<CompanyClient> doGetCompanyClientsObject(int companyId){
		
		
		List<CompanyClient> clientList = new ArrayList(); 
		
		try {
			
			String sql = 	" SELECT Company_Client_ID, Company_ID, Client_Name, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "+
							" FROM marketplace_db.company_client " +
							" WHERE Company_ID  = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
				pstmt.setInt(1, companyId);
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CompanyClient> driverRawMapper = new ResultSetMapper<CompanyClient>();
					clientList  = driverRawMapper.mapResultSetToObject(rs, CompanyClient.class);
			     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return clientList;
	}
	
	
	private List<CompanyBranch> doGetCompanyBranchesObject(int companyId){
		
		List<CompanyBranch> pojoList =  new ArrayList();
		List<CompanyBranch> companyBranchPojoList = new ArrayList();
		try {
			
			String sql =	" SELECT  Company_Branch_ID, Branch_Name, Is_Truck_Pool, Company_ID, Company_Address_ID, "+
  							" Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "+ 
  							" FROM company_branch WHERE company_id = ? order by Branch_Name;";
  			try (
  					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 					PreparedStatement pstmt = connection.prepareStatement(sql );) 
  				{
 				
 				pstmt.setInt(1, companyId);
 		        try (ResultSet rs = pstmt.executeQuery()) {
		        		ResultSetMapper<CompanyBranch> driverRawMapper = new ResultSetMapper<CompanyBranch>();
		        		 pojoList  = driverRawMapper.mapResultSetToObject(rs, CompanyBranch.class);
		        		if ((pojoList!=null) && (!pojoList.isEmpty())) {
		        			for (CompanyBranch cb: pojoList){
		        				CompanyAddress caObj = doGetCompanyAddress(cb.getCompanyAddressId());
		        				cb.setBranchAddress(caObj);
		        				companyBranchPojoList.add(cb);
		        			}
		        		}
 		         }
 			} catch (Exception e) {
 				log.error("Error message: " + e, e);
 				new ServiceException(Response.Status.CONFLICT);
 			}	
				
 		} catch (Exception e) {
			// TODO: handle exception
 		}
		return companyBranchPojoList;
	}
	
	
	private Country doGetCountryObject(String countryName){
		
		Country country = null;
		List<CompanyBranch> companyBranchPojoList = new ArrayList();
		try {
		
			String sql =	" SELECT Country_ID, Country_Name, Country_3_CD, Country_2_CD FROM marketplace_db.country "+
  							" where country_name = ?;";
  			try (
  					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 					PreparedStatement pstmt = connection.prepareStatement(sql );) 
  				{
 				
 				pstmt.setString(1, countryName);
 		        try (ResultSet rs = pstmt.executeQuery()) {
		        		ResultSetMapper<Country> driverRawMapper = new ResultSetMapper<Country>();
		        		List<Country> pojoList  = driverRawMapper.mapResultSetToObject(rs, Country.class);
		        		if ((pojoList!=null) && (!pojoList.isEmpty())) {
		        			country = pojoList.get(0);
		        		}
 		         }
 			} catch (Exception e) {
 				log.error("Error message: " + e, e);
 				new ServiceException(Response.Status.CONFLICT);
 			}	
				
 		} catch (Exception e) {
			// TODO: handle exception
 		}
		return country;
	}
	
	
	
	private CompanyAddress doGetCompanyAddressByGeoInfo(
				String line1Address,
				String line2Address,
				String cityName,
				int countryId,
				String provinceName,
				String postalCd,
				int userId
			){
		
		CompanyAddress companyAddress  = null;
		try {
			int provinceId= -1;
			if ((provinceName!=null) && (provinceName.length()>0)){ 
				provinceId=doUpsertProvinceByCountryAndProvinceNameObject(provinceName, countryId, userId) ;
			}
			
			String sql = 	" SELECT Company_Address_ID, Address_Line_1, Address_Line_2, Address_Line_3, City_Name, "+
					" Province_ID, Country_ID, Postal_Cd, Years_Of_Operation, Business_Headline, Business_Description, "+
					" Create_User_ID, Create_Dt, Update_User_ID, Update_Dt,latitude, longtitude "+ 
					" FROM marketplace_db.company_address "+
					" WHERE  Address_Line_1 = ? "+
					" and coalesce(Address_Line_2,'') = ? "+
					" and coalesce(City_Name,'') = ? "+
					" and Province_ID = ? "+
					" and Country_ID = ? "+
					" and coalesce(Postal_Cd,'') = ? ;";
			
 			try (
 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
 				{
				
	 				pstmt.setString(1, blankWhenNull(line1Address));
	 				pstmt.setString(2, blankWhenNull(line2Address));
					pstmt.setString(3, blankWhenNull(cityName));
					pstmt.setInt(4, provinceId);
					pstmt.setInt(5, countryId);
					pstmt.setString(6, blankWhenNull(postalCd));
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<CompanyAddress> driverRawMapper = new ResultSetMapper<CompanyAddress>();
					List<CompanyAddress> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, CompanyAddress.class);
					if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
						companyAddress = pojoListObj.get(0);
					}
					
			     }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}	} catch (Exception e) {
			// TODO: handle exception
		}
		return companyAddress;
	}
	
	private DocumentType doGetCompanyCertification(int companyId){
	
		DocumentType documentType  = null;
	try {
		
		String sql = 	" select -1 as document_type_id,  "+
						" group_concat(description_txt SEPARATOR ', ') as description_txt " +
						" from company_file cf " + 
						" join file_document_type fdt on  cf.company_file_id=fdt.company_file_id "  +
						" join document_type dt on dt.document_type_id = fdt.document_type_id " +
						" where cf.is_legal_document<>0 " +
						" and company_id = ? ;";
		
		 //log.info("companyId:"+companyId+">>>>"+sql);
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{

				pstmt.setInt(1, companyId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<DocumentType> driverRawMapper = new ResultSetMapper<DocumentType>();
				List<DocumentType> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, DocumentType.class);
				if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
					documentType = pojoListObj.get(0);
					// documentType.setDocumentTypeId(-1);
				}
				
		     }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}	} catch (Exception e) {
		// TODO: handle exception
	}
		return documentType;
	}
	
	private int doUpsertProvinceByCountryAndProvinceNameObject(String provinceName, int countryId, int userId){
		
		
		Province province  = new Province();
		if ((provinceName == null)  || (provinceName.length()==0)){
			return -1;
		}
		int provinceId = -1;
		
		try {
			
				String sql = 	" SELECT Province_ID, Country_ID, Province_Name, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt FROM marketplace_db.province "+
	 				" WHERE Province_Name  = ? and Country_ID = ?;";
				
	 			try (
	 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql);) 
	 				{
					
	 				pstmt.setString(1, provinceName);
					pstmt.setInt(2, countryId);
					try (ResultSet rs = pstmt.executeQuery()) {
						ResultSetMapper<Province> driverRawMapper = new ResultSetMapper<Province>();
						List<Province> pojoListObj  = driverRawMapper.mapResultSetToObject(rs, Province.class);
						if ((pojoListObj!=null) && (!pojoListObj.isEmpty())) {
							province = pojoListObj.get(0);
							provinceId = province.getProvinceId();
						}
						
				     }
				} catch (Exception e) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}	
	 			
	 			
	 		// if not exists, then insert
 			if(provinceId==-1){
 				
 				sql = "insert into province "
 						+" (province_Name,country_Id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
 						+" (?,?,?,?,?,?);";
 				
 				try (
 						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
 					{
 					
	 					pstmt.setString(1, provinceName);
	 					pstmt.setInt(2, countryId);
	 					pstmt.setInt(3, userId);
	 					pstmt.setTimestamp(4, getCurrentTimeStamp());
	 					pstmt.setInt(5, userId);
	 					pstmt.setTimestamp(6, getCurrentTimeStamp());
	 					
	 					int id = pstmt.executeUpdate();
	 					ResultSet rs = pstmt.getGeneratedKeys();
	 					if (rs != null && rs.next()) {
	 						provinceId = rs.getInt(1);
	 					}
 				} catch (Exception e) {
 					log.error("Error message: " + e, e);
 					new ServiceException(Response.Status.CONFLICT);
 				}
 			}else{
 				log.info("provinceId exists:"+provinceId);
 			}
	 			
	 	} catch (Exception e) {
	 		log.error(e,e);
		}
		return provinceId;
	}
	
	
	private CompanyAddress doUpsertCompanyAddressObject(int companyId , CompanyAddress corporateAddress, int provinceId, int countryId, int userId) {
		
		
		CompanyAddress companyAddress  = null;
		int companyAddressId = -1;
		
		try {
			
			companyAddress = doGetCompanyAddress(companyId);
				
 			if ((companyAddress==null) || (companyAddress.getCompanyAddressId() < 1) || (companyAddressId==-1)){
 				
 				String sql = "insert into company_address "
 						+" (address_Line_1,address_Line_2,city_Name,province_Id,country_Id,postal_Cd,create_User_Id,create_Dt,update_User_Id,update_Dt,latitude, longtitude) VALUES "
 						+" (?,?,?,?,?,?,?,?,?,?);";
 				
 				try (
 						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 						//PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
 						PreparedStatement pstmt = connection.prepareStatement(sql);)
 					{
	 					pstmt.setString(1, corporateAddress.getAddressLine1());
	 					pstmt.setString(2, corporateAddress.getAddressLine2());
	 					pstmt.setString(3, corporateAddress.getCityName());
	 					pstmt.setInt(4, provinceId);
	 					pstmt.setInt(5, countryId);
	 					pstmt.setString(6, corporateAddress.getPostalCd());
	 					pstmt.setInt(7, userId);
	 					pstmt.setTimestamp(8, getCurrentTimeStamp());
	 					pstmt.setInt(9, userId);
	 					pstmt.setTimestamp(10, getCurrentTimeStamp());
	 					pstmt.setBigDecimal(11, corporateAddress.getLat());
	 					pstmt.setBigDecimal(12, corporateAddress.getLon());
	 					
	 					int id = pstmt.executeUpdate();
	 					ResultSet rs = pstmt.getGeneratedKeys();
	 					if (rs != null && rs.next()) {
	 						companyAddressId = rs.getInt(1);
	 						companyAddress.setCompanyAddressId(companyAddressId);
	 						companyAddress=corporateAddress;
	 					}
	 					
 				} catch (Exception e) {
 					log.error("Error message: " + e, e);
 					new ServiceException(Response.Status.CONFLICT);
 				}
 			}else{
 				log.info("companyAddressId exists:"+companyAddressId);
 			}
	 			
	 	} catch (Exception e) {
	 		log.error(e,e);
		}
		return companyAddress;
	}
	public Integer getProvinceModel(String provinceName,Integer countryId) {
		// PROVINCE
		int provinceId = -1;

		// check if country already exists
		String sql = "SELECT * FROM `province` WHERE province_name = ? and country_id = ? ;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {

			pstmt.setString(1, provinceName);
			pstmt.setInt(2, countryId);
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					com.openport.marketplace.model.Province prvnc = new com.openport.marketplace.model.Province(rs);
					provinceId = prvnc.getProvinceId();
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		// if not exists, then insert
		if (provinceId == -1) {

			sql = "insert into province "
					+ " (province_Name,country_Id,create_User_Id,create_Dt,update_User_Id,update_Dt) VALUES "
					+ " (?,?,?,?,?,?);";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

				pstmt.setString(1, provinceName);
				pstmt.setInt(2, countryId);
				pstmt.setInt(3, 11);
				pstmt.setTimestamp(4, getCurrentTimeStamp());
				pstmt.setInt(5, 11);
				pstmt.setTimestamp(6, getCurrentTimeStamp());

				int id = pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs != null && rs.next()) {
					provinceId = rs.getInt(1);
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
			//return provinceId;
		}
		return provinceId;
	}
	private CompanyAddress updateCompanyAddress(CompanyAddress address) {
		String sql = "UPDATE `company_address` SET " + " address_Line_1 = ?,address_Line_2 = ? ,address_Line_3 = ?,city_Name=?,province_Id=?,country_id=?,postal_cd=?,update_user_id=?,"
				+" latitude = ? , longtitude = ? , "
				+ " update_dt=now() "
				+ " WHERE  company_address_id = ?  and update_dt=?";
		
		int provinceId=getProvinceModel(address.getProvinceName(), address.getCountryId());
		log.debug(" provinceId:"+provinceId);
		log.debug(" sql:"+sql);
		
		if((address.getCountryId()==-1) && (address.getCountryName()!=null)){
			Country country = doGetCountryObject(address.getCountryName());
			if(country != null){
				address.setCountryId(country.getCountryId());
			}
		}

		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, address.getAddressLine1());
			pstmt.setString(2,address.getAddressLine2());
			pstmt.setString(3, address.getAddressLine3());
			pstmt.setString(4, address.getCityName());
			pstmt.setInt(5, provinceId);
			pstmt.setInt(6, address.getCountryId());
			pstmt.setString(7, address.getPostalCd());
			pstmt.setInt(8, address.getUpdateUserId());
			pstmt.setBigDecimal(9, address.getLat());
			pstmt.setBigDecimal(10, address.getLon());
			pstmt.setInt(11, address.getCompanyAddressId());
			pstmt.setTimestamp(12, address.getUpdateDt());
			
			// pstmt.setString(5, trimString(company.getBusinessHeadline()));
			// pstmt.setString(6, trimString(company.getBusinessDescription()));
			int iRslt = pstmt.executeUpdate();
			return doGetCompanyAddress(address.getCompanyAddressId());
			// return address;
			
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return address;
	}
	
	
	private CompanyAddress updateCompanyAddressNoUpdateDtMatch(CompanyAddress address) {
		String sql = "UPDATE `company_address` SET " + " address_Line_1 = ?,address_Line_2 = ? ,address_Line_3 = ?,city_Name=?,province_Id=?,country_id=?,postal_cd=?,update_user_id=?,"
				+" latitude = ? , longtitude = ? , "
				+ " update_dt=now() "
				+ " WHERE  company_address_id = ?";
		
		int provinceId=getProvinceModel(address.getProvinceName(), address.getCountryId());
		log.debug(" provinceId:"+provinceId);
		log.debug(" sql:"+sql);

		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, address.getAddressLine1());
			pstmt.setString(2,address.getAddressLine2());
			pstmt.setString(3, address.getAddressLine3());
			pstmt.setString(4, address.getCityName());
			pstmt.setInt(5, provinceId);
			pstmt.setInt(6, address.getCountryId());
			pstmt.setString(7, address.getPostalCd());
			pstmt.setInt(8, address.getUpdateUserId());
			pstmt.setBigDecimal(9, address.getLat());
			pstmt.setBigDecimal(10, address.getLon());
			pstmt.setInt(11, address.getCompanyAddressId());
			
			// pstmt.setString(5, trimString(company.getBusinessHeadline()));
			// pstmt.setString(6, trimString(company.getBusinessDescription()));
			int iRslt = pstmt.executeUpdate();
			return doGetCompanyAddress(address.getCompanyAddressId());
			// return address;
			
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return address;
	}
	
	private CompanyAddress doUpsertCompanyAddressObjectByGeo(
			CompanyAddress aCompanyAddress,
			int userId) {
		
			int companyAddressId = -1;
			int provinceId = -1;
			
			CompanyAddress companyAddress = null;
			
			try {
				aCompanyAddress.setStatus(0);
				int countryId = -1;
				log.debug("start");
				try {
					log.debug("get aCompanyAddress.getCountryId()");
					countryId = aCompanyAddress.getCountryId();	
					log.debug("countryId:"+countryId);
				} catch (Exception e) {
					// TODO: handle exception
					log.info("country id is null. must be at least -1");
				}
				
				if((countryId == -1) && (aCompanyAddress.getCountryName()!=null)){
					Country country = doGetCountryObject(aCompanyAddress.getCountryName());
					if(country != null){
						countryId = country.getCountryId();
						log.debug("country id is :"+countryId);
						aCompanyAddress.setCountryId(countryId);
					}
				}
				
				companyAddress = doGetCompanyAddressByGeoInfo(
					aCompanyAddress.getAddressLine1(),
					aCompanyAddress.getAddressLine2(),
					aCompanyAddress.getCityName(),
					countryId,
					aCompanyAddress.getProvinceName(),
					aCompanyAddress.getPostalCd(),
					userId
				);
				
				if (companyAddress == null ){
					System.out.println("companyAddress to update");
					log.info("country id is :"+countryId+" province anme:"+aCompanyAddress.getProvinceName());
					provinceId = doUpsertProvinceByCountryAndProvinceNameObject(aCompanyAddress.getProvinceName(), aCompanyAddress.getCountryId(), userId);
					
					if((countryId==-1) && (companyAddress.getCountryName()!=null)){
						Country country = doGetCountryObject(companyAddress.getCountryName());
						if(country != null){
							countryId = country.getCountryId();
						}
					}
					
					String sql = "insert into company_address "
					+" (address_Line_1,address_Line_2,city_Name,province_Id,country_Id,postal_Cd,create_User_Id,create_Dt,update_User_Id,update_Dt,latitude, longtitude) VALUES "
					+" (?,?,?,?,?,?,?,now(),?,now(),?,?);";
					log.info("inserting " + aCompanyAddress);
					try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
					{
					pstmt.setString(1, aCompanyAddress.getAddressLine1());
					pstmt.setString(2, aCompanyAddress.getAddressLine2());
					pstmt.setString(3, aCompanyAddress.getCityName());
					pstmt.setInt(4, provinceId);
					pstmt.setInt(5, countryId);
					pstmt.setString(6, aCompanyAddress.getPostalCd());
					pstmt.setInt(7, userId);
				//	pstmt.setTimestamp(8, getCurrentTimeStamp());
					pstmt.setInt(8, userId);
				//	pstmt.setTimestamp(10, getCurrentTimeStamp());
					pstmt.setBigDecimal(9, aCompanyAddress.getLat());
					pstmt.setBigDecimal(10, aCompanyAddress.getLon());
					
					int id = pstmt.executeUpdate();
					ResultSet rs = pstmt.getGeneratedKeys();
					if (rs != null && rs.next()) {
						companyAddressId = rs.getInt(1);
						aCompanyAddress.setCompanyAddressId(companyAddressId);
						companyAddress = aCompanyAddress;
						companyAddress=doGetCompanyAddress(companyAddressId);
					}
					} catch (Exception e) {
						log.error("Error message: " + e, e);
						aCompanyAddress.setStatus(-1);
						return aCompanyAddress;
						//new ServiceException(Response.Status.CONFLICT);
					}
					
				}else if ((aCompanyAddress.getLat()!=null) || (aCompanyAddress.getLon()!=null)){
					log.debug("update latlon:"+aCompanyAddress.getLat()+" lon:"+aCompanyAddress.getLon());
					companyAddress.setLat(aCompanyAddress.getLat());
					companyAddress.setLon(aCompanyAddress.getLon());
					companyAddress=updateCompanyAddressNoUpdateDtMatch(companyAddress);
				}else{
					log.debug("Nothing to update");
				}
			
			} catch (Exception e) {
				log.error(e,e);
			}
			return companyAddress;
	}
	
	
	
	private void doUpdateCompanyWizardCorporateAddressId(Company c) {
		
		
		try {
			
			String sql = "UPDATE `company` SET "+
					" wizard_step_nbr = ? ,"+
					" corporate_address_id = ? "+
					" WHERE  company_id = ? "; 
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
			pstmt.setInt(1, c.getWizardStepNbr());
			pstmt.setInt(2, c.getCorporateAddressId());
			pstmt.setInt(3, c.getCompanyId());
			int iRslt = pstmt.executeUpdate();
			
			if(iRslt<1){
				log.error("Faild to update wizard_step_nbr:"+c.getWizardStepNbr()+" for companyId:"+c.getCompanyId());
			}
			
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	private void doUpdateCompanyWizardStepNbr(Company c) {
		
		
		try {
			
			String sql = "UPDATE `company` SET "+
					" wizard_step_nbr = ? "+
					" WHERE  company_id = ? "; 
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
			pstmt.setInt(1, c.getWizardStepNbr());
			pstmt.setInt(2, c.getCompanyId());
			int iRslt = pstmt.executeUpdate();
			
			if(iRslt<1){
				log.error("Faild to update wizard_step_nbr:"+c.getWizardStepNbr()+" for companyId:"+c.getCompanyId());
			}
			
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void doUpdateWizardStepNbr(int wizardStepNbr, int companyId) {
		
		
		try {
			
			String sql = "UPDATE `company` SET "+
					" wizard_step_nbr = ? "+
					" WHERE  company_id = ? "; 
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
			pstmt.setInt(1, wizardStepNbr);
			pstmt.setInt(2, companyId);
			int iRslt = pstmt.executeUpdate();
			
			if(iRslt<1){
				log.error("Faild to update wizard_step_nbr:"+wizardStepNbr+" for companyId:"+companyId);
			}
			
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void doUpdateCompanyBranchAddressId(int companyBranchId, int companyAddressId) {
		
		
		try {
			String sql = "UPDATE COMPANY_BRANCH SET "+
					" company_address_id = ? "+
					" WHERE  company_branch_id = ? "; 
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
			pstmt.setInt(1, companyAddressId);
			pstmt.setInt(2, companyBranchId);
			int iRslt = pstmt.executeUpdate();
			
			if(iRslt<1){
				log.error("Faild to update COMPANY_BRANCH company_address_id:"+companyAddressId+" for company_branch_id:"+companyBranchId);
			}
			
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void doUpdateCompanyBranchAddressIdAndBranchName(String branchName, Integer companyBranchId, int companyAddressId) {
		
		
		try {
			
			String sql = "UPDATE COMPANY_BRANCH SET "+
					" branch_name = ? "+
					" company_address_id = ? "+
					" WHERE  company_branch_id = ? "; 
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
				
			pstmt.setString(1, branchName);
			pstmt.setInt(2, companyAddressId);
			pstmt.setInt(3, companyBranchId);
			int iRslt = pstmt.executeUpdate();
			
			if(iRslt<1){
				log.error("Faild to update COMPANY_BRANCH company_address_id:"+companyAddressId+" for company_branch_id:"+companyBranchId);
			}
			
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void doUpdateCompanyBranchAddressIdAndBranchName(String branchName, Integer companyBranchId) {
		
		
		try {
			String sql = "UPDATE COMPANY_BRANCH SET "+
					" branch_name = ? "+
					" WHERE  company_branch_id = ? "; 
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
				
			pstmt.setString(1, branchName);
			pstmt.setInt(2, companyBranchId);
			int iRslt = pstmt.executeUpdate();
			
		
			
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void doDeleteCompanyBranch(int companyBranchId) {
		
		
		try {
			String sql = "DELETE FROM COMPANY_BRANCH "+
					" WHERE  company_branch_id = ? "; 
		
			try (
					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) 
				{
			pstmt.setInt(1, companyBranchId);
			int iRslt = pstmt.executeUpdate();
			
			if(iRslt<1){
				log.error("Faild to delete COMPANY_BRANCH companyBranchId:"+companyBranchId);
			}
			
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private ShipmentCarrierBid doUpsertShipmentCarrierBidObject(ShipmentCarrierBid carrierBid){
		
		int shipmentCarrierBidId = -1;
		
		try {
			
				String sql = "call sp_carrier_bid_insert( "
									+"'"+carrierBid.getShipmentId()+"',"
									+"'"+carrierBid.getCarrierId()+"',"
									+"'"+carrierBid.getRateTypeCd()+"',"
									+"'"+carrierBid.getRateTypeDescTxt()+"',"
									+"'"+carrierBid.getTransitTm()+"',"
									+"'"+carrierBid.getEquipmentTypeId()+"',"
									+"'"+carrierBid.getQuoteAmt()+"',"
									+"'"+carrierBid.getQuoteCurrencyCd()+"',"
									+"'"+blankWhenNull(carrierBid.getCommentTxt())+"',"
									+"'"+carrierBid.getCarrierId()+"'"
									+")";
				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql);) {
						System.out.println("sql doUpsertShipmentCarrierBidObject:"+sql);
						//return pstmt.executeUpdate();
						try (ResultSet rs = pstmt.executeQuery()) {
							ResultSetMapper<ShipmentCarrierBid> driverRawMapper = new ResultSetMapper<ShipmentCarrierBid>();
				        	List<ShipmentCarrierBid> pojoList = driverRawMapper.mapResultSetToObject(rs, ShipmentCarrierBid.class);
				        	if (pojoList!=null && !pojoList.isEmpty()) {
				        		return pojoList.get(0);
							}
						}
						
				} catch (Exception e) {
					
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
			
	 	} catch (Exception e) {
	 		log.error(e,e);
		}
		return null;
	}
	
	private int doUpsertShipmentCarrierBidObjectV1(ShipmentCarrierBid carrierBid){
		
		int shipmentCarrierBidId = -1;
		
		try {
			
				String sql = "call sp_carrier_bid_insert( "
									+"'"+carrierBid.getShipmentId()+"',"
									+"'"+carrierBid.getCarrierId()+"',"
									+"'"+carrierBid.getRateTypeCd()+"',"
									+"'"+carrierBid.getRateTypeDescTxt()+"',"
									+"'"+carrierBid.getTransitTm()+"',"
									+"'"+carrierBid.getEquipmentTypeId()+"',"
									+"'"+carrierBid.getQuoteAmt()+"',"
									+"'"+carrierBid.getQuoteCurrencyCd()+"',"
									+"'"+carrierBid.getCommentTxt()+"',"
									+"'"+carrierBid.getCarrierId()+"'"
									+")";
				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql);) {
						System.out.println("sql doUpsertShipmentCarrierBidObject:"+sql);
						return pstmt.executeUpdate();
						
				} catch (Exception e) {
					
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
				/*
 				String sql = "insert into shipment_carrier_bid "
 						+" (carrier_id, shipment_id, quote_status_cd, rate_type_cd, rate_type_desc_txt,"
 						+" transit_tm, equipment_type_id, quote_amt, quote_currency_cd,"
 						+" quote_datestamp, quote_expiration_value, quote_expiration_uom,"
 						+" comment_txt, create_Dt, create_user_id, update_dt, update_user_id"
 						+ ") VALUES "
 						+" (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
 				

 				
 				try (
 						java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
 						PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) 
 					{
 					
	 					pstmt.setInt(1, carrierBid.getCarrierId());
	 					pstmt.setInt(2, carrierBid.getShipmentId());
	 					pstmt.setString(3, "P");
	 					pstmt.setString(4, carrierBid.getRateTypeCd());
	 					pstmt.setString(5, carrierBid.getRateTypeDescTxt());
	 					pstmt.setDouble(6, carrierBid.getTransitTm());
	 					pstmt.setInt(7, carrierBid.getEquipmentTypeId()); // equipmentTypeId
	 					pstmt.setDouble(8, carrierBid.getQuoteAmt());
	 					pstmt.setString(9, carrierBid.getQuoteCurrencyCd());
	 					pstmt.setTimestamp(10, getCurrentTimeStamp());
	 					pstmt.setInt(11, 0); // quote expiration value
	 					pstmt.setString(12, ""); // quote expiration value
	 					pstmt.setString(13, carrierBid.getCommentTxt()); // comment txt
	 					pstmt.setTimestamp(14, getCurrentTimeStamp());
	 					pstmt.setInt(15, carrierBid.getCarrierId()); // create user id
	 					pstmt.setTimestamp(16, getCurrentTimeStamp());
	 					pstmt.setInt(17, carrierBid.getCarrierId()); // update user id
	 					
	 					int id = pstmt.executeUpdate();
	 					ResultSet rs = pstmt.getGeneratedKeys();
	 					if (rs != null && rs.next()) {
	 						shipmentCarrierBidId = rs.getInt(1);
	 					}
 				} catch (Exception e) {
 					log.error("Error message: " + e, e);
 					new ServiceException(Response.Status.CONFLICT);
 				}
 				*/
	 	} catch (Exception e) {
	 		log.error(e,e);
		}
		return shipmentCarrierBidId;
	}
	
	
	private int doCallStoredProcUpdate(String sql){
		
		try {
			
				
				try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
						PreparedStatement pstmt = connection.prepareStatement(sql);) {
					log.info("sql"+sql);
						return pstmt.executeUpdate();
				} catch (Exception e) {
					
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
				
	 	} catch (Exception e) {
	 		log.error(e,e);
		}
		return 1;
	}
	
	
	public UserStatus checkLanguage(UserStatus sts, long langVer) {
		//return dDao.checkLanguage(sts, langVer);


		int latestVer = 0;
		long currentLanguageId = -1;
		try {
			log.debug(latestVer);
			latestVer=doGetMaxLabelIdFromLabel();
		
			if (langVer>=latestVer) {
				return sts; // nothing to do
			}

    		LanguagePack languageUpdate = new LanguagePack(latestVer);
    		sts.setUpdateLanguage(languageUpdate);
    		
	    	List<Language> languagePacks = new LinkedList<Language>();
	    	languageUpdate.setLanguagePack(languagePacks);

	    	
	    	HashMap<Integer, Language> languages = new HashMap<Integer, Language>();
	    	List<Language> languageList = new ArrayList();
	    	languageList = doGetLanguages();
	    	
	    	for (Language l: languageList){
	    		int id = l.getLanguageId();
				String cd = l.getLanguageCd();
				String txt = l.getLanguageName();
				System.out.println(""+id+" "+cd+" "+txt);
				languages.put(id, new Language(cd,txt));
				// languages.put(id, l);
	    	}
	    	
	    	System.out.println("langVer:"+langVer);
	    	List<Label> labelsList = new ArrayList();
	    	labelsList = doGetLabels(langVer);
	    	System.out.println(" size:"+labelsList.size());
		    
	    	if ( (labelsList==null) || (labelsList.size()==0)) {
	    		System.out.println("returning");
				return sts;
			}
	    	
	    	List<Label> labels = null;

	    	List<Label> labels2 = null;
	    	System.out.println("looping label");
	    	try {
	    		int langId =0;
	    		for (Label lbl: labelsList) {
	    			try {
	    				System.out.println("1");
				    	
				    	langId = lbl.getLanguageId();
				    	String cd = lbl.getLabelCd();
				    	String txt = lbl.getLabelTxt();
				    	System.out.println(" langId:"+langId+" cd:"+cd+" txt:"+txt);
			            // log.debug("\t" + langId+"\t" + cd+"\t" + txt);
			            if (langId != currentLanguageId) {
			            	System.out.println("langId:"+langId+" currentLanguageId:"+currentLanguageId);
			            	Language language = languages.get(langId);
			     	    	labels = language.getData();
			     	    	System.out.println("labels size:"+labels.size());
							languagePacks.add(language);
			     	    	currentLanguageId = langId;
			            }
			        	labels.add(new Label(cd, txt));
			        	System.out.println("labels size:"+labels.size());
			        	Language language2 = languages.get(langId);
			        	labels2 = language2.getData();
		     	    	System.out.println("labels2 size:"+labels2.size());
					} catch (Exception ex) {
						log.error(ex,ex);
					}
			    	
			    }
	    		
	    		//Language language = languages.get(langId);
	  	    	//labels = language.getData();
	 			//languagePacks.add(language);
			} catch (Exception ex) {
				log.error(ex,ex);
			}
		    
		} catch (Exception ex) {
			log.error(ex,ex);
		} 
		return sts;
	
		
		
	}
	

	private List<Label> doGetLabels(long langVer) {
		// TODO Auto-generated method stub
		
		String sql = 	" select "
				+" language_id, "
				+" label_id, Label_CD, label_txt "
				+" create_dt, create_user_id "
				+ " from marketplace_db.label "
				+ " where label_id > ? "
				+ " order by language_id, label_id";
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setLong(1, langVer);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Label> driverRawMapper = new ResultSetMapper<Label>();
	        	List<Label> pojoList = driverRawMapper.mapResultSetToObject(rs, Label.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return null;
	}



	private List<Language> doGetLanguages() {
		String sql = " select language_id, language_cd, language_name "	
				//" select language_id,language_name, "
				// +" Iso_639_2_CD, "
				//+" language_cd, create_user_id,create_dt,update_user_id,update_dt "
				+" from  marketplace_db.language ; ";
				
		
		
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Language> driverRawMapper = new ResultSetMapper<Language>();
	        	List<Language> pojoList = driverRawMapper.mapResultSetToObject(rs, Language.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	private List<Language> doGetLanguageList() {
		String sql = " select  language_cd, language_name "	
				//" select language_id,language_name, "
				// +" Iso_639_2_CD, "
				//+" language_cd, create_user_id,create_dt,update_user_id,update_dt "
				+" from  marketplace_db.language ; ";
				
		
		
		
		System.out.println("sql:"+sql);
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Language> driverRawMapper = new ResultSetMapper<Language>();
	        	List<Language> pojoList = driverRawMapper.mapResultSetToObject(rs, Language.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		return pojoList;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}



	private int doGetMaxLabelIdFromLabel() {
		String sql = 	" select max(label_id) as label_id,  '' as label_cd,'' as label_txt,now() as create_dt,'' as create_user_id  from marketplace_db.label ; ";
		
		System.out.println("sql:"+sql);
		
	
		
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Label> driverRawMapper = new ResultSetMapper<Label>();
	        	List<Label> pojoList = driverRawMapper.mapResultSetToObject(rs, Label.class);
	        	if (pojoList!=null && !pojoList.isEmpty()) {
	        		Label lbl = pojoList.get(0);
	        		return lbl.getLabelId();
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return 0;
	}



	private Date futureExpiration() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 7);
		Date expires = cal.getTime();
		return expires;
	}

	private Token generateToken() {
		Token tkn = new Token();
		tkn.setAccessToken(MySqlPassword.generateToken());
		tkn.setExpires(futureExpiration());
		return tkn;
	}



	
	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}
	
	private String blankWhenNull(String s){
		if((s==null) || (s.equalsIgnoreCase("NULL"))){
			s="";
		}
		return s;
	}
	
	
	 private int zeroWhenNull(Integer yearsOfOperation) {
			if (yearsOfOperation==null){
				return 0;
			}else{
				return yearsOfOperation;
			}
			
		}

	 
	 private boolean isNotNullAndNotBlank(String s) {
			if ((s!=null) && (!s.trim().equalsIgnoreCase("NULL")) && (s.trim().length()>0)){
				return true;
			}else{
				return false;
			}
		}
	 
	 
	 private void checkVersion(String langCd, String appVersion, UserStatus sts, String appId) {
			if (appId!=null && appId.equalsIgnoreCase("com.openport.delivery.uat")) {
				if (appVersion.compareTo(CURRENT_UAT_VERSION) < 0) {
					if (langCd.equalsIgnoreCase("id")) {
						sts.setMessage("Versi terbaru (" + CURRENT_UAT_VERSION + ") telah tersedia, silahkan unduh dan instal dari https://www.pgyer.com/X4b5");
					} else if (langCd.equalsIgnoreCase("zh")) {
						sts.setMessage("Openpor物流手机APP（安卓系统）已更新至最新版本" + CURRENT_UAT_VERSION + "， 请通过https://www.pgyer.com/X4b5链接下载安装");
					} else if (langCd.equalsIgnoreCase("zh-Hant")) {
						sts.setMessage("Openpor物流手機APP（安卓系統）已更新至最新版本" + CURRENT_UAT_VERSION + "， 請通過https://www.pgyer.com/X4b5鏈接下載安裝");
					} else if (langCd.equalsIgnoreCase("ur")) {
						sts.setMessage("A new version (" + CURRENT_UAT_VERSION + ") is available, please download & install from https://www.pgyer.com/X4b5");
					} else {
						sts.setMessage("A new version (" + CURRENT_UAT_VERSION + ") is available, please download & install from https://www.pgyer.com/X4b5");
					}
				}
			} else if (appId!=null && appId.toLowerCase().startsWith("com.openport.delivery")) { // if (appId.equalsIgnoreCase("com.openport.delivery.uat")) {
				if (appVersion.compareTo(CURRENT_VERSION) < 0) {
					if (langCd.equalsIgnoreCase("id")) {
						sts.setMessage("Versi terbaru (" + CURRENT_VERSION + ") telah tersedia, silahkan unduh dan instal dari https://goo.gl/aDSMdF");
					} else if (langCd.equalsIgnoreCase("zh")) {
						sts.setMessage("Openpor物流手机APP（安卓系统）已更新至最新版本" + CURRENT_VERSION + "， 请通过http://www.pgyer.com/ldXH链接下载安装");
					} else if (langCd.equalsIgnoreCase("zh-Hant")) {
						sts.setMessage("Openpor物流手機APP（安卓系統）已更新至最新版本" + CURRENT_VERSION + "， 請通過http://www.pgyer.com/ldXH鏈接下載安裝");
					} else if (langCd.equalsIgnoreCase("ur")) {
						sts.setMessage("A new version (" + CURRENT_VERSION + ") is available, please download & install from https://goo.gl/aDSMdF");
					} else {
						sts.setMessage("A new version (" + CURRENT_VERSION + ") is available, please download & install from https://goo.gl/aDSMdF");
					}
				}
			}
		}
	 
	 
	 public List<ShipmentCarrierBid> getShipmentCarrierBidListByShipmentId(int shipmentid) {

	  		CompanyAddress corporateAddress = new CompanyAddress();
	  		List<ShipmentCarrierBid> shipmentCarrierBidList = new ArrayList();
			try {
		  		//shipmentCarrierBidList = doGetShipmentCarrierBids(shipmentid);
		  		List<ShipmentCarrierBid> shipmentCarrierBids =  doGetShipmentCarrierBids(shipmentid);
				if((shipmentCarrierBids!=null) &&(shipmentCarrierBids.size()>0)){
					
					List<ShipmentAccessorial> shipmentAccessorialsList =  doGetShipmentAccessorials(shipmentid);
					
					
					for (ShipmentCarrierBid scb: shipmentCarrierBids){
						if(scb.getShipmentCarrierBidId()>0){
							
							List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails = doGetShipmentCarrierBidDetailsWithDefault(
																			scb.getShipmentCarrierBidId(),
																			scb.getCarrierId(),
																			shipmentAccessorialsList
																			);
							
							if((shipmentCarrierBidDetails!=null) && (shipmentCarrierBidDetails.size()>0)){
								scb.setShipmentCarrierBidDetails(shipmentCarrierBidDetails);
							}
							shipmentCarrierBidList.add(scb);
						}
					}
				}
		  		/*
		  		if (shipmentCarrierBidList.get(0).getShipmentCarrierBidId() == -1){
		  			new ServiceException(Response.Status.CONFLICT);
		  		}else if (shipmentCarrierBidList.size()==0){
		  			new ServiceException(Response.Status.CONFLICT);
		  		}
		  		*/
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
			return shipmentCarrierBidList;
		}
	 
	 
	 private List<Company> doGetCompanyVendorListByCountry(String countryCd){
		 	List<Company> pojoListObj  = new ArrayList();
			try {
					String sql = 	" SELECT  c.Company_ID, c.Company_Name, c.Is_Carrier, c.Is_Shipper, c.Company_Code, c.Corporate_Address_ID, c.Truck_Pool_Address_ID, c.Branch_Address_ID, "+ 
									" c.Create_User_ID, c.Create_Dt, c.Update_User_ID, c.Update_Dt, c.Wizard_Step_Nbr, c.Years_of_operation, c.Business_Headline, c.Business_Description, c.timezone_id "+ 
									" FROM marketplace_db.company c "+
									" join company_address a on c.corporate_address_id = a.company_address_id "+ 
									" join country ctr on ctr.country_id=a.country_id 	 "+
									" where c.Is_Carrier=1 and c.Is_Shipper=1  "+
									" and ctr.Country_2_CD = ? ;";
					
		 			try (
		 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
							PreparedStatement pstmt = connection.prepareStatement(sql);) 
		 				{
							pstmt.setString(1, countryCd);
							try (ResultSet rs = pstmt.executeQuery()) {
								ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
								pojoListObj  = driverRawMapper.mapResultSetToObject(rs, Company.class);
								return pojoListObj;
								
						     }
					} catch (Exception e) {
						System.out.println("sql result vendorList is null");
						log.error("Error message1: " +sql+" >>> "+ e, e);
						new ServiceException(Response.Status.CONFLICT);
					}	
	 			} catch (Exception e) { 
			System.out.println("sql error");
	 			}
			return pojoListObj;
		}
	 
	 private List<Company> doGetCompanySetVendorListByCountry(String countryCd){
		 	List<Company> pojoListObj  = new ArrayList();
		 	List<Company> companyList  = new ArrayList();
			try {
					String sql = 	" SELECT  c.Company_ID, c.Company_Name, c.Is_Carrier, c.Is_Shipper, c.Company_Code, c.Corporate_Address_ID, c.Truck_Pool_Address_ID, c.Branch_Address_ID, "+ 
									" c.Create_User_ID, c.Create_Dt, c.Update_User_ID, c.Update_Dt, c.Wizard_Step_Nbr, c.Years_of_operation, c.Business_Headline, c.Business_Description, c.timezone_id "+ 
									" FROM marketplace_db.company c "+
									" join company_address a on c.corporate_address_id = a.company_address_id "+ 
									" join country ctr on ctr.country_id=a.country_id 	 "+
									" where c.Is_Carrier=1 and c.Is_Shipper=1  "+
									" and ctr.Country_2_CD = ? ;";
					
		 			try (
		 					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
							PreparedStatement pstmt = connection.prepareStatement(sql);) 
		 				{
							pstmt.setString(1, countryCd);
							try (ResultSet rs = pstmt.executeQuery()) {
								ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
								pojoListObj  = driverRawMapper.mapResultSetToObject(rs, Company.class);
								Company companyPojo = pojoListObj.get(0);
								Company companyTemp = doGetCompanyObjectSetById((companyPojo.getCompanyId()));
								companyList.add(companyTemp);
						     }
					} catch (Exception e) {
						System.out.println("sql result vendorList is null");
						log.error("Error message1: " +sql+" >>> "+ e, e);
						new ServiceException(Response.Status.CONFLICT);
					}	
	 			} catch (Exception e) { 
			System.out.println("sql error");
	 			}
			return companyList;
		}
	 
	 
	 private Company doGetCompanyObjectSetById(int companyID){
		 
		 	Company company = new Company();
	  		CompanyAddress corporateAddress = new CompanyAddress();
	  		List<CompanyClient> clients = new ArrayList();
	  		List<CompanyBranch> companyBranchPojoList = new ArrayList();
	  		List<CompanyBranch> pojoList =  new ArrayList();
	  		
			try {
			  		
			  		
			  		company = new Company();
					int companyAddressId = -1;
					int branchAddressId = -1;
					int countryId = -1 ;
					int provinceId = -1 ;
					
					try {
						company = doGetCompanyObjectById(companyID);
						companyAddressId = company.getCorporateAddressId();
						
						// COMPANY ADDRESS 
			  			corporateAddress = doGetCompanyAddress(companyAddressId);
			  			company.setCorporateAddress(corporateAddress);
			  			
			  			
			  			// CLIENTS
			  			clients = doGetCompanyClientsObject(companyID);
						company.setClients(clients);
						
						
						// BRANCHES
						companyBranchPojoList = doGetCompanyBranchesObject(companyID);
						company.setCompanyBranches(companyBranchPojoList);
			  			
			  			
					} catch (Exception e1) {
						log.error("Error message: " + e1, e1);
						new ServiceException(Response.Status.BAD_GATEWAY);
					}
					
		 			
	 			} catch (Exception e) { 
	 				System.out.println("sql error");
	 			}
			return company;
		}
	
	 private CompanyFile insertCompanyFile(CompanyFile cf) {
			String sql = "insert into company_file "
					+ " (company_ID,create_Dt,create_User_Id,document_name,Is_Legal_Document,relative_path,update_dt,update_user_id "

					// +" ,businessHeadline,businessDescription "
					+ " ) VALUES " + " (?,?,?,?,?,?,?,?);";

			try (

					java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

			{
				
			//	UUID relativePath = UUID.randomUUID();
				pstmt.setInt(1, cf.getCompanyId());
				pstmt.setTimestamp(2, getCurrentTimeStamp());
				pstmt.setInt(3, cf.getCreateUserId());
				pstmt.setString(4, cf.getDocumentName());
				pstmt.setBoolean(5, cf.isLegalDocument());
				pstmt.setString(6, cf.getRelativePath());
				pstmt.setTimestamp(7, getCurrentTimeStamp());
				pstmt.setInt(8, cf.getUpdateUserId());

				int id = pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs != null && rs.next()) {
					cf.setCompanyFileId(rs.getInt(1));
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
			return cf;
	}
	 
	 private CompanyFile updateCompanyFile(CompanyFile cf) {
			String sql = "UPDATE `company_file` SET " + " update_User_Id = ? " + " ,update_Dt = ? " + " ,document_name = ? "
					+ " ,relative_path = ? " +

					// " ,business_Headline = ? "+
					// " ,business_Description = ? "+
					" WHERE  company_file_id = ? ";

			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setInt(1, cf.getUpdateUserId());
				pstmt.setTimestamp(2, getCurrentTimeStamp());
				pstmt.setString(3, cf.getDocumentName());
				pstmt.setString(4, cf.getRelativePath());
				pstmt.setInt(5, cf.getCompanyFileId());
				// pstmt.setString(5, trimString(company.getBusinessHeadline()));
				// pstmt.setString(6, trimString(company.getBusinessDescription()));
				int iRslt = pstmt.executeUpdate();

				if (iRslt < 1) {
					log.error("Faild to update company file ID:" + cf.getCompanyFileId());
				}
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}

			return cf;
		}
	 
	 
		
	 private FileDocumentType insertFileDocumentType(FileDocumentType fdt, DocumentType dt, CompanyFile cf) {
		String sql = "insert into file_document_type " + " (company_file_id,document_type_id "

		// +" ,businessHeadline,businessDescription "
				+ " ) VALUES " + " (?,?);";

		try (

				java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

		{
			pstmt.setInt(1, cf.getCompanyFileId());
			pstmt.setInt(2, dt.getDocumentTypeId());

			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				fdt.setFileDocumentTypeId(rs.getInt(1));
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}

		return fdt;
	}
}
