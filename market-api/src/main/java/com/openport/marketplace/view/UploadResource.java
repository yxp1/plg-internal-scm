package com.openport.marketplace.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.json.JsonArrayBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.openport.marketplace.helper.DataResultHelper;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.UserStatus;
import com.openport.marketplace.repository.TmsParameterRepository;

@Path("upload") // http://localhost:8080/market-api/files
public class UploadResource {

	private static String isUat = System.getProperty("is.uat");
	private static final String ROOT_FOLDER = "/opt/openport"
				+(((isUat == null) || (isUat.isEmpty()) 
						|| isUat.equalsIgnoreCase("false")
						|| isUat.equalsIgnoreCase("no")
						|| isUat.equalsIgnoreCase("0"))?"":"-uat")
				+"/data/DOCUMENTS/";
	//private static final String ROOT_FOLDER="C:/Users/nargv2015/test/";
	
	private static final String OPEN_TM_CONFIG_CD = "OPEN_TM_CONFIG";
	
	private static final String OPEN_TM_UPLOAD_SHIPMENT = "UPLOAD_SHIPMENT_ENDPOINT_URL";
	
	private static final String UPLOAD_SHIPMENT_DEFAULT_URL = "http://localhost:8080/tms-api/dataloader/upload/";
	
	static final transient Logger log = Logger.getLogger(UploadResource.class);
	// private TmsDeliveryRepository tmsDeliveryRepository = new TmsDeliveryRepositoryStub();
	
	TmsParameterRepository tmsParameterRepository = new TmsParameterRepository();

	@POST
	@Path("upload") // http://localhost:8080/market-api/files/upload
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({ MediaType.APPLICATION_JSON })
	public UserStatus uploadFile(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,@FormDataParam("parent") String parent) {
		log.info("parent"+parent + " " +fileDetail.getFileName());
		if(parent == null) {
			parent="UNKNOWN";
		}
		
		String extension=fileDetail.getFileName().substring(fileDetail.getFileName().lastIndexOf("."),fileDetail.getFileName().length());
		UserStatus sts = new UserStatus();
		sts.setStatus(Response.Status.OK.getStatusCode());
		sts.setMessage(Response.Status.OK.getReasonPhrase());
		String filename = "null";
		if (fileDetail!=null&&fileDetail.getFileName()!=null) {
			filename = parent+"_"+UUID.randomUUID().toString()+extension;
		}
			
		try {
			log.info("filename "+filename);
			saveFile(uploadedInputStream, ROOT_FOLDER + filename);
			sts.setFileId(filename);
			return sts;
		} catch (IOException e) {
			log.error(e + " saving filename:"+filename, e);
			new ServiceException(Response.Status.INTERNAL_SERVER_ERROR, e.toString());
		}

		new ServiceException(Response.Status.FORBIDDEN);
		return null;
	}
	
	private void saveFile(InputStream uploadedInputStream, String fileLocation)
			throws FileNotFoundException, IOException {
		FileOutputStream out = new FileOutputStream(new File(fileLocation));
		int read = 0;
		byte[] bytes = new byte[1024];

		out = new FileOutputStream(new File(fileLocation));
		while ((read = uploadedInputStream.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}
		out.flush();
		out.close();
	}

	@GET
	@Path("{relativePath}/{filename}") // http://localhost:8080/market-api/files/32de53c1-21a6-430d-a55d-f92279810e07_1448949868112.jpg/1448949868112.jpg
	public Response downloadFile(@PathParam("relativePath") int relativePath, 
			@PathParam("filename") int filename) {
		log.info("start download of fileId:"+relativePath+" as :"+filename);

		try {
			StreamingOutput fileStream =  new StreamingOutput()
	        {
	            @Override
	            public void write(java.io.OutputStream output) throws IOException, WebApplicationException
	            {
	                try
	                {
	                	log.info("Downloading "+ROOT_FOLDER+relativePath);
	                    java.nio.file.Path path = Paths.get(ROOT_FOLDER+relativePath);
	                    byte[] data = Files.readAllBytes(path);
	                    output.write(data);
	                    output.flush();
	                }
	                catch (Exception e)
	                {
	                	log.error("Unable to read "+ROOT_FOLDER+relativePath, e);
	                    // throw new WebApplicationException("File Not Found !!");
	    				new ServiceException(Response.Status.CONFLICT);
	                }
	            }
	        };
	        return Response
	                .ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
	                .header("content-disposition","attachment; filename="+relativePath)
	                .build();
		} catch( WebApplicationException ex) {
        	return Response.status(Status.CONFLICT).build();
		}
	}

	@GET
	@Path("{relativePath}") // http://localhost:8080/market-api/files/32de53c1-21a6-430d-a55d-f92279810e07_1448949868112.jpg
	public Response displayFile(@PathParam("relativePath") String relativePath) {
		log.info("start download of relativePath:"+relativePath+"; ROOT_FOLDER:"+ROOT_FOLDER);
		
		if (relativePath == null || relativePath.trim().length()==0) {
			log.error("Unable to download relativePath:"+relativePath+"; ROOT_FOLDER:"+ROOT_FOLDER);
			new ServiceException(Response.Status.NOT_FOUND);
		} else {
			try {
				StreamingOutput fileStream =  new StreamingOutput()
		        {
		            @Override
		            public void write(java.io.OutputStream output) throws IOException, WebApplicationException
		            {
		                try
		                {
		                	log.info("Downloading "+ROOT_FOLDER+relativePath);
		                    java.nio.file.Path path = Paths.get(ROOT_FOLDER+relativePath);
		                    byte[] data = Files.readAllBytes(path);
		                    output.write(data);
		                    output.flush();
		                }
		                catch (Exception e)
		                {
		                	log.error("Unable to read "+ROOT_FOLDER+relativePath, e);
		                    // throw new WebApplicationException("File Not Found !!");
		    				new ServiceException(Response.Status.CONFLICT);
		                }
		            }
		        };
		        return Response
		                .ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
		                .header("content-disposition","inline; filename="+relativePath)
		                .build();
			} catch( WebApplicationException ex) {
            	return Response.status(Status.CONFLICT).build();
			}
		}
		log.error("should never get here. relativePath:"+relativePath+"; ROOT_FOLDER:"+ROOT_FOLDER);
		new ServiceException(Response.Status.FORBIDDEN);
		return null;
	}
	
	
	@POST
	@Path("uploadApi/{userId}/{fileFormat}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({ MediaType.APPLICATION_JSON })
	public Response uploadApi(
			@PathParam("userId") int userId,
			@PathParam("fileFormat") String fileFormat,
			@FormDataParam("file") InputStream inputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {
		
		String param = userId + "/"+ fileFormat;
		
		String url = TmsParameterRepository.getString(OPEN_TM_CONFIG_CD, OPEN_TM_UPLOAD_SHIPMENT, UPLOAD_SHIPMENT_DEFAULT_URL);
		url += param;
		
		String filename = fileDetail.getFileName();
		
		CloseableHttpClient client =  HttpClientBuilder.create().build();
		
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addBinaryBody("file", inputStream, ContentType.DEFAULT_BINARY, filename);
		
		HttpEntity entity = builder.build();
		
		HttpPost post = new HttpPost(url);
		post.setEntity(entity);
		
		HttpResponse response = null;
		String jsonString = null;
		try {
			response = client.execute(post);
			jsonString = EntityUtils.toString(response.getEntity());
			post.completed();
			client.close();
		} catch (ClientProtocolException e) {
			log.error(e);
		} catch (ParseException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		
        return Response.ok(jsonString).build();
	}
	
	/**
	 * get dataloaderoptions for Upload file type dropdown list
	 * @param token
	 * @param companyCd
	 * @return 
	 */
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("dataloaderOptions/{companyCd}")
	public Response getDataloaderOptions(@HeaderParam("x-openport-token") String token,
			@PathParam("companyCd") String companyCd) {
		
		try {			
			List<Map<String,Object>> dataResult = tmsParameterRepository.getDataLoaderOptions(companyCd); //repo get list file types
	      
			JsonArrayBuilder JSONresponse = DataResultHelper.parseDateResultToJSON(dataResult);
			
			return Response.ok(JSONresponse.build().toString()).build();

		}catch(Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}finally {
			log.debug("Done");
		}
		return Response.ok(DataResultHelper.createResponseJsonObject(0,"Upload failed.")).build();
	}

}
