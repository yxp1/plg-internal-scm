package com.openport.marketplace.local;

import java.util.LinkedHashMap;
import java.util.Map;

public class DataHolderLocal {

    private static final ThreadLocal<Map<Class, Object>> LOCAL = new ThreadLocal() {
        @Override
        protected Map<Class, Object> initialValue() {
            return new LinkedHashMap<>();
        }
    };

    public static <V> void set(V data) {
        LOCAL.get().put(data.getClass(), data);
    }

    public static <V> V get(Class<V> clazz) {
        return (V) LOCAL.get().get(clazz);
    }

    public static void remove() {
        LOCAL.get().clear();
        LOCAL.remove();
    }

}
