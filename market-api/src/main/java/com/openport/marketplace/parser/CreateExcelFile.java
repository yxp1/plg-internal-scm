package com.openport.marketplace.parser;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateExcelFile {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
	}
	
	 public static void doGenerateExcelFile(){
		 
	 }
	 
     public static void doGenerateExcelFile(List<Object[]> objectList, Integer noOfColumn,String filename, String sheetName){
    	try{
			HSSFWorkbook hwb=new HSSFWorkbook();
			HSSFSheet sheet =  hwb.createSheet(sheetName);
			
			int rowCtr = 0;
			for(Object[] o : objectList) {
			for (int x=0; x< noOfColumn;x++){
				if (rowCtr==0){
					HSSFRow rowhead=   sheet.createRow((short)rowCtr);
					rowhead.createCell((short)x ).setCellValue(trimString(o[x]));
				}else{
					HSSFRow row=   sheet.createRow((short)rowCtr);
						row.createCell((short) x).setCellValue(trimString(o[x]));
						
				}
			}
			rowCtr++;
			}
			

			
			FileOutputStream fileOut =  new FileOutputStream(filename);
			hwb.write(fileOut);
			fileOut.close();
					
		} catch ( Exception ex ) {
		
		}
    }
    
    
  
    public void doGenerateExcelFileXlsx(List rowHeaderList, List<Object[]> rowDataList, String absolutefileName, String sheetName){
    	try{
			String filename="C:/EF3/EF3_SYSTEMS/1.xlsx" ;
			filename =absolutefileName;
			
			Workbook wb = new XSSFWorkbook();
			CreationHelper createHelper = wb.getCreationHelper();
			Sheet sheet = wb.createSheet(sheetName);
			
			//HSSFWorkbook hwb=new HSSFWorkbook();
			//HSSFSheet sheet =  hwb.createSheet(sheetName);
			
			int rowCtr = 0;
			
			Row rowhead=   sheet.createRow((short)rowCtr);
			int rowHeaderCtr = 0;
			for (Object colHeaderData: rowHeaderList){
				rowhead.createCell((short) rowHeaderCtr).setCellValue(trimString(colHeaderData));
				rowHeaderCtr++;
			}
			
			rowCtr++;
			for (Object[] objectList: rowDataList){
				Row row=   sheet.createRow((short)rowCtr);
				int rowContentCtr = 0;
				for (Object colData: objectList){
					row.createCell((short) rowContentCtr).setCellValue(trimString(colData));
					rowContentCtr++;
				}
				rowCtr++;
			}
			
			

			
			FileOutputStream fileOut =  new FileOutputStream(filename);
			wb.write(fileOut);
			fileOut.close();
			System.out.println("Your xlsx file has been generated!");
			
			//FileInputStream in = new FileInputStream("C:/EF3/EF3_SYSTEMS/hello.xls");
			//Workbook workbook = WorkbookFactory.create(in);
			
			/*
			InputStream excelInput = null; 
			 File file = new File("C:/EF3/EF3_SYSTEMS/hello.xls"); 
		        excelInput = new FileInputStream( file ); 
		        OPCPackage pkg = OPCPackage.open( excelInput ); 
		        
		        
		        if (!Desktop.isDesktopSupported()) {
		            System.err.println("Desktop not supported");
		            // use alternative (Runtime.exec)
		            return;
		        }
		        */

				
		        Desktop desktop = Desktop.getDesktop();
		        if (!desktop.isSupported(Desktop.Action.OPEN)) {
		            System.err.println("EDIT not supported");
		            // use alternative (Runtime.exec)
		            return;
		        }

		        try {
		            //desktop.edit(new File("C:/EF3/EF3_SYSTEMS/hello.xls"));
		            desktop.open(new File(filename));
		        } catch (IOException ex) {
		            ex.printStackTrace();
		        }   
				
		} catch ( Exception ex ) {
		    System.out.println(ex);
		
		}
    }

    public static String trimString(Object s) {
    	// TODO Auto-generated method stub
    	if (s == null) {
    		return "";
    	} else {
    		return (""+s).replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
    	}
    }
}
