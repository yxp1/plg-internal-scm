package com.openport.marketplace.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.UserColumns;
import com.openport.marketplace.json.UserCriteria;
import com.openport.marketplace.json.UserSort;

public class ReportWizardParser {
	static final transient Logger log = Logger.getLogger(ReportWizardParser.class);
	public static List<UserSort> getSorting(String sql) {
		List<UserSort> cols = new LinkedList<>();
		int iIndex = sql.toUpperCase().indexOf("ORDER BY");
		if(iIndex != -1) {
		sql = sql.toUpperCase().substring(iIndex + 8, sql.length()).trim();
		String temp = "";
		Scanner sc = new Scanner(sql);
		sc.useDelimiter(",");
		while (sc.hasNext()) {
			temp = sc.next();
			
			int lastSpace = temp.lastIndexOf(" ");
			UserSort col = new UserSort();

			if (lastSpace > -1) {
				col.setColName(temp.substring(lastSpace + 1, temp.length()));

			} else {
				col.setColName(temp);
			}
			col.setColName( col.getColName().toUpperCase());
			col.setParameterId("COL_PARAM_" + col.getColName().toUpperCase());
			cols.add(col);

		}
		}
		return cols;
	}
	public static List<UserColumns> getDistinctColumn(String sql) {
		List<UserColumns> cols = new LinkedList<>();
		int iFrom = sql.toUpperCase().indexOf("FROM");
		sql = sql.toUpperCase().substring(0, iFrom).replaceAll("SELECT DISTINCT", "").trim();
		String temp = "";
		Scanner sc = new Scanner(sql);
		sc.useDelimiter(",");
		while (sc.hasNext()) {
			temp = sc.next();
			if (temp.startsWith("DATE_ADD")) {

			} else {
				int lastSpace = temp.lastIndexOf(" ");
				UserColumns col = new UserColumns();

				if (lastSpace > -1) {
					col.setColName(temp.substring(lastSpace + 1, temp.length()));

				} else {
					col.setColName(temp);
				}
				col.setColName(col.getColName().toUpperCase());
				cols.add(col);
			}

		}
		return cols;
	}
	public static String trimString(String s) {
		// TODO Auto-generated method stub
		if (s == null) {
			return "";
		} else {
			return s.replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
		}
	}
	
	public static List<UserCriteria> getCriteria(String sql) {
		List<UserSort> cols = new LinkedList<>();
		int iIndex = sql.toUpperCase().indexOf(" WHERE ");
		int lIndex = sql.toUpperCase().indexOf("ORDER BY");
		List<UserCriteria> crList = new LinkedList<>();

		if (iIndex != -1) {
			if (lIndex != -1) {
				sql = sql.toUpperCase().substring(iIndex + 7, lIndex);

			} else {
				sql = sql.toUpperCase().substring(iIndex + 7, sql.length()).trim();

			}

			Scanner sc = new Scanner(sql);
			sc.useDelimiter(" AND ");
			while (sc.hasNext()) {
				UserCriteria uc = new UserCriteria();
				String temp = trimString(sc.next());
				
				log.info("temp is " + temp);
				
				
				
				if (temp.indexOf(" IN ") != -1) {
					Scanner sc2 = new Scanner(temp.replaceAll(" IN ", " "));
					sc2.useDelimiter(" ");
					while (sc2.hasNext()) {

						uc.setColName(sc2.next().replaceAll("'", ""));
						uc.setCriteria(sc2.next().replaceAll("[')(]", "").replaceAll(",",";"));
						uc.setOperand("IN");
					}

				}
				else if (temp.indexOf(" NOT IN ") != -1) {
					Scanner sc2 = new Scanner(temp.replaceAll(" NOT IN ", " "));
					sc2.useDelimiter(" ");
					while (sc2.hasNext()) {

						uc.setColName(sc2.next().replaceAll("'", ""));
						uc.setCriteria(sc2.next().replaceAll("'", ""));
						uc.setOperand("NOT IN");
					}

				} else if (temp.indexOf("length(trim(") != -1) {

					String workString = temp.substring(temp.indexOf("length(trim(") + 12);
					workString = workString.trim();
					if (workString.indexOf(")) > 0") > 0) {
						workString = workString.substring(0, workString.indexOf(")) > 0"));
						uc.setColName(workString.trim());
						uc.setOperand("IS_NOT_BLANK");
					}

					if (workString.indexOf(")) = 0") > 0) {
						workString = workString.substring(0, workString.indexOf(")) = 0"));
						uc.setColName(workString.trim());
						uc.setOperand("IS_BLANK");
					}
					uc.setCriteria("");
				} else if (temp.indexOf("IS NULL") != -1) {
					int iSpace = temp.indexOf("IS NULL");
					uc.setColName(trimString(temp.substring(0, iSpace)).replaceAll("'", ""));
					uc.setCriteria("");
					uc.setOperand("IS NULL");
				} else if (temp.indexOf("IS NOT NULL") != -1) {
					int iSpace = temp.indexOf("IS NOT NULL");
					uc.setColName(trimString(temp.substring(0, iSpace)).replaceAll("'", ""));
					uc.setCriteria("");
					uc.setOperand("IS NOT NULL");
				} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL -1 DAY))"))) {

					// columnName = tempString.substring(0, operandPos);
					uc.setColName(doGetColNameWithDateIntervalValue(temp).replaceAll("'", ""));
					uc.setCriteria("");
					uc.setOperand("Day ago");
				} else if (temp.contains("INTERVAL -") && temp.endsWith("DAY))") && temp.contains(" = ")) {

					
					// columnName = tempString.substring(0, operandPos);
					uc.setColName(doGetColNameWithDateIntervalValue(temp).replaceAll("'", ""));
					temp=temp.substring(temp.indexOf("INTERVAL -")+10);
					uc.setCriteria(temp.substring(0,temp.indexOf(" ")));
					uc.setOperand("Day ago");

					
				} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL 0 DAY))"))) {
					// columnName = tempString.substring(0, operandPos);
					uc.setColName(doGetColNameWithDateIntervalValue(temp).replaceAll("'", ""));
					uc.setCriteria("");
					uc.setOperand("Today");

				}	else if (temp.indexOf(" > ") != -1) {
						int iSpace = temp.indexOf(" > ");
						uc.setColName(trimString(temp.substring(0, iSpace).replaceAll("'", "")));
						uc.setOperand(">");
						String aCriteria = temp.substring(iSpace + 3);
						int nbrOfInterval = aCriteria.replaceAll("INTERVAL", "INTERVAL").length() + 1;

						if (nbrOfInterval > 1) {
							if ((temp.contains("INTERVAL -")) && (temp.endsWith(" DAY))"))) {
								uc.setColName(doGetColNameWithDateIntervalValue(temp));
								uc.setOperand("Number of Days ago");
								String temp2 = temp.substring(iSpace + 3);
								int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
								int hourPos = temp2.lastIndexOf("DAY");
								temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
								uc.setCriteria(doRemoveOtherThanNumber(temp2));
							} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("HOUR)"))) {

								// columnName = tempString.substring(0, operandPos);
								uc.setColName(doGetColNameWithDateIntervalValue(temp.substring(0, iSpace)));
								uc.setOperand("Number of Hours ago");
								String temp2 = temp.substring(iSpace + 3);
								int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
								int hourPos = temp2.lastIndexOf("HOUR");
								temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
								uc.setCriteria(doRemoveOtherThanNumber(temp2));
							} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL -1 DAY))"))) {

								// columnName = tempString.substring(0, operandPos);
								uc.setColName(doGetColNameWithDateIntervalValue(temp));
								uc.setOperand("Day ago");
								uc.setCriteria("1");
							} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL 0 DAY))"))) {
								// columnName = tempString.substring(0, operandPos);
								uc.setColName(doGetColNameWithDateIntervalValue(temp));
								uc.setOperand("Today");
								uc.setCriteria("0");

							}else {
								uc.setColName(doGetColNameWithDateIntervalValue(temp));
								uc.setOperand(">");
								uc.setCriteria(temp.substring(iSpace+3).replaceAll("'", ""));
							}
						} else if (nbrOfInterval > 0) {
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand(">");
							String temp2 = temp.substring(iSpace + 3);
							int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
							int hourPos = temp2.lastIndexOf("DAY");
							temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
							uc.setCriteria(doRemoveOtherThanNumber(temp2));
						} else {
							uc.setColName(temp.substring(0, iSpace));
							uc.setOperand(">");
							uc.setCriteria(temp.substring(iSpace + 3));
						}
				}	else if (temp.indexOf(" < ") != -1) {
					int iSpace = temp.indexOf(" < ");
					uc.setColName(trimString(temp.substring(0, iSpace).replaceAll("'", "")));
					uc.setOperand("<");
					String aCriteria = temp.substring(iSpace + 3);
					int nbrOfInterval = aCriteria.replaceAll("INTERVAL", "INTERVAL").length() + 1;

					if (nbrOfInterval > 1) {
						if ((temp.contains("INTERVAL -")) && (temp.endsWith(" DAY))"))) {
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand("Number of Days ago");
							String temp2 = temp.substring(iSpace + 3);
							int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
							int hourPos = temp2.lastIndexOf("DAY");
							temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
							uc.setCriteria(doRemoveOtherThanNumber(temp2));
						} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("HOUR)"))) {

							// columnName = tempString.substring(0, operandPos);
							uc.setColName(doGetColNameWithDateIntervalValue(temp.substring(0, iSpace)));
							uc.setOperand("Number of Hours ago");
							String temp2 = temp.substring(iSpace + 3);
							int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
							int hourPos = temp2.lastIndexOf("HOUR");
							temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
							uc.setCriteria(doRemoveOtherThanNumber(temp2));
						} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL -1 DAY))"))) {

							// columnName = tempString.substring(0, operandPos);
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand("Day ago");
							uc.setCriteria("1");
						} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL 0 DAY))"))) {
							// columnName = tempString.substring(0, operandPos);
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand("Today");
							uc.setCriteria("0");

						}else {
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand("<");
							uc.setCriteria(temp.substring(iSpace+3).replaceAll("'", ""));
						}
					} else if (nbrOfInterval > 0) {
						uc.setColName(doGetColNameWithDateIntervalValue(temp));
						uc.setOperand("<");
						String temp2 = temp.substring(iSpace + 3);
						int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
						int hourPos = temp2.lastIndexOf("DAY");
						temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
						uc.setCriteria(doRemoveOtherThanNumber(temp2));
					} else {
						uc.setColName(temp.substring(0, iSpace));
						uc.setOperand("<");
						uc.setCriteria(temp.substring(iSpace + 3));
					}
					
					
				} else if (temp.indexOf(" >= ") != -1) {
					int iSpace = temp.indexOf(" >= ");
					uc.setColName(trimString(temp.substring(0, iSpace).replaceAll("'", "")));
					uc.setOperand(">=");
					String aCriteria = temp.substring(iSpace + 4);
					int nbrOfInterval = aCriteria.replaceAll("INTERVAL", "INTERVAL").length() + 1;

					if (nbrOfInterval > 1) {
						if ((temp.contains("INTERVAL -")) && (temp.endsWith(" DAY))"))) {
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand("Number of Days ago");
							String temp2 = temp.substring(iSpace + 4);
							int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
							int hourPos = temp2.lastIndexOf("DAY");
							temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
							uc.setCriteria(doRemoveOtherThanNumber(temp2));
						} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("HOUR)"))) {

							// columnName = tempString.substring(0, operandPos);
							uc.setColName(doGetColNameWithDateIntervalValue(temp.substring(0, iSpace)));
							uc.setOperand("Number of Hours ago");
							String temp2 = temp.substring(iSpace + 4);
							int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
							int hourPos = temp2.lastIndexOf("HOUR");
							temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
							uc.setCriteria(doRemoveOtherThanNumber(temp2));
						} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL -1 DAY))"))) {

							// columnName = tempString.substring(0, operandPos);
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand("Day ago");
							uc.setCriteria("1");
						} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL 0 DAY))"))) {
							// columnName = tempString.substring(0, operandPos);
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand("Today");
							uc.setCriteria("0");

						}else {
							uc.setColName(doGetColNameWithDateIntervalValue(temp));
							uc.setOperand(">=");
							uc.setCriteria(temp.substring(iSpace+4).replaceAll("'", ""));
						}
					} else if (nbrOfInterval > 0) {
						uc.setColName(doGetColNameWithDateIntervalValue(temp));
						uc.setOperand(">=");
						String temp2 = temp.substring(iSpace + 4);
						int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
						int hourPos = temp2.lastIndexOf("DAY");
						temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
						uc.setCriteria(doRemoveOtherThanNumber(temp2));
					} else {
						uc.setColName(temp.substring(0, iSpace));
						uc.setOperand(">=");
						uc.setCriteria(temp.substring(iSpace + 4));
					}
				}else if (temp.indexOf(" <= ") != -1) {
							int iSpace = temp.indexOf(" <= ");
							uc.setColName(trimString(temp.substring(0, iSpace).replaceAll("'", "")));
							uc.setOperand("<=");
							String aCriteria = temp.substring(iSpace + 4);
							int nbrOfInterval = aCriteria.replaceAll("INTERVAL", "INTERVAL").length() + 1;

							if (nbrOfInterval > 1) {
								if ((temp.contains("INTERVAL -")) && (temp.endsWith(" DAY))"))) {
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									// columnName = tempString.substring(0, operandPos);

									uc.setOperand("Number of Days ago");
									String temp2 = temp.substring(iSpace + 4);
									int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
									int hourPos = temp2.lastIndexOf("DAY");
									temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
									uc.setCriteria(doRemoveOtherThanNumber(temp2));
								} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("HOUR)"))) {

									// columnName = tempString.substring(0, operandPos);
									uc.setColName(doGetColNameWithDateIntervalValue(temp.substring(0, iSpace)));
									uc.setOperand("Number of Hours ago");
									String temp2 = temp.substring(iSpace + 4);
									int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
									int hourPos = temp2.lastIndexOf("HOUR");
									temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
									uc.setCriteria(doRemoveOtherThanNumber(temp2));
								} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL -1 DAY))"))) {

									// columnName = tempString.substring(0, operandPos);
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									uc.setOperand("Day ago");
									uc.setCriteria("1");
								} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL 0 DAY))"))) {
									// columnName = tempString.substring(0, operandPos);
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									uc.setOperand("Today");
									uc.setCriteria("0");

								}else {
									log.info("YOu are here " + temp);
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									uc.setOperand("<=");
									uc.setCriteria(temp.substring(iSpace+4, temp.length()).replaceAll("'", ""));
								}
							} else if (nbrOfInterval > 0) {
								uc.setColName(doGetColNameWithDateIntervalValue(temp));
								uc.setOperand("<=");
								String temp2 = temp.substring(iSpace + 4);
								int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
								int hourPos = temp2.lastIndexOf("DAY");
								temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
								uc.setCriteria(doRemoveOtherThanNumber(temp2));
								
								
							} else {
								uc.setColName(temp.substring(0, iSpace));
								uc.setOperand("<=");
								uc.setCriteria(temp.substring(iSpace + 4));
							}	
				}else if (temp.indexOf(" = ") != -1) {
							int iSpace = temp.indexOf(" = ");
							uc.setColName(trimString(temp.substring(0, iSpace).replaceAll("'", "")));
							uc.setOperand("=");
							String aCriteria = temp.substring(iSpace + 4);
							int nbrOfInterval = aCriteria.replaceAll("INTERVAL", "INTERVAL").length() + 1;

							if (nbrOfInterval > 1) {
								if ((temp.contains("INTERVAL -")) && (temp.endsWith(" DAY))"))) {
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									// columnName = tempString.substring(0, operandPos);

									uc.setOperand("Number of Days ago");
									String temp2 = temp.substring(iSpace + 4);
									int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
									int hourPos = temp2.lastIndexOf("DAY");
									temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
									uc.setCriteria(doRemoveOtherThanNumber(temp2));
								} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("HOUR)"))) {

									// columnName = tempString.substring(0, operandPos);
									uc.setColName(doGetColNameWithDateIntervalValue(temp.substring(0, iSpace)));
									uc.setOperand("Number of Hours ago");
									String temp2 = temp.substring(iSpace + 4);
									int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
									int hourPos = temp2.lastIndexOf("HOUR");
									temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
									uc.setCriteria(doRemoveOtherThanNumber(temp2));
								} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL -1 DAY))"))) {

									// columnName = tempString.substring(0, operandPos);
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									uc.setOperand("Day ago");
									uc.setCriteria("1");
								} else if ((temp.contains("INTERVAL -")) && (temp.endsWith("INTERVAL 0 DAY))"))) {
									// columnName = tempString.substring(0, operandPos);
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									uc.setOperand("Today");
									uc.setCriteria("0");
								} else if ((temp.contains("INTERVAL ")) && (temp.endsWith("INTERVAL 0 DAY))"))) {
									// columnName = tempString.substring(0, operandPos);
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									uc.setOperand("Today");
									uc.setCriteria("0");
		
								}else {
									uc.setColName(doGetColNameWithDateIntervalValue(temp));
									uc.setOperand("=");
									uc.setCriteria(temp.substring(iSpace+4, temp.length()).replaceAll("'", ""));
								}
							} else if (nbrOfInterval > 0) {
								uc.setColName(doGetColNameWithDateIntervalValue(temp));
								uc.setOperand("<=");
								String temp2 = temp.substring(iSpace + 4);
								int lastIntervalValPos = temp2.lastIndexOf("INTERVAL");
								int hourPos = temp2.lastIndexOf("DAY");
								temp2 = temp2.trim().substring(lastIntervalValPos + 8, hourPos);
								uc.setCriteria(doRemoveOtherThanNumber(temp2));
								
								
							} else {
								uc.setColName(temp.substring(0, iSpace));
								uc.setOperand("<=");
								uc.setCriteria(temp.substring(iSpace + 4));
							}		
				} else {
					Scanner sc2 = new Scanner(temp);
					sc2.useDelimiter(" ");
					while (sc2.hasNext()) {
						uc.setColName(sc2.next().replaceAll("'", ""));
						uc.setOperand(sc2.next());
						uc.setCriteria(sc2.next().replaceAll("'", ""));
					}
				}
				uc.setColName(trimString(uc.getColName()));
				uc.setCriteria(trimString(uc.getCriteria()));
				log.info(uc.toString());
				crList.add(uc);
			}
			return crList;
		}
		return null;
	}
	public static  String doGetColNameWithDateIntervalValue(String s) {
		String stringTemp = "";
		if ((s.startsWith("DATE_ADD(")) || (s.startsWith(" DATE_ADD("))) {
			int commaPositionTemp = s.trim().indexOf(',');
			stringTemp = s.trim().substring(9, commaPositionTemp);
		} else if ((s.startsWith("DATE(DATE_ADD(")) || (s.startsWith(" DATE(DATE_ADD("))) {
			int commaPositionTemp = s.trim().indexOf(',');
			stringTemp = s.trim().substring(14, commaPositionTemp);
		}
		return stringTemp;
	}
	public static String doRemoveOtherThanNumber(String temp2) {
		temp2 = temp2.trim();
		temp2 = temp2.replaceAll("'", "");
		temp2 = temp2.replaceAll("-", "");
		return temp2;
	}

}
