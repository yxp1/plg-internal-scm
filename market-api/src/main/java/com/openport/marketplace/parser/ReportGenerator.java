package com.openport.marketplace.parser;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.openport.marketplace.json.ReportSchedule;
import com.openport.marketplace.json.ReportWizardData;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.UserCriteria;
import com.openport.marketplace.json.UserSort;
import com.openport.marketplace.repository.DbConfigTMS;
import com.openport.util.CurrentDateFormatter;
import com.openport.util.db.ResultSetMapper;
public class ReportGenerator {
	static final transient Logger log = Logger.getLogger(ReportGenerator.class);
	public static Double timeOffSet=0.0;
	public static String reportSQL="";
	public static List<UserSort> sortOrderBy(List<UserSort> list) {
		List<UserSort> ucList = new LinkedList<>();
		List<SortObject> sortList = new LinkedList<>();
		for(UserSort u : list) {
			
			SortObject sObj = new SortObject();
			sObj.setiInt(u.getPriority().intValue());
			sObj.setObj(u);
			sObj.setType("int");
			sortList.add(sObj);
		}
		Collections.sort(sortList);//apply
		
		if(sortList!=null && sortList.size()>0) {
			for (SortObject i : sortList) {
				ucList.add((UserSort)i.getObj());
			}
		}
		return ucList;
	}	
	
	
public static List<UserCriteria> sortUserCriteria(List<UserCriteria> list) {
	List<UserCriteria> ucList = new LinkedList<>();
	List<SortObject> sortList = new LinkedList<>();
	for(UserCriteria u : list) {
		SortObject sObj = new SortObject();
		sObj.setiInt(u.getPriority().intValue());
		sObj.setObj(u);
		sObj.setType("int");
		sortList.add(sObj);
	}
	Collections.sort(sortList);//apply
	
	if(sortList!=null && sortList.size()>0) {
		for (SortObject i : sortList) {
			ucList.add((UserCriteria)i.getObj());
		}
	}
	return ucList;
}

public static String createColumnScript(List<UserCriteria> ucList) {
	String sql=" SELECT DISTINCT ";
	for(UserCriteria u : ucList) {
		if(u.getDatatype()!=null && !u.getDatatype().equalsIgnoreCase("DATETIME")) {
			sql+=u.getValue() + ",";
		}else if(trimString(u.getDatatype()).equalsIgnoreCase("DATETIME")) { 
			sql+="DATE_ADD("+u.getValue()+", INTERVAL "+timeOffSet+" HOUR) as "+u.getValue() +",";
		}
	}
	return sql.substring(0, sql.lastIndexOf(","));

}

public static String createWhereClauseScript(List<UserCriteria> ucList) {
	
	String sql="";
	String operand="";
	String criteria="";
	String col="";
	for(UserCriteria u : ucList) {
		operand=u.getOperand();
		criteria=u.getCriteria();
		col=u.getValue();
		log.info(" U :" + u.toString());
		if(!u.getOperand().equalsIgnoreCase("NONE")) {
			if(trimString(u.getDatatype()).equalsIgnoreCase("DATETIME")) {
				  
				if (u.getOperand().equalsIgnoreCase("Number of Days ago")) {
					col=" DATE(DATE_ADD("+u.getValue()+", INTERVAL "+timeOffSet+" HOUR))";
					criteria="DATE(DATE_ADD(DATE_ADD(now(), INTERVAL "+timeOffSet+" HOUR), INTERVAL -"+u.getCriteria()+" DAY))";
					operand=">=";
				}else if (u.getOperand().equalsIgnoreCase("Number of Hours ago")) {
					col="DATE_ADD("+u.getValue()+", INTERVAL "+timeOffSet+" HOUR)";
					criteria="DATE(DATE_ADD(DATE_ADD(now(), INTERVAL "+timeOffSet+" HOUR), INTERVAL -"+u.getCriteria()+" DAY))";
					operand=">=";
				}else if (u.getOperand().equalsIgnoreCase("Day ago")) {
					col=" DATE(DATE_ADD("+u.getValue()+", INTERVAL "+timeOffSet+" HOUR))";
					criteria="DATE(DATE_ADD(DATE_ADD(now(), INTERVAL "+timeOffSet+" HOUR), INTERVAL -"+u.getCriteria()+" DAY))";
					operand="=";
				}else if (u.getOperand().equalsIgnoreCase("Today")) {
					col=" DATE(DATE_ADD("+u.getValue()+", INTERVAL "+timeOffSet+" HOUR))";
					criteria="DATE(DATE_ADD(DATE_ADD(now(), INTERVAL "+timeOffSet+" HOUR), INTERVAL 0 DAY))";
					operand="=";
				}else {
					criteria="'"+u.getCriteria() + "'";
					if ((trimString(u.getCriteria()).length()>13)){
						col=" DATE_ADD("+u.getValue()+", INTERVAL "+timeOffSet+" HOUR)";
						
					}else{
						col=" DATE(DATE_ADD("+u.getValue()+", INTERVAL "+timeOffSet+" HOUR))";
						//criteriaValue = " '"+criteriaValue+"'";
					}
				}
				
				
				
			}else if(trimString(u.getDatatype()).equalsIgnoreCase("NBR")) {
				criteria=u.getCriteria();
			} else {
				 if(u.getOperand().equalsIgnoreCase("LIKE")) {
					 criteria="'%"+u.getCriteria()+"%'";
				 } else if(u.getOperand().equalsIgnoreCase("IN") || u.getOperand().equalsIgnoreCase("NOT IN") ) {
					 log.info("goes here...");
					 criteria="('"+u.getCriteria().replaceAll(";","','")+"')";	 
				 } else if(u.getOperand().equalsIgnoreCase("IS NULL") ||u.getOperand().equalsIgnoreCase("IS NOT NULL")) {
					 criteria="";
				 } else if (u.getOperand().equalsIgnoreCase("IS_BLANK")) {
					 operand="";
					 criteria="";
					 col="length(trim("+u.getValue()+")) = 0";
				}else if (u.getOperand().equalsIgnoreCase("IS_NOT_BLANK")) {
					 operand="";
					 criteria="";
					 col="length(trim("+u.getValue()+")) > 0";
				
				 }else {
					 criteria="'"+u.getCriteria() + "'";
				 }
			}
			
			sql+= col + " "+operand + " " + criteria + " AND ";
			
			
		}
	}
	if(!sql.isEmpty()) {
	return sql.substring(0, sql.lastIndexOf(" AND "));
	}
	return null;
}

public static String createOrderByScript(List<UserSort> usList) {
	String sql="";
	for(UserSort u : usList) {
			sql+=u.getParameterId().replaceAll("COL_PARAM_","") + ",";
		
	}
	if(!sql.isEmpty()) {
	return sql.substring(0, sql.lastIndexOf(","));
	}
	return null;
}

private static List<Object[]> executeSQL(String sql,Integer headerSize) {
	List<Object[]> objList = new LinkedList<>();
	reportSQL=sql.toUpperCase(); //will be the json output
	try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql.toUpperCase());) {

		try (ResultSet rs = pstmt.executeQuery()) {
			
			while (rs.next()) {
				Object[] o = new Object[headerSize];
				for(int i=0;i<headerSize;i++) {
					o[i]=rs.getObject(i+1);
				}
				objList.add(o);
			}
		} catch (Exception ee) {
			new ServiceException(Response.Status.CONFLICT);
		}

	} catch (Exception e) {
		log.info("error", e);
		log.error("Error message: " + e, e);
		new ServiceException(Response.Status.CONFLICT);
	}

	return objList;

}
public static void createSQLScript(ReportWizardData data) {
	
	List<UserCriteria> ucList = sortUserCriteria(data.getCriteriaList());
	List<UserSort> sortList = sortOrderBy(data.getSortList());
	String sql="";
	String columnScript=createColumnScript(ucList);
	sql=sql+columnScript;
	sql=sql+ " FROM  " + data.getDataSource();
	
	String whereScript=createWhereClauseScript(ucList);
	if(whereScript!=null ) {
		sql+=" WHERE "+whereScript;
	}
	
	// order 
	String orderScript=createOrderByScript(sortList);
	if(orderScript !=null){
		sql+=" ORDER BY "+orderScript;
	}
log.info("final sql "+sql);	
	List<Object[]> rows =executeSQL(sql.toUpperCase(), ucList.size());
	if(data.getReportFormat().equalsIgnoreCase("CSV")) {
	createCSVReport(rows, ucList,data.getReportLocalPath()+data.getFilename());
	}else 	if(data.getReportFormat().equalsIgnoreCase("XLS")) {
		createExcelReport(rows, ucList,data.getReportLocalPath()+data.getFilename(),"Report");
		
	}else 	if(data.getReportFormat().equalsIgnoreCase("XLSX")) {
		createExcelReportXLSX(rows, ucList,data.getReportLocalPath()+data.getFilename(),"Report");
		
	}
	if(data.getIsNewReport()!=null && data.getIsNewReport()) {
		saveReport(sql, data.getUserName(),data.getReportFormat(), data.getReportName(), data.getDataSource());
	}else if(data.getIsUpdateReport()!=null && data.getIsUpdateReport()) {
		updateReport(sql, data.getUserName(),data.getReportFormat(), data.getReportName(), data.getDataSource());
	}
	if(data.getReportSchedule()!=null) {
		if(getReportSchedule(data.getReportName(), data.getUserName())==null) {
			saveSchedule(data.getReportSchedule(), sql);
		}else {
		updateSchedule(data.getReportSchedule(), sql);
		}
	}
	
}
public static ReportSchedule getReportSchedule(String reportName,String userId) {
	
	try {
		String sql = " select * from report_schedule \n"
				+ " where report_name=? and report_owner_user_id=?";
		try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, reportName);
			pstmt.setString(2, userId);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<ReportSchedule> driverRawMapper = new ResultSetMapper<ReportSchedule>();
				List<ReportSchedule> pojoList = driverRawMapper.mapResultSetToObject(rs, ReportSchedule.class);
				if (pojoList != null && !pojoList.isEmpty()) {
					for (ReportSchedule rsched : pojoList) {
						rsched.setStartTm(new CurrentDateFormatter().formatGivenDate(rsched.getStartSchedTm(), "h:m a", new Locale("EN_US")));
						return rsched;
					}
				}
			}

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	} finally {
		log.debug("Done");
	}
	return null;

}


public static void updateReport(String reportSql,String userName,String formatType,String reportName,String sourceName) {
	String sql = "update report_wizard_sql set report_sql_txt=?,update_tstamp=now(),report_output_format=? where report_name=? and report_db_source_name=? and user_id=?"; 
	try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

	{
		pstmt.setString(1, reportSql);
		pstmt.setString(2,formatType);
		pstmt.setString(3, reportName);
		pstmt.setString(4,sourceName);
		pstmt.setString(5,userName);
		
		int id = pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs != null && rs.next()) {
		}
	} catch (Exception e) {
		log.error("Error message: " + e, e);
	}
}

public static void saveSchedule(ReportSchedule schedule,String reportSql) {
	String sql = "insert into report_schedule (" +
			     "report_name,"  //1
			     + "report_owner_user_id," //2
			     + "report_sql_txt," //3
			     + "delivery_format_cd," 
			     + "recipient_addr," //4
			     + "status_cd," //
			     + "start_dt," //5
			     + "start_tm," //6
			     + "last_run_dt," //
			     + "next_run_dt," //
			     + "last_run_status_cd," //
			     + "last_run_status_txt," //
			     + "run_weekly_flg," //7
			     + "run_mon_flg," //8
			     + "run_tue_flg," //9
			     + "run_wed_flg," //10
			     + "run_thu_flg," //11
			     + "run_fri_flg," //12
			     + "run_sat_flg," //13
			     + "run_sun_flg," //14
			     + "run_monthly_flg,"  //15
			     + "date_of_month_value," //16
			     + "monthly_freq_value," //17
			     + "monthly_day_of_week_value," //18
			     + "create_tstamp," //
			     + "update_tstamp," //
			     + "report_output_format," //19
			     + "time_zone_country," //
			     + "time_zone_offset)"+ //
			     " values ("+ 
			     "?,"+ //1 
			     "?,"+  //2
			     "?,"+ //3
			     "'EML',"+ 
			     "?,"+ //4
			     "'A',"+ //
			     "?,"+ //5
			     "?,"+ //6
			     "now(),"+ //
			     "null,"+ //
			     "'C',"+ //
			     "'OK',"+ //
			     "?,"+ //7
			     "?,"+ //8
			     "?,"+ //9
			     "?,"+ //10
			     "?,"+ //11
			     "?,"+ //12
			     "?,"+ //13
			     "?,"+ //14
			     "?,"+ //15
			     "?,"+ //16
			     "?,"+ //17
			     "?,"+ //18
			     "now(),"+ //
			     "now(),"+ //
			     "?,"+ //19
			     "'Server''s Local Time',"+ 
			     "0)"; 
			     log.info("run MOn flg " + schedule.getRunMonFlg());
			     log.info("run Tue flg " + schedule.getRunTueFlg());
		 			     
	try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
	
	
	


	{
		
		Date startTM = formatRealTimeYYYMMddhhmmA(schedule.getStartDt(),schedule.getStartTm()	);
		schedule.setStartDt(startTM);
		pstmt.setString(1, schedule.getReportName());
		pstmt.setString(2, schedule.getReportOwnerUserId());
		pstmt.setString(3, reportSql);
		pstmt.setString(4, schedule.getRecipientAddr());
		pstmt.setTimestamp(5, new Timestamp(schedule.getStartDt().getTime()));
		pstmt.setTimestamp(6, new Timestamp(startTM.getTime()));
		pstmt.setString(7, ""+schedule.getRunWeeklyFlg());
		pstmt.setString(8, schedule.getRunMonFlg());
		pstmt.setString(9, schedule.getRunTueFlg());
		pstmt.setString(10,schedule.getRunWedFlg());
		pstmt.setString(11,schedule.getRunThuFlg());
		pstmt.setString(12,schedule.getRunFriFlg());
		pstmt.setString(13,schedule.getRunSatFlg());
		pstmt.setString(14,schedule.getRunSunFlg());
		pstmt.setString(15,""+schedule.getRunMonthlyFlg());
		pstmt.setInt(16, schedule.getDateOfMonthValue());
		pstmt.setString(17, schedule.getMonthlyFreqValue());
		pstmt.setString(18, schedule.getMonthlyDayOfWeekValue());
		pstmt.setString(19, schedule.getReportOutputFormat());
			
		int id = pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs != null && rs.next()) {
		}
	} catch (Exception e) {
		log.error("Error message: " + e, e);
	}
}


public static void updateSchedule(ReportSchedule schedule,String reportSql) {
	String sql = "update report_schedule set " 
			     + "report_sql_txt=?," 
			     + "recipient_addr=?," //4
			     + "start_dt=?," //5
			     + "start_tm=?," //6
			     + "run_weekly_flg=?," //7
			     + "run_mon_flg=?," //8
			     + "run_tue_flg=?," //9
			     + "run_wed_flg=?," //10
			     + "run_thu_flg=?," //11
			     + "run_fri_flg=?," //12
			     + "run_sat_flg=?," //13
			     + "run_sun_flg=?," //14
			     + "run_monthly_flg=?,"  //15
			     + "date_of_month_value=?," //16
			     + "monthly_freq_value=?," //17
			     + "monthly_day_of_week_value=?," //18
			     + "update_tstamp=now()," //
			     + "report_output_format=?\n" //19
			     + " where report_name=? and report_owner_user_id=?"; 
			     		     
	try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
	
	
	


	{
		
		Date startTM = formatRealTimeYYYMMddhhmmA(schedule.getStartDt(),schedule.getStartTm()	);
		schedule.setStartDt(startTM);
		pstmt.setString(1, reportSql);
		pstmt.setString(2, schedule.getRecipientAddr());
		pstmt.setTimestamp(3, new Timestamp(schedule.getStartDt().getTime()));
		pstmt.setTimestamp(4, new Timestamp(startTM.getTime()));
		pstmt.setString(5, ""+schedule.getRunWeeklyFlg());
		pstmt.setString(6, schedule.getRunMonFlg());
		pstmt.setString(7, schedule.getRunTueFlg());
		pstmt.setString(8,schedule.getRunWedFlg());
		pstmt.setString(9,schedule.getRunThuFlg());
		pstmt.setString(10,schedule.getRunFriFlg());
		pstmt.setString(11,schedule.getRunSatFlg());
		pstmt.setString(12,schedule.getRunSunFlg());
		pstmt.setString(13,""+schedule.getRunMonthlyFlg());
		pstmt.setInt(14, schedule.getDateOfMonthValue());
		pstmt.setString(15, schedule.getMonthlyFreqValue());
		pstmt.setString(16, schedule.getMonthlyDayOfWeekValue());
		pstmt.setString(17, schedule.getReportOutputFormat());
		pstmt.setString(18, schedule.getReportName());
		pstmt.setString(19, schedule.getReportOwnerUserId());
			
		int id = pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs != null && rs.next()) {
		}
	} catch (Exception e) {
		log.error("Error message: " + e, e);
	}
}


public static void saveReport(String reportSql,String userName,String formatType,String reportName,String sourceName) {
	String sql = "insert into report_wizard_sql (" +
			     "user_id,report_name,report_sql_txt,create_tstamp,update_tstamp,report_db_source_name,time_zone_country,time_zone_offset,report_output_format)"+
			     " values ("+ 
			     "?,"+ 
			     "?,"+ 
			     "?,"+ 
			     "now(),"+ 
			     "now(),"+ 
			     "?,"+ 
			     "?,"+ 
			     "?,"+ 
			     "?)"; 
			     
			 	
			     

log.info("save report " +sql);
	try (java.sql.Connection connection = DbConfigTMS.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)

	{
		pstmt.setString(1, userName);
		pstmt.setString(2, reportName);
		pstmt.setString(3, reportSql);
		pstmt.setString(4, sourceName);
		pstmt.setString(5, "Server's local Time");
		pstmt.setDouble(6, 0.0);
		pstmt.setString(7, formatType);
		
		int id = pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs != null && rs.next()) {
		}
	} catch (Exception e) {
		log.error("Error message: " + e, e);
	}
}
public static void createCSVReport(List<Object[]> rows,List<UserCriteria> columns,String filename) {
	StringBuffer buf = new StringBuffer();
	String cHeader = "";
	String tempStr="";
	for(UserCriteria u : columns) {
		cHeader+=u.getValue() + ",";
	}
	
	buf.append(cHeader.substring(0,cHeader.lastIndexOf(",")) + "\n");
	//rows
	for(Object[] r : rows) {
		tempStr="";
		for(int x =0;x<columns.size();x++) {
			tempStr+=(""+trimObject(r[x])).replaceAll(",","") +",";
		}
		buf.append(tempStr.substring(0,tempStr.lastIndexOf(",")) + "\n");
	}
	log.info(buf.toString());
	createFileUtf8(filename, buf);
}

public static void createFileUtf8(String filename,StringBuffer buf) {
	try {
	File f = new File(filename); 
	FileUtils.writeStringToFile(f, trimString(buf.toString()), "UTF-8");
	}catch(Exception ee){
		
	}
}
public static String trimString(String s) {
	// TODO Auto-generated method stub
	if (s == null) {
		return "";
	} else {
		return s.replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
	}
}
public static String trimObject(Object s) {
	// TODO Auto-generated method stub
	if (s == null) {
		return "";
	} else {
		return (""+s).replaceAll("^\\s+", "").replaceAll("\\s+$", "").trim();
	}
}
public static void createExcelReport(List<Object[]> rows,List<UserCriteria> columns,String filename,String sheetName){
	log.info("filename"+filename);
	try{
		HSSFWorkbook hwb=new HSSFWorkbook();
		HSSFSheet sheet =  hwb.createSheet(sheetName);
		
		int rowCnt=0;
		int conCtr=0;
		HSSFRow rowhead=   sheet.createRow((short)rowCnt);
		
		for(UserCriteria uc : columns) {
			log.info("Column "+uc.getValue());
			rowhead.createCell((short)conCtr++).setCellValue(uc.getValue());
		
		}
		conCtr=0;
		rowCnt++;
		for(Object[] o : rows) {
	    	HSSFRow row=   sheet.createRow((short)rowCnt);
			
			while(conCtr < columns.size()) {
					row.createCell((short)conCtr).setCellValue(trimObject(o[conCtr]));
					conCtr++;
			    }	
			    	conCtr=0;
			    	rowCnt++;
			    
		}
		
		

		
		FileOutputStream fileOut =  new FileOutputStream(filename);
		hwb.write(fileOut);
		fileOut.close();
				
	} catch ( Exception ex ) {
	log.error("Error",ex);
	}
}

public static void createExcelReportXLSX(List<Object[]> rows,List<UserCriteria> columns,String filename,String sheetName){
	try{
		Workbook wb = new XSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		Sheet sheet = wb.createSheet(sheetName);
		
		
		int rowCnt=0;
		int conCtr=0;
		Row rowhead=   sheet.createRow((short)rowCnt);
		
		for(UserCriteria uc : columns) {
			rowhead.createCell((short)conCtr++).setCellValue(uc.getValue());
		
		}
		conCtr=0;
		rowCnt++;
		for(Object[] o : rows) {
	    	Row row=   sheet.createRow((short)rowCnt);
			
			while(conCtr < columns.size()) {
					row.createCell((short)conCtr).setCellValue(trimObject(o[conCtr]));
					conCtr++;
			    }	
			    	conCtr=0;
			    	rowCnt++;
			    
		}
		
		

		
		FileOutputStream fileOut =  new FileOutputStream(filename);
		wb.write(fileOut);
		fileOut.close();
				
	} catch ( Exception ex ) {
		log.error("Error",ex);
	}
}
private static Date formatRealTimeYYYMMddhhmmA(Date dt, String hhmmssValue) {
	Date retDt = new Date();
	String dt2 = new CurrentDateFormatter().formatGivenDate(dt, "yyyy-MM-dd", new Locale("en", "US"));

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd h:m a");
	try {
		retDt = df.parse(dt2 + " " + hhmmssValue);
	} catch (ParseException ex) {
	}
	return retDt;
}

}
