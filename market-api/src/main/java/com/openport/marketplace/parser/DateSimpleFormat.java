package com.openport.marketplace.parser;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateSimpleFormat {
	public static String formatSimpleDate(Date d,String format) {
		return new SimpleDateFormat(format).format(d);
	}
}