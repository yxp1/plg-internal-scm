package com.openport.marketplace.parser;

import java.util.Date;

public class SortObject implements Comparable<SortObject> {
private int iInt;
private Date dDt;
private Object obj;
private String type;
private String sStr;
private Double dDouble;
public int compareTo(SortObject o) {
	// TODO Auto-generated method stub
	int v=0;
	if(type.equalsIgnoreCase("int")) {
		int c1 = this.iInt;
		int c2 = o.iInt;
		if(c1 > c2)  {
			v=1;
		}else if(c1 < c2) {
			v=-1;
		}else {
			v=0;
		}
	}else	if(type.equalsIgnoreCase("double")) {
			double c1 = this.dDouble;
			double c2 = o.dDouble;
			if(c1 > c2)  {
				v=1;
			}else if(c1 < c2) {
				v=-1;
			}else {
				v=0;
			}
			
	}else 
	if(type.equalsIgnoreCase("string")) {
		v=this.sStr.compareTo(o.sStr);
	}else
	if(type.equalsIgnoreCase("date")) {
		Date d1=this.dDt;
		Date d2=o.dDt;
		long n1 = d1.getTime();
	    long n2 = d2.getTime();
	       if (n1 < n2) {
	        	v=-1;
	        }else if(n1>n2) {
	        	v=1;
	        }else {
	        	v=0;
	        }
	}
	
	return v;
}

public int getiInt() {
	return iInt;
}
public void setiInt(int iInt) {
	this.iInt = iInt;
}
public Object getObj() {
	return obj;
}
public void setObj(Object obj) {
	this.obj = obj;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getsStr() {
	return sStr;
}
public void setsStr(String sStr) {
	this.sStr = sStr;
}

public Date getdDt() {
	return dDt;
}

public void setdDt(Date dDt) {
	this.dDt = dDt;
}

public Double getdDouble() {
	return dDouble;
}

public void setdDouble(Double dDouble) {
	this.dDouble = dDouble;
}



}