/**
 * 
 */
package com.openport.marketplace.json;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;





/**
 * @author oscar_2
 *
 */
@XmlRootElement
public class UserStatus {

	private int status;
	private String message;
	private String errorCode;
	private Token token;
	private User user;
	private String fileId;
	private String announcement;
	private String refreshSeconds= "300"; // 5
	private String success;
	private LanguagePack updateLanguage;
	private List<Language> languages;
	private List<Label> labels;
	private Integer langVer;
	private String companyName;
	private String companyCode;
	
	/**
	 * 
	 */
	public UserStatus() {
		// TODO Auto-generated constructor stub
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

	public String getRefreshSeconds() {
		return refreshSeconds;
	}

	public void setRefreshSeconds(String refreshSeconds) {
		this.refreshSeconds = refreshSeconds;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public LanguagePack getUpdateLanguage() {
		return updateLanguage;
	}

	public void setUpdateLanguage(LanguagePack updateLanguage) {
		this.updateLanguage = updateLanguage;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<Label> getLabels() {
		return labels;
	}

	public void setLabels(List<Label> labels) {
		this.labels = labels;
	}

	
	
	public Integer getLangVer() {
		return langVer;
	}

	public void setLangVer(Integer langVer) {
		this.langVer = langVer;
	}

	public void addLabel(Label lbl) {
		if (this.labels ==null) {
			labels = new ArrayList<Label>();
		}
		labels.add(lbl);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	

}
