/**
 * 
 */
package com.openport.marketplace.json;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class ShipmentInfo {

	// ###### SHIPMENT

	@Column(name="shipment_id")
	private Integer shipmentId;
	@Column(name="shipment_nbr")
	private String shipmentNbr;
	@Column(name="shipper_id")
	private Integer shipperId;
	@Column(name="status_id")
	private Integer statusId;
	@Column(name="assigned_provider_id")
	private Integer assignedProviderId;
	@Column(name="pickup_address_id")
	private Integer pickupAddressId;
	@Column(name="delivery_address_id")
	private Integer deliveryAddressId;
	@Column(name="requested_equipment_type_id")
	private Integer requestedEquipmentTypeId;
	@Column(name="requested_pickup_dt")
	private Timestamp requestedPickupDt;
	@Column(name="requested_delivery_dt")
	private Timestamp requestedDeliveryDt;
	@Column(name="unit_count")
	private Double unitCount;
	@Column(name="unit_of_measure")
	private String unitOfMeasure;
	@Column(name="create_User_Id")
	private String createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_User_Id")
	private String updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;
	@Column(name="tender_expiration_dt")
	private Timestamp tenderExpirationDt;
	@Column(name="origin_service_area_id")
	private Integer originServiceAreaId;
	@Column(name="destination_service_area_id")
	private Integer destinationServiceAreaId;
	@Column(name="unit_wt")
	private Double unitWt;
	@Column(name="unit_vol")
	private Double unitVol;
	@Column(name="payment_term_id")
	private Integer paymentTermId;
	@Column(name="low_end_bid_amt")
	private Double lowEndBidAmt;
	@Column(name="high_end_bid_amt")
	private Double highEndBidAmt;
	@Column(name="avg_bid_amt")
	private Double avgBidAmt;
	@Column(name="target_rate_amt")
	private Double targetRateAmt;
	@Column(name="currency_cd")
	private String currencyCd;
	@Column(name="tender_type_cd")
	private String tenderTypeCd;
	@Column(name="origin_wait_tm")
	private Double originWaitTm;
	@Column(name="dest_wait_tm")
	private Double destWaitTm;
	@Column(name="origin_wait_tm_uom")
	private String originWaitTmUom;
	@Column(name="dest_wait_tm_uom")
	private String destWaitTmUom;
	@Column(name="commodity_desc_txt")
	private String commodityDescTxt;
	@Column(name="pro_bill_nbr")
	private String proBillNbr;
	@Column(name="parent_partner_cd")
	private String vendorId;
	@Column(name="Shipping_Instruction_Txt")
	private String si;
	@Column(name="Shipping_Type_Cd")
	private String shptType;
	@Column(name="stops_cnt")
	private Integer stopCnt;

	// ###### ENTITY ADDRESS PICKUP

	@Column(name="pickup_entity_Address_Id")
	private int pickupEntityAddressId;
	@Column(name="pickup_entity_Name")
	private String pickupEntityName;
	@Column(name="pickup_floor_Apartment")
	private String pickupFloorApartment;
	@Column(name="pickup_building")
	private String pickupBuilding;
	@Column(name="pickup_house_Number")
	private String pickupHouseNumber;
	@Column(name="pickup_line1")
	private String pickupLine1;
	@Column(name="pickup_street_Name")
	private String pickupStreetName;
	@Column(name="pickup_city_Locality")
	private String pickupCityLocality;
	@Column(name="pickup_province")
	private String pickupProvince;
	@Column(name="pickup_country")
	private String pickupCountry;
	@Column(name="pickup_postal_Code")
	private String pickupPostalCode;
	@Column(name="pickup_district_Name")
	private String pickupDistrictName;
	@Column(name="pickup_latitude")
	private BigDecimal pickupLat;
	@Column(name="pickup_longitude")
	private BigDecimal pickupLon;
	@Column(name="pickup_lat_Long_Status_Cd")
	private String pickupLatLongStatusCd;
	@Column(name="pickup_is_Dc")
	private boolean pickupIsDc;

	// ###### ENTITY ADDRESS DELIVERY

	@Column(name="delivery_entity_Address_Id")
	private Integer deliveryEntityAddressId;
	@Column(name="delivery_entity_Name")
	private String deliveryEntityName;
	@Column(name="delivery_floor_Apartment")
	private String deliveryFloorApartment;
	@Column(name="delivery_building")
	private String deliveryBuilding;
	@Column(name="delivery_house_Number")
	private String deliveryHouseNumber;
	@Column(name="delivery_line1")
	private String deliveryLine1;
	@Column(name="delivery_street_Name")
	private String deliveryStreetName;
	@Column(name="delivery_city_Locality")
	private String deliveryCityLocality;
	@Column(name="delivery_province")
	private String deliveryProvince;
	@Column(name="delivery_country")
	private String deliveryCountry;
	@Column(name="delivery_postal_Code")
	private String deliveryPostalCode;
	@Column(name="delivery_district_Name")
	private String deliveryDistrictName;
	@Column(name="delivery_latitude")
	private BigDecimal deliveryLat;
	@Column(name="delivery_longitude")
	private BigDecimal deliveryLon;
	@Column(name="delivery_lat_Long_Status_Cd")
	private String deliveryLatLongStatusCd;
	@Column(name="delivery_is_Dc")
	private boolean deliveryIsDc;

	// ###### EQUIPMENT TYPE

	@Column(name="equipment_type_id")
	private Integer equipmentTypeId;
	@Column(name="equipment_type_name")
	private String equipmentTypeName;


	// ###### SHIPMENT ACCESSORIAL

	@Column(name="ACCESSORIAL_CD")
	private String accessorialCd;
	@Column(name="ACCESSORIAL_DATA_SOURCE")
	private String accessorialDataSource;
	@Column(name="ACCESSORIAL_DESCRIPTION")
	private String accessorialDescription;
	@Column(name="ACCESSORIAL_TYPE")
	private String accessorialType;
	@Column(name="ACCESSORIAL_AMT")
	private Double accessorialAmt;

	// ###### PAYMENT TERM
	@Column(name="payment_term_cd")
	private String paymentTermCd;
	@Column(name="Payment_Term_Name")
	private String paymentTermName;

	// ###### SHIPMENT DETAIL
	@Column(name="shipment_detail_id")
	private Integer shipmentDetailId;
	@Column(name="qty")
	private Double qty;
	@Column(name="unit_of_measure_dtl")
	private String unitOfMeasureDtl;
	@Column(name="shipment_wt")
	private Double shipmentWt;
	@Column(name="wt_uom")
	private String wtUom;
	@Column(name="shipment_vol")
	private Double shipmentVol;
	@Column(name="vol_uom")
	private String volUom;
	@Column(name="shipment_len")
	private Double shipmentLen;
	@Column(name="shipment_wth")
	private Double shipmentWth;
	@Column(name="shipment_ht")
	private Double shipmentHt;
	@Column(name="dimension_uom")
	private String dimensionUom;
	@Column(name="freight_class_cd")
	private String freightClassCd;
	@Column(name="is_hazardous")
	private Boolean isHazardous;
	@Column(name="product_desc_txt")
	private String productDescTxt;
	@Column(name="stop_nbr")
	private Integer stopNbr;
	@Column(name="stop_location_name")
	private String stopLocName; 
	@Column(name="stop_location_addr")
	private String stopAddr;
	@Column(name="stop_location_city_name")
	private String stopCity;
	@Column(name="stop_location_province_name")
	private String stopProvince;
	@Column(name="stop_location_country_cd")
	private String stopCountry;
	@Column(name="stop_location_postal_cd")
	private String stopPostal;
	@Column(name="stop_po_nbr")
	private String stopPonbr;
	@Column(name="stop_release_nbr")
	private String stopRlsnbr;
	

	// ###### SHIPMENT CARRIER BID
	@Column(name="bid_rank")
	private Integer bidRank ;
	@Column(name="shipment_Carrier_Bid_Id")
	private Integer shipmentCarrierBidId;
	@Column(name="carrier_Id")
	private Integer carrierId;
	@Column(name="quote_Status_Cd")
	private String quoteStatusCd;
	@Column(name="rate_Type_Cd")
	private String rateTypeCd;
	@Column(name="rate_Type_Desc_Txt")
	private String rateTypeDescTxt;
	@Column(name="transit_Tm")
	private Double transitTm;
	@Column(name="quote_amt")
	private Double quoteAmt;
	@Column(name="quote_Currency_Cd")
	private String quoteCurrencyCd;
	@Column(name="quote_Datestamp")
	private Date quoteDatestamp;
	@Column(name="quote_Expiration_Value")
	private Integer quoteExpirationValue;
	@Column(name="quote_Expiration_Uom")
	private String quoteExpirationUom;
	@Column(name="comment_Txt")
	private String commentTxt;
	@Column(name="carrier_Cd")
	private String carrierCd;
	@Column(name="carrier_name")
	private String carrierName;
	@Column(name="carrier_rating")
	private String carrierRating;
	@Column(name="bid_ended")
	private String bidEnded;
	
	
	// ###### SHIPMENT CARRIER BID DETAIL
	@Column(name="shipper_defined_flg_bid_detail")
	private String  shipperDefinedFlgBidDetail;
	@Column(name="accessorial_amt_bid_detail")
	private Double accessorialAmtBidDetail;
	@Column(name="accessorial_ccy_bid_detail")
	private String accessorialCcyBidDetail;
	@Column(name="accessorial_cd_bid_detail")
	private String accessorialCdBidDetail;
	@Column(name="accessorial_data_source_bid_detail")
	private String accessorialDataSourceBidDetail;
	@Column(name="accessorial_description_bid_detail")
	private String accessorialDescriptionBidDetail;
	@Column(name="accessorial_rate_comment_bid_detail")
	private String accessorialRateCommentBidDetail;
	@Column(name="accessorial_type_bid_detail")
	private String accessorialTypeBidDetail;
	@Column(name="carrier_id_bid_detail")
	private Integer carrierIdBidDetail;
	@Column(name="create_dt_bid_detail")
	private Date createDtBidDetail;
	@Column(name="update_dt_bid_detail")
	private Date updateDtBidDetail;
	
	// ###### SHIPMENT CARRIER BID DETAIL
	@Column(name="service_name")
	private String  serviceName;
	@Column(name="service_id")
	private Integer serviceId;
	
	public Integer getShipmentId() {
		return shipmentId;
	}
	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}
	public String getShipmentNbr() {
		return shipmentNbr;
	}
	public void setShipmentNbr(String shipmentNbr) {
		this.shipmentNbr = shipmentNbr;
	}
	public Integer getShipperId() {
		return shipperId;
	}
	public void setShipperId(Integer shipperId) {
		this.shipperId = shipperId;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Integer getAssignedProviderId() {
		return assignedProviderId;
	}
	public void setAssignedProviderId(Integer assignedProviderId) {
		this.assignedProviderId = assignedProviderId;
	}
	public Integer getPickupAddressId() {
		return pickupAddressId;
	}
	public void setPickupAddressId(Integer pickupAddressId) {
		this.pickupAddressId = pickupAddressId;
	}
	public Integer getDeliveryAddressId() {
		return deliveryAddressId;
	}
	public void setDeliveryAddressId(Integer deliveryAddressId) {
		this.deliveryAddressId = deliveryAddressId;
	}
	public Integer getRequestedEquipmentTypeId() {
		return requestedEquipmentTypeId;
	}
	public void setRequestedEquipmentTypeId(Integer requestedEquipmentTypeId) {
		this.requestedEquipmentTypeId = requestedEquipmentTypeId;
	}
	public Timestamp getRequestedPickupDt() {
		return requestedPickupDt;
	}
	public void setRequestedPickupDt(Timestamp requestedPickupDt) {
		this.requestedPickupDt = requestedPickupDt;
	}
	public Timestamp getRequestedDeliveryDt() {
		return requestedDeliveryDt;
	}
	public void setRequestedDeliveryDt(Timestamp requestedDeliveryDt) {
		this.requestedDeliveryDt = requestedDeliveryDt;
	}
	public Double getUnitCount() {
		return unitCount;
	}
	public void setUnitCount(Double unitCount) {
		this.unitCount = unitCount;
	}
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public Timestamp getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Timestamp getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}
	public Timestamp getTenderExpirationDt() {
		return tenderExpirationDt;
	}
	public void setTenderExpirationDt(Timestamp tenderExpirationDt) {
		this.tenderExpirationDt = tenderExpirationDt;
	}
	public Integer getOriginServiceAreaId() {
		return originServiceAreaId;
	}
	public void setOriginServiceAreaId(Integer originServiceAreaId) {
		this.originServiceAreaId = originServiceAreaId;
	}
	public Integer getDestinationServiceAreaId() {
		return destinationServiceAreaId;
	}
	public void setDestinationServiceAreaId(Integer destinationServiceAreaId) {
		this.destinationServiceAreaId = destinationServiceAreaId;
	}
	public Double getUnitWt() {
		return unitWt;
	}
	public void setUnitWt(Double unitWt) {
		this.unitWt = unitWt;
	}
	public Double getUnitVol() {
		return unitVol;
	}
	public void setUnitVol(Double unitVol) {
		this.unitVol = unitVol;
	}
	public Integer getPaymentTermId() {
		return paymentTermId;
	}
	public void setPaymentTermId(Integer paymentTermId) {
		this.paymentTermId = paymentTermId;
	}
	public Double getLowEndBidAmt() {
		return lowEndBidAmt;
	}
	public void setLowEndBidAmt(Double lowEndBidAmt) {
		this.lowEndBidAmt = lowEndBidAmt;
	}
	public Double getHighEndBidAmt() {
		return highEndBidAmt;
	}
	public void setHighEndBidAmt(Double highEndBidAmt) {
		this.highEndBidAmt = highEndBidAmt;
	}
	public Double getAvgBidAmt() {
		return avgBidAmt;
	}
	public void setAvgBidAmt(Double avgBidAmt) {
		this.avgBidAmt = avgBidAmt;
	}
	public Double getTargetRateAmt() {
		return targetRateAmt;
	}
	public void setTargetRateAmt(Double targetRateAmt) {
		this.targetRateAmt = targetRateAmt;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public String getTenderTypeCd() {
		return tenderTypeCd;
	}
	public void setTenderTypeCd(String tenderTypeCd) {
		this.tenderTypeCd = tenderTypeCd;
	}
	
	public Double getOriginWaitTm() {
		return originWaitTm;
	}
	public void setOriginWaitTm(Double originWaitTm) {
		this.originWaitTm = originWaitTm;
	}
	public Double getDestWaitTm() {
		return destWaitTm;
	}
	public void setDestWaitTm(Double destWaitTm) {
		this.destWaitTm = destWaitTm;
	}
	public String getOriginWaitTmUom() {
		return originWaitTmUom;
	}
	public void setOriginWaitTmUom(String originWaitTmUom) {
		this.originWaitTmUom = originWaitTmUom;
	}
	public String getDestWaitTmUom() {
		return destWaitTmUom;
	}
	public void setDestWaitTmUom(String destWaitTmUom) {
		this.destWaitTmUom = destWaitTmUom;
	}
	public String getCommodityDescTxt() {
		return commodityDescTxt;
	}
	public void setCommodityDescTxt(String commodityDescTxt) {
		this.commodityDescTxt = commodityDescTxt;
	}
	public String getProBillNbr() {
		return proBillNbr;
	}
	public void setProBillNbr(String proBillNbr) {
		this.proBillNbr = proBillNbr;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public int getPickupEntityAddressId() {
		return pickupEntityAddressId;
	}
	public void setPickupEntityAddressId(int pickupEntityAddressId) {
		this.pickupEntityAddressId = pickupEntityAddressId;
	}
	public String getPickupEntityName() {
		return pickupEntityName;
	}
	public void setPickupEntityName(String pickupEntityName) {
		this.pickupEntityName = pickupEntityName;
	}
	public String getPickupFloorApartment() {
		return pickupFloorApartment;
	}
	public void setPickupFloorApartment(String pickupFloorApartment) {
		this.pickupFloorApartment = pickupFloorApartment;
	}
	public String getPickupBuilding() {
		return pickupBuilding;
	}
	public void setPickupBuilding(String pickupBuilding) {
		this.pickupBuilding = pickupBuilding;
	}
	public String getPickupHouseNumber() {
		return pickupHouseNumber;
	}
	public void setPickupHouseNumber(String pickupHouseNumber) {
		this.pickupHouseNumber = pickupHouseNumber;
	}
	public String getPickupLine1() {
		return pickupLine1;
	}
	public void setPickupLine1(String pickupLine1) {
		this.pickupLine1 = pickupLine1;
	}
	public String getPickupStreetName() {
		return pickupStreetName;
	}
	public void setPickupStreetName(String pickupStreetName) {
		this.pickupStreetName = pickupStreetName;
	}
	public String getPickupCityLocality() {
		return pickupCityLocality;
	}
	public void setPickupCityLocality(String pickupCityLocality) {
		this.pickupCityLocality = pickupCityLocality;
	}
	public String getPickupProvince() {
		return pickupProvince;
	}
	public void setPickupProvince(String pickupProvince) {
		this.pickupProvince = pickupProvince;
	}
	public String getPickupCountry() {
		return pickupCountry;
	}
	public void setPickupCountry(String pickupCountry) {
		this.pickupCountry = pickupCountry;
	}
	public String getPickupPostalCode() {
		return pickupPostalCode;
	}
	public void setPickupPostalCode(String pickupPostalCode) {
		this.pickupPostalCode = pickupPostalCode;
	}
	public String getPickupDistrictName() {
		return pickupDistrictName;
	}
	public void setDickupPistrictName(String pickupDistrictName) {
		this.pickupDistrictName = pickupDistrictName;
	}
	public BigDecimal getPickupLat() {
		return pickupLat;
	}
	public void setPickupLat(BigDecimal pickupLat) {
		this.pickupLat = pickupLat;
	}
	public BigDecimal getPickupLon() {
		return pickupLon;
	}
	public void setPickupLon(BigDecimal pickupLon) {
		this.pickupLon = pickupLon;
	}
	public String getPickupLatLongStatusCd() {
		return pickupLatLongStatusCd;
	}
	public void setPickupLatLongStatusCd(String pickupLatLongStatusCd) {
		this.pickupLatLongStatusCd = pickupLatLongStatusCd;
	}
	public boolean isPickupIsDc() {
		return pickupIsDc;
	}
	public void setPickupIsDc(boolean pickupIsDc) {
		this.pickupIsDc = pickupIsDc;
	}
	public Integer getDeliveryEntityAddressId() {
		return deliveryEntityAddressId;
	}
	public void setDeliveryEntityAddressId(int deliveryEntityAddressId) {
		this.deliveryEntityAddressId = deliveryEntityAddressId;
	}
	public String getDeliveryEntityName() {
		return deliveryEntityName;
	}
	public void setDeliveryEntityName(String deliveryEntityName) {
		this.deliveryEntityName = deliveryEntityName;
	}
	public String getDeliveryFloorApartment() {
		return deliveryFloorApartment;
	}
	public void setDeliveryFloorApartment(String deliveryFloorApartment) {
		this.deliveryFloorApartment = deliveryFloorApartment;
	}
	public String getDeliveryBuilding() {
		return deliveryBuilding;
	}
	public void setDeliveryBuilding(String deliveryBuilding) {
		this.deliveryBuilding = deliveryBuilding;
	}
	public String getDeliveryHouseNumber() {
		return deliveryHouseNumber;
	}
	public void setDeliveryHouseNumber(String deliveryHouseNumber) {
		this.deliveryHouseNumber = deliveryHouseNumber;
	}
	public String getDeliveryLine1() {
		return deliveryLine1;
	}
	public void setDeliveryLine1(String deliveryLine1) {
		this.deliveryLine1 = deliveryLine1;
	}
	public String getDeliveryStreetName() {
		return deliveryStreetName;
	}
	public void setDeliveryStreetName(String deliveryStreetName) {
		this.deliveryStreetName = deliveryStreetName;
	}
	public String getDeliveryCityLocality() {
		return deliveryCityLocality;
	}
	public void setDeliveryCityLocality(String deliveryCityLocality) {
		this.deliveryCityLocality = deliveryCityLocality;
	}
	public String getDeliveryProvince() {
		return deliveryProvince;
	}
	public void setDeliveryProvince(String deliveryProvince) {
		this.deliveryProvince = deliveryProvince;
	}
	public String getDeliveryCountry() {
		return deliveryCountry;
	}
	public void setDeliveryCountry(String deliveryCountry) {
		this.deliveryCountry = deliveryCountry;
	}
	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}
	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}
	public String getDeliveryDistrictName() {
		return deliveryDistrictName;
	}
	public void setDeliveryDistrictName(String deliveryDistrictName) {
		this.deliveryDistrictName = deliveryDistrictName;
	}
	public BigDecimal getDeliveryLat() {
		return deliveryLat;
	}
	public void setDeliveryLat(BigDecimal deliveryLat) {
		this.deliveryLat = deliveryLat;
	}
	public BigDecimal getDeliveryLon() {
		return deliveryLon;
	}
	public void setDeliveryLon(BigDecimal deliveryLon) {
		this.deliveryLon = deliveryLon;
	}
	public String getDeliveryLatLongStatusCd() {
		return deliveryLatLongStatusCd;
	}
	public void setDeliveryLatLongStatusCd(String deliveryLatLongStatusCd) {
		this.deliveryLatLongStatusCd = deliveryLatLongStatusCd;
	}
	public boolean isDeliveryIsDc() {
		return deliveryIsDc;
	}
	public void setDeliveryIsDc(boolean deliveryIsDc) {
		this.deliveryIsDc = deliveryIsDc;
	}
	public Integer getEquipmentTypeId() {
		return equipmentTypeId;
	}
	public void setEquipmentTypeId(Integer equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}
	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}
	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}
	public String getAccessorialCd() {
		return accessorialCd;
	}
	public void setAccessorialCd(String accessorialCd) {
		this.accessorialCd = accessorialCd;
	}
	public String getAccessorialDataSource() {
		return accessorialDataSource;
	}
	public void setAccessorialDataSource(String accessorialDataSource) {
		this.accessorialDataSource = accessorialDataSource;
	}
	public String getAccessorialDescription() {
		return accessorialDescription;
	}
	public void setAccessorialDescription(String accessorialDescription) {
		this.accessorialDescription = accessorialDescription;
	}
	public String getAccessorialType() {
		return accessorialType;
	}
	public void setAccessorialType(String accessorialType) {
		this.accessorialType = accessorialType;
	}
	public Double getAccessorialAmt() {
		return accessorialAmt;
	}
	public void setAccessorialAmt(Double accessorialAmt) {
		this.accessorialAmt = accessorialAmt;
	}
	public String getPaymentTermCd() {
		return paymentTermCd;
	}
	public void setPaymentTermCd(String paymentTermCd) {
		this.paymentTermCd = paymentTermCd;
	}
	
	
	
	public String getPaymentTermName() {
		return paymentTermName;
	}
	public void setPaymentTermName(String paymentTermName) {
		this.paymentTermName = paymentTermName;
	}
	public void setPickupDistrictName(String pickupDistrictName) {
		this.pickupDistrictName = pickupDistrictName;
	}
	public Integer getShipmentDetailId() {
		return shipmentDetailId;
	}
	public void setShipmentDetailId(Integer shipmentDetailId) {
		this.shipmentDetailId = shipmentDetailId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnitOfMeasureDtl() {
		return unitOfMeasureDtl;
	}
	public void setUnitOfMeasureDtl(String unitOfMeasureDtl) {
		this.unitOfMeasureDtl = unitOfMeasureDtl;
	}
	public Double getShipmentWt() {
		return shipmentWt;
	}
	public void setShipmentWt(Double shipmentWt) {
		this.shipmentWt = shipmentWt;
	}
	public String getWtUom() {
		return wtUom;
	}
	public void setWtUom(String wtUom) {
		this.wtUom = wtUom;
	}
	public Double getShipmentVol() {
		return shipmentVol;
	}
	public void setShipmentVol(Double shipmentVol) {
		this.shipmentVol = shipmentVol;
	}
	public String getVolUom() {
		return volUom;
	}
	public void setVolUom(String volUom) {
		this.volUom = volUom;
	}
	public Double getShipmentLen() {
		return shipmentLen;
	}
	public void setShipmentLen(Double shipmentLen) {
		this.shipmentLen = shipmentLen;
	}
	public Double getShipmentWth() {
		return shipmentWth;
	}
	public void setShipmentWth(Double shipmentWth) {
		this.shipmentWth = shipmentWth;
	}
	public Double getShipmentHt() {
		return shipmentHt;
	}
	public void setShipmentHt(Double shipmentHt) {
		this.shipmentHt = shipmentHt;
	}
	public String getDimensionUom() {
		return dimensionUom;
	}
	public void setDimensionUom(String dimensionUom) {
		this.dimensionUom = dimensionUom;
	}
	public String getFreightClassCd() {
		return freightClassCd;
	}
	public void setFreightClassCd(String freightClassCd) {
		this.freightClassCd = freightClassCd;
	}
	public Boolean getIsHazardous() {
		return isHazardous;
	}
	public void setIsHazardous(Boolean isHazardous) {
		this.isHazardous = isHazardous;
	}
	public String getProductDescTxt() {
		return productDescTxt;
	}
	public void setProductDescTxt(String productDescTxt) {
		this.productDescTxt = productDescTxt;
	}
	public Integer getBidRank() {
		return bidRank;
	}
	public void setBidRank(Integer bidRank) {
		this.bidRank = bidRank;
	}
	public Integer getShipmentCarrierBidId() {
		return shipmentCarrierBidId;
	}
	public void setShipmentCarrierBidId(Integer shipmentCarrierBidId) {
		this.shipmentCarrierBidId = shipmentCarrierBidId;
	}
	public Integer getCarrierId() {
		return carrierId;
	}
	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}
	public String getQuoteStatusCd() {
		return quoteStatusCd;
	}
	public void setQuoteStatusCd(String quoteStatusCd) {
		this.quoteStatusCd = quoteStatusCd;
	}
	public String getRateTypeCd() {
		return rateTypeCd;
	}
	public void setRateTypeCd(String rateTypeCd) {
		this.rateTypeCd = rateTypeCd;
	}
	public String getRateTypeDescTxt() {
		return rateTypeDescTxt;
	}
	public void setRateTypeDescTxt(String rateTypeDescTxt) {
		this.rateTypeDescTxt = rateTypeDescTxt;
	}
	public Double getTransitTm() {
		return transitTm;
	}
	public void setTransitTm(Double transitTm) {
		this.transitTm = transitTm;
	}
	public Double getQuoteAmt() {
		return quoteAmt;
	}
	public void setQuoteAmt(Double quoteAmt) {
		this.quoteAmt = quoteAmt;
	}
	public String getQuoteCurrencyCd() {
		return quoteCurrencyCd;
	}
	public void setQuoteCurrencyCd(String quoteCurrencyCd) {
		this.quoteCurrencyCd = quoteCurrencyCd;
	}
	public Date getQuoteDatestamp() {
		return quoteDatestamp;
	}
	public void setQuoteDatestamp(Date quoteDatestamp) {
		this.quoteDatestamp = quoteDatestamp;
	}
	public Integer getQuoteExpirationValue() {
		return quoteExpirationValue;
	}
	public void setQuoteExpirationValue(int quoteExpirationValue) {
		this.quoteExpirationValue = quoteExpirationValue;
	}
	public String getQuoteExpirationUom() {
		return quoteExpirationUom;
	}
	public void setQuoteExpirationUom(String quoteExpirationUom) {
		this.quoteExpirationUom = quoteExpirationUom;
	}
	public String getCommentTxt() {
		return commentTxt;
	}
	public void setCommentTxt(String commentTxt) {
		this.commentTxt = commentTxt;
	}
	public String getCarrierCd() {
		return carrierCd;
	}
	public void setCarrierCd(String carrierCd) {
		this.carrierCd = carrierCd;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public String getCarrierRating() {
		return carrierRating;
	}
	public void setCarrierRating(String carrierRating) {
		this.carrierRating = carrierRating;
	}
	public String getBidEnded() {
		return bidEnded;
	}
	public void setBidEnded(String bidEnded) {
		this.bidEnded = bidEnded;
	}
	public void setDeliveryEntityAddressId(Integer deliveryEntityAddressId) {
		this.deliveryEntityAddressId = deliveryEntityAddressId;
	}
	public void setCarrierId(Integer carrierId) {
		this.carrierId = carrierId;
	}
	public void setQuoteExpirationValue(Integer quoteExpirationValue) {
		this.quoteExpirationValue = quoteExpirationValue;
	}
	public String getShipperDefinedFlgBidDetail() {
		return shipperDefinedFlgBidDetail;
	}
	public void setShipperDefinedFlgBidDetail(String shipperDefinedFlgBidDetail) {
		this.shipperDefinedFlgBidDetail = shipperDefinedFlgBidDetail;
	}
	public Double getAccessorialAmtBidDetail() {
		return accessorialAmtBidDetail;
	}
	public void setAccessorialAmtBidDetail(Double accessorialAmtBidDetail) {
		this.accessorialAmtBidDetail = accessorialAmtBidDetail;
	}
	public String getAccessorialCcyBidDetail() {
		return accessorialCcyBidDetail;
	}
	public void setAccessorialCcyBidDetail(String accessorialCcyBidDetail) {
		this.accessorialCcyBidDetail = accessorialCcyBidDetail;
	}
	public String getAccessorialCdBidDetail() {
		return accessorialCdBidDetail;
	}
	public void setAccessorialCdBidDetail(String accessorialCdBidDetail) {
		this.accessorialCdBidDetail = accessorialCdBidDetail;
	}
	public String getAccessorialDataSourceBidDetail() {
		return accessorialDataSourceBidDetail;
	}
	public void setAccessorialDataSourceBidDetail(String accessorialDataSourceBidDetail) {
		this.accessorialDataSourceBidDetail = accessorialDataSourceBidDetail;
	}
	public String getAccessorialDescriptionBidDetail() {
		return accessorialDescriptionBidDetail;
	}
	public void setAccessorialDescriptionBidDetail(String accessorialDescriptionBidDetail) {
		this.accessorialDescriptionBidDetail = accessorialDescriptionBidDetail;
	}
	public String getAccessorialRateCommentBidDetail() {
		return accessorialRateCommentBidDetail;
	}
	public void setAccessorialRateCommentBidDetail(String accessorialRateCommentBidDetail) {
		this.accessorialRateCommentBidDetail = accessorialRateCommentBidDetail;
	}
	public String getAccessorialTypeBidDetail() {
		return accessorialTypeBidDetail;
	}
	public void setAccessorialTypeBidDetail(String accessorialTypeBidDetail) {
		this.accessorialTypeBidDetail = accessorialTypeBidDetail;
	}
	public Integer getCarrierIdBidDetail() {
		return carrierIdBidDetail;
	}
	public void setCarrierIdBidDetail(Integer carrierIdBidDetail) {
		this.carrierIdBidDetail = carrierIdBidDetail;
	}
	public Date getCreateDtBidDetail() {
		return createDtBidDetail;
	}
	public void setCreateDtBidDetail(Date createDtBidDetail) {
		this.createDtBidDetail = createDtBidDetail;
	}
	public Date getUpdateDtBidDetail() {
		return updateDtBidDetail;
	}
	public void setUpdateDtBidDetail(Date updateDtBidDetail) {
		this.updateDtBidDetail = updateDtBidDetail;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public String getSi() {
		return si;
	}
	public void setSi(String si) {
		this.si = si;
	}
	public Integer getStopNbr() {
		return stopNbr;
	}
	public void setStopNbr(Integer stopNbr) {
		this.stopNbr = stopNbr;
	}
	public String getStopLocName() {
		return stopLocName;
	}
	public void setStopLocName(String stopLocName) {
		this.stopLocName = stopLocName;
	}
	public String getStopAddr() {
		return stopAddr;
	}
	public void setStopAddr(String stopAddr) {
		this.stopAddr = stopAddr;
	}
	public String getStopCity() {
		return stopCity;
	}
	public void setStopCity(String stopCity) {
		this.stopCity = stopCity;
	}
	public String getStopProvince() {
		return stopProvince;
	}
	public void setStopProvince(String stopProvince) {
		this.stopProvince = stopProvince;
	}
	public String getStopCountry() {
		return stopCountry;
	}
	public void setStopCountry(String stopCountry) {
		this.stopCountry = stopCountry;
	}
	public String getStopPostal() {
		return stopPostal;
	}
	public void setStopPostal(String stopPostal) {
		this.stopPostal = stopPostal;
	}
	public String getStopPonbr() {
		return stopPonbr;
	}
	public void setStopPonbr(String stopPonbr) {
		this.stopPonbr = stopPonbr;
	}
	public String getStopRlsnbr() {
		return stopRlsnbr;
	}
	public void setStopRlsnbr(String stopRlsnbr) {
		this.stopRlsnbr = stopRlsnbr;
	}
	public String getShptType() {
		return shptType;
	}
	public void setShptType(String shptType) {
		this.shptType = shptType;
	}
	public Integer getStopCnt() {
		return stopCnt;
	}
	public void setStopCnt(Integer stopCnt) {
		this.stopCnt = stopCnt;
	}

	
	
	
	//private List<String> shipmenDetails;
	
	// Denormalized for JSON - end
	
	
	
}
