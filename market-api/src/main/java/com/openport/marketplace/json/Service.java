/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class Service {
	@Column(name="service_id")
	private int serviceId;
	@Column(name="service_name")
	private String serviceName;
	@Column(name="is_transportation")
	private boolean isTransportation;
	@Column(name="create_user_id")
	private int createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private int updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;

	@Column(name="company_id")
	private Integer companyId;
	@Column(name="company_service_id")
	private Integer companyServiceId;
	
	private Boolean status;
	private Integer shipmentId;
	/**
	 * 
	 */
	public Service() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the serviceId
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the isTransportation
	 */
	public boolean isTransportation() {
		return isTransportation;
	}
	public boolean getIsTransportation() {
		return isTransportation;
	}

	/**
	 * @param isTransportation the isTransportation to set
	 */
	public void setIsTransportation(boolean isTransportation) {
		this.isTransportation = isTransportation;
	}

	/**
	 * @return the createUserId
	 */
	public int getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the createDt
	 */
	public Timestamp getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt the createDt to set
	 */
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the updateUserId
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the updateDt
	 */
	public Timestamp getUpdateDt() {
		return updateDt;
	}

	/**
	 * @param updateDt the updateDt to set
	 */
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getCompanyServiceId() {
		return companyServiceId;
	}

	public void setCompanyServiceId(Integer companyServiceId) {
		this.companyServiceId = companyServiceId;
	}

	public void setTransportation(boolean isTransportation) {
		this.isTransportation = isTransportation;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}

}
