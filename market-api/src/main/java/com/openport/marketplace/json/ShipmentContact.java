/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * @author relly
 *
 */
@XmlRootElement
@Entity
public class ShipmentContact {

	@Column(name="shipment_id")
	private Integer shipmentId;
	@Column(name="contact_type_cd")
	private Character contactTypeCd;
	@Column(name="contact_name")
	private String contactName;
	@Column(name="contact_phone_nbr")
	private String contactPhoneNbr;
	@Column(name="contact_email_address")
	private String contactEmailAddress;
	public Integer getShipmentId() {
		return shipmentId;
	}
	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}
	public Character getContactTypeCd() {
		return contactTypeCd;
	}
	public void setContactTypeCd(Character contactTypeCd) {
		this.contactTypeCd = contactTypeCd;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactPhoneNbr() {
		return contactPhoneNbr;
	}
	public void setContactPhoneNbr(String contactPhoneNbr) {
		this.contactPhoneNbr = contactPhoneNbr;
	}
	public String getContactEmailAddress() {
		return contactEmailAddress;
	}
	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}
	@Override
	public String toString() {
		return "ShipmentContact [shipmentId=" + shipmentId + ", contactTypeCd=" + contactTypeCd + ", contactName="
				+ contactName + ", contactPhoneNbr=" + contactPhoneNbr + ", contactEmailAddress=" + contactEmailAddress
				+ "]";
	}
	
}
