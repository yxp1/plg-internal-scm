package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EquipmentProfile {
private String name;
private Integer qty;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Integer getQty() {
	return qty;
}
public void setQty(Integer qty) {
	this.qty = qty;
}

}
