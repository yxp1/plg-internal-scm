/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class CompanyUser {

	@Column(name="company_user_id")
	private int companyUserId;
	@Column(name="company_Id")
	private int companyId;
	@Column(name="user_id")
	private int userId;
	@Column(name="is_Primary_Contact")
	private boolean isPrimaryContact;
	@Column(name="is_Technical_Contact")
	private boolean isTechnicalContact;
	@Column(name="create_user_id")
	private int createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private int updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;

	// Denormalized for JSON - start
	private List<User> users;
	private User primaryContact;
	private User technicalContact;
	private User  user;
	// Denormalized for JSON - end
	
	/**
	 * 
	 */
	public CompanyUser() {
		// TODO Auto-generated constructor stub
	}

	public int getCompanyUserId() {
		return companyUserId;
	}

	public void setCompanyUserId(int companyUserId) {
		this.companyUserId = companyUserId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public boolean isPrimaryContact() {
		return isPrimaryContact;
	}

	public boolean getIsPrimaryContact() {
		return isPrimaryContact;
	}

	public void setIsPrimaryContact(boolean isPrimaryContact) {
		this.isPrimaryContact = isPrimaryContact;
	}

	public boolean isTechnicalContact() {
		return isTechnicalContact;
	}

	public boolean getIsTechnicalContact() {
		return isTechnicalContact;
	}

	public void setIsTechnicalContact(boolean isTechnicalContact) {
		this.isTechnicalContact = isTechnicalContact;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	/**
	 * @return the users
	 */
	public List<User> getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(List<User> users) {
		this.users = users;
	}

	/**
	 * @return the primaryContact
	 */
	public User getPrimaryContact() {
		return primaryContact;
	}

	/**
	 * @param primaryContact the primaryContact to set
	 */
	public void setPrimaryContact(User primaryContact) {
		this.primaryContact = primaryContact;
	}

	/**
	 * @return the technicalContact
	 */
	public User getTechnicalContact() {
		return technicalContact;
	}

	/**
	 * @param technicalContact the technicalContact to set
	 */
	public void setTechnicalContact(User technicalContact) {
		this.technicalContact = technicalContact;
	}

	/**
	 * @return the user information
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user for user information
	 */
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "CompanyUser [companyUserId=" + companyUserId + ", companyId=" + companyId + ", userId=" + userId
				+ ", isPrimaryContact=" + isPrimaryContact + ", isTechnicalContact=" + isTechnicalContact
				+ ", createUserId=" + createUserId + ", createDt=" + createDt + ", updateUserId=" + updateUserId
				+ ", updateDt=" + updateDt + ", users=" + users + ", primaryContact=" + primaryContact
				+ ", technicalContact=" + technicalContact + ", user=" + user + "]";
	}
    
}
