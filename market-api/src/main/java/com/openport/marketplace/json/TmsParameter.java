package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class TmsParameter {

@Column(name="parameter_name")
private String parameterName;
@Column(name="parameter_seq_nbr")
private Integer parameterSeqNbr;
@Column(name="parameter_1_value")
private String parameter1Value;
@Column(name="parameter_2_value")
private String parameter2Value;

//denormalized
private String parameterId;
public String getParameterName() {
	return parameterName;
}

public void setParameterName(String parameterName) {
	this.parameterName = parameterName.toUpperCase();
}

public String getParameterId() {
	return parameterId;
}

public void setParameterId(String parameterId) {
	this.parameterId = parameterId.toUpperCase();
}

public Integer getParameterSeqNbr() {
	return parameterSeqNbr;
}

public void setParameterSeqNbr(Integer parameterSeqNbr) {
	this.parameterSeqNbr = parameterSeqNbr;
}

public String getParameter1Value() {
	return parameter1Value.toUpperCase();
}

public void setParameter1Value(String parameter1Value) {
	this.parameter1Value = parameter1Value;
}

public String getParameter2Value() {
	return parameter2Value.toUpperCase();
}

public void setParameter2Value(String parameter2Value) {
	this.parameter2Value = parameter2Value;
}






}
