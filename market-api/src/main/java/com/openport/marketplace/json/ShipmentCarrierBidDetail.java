/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class ShipmentCarrierBidDetail {

	@Column(name="shipper_defined_flg")
	private String  shipperDefinedFlg;
	
	@Column(name="accessorial_amt")
	private Double accessorialAmt;
	
	@Column(name="accessorial_ccy")
	private String accessorialCcy;
	
	@Column(name="accessorial_cd")
	private String accessorialCd;
	
	@Column(name="accessorial_data_source")
	private String accessorialDataSource;
	
	@Column(name="accessorial_description")
	private String accessorialDescription;
	
	@Column(name="accessorial_rate_comment")
	private String accessorialRateComment;
	
	@Column(name="accessorial_type")
	private String accessorialType;
	
	@Column(name="carrier_id")
	private Integer carrierId;
	
	@Column(name="create_dt")
	private Date createDt;
	
	@Column(name="shipment_carrier_bid_id")
	private Integer shipmentCarrierBidId;
	
	@Column(name="shipment_id")
	private Integer shipmentId;
	
	@Column(name="update_dt")
	private Date updateDt;
	
	private boolean isShipperDefined;
	
		

	public Double getAccessorialAmt() {
		return accessorialAmt;
	}

	public void setAccessorialAmt(Double accessorialAmt) {
		this.accessorialAmt = accessorialAmt;
	}

	public String getAccessorialCcy() {
		return accessorialCcy;
	}

	public void setAccessorialCcy(String accessorialCcy) {
		this.accessorialCcy = accessorialCcy;
	}

	public String getAccessorialCd() {
		return accessorialCd;
	}

	public void setAccessorialCd(String accessorialCd) {
		this.accessorialCd = accessorialCd;
	}

	public String getAccessorialDataSource() {
		return accessorialDataSource;
	}

	public void setAccessorialDataSource(String accessorialDataSource) {
		this.accessorialDataSource = accessorialDataSource;
	}

	public String getAccessorialDescription() {
		return accessorialDescription;
	}

	public void setAccessorialDescription(String accessorialDescription) {
		this.accessorialDescription = accessorialDescription;
	}

	public String getAccessorialRateComment() {
		return accessorialRateComment;
	}

	public void setAccessorialRateComment(String accessorialRateComment) {
		this.accessorialRateComment = accessorialRateComment;
	}

	public String getAccessorialType() {
		return accessorialType;
	}

	public void setAccessorialType(String accessorialType) {
		this.accessorialType = accessorialType;
	}

	public Integer getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(Integer carrierId) {
		this.carrierId = carrierId;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public Integer getShipmentCarrierBidId() {
		return shipmentCarrierBidId;
	}

	public void setShipmentCarrierBidId(Integer shipmentCarrierBidId) {
		this.shipmentCarrierBidId = shipmentCarrierBidId;
	}

	public Integer getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public boolean isShipperDefined() {
		return isShipperDefined;
	}

	public void setShipperDefined(boolean isShipperDefined) {
		this.isShipperDefined = isShipperDefined;
	}

	public String getShipperDefinedFlg() {
		return shipperDefinedFlg;
	}

	public void setShipperDefinedFlg(String shipperDefinedFlg) {
		this.shipperDefinedFlg = shipperDefinedFlg;
	}

	
	
}
