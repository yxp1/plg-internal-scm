package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Photo {
private String relativePath;
private Integer companyId;
public String getRelativePath() {
	return relativePath;
}
public void setRelativePath(String relativePath) {
	this.relativePath = relativePath;
}
public Integer getCompanyId() {
	return companyId;
}
public void setCompanyId(Integer companyId) {
	this.companyId = companyId;
}

}
