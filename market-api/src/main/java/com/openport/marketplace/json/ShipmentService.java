/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * @author relly
 *
 */
@XmlRootElement
@Entity
public class ShipmentService {

	@Column(name="shipment_service_id")
	private Integer shipmentServiceId;
	
	@Column(name="shipment_id")
	private Integer shipmentId;

	@Column(name="service_id")
	private Integer serviceId;
	
	//denormalized json
	@Column(name="service_name")
	private String serviceName;
	
	private Boolean status = true;

	public Integer getShipmentServiceId() {
		return shipmentServiceId;
	}

	public void setShipmentServiceId(Integer shipmentServiceId) {
		this.shipmentServiceId = shipmentServiceId;
	}

	public Integer getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ShipmentService [shipmentServiceId=" + shipmentServiceId + ", shipmentId=" + shipmentId + ", serviceId="
				+ serviceId + ", serviceName=" + serviceName + ", status=" + status + "]";
	}
	
}
