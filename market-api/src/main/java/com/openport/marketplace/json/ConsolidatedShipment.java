package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class ConsolidatedShipment {

	@Column(name = "vpo")
	private String vpo;
	
	@Column(name = "po_nbr")
	private String purchaseOrderNumber;
	
	@Column(name = "delivery_nbr")
	private String deliveryNumber;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "qty")
	private int quantity;
	
	@Column(name = "uom")
	private String uom;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "pro_bill_nbr")
	private String proBill;

	
	public ConsolidatedShipment() {
		
	}
	
	public String getVpo() {
		return vpo;
	}

	public void setVpo(String vpo) {
		this.vpo = vpo;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getDeliveryNumber() {
		return deliveryNumber;
	}

	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getProBill() {
		return proBill;
	}

	public void setProBill(String proBill) {
		this.proBill = proBill;
	}
}
