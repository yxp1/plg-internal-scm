/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class ShipmentAccessorial {

	@Column(name="ACCESSORIAL_CD")
	private String accessorialCd;
	
	@Column(name="ACCESSORIAL_DATA_SOURCE")
	private String accessorialDataSource;
	
	@Column(name="ACCESSORIAL_DESCRIPTION")
	private String accessorialDescription;
	
	@Column(name="ACCESSORIAL_TYPE")
	private String accessorialType;
	
	@Column(name="create_dt")
	private Date createDt;
	
	@Column(name="shipment_id")
	private Integer shipmentId;
	
	@Column(name="update_dt")
	private Date updateDt;
	
	@Column(name="ACCESSORIAL_AMT")
	private Double accessorialAmt;
	

	public String getAccessorialCd() {
		return accessorialCd;
	}

	public void setAccessorialCd(String accessorialCd) {
		this.accessorialCd = accessorialCd;
	}

	public String getAccessorialDataSource() {
		return accessorialDataSource;
	}

	public void setAccessorialDataSource(String accessorialDataSource) {
		this.accessorialDataSource = accessorialDataSource;
	}

	public String getAccessorialDescription() {
		return accessorialDescription;
	}

	public void setAccessorialDescription(String accessorialDescription) {
		this.accessorialDescription = accessorialDescription;
	}

	public String getAccessorialType() {
		return accessorialType;
	}

	public void setAccessorialType(String accessorialType) {
		this.accessorialType = accessorialType;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public Integer getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public Double getAccessorialAmt() {
		return accessorialAmt;
	}

	public void setAccessorialAmt(Double accessorialAmt) {
		this.accessorialAmt = accessorialAmt;
	}

	
	
	
}
