package com.openport.marketplace.json;

import java.util.List;

public class SubmittedArea {
private String startDt;
private String endDt;
private List<Bid> bids;
public String getStartDt() {
	return startDt;
}
public void setStartDt(String startDt) {
	this.startDt = startDt;
}
public String getEndDt() {
	return endDt;
}
public void setEndDt(String endDt) {
	this.endDt = endDt;
}
public List<Bid> getBids() {
	return bids;
}
public void setBids(List<Bid> bids) {
	this.bids = bids;
}


}
