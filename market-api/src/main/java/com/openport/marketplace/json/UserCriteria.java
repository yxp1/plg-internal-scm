package com.openport.marketplace.json;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class UserCriteria {
public String colName;
private String operand;
private String criteria;
private Integer priority;

private String datatype;
private String value;


public String getOperand() {
	return operand;
}
public void setOperand(String operand) {
	this.operand = operand;
}

public String getCriteria() {
	return criteria;
}
public void setCriteria(String criteria) {
	this.criteria = criteria;
}
public Integer getPriority() {
	return priority;
}
public void setPriority(Integer priority) {
	this.priority = priority;
}
public String getDatatype() {
	return datatype;
}
public void setDatatype(String datatype) {
	this.datatype = datatype;
}
public String getValue() {
	return value;
}
public void setValue(String value) {
	this.value = value;
}
public String getColName() {
	return colName;
}
public void setColName(String colName) {
	this.colName = colName;
}
@Override
public String toString() {
	return "UserCriteria [colName=" + colName + ", operand=" + operand + ", criteria=" + criteria + ", priority="
			+ priority + ", datatype=" + datatype + ", value=" + value + "]";
}



}
