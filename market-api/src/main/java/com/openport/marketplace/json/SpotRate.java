package com.openport.marketplace.json;

import java.util.List;

public class SpotRate {
	private String parentPartnerCd;
	private String partnerCd;
	private String customerCd;
	private String shipperCd;
	private String poNbr;
	private String releaseNbr;
	private Integer equipmentShipmentSeqNbr;
	private Integer legNbr;
	private String equipmentType;
	private String equipmentDesc;
	private String rateType;
	private String carrierName;
	private String carrier;
	private String origin;
	private String originName;
	
	private String originAddress;
	private String originCity;
	private String originState;
	private String originCountry;
	private String originPostal;
	private String destination;
	private String destName;
	private String destCity;
	private String destState;
	private String destCountry;
	private String destPostal;
	private String destAddress;
	private Integer noOfEquipment;
	private Double exchangeRt=1.0;
	private Double rtWt = 0.0;
	private String userId;
	private String currencyCd;
	private Double weight;
	private String wtUom;
	private Revenue revenue;
	private String locationType;
	private String locationName;
	private String locationAddr1;
	private String locationAddr2;
	private String locationCity;
	private String locationState;
	private String locationCountry;
	private String locationPostal;
	private List<Cost> costs;
	private String proBillNbr;
	
	
	
	
	public String getProBillNbr() {
		return proBillNbr;
	}
	public void setProBillNbr(String proBillNbr) {
		this.proBillNbr = proBillNbr;
	}
	public String getOriginAddress() {
		return originAddress;
	}
	public void setOriginAddress(String originAddress) {
		this.originAddress = originAddress;
	}
	public String getDestAddress() {
		return destAddress;
	}
	public void setDestAddress(String destAddress) {
		this.destAddress = destAddress;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getLocationAddr1() {
		return locationAddr1;
	}
	public void setLocationAddr1(String locationAddr1) {
		this.locationAddr1 = locationAddr1;
	}
	public String getLocationAddr2() {
		return locationAddr2;
	}
	public void setLocationAddr2(String locationAddr2) {
		this.locationAddr2 = locationAddr2;
	}
	public String getLocationCity() {
		return locationCity;
	}
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}
	public String getLocationState() {
		return locationState;
	}
	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}
	public String getLocationCountry() {
		return locationCountry;
	}
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}
	public String getLocationPostal() {
		return locationPostal;
	}
	public void setLocationPostal(String locationPostal) {
		this.locationPostal = locationPostal;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	public Revenue getRevenue() {
		return revenue;
	}
	public void setRevenue(Revenue revenue) {
		this.revenue = revenue;
	}
	public List<Cost> getCosts() {
		return costs;
	}
	public void setCosts(List<Cost> costs) {
		this.costs = costs;
	}
	public String getParentPartnerCd() {
		return parentPartnerCd;
	}
	public void setParentPartnerCd(String parentPartnerCd) {
		this.parentPartnerCd = parentPartnerCd;
	}
	public String getPartnerCd() {
		return partnerCd;
	}
	public void setPartnerCd(String partnerCd) {
		this.partnerCd = partnerCd;
	}
	public String getCustomerCd() {
		return customerCd;
	}
	public void setCustomerCd(String customerCd) {
		this.customerCd = customerCd;
	}
	public String getShipperCd() {
		return shipperCd;
	}
	public void setShipperCd(String shipperCd) {
		this.shipperCd = shipperCd;
	}
	public String getPoNbr() {
		return poNbr;
	}
	public void setPoNbr(String poNbr) {
		this.poNbr = poNbr;
	}
	public String getReleaseNbr() {
		return releaseNbr;
	}
	public void setReleaseNbr(String releaseNbr) {
		this.releaseNbr = releaseNbr;
	}
	public Integer getEquipmentShipmentSeqNbr() {
		return equipmentShipmentSeqNbr;
	}
	public void setEquipmentShipmentSeqNbr(Integer equipmentShipmentSeqNbr) {
		this.equipmentShipmentSeqNbr = equipmentShipmentSeqNbr;
	}
	public Integer getLegNbr() {
		return legNbr;
	}
	public void setLegNbr(Integer legNbr) {
		this.legNbr = legNbr;
	}
	public String getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}
	public String getEquipmentDesc() {
		return equipmentDesc;
	}
	public void setEquipmentDesc(String equipmentDesc) {
		this.equipmentDesc = equipmentDesc;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getOriginName() {
		return originName;
	}
	public void setOriginName(String originName) {
		this.originName = originName;
	}
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public String getOriginState() {
		return originState;
	}
	public void setOriginState(String originState) {
		this.originState = originState;
	}
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	public String getOriginPostal() {
		return originPostal;
	}
	public void setOriginPostal(String originPostal) {
		this.originPostal = originPostal;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestName() {
		return destName;
	}
	public void setDestName(String destName) {
		this.destName = destName;
	}
	public String getDestCity() {
		return destCity;
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public String getDestState() {
		return destState;
	}
	public void setDestState(String destState) {
		this.destState = destState;
	}
	public String getDestCountry() {
		return destCountry;
	}
	public void setDestCountry(String destCountry) {
		this.destCountry = destCountry;
	}
	public String getDestPostal() {
		return destPostal;
	}
	public void setDestPostal(String destPostal) {
		this.destPostal = destPostal;
	}
	public Integer getNoOfEquipment() {
		return noOfEquipment;
	}
	public void setNoOfEquipment(Integer noOfEquipment) {
		this.noOfEquipment = noOfEquipment;
	}
	public Double getExchangeRt() {
		return exchangeRt;
	}
	public void setExchangeRt(Double exchangeRt) {
		this.exchangeRt = exchangeRt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getWtUom() {
		return wtUom;
	}
	public void setWtUom(String wtUom) {
		this.wtUom = wtUom;
	}
	
	public Double getRtWt() {
		return rtWt;
	}
	public void setRtWt(Double rtWt) {
		this.rtWt = rtWt;
	}
	@Override
	public String toString() {
		return "SpotRate [parentPartnerCd=" + parentPartnerCd + ", partnerCd=" + partnerCd + ", customerCd="
				+ customerCd + ", shipperCd=" + shipperCd + ", poNbr=" + poNbr + ", releaseNbr=" + releaseNbr
				+ ", equipmentShipmentSeqNbr=" + equipmentShipmentSeqNbr + ", legNbr=" + legNbr + ", equipmentType="
				+ equipmentType + ", equipmentDesc=" + equipmentDesc + ", rateType=" + rateType + ", carrierName="
				+ carrierName + ", carrier=" + carrier + ", origin=" + origin + ", originName=" + originName
				+ ", originAddress=" + originAddress + ", originCity=" + originCity + ", originState=" + originState
				+ ", originCountry=" + originCountry + ", originPostal=" + originPostal + ", destination=" + destination
				+ ", destName=" + destName + ", destCity=" + destCity + ", destState=" + destState + ", destCountry="
				+ destCountry + ", destPostal=" + destPostal + ", destAddress=" + destAddress + ", noOfEquipment="
				+ noOfEquipment + ", exchangeRt=" + exchangeRt + ", rtWt=" + rtWt + ", userId=" + userId
				+ ", currencyCd=" + currencyCd + ", weight=" + weight + ", wtUom=" + wtUom + ", revenue=" + revenue
				+ ", locationType=" + locationType + ", locationName=" + locationName + ", locationAddr1="
				+ locationAddr1 + ", locationAddr2=" + locationAddr2 + ", locationCity=" + locationCity
				+ ", locationState=" + locationState + ", locationCountry=" + locationCountry + ", locationPostal="
				+ locationPostal + ", costs=" + costs + ", proBillNbr=" + proBillNbr + "]";
	}
	
		
	
}
