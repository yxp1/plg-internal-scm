/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class ShipmentCarrierBid {

	@Column(name="bid_rank")
	private Integer bidRank ;
	
	@Column(name="shipment_Carrier_Bid_Id")
	private Integer shipmentCarrierBidId;
	
	@Column(name="shipment_id")
	private int shipmentId;
	
	@Column(name="carrier_Id")
	private int carrierId;
	
	@Column(name="quote_Status_Cd")
	private String quoteStatusCd;
	
	@Column(name="rate_Type_Cd")
	private String rateTypeCd;
	
	@Column(name="rate_Type_Desc_Txt")
	private String rateTypeDescTxt;
	
	@Column(name="transit_Tm")
	private Double transitTm;
	
	@Column(name="equipment_Type_Id")
	private Integer equipmentTypeId;
	
	@Column(name="quote_Amt")
	private Double quoteAmt;
	
	@Column(name="quote_Currency_Cd")
	private String quoteCurrencyCd;
	
	@Column(name="quote_Datestamp")
	private Date quoteDatestamp;
	
	@Column(name="quote_Expiration_Value")
	private int quoteExpirationValue;
	
	@Column(name="quote_Expiration_Uom")
	private String quoteExpirationUom;
	
	@Column(name="comment_Txt")
	private String commentTxt;
	
	@Column(name="create_Dt")
	private Date createDt;
	
	@Column(name="create_User_Id")
	private String createUserId;
	
	@Column(name="update_Dt")
	private Date updateDt;
	
	@Column(name="update_User_Id")
	private String updateUserId;
	
	@Column(name="carrier_Cd")
	private String carrierCd;
	
	@Column(name="carrier_name")
	private String carrierName;
	
	@Column(name="carrier_rating")
	private String carrierRating;
	
	@Column(name="bid_ended")
	private String bidEnded;
	
	@Column(name="status_id")
	private String statusId;
	
	// Denormalized for JSON - start
	
	private List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails;
	

	public String getStatusId() {
		return statusId;
	}


	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}


	public String getBidEnded() {
		return bidEnded;
	}


	public void setBidEnded(String bidEnded) {
		this.bidEnded = bidEnded;
	}


	public String getCarrierRating() {
		return carrierRating;
	}


	public void setCarrierRating(String carrierRating) {
		this.carrierRating = carrierRating;
	}


	public String getCarrierName() {
		return carrierName;
	}


	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}


	public ShipmentCarrierBid() {
	}


	public Integer getShipmentCarrierBidId() {
		return this.shipmentCarrierBidId;
	}

	public void setShipmentCarrierBidId(Integer shipmentCarrierBidId) {
		this.shipmentCarrierBidId = shipmentCarrierBidId;
	}

	

	public int getShipmentId() {
		return shipmentId;
	}


	public void setShipmentId(int shipmentId) {
		this.shipmentId = shipmentId;
	}


	public int getCarrierId() {
		return this.carrierId;
	}

	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}

	public String getQuoteStatusCd() {
		return this.quoteStatusCd;
	}

	public void setQuoteStatusCd(String quoteStatusCd) {
		this.quoteStatusCd = quoteStatusCd;
	}

	public String getRateTypeCd() {
		return this.rateTypeCd;
	}

	public void setRateTypeCd(String rateTypeCd) {
		this.rateTypeCd = rateTypeCd;
	}

	public String getRateTypeDescTxt() {
		return this.rateTypeDescTxt;
	}

	public void setRateTypeDescTxt(String rateTypeDescTxt) {
		this.rateTypeDescTxt = rateTypeDescTxt;
	}

	public Double getTransitTm() {
		return this.transitTm;
	}

	public void setTransitTm(Double transitTm) {
		this.transitTm = transitTm;
	}

	public Integer getEquipmentTypeId() {
		return this.equipmentTypeId;
	}

	public void setEquipmentTypeId(Integer equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}

	public Double getQuoteAmt() {
		return this.quoteAmt;
	}

	public void setQuoteAmt(Double quoteAmt) {
		this.quoteAmt = quoteAmt;
	}

	public String getQuoteCurrencyCd() {
		return this.quoteCurrencyCd;
	}

	public void setQuoteCurrencyCd(String quoteCurrencyCd) {
		this.quoteCurrencyCd = quoteCurrencyCd;
	}

	public Date getQuoteDatestamp() {
		return this.quoteDatestamp;
	}

	public void setQuoteDatestamp(Date quoteDatestamp) {
		this.quoteDatestamp = quoteDatestamp;
	}

	public int getQuoteExpirationValue() {
		return this.quoteExpirationValue;
	}

	public void setQuoteExpirationValue(int quoteExpirationValue) {
		this.quoteExpirationValue = quoteExpirationValue;
	}

	public String getQuoteExpirationUom() {
		return this.quoteExpirationUom;
	}

	public void setQuoteExpirationUom(String quoteExpirationUom) {
		this.quoteExpirationUom = quoteExpirationUom;
	}

	public String getCommentTxt() {
		return this.commentTxt;
	}

	public void setCommentTxt(String commentTxt) {
		this.commentTxt = commentTxt;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getUpdateUserId() {
		return this.updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}


	public String getCarrierCd() {
		return carrierCd;
	}


	public void setCarrierCd(String carrierCd) {
		this.carrierCd = carrierCd;
	}


	public Integer getBidRank() {
		return bidRank;
	}


	public void setBidRank(Integer bidRank) {
		this.bidRank = bidRank;
	}


	public List<ShipmentCarrierBidDetail> getShipmentCarrierBidDetails() {
		return shipmentCarrierBidDetails;
	}


	public void setShipmentCarrierBidDetails(List<ShipmentCarrierBidDetail> shipmentCarrierBidDetails) {
		this.shipmentCarrierBidDetails = shipmentCarrierBidDetails;
	}

	
	
	
}
