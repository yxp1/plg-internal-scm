package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Gmt {

@Column(name="country")
private String country;
@Column(name="pacific")
private String pacific ;
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getPacific() {
	return pacific;
}
public void setPacific(String pacific) {
	this.pacific = pacific;
}


}
