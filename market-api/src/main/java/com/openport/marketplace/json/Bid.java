package com.openport.marketplace.json;

public class Bid {
	private Integer rows;
	private Boolean display;
	private String origin;
	private String destination;
	private String commodity;
	private String equipmentType;
	private String ratingUnit;
	private Integer cnt;
	private String rateAwarded;
	private String rate;
	private Integer groupTenderId;
	private Integer equipmentTypeId;
	public Integer getRows() {
		return rows;
	}
	
	



	public Integer getEquipmentTypeId() {
		return equipmentTypeId;
	}





	public void setEquipmentTypeId(Integer equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}





	public Integer getGroupTenderId() {
		return groupTenderId;
	}





	public void setGroupTenderId(Integer groupTenderId) {
		this.groupTenderId = groupTenderId;
	}





	public String getRate() {
		return rate;
	}





	public void setRate(String rate) {
		this.rate = rate;
	}





	public void setRows(Integer rows) {
		this.rows = rows;
	}
	public Boolean getDisplay() {
		return display;
	}
	public void setDisplay(Boolean display) {
		this.display = display;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public String getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}
	public String getRatingUnit() {
		return ratingUnit;
	}
	public void setRatingUnit(String ratingUnit) {
		this.ratingUnit = ratingUnit;
	}
	public Integer getCnt() {
		return cnt;
	}
	public void setCnt(Integer cnt) {
		this.cnt = cnt;
	}
	public String getRateAwarded() {
		return rateAwarded;
	}
	public void setRateAwarded(String rateAwarded) {
		this.rateAwarded = rateAwarded;
	}





	@Override
	public String toString() {
		return "Bid [rows=" + rows + ", display=" + display + ", origin=" + origin + ", destination=" + destination
				+ ", commodity=" + commodity + ", equipmentType=" + equipmentType + ", ratingUnit=" + ratingUnit
				+ ", cnt=" + cnt + ", rateAwarded=" + rateAwarded + ", rate=" + rate + "]";
	}
	

}
