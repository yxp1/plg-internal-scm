/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class EquipmentType {
	@Column(name = "equipment_type_id")
	private int equipmentTypeId;
	
	@Column(name = "equipment_type_name")
	private String equipmentTypeName;
	
	@Column(name = "create_user_id")
	private int createUserId;
	
	@Column(name = "create_dt")
	private Timestamp createDt;
	
	@Column(name = "update_user_id")
	private int updateUserId;
	
	@Column(name = "update_dt")
	private Timestamp updateDt;
	
	@Column(name = "equipment_type_Cd")
	private String equipmentTypeCd;

	
	

	/**
	 * 
	 */
	public EquipmentType() {
		// TODO Auto-generated constructor stub
	}

	public int getEquipmentTypeId() {
		return equipmentTypeId;
	}

	public void setEquipmentTypeId(int equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}

	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}

	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public String getEquipmentTypeCd() {
		return equipmentTypeCd;
	}

	public void setEquipmentTypeCd(String equipmentTypeCd) {
		this.equipmentTypeCd = equipmentTypeCd;
	}

}
