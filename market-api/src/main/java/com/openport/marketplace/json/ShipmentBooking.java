/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Relly
 *
 */
@XmlRootElement
@Entity
public class ShipmentBooking {

	private int shipmentId;
	private String carrierCd;
	private int shipmentCarrierBidId;
	private int assignedProviderId;
	
	
	public ShipmentBooking() {
		
	}
	

	

	public int getAssignedProviderId() {
		return assignedProviderId;
	}




	public void setAssignedProviderId(int assignedProviderId) {
		this.assignedProviderId = assignedProviderId;
	}




	public int getShipmentId() {
		return shipmentId;
	}


	public void setShipmentId(int shipmentId) {
		this.shipmentId = shipmentId;
	}


	public String getCarrierCd() {
		return carrierCd;
	}


	public void setCarrierCd(String carrierCd) {
		this.carrierCd = carrierCd;
	}


	public int getShipmentCarrierBidId() {
		return shipmentCarrierBidId;
	}


	public void setShipmentCarrierBidId(int shipmentCarrierBidId) {
		this.shipmentCarrierBidId = shipmentCarrierBidId;
	}

	
	
	
}
