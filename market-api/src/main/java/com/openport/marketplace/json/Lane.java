package com.openport.marketplace.json;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Lane {
	@Column(name = "lane_id")
	private int laneId;
	private String originCity;
	private String originProvince; //or state
	private String originPostalCd;
	private String originDistrict;
	private String originCountry;
	private String destCity;
	private String destProvince; //or state
	private String destPostalCd;
	private String destDistrict;
	private String destCountry;
	private String commodityCd;
	
	private Boolean isSelected;
	private int groupTenderId;
	private List<Equipment> equipmentList;
	private CompanyContractTenderLane companyContractTenderLane;

	
	public int getGroupTenderId() {
		return groupTenderId;
	}
	public void setGroupTenderId(int groupTenderId) {
		this.groupTenderId = groupTenderId;
	}
	public CompanyContractTenderLane getCompanyContractTenderLane() {
		return companyContractTenderLane;
	}
	public void setCompanyContractTenderLane(CompanyContractTenderLane companyContractTenderLane) {
		this.companyContractTenderLane = companyContractTenderLane;
	}
	public List<Equipment> getEquipmentList() {
		return equipmentList;
	}
	public void setEquipmentList(List<Equipment> equipmentList) {
		this.equipmentList = equipmentList;
	}
	public String getCommodityCd() {
		return commodityCd;
	}
	public void setCommodityCd(String commodityCd) {
		this.commodityCd = commodityCd;
	}
	public int getLaneId() {
		return laneId;
	}
	public void setLaneId(int laneId) {
		this.laneId = laneId;
	}
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public String getOriginProvince() {
		return originProvince;
	}
	public void setOriginProvince(String originProvince) {
		this.originProvince = originProvince;
	}
	public String getOriginPostalCd() {
		return originPostalCd;
	}
	public void setOriginPostalCd(String originPostalCd) {
		this.originPostalCd = originPostalCd;
	}
	public String getOriginDistrict() {
		return originDistrict;
	}
	public void setOriginDistrict(String originDistrict) {
		this.originDistrict = originDistrict;
	}
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	public String getDestCity() {
		return destCity;
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public String getDestProvince() {
		return destProvince;
	}
	public void setDestProvince(String destProvince) {
		this.destProvince = destProvince;
	}
	public String getDestPostalCd() {
		return destPostalCd;
	}
	public void setDestPostalCd(String destPostalCd) {
		this.destPostalCd = destPostalCd;
	}
	public String getDestDistrict() {
		return destDistrict;
	}
	public void setDestDistrict(String destDistrict) {
		this.destDistrict = destDistrict;
	}
	public String getDestCountry() {
		return destCountry;
	}
	public void setDestCountry(String destCountry) {
		this.destCountry = destCountry;
	}
	public Boolean getIsSelected() {
		return isSelected;
	}
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
	@Override
	public String toString() {
		return "Lane [laneId=" + laneId + ", originCity=" + originCity + ", originProvince=" + originProvince
				+ ", originPostalCd=" + originPostalCd + ", originDistrict=" + originDistrict + ", originCountry="
				+ originCountry + ", destCity=" + destCity + ", destProvince=" + destProvince + ", destPostalCd="
				+ destPostalCd + ", destDistrict=" + destDistrict + ", destCountry=" + destCountry + ", commodityCd="
				+ commodityCd + ", isSelected=" + isSelected + ", groupTenderId=" + groupTenderId + ", equipmentList="
				+ equipmentList + ", companyContractTenderLane=" + companyContractTenderLane + "]";
	}
	
}
