package com.openport.marketplace.json;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CompanyContractRate {

	private String ratePerUnitCd;
	private int equipmentTypeId;
	private int carrierCompanyId;
	private int laneId;
	private int shipperCompanyId;
	private int bidRank;
	private String updateUserId;
	private Date updateDt;
	private String createUserId;
	private Date createDt;
	private String commentTxt;
	private Date rateTerminationDt;
	private Date rateEffectiveDt;
	private String rateCurrencyCd;
	private Double rateAccessorialAmt;
	private Double rateAmt;
	private String statusCd;
	private String contractNbr;
	
	public String getRatePerUnitCd() {
		return ratePerUnitCd;
	}
	public void setRatePerUnitCd(String ratePerUnitCd) {
		this.ratePerUnitCd = ratePerUnitCd;
	}
	public int getEquipmentTypeId() {
		return equipmentTypeId;
	}
	public void setEquipmentTypeId(int equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}
	public int getCarrierCompanyId() {
		return carrierCompanyId;
	}
	public void setCarrierCompanyId(int carrierCompanyId) {
		this.carrierCompanyId = carrierCompanyId;
	}
	public int getLaneId() {
		return laneId;
	}
	public void setLaneId(int laneId) {
		this.laneId = laneId;
	}
	public int getShipperCompanyId() {
		return shipperCompanyId;
	}
	public void setShipperCompanyId(int shipperCompanyId) {
		this.shipperCompanyId = shipperCompanyId;
	}
	public int getBidRank() {
		return bidRank;
	}
	public void setBidRank(int bidRank) {
		this.bidRank = bidRank;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	public String getCommentTxt() {
		return commentTxt;
	}
	public void setCommentTxt(String commentTxt) {
		this.commentTxt = commentTxt;
	}
	public Date getRateTerminationDt() {
		return rateTerminationDt;
	}
	public void setRateTerminationDt(Date rateTerminationDt) {
		this.rateTerminationDt = rateTerminationDt;
	}
	public Date getRateEffectiveDt() {
		return rateEffectiveDt;
	}
	public void setRateEffectiveDt(Date rateEffectiveDt) {
		this.rateEffectiveDt = rateEffectiveDt;
	}
	public String getRateCurrencyCd() {
		return rateCurrencyCd;
	}
	public void setRateCurrencyCd(String rateCurrencyCd) {
		this.rateCurrencyCd = rateCurrencyCd;
	}
	public Double getRateAccessorialAmt() {
		return rateAccessorialAmt;
	}
	public void setRateAccessorialAmt(Double rateAccessorialAmt) {
		this.rateAccessorialAmt = rateAccessorialAmt;
	}
	public Double getRateAmt() {
		return rateAmt;
	}
	public void setRateAmt(Double rateAmt) {
		this.rateAmt = rateAmt;
	}
	public String getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}
	public String getContractNbr() {
		return contractNbr;
	}
	public void setContractNbr(String contractNbr) {
		this.contractNbr = contractNbr;
	}
}
