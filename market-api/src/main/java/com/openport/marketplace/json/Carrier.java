package com.openport.marketplace.json;

import java.sql.ResultSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity
public class Carrier {
/** as requested by Nourvel due to carrier.java conflict**/
@Column(name="CARRIER_CD")
private String carrierCd;
@Column(name="CARRIER_NAME")
private String carrierName;

private int companyId;
private String carrierContactPerson;
private String carrierEmailAddress;
private String carrierTelephoneNbr;
private String parentPartnerCd;



public int getCompanyId() {
	return companyId;
}
public void setCompanyId(int companyId) {
	this.companyId = companyId;
}
public String getParentPartnerCd() {
	return parentPartnerCd;
}
public void setParentPartnerCd(String parentPartnerCd) {
	this.parentPartnerCd = parentPartnerCd;
}
public String getCarrierContactPerson() {
	return carrierContactPerson;
}
public void setCarrierContactPerson(String carrierContactPerson) {
	this.carrierContactPerson = carrierContactPerson;
}
public String getCarrierEmailAddress() {
	return carrierEmailAddress;
}
public void setCarrierEmailAddress(String carrierEmailAddress) {
	this.carrierEmailAddress = carrierEmailAddress;
}
public String getCarrierTelephoneNbr() {
	return carrierTelephoneNbr;
}
public void setCarrierTelephoneNbr(String carrierTelephoneNbr) {
	this.carrierTelephoneNbr = carrierTelephoneNbr;
}
public String getCarrierCd() {
	return carrierCd;
}
public void setCarrierCd(String carrierCd) {
	this.carrierCd = carrierCd;
}
public String getCarrierName() {
	return carrierName;
}
public void setCarrierName(String carrierName) {
	this.carrierName = carrierName;
}

}
