package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HttpStatus {

	private int statusCode;
	private String reasonPhrase;
	
	public HttpStatus() {
	}

	public HttpStatus(javax.ws.rs.core.Response.Status sts) {
		this.statusCode = sts.getStatusCode();
		this.setReasonPhrase(sts.getReasonPhrase());
	}


	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the reasonPhrase
	 */
	public String getReasonPhrase() {
		return reasonPhrase;
	}

	/**
	 * @param reasonPhrase the reasonPhrase to set
	 */
	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}

	
}
