/**
 * 
 */
package com.openport.marketplace.json;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

//import com.openport.marketplace.model.EquipmentType;
//import com.openport.marketplace.model.FileDocumentType;
//import com.openport.marketplace.model.Service;
//import com.openport.marketplace.model.ServiceArea;

/**
 * @author Nargel
 *
 */
@XmlRootElement
@Entity
public class Profile {
	
	
	@Column(name="company_name")	
	private String 	companyName;
	@Column(name="business_headline")	
	private String  businessHeadline;
	@Column(name="business_description")	
	private String  businessDescription;
	@Column(name="company_code")	
	private String  companyCode;
	@Column(name="years_of_operation")	
	private Integer yearsOfOperation;
	@Column(name="rating_value")	
	private Integer rating;
	@Column(name="relative_path")	
	private String  companyLogoPath;
	@Column(name="company_address")	
	private String  companyAddress;
	@Column(name="city_name")	
	private String  hqCity;
	@Column(name="province_name")	
	private String  hqProvince;
	@Column(name="country_name")	
	private String  hqCountry;
	@Column(name="truck_pool_address")	
	private String  truckPoolAddress;
	@Column(name="branch_address")	
	private String  branchAddress;
	@Column(name="contact_name")	
	private String  contactName;
	@Column(name="email_address")	
	private String  emailAddress;
	@Column(name="phone_nbr")	
	private String  phoneNbr;
	@Column(name="service_name")	
	private String  serviceName;
	@Column(name="service_area_name")	
	private String  serviceAreaName;
	
	//denormalized
	private List<ServiceAreaProfile> serviceAreas;
	private List<ServiceProfile> services;
	private List<CarrierEquipment> equipment;
	
	
	public List<CarrierEquipment> getEquipment() {
		return equipment;
	}
	public void setEquipment(List<CarrierEquipment> equipment) {
		this.equipment = equipment;
	}
	
	public List<ServiceAreaProfile> getServiceAreas() {
		return serviceAreas;
	}
	public void setServiceAreas(List<ServiceAreaProfile> serviceAreas) {
		this.serviceAreas = serviceAreas;
	}
	public List<ServiceProfile> getServices() {
		return services;
	}
	public void setServices(List<ServiceProfile> services) {
		this.services = services;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBusinessHeadline() {
		return businessHeadline;
	}
	public void setBusinessHeadline(String businessHeadline) {
		this.businessHeadline = businessHeadline;
	}
	public String getBusinessDescription() {
		return businessDescription;
	}
	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public Integer getYearsOfOperation() {
		return yearsOfOperation;
	}
	public void setYearsOfOperation(Integer yearsOfOperation) {
		this.yearsOfOperation = yearsOfOperation;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}
	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getHqCity() {
		return hqCity;
	}
	public void setHqCity(String hqCity) {
		this.hqCity = hqCity;
	}
	public String getHqProvince() {
		return hqProvince;
	}
	public void setHqProvince(String hqProvince) {
		this.hqProvince = hqProvince;
	}
	public String getHqCountry() {
		return hqCountry;
	}
	public void setHqCountry(String hqCountry) {
		this.hqCountry = hqCountry;
	}
	public String getTruckPoolAddress() {
		return truckPoolAddress;
	}
	public void setTruckPoolAddress(String truckPoolAddress) {
		this.truckPoolAddress = truckPoolAddress;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNbr() {
		return phoneNbr;
	}
	public void setPhoneNbr(String phoneNbr) {
		this.phoneNbr = phoneNbr;
	}
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceAreaName() {
		return serviceAreaName;
	}
	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}
	
	
	
	
	
	
	
	
	
	
			
	
}
