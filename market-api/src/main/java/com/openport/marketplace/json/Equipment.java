/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class Equipment {
	@Column(name="equipment_id")
	private int equipmentId;
	@Column(name="equipment_type_id")
	private int equipmentTypeId;
	@Column(name="equipment_name")
	private String equipmentName;
	@Column(name="equipment_type_name")
	private String equipmentTypeName;
	
	@Column(name="create_user_id")
	private int createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private int updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;

	// Denormalized for JSON - start
	private EquipmentType equipmentType;
	private double equipmentCount;
	private int companyContractTenderLaneId;
	private Boolean isSelected;
	private int groupTenderId;
	private int carrierId;
	private String carrierName;
	private String status;
	private Double rateAmt;
	
	
	
	
	
	
	
	
	
	// Denormalized for JSON - end
	
	public String getCarrierName() {
		return carrierName;
	}





	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}





	public Double getRateAmt() {
		return rateAmt;
	}





	public void setRateAmt(Double rateAmt) {
		this.rateAmt = rateAmt;
	}





	public String getStatus() {
		return status;
	}





	public void setStatus(String status) {
		this.status = status;
	}





	public int getCarrierId() {
		return carrierId;
	}





	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}





	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}





	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}





	public int getGroupTenderId() {
		return groupTenderId;
	}





	public void setGroupTenderId(int groupTenderId) {
		this.groupTenderId = groupTenderId;
	}





	/**
	 * 
	 */
	
	
	
	public Equipment() {
		// TODO Auto-generated constructor stub
	}
	

	


	public Boolean getIsSelected() {
		return isSelected;
	}





	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}





	public int getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(int equipmentId) {
		this.equipmentId = equipmentId;
	}

	public int getEquipmentTypeId() {
		return equipmentTypeId;
	}

	public void setEquipmentTypeId(int equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	/**
	 * @return the equipmentType
	 */
	public EquipmentType getEquipmentType() {
		return equipmentType;
	}

	/**
	 * @param equipmentType the equipmentType to set
	 */
	public void setEquipmentType(EquipmentType equipmentType) {
		this.equipmentType = equipmentType;
	}

	public double getEquipmentCount() {
		return equipmentCount;
	}

	public void setEquipmentCount(double equipmentCount) {
		this.equipmentCount = equipmentCount;
	}

	public int getCompanyContractTenderLaneId() {
		return companyContractTenderLaneId;
	}

	public void setCompanyContractTenderLaneId(int companyContractTenderLaneId) {
		this.companyContractTenderLaneId = companyContractTenderLaneId;
	}





	@Override
	public String toString() {
		return "Equipment [equipmentId=" + equipmentId + ", equipmentTypeId=" + equipmentTypeId + ", equipmentName="
				+ equipmentName + ", equipmentTypeName=" + equipmentTypeName + ", createUserId=" + createUserId
				+ ", createDt=" + createDt + ", updateUserId=" + updateUserId + ", updateDt=" + updateDt
				+ ", equipmentType=" + equipmentType + ", equipmentCount=" + equipmentCount
				+ ", companyContractTenderLaneId=" + companyContractTenderLaneId + ", isSelected=" + isSelected
				+ ", groupTenderId=" + groupTenderId + ", carrierId=" + carrierId + ", status=" + status + ", rateAmt="
				+ rateAmt + "]";
	}





	


	


}
