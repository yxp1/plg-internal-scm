/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Nargel
 *
 */
@XmlRootElement
@Entity
public class Currency {
	@Column(name = "currency_cd")
	private String currencyCd;

	@Column(name = "country_cd")
	private String countryCd;

	@Column(name = "currency_name")
	private String currencyName;

	/**
	 * 
	 */
	public Currency() {
		// TODO Auto-generated constructor stub
	}

	public String getCurrencyCd() {
		return currencyCd;
	}

	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}

	public String getCountryCd() {
		return countryCd;
	}

	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public Currency(String currencyCd, String countryCd, String currencyName) {
		super();
		this.currencyCd = currencyCd;
		this.countryCd = countryCd;
		this.currencyName = currencyName;
	}

}
