/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author oscar_2
 *
 */
@XmlRootElement
@Entity
public class UnitOfMeasure {
	@Column(name="uom_cd")
	private String uomCd;
	@Column(name="uom_description_txt")
	private String uomDescriptionTxt;
	@Column(name="internal_standard_flg")
	private Boolean internalStandardFlg;
	public String getUomCd() {
		return uomCd;
	}
	public void setUomCd(String uomCd) {
		this.uomCd = uomCd;
	}
	public String getUomDescriptionTxt() {
		return uomDescriptionTxt;
	}
	public void setUomDescriptionTxt(String uomDescriptionTxt) {
		this.uomDescriptionTxt = uomDescriptionTxt;
	}
	public Boolean getInternalStandardFlg() {
		return internalStandardFlg;
	}
	public void setInternalStandardFlg(Boolean internalStandardFlg) {
		this.internalStandardFlg = internalStandardFlg;
	}
	
}
