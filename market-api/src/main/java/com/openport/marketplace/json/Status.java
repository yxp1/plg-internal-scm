/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author oscar_2
 *
 */
@XmlRootElement
@Entity
public class Status {
	
	


	@Column(name="status_id")
	private int statusId;
	
	@Column(name="status_cd")
	private String statusCd;
	
	@Column(name="description_txt")
	private String descriptionTxt;
	
	/**
	 * 
	 */
	public Status() {
		// TODO Auto-generated constructor stub
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public String getDescriptionTxt() {
		return descriptionTxt;
	}

	public void setDescriptionTxt(String descriptionTxt) {
		this.descriptionTxt = descriptionTxt;
	}
	
	
	

}
