/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author oscar_2
 *
 */
@XmlRootElement
public class Dates {
private Date dt;

public Date getDt() {
	return dt;
}

public void setDt(Date dt) {
	this.dt = dt;
}



}
