package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity

public class CommodityCode {
	@Column(name="commodity_cd")
		private String commodityCd;
	@Column(name="commodity_desc_txt")
	private String commodityDescTxt;
	@Column(name="categoryCd")
	private String categoryCd;
	public String getCommodityCd() {
		return commodityCd;
	}
	public void setCommodityCd(String commodityCd) {
		this.commodityCd = commodityCd;
	}
	public String getCommodityDescTxt() {
		return commodityDescTxt;
	}
	public void setCommodityDescTxt(String commodityDescTxt) {
		this.commodityDescTxt = commodityDescTxt;
	}
	public String getCategoryCd() {
		return categoryCd;
	}
	public void setCategoryCd(String categoryCd) {
		this.categoryCd = categoryCd;
	}
	
}
