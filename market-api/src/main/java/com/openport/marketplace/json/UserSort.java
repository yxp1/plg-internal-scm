package com.openport.marketplace.json;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class UserSort {
private String colName;
private String parameterId;
private Integer priority;

public Integer getPriority() {
	return priority;
}

public void setPriority(Integer priority) {
	this.priority = priority;
}


public String getParameterId() {
	return parameterId;
}

public void setParameterId(String parameterId) {
	this.parameterId = parameterId;
}

public String getColName() {
	return colName;
}

public void setColName(String colName) {
	this.colName = colName;
}

@Override
public String toString() {
	return "UserSort [colName=" + colName + ", parameterId=" + parameterId + ", priority=" + priority + "]";
}


}
