/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;





/**
 * @author oscar_2
 *
 */
@XmlRootElement
@Entity
public class CarrierBidStats {

	@Column(name="my_Bids_Count")
	private int myBidsCount;
	
	@Column(name="my_Bids_Active_Count")
	private int myBidsActiveCount;
	
	@Column(name="my_Bids_Won_Count")
	private int myBidsWonCount;
	
	@Column(name="active_Shipments_Count")
	private int activeShipmentsCount;
	
	
	public int getMyBidsCount() {
		return myBidsCount;
	}
	public void setMyBidsCount(int myBidsCount) {
		this.myBidsCount = myBidsCount;
	}
	public int getMyBidsActiveCount() {
		return myBidsActiveCount;
	}
	public void setMyBidsActiveCount(int myBidsActiveCount) {
		this.myBidsActiveCount = myBidsActiveCount;
	}
	public int getMyBidsWonCount() {
		return myBidsWonCount;
	}
	public void setMyBidsWonCount(int myBidsWonCount) {
		this.myBidsWonCount = myBidsWonCount;
	}
	public int getActiveShipmentsCount() {
		return activeShipmentsCount;
	}
	public void setActiveShipmentsCount(int activeShipmentsCount) {
		this.activeShipmentsCount = activeShipmentsCount;
	}

	

}
