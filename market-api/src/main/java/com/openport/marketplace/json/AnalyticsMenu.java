/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Nargel
 *
 */


		
@XmlRootElement
@Entity
public class AnalyticsMenu {

	@Column(name="partner_cd")
	private String partnerCd;
	
	
	@Column(name="bookmark_id")
	private String bookmarkId;
	
	@Column(name="menu_label_txt")
	private String menuLabelTxt;
	
	@Column(name="report_id")
	private Integer reportId;
	
	@Column(name="report_type_cd")
	private String reportTypeCd;

	public String getPartnerCd() {
		return partnerCd;
	}

	public void setPartnerCd(String partnerCd) {
		this.partnerCd = partnerCd;
	}

	public String getBookmarkId() {
		return bookmarkId;
	}

	public void setBookmarkId(String bookmarkId) {
		this.bookmarkId = bookmarkId;
	}

	public String getMenuLabelTxt() {
		return menuLabelTxt;
	}

	public void setMenuLabelTxt(String menuLabelTxt) {
		this.menuLabelTxt = menuLabelTxt;
	}

	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public String getReportTypeCd() {
		return reportTypeCd;
	}

	public void setReportTypeCd(String reportTypeCd) {
		this.reportTypeCd = reportTypeCd;
	}
	
	
	
}
