package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class ServiceAreaProfile {
@Column(name="service_area_name")	
private String name;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public ServiceAreaProfile() {
}

}
