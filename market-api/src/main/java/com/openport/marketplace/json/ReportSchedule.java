package com.openport.marketplace.json;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class ReportSchedule {
	@Column(name="report_name")
	private String reportName;
	
	@Column(name="report_owner_user_id")
	private String reportOwnerUserId;
	
	@Column(name="report_sql_txt")
	private String reportSqlTxt;
	
	@Column(name="delivery_format_cd")
	private String deliveryFormatCd;
	
	@Column(name="recipient_addr")
	private String recipientAddr;
	
	@Column(name="status_cd")
	private String statusCd;
	
	@Column(name="start_dt")
	private Date startDt;
	
	@Column(name="start_tm")
	private Date startSchedTm;
	
	@Column(name="last_run_dt")
	private Date lastRunDt;
	
	@Column(name="next_run_dt")
	private Date nextRunDt;
	
	@Column(name="last_run_status_cd")
	private String lastRunStatusCd;
	
	@Column(name="last_run_status_txt")
	private String lastRunStatusTxt;
	
	@Column(name="run_weekly_flg")
	private Character runWeeklyFlg;
	
	@Column(name="run_mon_flg")
	private String runMonFlg;
	
	@Column(name="run_tue_flg")
	private String runTueFlg;
	
	@Column(name="run_wed_flg")
	private String runWedFlg;
	
	@Column(name="run_thu_flg")
	private String runThuFlg;
	
	@Column(name="run_fri_flg")
	private String runFriFlg;
	
	@Column(name="run_sat_flg")
	private String runSatFlg;
	
	@Column(name="run_sun_flg")
	private String runSunFlg;
	
	@Column(name="run_monthly_flg")
	private Character runMonthlyFlg;
	
	@Column(name="date_of_month_value")
	private Integer dateOfMonthValue;
	
	@Column(name="monthly_freq_value")
	private String monthlyFreqValue;
	
	
	
	@Column(name="monthly_day_of_week_value")
	private String monthlyDayOfWeekValue;
	
	@Column(name="create_tstamp")
	private Date createTstamp;
	
	@Column(name="update_tstamp")
	private Date updateTstamp;
	
	@Column(name="report_output_format")
	private String reportOutputFormat;
	
	@Column(name="time_zone_country")
	private String timeZoneCountry;
	
	@Column(name="time_zone_offset")
	private Double timeZoneOffset;

	//denormalized
	private String startTm;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportOwnerUserId() {
		return reportOwnerUserId;
	}

	public void setReportOwnerUserId(String reportOwnerUserId) {
		this.reportOwnerUserId = reportOwnerUserId;
	}

	public String getReportSqlTxt() {
		return reportSqlTxt;
	}

	public void setReportSqlTxt(String reportSqlTxt) {
		this.reportSqlTxt = reportSqlTxt;
	}

	public String getDeliveryFormatCd() {
		return deliveryFormatCd;
	}

	public void setDeliveryFormatCd(String deliveryFormatCd) {
		this.deliveryFormatCd = deliveryFormatCd;
	}

	public String getRecipientAddr() {
		return recipientAddr;
	}

	public void setRecipientAddr(String recipientAddr) {
		this.recipientAddr = recipientAddr;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public Date getStartDt() {
		return startDt;
	}

	public void setStartDt(Date startDt) {
		this.startDt = startDt;
	}

	public Date getStartSchedTm() {
		return startSchedTm;
	}

	public void setStartSchedTm(Date startSchedTm) {
		this.startSchedTm = startSchedTm;
	}

	public Date getLastRunDt() {
		return lastRunDt;
	}

	public void setLastRunDt(Date lastRunDt) {
		this.lastRunDt = lastRunDt;
	}

	public Date getNextRunDt() {
		return nextRunDt;
	}

	public void setNextRunDt(Date nextRunDt) {
		this.nextRunDt = nextRunDt;
	}

	public String getLastRunStatusCd() {
		return lastRunStatusCd;
	}

	public void setLastRunStatusCd(String lastRunStatusCd) {
		this.lastRunStatusCd = lastRunStatusCd;
	}

	public String getLastRunStatusTxt() {
		return lastRunStatusTxt;
	}

	public void setLastRunStatusTxt(String lastRunStatusTxt) {
		this.lastRunStatusTxt = lastRunStatusTxt;
	}

	public Character getRunWeeklyFlg() {
		return runWeeklyFlg;
	}

	public void setRunWeeklyFlg(Character runWeeklyFlg) {
		this.runWeeklyFlg = runWeeklyFlg;
	}

	public String getRunMonFlg() {
		return runMonFlg;
	}

	public void setRunMonFlg(String runMonFlg) {
		this.runMonFlg = runMonFlg;
	}

	public String getRunTueFlg() {
		return runTueFlg;
	}

	public void setRunTueFlg(String runTueFlg) {
		this.runTueFlg = runTueFlg;
	}

	public String getRunWedFlg() {
		return runWedFlg;
	}

	public void setRunWedFlg(String runWedFlg) {
		this.runWedFlg = runWedFlg;
	}

	public String getRunThuFlg() {
		return runThuFlg;
	}

	public void setRunThuFlg(String runThuFlg) {
		this.runThuFlg = runThuFlg;
	}

	public String getRunFriFlg() {
		return runFriFlg;
	}

	public void setRunFriFlg(String runFriFlg) {
		this.runFriFlg = runFriFlg;
	}

	public String getRunSatFlg() {
		return runSatFlg;
	}

	public void setRunSatFlg(String runSatFlg) {
		this.runSatFlg = runSatFlg;
	}

	public String getRunSunFlg() {
		return runSunFlg;
	}

	public void setRunSunFlg(String runSunFlg) {
		this.runSunFlg = runSunFlg;
	}

	public Character getRunMonthlyFlg() {
		return runMonthlyFlg;
	}

	public void setRunMonthlyFlg(Character runMonthlyFlg) {
		this.runMonthlyFlg = runMonthlyFlg;
	}

	public Integer getDateOfMonthValue() {
		return dateOfMonthValue;
	}

	public void setDateOfMonthValue(Integer dateOfMonthValue) {
		this.dateOfMonthValue = dateOfMonthValue;
	}



	public String getMonthlyDayOfWeekValue() {
		return monthlyDayOfWeekValue;
	}

	public void setMonthlyDayOfWeekValue(String monthlyDayOfWeekValue) {
		this.monthlyDayOfWeekValue = monthlyDayOfWeekValue;
	}

	public Date getCreateTstamp() {
		return createTstamp;
	}

	public void setCreateTstamp(Date createTstamp) {
		this.createTstamp = createTstamp;
	}

	public Date getUpdateTstamp() {
		return updateTstamp;
	}

	public void setUpdateTstamp(Date updateTstamp) {
		this.updateTstamp = updateTstamp;
	}

	public String getReportOutputFormat() {
		return reportOutputFormat;
	}

	public void setReportOutputFormat(String reportOutputFormat) {
		this.reportOutputFormat = reportOutputFormat;
	}

	public String getTimeZoneCountry() {
		return timeZoneCountry;
	}

	public void setTimeZoneCountry(String timeZoneCountry) {
		this.timeZoneCountry = timeZoneCountry;
	}

	public Double getTimeZoneOffset() {
		return timeZoneOffset;
	}

	public void setTimeZoneOffset(Double timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}

	public String getStartTm() {
		return startTm;
	}

	public void setStartTm(String startTm) {
		this.startTm = startTm;
	}

	public String getMonthlyFreqValue() {
		return monthlyFreqValue;
	}

	public void setMonthlyFreqValue(String monthlyFreqValue) {
		this.monthlyFreqValue = monthlyFreqValue;
	}
	


	
}
