/**
 * 
 */
package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Relly
 *
 */
@XmlRootElement
public class CompanyServicesByRegion {

	private String 	seqNbr;
	private int 	companyId;
	private String 	companyName;
	private String 	companyCode;
	private String 	serviceAreaName;
	private String 	serviceName;
	private String 	equipmentTypeName;
	private int 	ratingValue;
	
	/**
	 * 
	 */
	public CompanyServicesByRegion() {
		// TODO Auto-generated constructor stub
	}

	public String getSeqNbr() {
		return seqNbr;
	}
	
	public void setSeqNbr(String seqNbr) {
		this.seqNbr = seqNbr;
	}	
	
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getServiceAreaName() {
		return serviceAreaName;
	}

	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}

	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}

	public int getRatingValue() {
		return ratingValue;
	}

	public void setRatingValue(int ratingValue) {
		this.ratingValue = ratingValue;
	}

	
	
}
