/**
 * 
 */
package com.openport.marketplace.json;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author relly
 *
 */
@XmlRootElement
@Entity
public class ShipmentDetail implements Cloneable {

    @Column(name = "shipment_detail_id")
    private Integer shipmentDetailId;

    @Column(name = "shipment_id")
    private Integer shipmentId;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "unit_of_measure")
    private String unitOfMeasure;

    @Column(name = "shipment_wt")
    private Double shipmentWt;

    @Column(name = "wt_uom")
    private String wtUom;

    @Column(name = "shipment_vol")
    private Double shipmentVol;

    @Column(name = "vol_uom")
    private String volUom;

    @Column(name = "shipment_len")
    private Double shipmentLen;

    @Column(name = "shipment_wth")
    private Double shipmentWth;

    @Column(name = "shipment_ht")
    private Double shipmentHt;

    @Column(name = "dimension_uom")
    private String dimensionUom;

    @Column(name = "freight_class_cd")
    private String freightClassCd;

    @Column(name = "is_hazardous")
    private Boolean isHazardous;

    @Column(name = "create_Dt")
    private Timestamp createDt;

    @Column(name = "update_Dt")
    private Timestamp updateDt;

    @Column(name = "product_desc_txt")
    private String productDescTxt;

    @Column(name = "stop_nbr")
    private Integer stopNbr;

    @Column(name = "stop_location_name")
    private String stopLocName;

    @Column(name = "stop_location_addr")
    private String stopAddr;

    @Column(name = "stop_location_city_name")
    private String stopCity;

    @Column(name = "stop_location_province_name")
    private String stopProvince;

    @Column(name = "stop_location_country_cd")
    private String stopCountry;

    @Column(name = "stop_location_postal_cd")
    private String stopPostal;

    @Column(name = "stop_po_nbr")
    private String stopPonbr;

    @Column(name = "stop_release_nbr")
    private String stopRlsnbr;
    
    private String qtyUom;
    
    @Column(name = "product_cd")
    private String productCode;
    
    @Column(name = "unit_price")
    private double unitPrice;
    
    @Column(name = "goods_value_bill_to_cd")
    private String goodsValueBillToCode;
    
    @Transient
    private String actualPoNumber;
    
    @Transient
    private String actualPoReleaseNumber;
    
    @Transient
    private boolean addressUpdated;

    
    public ShipmentDetail() {

    }
    
    
    public ShipmentDetail(ShipmentDetail detail) {
		super();
		this.shipmentDetailId = detail.shipmentDetailId;
		this.shipmentId = detail.shipmentId;
		this.qty = detail.qty;
		this.unitOfMeasure = detail.unitOfMeasure;
		this.shipmentWt = detail.shipmentWt;
		this.wtUom = detail.wtUom;
		this.shipmentVol = detail.shipmentVol;
		this.volUom = detail.volUom;
		this.shipmentLen = detail.shipmentLen;
		this.shipmentWth = detail.shipmentWth;
		this.shipmentHt = detail.shipmentHt;
		this.dimensionUom = detail.dimensionUom;
		this.freightClassCd = detail.freightClassCd;
		this.isHazardous = detail.isHazardous;
		this.createDt = detail.createDt;
		this.updateDt = detail.updateDt;
		this.productDescTxt = detail.productDescTxt;
		this.stopNbr = detail.stopNbr;
		this.stopLocName = detail.stopLocName;
		this.stopAddr = detail.stopAddr;
		this.stopCity = detail.stopCity;
		this.stopProvince = detail.stopProvince;
		this.stopCountry = detail.stopCountry;
		this.stopPostal = detail.stopPostal;
		this.stopPonbr = detail.stopPonbr;
		this.stopRlsnbr = detail.stopRlsnbr;
		this.qtyUom = detail.qtyUom;
		this.productCode = detail.productCode;
		this.actualPoNumber = detail.actualPoNumber;
		this.actualPoReleaseNumber = detail.actualPoReleaseNumber;
		this.addressUpdated = detail.addressUpdated;
		this.unitPrice = detail.unitPrice;
		this.goodsValueBillToCode = detail.goodsValueBillToCode;
	}



	public Integer getShipmentDetailId() {
        return shipmentDetailId;
    }

    public void setShipmentDetailId(Integer shipmentDetailId) {
        this.shipmentDetailId = shipmentDetailId;
    }

    public Integer getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Integer shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public Double getShipmentWt() {
        return shipmentWt;
    }

    public void setShipmentWt(Double shipmentWt) {
        this.shipmentWt = shipmentWt;
    }

    public String getWtUom() {
        return wtUom;
    }

    public void setWtUom(String wtUom) {
        this.wtUom = wtUom;
    }

    public Double getShipmentVol() {
        return shipmentVol;
    }

    public void setShipmentVol(Double shipmentVol) {
        this.shipmentVol = shipmentVol;
    }

    public String getVolUom() {
        return volUom;
    }

    public void setVolUom(String volUom) {
        this.volUom = volUom;
    }

    public Double getShipmentLen() {
        return shipmentLen;
    }

    public void setShipmentLen(Double shipmentLen) {
        this.shipmentLen = shipmentLen;
    }

    public Double getShipmentWth() {
        return shipmentWth;
    }

    public void setShipmentWth(Double shipmentWth) {
        this.shipmentWth = shipmentWth;
    }

    public Double getShipmentHt() {
        return shipmentHt;
    }

    public void setShipmentHt(Double shipmentHt) {
        this.shipmentHt = shipmentHt;
    }

    public String getDimensionUom() {
        return dimensionUom;
    }

    public void setDimensionUom(String dimensionUom) {
        this.dimensionUom = dimensionUom;
    }

    public String getFreightClassCd() {
        return freightClassCd;
    }

    public void setFreightClassCd(String freightClassCd) {
        this.freightClassCd = freightClassCd;
    }

    public Boolean getIsHazardous() {
        return isHazardous;
    }

    public void setIsHazardous(Boolean isHazardous) {
        this.isHazardous = isHazardous;
    }

    public Timestamp getCreateDt() {
        return createDt;
    }

    public void setCreateDt(Timestamp createDt) {
        this.createDt = createDt;
    }

    public Timestamp getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(Timestamp updateDt) {
        this.updateDt = updateDt;
    }

    public String getProductDescTxt() {
        return productDescTxt;
    }

    public void setProductDescTxt(String productDescTxt) {
        this.productDescTxt = productDescTxt;
    }

    public Integer getStopNbr() {
        return stopNbr;
    }

    public void setStopNbr(Integer stopNbr) {
        this.stopNbr = stopNbr;
    }

    public String getStopLocName() {
        return stopLocName;
    }

    public void setStopLocName(String stopLocName) {
        this.stopLocName = stopLocName;
    }

    public String getStopAddr() {
        return stopAddr;
    }

    public void setStopAddr(String stopAddr) {
        this.stopAddr = stopAddr;
    }

    public String getStopCity() {
        return stopCity;
    }

    public void setStopCity(String stopCity) {
        this.stopCity = stopCity;
    }

    public String getStopProvince() {
        return stopProvince;
    }

    public void setStopProvince(String stopProvince) {
        this.stopProvince = stopProvince;
    }

    public String getStopCountry() {
        return stopCountry;
    }

    public void setStopCountry(String stopCountry) {
        this.stopCountry = stopCountry;
    }

    public String getStopPostal() {
        return stopPostal;
    }

    public void setStopPostal(String stopPostal) {
        this.stopPostal = stopPostal;
    }

    public String getStopPonbr() {
        return stopPonbr;
    }

    public void setStopPonbr(String stopPonbr) {
        this.stopPonbr = stopPonbr;
    }

    public String getStopRlsnbr() {
        return stopRlsnbr;
    }

    public void setStopRlsnbr(String stopRlsnbr) {
        this.stopRlsnbr = stopRlsnbr;
    }

    public String getQtyUom() {
        return qtyUom;
    }

    public void setQtyUom(String qtyUom) {
        this.qtyUom = qtyUom;
    }
    
    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    
    public String getActualPoNumber() {
        return actualPoNumber;
    }

    public void setActualPoNumber(String actualPoNumber) {
        this.actualPoNumber = actualPoNumber;
    }

    public String getActualPoReleaseNumber() {
        return actualPoReleaseNumber;
    }

    public void setActualPoReleaseNumber(String actualPoReleaseNumber) {
        this.actualPoReleaseNumber = actualPoReleaseNumber;
    }

	public boolean isAddressUpdated() {
		return addressUpdated;
	}

	public void setAddressUpdated(boolean addressUpdated) {
		this.addressUpdated = addressUpdated;
	}
	
	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public String getGoodsValueBillToCode() {
		return goodsValueBillToCode;
	}


	public void setGoodsValueBillToCode(String goodsValueBillToCode) {
		this.goodsValueBillToCode = goodsValueBillToCode;
	}

	@Override
	public String toString() {
		return "ShipmentDetail [shipmentDetailId=" + shipmentDetailId + ", shipmentId=" + shipmentId + ", qty=" + qty
				+ ", unitOfMeasure=" + unitOfMeasure + ", shipmentWt=" + shipmentWt + ", wtUom=" + wtUom
				+ ", shipmentVol=" + shipmentVol + ", volUom=" + volUom + ", shipmentLen=" + shipmentLen
				+ ", shipmentWth=" + shipmentWth + ", shipmentHt=" + shipmentHt + ", dimensionUom=" + dimensionUom
				+ ", freightClassCd=" + freightClassCd + ", isHazardous=" + isHazardous + ", createDt=" + createDt
				+ ", updateDt=" + updateDt + ", productDescTxt=" + productDescTxt + ", stopNbr=" + stopNbr
				+ ", stopLocName=" + stopLocName + ", stopAddr=" + stopAddr + ", stopCity=" + stopCity
				+ ", stopProvince=" + stopProvince + ", stopCountry=" + stopCountry + ", stopPostal=" + stopPostal
				+ ", stopPonbr=" + stopPonbr + ", stopRlsnbr=" + stopRlsnbr + ", qtyUom=" + qtyUom + ", productCode="
				+ productCode + ", actualPoNumber=" + actualPoNumber + ", actualPoReleaseNumber="
				+ actualPoReleaseNumber + ", addressUpdated=" + addressUpdated + ", unitPrice=" + unitPrice
				+ ", goodsValueBillToCode=" + goodsValueBillToCode
				+ "]";
	}

}
