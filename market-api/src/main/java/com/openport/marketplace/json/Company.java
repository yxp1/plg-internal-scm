/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class Company {

	@Column(name="company_Id")
	private Integer companyId;
	@Column(name="company_Name")
	private String companyName;
	@Column(name="is_Carrier")
	private Boolean isCarrier;
	@Column(name="is_Shipper")
	private Boolean isShipper;
	@Column(name="company_Code")
	private String companyCode;
	@Column(name="corporate_Address_Id")
	private Integer corporateAddressId;
	@Column(name="years_Of_Operation")
	private Integer yearsOfOperation;
	@Column(name="business_Headline")
	private String businessHeadline;
	@Column(name="business_Description")
	private String businessDescription;
	@Column(name="create_User_Id")
	private Integer createUserId;
	@Column(name="create_Dt")
	private Timestamp createDt;
	@Column(name="update_User_Id")
	private Integer updateUserId;
	@Column(name="update_Dt")
	private Timestamp updateDt;
	@Column(name="wizard_Step_Nbr")
	private Integer wizardStepNbr;
	@Column(name="has_pod")
	private Boolean hasPod;
	@Column(name="rating_value")
	private Integer ratingValue;
	@Column(name="TIMEZONE_ID")
	private String timezoneId;
	@Column(name="currency_cd")
	private String currencyCd;


	// Denormalized for JSON - start
	private CompanyAddress corporateAddress;
/*
	@Column(name="truck_Pool_Address_Id")
	private Integer truckPoolAddressId;
	@Column(name="branch_Address_Id")
	private Integer branchAddressId;
	private CompanyAddress truckPoolAddress;
	private CompanyAddress branchAddress;
	*/
	private List<CompanyBranch> companyBranches;
	private List<Service> services;
	private List<ServiceArea> serviceAreas;
	private List<Equipment> equipments;
	private List<CompanyClient> clients;
	private List<FileDocumentType> fileDocTypes;
	private List<CompanyUser> companyUsers;
	
	private List<User> users;
	
	
	private List<CompanyService> companyServices;
	private List<CarrierRegion> carrierRegions;
	private List<CompanyEquipment> companyEquipments;
	// Denormalized for JSON - end
	
	private String countryCd;
	private int status;
	private String message;
	
	
	private List<AnalyticsMenu> analytics;
	
	
	
	public List<AnalyticsMenu> getAnalytics() {
		return analytics;
	}
	public void setAnalytics(List<AnalyticsMenu> analytics) {
		this.analytics = analytics;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCountryCd() {
		return countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public String getTimezoneId() {
		return timezoneId;
	}
	public void setTimezoneId(String timezoneId) {
		this.timezoneId = timezoneId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Boolean getIsCarrier() {
		return isCarrier;
	}
	public Boolean isCarrier() {
		return isCarrier;
	}
	public void setIsCarrier(Boolean isCarrier) {
		this.isCarrier = isCarrier;
	}
	public void isCarrier(Boolean isCarrier) {
		this.isCarrier = isCarrier;
	}
	public Boolean getIsShipper() {
		return isShipper;
	}
	public Boolean isShipper() {
		return isShipper;
	}
	public void setIsShipper(Boolean isShipper) {
		this.isShipper = isShipper;
	}
	public void isShipper(Boolean isShipper) {
		this.isShipper = isShipper;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public Integer getCorporateAddressId() {
		return corporateAddressId;
	}
	public void setCorporateAddressId(Integer corporateAddressId) {
		this.corporateAddressId = corporateAddressId;
	}
	public Integer getYearsOfOperation() {
		return yearsOfOperation;
	}
	public void setYearsOfOperation(Integer yearsOfOperation) {
		this.yearsOfOperation = yearsOfOperation;
	}
	public String getBusinessHeadline() {
		return businessHeadline;
	}
	public void setBusinessHeadline(String businessHeadline) {
		this.businessHeadline = businessHeadline;
	}
	public String getBusinessDescription() {
		return businessDescription;
	}
	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}
	public Integer getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}
	public Timestamp getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}
	public Integer getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Timestamp getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}
	public Integer getWizardStepNbr() {
		return wizardStepNbr;
	}
	public void setWizardStepNbr(Integer wizardStepNbr) {
		this.wizardStepNbr = wizardStepNbr;
	}
	public CompanyAddress getCorporateAddress() {
		return corporateAddress;
	}
	public void setCorporateAddress(CompanyAddress corporateAddress) {
		this.corporateAddress = corporateAddress;
	}
	public List<Service> getServices() {
		return services;
	}
	public void setServices(List<Service> services) {
		this.services = services;
	}
	public List<ServiceArea> getServiceAreas() {
		return serviceAreas;
	}
	public void setServiceAreas(List<ServiceArea> serviceAreas) {
		this.serviceAreas = serviceAreas;
	}
	public List<Equipment> getEquipments() {
		return equipments;
	}
	public void setEquipments(List<Equipment> equipments) {
		this.equipments = equipments;
	}
	public List<CompanyClient> getClients() {
		return clients;
	}
	public void setClients(List<CompanyClient> clients) {
		this.clients = clients;
	}
	public List<FileDocumentType> getFileDocTypes() {
		return fileDocTypes;
	}
	public void setFileDocTypes(List<FileDocumentType> fileDocTypes) {
		this.fileDocTypes = fileDocTypes;
	}
	public List<CompanyUser> getCompanyUsers() {
		return companyUsers;
	}
	public void setCompanyUsers(List<CompanyUser> companyUsers) {
		this.companyUsers = companyUsers;
	}
	
	public List<CarrierRegion> getCarrierRegions() {
		return carrierRegions;
	}
	public void setCarrierRegions(List<CarrierRegion> carrierRegions) {
		this.carrierRegions = carrierRegions;
	}
	public List<CompanyService> getCompanyServices() {
		return companyServices;
	}
	public List<CompanyBranch> getCompanyBranches() {
		return companyBranches;
	}
	public void setCompanyBranches(List<CompanyBranch> companyBranches) {
		this.companyBranches = companyBranches;
	}

	public void setCompanyServices(List<CompanyService> companyServices) {
		this.companyServices = companyServices;
	}
	public List<CompanyEquipment> getCompanyEquipments() {
		return companyEquipments;
	}
	public void setCompanyEquipments(List<CompanyEquipment> companyEquipments) {
		this.companyEquipments = companyEquipments;
	}
	public Boolean getHasPod() {
		return hasPod;
	}
	public void setHasPod(Boolean hasPod) {
		this.hasPod = hasPod;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public Integer getRatingValue() {
		return ratingValue;
	}
	public void setRatingValue(Integer ratingValue) {
		this.ratingValue = ratingValue;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	
	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", companyName=" + companyName + ", isCarrier=" + isCarrier
				+ ", isShipper=" + isShipper + ", companyCode=" + companyCode + ", corporateAddressId="
				+ corporateAddressId + ", yearsOfOperation=" + yearsOfOperation + ", businessHeadline="
				+ businessHeadline + ", businessDescription=" + businessDescription + ", createUserId=" + createUserId
				+ ", createDt=" + createDt + ", updateUserId=" + updateUserId + ", updateDt=" + updateDt
				+ ", wizardStepNbr=" + wizardStepNbr + ", hasPod=" + hasPod + ", ratingValue=" + ratingValue
				+ ", timezoneId=" + timezoneId + ", currencyCd=" + currencyCd + ", corporateAddress=" + corporateAddress
				+ ", companyBranches=" + companyBranches + ", services=" + services + ", serviceAreas=" + serviceAreas
				+ ", equipments=" + equipments + ", clients=" + clients + ", fileDocTypes=" + fileDocTypes
				+ ", companyUsers=" + companyUsers + ", users=" + users + ", companyServices=" + companyServices
				+ ", carrierRegions=" + carrierRegions + ", companyEquipments=" + companyEquipments + ", countryCd="
				+ countryCd + ", status=" + status + ", message=" + message + ", analytics=" + analytics + "]";
	}
	
}
