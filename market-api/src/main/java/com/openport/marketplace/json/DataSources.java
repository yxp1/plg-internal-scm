/**
 * 
 */
package com.openport.marketplace.json;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Nargel
 *
 */
@XmlRootElement
@Entity
public class DataSources implements Serializable{
	@Column(name="report_data_group_name")
	private String reportDataGroupName;
	@Column(name="report_db_source_name")
	private String reportDbSourceName;
	public String getReportDataGroupName() {
		return reportDataGroupName;
	}
	public void setReportDataGroupName(String reportDataGroupName) {
		this.reportDataGroupName = reportDataGroupName;
	}
	public String getReportDbSourceName() {
		return reportDbSourceName;
	}
	public void setReportDbSourceName(String reportDbSourceName) {
		this.reportDbSourceName = reportDbSourceName;
	}
		
	
}
