package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;

@XmlRootElement
@Entity
public class LongHaulReward{
	@Column(name="origin_city")
	private String originCity;
	@Column(name="origin_province")
	private String originProvince;
	@Column(name="origin_postal_cd")
	private String originPostalCd;
	@Column(name="origin_country_cd")
	private String originCountryCd;
	@Column(name="dest_city")
	private String destCity;
	@Column(name="dest_province")
	private String destProvince;
	@Column(name="dest_postal_cd")
	private String destPostalCd;
	@Column(name="dest_country_cd")
	private String destCountryCd;
	@Column(name="distance")
	private double distance;
	@Column(name="distance_uom")
	private String distanceUom;
	@Column(name="duration")
	private double duration;
	@Column(name="duration_uom")
	private String durationUom;
	@Column(name="long_haul_cnt")
	private int longHaulCnt;
	@Column(name="update_user_id")
	private String updateUserId;
	@Column(name="update_dt")
	private Date updateDt;
	
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public String getOriginProvince() {
		return originProvince;
	}
	public void setOriginProvince(String originProvince) {
		this.originProvince = originProvince;
	}
	public String getOriginPostalCd() {
		return originPostalCd;
	}
	public void setOriginPostalCd(String originPostalCd) {
		this.originPostalCd = originPostalCd;
	}
	public String getOriginCountryCd() {
		return originCountryCd;
	}
	public void setOriginCountryCd(String originCountryCd) {
		this.originCountryCd = originCountryCd;
	}
	public String getDestCity() {
		return destCity;
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public String getDestProvince() {
		return destProvince;
	}
	public void setDestProvince(String destProvince) {
		this.destProvince = destProvince;
	}
	public String getDestPostalCd() {
		return destPostalCd;
	}
	public void setDestPostalCd(String destPostalCd) {
		this.destPostalCd = destPostalCd;
	}
	public String getDestCountryCd() {
		return destCountryCd;
	}
	public void setDestCountryCd(String destCountryCd) {
		this.destCountryCd = destCountryCd;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public String getDistanceUom() {
		return distanceUom;
	}
	public void setDistanceUom(String distanceUom) {
		this.distanceUom = distanceUom;
	}
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public String getDurationUom() {
		return durationUom;
	}
	public void setDurationUom(String durationUom) {
		this.durationUom = durationUom;
	}
	public int getLongHaulCnt() {
		return longHaulCnt;
	}
	public void setLongHaulCnt(int longHaulCnt) {
		this.longHaulCnt = longHaulCnt;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}
	
	
	
}