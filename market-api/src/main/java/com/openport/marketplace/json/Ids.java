package com.openport.marketplace.json;

public class Ids {
private Integer companyId;
private Integer corporateAddressId;
public Integer getCompanyId() {
	return companyId;
}
public void setCompanyId(Integer companyId) {
	this.companyId = companyId;
}
public Integer getCorporateAddressId() {
	return corporateAddressId;
}
public void setCorporateAddressId(Integer corporateAddressId) {
	this.corporateAddressId = corporateAddressId;
}

}
