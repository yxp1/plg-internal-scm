/**
 * 
 */
package com.openport.marketplace.json;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;





/**
 * @author oscar_2
 *
 */
@XmlRootElement
public class Vendors {

	private int status;
	private List<Company> companies;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<Company> getCompanies() {
		return companies;
	}
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}
	
	/**
	 * 
	 */
	 

}
