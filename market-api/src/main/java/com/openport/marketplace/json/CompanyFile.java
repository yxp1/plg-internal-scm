/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity	
public class CompanyFile {
	@Column(name="company_file_id")
	private Integer companyFileId;
	@Column(name="company_Id")
	private Integer companyId;
	@Column(name="is_legal_document")
	private boolean isLegalDocument;
	@Column(name="document_name")
	private String documentName;
	@Column(name="relative_path")
	private String relativePath;
	@Column(name="create_user_id")
	private Integer createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private Integer updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;
	private DocumentType documentType;

	
	//denormalize
	private Integer documentIndicator;
	/**
	 * 
	 */
	public CompanyFile() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCompanyFileId() {
		return companyFileId;
	}

	public void setCompanyFileId(Integer companyFileId) {
		this.companyFileId = companyFileId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public boolean isLegalDocument() {
		return isLegalDocument;
	}

	public boolean getIsLegalDocument() {
		return isLegalDocument;
	}

	public void setIsLegalDocument(boolean isLegalDocument) {
		this.isLegalDocument = isLegalDocument;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public Integer getDocumentIndicator() {
		return documentIndicator;
	}

	public void setDocumentIndicator(Integer documentIndicator) {
		this.documentIndicator = documentIndicator;
	}

	public void setLegalDocument(boolean isLegalDocument) {
		this.isLegalDocument = isLegalDocument;
	}


	

}
