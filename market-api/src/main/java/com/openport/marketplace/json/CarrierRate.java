package com.openport.marketplace.json;

import java.sql.Timestamp;
import java.util.List;

public class CarrierRate {

	private String poNbr;
	private String releaseNbr;
	private String proBillNbr;
	private String parentPartnerCd;
	private String partnerCd;
	private String shipperCd;
	private Integer transportLegNbr;
	private String chargeTypeCd;
	private String carrierCd;
	private Integer carrierSeqNbr;	
	private Boolean sent204Flg;
	private Boolean accept990Flg;
	private Boolean reject990Flg;
	private Boolean cancelFlg;
	private Boolean brFlg;
	private String accessorialCarrierCd;
	private Integer accessorialCarrierSeqNbr;
	private String accessorialChargeCd;
	private String accessorialChargeDescTxt;
	private Double accessorialChargeAmt;
	private String accessorialChargeCcyCd;
	private String accessorialChargeTypeCd;
	private Integer accessorialLineItemNbr;
	private List<CarrierAccessorial> accessorials;
	private Integer equipmentShipmentSeqNbr;
	private String deliveryCityName;
	private String deliveryState;
	private String deliveryCountry;
	private String pickupCityName;
	private String pickupState;
	private String pickupCountry;
	private Double totalWt;
	private String wtUom;
	private String customerCd;
	private String weight;
	private Double rtWt;
	private Double totalBase;
	private Double totalDiscount;
	private Double totalAccessorial;
	private Double totalCharge;
	private String userId;
	private String equipmentTypeCd;
	private Integer transportLegId;
	private Double exchangeRt;
	private Double totalFsc;
	
	private Double costTotalCharge;
	private Double costTotalRevenue;
	private Timestamp sent204Tstamp;
	private Timestamp cancelTstamp;
	
	private Boolean isSent204Flg;
	private Boolean isAccept990Flg;
	private Boolean isReject990Flg;
	private Boolean isCancelFlg;
	private Boolean isBrFlg;
	private String poTypeCd;
	private String currencyCd;
	private Boolean isNewCarrierRate;
	
	
	public Boolean getIsNewCarrierRate() {
		return isNewCarrierRate;
	}
	public void setIsNewCarrierRate(Boolean isNewCarrierRate) {
		this.isNewCarrierRate = isNewCarrierRate;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public String getPoTypeCd() {
		return poTypeCd;
	}
	public void setPoTypeCd(String poTypeCd) {
		this.poTypeCd = poTypeCd;
	}
	public Boolean getIsSent204Flg() {
		return isSent204Flg;
	}
	public void setIsSent204Flg(Boolean isSent204Flg) {
		this.isSent204Flg = isSent204Flg;
	}

	public Boolean getIsCancelFlg() {
		return isCancelFlg;
	}
	public void setIsCancelFlg(Boolean isCanceFlg) {
		this.isCancelFlg = isCanceFlg;
	}
	public Boolean getIsBrFlg() {
		return isBrFlg;
	}
	public void setIsBrFlg(Boolean isBrFlg) {
		this.isBrFlg = isBrFlg;
	}
	public Timestamp getSent204Tstamp() {
		return sent204Tstamp;
	}
	public void setSent204Tstamp(Timestamp sent204Tstamp) {
		this.sent204Tstamp = sent204Tstamp;
	}
	public Timestamp getCancelTstamp() {
		return cancelTstamp;
	}
	public void setCancelTstamp(Timestamp cancelTstamp) {
		this.cancelTstamp = cancelTstamp;
	}
	public Double getCostTotalCharge() {
		return costTotalCharge;
	}
	public void setCostTotalCharge(Double costTotalCharge) {
		this.costTotalCharge = costTotalCharge;
	}
	public Double getCostTotalRevenue() {
		return costTotalRevenue;
	}
	public void setCostTotalRevenue(Double costTotalRevenue) {
		this.costTotalRevenue = costTotalRevenue;
	}
	public Double getTotalFsc() {
		return totalFsc;
	}
	public void setTotalFsc(Double totalFsc) {
		this.totalFsc = totalFsc;
	}
	public String getEquipmentTypeCd() {
		return equipmentTypeCd;
	}
	public void setEquipmentTypeCd(String equipmentTypeCd) {
		this.equipmentTypeCd = equipmentTypeCd;
	}
	public Integer getTransportLegId() {
		return transportLegId;
	}
	public void setTransportLegId(Integer transportLegId) {
		this.transportLegId = transportLegId;
	}
	public Double getExchangeRt() {
		return exchangeRt;
	}
	public void setExchangeRt(Double exchangeRt) {
		this.exchangeRt = exchangeRt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Double getTotalBase() {
		return totalBase;
	}
	public void setTotalBase(Double totalBase) {
		this.totalBase = totalBase;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Double getTotalAccessorial() {
		return totalAccessorial;
	}
	public void setTotalAccessorial(Double totalAccessorial) {
		this.totalAccessorial = totalAccessorial;
	}
	public Double getTotalCharge() {
		return totalCharge;
	}
	public void setTotalCharge(Double totalCharge) {
		this.totalCharge = totalCharge;
	}
	
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public Double getRtWt() {
		return rtWt;
	}
	public void setRtWt(Double rtWt) {
		this.rtWt = rtWt;
	}
	public String getCustomerCd() {
		return customerCd;
	}
	public void setCustomerCd(String customerCd) {
		this.customerCd = customerCd;
	}
	public Integer getEquipmentShipmentSeqNbr() {
		return equipmentShipmentSeqNbr;
	}
	public void setEquipmentShipmentSeqNbr(Integer equipmentShipmentSeqNbr) {
		this.equipmentShipmentSeqNbr = equipmentShipmentSeqNbr;
	}
	public String getDeliveryCityName() {
		return deliveryCityName;
	}
	public void setDeliveryCityName(String deliveryCityName) {
		this.deliveryCityName = deliveryCityName;
	}
	public String getDeliveryState() {
		return deliveryState;
	}
	public void setDeliveryState(String deliveryState) {
		this.deliveryState = deliveryState;
	}
	public String getDeliveryCountry() {
		return deliveryCountry;
	}
	public void setDeliveryCountry(String deliveryCountry) {
		this.deliveryCountry = deliveryCountry;
	}
	public String getPickupCityName() {
		return pickupCityName;
	}
	public void setPickupCityName(String pickupCityName) {
		this.pickupCityName = pickupCityName;
	}
	public String getPickupState() {
		return pickupState;
	}
	public void setPickupState(String pickupState) {
		this.pickupState = pickupState;
	}
	public String getPickupCountry() {
		return pickupCountry;
	}
	public void setPickupCountry(String pickupCountry) {
		this.pickupCountry = pickupCountry;
	}
	public Double getTotalWt() {
		return totalWt;
	}
	public void setTotalWt(Double totalWt) {
		this.totalWt = totalWt;
	}
	public String getWtUom() {
		return wtUom;
	}
	public void setWtUom(String wtUom) {
		this.wtUom = wtUom;
	}
	public Boolean getCancelFlg() {
		return cancelFlg;
	}
	public void setCancelFlg(Boolean cancelFlg) {
		this.cancelFlg = cancelFlg;
	}
	public String getProBillNbr() {
		return proBillNbr;
	}
	public void setProBillNbr(String proBillNbr) {
		this.proBillNbr = proBillNbr;
	}
	public String getPoNbr() {
		return poNbr;
	}
	public void setPoNbr(String poNbr) {
		this.poNbr = poNbr;
	}
	public String getReleaseNbr() {
		return releaseNbr;
	}
	public void setReleaseNbr(String releaseNbr) {
		this.releaseNbr = releaseNbr;
	}
	public String getParentPartnerCd() {
		return parentPartnerCd;
	}
	public void setParentPartnerCd(String parentPartnerCd) {
		this.parentPartnerCd = parentPartnerCd;
	}
	public String getPartnerCd() {
		return partnerCd;
	}
	public void setPartnerCd(String partnerCd) {
		this.partnerCd = partnerCd;
	}
	public String getShipperCd() {
		return shipperCd;
	}
	public void setShipperCd(String shipperCd) {
		this.shipperCd = shipperCd;
	}
	public Integer getTransportLegNbr() {
		return transportLegNbr;
	}
	public void setTransportLegNbr(Integer transportLegNbr) {
		this.transportLegNbr = transportLegNbr;
	}
	public String getChargeTypeCd() {
		return chargeTypeCd;
	}
	public void setChargeTypeCd(String chargeTypeCd) {
		this.chargeTypeCd = chargeTypeCd;
	}
	public String getCarrierCd() {
		return carrierCd;
	}
	public void setCarrierCd(String carrierCd) {
		this.carrierCd = carrierCd;
	}
	public Integer getCarrierSeqNbr() {
		return carrierSeqNbr;
	}
	public void setCarrierSeqNbr(Integer carrierSeqNbr) {
		this.carrierSeqNbr = carrierSeqNbr;
	}
	public Boolean getSent204Flg() {
		return sent204Flg;
	}
	public void setSent204Flg(Boolean sent204Flg) {
		this.sent204Flg = sent204Flg;
	}
	
	public Boolean getReject990Flg() {
		return reject990Flg;
	}
	public void setReject990Flg(Boolean reject990Flg) {
		this.reject990Flg = reject990Flg;
	}
	public Boolean getBrFlg() {
		return brFlg;
	}
	public void setBrFlg(Boolean brFlg) {
		this.brFlg = brFlg;
	}
	public String getAccessorialCarrierCd() {
		return accessorialCarrierCd;
	}
	public void setAccessorialCarrierCd(String accessorialCarrierCd) {
		this.accessorialCarrierCd = accessorialCarrierCd;
	}
	public Integer getAccessorialCarrierSeqNbr() {
		return accessorialCarrierSeqNbr;
	}
	public void setAccessorialCarrierSeqNbr(Integer accessorialCarrierSeqNbr) {
		this.accessorialCarrierSeqNbr = accessorialCarrierSeqNbr;
	}
	public String getAccessorialChargeCd() {
		return accessorialChargeCd;
	}
	public void setAccessorialChargeCd(String accessorialChargeCd) {
		this.accessorialChargeCd = accessorialChargeCd;
	}
	public String getAccessorialChargeDescTxt() {
		return accessorialChargeDescTxt;
	}
	public void setAccessorialChargeDescTxt(String accessorialChargeDescTxt) {
		this.accessorialChargeDescTxt = accessorialChargeDescTxt;
	}
	public Double getAccessorialChargeAmt() {
		return accessorialChargeAmt;
	}
	public void setAccessorialChargeAmt(Double accessorialChargeAmt) {
		this.accessorialChargeAmt = accessorialChargeAmt;
	}
	public String getAccessorialChargeCcyCd() {
		return accessorialChargeCcyCd;
	}
	public void setAccessorialChargeCcyCd(String accessorialChargeCcyCd) {
		this.accessorialChargeCcyCd = accessorialChargeCcyCd;
	}
	public String getAccessorialChargeTypeCd() {
		return accessorialChargeTypeCd;
	}
	public void setAccessorialChargeTypeCd(String accessorialChargeTypeCd) {
		this.accessorialChargeTypeCd = accessorialChargeTypeCd;
	}
	public Integer getAccessorialLineItemNbr() {
		return accessorialLineItemNbr;
	}
	public void setAccessorialLineItemNbr(Integer accessorialLineItemNbr) {
		this.accessorialLineItemNbr = accessorialLineItemNbr;
	}
	public List<CarrierAccessorial> getAccessorials() {
		return accessorials;
	}
	public void setAccessorials(List<CarrierAccessorial> accessorials) {
		this.accessorials = accessorials;
	}
	public Boolean getAccept990Flg() {
		return accept990Flg;
	}
	public void setAccept990Flg(Boolean accept990Flg) {
		this.accept990Flg = accept990Flg;
	}
	public Boolean getIsAccept990Flg() {
		return isAccept990Flg;
	}
	public void setIsAccept990Flg(Boolean isAccept990Flg) {
		this.isAccept990Flg = isAccept990Flg;
	}
	public Boolean getIsReject990Flg() {
		return isReject990Flg;
	}
	public void setIsReject990Flg(Boolean isReject990Flg) {
		this.isReject990Flg = isReject990Flg;
	}
	@Override
	public String toString() {
		return "CarrierRate [poNbr=" + poNbr + ", releaseNbr=" + releaseNbr + ", proBillNbr=" + proBillNbr
				+ ", parentPartnerCd=" + parentPartnerCd + ", partnerCd=" + partnerCd + ", shipperCd=" + shipperCd
				+ ", transportLegNbr=" + transportLegNbr + ", chargeTypeCd=" + chargeTypeCd + ", carrierCd=" + carrierCd
				+ ", carrierSeqNbr=" + carrierSeqNbr + ", sent204Flg=" + sent204Flg + ", accept990Flg=" + accept990Flg
				+ ", reject990Flg=" + reject990Flg + ", cancelFlg=" + cancelFlg + ", brFlg=" + brFlg
				+ ", accessorialCarrierCd=" + accessorialCarrierCd + ", accessorialCarrierSeqNbr="
				+ accessorialCarrierSeqNbr + ", accessorialChargeCd=" + accessorialChargeCd
				+ ", accessorialChargeDescTxt=" + accessorialChargeDescTxt + ", accessorialChargeAmt="
				+ accessorialChargeAmt + ", accessorialChargeCcyCd=" + accessorialChargeCcyCd
				+ ", accessorialChargeTypeCd=" + accessorialChargeTypeCd + ", accessorialLineItemNbr="
				+ accessorialLineItemNbr + ", accessorials=" + accessorials + ", equipmentShipmentSeqNbr="
				+ equipmentShipmentSeqNbr + ", deliveryCityName=" + deliveryCityName + ", deliveryState="
				+ deliveryState + ", deliveryCountry=" + deliveryCountry + ", pickupCityName=" + pickupCityName
				+ ", pickupState=" + pickupState + ", pickupCountry=" + pickupCountry + ", totalWt=" + totalWt
				+ ", wtUom=" + wtUom + ", customerCd=" + customerCd + ", weight=" + weight + ", rtWt=" + rtWt
				+ ", totalBase=" + totalBase + ", totalDiscount=" + totalDiscount + ", totalAccessorial="
				+ totalAccessorial + ", totalCharge=" + totalCharge + ", userId=" + userId + ", equipmentTypeCd="
				+ equipmentTypeCd + ", transportLegId=" + transportLegId + ", exchangeRt=" + exchangeRt + ", totalFsc="
				+ totalFsc + ", costTotalCharge=" + costTotalCharge + ", costTotalRevenue=" + costTotalRevenue
				+ ", sent204Tstamp=" + sent204Tstamp + ", cancelTstamp=" + cancelTstamp + ", isSent204Flg="
				+ isSent204Flg + ", isAccept990Flg=" + isAccept990Flg + ", isReject990Flg=" + isReject990Flg
				+ ", isCancelFlg=" + isCancelFlg + ", isBrFlg=" + isBrFlg + ", poTypeCd=" + poTypeCd + ", currencyCd="
				+ currencyCd + ", isNewCarrierRate=" + isNewCarrierRate + "]";
	}
	
	





}
