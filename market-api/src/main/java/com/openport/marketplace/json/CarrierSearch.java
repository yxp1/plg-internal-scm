/**
 * 
 */
package com.openport.marketplace.json;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Relly
 *
 */
@XmlRootElement
public class CarrierSearch {

	private ArrayList<DropDownList> areaList;
	private ArrayList<DropDownList> serviceList;
	private ArrayList<DropDownList> equipmentList;
	
	public CarrierSearch() {
	}

	public ArrayList<DropDownList> getAreaList() {
		return areaList;
	}

	public void setAreaList(ArrayList<DropDownList> areaList) {
		this.areaList = areaList;
	}

	public ArrayList<DropDownList> getServiceList() {
		return serviceList;
	}

	public void setServiceList(ArrayList<DropDownList> serviceList) {
		this.serviceList = serviceList;
	}

	public ArrayList<DropDownList> getEquipmentList() {
		return equipmentList;
	}

	public void setEquipmentList(ArrayList<DropDownList> equipmentList) {
		this.equipmentList = equipmentList;
	}

}
