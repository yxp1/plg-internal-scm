/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity	
public class CarrierRegion {
	@Column(name="carrier_region_id")
	private Integer carrierRegionId;
	@Column(name="company_Id")
	private Integer companyId;
	@Column(name="service_area_id")
	private Integer serviceAreaId;
	@Column(name="create_user_id")
	private Integer createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private Integer updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;

	private ServiceArea serviceArea;

	/**
	 * 
	 */
	public CarrierRegion() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCarrierRegionId() {
		return carrierRegionId;
	}

	public void setCarrierRegionId(Integer carrierRegionId) {
		this.carrierRegionId = carrierRegionId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getServiceAreaId() {
		return serviceAreaId;
	}

	public void setServiceAreaId(Integer serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public ServiceArea getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(ServiceArea serviceArea) {
		this.serviceArea = serviceArea;
	}



	

}
