/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity	
public class CompanyCertifications {
	@Column(name="certifications")
	private Integer certifications;

	/**
	 * 
	 */
	public CompanyCertifications() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCertifications() {
		return certifications;
	}

	public void setCertifications(Integer certifications) {
		this.certifications = certifications;
	}

	
	

}
