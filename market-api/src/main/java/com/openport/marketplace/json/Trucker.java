package com.openport.marketplace.json;

import java.io.Serializable;

public class Trucker implements Serializable {
private String businessName="nargel";
private String businessPrimaryContact;
private String transportName;
public String getBusinessName() {
	return businessName;
}
public void setBusinessName(String businessName) {
	this.businessName = businessName;
}
public String getBusinessPrimaryContact() {
	return businessPrimaryContact;
}
public void setBusinessPrimaryContact(String businessPrimaryContact) {
	this.businessPrimaryContact = businessPrimaryContact;
}
public String getTransportName() {
	return transportName;
}
public void setTransportName(String transportName) {
	this.transportName = transportName;
}

}
