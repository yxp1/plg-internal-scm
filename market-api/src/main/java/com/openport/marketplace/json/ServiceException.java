/**
 * 
 */
package com.openport.marketplace.json;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

/**
 * @author oscar_2
 *
 */
public class ServiceException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3076554805962621041L;

	/**
	 * 
	 */
	public ServiceException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public ServiceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServiceException(Status badRequest) {
		this(badRequest, badRequest.getReasonPhrase());
	}

	public ServiceException(Status badRequest, String message) {
		UserStatus sts = new UserStatus();
		sts.setStatus(badRequest.getStatusCode());
		sts.setMessage(message);
		ResponseBuilder builder = Response.status(badRequest);
		builder.entity(sts);
		Response response = builder.build();
		throw new WebApplicationException(response);
	}
	
	public ServiceException(Status badRequest, String message, String companyName, String companyCode) {
		UserStatus sts = new UserStatus();
		sts.setStatus(badRequest.getStatusCode());
		sts.setMessage(message);
		sts.setCompanyName(companyName);
		sts.setCompanyCode(companyCode);
		
		ResponseBuilder builder = Response.status(badRequest);
		builder.entity(sts);
		Response response = builder.build();
		throw new WebApplicationException(response);
	}

}
