/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Relly
 *
 */
@XmlRootElement
@Entity
public class ShipmentSearch {

	private String originTxt;
	private String destinationTxt;
	private String originCity;
	private String destinationCity;
	private Date pickupDate;
	private Date deliveryDate;
	private String pickupDateTxt;
	private String deliveryDateTxt;
	
	private String originTxt2;
	private String destinationTxt2;
	private String originCity2;
	private String destinationCity2;
	private Date pickupDate2;
	private Date deliveryDate2;
	private String pickupDateTxt2;
	private String deliveryDateTxt2;
	
	private String statusCd;
	private int statusId;
	private String referenceNbr;
	private String equipmentType;
	private String serviceType;
	private String shipmentDescTxt;
	private int companyId;
	
	
	public ShipmentSearch() {
		
	}
	
	
	



	public int getStatusId() {
		return statusId;
	}






	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}






	public String getOriginTxt2() {
		return originTxt2;
	}





	public void setOriginTxt2(String originTxt2) {
		this.originTxt2 = originTxt2;
	}





	public String getDestinationTxt2() {
		return destinationTxt2;
	}





	public void setDestinationTxt2(String destinationTxt2) {
		this.destinationTxt2 = destinationTxt2;
	}





	public String getOriginCity2() {
		return originCity2;
	}





	public void setOriginCity2(String originCity2) {
		this.originCity2 = originCity2;
	}





	public String getDestinationCity2() {
		return destinationCity2;
	}





	public void setDestinationCity2(String destinationCity2) {
		this.destinationCity2 = destinationCity2;
	}





	public Date getPickupDate2() {
		return pickupDate2;
	}





	public void setPickupDate2(Date pickupDate2) {
		this.pickupDate2 = pickupDate2;
	}





	public Date getDeliveryDate2() {
		return deliveryDate2;
	}





	public void setDeliveryDate2(Date deliveryDate2) {
		this.deliveryDate2 = deliveryDate2;
	}





	public String getPickupDateTxt2() {
		return pickupDateTxt2;
	}





	public void setPickupDateTxt2(String pickupDateTxt2) {
		this.pickupDateTxt2 = pickupDateTxt2;
	}





	public String getDeliveryDateTxt2() {
		return deliveryDateTxt2;
	}





	public void setDeliveryDateTxt2(String deliveryDateTxt2) {
		this.deliveryDateTxt2 = deliveryDateTxt2;
	}





	public int getCompanyId() {
		return companyId;
	}


	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}




	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getOriginTxt() {
		return originTxt;
	}

	public void setOriginTxt(String originTxt) {
		this.originTxt = originTxt;
	}

	public String getDestinationTxt() {
		return destinationTxt;
	}

	public void setDestinationTxt(String destinationTxt) {
		this.destinationTxt = destinationTxt;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public String getReferenceNbr() {
		return referenceNbr;
	}

	public void setReferenceNbr(String referenceNbr) {
		this.referenceNbr = referenceNbr;
	}

	public String getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getShipmentDescTxt() {
		return shipmentDescTxt;
	}

	public void setShipmentDescTxt(String shipmentDescTxt) {
		this.shipmentDescTxt = shipmentDescTxt;
	}

	public String getPickupDateTxt() {
		return pickupDateTxt;
	}

	public void setPickupDateTxt(String pickupDateTxt) {
		this.pickupDateTxt = pickupDateTxt;
	}

	public String getDeliveryDateTxt() {
		return deliveryDateTxt;
	}

	public void setDeliveryDateTxt(String deliveryDateTxt) {
		this.deliveryDateTxt = deliveryDateTxt;
	}

	
	
}
