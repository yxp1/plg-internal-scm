package com.openport.marketplace.json;

public class Contract {
private String startDt;
private String origin;
private String destination;
private String equipment;
private String commodity;
public String getStartDt() {
	return startDt;
}
public void setStartDt(String startDt) {
	this.startDt = startDt;
}
public String getOrigin() {
	return origin;
}
public void setOrigin(String origin) {
	this.origin = origin;
}
public String getDestination() {
	return destination;
}
public void setDestination(String destination) {
	this.destination = destination;
}
public String getEquipment() {
	return equipment;
}
public void setEquipment(String equipment) {
	this.equipment = equipment;
}
public String getCommodity() {
	return commodity;
}
public void setCommodity(String commodity) {
	this.commodity = commodity;
}

}
