package com.openport.marketplace.json;

import java.util.Date;
import java.util.List;

public class RatesWorkArea {
private List<Lane> lanes;
private Lane lane;

private String startDt;
private String endDt;
private Date sortByDate;//termination
private List<Contract> failed;



private String country;


public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public List<Contract> getFailed() {
	return failed;
}
public void setFailed(List<Contract> failed) {
	this.failed = failed;
}
public List<Lane> getLanes() {
	return lanes;
}
public void setLanes(List<Lane> lanes) {
	this.lanes = lanes;
}
public Lane getLane() {
	return lane;
}
public void setLane(Lane lane) {
	this.lane = lane;
}
public String getStartDt() {
	return startDt;
}
public void setStartDt(String startDt) {
	this.startDt = startDt;
}
public String getEndDt() {
	return endDt;
}
public void setEndDt(String endDt) {
	this.endDt = endDt;
}
public Date getSortByDate() {
	return sortByDate;
}
public void setSortByDate(Date sortByDate) {
	this.sortByDate = sortByDate;
}
@Override
public String toString() {
	return "RatesWorkArea [lanes=" + lanes + ", lane=" + lane + ", startDt=" + startDt + ", endDt=" + endDt
			+ ", sortByDate=" + sortByDate + ", failed=" + failed + ", country=" + country + "]";
}



}