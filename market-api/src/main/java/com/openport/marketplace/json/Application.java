/**
 * 
 */
package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author oscar_2
 *
 */
@XmlRootElement
public class Application {

	private String appFrom; // ":"Application source",
	private String appId; // ":"Application ID, it is unique",
	private String appVersion; // 

	/**
	 * 
	 */
	public Application() {
		// TODO Auto-generated constructor stub
	}

	public String getAppFrom() {
		return appFrom;
	}

	public void setAppFrom(String appFrom) {
		this.appFrom = appFrom;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

}
