/**
 * 
 */
package com.openport.marketplace.json;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
public class LanguagePack {

	private long langVer;
	private List<Language> languagePack;
	
	/**
	 * 
	 */
	public LanguagePack() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public LanguagePack(long langVer) {
		this.langVer = langVer;
	}
	
	/**
	 * @return the langVer
	 */
	public long getLangVer() {
		return langVer;
	}

	/**
	 * @param langVer the langVer to set
	 */
	public void setLangVer(long langVer) {
		this.langVer = langVer;
	}

	/**
	 * @return the languagePack
	 */
	public List<Language> getLanguagePack() {
		return languagePack;
	}

	/**
	 * @param languagePack the languagePack to set
	 */
	public void setLanguagePack(List<Language> languagePack) {
		this.languagePack = languagePack;
	}

}
