package com.openport.marketplace.json;

public class CarrierAccessorial {
private String chargeDescTxt;
private String chargeCd;
private Double chargeAmt;
private String chargeCcyCd;
private Integer lineItemNbr;
public String getChargeDescTxt() {
	return chargeDescTxt;
}
public void setChargeDescTxt(String chargeDescTxt) {
	this.chargeDescTxt = chargeDescTxt;
}
public String getChargeCd() {
	return chargeCd;
}
public void setChargeCd(String chargeCd) {
	this.chargeCd = chargeCd;
}
public Double getChargeAmt() {
	return chargeAmt;
}
public void setChargeAmt(Double chargeAmt) {
	this.chargeAmt = chargeAmt;
}
public String getChargeCcyCd() {
	return chargeCcyCd;
}
public void setChargeCcyCd(String chargeCcyCd) {
	this.chargeCcyCd = chargeCcyCd;
}
public Integer getLineItemNbr() {
	return lineItemNbr;
}
public void setLineItemNbr(Integer lineItemNbr) {
	this.lineItemNbr = lineItemNbr;
}
@Override
public String toString() {
	return "CarrierAccessorial [chargeDescTxt=" + chargeDescTxt + ", chargeCd=" + chargeCd + ", chargeAmt=" + chargeAmt
			+ ", chargeCcyCd=" + chargeCcyCd + ", lineItemNbr=" + lineItemNbr + "]";
}


}
