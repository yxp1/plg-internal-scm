/**
 * 
 */
package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Relly
 *
 */

@XmlRootElement

public class DropDownList {

	private String name;
	private String maker;
	private String ticked;
	
	public DropDownList() {
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getTicked() {
		return ticked;
	}
	public void setTicked(String ticked) {
		this.ticked = ticked;
	}
	
	
	
}
