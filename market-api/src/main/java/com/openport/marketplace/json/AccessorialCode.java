/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */


		
@XmlRootElement
@Entity
public class AccessorialCode {

	@Column(name="ACCESSORIAL_CD")
	private String accessorialCd;
	
	@Column(name="ACCESSORIAL_DATA_SOURCE")
	private String accessorialDataSource;
	
	@Column(name="ACCESSORIAL_TYPE")
	private String accessorialType;
	
	@Column(name="CUSTOMER_CD")
	private String customerCd;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="LAST_UPDATE")
	private Date lastUpadte;
	
	@Column(name="LAST_UPDATE_SOURCE")
	private String lastUpdateSource;
	
	@Column(name="PARENT_PARTNER_CD")
	private String parentPartnerCd;
	
	@Column(name="SIZE_UOM")
	private String sizeUom;

	private Integer id;
	
	private Boolean isNew;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccessorialCd() {
		return accessorialCd;
	}

	public void setAccessorialCd(String accessorialCd) {
		this.accessorialCd = accessorialCd;
	}

	public String getAccessorialDataSource() {
		return accessorialDataSource;
	}

	public void setAccessorialDataSource(String accessorialDataSource) {
		this.accessorialDataSource = accessorialDataSource;
	}

	public String getAccessorialType() {
		return accessorialType;
	}

	public void setAccessorialType(String accessorialType) {
		this.accessorialType = accessorialType;
	}

	public String getCustomerCd() {
		return customerCd;
	}

	public void setCustomerCd(String customerCd) {
		this.customerCd = customerCd;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastUpadte() {
		return lastUpadte;
	}

	public void setLastUpadte(Date lastUpadte) {
		this.lastUpadte = lastUpadte;
	}



	public String getLastUpdateSource() {
		return lastUpdateSource;
	}

	public void setLastUpdateSource(String lastUpdateSource) {
		this.lastUpdateSource = lastUpdateSource;
	}

	public String getParentPartnerCd() {
		return parentPartnerCd;
	}

	public void setParentPartnerCd(String parentPartnerCd) {
		this.parentPartnerCd = parentPartnerCd;
	}

	public String getSizeUom() {
		return sizeUom;
	}

	public void setSizeUom(String sizeUom) {
		this.sizeUom = sizeUom;
	}

	public Boolean isNew() {
		return isNew;
	}

	public void setNew(Boolean isNew) {
		this.isNew = isNew;
	}
	
	
	
}
