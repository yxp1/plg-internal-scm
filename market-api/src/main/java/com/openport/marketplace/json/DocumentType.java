package com.openport.marketplace.json;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity	
public class DocumentType {

	/**
	 * 
	 */
	@Column(name="document_type_id")
	private Integer documentTypeId;
	@Column(name="description_txt")
	private String descriptionTxt;
	
	public DocumentType(ResultSet rs) throws SQLException {
		this.documentTypeId                = rs.getInt("document_type_id");
		this.descriptionTxt               = rs.getString("description_txt");
	}
	public DocumentType() {
	}


	
	public Integer getDocumentTypeId() {
		return documentTypeId;
	}
	public void setDocumentTypeId(Integer documentTypeId) {
		this.documentTypeId = documentTypeId;
	}
	public String getDescriptionTxt() {
		return this.descriptionTxt;
	}

	public void setDescriptionTxt(String descriptionTxt) {
		this.descriptionTxt = descriptionTxt;
	}

}
