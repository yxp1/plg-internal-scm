package com.openport.marketplace.json;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity
public class PoLineItem {

    // This pojo is not complete. It is only based for edit order screen. 
    // Please add other columns if necessary and update the constructor PoLineItem(PoLineItem item) and toString() method

    @Column(name = "PARTNER_CD")
    private String partnerCode;

    @Column(name = "PARENT_PARTNER_CD")
    private String parentPartnerCode;

    @Column(name = "SHIPPER_CD")
    private String shipperCode;

    @Column(name = "CUSTOMER_CD")
    private String customerCode;

    @Column(name = "PO_NBR")
    private String poNumber;

    @Column(name = "ACTUAL_PO_NBR")
    private String actualPoNumber;

    @Column(name = "RELEASE_NBR")
    private String releaseNumber;
    
    @Column(name = "ACTUAL_PO_RELEASE_NBR")
    private String actualPoReleaseNumber;

    @Column(name = "EQUIPMENT_SHIPMENT_SEQ_NBR")
    private int equipmentShipmentSequenceNumber;

    @Column(name = "PRODUCT_CD")
    private String productCode;

    @Column(name = "PO_LINE_ITEM_NBR")
    private int poLineItemNumber;

    @Column(name = "PACKAGE_LINE_NBR")
    private int packageLineNumber;

    @Column(name = "PRODUCT_DESC_TXT")
    private String productDescription;

    @Column(name = "RELEASE_LINE_NBR")
    private String releaseLineNumber;

    @Column(name = "HAZARDOUS_FLG")
    private boolean isHazardous;

    @Column(name = "ORDERED_QTY")
    private double orderQuantity;

    @Column(name = "ORDERED_WT")
    private double orderWeight;

    @Column(name = "ORDERED_VOL")
    private double orderVolume;

    @Column(name = "DUE_QTY")
    private double dueQuantity;

    @Column(name = "DUE_WT")
    private double dueWeight;

    @Column(name = "DUE_VOL")
    private double dueVolume;

    @Column(name = "RECEIVED_QTY")
    private double receivedQuantity;

    @Column(name = "RECEIVED_WT")
    private double receivedWeight;

    @Column(name = "RECEIVED_VOL")
    private double receivedVolume;

    @Column(name = "SHIPPED_QTY")
    private double shippedQuantity;

    @Column(name = "SHIPPED_WT")
    private double shippedWeight;

    @Column(name = "SHIPPED_VOL")
    private double shippedVolume;

    @Column(name = "PROMISED_QTY")
    private double promisedQuantity;

    @Column(name = "PROMISED_WT")
    private double promisedWeight;

    @Column(name = "PROMISED_VOL")
    private double promisedVolume;

    @Column(name = "PACKAGE_UOM")
    private String packageUom;

    @Column(name = "QTY_UOM")
    private String quantityUom;

    @Column(name = "WT_UOM")
    private String weightUom;

    @Column(name = "VOL_UOM")
    private String volumeUom;

    @Column(name = "CREATE_TSTAMP")
    private Date createTimestamp;

    @Column(name = "CREATE_USER_ID")
    private String createUserId;

    @Column(name = "TOTAL_WT")
    private double totalWeight;

    @Column(name = "TOTAL_VOL")
    private double totalVolume;

    @Column(name = "SUPPLIER_PRODUCT_CD")
    private String supplierProductCode;

    @Column(name = "OUTER_PKG_CNT")
    private int outerPackageCount;

    @Column(name = "PACKAGE_CNT")
    private int packageCount;

    @Column(name = "PACKAGE_CODE")
    private String packageCode;

    @Column(name = "PACKAGE_DESCRIPTION")
    private String packageDescription;

    @Column(name = "CARGO_REFERENCE_NBR")
    private String cargoReferenceNumber;
    
    @Column(name = "UNIT_PRICE")
    private double unitPrice;
    
    @Column(name = "UNIT_PRICE_CURRENCY_CD")
    private String unitPriceCurrencyCode;
    
    @Column(name = "goods_value_bill_to_cd")
    private String goodsValueBillToCode;
    
    
    public PoLineItem() {

    }
    
    public PoLineItem(PoLineItem item) {
        this.partnerCode = item.partnerCode;
        this.parentPartnerCode = item.parentPartnerCode;
        this.shipperCode = item.shipperCode;
        this.customerCode = item.customerCode;
        this.poNumber = item.poNumber;
        this.actualPoNumber = item.actualPoNumber;
        this.releaseNumber = item.releaseNumber;
        this.actualPoReleaseNumber = item.actualPoReleaseNumber;
        this.equipmentShipmentSequenceNumber = item.equipmentShipmentSequenceNumber;
        this.productCode = item.productCode;
        this.poLineItemNumber = item.poLineItemNumber;
        this.packageLineNumber = item.packageLineNumber;
        this.productDescription = item.productDescription;
        this.releaseLineNumber = item.releaseLineNumber;
        this.isHazardous = item.isHazardous;
        this.orderQuantity = item.orderQuantity;
        this.orderWeight = item.orderWeight;
        this.orderVolume = item.orderVolume;
        this.dueQuantity = item.dueQuantity;
        this.dueWeight = item.dueWeight;
        this.dueVolume = item.dueVolume;
        this.receivedQuantity = item.receivedQuantity;
        this.receivedWeight = item.receivedWeight;
        this.receivedVolume = item.receivedVolume;
        this.shippedQuantity = item.shippedQuantity;
        this.shippedWeight = item.shippedWeight;
        this.shippedVolume = item.shippedVolume;
        this.promisedQuantity = item.promisedQuantity;
        this.promisedWeight = item.promisedWeight;
        this.promisedVolume = item.promisedVolume;
        this.packageUom = item.packageUom;
        this.quantityUom = item.quantityUom;
        this.weightUom = item.weightUom;
        this.volumeUom = item.volumeUom;
        this.createTimestamp = item.createTimestamp;
        this.createUserId = item.createUserId;
        this.totalWeight = item.totalWeight;
        this.totalVolume = item.totalVolume;
        this.supplierProductCode = item.supplierProductCode;
        this.outerPackageCount = item.outerPackageCount;
        this.packageCount = item.packageCount;
        this.packageCode = item.packageCode;
        this.packageDescription = item.packageDescription;
        this.cargoReferenceNumber = item.cargoReferenceNumber;
        this.unitPrice = item.unitPrice;
        this.unitPriceCurrencyCode = item.unitPriceCurrencyCode;
        this.goodsValueBillToCode = item.goodsValueBillToCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getParentPartnerCode() {
        return parentPartnerCode;
    }

    public void setParentPartnerCode(String parentPartnerCode) {
        this.parentPartnerCode = parentPartnerCode;
    }

    public String getShipperCode() {
        return shipperCode;
    }

    public void setShipperCode(String shipperCode) {
        this.shipperCode = shipperCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getActualPoNumber() {
        return actualPoNumber;
    }

    public void setActualPoNumber(String actualPoNumber) {
        this.actualPoNumber = actualPoNumber;
    }

    public String getReleaseNumber() {
        return releaseNumber;
    }

    public void setReleaseNumber(String releaseNumber) {
        this.releaseNumber = releaseNumber;
    }

    public String getActualPoReleaseNumber() {
        return actualPoReleaseNumber;
    }

    public void setActualPoReleaseNumber(String actualPoReleaseNumber) {
        this.actualPoReleaseNumber = actualPoReleaseNumber;
    }

    public int getEquipmentShipmentSequenceNumber() {
        return equipmentShipmentSequenceNumber;
    }

    public void setEquipmentShipmentSequenceNumber(int equipmentShipmentSequenceNumber) {
        this.equipmentShipmentSequenceNumber = equipmentShipmentSequenceNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getPoLineItemNumber() {
        return poLineItemNumber;
    }

    public void setPoLineItemNumber(int poLineItemNumber) {
        this.poLineItemNumber = poLineItemNumber;
    }

    public int getPackageLineNumber() {
        return packageLineNumber;
    }

    public void setPackageLineNumber(int packageLineNumber) {
        this.packageLineNumber = packageLineNumber;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getReleaseLineNumber() {
        return releaseLineNumber;
    }

    public void setReleaseLineNumber(String releaseLineNumber) {
        this.releaseLineNumber = releaseLineNumber;
    }

    public boolean isHazardous() {
        return isHazardous;
    }

    public void setHazardous(boolean isHazardous) {
        this.isHazardous = isHazardous;
    }

    public double getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(double orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public double getOrderWeight() {
        return orderWeight;
    }

    public void setOrderWeight(double orderWeight) {
        this.orderWeight = orderWeight;
    }

    public double getOrderVolume() {
        return orderVolume;
    }

    public void setOrderVolume(double orderVolume) {
        this.orderVolume = orderVolume;
    }

    public double getDueQuantity() {
        return dueQuantity;
    }

    public void setDueQuantity(double dueQuantity) {
        this.dueQuantity = dueQuantity;
    }

    public double getDueWeight() {
        return dueWeight;
    }

    public void setDueWeight(double dueWeight) {
        this.dueWeight = dueWeight;
    }

    public double getDueVolume() {
        return dueVolume;
    }

    public void setDueVolume(double dueVolume) {
        this.dueVolume = dueVolume;
    }

    public double getReceivedQuantity() {
        return receivedQuantity;
    }

    public void setReceivedQuantity(double receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    public double getReceivedWeight() {
        return receivedWeight;
    }

    public void setReceivedWeight(double receivedWeight) {
        this.receivedWeight = receivedWeight;
    }

    public double getReceivedVolume() {
        return receivedVolume;
    }

    public void setReceivedVolume(double receivedVolume) {
        this.receivedVolume = receivedVolume;
    }

    public double getShippedQuantity() {
        return shippedQuantity;
    }

    public void setShippedQuantity(double shippedQuantity) {
        this.shippedQuantity = shippedQuantity;
    }

    public double getShippedWeight() {
        return shippedWeight;
    }

    public void setShippedWeight(double shippedWeight) {
        this.shippedWeight = shippedWeight;
    }

    public double getShippedVolume() {
        return shippedVolume;
    }

    public void setShippedVolume(double shippedVolume) {
        this.shippedVolume = shippedVolume;
    }

    public double getPromisedQuantity() {
        return promisedQuantity;
    }

    public void setPromisedQuantity(double promisedQuantity) {
        this.promisedQuantity = promisedQuantity;
    }

    public double getPromisedWeight() {
        return promisedWeight;
    }

    public void setPromisedWeight(double promisedWeight) {
        this.promisedWeight = promisedWeight;
    }

    public double getPromisedVolume() {
        return promisedVolume;
    }

    public void setPromisedVolume(double promisedVolume) {
        this.promisedVolume = promisedVolume;
    }

    public String getPackageUom() {
        return packageUom;
    }

    public void setPackageUom(String packageUom) {
        this.packageUom = packageUom;
    }

    public String getQuantityUom() {
        return quantityUom;
    }

    public void setQuantityUom(String quantityUom) {
        this.quantityUom = quantityUom;
    }

    public String getWeightUom() {
        return weightUom;
    }

    public void setWeightUom(String weightUom) {
        this.weightUom = weightUom;
    }

    public String getVolumeUom() {
        return volumeUom;
    }

    public void setVolumeUom(String volumeUom) {
        this.volumeUom = volumeUom;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getSupplierProductCode() {
        return supplierProductCode;
    }

    public void setSupplierProductCode(String supplierProductCode) {
        this.supplierProductCode = supplierProductCode;
    }

    public int getOuterPackageCount() {
        return outerPackageCount;
    }

    public void setOuterPackageCount(int outerPackageCount) {
        this.outerPackageCount = outerPackageCount;
    }

    public int getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(int packageCount) {
        this.packageCount = packageCount;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getCargoReferenceNumber() {
        return cargoReferenceNumber;
    }

    public void setCargoReferenceNumber(String cargoReferenceNumber) {
        this.cargoReferenceNumber = cargoReferenceNumber;
    }
    
    public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getUnitPriceCurrencyCode() {
		return unitPriceCurrencyCode;
	}

	public void setUnitPriceCurrencyCode(String unitPriceCurrencyCode) {
		this.unitPriceCurrencyCode = unitPriceCurrencyCode;
	}
	
	public String getGoodsValueBillToCode() {
		return goodsValueBillToCode;
	}

	public void setGoodsValueBillToCode(String goodsValueBillToCode) {
		this.goodsValueBillToCode = goodsValueBillToCode;
	}

	@Override
	public String toString() {
		return "PoLineItem [partnerCode=" + partnerCode + ", parentPartnerCode=" + parentPartnerCode + ", shipperCode="
				+ shipperCode + ", customerCode=" + customerCode + ", poNumber=" + poNumber + ", actualPoNumber="
				+ actualPoNumber + ", releaseNumber=" + releaseNumber + ", actualPoReleaseNumber="
				+ actualPoReleaseNumber + ", equipmentShipmentSequenceNumber=" + equipmentShipmentSequenceNumber
				+ ", productCode=" + productCode + ", poLineItemNumber=" + poLineItemNumber + ", packageLineNumber="
				+ packageLineNumber + ", productDescription=" + productDescription + ", releaseLineNumber="
				+ releaseLineNumber + ", isHazardous=" + isHazardous + ", orderQuantity=" + orderQuantity
				+ ", orderWeight=" + orderWeight + ", orderVolume=" + orderVolume + ", dueQuantity=" + dueQuantity
				+ ", dueWeight=" + dueWeight + ", dueVolume=" + dueVolume + ", receivedQuantity=" + receivedQuantity
				+ ", receivedWeight=" + receivedWeight + ", receivedVolume=" + receivedVolume + ", shippedQuantity="
				+ shippedQuantity + ", shippedWeight=" + shippedWeight + ", shippedVolume=" + shippedVolume
				+ ", promisedQuantity=" + promisedQuantity + ", promisedWeight=" + promisedWeight + ", promisedVolume="
				+ promisedVolume + ", packageUom=" + packageUom + ", quantityUom=" + quantityUom + ", weightUom="
				+ weightUom + ", volumeUom=" + volumeUom + ", createTimestamp=" + createTimestamp + ", createUserId="
				+ createUserId + ", totalWeight=" + totalWeight + ", totalVolume=" + totalVolume
				+ ", supplierProductCode=" + supplierProductCode + ", outerPackageCount=" + outerPackageCount
				+ ", packageCount=" + packageCount + ", packageCode=" + packageCode + ", packageDescription="
				+ packageDescription + ", cargoReferenceNumber=" + cargoReferenceNumber + ", unitPrice=" + unitPrice
				+ ", unitPriceCurrencyCode=" + unitPriceCurrencyCode + ", goodsValueBillToCode=" + goodsValueBillToCode
				+ "]";
	}

}
