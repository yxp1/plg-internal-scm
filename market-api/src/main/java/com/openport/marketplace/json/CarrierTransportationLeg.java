package com.openport.marketplace.json;

import java.util.List;

public class CarrierTransportationLeg {
private Integer legNbr;
private String origin;
private String destination;
private List<CarrierRate> revenue;
private List<CarrierRate> cost;
public Integer getLegNbr() {
	return legNbr;
}
public void setLegNbr(Integer legNbr) {
	this.legNbr = legNbr;
}
public String getOrigin() {
	return origin;
}
public void setOrigin(String origin) {
	this.origin = origin;
}
public String getDestination() {
	return destination;
}
public void setDestination(String destination) {
	this.destination = destination;
}
public List<CarrierRate> getRevenue() {
	return revenue;
}
public void setRevenue(List<CarrierRate> revenue) {
	this.revenue = revenue;
}
public List<CarrierRate> getCost() {	
	return cost;
}
public void setCost(List<CarrierRate> cost) {
	this.cost = cost;
}
@Override
public String toString() {
	return "CarrierTransportationLeg [legNbr=" + legNbr + ", origin=" + origin + ", destination=" + destination
			+ ", revenue=" + revenue + ", cost=" + cost + "]";
}


}
