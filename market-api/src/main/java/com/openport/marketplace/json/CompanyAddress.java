/**
 * 
 */
package com.openport.marketplace.json;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class CompanyAddress {

	
	@Column(name="address_Line_1")
	private String addressLine1;
	@Column(name="address_Line_2")
	private String addressLine2;
	@Column(name="address_Line_3")
	private String addressLine3;
	@Column(name="city_Name")
	private String cityName;
	@Column(name="province_Id")
	private int provinceId;
	@Column(name="country_Id")
	private int countryId;
	@Column(name="postal_Cd")
	private String postalCd;
	@Column(name="update_user_id")
	private int updateUserId;
	@Column(name="create_user_id")
	private int createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_dt")
	private Timestamp updateDt;
	
//	private int createUserId;
//	private Timestamp createDt;
//	private int updateUserId;
//	private Timestamp updateDt;
	
	@Column(name="company_Address_Id")
	private int companyAddressId;

	
	@Column(name="latitude")
	private BigDecimal lat;
	
	@Column(name="longtitude")
	private BigDecimal lon;
	
	// Denormalized for JSON - start
	@Column(name="province_Name")
	private String provinceName;
	@Column(name="country_Name")
	private String countryName;
	
	// Denormalized for JSON - end
	private int status;
	
	
	
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * 
	 */
	public CompanyAddress() {
		// TODO Auto-generated constructor stub
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getPostalCd() {
		return postalCd;
	}

	public void setPostalCd(String postalCd) {
		this.postalCd = postalCd;
	}

	/**
	 * @return the provinceName
	 */
	public String getProvinceName() {
		return provinceName;
	}

	/**
	 * @param provinceName the provinceName to set
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getCompanyAddressId() {
		return companyAddressId;
	}

	public void setCompanyAddressId(int companyAddressId) {
		this.companyAddressId = companyAddressId;
	}

	public int getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLon() {
		return lon;
	}

	public void setLon(BigDecimal lon) {
		this.lon = lon;
	}

	@Override
	public String toString() {
		return "CompanyAddress [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", addressLine3="
				+ addressLine3 + ", cityName=" + cityName + ", provinceId=" + provinceId + ", countryId=" + countryId
				+ ", postalCd=" + postalCd + ", updateUserId=" + updateUserId + ", createUserId=" + createUserId
				+ ", createDt=" + createDt + ", updateDt=" + updateDt + ", companyAddressId=" + companyAddressId
				+ ", lat=" + lat + ", lon=" + lon + ", provinceName=" + provinceName + ", countryName=" + countryName
				+ ", status=" + status + "]";
	}


	
}
