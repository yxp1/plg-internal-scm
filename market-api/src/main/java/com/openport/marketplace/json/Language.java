/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class Language {
	@Column(name="language_id")
	private Integer languageId;
	@Column(name="language_name")
	private String languageName;
	//@Column(name="iso_639_2_Cd")
	//private String iso6392Cd;
	@Column(name="language_Cd")
	private String languageCd;
	@Column(name="create_user_id")
	private Integer createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private Integer updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;
	
	//denormalized
	@Column(name="user_language_id")
	private Integer userLanguageId;
	
	@Column(name="user_id")
	private Integer userId;
	
	private Boolean status;
	private List<Label> data;
	

	/**
	 * 
	 */
	public Language() {
		// TODO Auto-generated constructor stub
	}

	public Language(String lang, String name) {
		//this.iso6392Cd = lang;
		this.languageName = name;
		this.languageCd=lang;
    	data = new LinkedList<Label>();
	}
	
	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	
	
	


	public String getLanguageCd() {
		return languageCd;
	}

	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}


	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public Integer getUserLanguageId() {
		return userLanguageId;
	}

	public void setUserLanguageId(Integer userLanguageId) {
		this.userLanguageId = userLanguageId;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<Label> getData() {
		return data;
	}

	public void setData(List<Label> data) {
		this.data = data;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	
}
