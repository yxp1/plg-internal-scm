/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

import com.openport.marketplace.model.Accessorial;



/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class Shipment {

	@Column(name="shipment_id")
	private Integer shipmentId;
	
	@Column(name="shipment_nbr")
	private String shipmentNbr;
	
	@Column(name="shipper_id")
	private Integer shipperId;
	
	@Column(name="status_id")
	private Integer statusId;

	@Column(name="assigned_provider_id")
	private Integer assignedProviderId;
	
	@Column(name="pickup_address_id")
	private Integer pickupAddressId;
	
	@Column(name="delivery_address_id")
	private Integer deliveryAddressId;
	
	@Column(name="requested_equipment_type_id")
	private Integer requestedEquipmentTypeId;
	
	@Column(name="requested_pickup_dt")
	private Timestamp requestedPickupDt;
	
	@Column(name="requested_delivery_dt")
	private Timestamp requestedDeliveryDt;
	
	@Column(name="unit_count")
	private Double unitCount;
	
	@Column(name="unit_of_measure")
	private String unitOfMeasure;
	
	@Column(name="create_User_Id")
	private String createUserId;
	
	@Column(name="create_dt")
	private Timestamp createDt;
	
	@Column(name="update_User_Id")
	private String updateUserId;
	
	@Column(name="update_dt")
	private Timestamp updateDt;
	
	@Column(name="tender_expiration_dt")
	private Timestamp tenderExpirationDt;

	@Column(name="origin_service_area_id")
	private Integer originServiceAreaId;

	@Column(name="destination_service_area_id")
	private Integer destinationServiceAreaId;
	
	@Column(name="unit_wt")
	private Double unitWt;
	
	@Column(name="unit_vol")
	private Double unitVol;
	
	@Column(name="payment_term_id")
	private Integer paymentTermId;
	
	@Column(name="low_end_bid_amt")
	private Double lowEndBidAmt;
	
	@Column(name="high_end_bid_amt")
	private Double highEndBidAmt;
	
	@Column(name="avg_bid_amt")
	private Double avgBidAmt;
	
	@Column(name="target_rate_amt")
	private Double targetRateAmt;
	
	@Column(name="currency_cd")
	private String currencyCd;
	
	@Column(name="tender_type_cd")
	private String tenderTypeCd;
	
	@Column(name="bid_rank")
	private Integer bidRank ;
	
	@Column(name="rate_count")
	private Integer rateCount;
	
	@Column(name="origin_wait_tm")
	private Double originWaitTm;
	
	@Column(name="dest_wait_tm")
	private Double destWaitTm;
	
	@Column(name="origin_wait_tm_uom")
	private String originWaitTmUom;
	
	@Column(name="dest_wait_tm_uom")
	private String destWaitTmUom;
	
	@Column(name="commodity_desc_txt")
	private String commodityDescTxt;
	
	@Column(name="pro_bill_nbr")
	private String proBillNbr;
	
	@Column(name="parent_partner_cd")
	private String vendorId;
	
	@Column(name="Shipping_Instruction_Txt")
	private String si;
	
	@Column(name="Shipping_Type_Cd")
	private String shptType;
	
	@Column(name="stops_cnt")
	private Integer stopCnt;
	
	@Column(name="PO_Nbr")
	private String purchaseOrderNumber;
	
	@Column(name="Release_Nbr")
	private String releaseNumber;
	
	@Column(name = "Actual_Delivery_Dt")
	private Timestamp actualDeliveryDate;
	
	// Denormalized for JSON - start
	private List<ShipmentAccessorial> shipmentAccessorials;
	private ShipmentDetail shipmentDetail;
	private List<ShipmentContact> shipmentContacts;
	private PaymentTerm paymentTerm;
	private List<Service> services;
	private List<ShipmentDetail> shipmentDetails;
	private List<ShipmentService> shipmentServices;
	private List<ShipmentCarrierBid> shipmentCarrierBids;
	private EntityAddress pickupAddress;
	private EntityAddress deliveryAddress;
	private Double lowestCost;
	private Double carrierBid;
	private String carrierBidType;
	private String lowestCostCurrency;
	private String rateType;
	private EquipmentType equipmentType;
	private Integer equipmentTypeId;
	private String equipmentTypeCd;
	private String pickupDt;
	private String deliveryDt;
	private Integer status;
	private String companyCd;
	private String timezone;
	private CorporateAddress corpAddress;
	private List<Accessorial> accessorials;
	private String pickupDtAmPm;
	private String deliveryDtAmPm;
	private String statusDescription;
	private boolean isSent204Flag;
	private boolean isBrFlag;
	private String accountCode;
	//private List<String> shipmenDetails;
	// Denormalized for JSON - end
	

	public String getCompanyCd() {
		return companyCd;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public void setCompanyCd(String companyCd) {
		this.companyCd = companyCd;
	}

	public Double getOriginWaitTm() {
		return originWaitTm;
	}

	public void setOriginWaitTm(Double originWaitTm) {
		this.originWaitTm = originWaitTm;
	}

	public Double getDestWaitTm() {
		return destWaitTm;
	}

	public void setDestWaitTm(Double destWaitTm) {
		this.destWaitTm = destWaitTm;
	}

	public String getOriginWaitTmUom() {
		return originWaitTmUom;
	}

	public void setOriginWaitTmUom(String originWaitTmUom) {
		this.originWaitTmUom = originWaitTmUom;
	}

	public String getDestWaitTmUom() {
		return destWaitTmUom;
	}

	public void setDestWaitTmUom(String destWaitTmUom) {
		this.destWaitTmUom = destWaitTmUom;
	}

	public String getCommodityDescTxt() {
		return commodityDescTxt;
	}

	public void setCommodityDescTxt(String commodityDescTxt) {
		this.commodityDescTxt = commodityDescTxt;
	}

	public void setBidRank(Integer bidRank) {
		this.bidRank = bidRank;
	}

	public void setRateCount(Integer rateCount) {
		this.rateCount = rateCount;
	}

	public Integer getShipmentId() {
		return shipmentId;
	}

	public PaymentTerm getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(PaymentTerm paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
/*
	public List<String> getShipmenDetails() {
		return shipmenDetails;
	}

	public void setShipmenDetails(List<String> shipmenDetails) {
		this.shipmenDetails = shipmenDetails;
	}
*/
	public Double getCarrierBid() {
		return carrierBid;
	}

	public void setCarrierBid(Double carrierBid) {
		this.carrierBid = carrierBid;
	}

	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}

	public String getShipmentNbr() {
		return shipmentNbr;
	}

	public void setShipmentNbr(String shipmentNbr) {
		this.shipmentNbr = shipmentNbr;
	}

	public Integer getShipperId() {
		return shipperId;
	}

	public void setShipperId(Integer shipperId) {
		this.shipperId = shipperId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getAssignedProviderId() {
		return assignedProviderId;
	}

	public void setAssignedProviderId(Integer assignedProviderId) {
		this.assignedProviderId = assignedProviderId;
	}

	public Integer getPickupAddressId() {
		return pickupAddressId;
	}

	public void setPickupAddressId(Integer pickupAddressId) {
		this.pickupAddressId = pickupAddressId;
	}

	public Integer getDeliveryAddressId() {
		return deliveryAddressId;
	}

	public void setDeliveryAddressId(Integer deliveryAddressId) {
		this.deliveryAddressId = deliveryAddressId;
	}

	public Integer getRequestedEquipmentTypeId() {
		return requestedEquipmentTypeId;
	}

	public void setRequestedEquipmentTypeId(Integer requestedEquipmentTypeId) {
		this.requestedEquipmentTypeId = requestedEquipmentTypeId;
	}

	public Timestamp getRequestedPickupDt() {
		return requestedPickupDt;
	}

	public void setRequestedPickupDt(Timestamp requestedPickupDt) {
		this.requestedPickupDt = requestedPickupDt;
	}

	public Timestamp getRequestedDeliveryDt() {
		return requestedDeliveryDt;
	}

	public void setRequestedDeliveryDt(Timestamp requestedDeliveryDt) {
		this.requestedDeliveryDt = requestedDeliveryDt;
	}

	public Double getUnitCount() {
		return unitCount;
	}

	public void setUnitCount(Double unitCount) {
		this.unitCount = unitCount;
	}

	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public ShipmentDetail getShipmentDetail() {
		return shipmentDetail;
	}

	public void setShipmentDetail(ShipmentDetail shipmentDetail) {
		this.shipmentDetail = shipmentDetail;
	}

	public List<ShipmentDetail> getShipmentDetails() {
		return shipmentDetails;
	}

	public void setShipmentDetails(List<ShipmentDetail> shipmentDetails) {
		this.shipmentDetails = shipmentDetails;
	}

	public List<ShipmentService> getShipmentServices() {
		return shipmentServices;
	}

	public void setShipmentServices(List<ShipmentService> shipmentServices) {
		this.shipmentServices = shipmentServices;
	}

	public Timestamp getTenderExpirationDt() {
		return tenderExpirationDt;
	}

	public void setTenderExpirationDt(Timestamp tenderExpirationDt) {
		this.tenderExpirationDt = tenderExpirationDt;
	}

	public List<ShipmentCarrierBid> getShipmentCarrierBids() {
		return shipmentCarrierBids;
	}

	public void setShipmentCarrierBids(List<ShipmentCarrierBid> shipmentCarrierBids) {
		this.shipmentCarrierBids = shipmentCarrierBids;
	}

	public EntityAddress getPickupAddress() {
		return pickupAddress;
	}

	public void setPickupAddress(EntityAddress pickupAddress) {
		this.pickupAddress = pickupAddress;
	}

	public EntityAddress getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(EntityAddress deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Double getLowestCost() {
		return lowestCost;
	}

	public void setLowestCost(Double lowestCost) {
		this.lowestCost = lowestCost;
	}

	public int getRateCount() {
		return rateCount;
	}

	public void setRateCount(int rateCount) {
		this.rateCount = rateCount;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getLowestCostCurrency() {
		return lowestCostCurrency;
	}

	public void setLowestCostCurrency(String lowestCostCurrency) {
		this.lowestCostCurrency = lowestCostCurrency;
	}

	public EquipmentType getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentType equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Integer getOriginServiceAreaId() {
		return originServiceAreaId;
	}

	public void setOriginServiceAreaId(Integer originServiceAreaId) {
		this.originServiceAreaId = originServiceAreaId;
	}

	public Integer getDestinationServiceAreaId() {
		return destinationServiceAreaId;
	}

	public void setDestinationServiceAreaId(Integer destinationServiceAreaId) {
		this.destinationServiceAreaId = destinationServiceAreaId;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public Integer getEquipmentTypeId() {
		return equipmentTypeId;
	}

	public void setEquipmentTypeId(Integer equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}

	public Double getUnitWt() {
		return unitWt;
	}

	public void setUnitWt(Double unitWt) {
		this.unitWt = unitWt;
	}

	public Double getUnitVol() {
		return unitVol;
	}

	public void setUnitVol(Double unitVol) {
		this.unitVol = unitVol;
	}

	public String getPickupDt() {
		return pickupDt;
	}

	public void setPickupDt(String pickupDt) {
		this.pickupDt = pickupDt;
	}

	public String getDeliveryDt() {
		return deliveryDt;
	}

	public void setDeliveryDt(String deliveryDt) {
		this.deliveryDt = deliveryDt;
	}


	public Integer getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(Integer paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Double getLowEndBidAmt() {
		return lowEndBidAmt;
	}

	public void setLowEndBidAmt(Double lowEndBidAmt) {
		this.lowEndBidAmt = lowEndBidAmt;
	}

	public Double getHighEndBidAmt() {
		return highEndBidAmt;
	}

	public void setHighEndBidAmt(Double highEndBidAmt) {
		this.highEndBidAmt = highEndBidAmt;
	}

	public Double getTargetRateAmt() {
		return targetRateAmt;
	}

	public void setTargetRateAmt(Double targetRateAmt) {
		this.targetRateAmt = targetRateAmt;
	}

	public String getCurrencyCd() {
		return currencyCd;
	}

	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}

	public String getTenderTypeCd() {
		return tenderTypeCd;
	}

	public void setTenderTypeCd(String tenderTypeCd) {
		this.tenderTypeCd = tenderTypeCd;
	}

	public List<ShipmentContact> getShipmentContacts() {
		return shipmentContacts;
	}

	public void setShipmentContacts(List<ShipmentContact> shipmentContacts) {
		this.shipmentContacts = shipmentContacts;
	}

	public Double getAvgBidAmt() {
		return avgBidAmt;
	}

	public void setAvgBidAmt(Double avgBidAmt) {
		this.avgBidAmt = avgBidAmt;
	}

	public int getBidRank() {
		return bidRank;
	}

	public void setBidRank(int bidRank) {
		this.bidRank = bidRank;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCarrierBidType() {
		return carrierBidType;
	}

	public void setCarrierBidType(String carrierBidType) {
		this.carrierBidType = carrierBidType;
	}

	public List<ShipmentAccessorial> getShipmentAccessorials() {
		return shipmentAccessorials;
	}

	public void setShipmentAccessorials(List<ShipmentAccessorial> shipmentAccessorials) {
		this.shipmentAccessorials = shipmentAccessorials;
	}

	public String getProBillNbr() {
		return proBillNbr;
	}

	public void setProBillNbr(String proBillNbr) {
		this.proBillNbr = proBillNbr;
	}

	public CorporateAddress getCorpAddress() {
		return corpAddress;
	}

	public void setCorpAddress(CorporateAddress corpAddress) {
		this.corpAddress = corpAddress;
	}

	public List<Accessorial> getAccessorials() {
		return accessorials;
	}

	public void setAccessorials(List<Accessorial> accessorials) {
		this.accessorials = accessorials;
	}

	public String getPickupDtAmPm() {
		return pickupDtAmPm;
	}

	public void setPickupDtAmPm(String pickupDtAmPm) {
		this.pickupDtAmPm = pickupDtAmPm;
	}

	public String getDeliveryDtAmPm() {
		return deliveryDtAmPm;
	}

	public void setDeliveryDtAmPm(String deliveryDtAmPm) {
		this.deliveryDtAmPm = deliveryDtAmPm;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getSi() {
		return si;
	}

	public void setSi(String si) {
		this.si = si;
	}

	public String getShptType() {
		return shptType;
	}

	public void setShptType(String shptType) {
		this.shptType = shptType;
	}

	public Integer getStopCnt() {
		return stopCnt;
	}

	public void setStopCnt(Integer stopCnt) {
		this.stopCnt = stopCnt;
	}

	public String getEquipmentTypeCd() {
		return equipmentTypeCd;
	}

	public void setEquipmentTypeCd(String equipmentTypeCd) {
		this.equipmentTypeCd = equipmentTypeCd;
	}

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public boolean isSent204Flag() {
        return isSent204Flag;
    }

    public void setSent204Flag(boolean isSent204Flag) {
        this.isSent204Flag = isSent204Flag;
    }

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getReleaseNumber() {
		return releaseNumber;
	}

	public void setReleaseNumber(String releaseNumber) {
		this.releaseNumber = releaseNumber;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public Timestamp getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public void setActualDeliveryDate(Timestamp actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}

	public boolean isBrFlag() {
		return isBrFlag;
	}

	public void setBrFlag(boolean isBrFlag) {
		this.isBrFlag = isBrFlag;
	}

	@Override
	public String toString() {
		return "Shipment [shipmentId=" + shipmentId + ", shipmentNbr=" + shipmentNbr + ", shipperId=" + shipperId
				+ ", statusId=" + statusId + ", assignedProviderId=" + assignedProviderId + ", pickupAddressId="
				+ pickupAddressId + ", deliveryAddressId=" + deliveryAddressId + ", requestedEquipmentTypeId="
				+ requestedEquipmentTypeId + ", requestedPickupDt=" + requestedPickupDt + ", requestedDeliveryDt="
				+ requestedDeliveryDt + ", unitCount=" + unitCount + ", unitOfMeasure=" + unitOfMeasure
				+ ", createUserId=" + createUserId + ", createDt=" + createDt + ", updateUserId=" + updateUserId
				+ ", updateDt=" + updateDt + ", tenderExpirationDt=" + tenderExpirationDt + ", originServiceAreaId="
				+ originServiceAreaId + ", destinationServiceAreaId=" + destinationServiceAreaId + ", unitWt=" + unitWt
				+ ", unitVol=" + unitVol + ", paymentTermId=" + paymentTermId + ", lowEndBidAmt=" + lowEndBidAmt
				+ ", highEndBidAmt=" + highEndBidAmt + ", avgBidAmt=" + avgBidAmt + ", targetRateAmt=" + targetRateAmt
				+ ", currencyCd=" + currencyCd + ", tenderTypeCd=" + tenderTypeCd + ", bidRank=" + bidRank
				+ ", rateCount=" + rateCount + ", originWaitTm=" + originWaitTm + ", destWaitTm=" + destWaitTm
				+ ", originWaitTmUom=" + originWaitTmUom + ", destWaitTmUom=" + destWaitTmUom + ", commodityDescTxt="
				+ commodityDescTxt + ", proBillNbr=" + proBillNbr + ", vendorId=" + vendorId + ", si=" + si
				+ ", shptType=" + shptType + ", stopCnt=" + stopCnt + ", purchaseOrderNumber=" + purchaseOrderNumber
				+ ", releaseNumber=" + releaseNumber + ", actualDeliveryDate=" + actualDeliveryDate
				+ ", shipmentAccessorials=" + shipmentAccessorials + ", shipmentDetail=" + shipmentDetail
				+ ", shipmentContacts=" + shipmentContacts + ", paymentTerm=" + paymentTerm + ", services=" + services
				+ ", shipmentDetails=" + shipmentDetails + ", shipmentServices=" + shipmentServices
				+ ", shipmentCarrierBids=" + shipmentCarrierBids + ", pickupAddress=" + pickupAddress
				+ ", deliveryAddress=" + deliveryAddress + ", lowestCost=" + lowestCost + ", carrierBid=" + carrierBid
				+ ", carrierBidType=" + carrierBidType + ", lowestCostCurrency=" + lowestCostCurrency + ", rateType="
				+ rateType + ", equipmentType=" + equipmentType + ", equipmentTypeId=" + equipmentTypeId
				+ ", equipmentTypeCd=" + equipmentTypeCd + ", pickupDt=" + pickupDt + ", deliveryDt=" + deliveryDt
				+ ", status=" + status + ", companyCd=" + companyCd + ", timezone=" + timezone + ", corpAddress="
				+ corpAddress + ", accessorials=" + accessorials + ", pickupDtAmPm=" + pickupDtAmPm
				+ ", deliveryDtAmPm=" + deliveryDtAmPm + ", statusDescription=" + statusDescription + ", isSent204Flag="
				+ isSent204Flag + ", isBrFlag=" + isBrFlag + ", accountCode=" + accountCode + "]";
	}

}
