package com.openport.marketplace.json;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CarrierProfile {

	
	private String 	companyName;
	private String  businessHeadline;
	private String  businessDescription;
	private String  companyCode;
	private Integer yearsOfOperation;
	private Integer rating;
	private String  companyLogoPath;
	private String  companyAddress;
	private String  hqCity;
	private String  hqProvince;
	private String  hqCountry;
	private String  truckPoolAddress;
	private String  branchAddress;
	private String  contactName;
	private String  emailAddress;
	private String  phoneNbr;
	private String  serviceName;
	private String  serviceAreaName;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBusinessHeadline() {
		return businessHeadline;
	}
	public void setBusinessHeadline(String businessHeadline) {
		this.businessHeadline = businessHeadline;
	}
	public String getBusinessDescription() {
		return businessDescription;
	}
	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public Integer getYearsOfOperation() {
		return yearsOfOperation;
	}
	public void setYearsOfOperation(Integer yearsOfOperation) {
		this.yearsOfOperation = yearsOfOperation;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public String getCompanyLogoPath() {
		return companyLogoPath;
	}
	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getHqCity() {
		return hqCity;
	}
	public void setHqCity(String hqCity) {
		this.hqCity = hqCity;
	}
	public String getHqProvince() {
		return hqProvince;
	}
	public void setHqProvince(String hqProvince) {
		this.hqProvince = hqProvince;
	}
	public String getHqCountry() {
		return hqCountry;
	}
	public void setHqCountry(String hqCountry) {
		this.hqCountry = hqCountry;
	}
	public String getTruckPoolAddress() {
		return truckPoolAddress;
	}
	public void setTruckPoolAddress(String truckPoolAddress) {
		this.truckPoolAddress = truckPoolAddress;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNbr() {
		return phoneNbr;
	}
	public void setPhoneNbr(String phoneNbr) {
		this.phoneNbr = phoneNbr;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceAreaName() {
		return serviceAreaName;
	}
	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}
	
	//denormalized
	private List<ServiceAreaProfile> serviceAreas;
	private ArrayList<CompanyFile> companyFiles;
	private List<CarrierEquipment> equipment;
	public List<ServiceAreaProfile> getServiceAreas() {
		return serviceAreas;
	}
	public void setServiceAreas(List<ServiceAreaProfile> serviceAreas) {
		this.serviceAreas = serviceAreas;
	}
	public ArrayList<CompanyFile> getCompanyFiles() {
		return companyFiles;
	}
	public void setCompanyFiles(ArrayList<CompanyFile> companyFiles) {
		this.companyFiles = companyFiles;
	}
	public List<CarrierEquipment> getEquipment() {
		return equipment;
	}
	public void setEquipment(List<CarrierEquipment> equipment) {
		this.equipment = equipment;
	}
	
	
	
	
	
	

}
