/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author oscar_2
 *
 */
@XmlRootElement
@Entity
public class User {
	@Column(name="user_id")
	private int userId;
	@Column(name="username")
	private String username;
	@Column(name="role")
	private String role;
	@Column(name="email_address")
	private String emailAddress;
	@Column(name="phone_Nbr")
	private String phoneNumber;
	@Column(name="password")
	private String password;
	@Column(name="first_Name")
	private String firstName;
	@Column(name="last_Name")
	private String lastName;
	@Column(name="create_user_id")
	private int createUserId;
	@Column(name="update_user_id")
	private int updateUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_dt")
	private Timestamp updateDt;
	@Column(name="Is_Active")
	private Boolean isActive;
	@Column(name="Is_Password_Weak")
	private Boolean isPasswordWeak;

	private Company company;
	private List<UserLanguage> userLanguages;
	private List<Language> languages;
	
	@Column(name="company_id")
	private Integer companyId;
	
	@Column(name="is_Primary_Contact")
	private Boolean isPrimaryContact;
	@Column(name="is_Technical_Contact")
	private Boolean isTechnicalContact;
	
	private Integer companyUserId;
	

	
	
	
	/**
	 * 
	 */
	public User() {
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<UserLanguage> getUserLanguages() {
		return userLanguages;
	}

	public void setUserLanguages(List<UserLanguage> userLanguages) {
		this.userLanguages = userLanguages;
	}

	/**
	 * @return the isActive
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", role=" + role + ", emailAddress=" + emailAddress
				+ ", phoneNumber=" + phoneNumber + ", password=" + password + ", firstName=" + firstName + ", lastName="
				+ lastName + ", createUserId=" + createUserId + ", updateUserId=" + updateUserId + ", createDt="
				+ createDt + ", updateDt=" + updateDt + ", isActive=" + isActive + ", isPasswordWeak=" + isPasswordWeak
				+ ", company=" + company + ", userLanguages=" + userLanguages + ", languages=" + languages
				+ ", companyId=" + companyId + ", isPrimaryContact=" + isPrimaryContact + ", isTechnicalContact="
				+ isTechnicalContact + ", companyUserId=" + companyUserId + "]";
	}

	public Boolean isPasswordWeak() {
		return isPasswordWeak;
	}

	public Boolean getIsPasswordWeak() {
		return isPasswordWeak;
	}

	public void setIsPasswordWeak(Boolean isPasswordWeak) {
		this.isPasswordWeak = isPasswordWeak;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Boolean getIsPrimaryContact() {
		return isPrimaryContact;
	}

	public void setIsPrimaryContact(Boolean isPrimaryContact) {
		this.isPrimaryContact = isPrimaryContact;
	}

	public Boolean getIsTechnicalContact() {
		return isTechnicalContact;
	}

	public void setIsTechnicalContact(Boolean isTechnicalContact) {
		this.isTechnicalContact = isTechnicalContact;
	}

	public Integer getCompanyUserId() {
		return companyUserId;
	}

	public void setCompanyUserId(Integer companyUserId) {
		this.companyUserId = companyUserId;
	}

	

}
