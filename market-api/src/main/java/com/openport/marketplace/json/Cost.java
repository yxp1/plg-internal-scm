package com.openport.marketplace.json;

import java.util.List;

public class Cost {

private Boolean selected;
private String carrierCd;
private String contractNumber;
private Double mileage;
private Integer transitDay;
private Double rtWt;
private Double baseFreight;
private Double dsc;
private Double fsc;
private Double accessorialCharge;
private Double revenue;
private Double margin;
private Double totalCharge;



public Double getTotalCharge() {
	return totalCharge;
}
public void setTotalCharge(Double totalCharge) {
	this.totalCharge = totalCharge;
}
private List<CarrierAccessorial> accessorials;

public Boolean getSelected() {
	return selected;
}
public void setSelected(Boolean selected) {
	this.selected = selected;
}

public String getCarrierCd() {
	return carrierCd;
}
public void setCarrierCd(String carrierCd) {
	this.carrierCd = carrierCd;
}
public String getContractNumber() {
	return contractNumber;
}
public void setContractNumber(String contractNumber) {
	this.contractNumber = contractNumber;
}
public Double getMileage() {
	return mileage;
}
public void setMileage(Double mileage) {
	this.mileage = mileage;
}
public Integer getTransitDay() {
	return transitDay;
}
public void setTransitDay(Integer transitDay) {
	this.transitDay = transitDay;
}
public Double getRtWt() {
	return rtWt;
}
public void setRtWt(Double rtWt) {
	this.rtWt = rtWt;
}
public Double getBaseFreight() {
	return baseFreight;
}
public void setBaseFreight(Double baseFreight) {
	this.baseFreight = baseFreight;
}
public Double getDsc() {
	return dsc;
}
public void setDsc(Double dsc) {
	this.dsc = dsc;
}
public Double getFsc() {
	return fsc;
}
public void setFsc(Double fsc) {
	this.fsc = fsc;
}
public Double getAccessorialCharge() {
	return accessorialCharge;
}
public void setAccessorialCharge(Double accessorialCharge) {
	this.accessorialCharge = accessorialCharge;
}
public Double getRevenue() {
	return revenue;
}
public void setRevenue(Double revenue) {
	this.revenue = revenue;
}
public Double getMargin() {
	return margin;
}
public void setMargin(Double margin) {
	this.margin = margin;
}
public List<CarrierAccessorial> getAccessorials() {
	return accessorials;
}
public void setAccessorials(List<CarrierAccessorial> accessorials) {
	this.accessorials = accessorials;
}
@Override
public String toString() {
	return "Cost [selected=" + selected + ", carrierCd=" + carrierCd + ", contractNumber="
			+ contractNumber + ", mileage=" + mileage + ", transitDay=" + transitDay + ", rtWt=" + rtWt
			+ ", baseFreight=" + baseFreight + ", dsc=" + dsc + ", fsc=" + fsc + ", accessorialCharge="
			+ accessorialCharge + ", revenue=" + revenue + ", margin=" + margin + ", totalCharge=" + totalCharge
			+ ", accessorials=" + accessorials + "]";
}


}
