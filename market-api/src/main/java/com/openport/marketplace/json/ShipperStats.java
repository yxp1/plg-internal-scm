/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;





/**
 * @author oscar_2
 *
 */
@XmlRootElement
@Entity
public class ShipperStats {
	


	@Column(name="my_active_bids_count")
	private int myActiveBidsCount;
	
	@Column(name="my_pending_booking_count")
	private int myPendingBookingCount;
	
	@Column(name="my_booked_shipment")
	private int myBookedShipment;

	public int getMyActiveBidsCount() {
		return myActiveBidsCount;
	}

	public void setMyActiveBidsCount(int myActiveBidsCount) {
		this.myActiveBidsCount = myActiveBidsCount;
	}

	public int getMyPendingBookingCount() {
		return myPendingBookingCount;
	}

	public void setMyPendingBookingCount(int myPendingBookingCount) {
		this.myPendingBookingCount = myPendingBookingCount;
	}

	public int getMyBookedShipment() {
		return myBookedShipment;
	}

	public void setMyBookedShipment(int myBookedShipment) {
		this.myBookedShipment = myBookedShipment;
	}
	
	
	
	

}
