package com.openport.marketplace.json;

import java.util.Date;

public class TransportationLeg {
	
	//id
	private String partnerCd;
	private String parentPartnerCd;
	private String shipperCd;
	private String customerCd;
	private String poNbr;
	private String releaseNbr;
	private int equipmentShipmentSeqNbr;
	
	
	private Date createTstamp;
	private String createUserId;
	private String updateTstamp;
	private String updateUserId;
	private char legCompletedFlg;
	private Integer transportLegNbr;
	private String pickupCityName;
	private String pickupStateCd;
	private String pickupCountryCd;
	private String pickupLocationName;
	private String pickupPostalCd;
	private String deliveryCityName;
	private String deliveryStateCd;
	private String deliveryCountryCd;
	private String deliveryLocationName;
	private String deliveryPostalCd;
	private String preferredCarrierCd;
	}
