package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Timezone {
private String name;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

}
