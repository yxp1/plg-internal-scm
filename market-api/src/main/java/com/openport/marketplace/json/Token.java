/**
 * 
 */
package com.openport.marketplace.json;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author oscar_2
 *
 */
@XmlRootElement
public class Token {

	private String accessToken;
	private Date expires;
	
	/**
	 * 
	 */
	public Token() {
		// TODO Auto-generated constructor stub
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

}
