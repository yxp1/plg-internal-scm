/**
 * 
 */
package com.openport.marketplace.json;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

//import com.openport.marketplace.model.EquipmentType;
//import com.openport.marketplace.model.FileDocumentType;
//import com.openport.marketplace.model.Service;
//import com.openport.marketplace.model.ServiceArea;

/**
 * @author Relly
 *
 */
@XmlRootElement
public class VwCarrierProfile {
	
	private String certifications;
	private int 	companyId;
	private String 	companyName;
	private String 	companyCode;
	private int 	yearsOfOperation;	
	private int   rating;
	private String 	companyAddress;
	private String hqCity;
	
	private String 	hqProvince;
	private String 	hqCountry;
	private String 	truckPoolAddress;
	private String 	branchAddress;
	private String 	contactName;
	private String 	emailAddress;
	
	
	private String 	companyLogoPath;
	private String 	phoneNbr;
	private String  businessHeadline;
	private String  businessDescription;	
	
	private ArrayList<com.openport.marketplace.json.CompanyEquipmentList> companyEquipmentList;
	private ArrayList<com.openport.marketplace.json.CompanyFile> carrierPicturesList;
	
	private List<com.openport.marketplace.json.ServiceArea> carrierServiceAreasList;
	private List<com.openport.marketplace.json.Service> carrierServicesList;
	private List<com.openport.marketplace.json.User> carrierUsersList;
		
	/**
	 * 
	 */	
	public VwCarrierProfile() {
		// TODO Auto-generated constructor stub
	}	
	
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public int getYearsOfOperation() {
		return yearsOfOperation;
	}
	public void setYearsOfOperation(int yearsOfOperation) {
		this.yearsOfOperation = yearsOfOperation;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getHqProvince() {
		return hqProvince;
	}
	public void setHqProvince(String hqProvince) {
		this.hqProvince = hqProvince;
	}
	public String getHqCountry() {
		return hqCountry;
	}
	public void setHqCountry(String hqCountry) {
		this.hqCountry = hqCountry;
	}
	public String getTruckPoolAddress() {
		return truckPoolAddress;
	}
	public void setTruckPoolAddress(String truckPoolAddress) {
		this.truckPoolAddress = truckPoolAddress;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public String getPhoneNbr() {
		return phoneNbr;
	}

	public void setPhoneNbr(String phoneNbr) {
		this.phoneNbr = phoneNbr;
	}

	public String getBusinessHeadline() {
		return businessHeadline;
	}

	public void setBusinessHeadline(String businessHeadline) {
		this.businessHeadline = businessHeadline;
	}

	public String getBusinessDescription() {
		return businessDescription;
	}

	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}

	public ArrayList<CompanyEquipmentList> getCompanyEquipmentList() {
		return companyEquipmentList;
	}

	public void setCompanyEquipmentList(ArrayList<CompanyEquipmentList> companyEquipmentList) {
		this.companyEquipmentList = companyEquipmentList;
	}

	public ArrayList<com.openport.marketplace.json.CompanyFile> getCarrierPicturesList() {
		return carrierPicturesList;
	}

	public void setCarrierPicturesList(ArrayList<com.openport.marketplace.json.CompanyFile> carrierPicturesList) {
		this.carrierPicturesList = carrierPicturesList;
	}

	public List<ServiceArea> getCarrierServiceAreasList() {
		return carrierServiceAreasList;
	}

	public void setCarrierServiceAreasList(List<ServiceArea> carrierServiceAreasList) {
		this.carrierServiceAreasList = carrierServiceAreasList;
	}

	public List<Service> getCarrierServicesList() {
		return carrierServicesList;
	}

	public void setCarrierServicesList(List<Service> carrierServicesList) {
		this.carrierServicesList = carrierServicesList;
	}

	public List<com.openport.marketplace.json.User> getCarrierUsersList() {
		return carrierUsersList;
	}

	public void setCarrierUsersList(List<com.openport.marketplace.json.User> carrierUsersList) {
		this.carrierUsersList = carrierUsersList;
	}

	public String getCertifications() {
		return certifications;
	}

	public void setCertifications(String certifications) {
		this.certifications = certifications;
	}

	public String getHqCity() {
		return hqCity;
	}

	public void setHqCity(String hqCity) {
		this.hqCity = hqCity;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	
	
}
