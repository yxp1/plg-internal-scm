package com.openport.marketplace.json;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class UserColumns {
private String colName;

public String getColumnName() {
	return colName;
}

public String getColName() {
	return colName;
}

public void setColName(String colName) {
	this.colName = colName;
}




}
