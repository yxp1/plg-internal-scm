package com.openport.marketplace.json;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TrackTrace {
	private String createUserId;
	private Integer activityId;
	private String eventDt;
	private Long uuid;
	private String eventDesc;
	private String bolNbr;
	private String proBillNbr;
	private String poNbr;
	private String poReleaseNbr;
	private String location;
	private String comments;
	private Boolean is_deleted;
	private String bcTransaction;
	private String eventCode;
	private Date sortDate;
	private String bcTransactionId;

	public String getBcTransaction() {
		return bcTransaction;
	}

	public void setBcTransaction(String bcTransaction) {
		this.bcTransaction = bcTransaction;
	}

	public Date getSortDate() {
		return sortDate;
	}

	public void setSortDate(Date sortDate) {
		this.sortDate = sortDate;
	}

	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	private List<ProofDocument> proofDocument;

	public List<ProofDocument> getProofDocument() {
		return proofDocument;
	}

	public void setProofDocument(List<ProofDocument> proofDocument) {
		this.proofDocument = proofDocument;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public String getEventDt() {
		return eventDt;
	}

	public void setEventDt(String eventDt) {
		this.eventDt = eventDt;
	}

	public Long getUuid() {
		return uuid;
	}

	public void setUuid(Long uuid) {
		this.uuid = uuid;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public String getBolNbr() {
		return bolNbr;
	}

	public void setBolNbr(String bolNbr) {
		this.bolNbr = bolNbr;
	}

	public String getProBillNbr() {
		return proBillNbr;
	}

	public void setProBillNbr(String proBillNbr) {
		this.proBillNbr = proBillNbr;
	}

	public String getPoNbr() {
		return poNbr;
	}

	public void setPoNbr(String poNbr) {
		this.poNbr = poNbr;
	}

	public String getPoReleaseNbr() {
		return poReleaseNbr;
	}

	public void setPoReleaseNbr(String poReleaseNbr) {
		this.poReleaseNbr = poReleaseNbr;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getBcTransactionId() {
		return bcTransactionId;
	}

	public void setBcTransactionId(String bcTransactionId) {
		this.bcTransactionId = bcTransactionId;
	}
	
}
