/**
 * 
 */
package com.openport.marketplace.json;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Rodel
 *
 */
@XmlRootElement
@Entity
public class EntityAddress {
	
	@Column(name="entity_Address_Id")
	private int entityAddressId;
	
	@Column(name="entity_Name")
	private String entityName;
	
	@Column(name="floor_Apartment")
	private String floorApartment;
	
	@Column(name="building")
	private String building;
	
	@Column(name="house_Number")
	private String houseNumber;
	
	@Column(name="line1")
	private String line1;
	
	@Column(name="street_Name")
	private String streetName;
	
	@Column(name="city_Locality")
	private String cityLocality;
	
	@Column(name="province")
	private String province;
	
	@Column(name="country")
	private String country;
	
	@Column(name="postal_Code")
	private String postalCode;
	
	@Column(name="district_Name")
	private String districtName;
	
	@Column(name="latitude")
	private BigDecimal lat;
	
	@Column(name="longitude")
	private BigDecimal lon;
	
	@Column(name="lat_Long_Status_Cd")
	private String latLongStatusCd;
	
	@Column(name="is_Dc")
	private boolean isDc;
	


	public EntityAddress() {
	}

	public EntityAddress(boolean isDc) {
		this.isDc = isDc;
	}


	public Integer getEntityAddressId() {
		return this.entityAddressId;
	}

	public void setEntityAddressId(Integer entityAddressId) {
		this.entityAddressId = entityAddressId;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getFloorApartment() {
		return this.floorApartment;
	}

	public void setFloorApartment(String floorApartment) {
		this.floorApartment = floorApartment;
	}

	public String getBuilding() {
		return this.building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getHouseNumber() {
		return this.houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getLine1() {
		return this.line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getStreetName() {
		return this.streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getCityLocality() {
		return this.cityLocality;
	}

	public void setCityLocality(String cityLocality) {
		this.cityLocality = cityLocality;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getDistrictName() {
		return this.districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	
	
	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLon() {
		return lon;
	}

	public void setLon(BigDecimal lon) {
		this.lon = lon;
	}

	public String getLatLongStatusCd() {
		return this.latLongStatusCd;
	}

	public void setLatLongStatusCd(String latLongStatusCd) {
		this.latLongStatusCd = latLongStatusCd;
	}

	public boolean isDc() {
		return this.isDc;
	}

	public void setIsDc(boolean isDc) {
		this.isDc = isDc;
	}

	public boolean getIsDc() {
		return this.isDc;
	}

	@Override
	public String toString() {
		return "EntityAddress [entityAddressId=" + entityAddressId + ", entityName=" + entityName + ", floorApartment="
				+ floorApartment + ", building=" + building + ", houseNumber=" + houseNumber + ", line1=" + line1
				+ ", streetName=" + streetName + ", cityLocality=" + cityLocality + ", province=" + province
				+ ", country=" + country + ", postalCode=" + postalCode + ", districtName=" + districtName
				+ ", latitude=" + lat + ", longitude=" + lon + ", latLongStatusCd=" + latLongStatusCd
				+ ", isDc=" + isDc + "]";
	}


}
