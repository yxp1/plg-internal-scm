package com.openport.marketplace.json;

public class BCModel {
	private Long activityId;
	private String bcHashValue;
	private String proBillNbr;
	private String status;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public String getBcHashValue() {
		return bcHashValue;
	}
	public void setBcHashValue(String bcHashValue) {
		this.bcHashValue = bcHashValue;
	}
	public String getProBillNbr() {
		return proBillNbr;
	}
	public void setProBillNbr(String proBillNbr) {
		this.proBillNbr = proBillNbr;
	}

}
