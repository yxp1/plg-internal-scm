/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity	
public class CompanyService {
	@Column(name="company_service_id")
	private Integer companyServiceId;
	@Column(name="company_Id")
	private Integer companyId;
	@Column(name="service_id")
	private Integer serviceId;
	@Column(name="create_user_id")
	private Integer createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private Integer updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;
	@Column(name="rating_value")
	private Integer ratingValue;
	private Service service;

	/**
	 * 
	 */
	public CompanyService() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCompanyServiceId() {
		return companyServiceId;
	}

	public void setCompanyServiceId(Integer companyServiceId) {
		this.companyServiceId = companyServiceId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Integer getRatingValue() {
		return ratingValue;
	}

	public void setRatingValue(Integer ratingValue) {
		this.ratingValue = ratingValue;
	}

	
	

	

}
