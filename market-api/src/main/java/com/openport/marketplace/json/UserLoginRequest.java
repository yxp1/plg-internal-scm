/**
 * 
 */
package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author oscar_2
 *
 */
@XmlRootElement
public class UserLoginRequest {

	private String username;
	private String password; // or accessToken
//	private Application app;
//	private Device device;
//	private String language;
//	private String networkType;
	
	/**
	 * 
	 */
	public UserLoginRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
/*
	public Application getApp() {
		return app;
	}

	public void setApp(Application app) {
		this.app = app;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
*/
}
