/**
 * 
 */
package com.openport.marketplace.json;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Relly
 *
 */
@XmlRootElement
@Entity
public class ShipmentDetails {

	private String shipmentDetailTitle;
	private String shipmentDetailVal;
	
	
	
	public ShipmentDetails() {
		
	}



	public String getShipmentDetailTitle() {
		return shipmentDetailTitle;
	}



	public void setShipmentDetailTitle(String shipmentDetailTitle) {
		this.shipmentDetailTitle = shipmentDetailTitle;
	}



	public String getShipmentDetailVal() {
		return shipmentDetailVal;
	}



	public void setShipmentDetailVal(String shipmentDetailVal) {
		this.shipmentDetailVal = shipmentDetailVal;
	}
	
	
	

	
	
}
