/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity	
public class Country {
	@Column(name="country_id")
	private int countryId;
	@Column(name="country_name")
	private String countryName;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;
	@Column(name="country_3_Cd")
	private String country3Cd;
	@Column(name="country_2_Cd")
	private String country2Cd;
	@Column(name="country_cd")
	private String countryCd;
	/**
	 * 
	 */
	public Country() {
		// TODO Auto-generated constructor stub
	}

	public String getCountryCd() {
		return countryCd;
	}

	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public String getCountry3Cd() {
		return country3Cd;
	}

	public void setCountry3Cd(String country3Cd) {
		this.country3Cd = country3Cd;
	}

	public String getCountry2Cd() {
		return country2Cd;
	}

	public void setCountry2Cd(String country2Cd) {
		this.country2Cd = country2Cd;
	}
	
	

}
