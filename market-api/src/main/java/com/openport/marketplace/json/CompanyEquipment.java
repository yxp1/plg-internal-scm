/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class CompanyEquipment {
	@Column(name="company_equipment_id")
	private int companyEquipmentId;
	@Column(name="company_id")
	private int companyId;
	@Column(name="equipment_id")
	private int equipmentId;
	@Column(name="equipment_count")
	private double equipmentCount;
	@Column(name="create_user_id")
	private int createUserId;
	@Column(name="create_dt")
	private Timestamp createDt;
	@Column(name="update_user_id")
	private int updateUserId;
	@Column(name="update_dt")
	private Timestamp updateDt;
	
	private Equipment equipment;
	/**
	 * 
	 */
	public CompanyEquipment() {
		// TODO Auto-generated constructor stub
	}

	public int getCompanyEquipmentId() {
		return companyEquipmentId;
	}

	public void setCompanyEquipmentId(int companyEquipmentId) {
		this.companyEquipmentId = companyEquipmentId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(int equipmentId) {
		this.equipmentId = equipmentId;
	}

	public double getEquipmentCount() {
		return equipmentCount;
	}

	public void setEquipmentCount(double equipmentCount) {
		this.equipmentCount = equipmentCount;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

}
