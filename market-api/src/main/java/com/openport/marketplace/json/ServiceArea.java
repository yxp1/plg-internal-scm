/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class ServiceArea {
	@Column(name = "service_area_id")
	private int serviceAreaId;
	@Column(name = "service_area_name")
	private String serviceAreaName;
	@Column(name = "create_user_id")
	private int createUserId;
	@Column(name = "create_dt")
	private Timestamp createDt;
	@Column(name = "update_user_id")
	private int updateUserId;
	@Column(name = "update_dt")
	private Timestamp updateDt;
	@Column(name = "company_id")
	private int companyId;
	@Column(name = "status")
	private Boolean status;
	@Column(name = "carrier_region_id")
	private Integer carrierRegionId;

	@Column(name = "country_id")
	private Integer countryId;
	@Column(name = "country_name")
	private String countryName;
	@Column(name = "country_2_Cd")
	private String country2Cd;

	/**
	 * 
	 */
	public ServiceArea() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the serviceAreaId
	 */
	public int getServiceAreaId() {
		return serviceAreaId;
	}

	/**
	 * @param serviceAreaId
	 *            the serviceAreaId to set
	 */
	public void setServiceAreaId(int serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	/**
	 * @return the serviceAreaName
	 */
	public String getServiceAreaName() {
		return serviceAreaName;
	}

	/**
	 * @param serviceAreaName
	 *            the serviceAreaName to set
	 */
	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}

	/**
	 * @return the createUserId
	 */
	public int getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId
	 *            the createUserId to set
	 */
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the createDt
	 */
	public Timestamp getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt
	 *            the createDt to set
	 */
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the updateUserId
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId
	 *            the updateUserId to set
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the updateDt
	 */
	public Timestamp getUpdateDt() {
		return updateDt;
	}

	/**
	 * @param updateDt
	 *            the updateDt to set
	 */
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getCarrierRegionId() {
		return carrierRegionId;
	}

	public void setCarrierRegionId(Integer carrierRegionId) {
		this.carrierRegionId = carrierRegionId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountry2Cd() {
		return country2Cd;
	}

	public void setCountry2Cd(String country2Cd) {
		this.country2Cd = country2Cd;
	}

	

}
