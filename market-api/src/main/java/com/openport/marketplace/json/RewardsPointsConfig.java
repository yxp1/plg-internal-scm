package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;

@XmlRootElement
@Entity
public class RewardsPointsConfig {
	@Column(name="id")
	private Integer id;
	@Column(name="shipper_id")
	private Integer shipperId;
	@Column(name="shipper_cd")
	private String shipperCd;
	@Column(name="carrier_id")
	private Integer carrierId;
	@Column(name="carrier_cd")
	private String carrierCd;
	@Column(name="event")
	private String event;
	@Column(name="description")
	private String description;
	@Column(name="points")
	private Integer points;
	@Column(name="default_status")
	private String defaultStatus;
	@Column(name="start_date")
	private Date startDate;
	@Column(name="termination_date")
	private Date terminationDate;
	@Column(name="pingcount")
	private Integer pingcount;
	 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getShipperId() {
		return shipperId;
	}
	public void setShipperId(Integer shipperId) {
		this.shipperId = shipperId;
	}
	public String getShipperCd() {
		return shipperCd;
	}
	public void setShipperCd(String shipperCd) {
		this.shipperCd = shipperCd;
	}
	public Integer getCarrierId() {
		return carrierId;
	}
	public void setCarrierId(Integer carrierId) {
		this.carrierId = carrierId;
	}
	public String getCarrierCd() {
		return carrierCd;
	}
	public void setCarrierCd(String carrierCd) {
		this.carrierCd = carrierCd;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public String getDefaultStatus() {
		return defaultStatus;
	}
	public void setDefaultStatus(String defaultStatus) {
		this.defaultStatus = defaultStatus;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	public void setPingcount(Integer pingcount) {
		this.pingcount = pingcount;
	}
	public Integer getPingcount() {
		return pingcount;
	}
}
