/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class CompanyClient {

	@Column(name="company_Client_Id")
	private int companyClientId;
	@Column(name="company_Id")
	private int companyId;
	@Column(name="client_Name")
	private String clientName;
	@Column(name="create_User_Id")
	private int createUserId;
	@Column(name="create_Dt")
	private Timestamp createDt;
	@Column(name="update_User_Id")
	private int updateUserId;
	@Column(name="update_Dt")
	private Timestamp updateDt;

	/**
	 * 
	 */
	public CompanyClient() {
		// TODO Auto-generated constructor stub
	}

	public int getCompanyClientId() {
		return companyClientId;
	}

	public void setCompanyClientId(int companyClientId) {
		this.companyClientId = companyClientId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
