/**
 * 
 */
package com.openport.marketplace.json;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * @author Oscar
 *
 */
@XmlRootElement
@Entity
public class CompanyBranch {

	@Column(name="company_Branch_Id")
	private Integer companyBranchId;
	@Column(name="Branch_Name")
	private String branchName;
	@Column(name="company_Id")
	private Integer companyId;
	@Column(name="company_Address_Id")
	private int companyAddressId;


	@Column(name="is_Truck_Pool")
	private Boolean isTruckPool;
	@Column(name="create_User_Id")
	private Integer createUserId;
	@Column(name="create_Dt")
	private Timestamp createDt;
	@Column(name="update_User_Id")
	private Integer updateUserId;
	@Column(name="update_Dt")
	private Timestamp updateDt;
	private CompanyAddress branchAddress;
	
	public CompanyBranch() {}

	public Integer getCompanyBranchId() {
		return companyBranchId;
	}

	public void setCompanyBranchId(Integer companyBranchId) {
		this.companyBranchId = companyBranchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public int getCompanyAddressId() {
		return companyAddressId;
	}

	public void setCompanyAddressId(int companyAddressId) {
		this.companyAddressId = companyAddressId;
	}

	public Boolean getIsTruckPool() {
		return isTruckPool;
	}

	public Boolean isTruckPool() {
		return isTruckPool;
	}

	public void setIsTruckPool(Boolean isTruckPool) {
		this.isTruckPool = isTruckPool;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}
	
	public CompanyAddress getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(CompanyAddress branchAddress) {
		this.branchAddress = branchAddress;
	}

}
