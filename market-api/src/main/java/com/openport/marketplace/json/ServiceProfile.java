package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServiceProfile {
private String name;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public ServiceProfile(String name) {
	super();
	this.name = name;
}

}
