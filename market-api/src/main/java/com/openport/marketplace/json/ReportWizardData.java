package com.openport.marketplace.json;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReportWizardData {
private List<UserCriteria> criteriaList;
private List<UserSort> sortList;
private String reportFormat;
private String dataSource;
private String availableReport;
private String filename;
private Boolean isFileCreated;
private String userName;
private String reportUrlPath;
private String reportLocalPath;
private Boolean isNewReport;
private Boolean isUpdateReport;
private String reportName;
private String sqlScript;


//denormailized
private ReportSchedule reportSchedule;




public String getSqlScript() {
	return sqlScript;
}
public void setSqlScript(String sqlScript) {
	this.sqlScript = sqlScript;
}
public String getReportName() {
	return reportName;
}
public void setReportName(String reportName) {
	this.reportName = reportName;
}

public String getReportLocalPath() {
	return reportLocalPath;
}
public void setReportLocalPath(String reportLocalPath) {
	this.reportLocalPath = reportLocalPath;
}
public String getReportUrlPath() {
	return reportUrlPath;
}
public void setReportUrlPath(String reportUrlPath) {
	this.reportUrlPath = reportUrlPath;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public Boolean getIsFileCreated() {
	return isFileCreated;
}
public void setIsFileCreated(Boolean isFileCreated) {
	this.isFileCreated = isFileCreated;
}
public String getFilename() {
	return filename;
}
public void setFilename(String filename) {
	this.filename = filename;
}
public String getDataSource() {
	return dataSource;
}
public void setDataSource(String dataSource) {
	this.dataSource = dataSource;
}
public String getAvailableReport() {
	return availableReport;
}
public void setAvailableReport(String availableReport) {
	this.availableReport = availableReport;
}
public String getReportFormat() {
	return reportFormat;
}
public void setReportFormat(String reportFormat) {
	this.reportFormat = reportFormat;
}
public List<UserCriteria> getCriteriaList() {
	return criteriaList;
}
public void setCriteriaList(List<UserCriteria> criteriaList) {
	this.criteriaList = criteriaList;
}
public List<UserSort> getSortList() {
	return sortList;
}
public void setSortList(List<UserSort> sortList) {
	this.sortList = sortList;
}
public Boolean getIsNewReport() {
	return isNewReport;
}
public void setIsNewReport(Boolean isNewReport) {
	this.isNewReport = isNewReport;
}
public Boolean getIsUpdateReport() {
	return isUpdateReport;
}
public void setIsUpdateReport(Boolean isUpdateReport) {
	this.isUpdateReport = isUpdateReport;
}
public ReportSchedule getReportSchedule() {
	return reportSchedule;
}
public void setReportSchedule(ReportSchedule reportSchedule) {
	this.reportSchedule = reportSchedule;
}


}
