package com.openport.marketplace.json;

import java.util.Date;

public class StageTenderLane {
	private int companyContractTenderLaneId;
	private int shipperCompanyId;
	private int laneId;
	private String commodityCd;
	private String ratePerUnitCd;
	private int equipmentTypeId;
	private Date rateEffectiveDt;
	private Date rateTerminationDt;
	private String contractNbr;
	private String statusCd;
	private int carrierCompanyId;
	private int bidRank;
	private Double rateAmt;
	private Double accessorialAmt;
	private String currencyCd;
	private String commentTxt;
	private Date createDt;
	private String createUserId;
	private Date updateDt;
	private String updateUserId;
	private int groupTenderId;
	
	
	
	
	public int getGroupTenderId() {
		return groupTenderId;
	}
	public void setGroupTenderId(int groupTenderId) {
		this.groupTenderId = groupTenderId;
	}
	public int getCompanyContractTenderLaneId() {
		return companyContractTenderLaneId;
	}
	public void setCompanyContractTenderLaneId(int companyContractTenderLaneId) {
		this.companyContractTenderLaneId = companyContractTenderLaneId;
	}
	public int getEquipmentTypeId() {
		return equipmentTypeId;
	}
	public void setEquipmentTypeId(int equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}
	public int getShipperCompanyId() {
		return shipperCompanyId;
	}
	public void setShipperCompanyId(int shipperCompanyId) {
		this.shipperCompanyId = shipperCompanyId;
	}
	public int getLaneId() {
		return laneId;
	}
	public void setLaneId(int laneId) {
		this.laneId = laneId;
	}
	public String getCommodityCd() {
		return commodityCd;
	}
	public void setCommodityCd(String commodityCd) {
		this.commodityCd = commodityCd;
	}
	public String getRatePerUnitCd() {
		return ratePerUnitCd;
	}
	public void setRatePerUnitCd(String ratePerUnitCd) {
		this.ratePerUnitCd = ratePerUnitCd;
	}
	public Date getRateEffectiveDt() {
		return rateEffectiveDt;
	}
	public void setRateEffectiveDt(Date rateEffectiveDt) {
		this.rateEffectiveDt = rateEffectiveDt;
	}
	public Date getRateTerminationDt() {
		return rateTerminationDt;
	}
	public void setRateTerminationDt(Date rateTerminationDt) {
		this.rateTerminationDt = rateTerminationDt;
	}
	public String getContractNbr() {
		return contractNbr;
	}
	public void setContractNbr(String contractNbr) {
		this.contractNbr = contractNbr;
	}
	public String getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}
	public int getCarrierCompanyId() {
		return carrierCompanyId;
	}
	public void setCarrierCompanyId(int carrierCompanyId) {
		this.carrierCompanyId = carrierCompanyId;
	}

	public int getBidRank() {
		return bidRank;
	}
	public void setBidRank(int bidRank) {
		this.bidRank = bidRank;
	}
	public Double getRateAmt() {
		return rateAmt;
	}
	public void setRateAmt(Double rateAmt) {
		this.rateAmt = rateAmt;
	}
	public Double getAccessorialAmt() {
		return accessorialAmt;
	}
	public void setAccessorialAmt(Double accessorialAmt) {
		this.accessorialAmt = accessorialAmt;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public String getCommentTxt() {
		return commentTxt;
	}
	public void setCommentTxt(String commentTxt) {
		this.commentTxt = commentTxt;
	}
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public Date getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	@Override
	public String toString() {
		return "CompanyContractTenderLane [shipperCompanyId=" + shipperCompanyId + ", laneId=" + laneId
				+ ", commodityCd=" + commodityCd + ", ratePerUnitCd=" + ratePerUnitCd + ", equipmentTypeId="
				+ equipmentTypeId + ", rateEffectiveDt=" + rateEffectiveDt + ", rateTerminationDt=" + rateTerminationDt
				+ ", contractNbr=" + contractNbr + ", statusCd=" + statusCd + ", carrierCompanyId=" + carrierCompanyId
				+ ", bidRank=" + bidRank + ", rateAmt=" + rateAmt + ", accessorialAmt=" + accessorialAmt
				+ ", currencyCd=" + currencyCd + ", commentTxt=" + commentTxt + ", createDt=" + createDt
				+ ", createUserId=" + createUserId + ", updateDt=" + updateDt + ", updateUserId=" + updateUserId + "]";
	}
	
}
