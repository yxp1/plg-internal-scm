package com.openport.marketplace.json;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FileDocumentType implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6065928973881804349L;
	private Integer fileDocumentTypeId;
	private CompanyFile companyFile;
	private DocumentType documentType;
	

	public FileDocumentType() {
	}

	public Integer getFileDocumentTypeId() {
		return this.fileDocumentTypeId;
	}

	public void setFileDocumentTypeId(Integer fileDocumentTypeId) {
		this.fileDocumentTypeId = fileDocumentTypeId;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public CompanyFile getCompanyFile() {
		return companyFile;
	}

	public void setCompanyFile(CompanyFile companyFile) {
		this.companyFile = companyFile;
	}

	

}
