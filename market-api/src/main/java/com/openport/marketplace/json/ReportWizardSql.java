/**
 * 
 */
package com.openport.marketplace.json;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Nargel
 *
 */
@XmlRootElement
@Entity
public class ReportWizardSql implements Serializable{
	@Column(name="report_name")
	private String reportName;
	@Column(name="user_id")
	private String userId ;
	
	@Column(name="report_sql_txt")
	private String reportSqlTxt ;
	
	@Column(name="report_output_format")
	private String reportOutputFormat ;
	
	@Column(name="time_zone_country")
	private String timeZoneCountry ;
	
	@Column(name="time_zone_offset")
	private String timeZoneOffset ;
	
	
	//denormalized
	
	private List<UserColumns> userColumns;
	private List<UserSort> userSort;
	private List<UserCriteria> userCriteria;
	
	private ReportSchedule reportSchedule;
	
	
	
	

	public ReportSchedule getReportSchedule() {
		return reportSchedule;
	}
	public void setReportSchedule(ReportSchedule reportSchedule) {
		this.reportSchedule = reportSchedule;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getReportSqlTxt() {
		return reportSqlTxt;
	}
	public void setReportSqlTxt(String reportSqlTxt) {
		this.reportSqlTxt = reportSqlTxt;
	}
	public String getReportOutputFormat() {
		return reportOutputFormat;
	}
	public void setReportOutputFormat(String reportOutputFormat) {
		this.reportOutputFormat = reportOutputFormat;
	}
	public String getTimeZoneCountry() {
		return timeZoneCountry;
	}
	public void setTimeZoneCountry(String timeZoneCountry) {
		this.timeZoneCountry = timeZoneCountry;
	}
	public String getTimeZoneOffset() {
		return timeZoneOffset;
	}
	public void setTimeZoneOffset(String timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}
	public List<UserColumns> getUserColumns() {
		return userColumns;
	}
	public void setUserColumns(List<UserColumns> userColumns) {
		this.userColumns = userColumns;
	}
	public List<UserSort> getUserSort() {
		return userSort;
	}
	public void setUserSort(List<UserSort> userSort) {
		this.userSort = userSort;
	}
	public List<UserCriteria> getUserCriteria() {
		return userCriteria;
	}
	public void setUserCriteria(List<UserCriteria> userCriteria) {
		this.userCriteria = userCriteria;
	}

	
}
