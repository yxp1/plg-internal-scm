package com.openport.marketplace.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class PoHeader {
@Column(name="PARENT_PARTNER_CD")	
private String parentPartnerCd;
@Column(name="PARTNER_CD")
private String partnerCd;
@Column(name="SHIPPER_CD")
private String shipperCd;
@Column(name="CUSTOMER_CD")
private String customerCd;
@Column(name="PO_NBR")
private String poNbr;
@Column(name="RELEASE_NBR")
private String releaseNbr;
@Column(name="PO_TYPE_CD")
private String poTypeCd;
@Column(name="PRO_BILL_NBR")
private String proBillNbr;

public String getProBillNbr() {
	return proBillNbr;
}
public void setProBillNbr(String proBillNbr) {
	this.proBillNbr = proBillNbr;
}
public String getParentPartnerCd() {
	return parentPartnerCd;
}
public void setParentPartnerCd(String parentPartnerCd) {
	this.parentPartnerCd = parentPartnerCd;
}
public String getPartnerCd() {
	return partnerCd;
}
public void setPartnerCd(String partnerCd) {
	this.partnerCd = partnerCd;
}
public String getShipperCd() {
	return shipperCd;
}
public void setShipperCd(String shipperCd) {
	this.shipperCd = shipperCd;
}
public String getCustomerCd() {
	return customerCd;
}
public void setCustomerCd(String customerCd) {
	this.customerCd = customerCd;
}
public String getPoNbr() {
	return poNbr;
}
public void setPoNbr(String poNbr) {
	this.poNbr = poNbr;
}
public String getReleaseNbr() {
	return releaseNbr;
}
public void setReleaseNbr(String releaseNbr) {
	this.releaseNbr = releaseNbr;
}
public String getPoTypeCd() {
	return poTypeCd;
}
public void setPoTypeCd(String poTypeCd) {
	this.poTypeCd = poTypeCd;
}


}
