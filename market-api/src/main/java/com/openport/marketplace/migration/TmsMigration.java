package com.openport.marketplace.migration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.Shipment;
import com.openport.marketplace.json.ShipmentDetail;
import com.openport.marketplace.model.Partner;
import com.openport.marketplace.repository.DbConfigODS;
import com.openport.marketplace.repository.DbConfigTmsUtf8;

public class TmsMigration {
	static final transient Logger log = Logger.getLogger(TmsMigration.class);
	
	public static void doMapping_MP_Shipment_PoHeader(Shipment shipment, Partner partner) {
		String sql = "insert into po_header ("
				   + "partner_cd,"
				   + "parent_partner_cd,"
				   + "shipper_cd,"
				   + "customer_cd,"
				   + "po_nbr,"
				   + "release_nbr,"
				   + "equipment_shipment_seq_nbr,"
				   + "release_type,"
				   + "po_type_cd,"
				   + "request_dt,"
				   + "internal_status_cd,"
				   + "movement_type_cd,"
				   + "transport_mode_cd,"
				   + "available_dt,"
				   + "requested_delivery_dt,"
				   + "create_tstamp,"
				   + "create_user_id,"
				   + "total_qty,"
				   + "total_wt,"
				   + "total_vol,"
				   + "wt_uom,"
				   + "vol_uom,"
				   + "pro_bill_nbr,"
				   + "ccy_cd, QTY_UOM) values("
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?)";
		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			
			pstmt.setString(1, shipment.getCompanyCd());
			pstmt.setString(2, partner.getParentPartnerCode());
			pstmt.setString(3, shipment.getCompanyCd());
			pstmt.setString(4, shipment.getCompanyCd());
			pstmt.setString(5, shipment.getShipmentNbr());
			pstmt.setString(6, shipment.getShipmentNbr());
			pstmt.setInt(7, 1);
			pstmt.setString(8, "MP");
			pstmt.setString(9, "DPO");
			pstmt.setTimestamp(10, new Timestamp(shipment.getRequestedPickupDt().getTime()));
			pstmt.setString(11, "SG");
			pstmt.setString(12, "T");
			pstmt.setString(13, "TL");
			pstmt.setTimestamp(14, new Timestamp(shipment.getRequestedPickupDt().getTime()));
			pstmt.setTimestamp(15, new Timestamp(shipment.getRequestedDeliveryDt().getTime()));
			pstmt.setTimestamp(16, new Timestamp(new Date().getTime()));
			pstmt.setString(17, shipment.getCreateUserId());
			pstmt.setDouble(18, shipment.getUnitCount());
			pstmt.setDouble(19, shipment.getUnitWt());
			pstmt.setDouble(20, shipment.getUnitVol());
			pstmt.setString(21, "KGS");
			pstmt.setString(22, "CBM");
			pstmt.setString(23, shipment.getProBillNbr());
		//	pstmt.setString(24, shipment.getPaymentTerm().getPaymentTermCd());
		//	pstmt.setString(25, shipment.getPaymentTerm().getPaymentTermCd());
			pstmt.setString(24, shipment.getCurrencyCd());
			pstmt.setString(25, shipment.getShipmentDetails().get(0).getQtyUom());
			
			int id = pstmt.executeUpdate();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static void deleteMarketplaceTms(String shipmentNumber) {
		String sql = "delete from po_header where po_nbr = ? and release_nbr = ? and release_type='MP'";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, shipmentNumber);
			pstmt.setString(2, shipmentNumber);
			pstmt.execute();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static void doMapping_MP_Shipment_PoReference(String companyCd,String poNbr,String releaseNbr,String referenceTypeCd,String locationName,
			String addr1,String addr2,String cityName,String stateName,String countryCd,String postalCd,String contactPersonName,
			String telephone,String email,String userId, int sequenceNumber, String telephoneNumber, String locationCode, String accountCode, String parentPartner)  {
		String sql="insert into po_reference_info ("
				   + "partner_cd,"
				   + "parent_partner_cd,"
				   + "shipper_cd,"
				   + "customer_cd,"
				   + "po_nbr,"
				   + "release_nbr,"
				   + "equipment_shipment_seq_nbr,"
				   + "reference_type_cd,"
				   + "seq_nbr,"
				   + "location_name,"
				   + "line_1_addr,"
				   + "line_2_addr,"
				   + "city_name,"
				   + "state_name,"
				   + "country_cd,"
				   + "postal_cd,"
				   + "contact_person_name,"
				   + "telephone_1_nbr,"
				   + "email_addr,"
				   + "create_tstamp,"
				   + "create_user_id,"
				   + "telephone_nbr,"
				   + "location_cd,"
				   + "account_cd)"
				   + " values("
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?)";
				   		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
									
			pstmt.setString(1, companyCd);
			pstmt.setString(2, parentPartner);
			pstmt.setString(3, companyCd);
			pstmt.setString(4, companyCd);
			pstmt.setString(5, poNbr);
			pstmt.setString(6, releaseNbr);
			pstmt.setInt(7, 1);
			pstmt.setString(8, referenceTypeCd);
			pstmt.setInt(9, sequenceNumber);
			pstmt.setString(10, locationName);
			pstmt.setString(11, addr1);
			pstmt.setString(12, addr2);
			pstmt.setString(13, cityName);
			pstmt.setString(14, stateName);
			pstmt.setString(15, countryCd);
			pstmt.setString(16, postalCd);
			pstmt.setString(17, contactPersonName);
			pstmt.setString(18, telephone);
			pstmt.setString(19, email);
			pstmt.setTimestamp(20, new Timestamp(new Date().getTime()));
			pstmt.setString(21, userId);
			pstmt.setString(22, telephoneNumber);
			pstmt.setString(23, locationCode);
			pstmt.setString(24, accountCode);
		
			int id = pstmt.executeUpdate();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static Integer migrateMPToTMS(String shipmentNumber) {
		if(validateMarketplaceTMS(shipmentNumber)) {
			
			log.info("deleting : *" + shipmentNumber + "*. " + shipmentNumber.length());
			
			deleteMarketplaceTms(shipmentNumber);
			deletePoReferenceInfo(shipmentNumber, shipmentNumber);
			deletePoLineNumber(shipmentNumber, shipmentNumber);
			deletePoEquipmentRequirement(shipmentNumber, shipmentNumber);
			deleteDataStaging(shipmentNumber);
			
			return 1;
		} else {
			return 0;
		}
	}
	
	public static boolean validateMarketplaceTMS(String shipmentNumber) {
		
//		String sql = "SELECT po_nbr from po_header where pro_bill_nbr='"+proBillNbr+"' and release_type='MP'";
		String sql = "select po_nbr from po_header where po_nbr = ? and release_nbr = ? and release_type='MP'";
				
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, shipmentNumber);
			pstmt.setString(2, shipmentNumber);
				
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
		return false;
}
	public static void doMapping_MP_Shipment_PoLineItem(ShipmentDetail detail,String companyCd,String poNbr,String releaseNbr,String userId,String commodityDescTxt,Integer lineNbr, String parentPartner, String unitPriceCurrencyCode)  {
		String sql="insert into po_line_item ("
				   + "partner_cd," //1
				   + "parent_partner_cd," //2
				   + "shipper_cd," //3
				   + "customer_cd," //4
				   + "po_nbr," //5
				   + "release_nbr," //6
				   + "equipment_shipment_seq_nbr," //7
				   + "product_cd," //8
				   + "po_line_item_nbr," //9
				   + "release_line_nbr," //11
				   + "product_desc_txt," //12
				
				   + "ordered_qty," //13
				   + "package_qty," //14
				   + "package_cnt," //15
				
				   + "package_uom," //16
				   + "qty_uom," //17
				   + "wt_uom," //18
				   + "vol_uom," //19
				   
				   + "total_wt," //20
				   + "ordered_wt," //21
				   + "package_wt," //22
				   + "unit_wt," //23
				   + "ordered_vol," //24
				   + "package_vol," //25
				   + "total_vol," //26
				   + "create_tstamp," //27
				   + "create_user_id," //28
				   + "UNIT_PRICE,"
				   + "UNIT_PRICE_CURRENCY_CD,"
				   + "goods_value_bill_to_cd"
				   + ")values("
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?)";
				   		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
									
			pstmt.setString(1, companyCd);
			pstmt.setString(2, parentPartner);
			pstmt.setString(3, companyCd);
			pstmt.setString(4, companyCd);
			pstmt.setString(5, poNbr);
			pstmt.setString(6, releaseNbr);
			pstmt.setInt(7, 1);
			
			if(detail.getProductCode() != null) {
				pstmt.setString(8, detail.getProductCode());
			} else {
				pstmt.setString(8, "FAK");
			}
			
			pstmt.setInt(9, lineNbr);
			pstmt.setInt(10, lineNbr);
			
			if(detail.getProductDescTxt() != null) {
				pstmt.setString(11,detail.getProductDescTxt());
			} else {
				pstmt.setString(11,"Freight Any Kind");
			}
			
			pstmt.setDouble(12,detail.getQty());
			pstmt.setDouble(13,detail.getQty());
			pstmt.setDouble(14,detail.getQty());
			
			pstmt.setString(15,detail.getUnitOfMeasure());
			pstmt.setString(16,detail.getUnitOfMeasure());
			pstmt.setString(17,detail.getWtUom());
			pstmt.setString(18,detail.getVolUom());
			
			pstmt.setDouble(19,detail.getShipmentWt());
			pstmt.setDouble(20,detail.getShipmentWt());
			pstmt.setDouble(21,detail.getShipmentWt());
			pstmt.setDouble(22,detail.getShipmentWt());
			
			pstmt.setDouble(23,detail.getShipmentVol());
			pstmt.setDouble(24,detail.getShipmentVol());
			pstmt.setDouble(25,detail.getShipmentVol());
			
			pstmt.setTimestamp(26, new Timestamp(new Date().getTime()));
			pstmt.setString(27, userId);
			
			pstmt.setDouble(28, detail.getUnitPrice());
			pstmt.setString(29, unitPriceCurrencyCode);
			pstmt.setString(30, detail.getGoodsValueBillToCode());
			
			int id = pstmt.executeUpdate();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static void doMapping_MP_Shipment_PoRequirement(String equipmentTypeCd,Integer equipmentQty,String equipmentDescTxt,String companyCd,String poNbr,String releaseNbr,String userId, String parentPartner)  {
		
		if(DbConfigODS.getDataSource() == null){
			System.out.println("DbConfigODS.getDataSource() is null");
		}
		
		String sql="insert into po_equipment_requirement ("
				   + "partner_cd," //1
				   + "parent_partner_cd," //2
				   + "shipper_cd," //3
				   + "customer_cd," //4
				   + "po_nbr," //5
				   + "release_nbr," //6
				   + "equipment_shipment_seq_nbr," //7
				   + "equipment_type_cd," //8
				   + "equipment_qty," //9
				   + "equipment_desc_txt" //10
				   + ")" //28
				   + " values("
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?)";
		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
									
			pstmt.setString(1, companyCd);
			pstmt.setString(2, parentPartner);
			pstmt.setString(3, companyCd);
			pstmt.setString(4, companyCd);
			pstmt.setString(5, poNbr);
			pstmt.setString(6, releaseNbr);
			pstmt.setInt(7, 1);
			pstmt.setString(8, equipmentTypeCd);
			pstmt.setInt(9, equipmentQty);
			pstmt.setString(10, equipmentDescTxt);
			
			int id = pstmt.executeUpdate();
			

		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static void createTrack(String companyCd,String probillNbr,String statusCd,String statusDesc,Date eventDt,String commentTxt,String createUserId)  {
		
		if(DbConfigODS.getDataSource() == null){
			System.out.println("DbConfigODS.getDataSource() is null");
		}
		
		String sql="insert into data_staging ("
				   + "activity_id," //1
				   + "probill_nbr," //2
				   + "application," //3
				   + "status_cd," //4
				   + "status_desc," //5
				   + "event_dt," //6
				   + "comment_txt," //7
				   + "create_user_id," //8
				   + "create_dt" //9
				   + ")" //28
				   + " values("
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?,"
				   + "?)";
		
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

			pstmt.setInt(1, -1);
			pstmt.setString(2, probillNbr);
			pstmt.setString(3, companyCd);
			pstmt.setString(4, statusCd);
			pstmt.setString(5, statusDesc);
			pstmt.setTimestamp(6, new Timestamp(eventDt.getTime()));
			pstmt.setString(7, commentTxt);
			pstmt.setString(8, createUserId);
			pstmt.setTimestamp(9, new Timestamp(new Date().getTime()));
			
			int id = pstmt.executeUpdate();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	
	public static void deletePoReferenceInfo(String purchaseOrderNumber, String releaseNumber)  {
		String sql= "delete from po_reference_info where po_nbr = ? and release_nbr = ?";
		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, purchaseOrderNumber);
			pstmt.setString(2, releaseNumber);
			pstmt.execute();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static void deletePoLineNumber(String purchaseOrderNumber, String releaseNumber)  {
		String sql= "delete from po_line_item where po_nbr = ? and release_nbr = ?";
		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, purchaseOrderNumber);
			pstmt.setString(2, releaseNumber);
			pstmt.execute();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static void deletePoEquipmentRequirement(String purchaseOrderNumber, String releaseNumber)  {
		String sql= "delete from po_equipment_requirement where po_nbr = ? and release_nbr = ?";
		
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, purchaseOrderNumber);
			pstmt.setString(2, releaseNumber);
			pstmt.execute();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
	public static void deleteDataStaging(String proBillNbr)  {
		String sql= "delete from data_staging where probill_nbr = ?";
		
		try (java.sql.Connection connection = DbConfigODS.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, proBillNbr);
			pstmt.execute();
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
	}
	
}
