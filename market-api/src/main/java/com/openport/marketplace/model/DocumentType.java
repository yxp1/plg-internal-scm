package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentType implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7518783771433849135L;
	private int documentTypeId;
	private String descriptionTxt;

	public DocumentType() {
	}
	public DocumentType(ResultSet rs) throws SQLException {
		this.documentTypeId                = rs.getInt("document_type_id");
		this.descriptionTxt               = rs.getString("description_txt");
	}
	public DocumentType(int documentTypeId, String descriptionTxt) {
		this.documentTypeId = documentTypeId;
		this.descriptionTxt = descriptionTxt;
	}

	public int getDocumentTypeId() {
		return this.documentTypeId;
	}

	public void setDocumentTypeId(int documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getDescriptionTxt() {
		return this.descriptionTxt;
	}

	public void setDescriptionTxt(String descriptionTxt) {
		this.descriptionTxt = descriptionTxt;
	}

}
