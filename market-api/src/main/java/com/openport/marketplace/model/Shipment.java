/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Rodel
 *
 */
public class Shipment {

	private int shipmentId;
	private int entityAddressByDeliveryAddressId;
	private int entityAddressByPickupAddressId;
	private String shipmentNbr;
	private int shipperId;
	private int statusId;
	private int assignedProviderId;
	private int requestedEquipmentTypeId;
	private Date requestedPickupDt;
	private Date requestedDeliveryDt;
	private int unitCount;
	private String unitOfMeasure;
	private Date createDt;
	private String createUserId;
	private Date updateDt;
	private String updateUserId;
	private Timestamp tenderExpirationDt;
	private Integer originServiceAreaId;
	private Integer destinationServiceAreaId;
	
	private String companyCd;
	
	
	public String getCompanyCd() {
		return companyCd;
	}

	public void setCompanyCd(String companyCd) {
		this.companyCd = companyCd;
	}

	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public Shipment(ResultSet rs) throws SQLException {
		this.shipmentId             			= rs.getInt("shipment_Id");
		this.entityAddressByDeliveryAddressId   = rs.getInt("entity_Address_By_Delivery_Address_Id");
		this.entityAddressByPickupAddressId     = rs.getInt("entity_Address_By_Pickup_Address_Id");
		this.shipmentNbr                  		= rs.getString("shipment_Nbr");
		this.shipperId              			= rs.getInt("shipper_Id");
		this.statusId                  			= rs.getInt("status_Id");
		this.assignedProviderId             	= rs.getInt("assigned_Provider_Id");
		this.requestedEquipmentTypeId           = rs.getInt("requested_Equipment_type_Id");
		this.requestedPickupDt            		= rs.getTimestamp("requested_Pickup_Dt");
		this.requestedDeliveryDt             	= rs.getTimestamp("requested_Delivery_Dt");
		this.unitCount             				= rs.getInt("unit_Count");
		this.unitOfMeasure             			= rs.getString("unit_Of_Measure");
		this.createDt             				= rs.getTimestamp("create_Dt");
		this.createUserId             			= rs.getString("create_User_Id");
		this.updateDt             				= rs.getTimestamp("update_Dt");
		this.updateUserId             			= rs.getString("update_User_Id");
		this.tenderExpirationDt                 = rs.getTimestamp("tender_expiration_dt");
		this.originServiceAreaId                =  rs.getInt("origin_service_area_id");
		this.destinationServiceAreaId           =  rs.getInt("destination_service_area_id");
	}

	public Shipment() {
	}


	public Integer getShipmentId() {
		return this.shipmentId;
	}

	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}

	public int getEntityAddressByDeliveryAddressId() {
		return this.entityAddressByDeliveryAddressId;
	}

	public void setEntityAddressByDeliveryAddressId(int entityAddressByDeliveryAddressId) {
		this.entityAddressByDeliveryAddressId = entityAddressByDeliveryAddressId;
	}

	public int getEntityAddressByPickupAddressId() {
		return this.entityAddressByPickupAddressId;
	}

	public void setEntityAddressByPickupAddressId(int entityAddressByPickupAddressId) {
		this.entityAddressByPickupAddressId = entityAddressByPickupAddressId;
	}

	public String getShipmentNbr() {
		return this.shipmentNbr;
	}

	public void setShipmentNbr(String shipmentNbr) {
		this.shipmentNbr = shipmentNbr;
	}

	public Integer getShipperId() {
		return this.shipperId;
	}

	public void setShipperId(Integer shipperId) {
		this.shipperId = shipperId;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getAssignedProviderId() {
		return this.assignedProviderId;
	}

	public void setAssignedProviderId(int assignedProviderId) {
		this.assignedProviderId = assignedProviderId;
	}

	

	public int getRequestedEquipmentTypeId() {
		return requestedEquipmentTypeId;
	}

	public void setRequestedEquipmentTypeId(int requestedEquipmentTypeId) {
		this.requestedEquipmentTypeId = requestedEquipmentTypeId;
	}

	public void setShipmentId(int shipmentId) {
		this.shipmentId = shipmentId;
	}

	public void setShipperId(int shipperId) {
		this.shipperId = shipperId;
	}

	public Date getRequestedPickupDt() {
		return this.requestedPickupDt;
	}

	public void setRequestedPickupDt(Date requestedPickupDt) {
		this.requestedPickupDt = requestedPickupDt;
	}

	public Date getRequestedDeliveryDt() {
		return this.requestedDeliveryDt;
	}

	public void setRequestedDeliveryDt(Date requestedDeliveryDt) {
		this.requestedDeliveryDt = requestedDeliveryDt;
	}

	public int getUnitCount() {
		return this.unitCount;
	}

	public void setUnitCount(int unitCount) {
		this.unitCount = unitCount;
	}

	public String getUnitOfMeasure() {
		return this.unitOfMeasure;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getUpdateUserId() {
		return this.updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	
	public Timestamp getTenderExpirationDt() {
		return tenderExpirationDt;
	}

	public void setTenderExpirationDt(Timestamp tenderExpirationDt) {
		this.tenderExpirationDt = tenderExpirationDt;
	}

	public Integer getOriginServiceAreaId() {
		return originServiceAreaId;
	}

	public void setOriginServiceAreaId(Integer originServiceAreaId) {
		this.originServiceAreaId = originServiceAreaId;
	}

	public Integer getDestinationServiceAreaId() {
		return destinationServiceAreaId;
	}

	public void setDestinationServiceAreaId(Integer destinationServiceAreaId) {
		this.destinationServiceAreaId = destinationServiceAreaId;
	}



}
