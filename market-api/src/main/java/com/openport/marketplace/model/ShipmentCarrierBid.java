/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Oscar
 *
 */
public class ShipmentCarrierBid {

	private int shipmentCarrierBidId;
	private int carrierId;
	private String quoteStatusCd;
	private String rateTypeCd;
	private String rateTypeDescTxt;
	private Double transitTm;
	private int equipmentTypeId;
	private Double quoteAmt;
	private String quoteCurrencyCd;
	private Date quoteDatestamp;
	private int quoteExpirationValue;
	private String quoteExpirationUom;
	private String commentTxt;
	private Date createDt;
	private String createUserId;
	private Date updateDt;
	private String updateUserId;
	
	
	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public ShipmentCarrierBid(ResultSet rs) throws SQLException {
		this.shipmentCarrierBidId   = rs.getInt("shipment_Carrier_Bid_Id");
		this.carrierId           	= rs.getInt("carrier_Id");
		this.quoteStatusCd          = rs.getString("quote_Status_Cd");
		this.rateTypeCd             = rs.getString("rate_Type_Cd");
		this.rateTypeDescTxt        = rs.getString("rate_Type_Desc_Txt");
		this.transitTm              = rs.getDouble("transit_Tm");
		this.equipmentTypeId        = rs.getInt("equipment_Type_Id");
		this.quoteAmt             	= rs.getDouble("quote_Amt");
		this.quoteCurrencyCd        = rs.getString("quote_Currency_Cd");
		this.quoteDatestamp         = rs.getTimestamp("quote_Datestamp");
		this.quoteExpirationValue   = rs.getInt("quote_Expiration_Value");
		this.quoteExpirationUom     = rs.getString("quote_Expiration_Uom");
		this.commentTxt             = rs.getString("comment_Txt");
		this.createDt             	= rs.getTimestamp("create_Dt");
		this.createUserId           = rs.getString("create_User_Id");
		this.updateDt             	= rs.getTimestamp("update_Dt");
		this.updateUserId           = rs.getString("update_User_Id");
	}


	public ShipmentCarrierBid() {
	}


	public Integer getShipmentCarrierBidId() {
		return this.shipmentCarrierBidId;
	}

	public void setShipmentCarrierBidId(Integer shipmentCarrierBidId) {
		this.shipmentCarrierBidId = shipmentCarrierBidId;
	}

	public int getCarrierId() {
		return this.carrierId;
	}

	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}

	public String getQuoteStatusCd() {
		return this.quoteStatusCd;
	}

	public void setQuoteStatusCd(String quoteStatusCd) {
		this.quoteStatusCd = quoteStatusCd;
	}

	public String getRateTypeCd() {
		return this.rateTypeCd;
	}

	public void setRateTypeCd(String rateTypeCd) {
		this.rateTypeCd = rateTypeCd;
	}

	public String getRateTypeDescTxt() {
		return this.rateTypeDescTxt;
	}

	public void setRateTypeDescTxt(String rateTypeDescTxt) {
		this.rateTypeDescTxt = rateTypeDescTxt;
	}

	public Double getTransitTm() {
		return this.transitTm;
	}

	public void setTransitTm(Double transitTm) {
		this.transitTm = transitTm;
	}

	public Integer getEquipmentTypeId() {
		return this.equipmentTypeId;
	}

	public void setEquipmentTypeId(Integer equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}

	public Double getQuoteAmt() {
		return this.quoteAmt;
	}

	public void setQuoteAmt(Double quoteAmt) {
		this.quoteAmt = quoteAmt;
	}

	public String getQuoteCurrencyCd() {
		return this.quoteCurrencyCd;
	}

	public void setQuoteCurrencyCd(String quoteCurrencyCd) {
		this.quoteCurrencyCd = quoteCurrencyCd;
	}

	public Date getQuoteDatestamp() {
		return this.quoteDatestamp;
	}

	public void setQuoteDatestamp(Date quoteDatestamp) {
		this.quoteDatestamp = quoteDatestamp;
	}

	public int getQuoteExpirationValue() {
		return this.quoteExpirationValue;
	}

	public void setQuoteExpirationValue(int quoteExpirationValue) {
		this.quoteExpirationValue = quoteExpirationValue;
	}

	public String getQuoteExpirationUom() {
		return this.quoteExpirationUom;
	}

	public void setQuoteExpirationUom(String quoteExpirationUom) {
		this.quoteExpirationUom = quoteExpirationUom;
	}

	public String getCommentTxt() {
		return this.commentTxt;
	}

	public void setCommentTxt(String commentTxt) {
		this.commentTxt = commentTxt;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getUpdateUserId() {
		return this.updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

}
