/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Oscar
 *
 */
public class Country {

	private int countryId;
	private String countryName;
	private String country3Cd;
	private String country2Cd;

	/**
	 * 
	 */
	public Country() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public Country(ResultSet rs) throws SQLException {
		this.countryId                = rs.getInt("country_id");
		this.countryName              = rs.getString("country_Name");
		this.country3Cd             = rs.getString("country_3_Cd");
		this.country2Cd                 = rs.getString("country_2_Cd");
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountry3Cd() {
		return country3Cd;
	}


	public void setCountry3Cd(String country3Cd) {
		this.country3Cd = country3Cd;
	}


	public String getCountry2Cd() {
		return country2Cd;
	}


	public void setCountry2Cd(String country2Cd) {
		this.country2Cd = country2Cd;
	}

}
