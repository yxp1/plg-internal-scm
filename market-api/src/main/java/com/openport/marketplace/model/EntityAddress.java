/**
 * 
 */
package com.openport.marketplace.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Oscar
 *
 */
public class EntityAddress {

	private int entityAddressId;
	private String entityName;
	private String floorApartment;
	private String building;
	private String houseNumber;
	private String line1;
	private String streetName;
	private String cityLocality;
	private String province;
	private String country;
	private String postalCode;
	private String districtName;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private String latLongStatusCd;
	private boolean isDc;
	
	
	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public EntityAddress(ResultSet rs) throws SQLException {
		this.entityAddressId   = rs.getInt("entity_Address_Id");
		this.entityName        = rs.getString("entity_Name");
		this.floorApartment    = rs.getString("floor_Apartment");
		this.building          = rs.getString("building");
		this.houseNumber       = rs.getString("house_Number");
		this.line1             = rs.getString("line1");
		this.streetName        = rs.getString("street_Name");
		this.cityLocality      = rs.getString("city_Locality");
		this.province          = rs.getString("province");
		this.country           = rs.getString("country");
		this.postalCode        = rs.getString("postal_Code");
		this.districtName      = rs.getString("district_Name");
		this.latitude          = rs.getBigDecimal("latitude");
		this.longitude         = rs.getBigDecimal("longitude");
		this.latLongStatusCd   = rs.getString("lat_Long_Status_Cd");
		this.isDc              = rs.getBoolean("is_Dc");

	}
	

	public EntityAddress() {
	}

	public EntityAddress(boolean isDc) {
		this.isDc = isDc;
	}


	public Integer getEntityAddressId() {
		return this.entityAddressId;
	}

	public void setEntityAddressId(Integer entityAddressId) {
		this.entityAddressId = entityAddressId;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getFloorApartment() {
		return this.floorApartment;
	}

	public void setFloorApartment(String floorApartment) {
		this.floorApartment = floorApartment;
	}

	public String getBuilding() {
		return this.building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getHouseNumber() {
		return this.houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getLine1() {
		return this.line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getStreetName() {
		return this.streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getCityLocality() {
		return this.cityLocality;
	}

	public void setCityLocality(String cityLocality) {
		this.cityLocality = cityLocality;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getDistrictName() {
		return this.districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getLatLongStatusCd() {
		return this.latLongStatusCd;
	}

	public void setLatLongStatusCd(String latLongStatusCd) {
		this.latLongStatusCd = latLongStatusCd;
	}

	public boolean isIsDc() {
		return this.isDc;
	}

	public void setIsDc(boolean isDc) {
		this.isDc = isDc;
	}


}
