/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * @author Relly
 *
 */
@XmlRootElement
public class CompanyEquipmentList {

	private int 	companyId;
	private String 	companyName;
	private int 	equipmentTypeId;
	private String 	equipmentTypeName;
	private double 	equipmentCount;
	
	
	public CompanyEquipmentList() {
		// TODO Auto-generated constructor stub
	}
	
	public CompanyEquipmentList(ResultSet rs) throws SQLException {
		this.companyId         = rs.getInt("company_id");
		this.companyName       = rs.getString("company_name");
		this.equipmentTypeId   = rs.getInt("equipment_Type_Id");
		this.equipmentTypeName = rs.getString("equipment_Type_Name");
		this.equipmentCount    = rs.getInt("equipment_Count");
	}	
	
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getEquipmentTypeId() {
		return equipmentTypeId;
	}
	public void setEquipmentTypeId(int equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}
	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}
	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}
	public double getEquipmentCount() {
		return equipmentCount;
	}
	public void setEquipmentCount(double equipmentCount) {
		this.equipmentCount = equipmentCount;
	}
	
	
}
