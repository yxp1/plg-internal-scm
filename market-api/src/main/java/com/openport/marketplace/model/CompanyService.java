/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class CompanyService {

	private int companyServiceId;
	private int companyId;
	private int serviceId;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public CompanyService() {
		// TODO Auto-generated constructor stub
	}

	public CompanyService(ResultSet rs) throws SQLException {
		this.companyServiceId 	= rs.getInt("company_service_id");
		this.companyId 		= rs.getInt("company_id");
		this.serviceId 		= rs.getInt("service_id");
		this.createUserId 	= rs.getInt("create_user_id");
		this.createDt 		= rs.getTimestamp("create_dt");
		this.updateUserId 	= rs.getInt("update_user_id");
		this.updateDt 		= rs.getTimestamp("update_dt");
	}	
	
	
	public int getCompanyServiceId() {
		return companyServiceId;
	}

	public void setCompanyServiceId(int companyServiceId) {
		this.companyServiceId = companyServiceId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
