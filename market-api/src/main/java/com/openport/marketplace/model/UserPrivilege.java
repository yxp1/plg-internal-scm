/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class UserPrivilege {

	private int userPrivilegeId;
	private int userId;
	private int privilegeId;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public UserPrivilege() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the userPrivilegeId
	 */
	public int getUserPrivilegeId() {
		return userPrivilegeId;
	}

	/**
	 * @param userPrivilegeId the userPrivilegeId to set
	 */
	public void setUserPrivilegeId(int userPrivilegeId) {
		this.userPrivilegeId = userPrivilegeId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the privilegeId
	 */
	public int getPrivilegeId() {
		return privilegeId;
	}

	/**
	 * @param privilegeId the privilegeId to set
	 */
	public void setPrivilegeId(int privilegeId) {
		this.privilegeId = privilegeId;
	}

	/**
	 * @return the createUserId
	 */
	public int getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the createDt
	 */
	public Timestamp getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt the createDt to set
	 */
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the updateUserId
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the updateDt
	 */
	public Timestamp getUpdateDt() {
		return updateDt;
	}

	/**
	 * @param updateDt the updateDt to set
	 */
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
