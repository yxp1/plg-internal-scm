package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Partner {

	private String partnerCode;
	
	private String parentPartnerCode;
	
	private String partnerName;
	
	private String currencyCode;
	
	private double insuranceAmount;
	
	private double creditRating;
	
	private boolean shipper;
	
	private boolean carrier;
	
	private boolean consignee;
	
	private boolean seller;
	
	private boolean importer;
	
	private boolean creditProvider;
	
	private boolean manufacturer;
	
	private boolean buyer;
	
	private boolean billToParty;
	
	private boolean consolidator;
	
	private boolean decon;
	
	private boolean warehouse;
	
	private boolean freightForwarder;
	
	private boolean customBrokerage;
	
	private String lineAddress1;
	
	private String lineAddress2; 
	
	private String lineAddress3;
	
	private String city;
	
	private String state;
	
	private String postal;
	
	private String country;
	
	private String contactPerson;
	
	private String email;
	
	private String telephone;
	
	private String alternateTelephone;
	
	private String telephoneType;

	private String alternateTelephoneType;
	
	private String mobile;
	
	private String fax;
	
	private String locationCode;
	
	private String locationTypeCode;
	
	private String billToPartyCode;
	
	private String defaultBillToParty;
	
	
	public Partner() {
		
	}

	public Partner(ResultSet rs) throws SQLException {
		this.partnerCode = rs.getString("partner_cd");
		this.parentPartnerCode = rs.getString("parent_partner_cd");
		this.partnerName = rs.getString("partner_name");
		this.currencyCode = rs.getString("currency_cd");
		this.insuranceAmount = rs.getDouble("insurance_amt");
		this.creditRating = rs.getDouble("credit_rating_nbr");
		this.shipper = rs.getBoolean("shipper_flg");
		this.carrier = rs.getBoolean("carrier_flg");
		this.consignee = rs.getBoolean("consignee_flg");
		this.seller = rs.getBoolean("seller_flg");
		this.importer = rs.getBoolean("importer_flg");
		this.creditProvider = rs.getBoolean("credit_provider_flg");
		this.manufacturer = rs.getBoolean("manufacturer_flg");
		this.buyer = rs.getBoolean("buyer_flg");
		this.billToParty = rs.getBoolean("bill_to_party_flg");
		this.consolidator = rs.getBoolean("consolidator_flg");
		this.decon = rs.getBoolean("decon_flg");
		this.warehouse = rs.getBoolean("warehouse_flg");
		this.freightForwarder = rs.getBoolean("freight_forwarder_flg");
		this.customBrokerage = rs.getBoolean("custom_brokerage_flg");
		this.lineAddress1 = rs.getString("line_1_addr");
		this.lineAddress2 = rs.getString("line_2_addr");
		this.lineAddress3 = rs.getString("line_3_addr");
		this.city = rs.getString("city_name");
		this.state = rs.getString("state_cd");
		this.postal = rs.getString("postal_cd");
		this.country = rs.getString("country_cd");
		this.contactPerson = rs.getString("contact_person_name");
		this.email = rs.getString("email_addr");
		this.telephone = rs.getString("telephone_nbr");
		this.alternateTelephone = rs.getString("alt_telephone_nbr");
		this.telephoneType = rs.getString("telephone_nbr_type");
		this.alternateTelephoneType = rs.getString("alt_telephone_nbr_type");
		this.mobile = rs.getString("mobile_nbr");
		this.fax = rs.getString("fax_nbr");
		this.locationCode = rs.getString("location_cd");
		this.locationTypeCode = rs.getString("location_type_cd");
		this.billToPartyCode = rs.getString("bill_to_party_cd");
		this.defaultBillToParty = rs.getString("default_bill_to_party");
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getParentPartnerCode() {
		return parentPartnerCode;
	}

	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public double getInsuranceAmount() {
		return insuranceAmount;
	}

	public void setInsuranceAmount(double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}

	public double getCreditRating() {
		return creditRating;
	}

	public void setCreditRating(double creditRating) {
		this.creditRating = creditRating;
	}

	public boolean isShipper() {
		return shipper;
	}

	public void setShipper(boolean shipper) {
		this.shipper = shipper;
	}

	public boolean isCarrier() {
		return carrier;
	}

	public void setCarrier(boolean carrier) {
		this.carrier = carrier;
	}

	public boolean isConsignee() {
		return consignee;
	}

	public void setConsignee(boolean consignee) {
		this.consignee = consignee;
	}

	public boolean isSeller() {
		return seller;
	}

	public void setSeller(boolean seller) {
		this.seller = seller;
	}

	public boolean isImporter() {
		return importer;
	}

	public void setImporter(boolean importer) {
		this.importer = importer;
	}

	public boolean isCreditProvider() {
		return creditProvider;
	}

	public void setCreditProvider(boolean creditProvider) {
		this.creditProvider = creditProvider;
	}

	public boolean isManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(boolean manufacturer) {
		this.manufacturer = manufacturer;
	}

	public boolean isBuyer() {
		return buyer;
	}

	public void setBuyer(boolean buyer) {
		this.buyer = buyer;
	}

	public boolean isBillToParty() {
		return billToParty;
	}

	public void setBillToParty(boolean billToParty) {
		this.billToParty = billToParty;
	}

	public boolean isConsolidator() {
		return consolidator;
	}

	public void setConsolidator(boolean consolidator) {
		this.consolidator = consolidator;
	}

	public boolean isDecon() {
		return decon;
	}

	public void setDecon(boolean decon) {
		this.decon = decon;
	}

	public boolean isWarehouse() {
		return warehouse;
	}

	public void setWarehouse(boolean warehouse) {
		this.warehouse = warehouse;
	}

	public boolean isFreightForwarder() {
		return freightForwarder;
	}

	public void setFreightForwarder(boolean freightForwarder) {
		this.freightForwarder = freightForwarder;
	}

	public boolean isCustomBrokerage() {
		return customBrokerage;
	}

	public void setCustomBrokerage(boolean customBrokerage) {
		this.customBrokerage = customBrokerage;
	}

	public String getLineAddress1() {
		return lineAddress1;
	}

	public void setLineAddress1(String lineAddress1) {
		this.lineAddress1 = lineAddress1;
	}

	public String getLineAddress2() {
		return lineAddress2;
	}

	public void setLineAddress2(String lineAddress2) {
		this.lineAddress2 = lineAddress2;
	}

	public String getLineAddress3() {
		return lineAddress3;
	}

	public void setLineAddress3(String lineAddress3) {
		this.lineAddress3 = lineAddress3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAlternateTelephone() {
		return alternateTelephone;
	}

	public void setAlternateTelephone(String alternateTelephone) {
		this.alternateTelephone = alternateTelephone;
	}

	public String getTelephoneType() {
		return telephoneType;
	}

	public void setTelephoneType(String telephoneType) {
		this.telephoneType = telephoneType;
	}

	public String getAlternateTelephoneType() {
		return alternateTelephoneType;
	}

	public void setAlternateTelephoneType(String alternateTelephoneType) {
		this.alternateTelephoneType = alternateTelephoneType;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationTypeCode() {
		return locationTypeCode;
	}

	public void setLocationTypeCode(String locationTypeCode) {
		this.locationTypeCode = locationTypeCode;
	}

	public String getBillToPartyCode() {
		return billToPartyCode;
	}

	public void setBillToPartyCode(String billToPartyCode) {
		this.billToPartyCode = billToPartyCode;
	}

	public String getDefaultBillToParty() {
		return defaultBillToParty;
	}

	public void setDefaultBillToParty(String defaultBillToParty) {
		this.defaultBillToParty = defaultBillToParty;
	}

	@Override
	public String toString() {
		return "Partner [partnerCode=" + partnerCode + ", parentPartnerCode=" + parentPartnerCode + ", partnerName="
				+ partnerName + ", currencyCode=" + currencyCode + ", insuranceAmount=" + insuranceAmount
				+ ", creditRating=" + creditRating + ", shipper=" + shipper + ", carrier=" + carrier + ", consignee="
				+ consignee + ", seller=" + seller + ", importer=" + importer + ", creditProvider=" + creditProvider
				+ ", manufacturer=" + manufacturer + ", buyer=" + buyer + ", billToParty=" + billToParty
				+ ", consolidator=" + consolidator + ", decon=" + decon + ", warehouse=" + warehouse
				+ ", freightForwarder=" + freightForwarder + ", customBrokerage=" + customBrokerage + ", lineAddress1="
				+ lineAddress1 + ", lineAddress2=" + lineAddress2 + ", lineAddress3=" + lineAddress3 + ", city=" + city
				+ ", state=" + state + ", postal=" + postal + ", country=" + country + ", contactPerson="
				+ contactPerson + ", email=" + email + ", telephone=" + telephone + ", alternateTelephone="
				+ alternateTelephone + ", telephoneType=" + telephoneType + ", alternateTelephoneType="
				+ alternateTelephoneType + ", mobile=" + mobile + ", fax=" + fax + ", locationCode=" + locationCode
				+ ", locationTypeCode=" + locationTypeCode + ", billToPartyCode=" + billToPartyCode
				+ ", defaultBillToParty=" + defaultBillToParty + "]";
	}
	
}
