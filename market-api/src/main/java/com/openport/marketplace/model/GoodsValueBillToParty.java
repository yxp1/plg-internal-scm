package com.openport.marketplace.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class GoodsValueBillToParty {
	
	@Column(name = "PARTNER_CD")
	private String partnerCode;
	
	@Column(name = "parent_partner_cd")
	private String parentPartnerCode;
	
	@Column(name = "partner_name")
	private String partnerName;
	
	@Column(name = "currency_cd")
	private String currencyCode;
	
	@Column(name = "name_code")
	private String nameAndCode;
	
	public GoodsValueBillToParty() {
		
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getParentPartnerCode() {
		return parentPartnerCode;
	}

	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getNameAndCode() {
		return nameAndCode;
	}

	public void setNameAndCode(String nameAndCode) {
		this.nameAndCode = nameAndCode;
	}

	@Override
	public String toString() {
		return "GoodsValueBillToParty [partnerCode=" + partnerCode + ", parentPartnerCode=" + parentPartnerCode
				+ ", partnerName=" + partnerName + ", currencyCode=" + currencyCode + ", nameAndCode=" + nameAndCode
				+ "]";
	}

}
