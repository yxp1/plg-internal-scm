package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;



public class FileDocumentType implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6065928973881804349L;
	private Integer fileDocumentTypeId;
	private Integer companyFileId;
	private Integer documentTypeId;
	
	
	public FileDocumentType(ResultSet rs) throws SQLException {
		this.fileDocumentTypeId                 = rs.getInt("file_document_type_id");
		this.companyFileId                 = rs.getInt("company_file_id");
		this.documentTypeId                 = rs.getInt("document_type_id");
		
		//this.fileDocumentTypeId                 = rs.getInt("file_document_type_id");
					
	}


	public Integer getFileDocumentTypeId() {
		return fileDocumentTypeId;
	}


	public void setFileDocumentTypeId(Integer fileDocumentTypeId) {
		this.fileDocumentTypeId = fileDocumentTypeId;
	}


	public Integer getCompanyFileId() {
		return companyFileId;
	}


	public void setCompanyFileId(Integer companyFileId) {
		this.companyFileId = companyFileId;
	}


	public Integer getDocumentTypeId() {
		return documentTypeId;
	}


	public void setDocumentTypeId(Integer documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public FileDocumentType() {
		
	}
	public FileDocumentType(Integer fileDocumentTypeId, Integer companyFileId, Integer documentTypeId) {
		super();
		this.fileDocumentTypeId = fileDocumentTypeId;
		this.companyFileId = companyFileId;
		this.documentTypeId = documentTypeId;
	}


	

	

}
