package com.openport.marketplace.model;


// created by jero.dungog@openport.com
public class LogisticsTab {

    private String assigned_carrier;
    private String category;
    private String consignee_order_nbr;
    private String dest_address;
    private String dest_city;
    private String dest_district;
    private String dest_state_province;
    private String destination;
    private String driver_phone;
    private String equipment_type;
    private String order_type;
    private String origin;
    private String pickup_dt;
    private String PRO_BILL_NBR;
    private String order_nbr;
    private String rated_cost;
    private String recommended_carrier;
    private String delivery_nbr;
    private String requested_delivery_dt;
    private String sales_channel;
    private String status;
    private int total_qty;
    private String transportation;
    private String uom;
    private int volume;
    private int weight;
    private String Order_upload_Dt;
    private String PARENT_PARTNER_CD;
    private String PARTNER_CD;
    private String CUSTOMER_CD;
    private String SHIPPER_CD;
    private String legNumber;
    private String userTimezone;
    private String userId;
    private String rowNumber;
    private int groupNumber;
    private String productCode;
    private String parent;
    private String child;
    private String vpo;
    private String comment;
    
    
    public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAssigned_carrier() {
        return assigned_carrier;
    }
    public void setAssigned_carrier(String assigned_carrier) {
        this.assigned_carrier = assigned_carrier;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getConsignee_order_nbr() {
        return consignee_order_nbr;
    }
    public void setConsignee_order_nbr(String consignee_order_nbr) {
        this.consignee_order_nbr = consignee_order_nbr;
    }
    public String getDest_address() {
        return dest_address;
    }
    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }
    public String getDest_city() {
        return dest_city;
    }
    public void setDest_city(String dest_city) {
        this.dest_city = dest_city;
    }
    public String getDest_district() {
        return dest_district;
    }
    public void setDest_district(String dest_district) {
        this.dest_district = dest_district;
    }
    public String getDest_state_province() {
        return dest_state_province;
    }
    public void setDest_state_province(String dest_state_province) {
        this.dest_state_province = dest_state_province;
    }
    public String getDestination() {
        return destination;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getDriver_phone() {
        return driver_phone;
    }
    public void setDriver_phone(String driver_phone) {
        this.driver_phone = driver_phone;
    }
    public String getEquipment_type() {
        return equipment_type;
    }
    public void setEquipment_type(String equipment_type) {
        this.equipment_type = equipment_type;
    }
    public String getOrder_type() {
        return order_type;
    }
    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }
    public String getOrigin() {
        return origin;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public String getPickup_dt() {
        return pickup_dt;
    }
    public void setPickup_dt(String pickup_dt) {
        this.pickup_dt = pickup_dt;
    }
    public String getPRO_BILL_NBR() {
        return PRO_BILL_NBR;
    }
    public void setPRO_BILL_NBR(String pRO_BILL_NBR) {
        PRO_BILL_NBR = pRO_BILL_NBR;
    }
    public String getOrder_nbr() {
        return order_nbr;
    }
    public void setOrder_nbr(String order_nbr) {
        this.order_nbr = order_nbr;
    }
    public String getRated_cost() {
        return rated_cost;
    }
    public void setRated_cost(String rated_cost) {
        this.rated_cost = rated_cost;
    }
    public String getRecommended_carrier() {
        return recommended_carrier;
    }
    public void setRecommended_carrier(String recommended_carrier) {
        this.recommended_carrier = recommended_carrier;
    }
    public String getDelivery_nbr() {
        return delivery_nbr;
    }
    public void setDelivery_nbr(String delivery_nbr) {
        this.delivery_nbr = delivery_nbr;
    }
    public String getRequested_delivery_dt() {
        return requested_delivery_dt;
    }
    public void setRequested_delivery_dt(String requested_delivery_dt) {
        this.requested_delivery_dt = requested_delivery_dt;
    }
    public String getSales_channel() {
        return sales_channel;
    }
    public void setSales_channel(String sales_channel) {
        this.sales_channel = sales_channel;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public int getTotal_qty() {
        return total_qty;
    }
    public void setTotal_qty(int total_qty) {
        this.total_qty = total_qty;
    }
    public String getTransportation() {
        return transportation;
    }
    public void setTransportation(String transportation) {
        this.transportation = transportation;
    }
    public String getUom() {
        return uom;
    }
    public void setUom(String uom) {
        this.uom = uom;
    }
    public int getVolume() {
        return volume;
    }
    public void setVolume(int volume) {
        this.volume = volume;
    }
    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }
    public String getOrder_upload_Dt() {
        return Order_upload_Dt;
    }
    public void setOrder_upload_Dt(String order_upload_Dt) {
        Order_upload_Dt = order_upload_Dt;
    }
    public String getPARENT_PARTNER_CD() {
        return PARENT_PARTNER_CD;
    }
    public void setPARENT_PARTNER_CD(String pARENT_PARTNER_CD) {
        PARENT_PARTNER_CD = pARENT_PARTNER_CD;
    }
    public String getPARTNER_CD() {
        return PARTNER_CD;
    }
    public void setPARTNER_CD(String pARTNER_CD) {
        PARTNER_CD = pARTNER_CD;
    }
    public String getCUSTOMER_CD() {
        return CUSTOMER_CD;
    }
    public void setCUSTOMER_CD(String cUSTOMER_CD) {
        CUSTOMER_CD = cUSTOMER_CD;
    }
    public String getSHIPPER_CD() {
        return SHIPPER_CD;
    }
    public void setSHIPPER_CD(String sHIPPER_CD) {
        SHIPPER_CD = sHIPPER_CD;
    }
	public String getLegNumber() {
		return legNumber;
	}
	public void setLegNumber(String legNumber) {
		this.legNumber = legNumber;
	}
	public String getUserTimezone() {
		return userTimezone;
	}
	public void setUserTimezone(String userTimezone) {
		this.userTimezone = userTimezone;
	}
	public String getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(String rowNumber) {
		this.rowNumber = rowNumber;
	}
	public int getGroupNumber() {
		return groupNumber;
	}
	public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getChild() {
		return child;
	}
	public void setChild(String child) {
		this.child = child;
	}
	public String getVpo() {
		return vpo;
	}
	public void setVpo(String vpo) {
		this.vpo = vpo;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
    
}
