/**
 * 
 */
package com.openport.marketplace.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Nargel
 *
 */
@XmlRootElement
@Entity	
public class Accessorial implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2624871855763743843L;

	@Column(name="shipment_id")
	private Integer shipmentId;
	
	@Column(name="customer_cd")
	private String customerCd;
	
	@Column(name="parent_partner_cd")
	private String parentPartnerCd;
	
	@Column(name="accessorial_cd")
	private String accessorialCd;
	
	@Column(name="accessorial_type")
	private String accessorialType;
	
	@Column(name="description")
	private String description;

	@Column(name="accessorial_amt")
	private Double accessorialAmt;

	
	@Column(name="accessorial_data_Source")
	private String accessorialDataSource;
	//denormalized
	private Boolean status;
	
	
	
	public String getAccessorialDataSource() {
		return accessorialDataSource;
	}

	public void setAccessorialDataSource(String accessorialDataSource) {
		this.accessorialDataSource = accessorialDataSource;
	}

	public Double getAccessorialAmt() {
		return accessorialAmt;
	}

	public void setAccessorialAmt(Double accessorialAmt) {
		this.accessorialAmt = accessorialAmt;
	}

	public Integer getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Integer shipmentId) {
		this.shipmentId = shipmentId;
	}



	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getCustomerCd() {
		return customerCd;
	}

	public void setCustomerCd(String customerCd) {
		this.customerCd = customerCd;
	}

	public String getParentPartnerCd() {
		return parentPartnerCd;
	}

	public void setParentPartnerCd(String parentPartnerCd) {
		this.parentPartnerCd = parentPartnerCd;
	}

	public String getAccessorialCd() {
		return accessorialCd;
	}

	public void setAccessorialCd(String accessorialCd) {
		this.accessorialCd = accessorialCd;
	}

	public String getAccessorialType() {
		return accessorialType;
	}

	public void setAccessorialType(String accessorialType) {
		this.accessorialType = accessorialType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
