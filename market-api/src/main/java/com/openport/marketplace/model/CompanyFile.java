/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Oscar
 *
 */
@XmlRootElement
public class CompanyFile {

	private int companyFileId;
	private int companyId;
	private Boolean isLegalDocument;
	private String documentName;
	private String relativePath;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public CompanyFile() {
		// TODO Auto-generated constructor stub
	}

	public CompanyFile(ResultSet rs) throws SQLException {
		this.companyFileId   = rs.getInt("company_File_Id");
		this.companyId       = rs.getInt("company_Id");
		this.isLegalDocument = rs.getBoolean("is_Legal_Document");
		this.documentName    = rs.getString("document_Name");
		this.relativePath    = rs.getString("relative_Path");
		this.createUserId    = rs.getInt("create_User_Id");
		this.createDt        = rs.getTimestamp("create_Dt");
		this.updateUserId    = rs.getInt("update_User_Id");
		this.updateDt        = rs.getTimestamp("update_Dt");
	}	
	
	public int getCompanyFileId() {
		return companyFileId;
	}

	public void setCompanyFileId(int companyFileId) {
		this.companyFileId = companyFileId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public Boolean isLegalDocument() {
		return isLegalDocument;
	}

	public void setIsLegalDocument(Boolean isLegalDocument) {
		this.isLegalDocument = isLegalDocument;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	public Boolean getIsLegalDocument() {
		return isLegalDocument;
	}

}
