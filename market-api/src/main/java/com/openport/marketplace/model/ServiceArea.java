/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class ServiceArea {

	private int serviceAreaId;
	private String serviceAreaName;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	
	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public ServiceArea(ResultSet rs) throws SQLException {
		this.serviceAreaId             = rs.getInt("service_area_id");
		this.serviceAreaName           = rs.getString("service_area_name");
		this.createUserId              = rs.getInt("create_user_id");
		this.createDt                  = rs.getTimestamp("create_dt");
		this.updateUserId              = rs.getInt("update_user_id");
		this.updateDt                  = rs.getTimestamp("update_dt");
	}		
	
	/**
	 * 
	 */
	public ServiceArea() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the serviceAreaId
	 */
	public int getServiceAreaId() {
		return serviceAreaId;
	}

	/**
	 * @param serviceAreaId the serviceAreaId to set
	 */
	public void setServiceAreaId(int serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	/**
	 * @return the serviceAreaName
	 */
	public String getServiceAreaName() {
		return serviceAreaName;
	}

	/**
	 * @param serviceAreaName the serviceAreaName to set
	 */
	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}

	/**
	 * @return the createUserId
	 */
	public int getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the createDt
	 */
	public Timestamp getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt the createDt to set
	 */
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the updateUserId
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the updateDt
	 */
	public Timestamp getUpdateDt() {
		return updateDt;
	}

	/**
	 * @param updateDt the updateDt to set
	 */
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
