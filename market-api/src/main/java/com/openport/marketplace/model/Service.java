/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class Service {

	private int serviceId;
	private String serviceName;
	private boolean isTransportation;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	
	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public Service(ResultSet rs) throws SQLException {
		this.serviceId          = rs.getInt("service_id");
		this.serviceName        = rs.getString("service_name");
		this.isTransportation   = rs.getBoolean("is_transportation"); 
		this.createUserId       = rs.getInt("create_user_id");
		this.createDt           = rs.getTimestamp("Create_Dt");
		this.updateUserId       = rs.getInt("update_user_id");
		this.updateDt           = rs.getTimestamp("Update_dt");
	}	
	
	/**
	 * 
	 */
	public Service() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the serviceId
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the isTransportation
	 */
	public boolean isTransportation() {
		return isTransportation;
	}

	/**
	 * @param isTransportation the isTransportation to set
	 */
	public void setTransportation(boolean isTransportation) {
		this.isTransportation = isTransportation;
	}

	/**
	 * @return the createUserId
	 */
	public int getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the createDt
	 */
	public Timestamp getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt the createDt to set
	 */
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the updateUserId
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the updateDt
	 */
	public Timestamp getUpdateDt() {
		return updateDt;
	}

	/**
	 * @param updateDt the updateDt to set
	 */
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
