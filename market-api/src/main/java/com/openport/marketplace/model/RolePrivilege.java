/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class RolePrivilege {

	private int rolePrivilegeId;
	private int roleId;
	private int privilegeId;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public RolePrivilege() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the rolePrivilegeId
	 */
	public int getRolePrivilegeId() {
		return rolePrivilegeId;
	}

	/**
	 * @param rolePrivilegeId the rolePrivilegeId to set
	 */
	public void setRolePrivilegeId(int rolePrivilegeId) {
		this.rolePrivilegeId = rolePrivilegeId;
	}

	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the privilegeId
	 */
	public int getPrivilegeId() {
		return privilegeId;
	}

	/**
	 * @param privilegeId the privilegeId to set
	 */
	public void setPrivilegeId(int privilegeId) {
		this.privilegeId = privilegeId;
	}

	/**
	 * @return the createUserId
	 */
	public int getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the createDt
	 */
	public Timestamp getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt the createDt to set
	 */
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the updateUserId
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the updateDt
	 */
	public Timestamp getUpdateDt() {
		return updateDt;
	}

	/**
	 * @param updateDt the updateDt to set
	 */
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
