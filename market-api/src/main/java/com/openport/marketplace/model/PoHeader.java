package com.openport.marketplace.model;

import java.util.Date;

public class PoHeader {
    
    // This pojo is not complete. It is only based for edit order screen. Please add
    // other columns if necessary and update the toString() method
    
    private String releaseNumber;
    
    private String poNumber;
    
    private String actualPoReleaseNumber;
    
    private String actualPoNumber;

    private double totalQuantity;
    
    private double totalVolume;
    
    private double totalWeight;
    
    private String quantityUom;
    
    private String volumeUom;
    
    private String weightUom;
    
    private String equipmentTypeCode; 
    
    private Date availableDate;
    
    private Date requestedDeliveryDate;
    
    private Date updateTimestamp;
    
    private String updateUserId;
    

    public String getReleaseNumber() {
        return releaseNumber;
    }

    public void setReleaseNumber(String releaseNumber) {
        this.releaseNumber = releaseNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getActualPoReleaseNumber() {
        return actualPoReleaseNumber;
    }

    public void setActualPoReleaseNumber(String actualPoReleaseNumber) {
        this.actualPoReleaseNumber = actualPoReleaseNumber;
    }

    public String getActualPoNumber() {
        return actualPoNumber;
    }

    public void setActualPoNumber(String actualPoNumber) {
        this.actualPoNumber = actualPoNumber;
    }

    public double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getQuantityUom() {
        return quantityUom;
    }

    public void setQuantityUom(String quantityUom) {
        this.quantityUom = quantityUom;
    }

    public String getVolumeUom() {
        return volumeUom;
    }

    public void setVolumeUom(String volumeUom) {
        this.volumeUom = volumeUom;
    }

    public String getWeightUom() {
        return weightUom;
    }

    public void setWeightUom(String weightUom) {
        this.weightUom = weightUom;
    }

    public String getEquipmentTypeCode() {
        return equipmentTypeCode;
    }

    public void setEquipmentTypeCode(String equipmentTypeCode) {
        this.equipmentTypeCode = equipmentTypeCode;
    }

    public Date getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(Date availableDate) {
        this.availableDate = availableDate;
    }

    public Date getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(Date requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    @Override
    public String toString() {
        return "PoHeader [releaseNumber=" + releaseNumber + ", poNumber=" + poNumber + ", actualPoReleaseNumber="
                + actualPoReleaseNumber + ", actualPoNumber=" + actualPoNumber + ", totalQuantity=" + totalQuantity
                + ", totalVolume=" + totalVolume + ", totalWeight=" + totalWeight + ", quantityUom=" + quantityUom
                + ", volumeUom=" + volumeUom + ", weightUom=" + weightUom + ", equipmentTypeCode=" + equipmentTypeCode
                + ", availableDate=" + availableDate + ", requestedDeliveryDate=" + requestedDeliveryDate
                + ", updateTimestamp=" + updateTimestamp + ", updateUserId=" + updateUserId + "]";
    }
    
}
