/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class EquipmentType {

	private int equipmentTypeId;
	private String equipmentTypeName;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	
	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public EquipmentType(ResultSet rs) throws SQLException {
		this.equipmentTypeId    = rs.getInt("equipment_type_id");
		this.equipmentTypeName  = rs.getString("equipment_type_name");
		this.createUserId       = rs.getInt("create_user_id");
		this.createDt           = rs.getTimestamp("Create_Dt");
		this.updateUserId       = rs.getInt("update_user_id");
		this.updateDt           = rs.getTimestamp("Update_dt");
	}	
	
	/**
	 * 
	 */
	public EquipmentType() {
		// TODO Auto-generated constructor stub
	}

	public int getEquipmentTypeId() {
		return equipmentTypeId;
	}

	public void setEquipmentTypeId(int equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}

	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}

	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
