/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class Company {

	private int companyId;
	private String companyName;
	private boolean isCarrier;
	private boolean isShipper;
	private String companyCode;
	private int corporateAddressId;
	private int truckPoolAddressId;
	private int branchAddressId;
	private int yearsOfOperation;
	private String businessHeadline;
	private String businessDescription;
	private int wizardStepNbr;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public Company() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public Company(ResultSet rs) throws SQLException {
		this.companyId                 = rs.getInt("company_id");
		this.companyName               = rs.getString("company_Name");
		this.companyCode               = rs.getString("company_Code");
		this.isCarrier                 = rs.getBoolean("is_carrier");
		this.isShipper                 = rs.getBoolean("is_shipper");
		this.createUserId              = rs.getInt("create_user_id");
		this.createDt                  = rs.getTimestamp("Create_Dt");
		this.updateUserId              = rs.getInt("update_user_id");
		this.updateDt                  = rs.getTimestamp("Update_dt");
		this.yearsOfOperation          = rs.getInt("years_of_operation");
		this.corporateAddressId        = rs.getInt("corporate_Address_Id");
		this.truckPoolAddressId        = rs.getInt("truck_Pool_Address_Id");
		this.branchAddressId           = rs.getInt("branch_Address_Id");
		this.wizardStepNbr 			= rs.getShort("Wizard_Step_Nbr");

		
	}


	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public boolean isCarrier() {
		return isCarrier;
	}

	public void setCarrier(boolean isCarrier) {
		this.isCarrier = isCarrier;
	}

	public boolean isShipper() {
		return isShipper;
	}

	public void setShipper(boolean isShipper) {
		this.isShipper = isShipper;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public int getCorporateAddressId() {
		return corporateAddressId;
	}

	public void setCorporateAddressId(int corporateAddressId) {
		this.corporateAddressId = corporateAddressId;
	}

	public int getTruckPoolAddressId() {
		return truckPoolAddressId;
	}

	public void setTruckPoolAddressId(int truckPoolAddressId) {
		this.truckPoolAddressId = truckPoolAddressId;
	}

	public int getBranchAddressId() {
		return branchAddressId;
	}

	public void setBranchAddressId(int branchAddressId) {
		this.branchAddressId = branchAddressId;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public int getYearsOfOperation() {
		return yearsOfOperation;
	}

	public void setYearsOfOperation(int yearsOfOperation) {
		this.yearsOfOperation = yearsOfOperation;
	}

	public String getBusinessHeadline() {
		return businessHeadline;
	}

	public void setBusinessHeadline(String businessHeadline) {
		this.businessHeadline = businessHeadline;
	}

	public String getBusinessDescription() {
		return businessDescription;
	}

	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}

	public int getWizardStepNbr() {
		return wizardStepNbr;
	}

	public void setWizardStepNbr(int wizardStepNbr) {
		this.wizardStepNbr = wizardStepNbr;
	}
	
}
