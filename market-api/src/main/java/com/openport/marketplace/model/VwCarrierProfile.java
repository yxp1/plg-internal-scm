/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import com.openport.marketplace.json.DropDownList;

/**
 * @author Relly
 *
 */
@XmlRootElement
public class VwCarrierProfile {

	private int 	companyId;
	private String 	companyName;
	private String 	companyCode;
	private int 	yearsOfOperation;
	private String 	companyAddress;
	private String 	hqProvince;
	private String 	hqCountry;
	private String 	truckPoolAddress;
	private String 	branchAddress;
	private String 	contactName;
	private String 	emailAddress;
	
	private String 	companyLogoPath;
	private String  phoneNbr;
	private String  businessHeadline;
	private String  businessDescription;
	private String hqCity;
	private int    rating;
	
	private ArrayList<EquipmentType> equipmentList;
	private ArrayList<FileDocumentType> carrierPicturesList;
	private ArrayList<ServiceArea> carrierRegionsList;
	private ArrayList<Service> carrierServicesList;
	
	/**
	 * 
	 */
	public VwCarrierProfile() {
		// TODO Auto-generated constructor stub
	}
	
	public VwCarrierProfile(ResultSet rs) throws SQLException {

		this.companyId 		= rs.getInt("company_id");
		this.companyName 	= rs.getString("company_name");
		this.companyCode 	= rs.getString("company_code");
		this.yearsOfOperation = rs.getInt("years_of_operation");
		this.rating = rs.getInt("rating");
		this.companyAddress = rs.getString("company_address");
		this.hqCity = 		 rs.getString("hq_city");
		this.hqProvince 	= rs.getString("hq_province");
		this.hqCountry      = rs.getString("hq_country");
		this.truckPoolAddress = rs.getString("truck_pool_address");
		this.branchAddress  = rs.getString("branch_address");
		this.contactName    = rs.getString("contact_name");
		this.emailAddress   = rs.getString("email_address");
		this.companyLogoPath = rs.getString("company_logo_path");
		this.phoneNbr        = rs.getString("phone_nbr");
		this.businessHeadline    = rs.getString("Business_Headline");
		this.businessDescription = rs.getString("Business_Description");
		
		
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public int getYearsOfOperation() {
		return yearsOfOperation;
	}

	public void setYearsOfOperation(int yearsOfOperation) {
		this.yearsOfOperation = yearsOfOperation;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getHqProvince() {
		return hqProvince;
	}

	public void setHqProvince(String hqProvince) {
		this.hqProvince = hqProvince;
	}

	public String getHqCountry() {
		return hqCountry;
	}

	public void setHqCountry(String hqCountry) {
		this.hqCountry = hqCountry;
	}

	public String getTruckPoolAddress() {
		return truckPoolAddress;
	}

	public void setTruckPoolAddress(String truckPoolAddress) {
		this.truckPoolAddress = truckPoolAddress;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public String getPhoneNbr() {
		return phoneNbr;
	}

	public void setPhoneNbr(String phoneNbr) {
		this.phoneNbr = phoneNbr;
	}

	public String getBusinessHeadline() {
		return businessHeadline;
	}

	public void setBusinessHeadline(String businessHeadline) {
		this.businessHeadline = businessHeadline;
	}

	public String getBusinessDescription() {
		return businessDescription;
	}

	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}

	public ArrayList<EquipmentType> getEquipmentList() {
		return equipmentList;
	}

	public void setEquipmentList(ArrayList<EquipmentType> equipmentList) {
		this.equipmentList = equipmentList;
	}

	public ArrayList<FileDocumentType> getCarrierPicturesList() {
		return carrierPicturesList;
	}

	public void setCarrierPicturesList(ArrayList<FileDocumentType> carrierPicturesList) {
		this.carrierPicturesList = carrierPicturesList;
	}

	public ArrayList<ServiceArea> getCarrierRegionsList() {
		return carrierRegionsList;
	}

	public void setCarrierRegionsList(ArrayList<ServiceArea> carrierRegionsList) {
		this.carrierRegionsList = carrierRegionsList;
	}

	public ArrayList<Service> getCarrierServicesList() {
		return carrierServicesList;
	}

	public void setCarrierServicesList(ArrayList<Service> carrierServicesList) {
		this.carrierServicesList = carrierServicesList;
	}

	public String getHqCity() {
		return hqCity;
	}

	public void setHqCity(String hqCity) {
		this.hqCity = hqCity;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

}
