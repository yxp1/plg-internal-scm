/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class User {

	private int userId;
	private String emailAddress;
	private String firstname;
	private String lastname;
	private String passwordHash;
	private String phoneNbr;
	private int primaryUserRoleId;
	private boolean isTest;
	private boolean isActive;
	private boolean isInternal;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * @param userId
	 * @param emailAddress
	 * @param firstname
	 * @param lastname
	 * @param passwordHash
	 * @param phoneNbr
	 * @param primaryUserRoleId
	 * @param isTest
	 * @param isActive
	 * @param isInternal
	 * @param createUserId
	 * @param createDt
	 * @param updateUserId
	 * @param updateDt
	 */
	public User(int userId,
			String emailAddress,
			String firstname,
			String lastname,
			String passwordHash,
			String phoneNbr,
			int primaryUserRoleId,
			boolean isTest,
			boolean isActive,
			boolean isInternal,
			int createUserId,
			Timestamp createDt,
			int updateUserId,
			Timestamp updateDt) {
		this.userId            = userId;
		this.emailAddress      = emailAddress;
		this.firstname         = firstname;
		this.lastname          = lastname;
		this.passwordHash      = passwordHash;
		this.phoneNbr          = phoneNbr;
		this.primaryUserRoleId = primaryUserRoleId;
		this.isTest            = isTest;
		this.isActive          = isActive;
		this.isInternal        = isInternal;
		this.createUserId      = createUserId;
		this.createDt          = createDt;
		this.updateUserId      = updateUserId;
		this.updateDt          = updateDt;
	}

	public User() {
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getPhoneNbr() {
		return phoneNbr;
	}

	public void setPhoneNbr(String phoneNbr) {
		this.phoneNbr = phoneNbr;
	}

	public int getPrimaryUserRoleId() {
		return primaryUserRoleId;
	}

	public void setPrimaryUserRoleId(int primaryUserRoleId) {
		this.primaryUserRoleId = primaryUserRoleId;
	}

	public boolean isTest() {
		return isTest;
	}

	public void setTest(boolean isTest) {
		this.isTest = isTest;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isInternal() {
		return isInternal;
	}

	public void setInternal(boolean isInternal) {
		this.isInternal = isInternal;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	
}
