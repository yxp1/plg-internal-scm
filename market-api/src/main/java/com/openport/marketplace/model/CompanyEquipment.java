/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class CompanyEquipment {

	private int companyEquipmentId;
	private int companyId;
	private int equipmentId;
	private double equipmentCount;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public CompanyEquipment() {
		// TODO Auto-generated constructor stub
	}

	public int getCompanyEquipmentId() {
		return companyEquipmentId;
	}

	public void setCompanyEquipmentId(int companyEquipmentId) {
		this.companyEquipmentId = companyEquipmentId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(int equipmentId) {
		this.equipmentId = equipmentId;
	}

	public double getEquipmentCount() {
		return equipmentCount;
	}

	public void setEquipmentCount(double equipmentCount) {
		this.equipmentCount = equipmentCount;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
