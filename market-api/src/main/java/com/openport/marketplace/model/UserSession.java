/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class UserSession {

	private int userSessionId;
	private String token;
	private Timestamp tokenExpirationDt;
	private String deviceName;
	private String deviceType;
	private String identifyKey;
	private String deviceVersion;
	private String appFrom;
	private String appId;
	private String appVersion;
	private int userId;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public UserSession() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the userSessionId
	 */
	public int getUserSessionId() {
		return userSessionId;
	}

	/**
	 * @param userSessionId the userSessionId to set
	 */
	public void setUserSessionId(int userSessionId) {
		this.userSessionId = userSessionId;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the tokenExpirationDt
	 */
	public Timestamp getTokenExpirationDt() {
		return tokenExpirationDt;
	}

	/**
	 * @param tokenExpirationDt the tokenExpirationDt to set
	 */
	public void setTokenExpirationDt(Timestamp tokenExpirationDt) {
		this.tokenExpirationDt = tokenExpirationDt;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @return the identifyKey
	 */
	public String getIdentifyKey() {
		return identifyKey;
	}

	/**
	 * @param identifyKey the identifyKey to set
	 */
	public void setIdentifyKey(String identifyKey) {
		this.identifyKey = identifyKey;
	}

	/**
	 * @return the deviceVersion
	 */
	public String getDeviceVersion() {
		return deviceVersion;
	}

	/**
	 * @param deviceVersion the deviceVersion to set
	 */
	public void setDeviceVersion(String deviceVersion) {
		this.deviceVersion = deviceVersion;
	}

	/**
	 * @return the appFrom
	 */
	public String getAppFrom() {
		return appFrom;
	}

	/**
	 * @param appFrom the appFrom to set
	 */
	public void setAppFrom(String appFrom) {
		this.appFrom = appFrom;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the appVersion
	 */
	public String getAppVersion() {
		return appVersion;
	}

	/**
	 * @param appVersion the appVersion to set
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the createUserId
	 */
	public int getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the createDt
	 */
	public Timestamp getCreateDt() {
		return createDt;
	}

	/**
	 * @param createDt the createDt to set
	 */
	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	/**
	 * @return the updateUserId
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the updateDt
	 */
	public Timestamp getUpdateDt() {
		return updateDt;
	}

	/**
	 * @param updateDt the updateDt to set
	 */
	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

}
