/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Relly
 *
 */
@XmlRootElement
public class CompanyServicesByRegion {

	private int 	companyId;
	private String 	companyName;
	private String 	companyCode;
	private String 	serviceAreaName;
	private String 	serviceName;
	
	private String 	equipmentTypeName;
	
	private int 	ratingValue;
	/**
	 * 
	 */
	public CompanyServicesByRegion() {
		// TODO Auto-generated constructor stub
	}
	
	public CompanyServicesByRegion(ResultSet rs) throws SQLException {
		this.companyId 			= rs.getInt("company_id");
		this.companyName 		= rs.getString("company_name");
		this.companyCode 		= rs.getString("company_code");
		this.serviceAreaName 	= rs.getString("service_area_name");
		this.serviceName 		= rs.getString("service_name");
		this.equipmentTypeName  = rs.getString("equipment_type_name");
		this.ratingValue        = rs.getInt("rating_value");
	}
	
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getServiceAreaName() {
		return serviceAreaName;
	}

	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getEquipmentTypeName() {
		return equipmentTypeName;
	}

	public void setEquipmentTypeName(String equipmentTypeName) {
		this.equipmentTypeName = equipmentTypeName;
	}

	public int getRatingValue() {
		return ratingValue;
	}

	public void setRatingValue(int ratingValue) {
		this.ratingValue = ratingValue;
	}

	
}
