/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class CompanyClient {

	private int companyClientId;
	private int companyId;
	private String clientName;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public CompanyClient() {
		// TODO Auto-generated constructor stub
	}

	public int getCompanyClientId() {
		return companyClientId;
	}

	public void setCompanyClientId(int companyClientId) {
		this.companyClientId = companyClientId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	
	public CompanyClient(ResultSet rs) throws SQLException {
		this.companyClientId                 = rs.getInt("company_client_id");
		this.clientName               = rs.getString("client_name");
	}
	
}
