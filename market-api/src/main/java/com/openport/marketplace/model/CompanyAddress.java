/**
 * 
 */
package com.openport.marketplace.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Oscar
 *
 */
public class CompanyAddress {

	private int companyAddressId;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String cityName;
	private int provinceId;
	private int countryId;
	private String postalCd;
	private int createUserId;
	private Timestamp createDt;
	private int updateUserId;
	private Timestamp updateDt;

	/**
	 * 
	 */
	public CompanyAddress() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param resultSet
	 * @throws SQLException 
	 * 
	 */
	public CompanyAddress(ResultSet rs) throws SQLException {
		this.companyAddressId           = rs.getInt("company_Address_Id");
		this.addressLine1             	= rs.getString("address_Line_1");
		this.addressLine2             	= rs.getString("address_Line_2");
		this.addressLine3             	= rs.getString("address_Line_3");
		this.cityName             		= rs.getString("city_Name");
		this.provinceId              	= rs.getInt("province_Id");
		this.countryId              	= rs.getInt("country_id");
		this.postalCd             		= rs.getString("postal_Cd");
		this.createUserId               = rs.getInt("create_User_Id");
		this.createDt             		= rs.getTimestamp("create_dt");
		this.updateUserId               = rs.getInt("update_User_Id");
		this.updateDt             		= rs.getTimestamp("update_Dt");
		
	}

	public int getCompanyAddressId() {
		return companyAddressId;
	}

	public void setCompanyAddressId(int companyAddressId) {
		this.companyAddressId = companyAddressId;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public int getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public int getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public int getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getPostalCd() {
		return postalCd;
	}

	public void setPostalCd(String postalCd) {
		this.postalCd = postalCd;
	}

	@Override
	public String toString() {
		return "CompanyAddress [companyAddressId=" + companyAddressId + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", addressLine3=" + addressLine3 + ", cityName=" + cityName
				+ ", provinceId=" + provinceId + ", countryId=" + countryId + ", postalCd=" + postalCd
				+ ", createUserId=" + createUserId + ", createDt=" + createDt + ", updateUserId=" + updateUserId
				+ ", updateDt=" + updateDt + "]";
	}

}
