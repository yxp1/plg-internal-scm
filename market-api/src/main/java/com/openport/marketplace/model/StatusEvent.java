package com.openport.marketplace.model;

public class StatusEvent {

	private String application;
	private String eventCode;
	private String parentPartnerCode;
	private String partnerCode;
	private String status;
	private String eventDescription;
	private String eventLocationCode;

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getParentPartnerCode() {
		return parentPartnerCode;
	}

	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getEventLocationCode() {
		return eventLocationCode;
	}

	public void setEventLocationCode(String eventLocationCode) {
		this.eventLocationCode = eventLocationCode;
	}

}
