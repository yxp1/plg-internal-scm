package com.openport.marketplace.constants;

public enum LogisticsFilter {
	ALL(0), 
	EXCLUDE_CONSOLIDATED_ORDERS(1), 
	ONLY_CONSOLIDATED_LOADS(2),
	EXCLUDE_CANCELLED_SHIPMENT(3);

	private int value;

	LogisticsFilter(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}