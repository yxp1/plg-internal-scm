package com.openport.marketplace.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.openport.marketplace.json.CompanyContractTenderLane;
import com.openport.util.db.ResultSetMapper;

public class BidRepository {
	
	static final transient Logger log = Logger.getLogger(BidRepository.class);
	
	public Map<String,Object> findTenderLaneById(int id)throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResultList =  new LinkedList<Map<String,Object>>();
		Map<String,Object> dataResult = new LinkedHashMap<String,Object>();
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM company_contract_tender_lane WHERE COMPANY_CONTRACT_TENDER_LANE_ID = ?");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			p.setInt(1, id);
			
			ResultSet rs = p.executeQuery();
			populateDataResult(dataResultList,rs);
			
			if(!dataResultList.isEmpty())
				dataResult = dataResultList.get(dataResultList.size()-1);
		}
		log.info("---  END METHOD CALL ---");
		
		return dataResult;
	}
	
	public Map<String,Object> findTenderLaneByContractNbr(String contractNbr)throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResultList =  new LinkedList<Map<String,Object>>();
		Map<String,Object> dataResult = new LinkedHashMap<String,Object>();
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM company_contract_tender_lane WHERE CONTRACT_NBR = ?");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			p.setString(1, contractNbr);
			
			ResultSet rs = p.executeQuery();
			populateDataResult(dataResultList,rs);
			
			if(!dataResultList.isEmpty())
				dataResult = dataResultList.get(dataResultList.size()-1);
		}
		log.info("---  END METHOD CALL ---");
		
		return dataResult;
	}
	
	public Integer updateBidByContractNbr(String contractNbr, Double rateAmt)throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		StringBuilder query = new StringBuilder();
		query.append("UPDATE company_contract_tender_lane SET RATE_AMT = ? WHERE CONTRACT_NBR = ? ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			int i = 0;

			p.setDouble(++i, rateAmt);
			p.setString(++i, contractNbr);

			log.info("---  END METHOD CALL ---");
			return p.executeUpdate();
		}
	}
	
	public Map<String,Object> getUserInfo(String token) throws SQLException{
		
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResultList =  new LinkedList<Map<String,Object>>();
		Map<String,Object> dataResult = new LinkedHashMap<String,Object>();
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT u.*,c.* ")
		.append("FROM user_session us ")
		.append("JOIN user u ON u.User_ID = us.User_ID ")
		.append("JOIN company_user cu ON cu.User_ID = u.User_ID ")
		.append("JOIN company c ON c.Company_ID = cu.Company_ID  ")
		.append("WHERE us.token =  ?");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			p.setString(1, token);
			
			ResultSet rs = p.executeQuery();
			populateDataResult(dataResultList,rs);
			
			if(!dataResultList.isEmpty())
				dataResult = dataResultList.get(dataResultList.size()-1);
		}
		log.info("---  END METHOD CALL ---");
		
		return dataResult;
	}
	
	public List<Map<String,Object>> getRatePerUnit() throws SQLException{
		
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResult =  new LinkedList<Map<String,Object>>();
		StringBuilder query = new StringBuilder();

		query.append("SELECT * FROM rate_per_unit ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			ResultSet rs = p.executeQuery();
			populateDataResult(dataResult,rs);
		}
		log.info("---  END METHOD CALL ---");
		
		return dataResult;
	}
	
	public List<Map<String,Object>> getShippers2(Integer userCompanyId) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResult =  new LinkedList<Map<String,Object>>();
		StringBuilder query = new StringBuilder();

		query
		.append("SELECT DISTINCT sc.Company_ID, sc.Company_Name, sc.Company_Code ")
		.append("FROM company sc ")
		.append("JOIN company_contract_tender_lane cctl ON sc.Company_ID = cctl.SHIPPER_COMPANY_ID AND cctl.CARRIER_COMPANY_ID = ? ")
		.append("WHERE sc.Is_Shipper = 1");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			int i = 0;
			p.setInt(++i, userCompanyId);
			
			ResultSet rs = p.executeQuery();
			populateDataResult(dataResult,rs);
		}
		log.info("---  END METHOD CALL ---");
		return dataResult;
	}
	
	public List<Map<String,Object>> getContracts2(Integer userCompanyId, Integer status, 
			Integer shipperId, String contractNbr, String from, String to) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResult =  new LinkedList<Map<String,Object>>();
		List<Object> values = new LinkedList<Object>();
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT cctl.COMPANY_CONTRACT_TENDER_LANE_ID,cctl.SHIPPER_COMPANY_ID, c.Company_Name,c.Company_Code, ")
		.append("cctl.CONTRACT_NBR, DATE_FORMAT(cctl.RATE_EFFECTIVE_DT,'%Y/%m/%d') as RATE_EFFECTIVE_DT, DATE_FORMAT(cctl.RATE_TERMINATION_DT,'%Y/%m/%d') as RATE_TERMINATION_DT, ")
		.append("l.lane_id, l.origin_district,l.origin_city, l.origin_country, l.dest_district, l.dest_city, l.dest_country, ")
		.append("cc.COMMODITY_CD, cc.COMMODITY_DESC_TXT, cc.CATEGORY_CD, ")
		.append("e.Equipment_Type_ID, e.Equipment_Type_Name, ")
		.append("cctl.RATE_PER_UNIT_CD, cctl.CURRENCY_CD, CASE WHEN cctl.rate_amt IS NULL THEN '0' ELSE cctl.rate_amt END AS rate_amt, CASE WHEN (cctl.rate_amt = NULL OR cctl.rate_amt = 0 ) AND cctl.STATUS_CD = 'U' THEN '0' ELSE cctl.STATUS_CD END AS status_cd ")
		.append("FROM company_contract_tender_lane cctl ")
		.append("JOIN company c ON c.Company_ID = cctl.SHIPPER_COMPANY_ID ")
		.append("JOIN lane l ON l.lane_id = cctl.LANE_ID ")
		.append("JOIN commodity_code cc ON cc.COMMODITY_CD = cctl.COMMODITY_CD ")
		.append("JOIN equipment_type e ON e.Equipment_Type_ID = cctl.EQUIPMENT_TYPE_ID ")
		.append("WHERE cctl.STATUS_CD != 'R' AND cctl.CARRIER_COMPANY_ID = ? ");
		values.add(userCompanyId);
		
		if(status != null){
			query.append("AND cctl.STATUS_CD = ? ");
			switch(status){
				case 1 : 
					values.add("U");
					query.append("AND (cctl.rate_amt != NULL OR cctl.rate_amt > 0 ) ");
				break;
				case 2 : values.add("A");break;
				case 3 : values.add("E");break;
				case 4 : values.add("W");break;
				default: 
					values.add("U");
					query.append("AND (cctl.rate_amt = NULL OR cctl.rate_amt = 0 ) ");
				break;
			}
		}	
		
		if(shipperId != null){
			query.append("AND cctl.SHIPPER_COMPANY_ID = ? ");
			values.add(shipperId);
		}
		
		if(contractNbr != null){
			query.append("AND cctl.CONTRACT_NBR = ? ");
			values.add(contractNbr);
		}
		
		if(from != null && to != null){
			query.append("AND cctl.RATE_EFFECTIVE_DT BETWEEN STR_TO_DATE(?,'%m/%d/%Y') AND STR_TO_DATE(?,'%m/%d/%Y') ");
			values.add(from.trim());
			values.add(to.trim());
		}
		
		query.append("ORDER BY cctl.RATE_EFFECTIVE_DT, cctl.CONTRACT_NBR,l.lane_id,e.Equipment_Type_ID ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			
			int i = 0;
			for(Object value : values){
				p.setObject(++i, value);
			}
			
			ResultSet rs = p.executeQuery();
			dataResult = segregateContractInfo(populateDataResult(dataResult,rs));
		}
		log.info("---  END METHOD CALL ---");
		return dataResult;
	}
	
	public Integer submitTenderLaneRate(CompanyContractTenderLane companyContractTenderLane, Integer userId) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		StringBuilder query = new StringBuilder();

		query.append("UPDATE company_contract_tender_lane ")
		.append("SET status_cd = 'U', rate_amt = ?, RATE_PER_UNIT_CD = ?, update_user_id = ?, update_dt = NOW() ")
		.append("WHERE COMPANY_CONTRACT_TENDER_LANE_ID = ? ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			
			int i = 0;
			p.setDouble(++i, companyContractTenderLane.getRateAmt());
			p.setString(++i, companyContractTenderLane.getRatePerUnitCd());
			p.setInt(++i, userId);
			p.setInt(++i, companyContractTenderLane.getCompanyContractTenderLaneId());
			log.info("---  END METHOD CALL ---");
			return p.executeUpdate();
		}
	}
	
	public Integer editTenderLaneRate(CompanyContractTenderLane companyContractTenderLane, Integer userId) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		StringBuilder query = new StringBuilder();

		query.append("UPDATE company_contract_tender_lane ")
		.append("SET status_cd = 'U',  rate_amt = '0', update_user_id = ?, update_dt = NOW() ")
		.append("WHERE COMPANY_CONTRACT_TENDER_LANE_ID = ? ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			
			int i = 0;
			p.setInt(++i, userId);
			p.setInt(++i, companyContractTenderLane.getCompanyContractTenderLaneId());
			log.info("---  END METHOD CALL ---");
			return p.executeUpdate();
		}
	}
	
	public Integer removeTenderLaneRate(List<String> contractTenderLaneIds, Integer userId) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		StringBuilder query = new StringBuilder();

		query.append("UPDATE company_contract_tender_lane ")
		.append("SET status_cd = 'R', update_user_id = ?, update_dt = NOW() ")
		.append("WHERE COMPANY_CONTRACT_TENDER_LANE_ID IN ("+StringUtils.join(contractTenderLaneIds.toArray(),",")+") ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			
			int i = 0;
			
			p.setInt(++i, userId);
			log.info("---  END METHOD CALL ---");
			return p.executeUpdate();
		}
	}
	
	public List<Map<String,Object>> getContractsByTenderLaneId(Integer companyId, List<String> tenderIds, Boolean isShipper, Boolean isCarrier) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResult =  new LinkedList<Map<String,Object>>();
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT cctl.COMPANY_CONTRACT_TENDER_LANE_ID,cctl.SHIPPER_COMPANY_ID, c.Company_Name,c.Company_Code, ")
		.append("cctl.CONTRACT_NBR, DATE_FORMAT(cctl.RATE_EFFECTIVE_DT,'%Y/%m/%d') as RATE_EFFECTIVE_DT, DATE_FORMAT(cctl.RATE_TERMINATION_DT,'%Y/%m/%d') as RATE_TERMINATION_DT, ")
		.append("l.lane_id, l.origin_district,l.origin_city, l.origin_country, l.dest_district, l.dest_city, l.dest_country, ")
		.append("cc.COMMODITY_CD, cc.COMMODITY_DESC_TXT, cc.CATEGORY_CD, ")
		.append("e.Equipment_Type_ID, e.Equipment_Type_Name, ")
		.append("cctl.RATE_PER_UNIT_CD, cctl.CURRENCY_CD, CASE WHEN cctl.rate_amt IS NULL THEN '0' ELSE cctl.rate_amt END AS rate_amt, CASE WHEN (cctl.rate_amt = NULL OR cctl.rate_amt > 0 ) AND cctl.STATUS_CD = 'U' THEN 'P' ELSE cctl.STATUS_CD END AS status_cd, ")
		.append("CASE WHEN cctl.BID_RANK = 0 THEN '~' ELSE cctl.BID_RANK END as bid_rank ")
		.append("FROM company_contract_tender_lane cctl ")
		.append("JOIN company c ON c.Company_ID = cctl.SHIPPER_COMPANY_ID ")
		.append("JOIN lane l ON l.lane_id = cctl.LANE_ID ")
		.append("JOIN commodity_code cc ON cc.COMMODITY_CD = cctl.COMMODITY_CD ")
		.append("JOIN equipment_type e ON e.Equipment_Type_ID = cctl.EQUIPMENT_TYPE_ID ");
		
		if(isShipper)query.append("WHERE cctl.SHIPPER_COMPANY_ID = ? ");
		else if(isCarrier)query.append("WHERE cctl.CARRIER_COMPANY_ID = ? ");
		
		query.append("AND cctl.COMPANY_CONTRACT_TENDER_LANE_ID IN ("+StringUtils.join(tenderIds.toArray(),",")+") ")
		.append("ORDER BY cctl.RATE_EFFECTIVE_DT, cctl.CONTRACT_NBR,l.lane_id,e.Equipment_Type_ID ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			int i =0;
			p.setInt(++i, companyId);
			ResultSet rs = p.executeQuery();
			dataResult = populateDataResult(dataResult,rs);
		}
		log.info("---  END METHOD CALL ---");
		return dataResult;
	}
	
	
//	public List<Map<String,Object>> getContractTendersByTenderLaneId(Integer companyId, List<String> tenderIds, Boolean isShipper, Boolean isCarrier) throws SQLException{
//		log.info("---  BEGIN METHOD CALL ---");
//		
//		List<Map<String,Object>> dataResult =  new LinkedList<Map<String,Object>>();
//		StringBuilder query = new StringBuilder();
//		
//		query.append("SELECT cctl.SHIPPER_COMPANY_ID, c.Company_Name,c.Company_Code, ")
//		.append("DATE_FORMAT(cctl.RATE_EFFECTIVE_DT,'%Y/%m/%d') as RATE_EFFECTIVE_DT, DATE_FORMAT(cctl.RATE_TERMINATION_DT,'%Y/%m/%d') as RATE_TERMINATION_DT, ")
//		.append("l.lane_id, l.origin_district,l.origin_city, l.origin_country, l.dest_district, l.dest_city, l.dest_country, ")
//		.append("cc.COMMODITY_CD, cc.COMMODITY_DESC_TXT, cc.CATEGORY_CD, ")
//		.append("e.Equipment_Type_ID, e.Equipment_Type_Name ")
//		.append("FROM company_contract_tender_lane cctl ")
//		.append("JOIN company c ON c.Company_ID = cctl.SHIPPER_COMPANY_ID ")
//		.append("JOIN lane l ON l.lane_id = cctl.LANE_ID ")
//		.append("JOIN commodity_code cc ON cc.COMMODITY_CD = cctl.COMMODITY_CD ")
//		.append("JOIN equipment_type e ON e.Equipment_Type_ID = cctl.EQUIPMENT_TYPE_ID ");
//		
//		if(isShipper)query.append("WHERE cctl.SHIPPER_COMPANY_ID = ? ");
//		else if(isCarrier)query.append("WHERE cctl.CARRIER_COMPANY_ID = ? ");
//		
//		query.append("AND cctl.COMPANY_CONTRACT_TENDER_LANE_ID IN ("+StringUtils.join(tenderIds.toArray(),",")+") ")
//		.append("GROUP BY cctl.SHIPPER_COMPANY_ID, c.Company_Name,c.Company_Code, cctl.RATE_EFFECTIVE_DT,cctl.RATE_TERMINATION_DT, ")
//		.append("l.lane_id, l.origin_district,l.origin_city, l.origin_country, l.dest_district, l.dest_city, l.dest_country, ")
//		.append("cc.COMMODITY_CD, cc.COMMODITY_DESC_TXT, cc.CATEGORY_CD, e.Equipment_Type_ID, e.Equipment_Type_Name ")
//		.append("ORDER BY cctl.RATE_EFFECTIVE_DT,l.lane_id,e.Equipment_Type_ID ");
//		
//		log.debug("SQL: "+query.toString());
//		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
//			PreparedStatement p = conn.prepareStatement(query.toString());
//			int i =0;
//			p.setInt(++i, companyId);
//			ResultSet rs = p.executeQuery();
//			dataResult = populateDataResult(dataResult,rs);
//		}
//		log.info("---  END METHOD CALL ---");
//		return dataResult;
//	}
	
	public List<Map<String,Object>> getContractTendersByTenderLaneId(Integer companyId, List<String> tenderIds, Boolean isShipper, Boolean isCarrier) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResult =  new LinkedList<Map<String,Object>>();
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT cctl.COMPANY_CONTRACT_TENDER_LANE_ID,cctl.CARRIER_COMPANY_ID, c.Company_Name,c.Company_Code, ")
		.append("cctl.CONTRACT_NBR, DATE_FORMAT(cctl.RATE_EFFECTIVE_DT,'%Y/%m/%d') as RATE_EFFECTIVE_DT, DATE_FORMAT(cctl.RATE_TERMINATION_DT,'%Y/%m/%d') as RATE_TERMINATION_DT, ")
		.append("l.lane_id, l.origin_district,l.origin_city, l.origin_country, l.dest_district, l.dest_city, l.dest_country, ")
		.append("cc.COMMODITY_CD, cc.COMMODITY_DESC_TXT, cc.CATEGORY_CD, ")
		.append("e.Equipment_Type_ID, e.Equipment_Type_Name, ")
		.append("cctl.RATE_PER_UNIT_CD, cctl.CURRENCY_CD, CASE WHEN cctl.rate_amt IS NULL THEN '0' ELSE cctl.rate_amt END AS rate_amt, CASE WHEN (cctl.rate_amt = NULL OR cctl.rate_amt > 0 ) AND cctl.STATUS_CD = 'U' THEN 'P' ELSE cctl.STATUS_CD END AS status_cd, ")
		.append("CASE WHEN cctl.BID_RANK = 0 THEN '~' ELSE cctl.BID_RANK END as bid_rank ")
		.append("FROM company_contract_tender_lane cctl ")
		.append("JOIN company c ON c.Company_ID = cctl.CARRIER_COMPANY_ID ")
		.append("JOIN lane l ON l.lane_id = cctl.LANE_ID ")
		.append("JOIN commodity_code cc ON cc.COMMODITY_CD = cctl.COMMODITY_CD ")
		.append("JOIN equipment_type e ON e.Equipment_Type_ID = cctl.EQUIPMENT_TYPE_ID ");
		
		if(isShipper)query.append("WHERE cctl.SHIPPER_COMPANY_ID = ? ");
		else if(isCarrier)query.append("WHERE cctl.CARRIER_COMPANY_ID = ? ");
		
		query.append("AND cctl.COMPANY_CONTRACT_TENDER_LANE_ID IN ("+StringUtils.join(tenderIds.toArray(),",")+") ")
		.append("ORDER BY cctl.RATE_EFFECTIVE_DT, cctl.CONTRACT_NBR,l.lane_id,e.Equipment_Type_ID ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			int i =0;
			p.setInt(++i, companyId);
			ResultSet rs = p.executeQuery();
			dataResult = populateDataResult(dataResult,rs);
		}
		log.info("---  END METHOD CALL ---");
		return dataResult;
	}
	
	public List<Map<String,Object>> getContractsByTenderLaneIdByCompany(Integer companyId, List<String> tenderIds) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> dataResult =  new LinkedList<Map<String,Object>>();
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT cctl.COMPANY_CONTRACT_TENDER_LANE_ID,cctl.SHIPPER_COMPANY_ID, c.Company_Name,c.Company_Code, ")
		.append("cctl.CONTRACT_NBR, DATE_FORMAT(cctl.RATE_EFFECTIVE_DT,'%Y/%m/%d') as RATE_EFFECTIVE_DT, DATE_FORMAT(cctl.RATE_TERMINATION_DT,'%Y/%m/%d') as RATE_TERMINATION_DT, ")
		.append("l.lane_id, l.origin_district,l.origin_city, l.origin_country, l.dest_district, l.dest_city, l.dest_country, ")
		.append("cc.COMMODITY_CD, cc.COMMODITY_DESC_TXT, cc.CATEGORY_CD, ")
		.append("e.Equipment_Type_ID, e.Equipment_Type_Name, ")
		.append("cctl.RATE_PER_UNIT_CD, cctl.CURRENCY_CD, CASE WHEN cctl.rate_amt IS NULL THEN '0' ELSE cctl.rate_amt END AS rate_amt, CASE WHEN (cctl.rate_amt = NULL OR cctl.rate_amt > 0 ) AND cctl.STATUS_CD = 'U' THEN 'P' ELSE cctl.STATUS_CD END AS status_cd ")
		.append("FROM company_contract_tender_lane cctl ")
		.append("JOIN company c ON c.Company_ID = cctl.SHIPPER_COMPANY_ID ")
		.append("JOIN lane l ON l.lane_id = cctl.LANE_ID ")
		.append("JOIN commodity_code cc ON cc.COMMODITY_CD = cctl.COMMODITY_CD ")
		.append("JOIN equipment_type e ON e.Equipment_Type_ID = cctl.EQUIPMENT_TYPE_ID ")
		.append("WHERE cctl.SHIPPER_COMPANY_ID = ? AND cctl.COMPANY_CONTRACT_TENDER_LANE_ID IN ("+StringUtils.join(tenderIds.toArray(),",")+") ")
		.append("ORDER BY cctl.RATE_EFFECTIVE_DT, cctl.CONTRACT_NBR,l.lane_id,e.Equipment_Type_ID ");
		
		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			int i =0;
			p.setInt(++i, companyId);
			ResultSet rs = p.executeQuery();
			dataResult = populateDataResult(dataResult,rs);
		}
		log.info("---  END METHOD CALL ---");
		return dataResult;
	}
	
	
	private List<Map<String,Object>> populateDataResult(List<Map<String,Object>> dataResult, ResultSet rs) throws SQLException{
		log.info("---  BEGIN METHOD CALL ---");
		
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		while(rs.next()){
			Map<String,Object> rowData = new LinkedHashMap<String,Object>();
			for (int i = 1; i <= columnCount; i++ ) {
			  String columnLabel = rsmd.getColumnLabel(i);
			  rowData.put(columnLabel, rs.getObject(columnLabel));
			}
			dataResult.add(rowData);
		}
		log.info("---  END METHOD CALL ---");
		return dataResult;
	}
	
	private List<Map<String,Object>> segregateContractInfo(List<Map<String,Object>> contracts){
		log.info("---  BEGIN METHOD CALL ---");
		
		List<Map<String,Object>> contractList = new LinkedList<Map<String,Object>>();
		List<Map<String,Object>> laneCommodityList = null;
		
		if(contracts.isEmpty())
			return contractList;
		
		Map<String,Object> contractInfo = null;
		Map<String,Object> laneCommodityInfo = null;
		
		Integer shipperId = -1;
		for(Map<String,Object> contract : contracts){
			
			if(!shipperId.equals(contract.get("SHIPPER_COMPANY_ID"))){
				
				if(shipperId != -1)
					contractList.add(contractInfo);

				contractInfo = new LinkedHashMap<String,Object>();				
				contractInfo.put("SHIPPER_COMPANY_ID", contract.get("SHIPPER_COMPANY_ID"));
				contractInfo.put("Company_Name", contract.get("Company_Name"));
				contractInfo.put("Company_Code", contract.get("Company_Code"));
	
				laneCommodityList = new LinkedList<Map<String,Object>>();
				shipperId = (Integer)contract.get("SHIPPER_COMPANY_ID");
				contractInfo.put("laneCommodity", laneCommodityList);
			} 
	
			laneCommodityInfo = new LinkedHashMap<String,Object>();
			
			laneCommodityInfo.put("COMPANY_CONTRACT_TENDER_LANE_ID", contract.get("COMPANY_CONTRACT_TENDER_LANE_ID"));
			laneCommodityInfo.put("CONTRACT_NBR", contract.get("CONTRACT_NBR"));
			laneCommodityInfo.put("RATE_EFFECTIVE_DT", contract.get("RATE_EFFECTIVE_DT"));
			laneCommodityInfo.put("RATE_TERMINATION_DT", contract.get("RATE_TERMINATION_DT"));
			laneCommodityInfo.put("lane_id", contract.get("lane_id"));
			laneCommodityInfo.put("origin_district", contract.get("origin_district"));
			laneCommodityInfo.put("origin_city", contract.get("origin_city"));
			laneCommodityInfo.put("origin_country", contract.get("origin_country"));
			laneCommodityInfo.put("dest_district", contract.get("dest_district"));
			laneCommodityInfo.put("dest_city", contract.get("dest_city"));
			laneCommodityInfo.put("dest_country", contract.get("dest_country"));
			laneCommodityInfo.put("CURRENCY_CD", contract.get("CURRENCY_CD"));
			laneCommodityInfo.put("COMMODITY_CD", contract.get("COMMODITY_CD"));
			laneCommodityInfo.put("COMMODITY_DESC_TXT", contract.get("COMMODITY_DESC_TXT"));
			laneCommodityInfo.put("CATEGORY_CD", contract.get("CATEGORY_CD"));
			laneCommodityInfo.put("Equipment_Type_ID", contract.get("Equipment_Type_ID"));
			laneCommodityInfo.put("Equipment_Type_Name", contract.get("Equipment_Type_Name"));

			laneCommodityInfo.put("RATE_PER_UNIT_CD", contract.get("RATE_PER_UNIT_CD"));
			laneCommodityInfo.put("rate_amt", contract.get("rate_amt"));
			laneCommodityInfo.put("status_cd", contract.get("status_cd"));
			
			laneCommodityList.add(laneCommodityInfo);
		}
		contractList.add(contractInfo);
		log.info("---  END METHOD CALL ---");
		return contractList;
	}
}
