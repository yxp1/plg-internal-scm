package com.openport.marketplace.repository;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.mysql.jdbc.Statement;
import com.openport.marketplace.json.ServiceException;

public class TmsParameterRepository {

	private static final String SQL_PARAMTER_QUERY = "SELECT PARAMETER_%d_VALUE FROM tms_utf8_db.tms_parameter where PARAMETER_CD = ? AND PARAMETER_NAME = ? LIMIT 1";
    static final transient Logger log = Logger.getLogger(TmsParameterRepository.class);

    public TmsParameterRepository() {
	// TODO Auto-generated constructor stub
    }

    public static String getDefaultParameters() {

//	ClassLoader classLoader = TmsParameterRepository.class.getClassLoader();
//	File file = new File(classLoader.getResource("DefaultParametersCSV.txt").getFile());
	int seqNumber = getStratingSeqNum();
	JsonArrayBuilder jsonArray = Json.createArrayBuilder(); 
	String sql = "select * from tms_utf8_db.tms_parameter where PARAMETER_CD = 'TMS_DEFAULT_PARAM';";
	try (Connection connection = DbConfigTmsUtf8.getDataSource().getConnection(); 
		PreparedStatement ps = connection.prepareStatement(sql)) {

	    ResultSet rs = ps.executeQuery();
	    while (rs.next()) {
		JsonObjectBuilder jsonObject = Json.createObjectBuilder();
		// Default
		jsonObject.add("PARAMETER_CD", "TMS_ROUTINES");
		// User Input
		jsonObject.add("PARAMETER_SEQ_NBR", ++seqNumber);
		// User Input + values[0] Data from CSV File
		jsonObject.add("PARAMETER_NAME", ((rs.getString("PARAMETER_NAME") == null || rs.getString("PARAMETER_NAME") == "") ? "" : rs.getString("PARAMETER_NAME").trim()));
		// Default
		jsonObject.add("PROGRAM_ID", "SHIPMENT_DEFAULT_ATTRIBUTES");
		// Default
		jsonObject.add("REQUIRED_FLG", "Y");
		// Data from CSV File
		jsonObject.add("PARAMETER_1_VALUE", ((rs.getString("PARAMETER_1_VALUE") == null || rs.getString("PARAMETER_1_VALUE") == "") ? "" : rs.getString("PARAMETER_1_VALUE").trim()));
		// User Input
		jsonObject.add("PARAMETER_1_DESC_TXT", ((rs.getString("PARAMETER_1_DESC_TXT") == null || rs.getString("PARAMETER_1_DESC_TXT") == "") ? "" : rs.getString("PARAMETER_1_DESC_TXT").trim()));
		// Default
		jsonObject.add("PARAMETER_2_VALUE", JsonObject.NULL);
		// Default
		jsonObject.add("PARAMETER_2_DESC_TXT", JsonObject.NULL);
		// Default
		jsonObject.add("PARAMETER_3_VALUE", JsonObject.NULL);
		// Default
		jsonObject.add("PARAMETER_3_DESC_TXT", JsonObject.NULL);
		// Default
		jsonObject.add("PARAMETER_4_VALUE", JsonObject.NULL);
		// Default
		jsonObject.add("PARAMETER_4_DESC_TXT", JsonObject.NULL);
		// Date today to be filled up by JS
		jsonObject.add("EFFECTIVE_DT", "now()");
		// Date today plus 10 years to be filled up by JS
		jsonObject.add("TERMINATION_DT", "date_add(now(),INTERVAL 10 YEAR)");
		// lecel said to be default but I think this needs to be
		// clarified further
		jsonObject.add("CREATE_USER_ID", "rnoman");
		// Date today to be filled up by JS
		jsonObject.add("CREATE_TSTAMP", "now()");
		// lecel said to be default but I think this needs to be
		// clarified further
		jsonObject.add("UPDATE_USER_ID", "rnoman");
		// Date today to be filled up by JS
		jsonObject.add("UPDATE_TSTAMP", "now()");
		jsonObject.add("eEditable", -1);
		// Add the object to the JSONARRAY
		jsonArray.add(jsonObject.build());
	    }
	    
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	
	StringWriter stringWriter = new StringWriter();
	JsonWriter writer = Json.createWriter(stringWriter);
	writer.writeArray(jsonArray.build());
	writer.close();

	return stringWriter.getBuffer().toString();
    }
    
    private static Integer getStratingSeqNum(){
	String sql = "select parameter_seq_nbr from tms_utf8_db.tms_parameter order by PARAMETER_SEQ_NBR DESC limit 1;";
	try (Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
		PreparedStatement ps = connection.prepareStatement(sql)) {

	    ResultSet rs = ps.executeQuery();
	    while (rs.next()) {
		return Integer.parseInt(rs.getString("parameter_seq_nbr"));
	    }
	    
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return null;
    }

    public static String getRoles() {
	String sql = "SELECT DISTINCT (role) FROM tms_utf8_db.role";
	try (Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
		PreparedStatement ps = connection.prepareStatement(sql)) {

	    ResultSet rs = ps.executeQuery();
	    JsonArrayBuilder jsonArray = Json.createArrayBuilder();
	    while (rs.next()) {
		jsonArray.add(rs.getString("role"));
	    }
	    StringWriter stringWriter = new StringWriter();
	    JsonWriter writer = Json.createWriter(stringWriter);
	    writer.writeArray(jsonArray.build());
	    writer.close();

	    return stringWriter.getBuffer().toString();
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return "";
    }

    public static List<String> generateTableQueries(List<Map<String, Object>> tableParameters) {

	List<String> TmsParameterQueryList = new ArrayList<String>();
	StringBuilder query = new StringBuilder();

	for (Map<String, Object> params : tableParameters) {
	    query = new StringBuilder();
	    params.remove("$$hashKey");
	    params.remove("starting_url");
	    params.remove("eEditable");
	    if (params.get("PARAMETER_SEQ_NBR") == null || params.get("PARAMETER_SEQ_NBR").toString().isEmpty()) {
		params.put("PARAMETER_SEQ_NBR", "");
	    } else {
		params.put("PARAMETER_SEQ_NBR", Integer.parseInt(params.get("PARAMETER_SEQ_NBR").toString()));
	    }

	    query.append("insert into tms_utf8_db.TMS_PARAMETER(");

	    Object[] keys = params.keySet().toArray();
	    for (int keyIndex = 0; keyIndex < keys.length; keyIndex++) {
		if (keyIndex > 0) {
		    query.append(",");
		}
		query.append(keys[keyIndex].toString().trim());
	    }

	    query.append(") values (");

	    Object[] values = params.values().toArray();
	    for (int valueIndex = 0; valueIndex < values.length; valueIndex++) {
		if (valueIndex > 0) {
		    query.append(",");
		}
		if (values[valueIndex] instanceof String) {
		    if (values[valueIndex].toString().contains("now()")) {
			query.append(values[valueIndex].toString().trim());
		    } else {
			if (!values[valueIndex].toString().isEmpty()) {
			    query.append("'");
			    values[valueIndex] = values[valueIndex].toString().replace("'", "\\'");
			    query.append(values[valueIndex]);
			    query.append("'");
			} else {
			    query.append("'");
			    query.append("'");
			}
		    }
		} else if (values[valueIndex] instanceof Integer) {
		    query.append(values[valueIndex]);
		} else {
		    query.append("null");
		}
	    }

	    query.append(");");
	    TmsParameterQueryList.add(query.toString());

	}

	return TmsParameterQueryList;
    }
    
	public static List<String> generateUpdateTableParameterQueries(List<Map<String, Object>> tableParameters) {

		List<String> TmsParameterQueryList = new ArrayList<String>();
		StringBuilder query = null;

		for (Map<String, Object> params : tableParameters) {
			if (params.get("PARAMETER_SEQ_NBR") == null || params.get("PARAMETER_SEQ_NBR").toString().isEmpty()) {
				params.put("PARAMETER_SEQ_NBR", "");
			} else {
				params.put("PARAMETER_SEQ_NBR", Integer.parseInt(params.get("PARAMETER_SEQ_NBR").toString()));
			}
			
			int sequenceNumber = Integer.parseInt(params.get("PARAMETER_SEQ_NBR").toString());
			String code = params.get("PARAMETER_CD").toString();
			String name = params.get("PARAMETER_NAME").toString();
			
			query = new StringBuilder();
			
			params.remove("$$hashKey");
			params.remove("starting_url");
			params.remove("eEditable");
			params.remove("PARAMETER_CD");
			params.remove("PARAMETER_SEQ_NBR");
			params.remove("PARAMETER_NAME");
			
			query.append("update tms_utf8_db.TMS_PARAMETER SET ");
			
			Object[] keys = params.keySet().toArray();
			Object[] values = params.values().toArray();
			
			for (int index = 0; index < keys.length; index++) {
				if (index > 0) {
					query.append(", ");
				}
				query.append(keys[index].toString().trim());
				
				if (values[index] instanceof String) {
					if (values[index].toString().contains("now()")) {
						query.append(" = ");
						query.append(values[index].toString().trim());
					} else {
						if (!values[index].toString().isEmpty()) {
							query.append(" = '");
							values[index] = values[index].toString().replace("'", "\\'");
							query.append(values[index]);
							query.append("'");
						} else {
							query.append(" = '");
							query.append("'");
						}
					}
				} else if (values[index] instanceof Integer) {
					query.append(" = ");
					query.append(values[index]);
				} else {
					query.append(" = null");
				}
			}
			
			query.append("WHERE PARAMETER_CD = '");
			query.append(code);
			query.append("'");
			
			query.append(" AND PARAMETER_SEQ_NBR = ");
			query.append(sequenceNumber);
			
			query.append(" AND PARAMETER_NAME = '");
			query.append(name);
			query.append("';");
			
			TmsParameterQueryList.add(query.toString());
		}

		return TmsParameterQueryList;
	}

    public static void insertQueries (List<String> queryList) {

	int executeUpdate = 0;
	try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection()) {
	    for (String sql : queryList) {
		System.out.println(sql);
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.executeUpdate();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("Error: " + e.getMessage());
	    new ServiceException(Response.Status.CONFLICT);
	} finally {
	    log.info("Total number inserted: " + executeUpdate);
	}
    }

    public static List<String> generateAdditionalQueries(String userInfo, String role, String userRole)
	    throws JsonParseException, JsonMappingException, IOException, SQLException {

	List<String> additionalQueriesList = new ArrayList<String>();
	ObjectMapper mapper = new ObjectMapper();

	Map<String, Object> userInfoMap = mapper.readValue(userInfo, new TypeReference<Map<String, Object>>() {
	});

	String userInfoQuery = createQuery(userInfoMap, "tms_utf8_db.user_info");
	additionalQueriesList.add(userInfoQuery);
	
	List<String> roleQueries = createRoleQuery(mapper.readValue(role, new TypeReference<List<String>>() {
	}), (String) userInfoMap.get("PARTNER_CD"));

	additionalQueriesList.addAll(roleQueries);

	List<String> userRoleQueries = createUserRoleQuery(
		mapper.readValue(userRole, new TypeReference<List<String>>() {
		}), userInfoMap.get("PARENT_PARTNER_CD").toString());
	additionalQueriesList.addAll(userRoleQueries);
	
	String userAccountQuery = createUserAccountQuery(userInfoMap.get("USER_ID").toString(),
		userInfoMap.get("PARTNER_CD").toString());
	additionalQueriesList.add(userAccountQuery);
	
	String passwordProfileQuery = createPasswordProfileQuery(userInfoMap.get("PARENT_PARTNER_CD").toString(),
		userInfoMap.get("PARTNER_CD").toString());
	additionalQueriesList.add(passwordProfileQuery);

	List<String> packageUomQueries = createPackageUOMQuery(userInfoMap.get("PARENT_PARTNER_CD").toString());
	additionalQueriesList.addAll(packageUomQueries);

	List<String> guiDefaultValueQueries = createGuiDefaultValueQuery(
		userInfoMap.get("PARENT_PARTNER_CD").toString());
	additionalQueriesList.addAll(guiDefaultValueQueries);

	List<String> equipmentTranslatorQueries = createEquipmentTranslatorQuery(
		userInfoMap.get("PARENT_PARTNER_CD").toString());
	additionalQueriesList.addAll(equipmentTranslatorQueries);

	return additionalQueriesList;
    }

    private static List<String> createEquipmentTranslatorQuery(String parentPartnerCode) {
	List<String> equipmentTranslaterQueries = new ArrayList<String>();
	StringBuilder sb = new StringBuilder();	
	sb.append("insert into tms_utf8_db.EQUIPMENT_TRANSLATOR values (");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'50FT','50FT','N','N','N',null,null,'95 cbm',2.74,2.43,10000,'KG',10000,'CBM',105,null,'Y');");
	equipmentTranslaterQueries.add(sb.toString());

	sb = new StringBuilder();
	sb.append("insert into tms_utf8_db.EQUIPMENT_TRANSLATOR values (");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'7MT','7MT','Y','Y','N',null,null,'7 MT',null,null,null,null,null,null,null,null,'N');");
	equipmentTranslaterQueries.add(sb.toString());
	
	sb = new StringBuilder();
	sb.append("insert into tms_utf8_db.EQUIPMENT_TRANSLATOR values (");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'9MT','9MT','Y','N','N',null,null,'9 MT',null,null,null,null,null,null,null,null,'N');");
	equipmentTranslaterQueries.add(sb.toString());

	sb = new StringBuilder();
	sb.append("insert into tms_utf8_db.EQUIPMENT_TRANSLATOR values (");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'CANTER','CANTER','Y','N','N',null,null,'Canter',null,null,null,null,null,null,null,null,'N');");
	equipmentTranslaterQueries.add(sb.toString());

	return equipmentTranslaterQueries;
    }

    private static List<String> createGuiDefaultValueQuery(String parentPartnerCode) {
	List<String> guiDefaultValueQueries = new ArrayList<String>();

	StringBuilder sb = new StringBuilder();
	sb.append(
		"insert into tms_utf8_db.GUI_DEFAULT_VALUE(MODULE_NAME,PARENT_PARTNER_CD,PARTNER_CD,LABEL,VALUE,DEFAULT_FLG) values ('TRANSPORT_MODE',");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'*','FTL - Full Truckload','FTL','N');");
	guiDefaultValueQueries.add(sb.toString());

	sb = new StringBuilder();
	sb.append(
		"insert into tms_utf8_db.GUI_DEFAULT_VALUE(MODULE_NAME,PARENT_PARTNER_CD,PARTNER_CD,LABEL,VALUE,DEFAULT_FLG) values ('TRANSPORT_MODE',");
	sb.append("'").append(parentPartnerCode.trim()).append("',");
	sb.append("'*','LTL - Less than Truckload','LTL','Y');");
	guiDefaultValueQueries.add(sb.toString());
	return guiDefaultValueQueries;
    }

    private static List<String> createPackageUOMQuery(String parentPartnerCode) {
	List<String> packageUOMQueries = new ArrayList<String>();
	List<String> UOMCD = new ArrayList<String>(Arrays
		.asList(new String[] { "SCK", "PLT", "PCS", "CTN", "BGS", "DRM", "CRT", "PKG", "BOX", "UNI", "MXP" }));
	StringBuilder sb;
	for (String code : UOMCD) {
	    sb = new StringBuilder(
		    "insert into tms_utf8_db.PACKAGE_UOM(PARENT_PARTNER_CD,PARTNER_CD,UOM_CD,DIVISION_CD,UOM_DESCRIPTION_TXT,CATEGORY_CD,INTERNAL_STANDARD_FLG) values (");
	    sb.append("'").append(parentPartnerCode).append("',");
	    sb.append("'*',");
	    sb.append("'").append(code.trim()).append("'");
	    sb.append(",'*','Sacks','EDI','Y');");
	    packageUOMQueries.add(sb.toString());
	}
	return packageUOMQueries;
    }

    private static String createPasswordProfileQuery(String parentPartnerCode, String partnerCode) {
	StringBuilder passwordProfileQuery = new StringBuilder();
	passwordProfileQuery.append("insert into tms_utf8_db.password_profile values (");
	passwordProfileQuery.append("'").append(parentPartnerCode.trim()).append("',");
	passwordProfileQuery.append("'").append(partnerCode.trim()).append("',");
	passwordProfileQuery.append("10000,15,6,10,3,365,'Y','N','Y','N','Y','3','Y','Y');");

	return passwordProfileQuery.toString();
    }

    private static String createUserAccountQuery(String userId, String partnerCode) {
	StringBuilder userAccountQuery = new StringBuilder();
	userAccountQuery.append("insert into tms_utf8_db.USER_ACCOUNT(USER_ID,partner_cd,create_tstamp,create_user_cd) values (");
	userAccountQuery.append("'").append(userId.trim()).append("',");
	userAccountQuery.append("'").append(partnerCode.trim()).append("',");
	userAccountQuery.append("now(),");
	userAccountQuery.append("'lecelq');");
	return userAccountQuery.toString();
    }

    private static List<String> createUserRoleQuery(List<String> userRoles, String customerCode) {
	List<String> userRoleQueries = new ArrayList<String>();
	StringBuilder userRoleQuery;
	for (String userRole : userRoles) {
	    userRoleQuery = new StringBuilder();
	    userRoleQuery.append("insert into tms_utf8_db.USER_ROLE(USER_ID,ROLE,CREATE_USER_ID,CREATE_TSTAMP) values (");
	    userRoleQuery.append("'").append(customerCode.trim()).append("',");
	    userRoleQuery.append("'").append(userRole.trim()).append("',");
	    userRoleQuery.append("'lecelq',");
	    userRoleQuery.append("now());");
	    userRoleQueries.add(userRoleQuery.toString());
	}

	return userRoleQueries;
    }

    private static List<String> createRoleQuery(List<String> roles, String partner_cd) {
	List<String> roleQueries = new ArrayList<String>();

	StringBuilder roleQuery;

	for (String role : roles) {
	    roleQuery = new StringBuilder();
	    roleQuery.append("insert into tms_utf8_db.role(role,user_id,description) values ('");
	    roleQuery.append(role);
	    roleQuery.append("',");
	    roleQuery.append("(select id from tms_utf8_db.user_info where partner_cd in ('").append(partner_cd.trim())
		    .append("'))");
	    roleQuery.append(",null);");

	    roleQueries.add(roleQuery.toString());
	}
	return roleQueries;
    }

    private static int queryUserIdBasedOnPartnerCode(Object PARTNER_CD, String tableName) throws SQLException {

	StringBuilder sql = new StringBuilder();
	sql.append("select * from ");
	sql.append(tableName);
	sql.append(" where partner_cd in ('");
	sql.append(PARTNER_CD);
	sql.append("');");

	System.out.println("QUERY: " + sql.toString());

	try (Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
		PreparedStatement ps = connection.prepareStatement(sql.toString())) {

	    ResultSet rs = ps.executeQuery();
	    while (rs.next()) {
		System.out.println("ID IS: " + rs.getInt("id"));
		System.out.println("ID IN INT: " + rs.getInt(1));
		return rs.getInt("id");
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw e;
	}
	return 0;
    }

    private static String createQuery(Map<String, Object> jsObject, String tableName) {

	StringBuilder query = new StringBuilder();
	query.append("insert into ");
	query.append(tableName);
	query.append("(");

	byte count = 0;
	for (String key : jsObject.keySet()) {
	    if (count > 0) {
		query.append(",");
	    }
	    query.append(key);
	    count++;
	}
	query.append(") values (");
	count = 0;
	for (Object value : jsObject.values()) {
	    if (count > 0) {
		query.append(",");
	    }
	    if (value instanceof String) {
		if (((String) value).contains("now()")) {
		    query.append(value);
		} else if (value.toString().isEmpty()) {
		    query.append("'");
		    query.append("'");

		} else {
		    query.append("'");
		    value = value.toString().replace("'", "\\'");
		    query.append(value.toString().trim());
		    query.append("'");
		}
	    } else if (value instanceof Integer) {
		query.append(Integer.parseInt(value.toString()));
	    } else {
		query.append("null");
	    }
	    count++;
	}

	query.append(");");
	return query.toString();
    }
    
    public void addAnalytics(String partnerCode) throws SQLException {

    	String query = "INSERT INTO analytics_db.analytics_menu (partner_cd, bookmark_id, menu_label_txt, create_user, create_dt, url_address, TYPE, report_id, report_type_cd) "
    			+ "VALUES(?, NULL, 'Performance Dashboards', 'lecelq', NOW(), NULL, NULL, "
    			+ "(select ID from analytics_db.dashboard_details where Customer_Cd = 'Users'), 'PBI');";

    	try (Connection connection = DbConfigAnalytics.getDataSource().getConnection();
    		PreparedStatement ps = connection.prepareStatement(query)) {
    		ps.setString(1, partnerCode);
    	    ps.execute();
    	} catch (SQLException e) {
    		log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
    	}
    }
    
    public void removeAnalytics(String partnerCode) throws SQLException {

    	String checkSql = "SELECT analytics_menu_id FROM analytics_db.analytics_menu WHERE partner_cd = ? "
    			+ " AND menu_label_txt = 'Performance Dashboards'"
    			+ " AND create_user = 'lecelq'"
    			+ " AND report_id = (select ID from analytics_db.dashboard_details where Customer_Cd = 'Users')"
    			+ " AND report_type_cd = 'PBI' LIMIT 1";
    	
    	
		String deleteSql = "DELETE FROM analytics_db.analytics_menu WHERE partner_cd = ? "
    			+ " AND menu_label_txt = 'Performance Dashboards'"
    			+ " AND create_user = 'lecelq'"
    			+ " AND report_id = (select ID from analytics_db.dashboard_details where Customer_Cd = 'Users')"
    			+ " AND report_type_cd = 'PBI' LIMIT 1";
		
		try {
			java.sql.Connection connection = DbConfigAnalytics.getDataSource().getConnection();
			PreparedStatement checkPstmt = connection.prepareStatement(checkSql);
			PreparedStatement deletePstmt = connection.prepareStatement(deleteSql);
			
			checkPstmt.setString(1, partnerCode);

	        try (ResultSet checkRs = checkPstmt.executeQuery()) {
				if(checkRs.next()) {
					connection.setAutoCommit(false);
					deletePstmt.setString(1, partnerCode);
					deletePstmt.execute();
					connection.commit();
		        }
	         }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
    }
    
    public void setUploader(String parentPartnerCode, String partnerCode) {
    	java.sql.Connection connection = null;
    	
		try {
			connection = DbConfigTmsUtf8.getDataSource().getConnection();
			CallableStatement pstmt = connection.prepareCall("call sp_tms_uploader(?,?)");
			
			connection.setAutoCommit(false);
			pstmt.setString(1, parentPartnerCode);
			pstmt.setString(2, partnerCode);
			pstmt.executeUpdate();
			
			connection.commit();
		} catch (Exception e) {
			if (connection != null) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
					log.error("Error message: " + e, e);
					new ServiceException(Response.Status.CONFLICT);
				}
			}
			
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
    }
    
    public long setShipper(String partnerCode, String companyName) throws SQLException {
    	long key = 0;
    	String query = "insert into tms_delivery_db.shipper(shipper_cd, shipper_name, create_dt, update_dt, logo_path) values(?, ?, NOW(), NOW(), '');";

    	try (Connection connection = DbConfigTmsDelivery.getDataSource().getConnection();
    		PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
    		ps.setString(1, partnerCode);
    		ps.setString(2, companyName);
    	    ps.executeUpdate();
    	    
    	    ResultSet rs = ps.getGeneratedKeys();
    	    if (rs != null && rs.next()) {
    	        key = rs.getLong(1);
    	    }
    	} catch (SQLException e) {
    		log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
    	}
    	
		return key;
    }
    
    public void setShipperDashboard(long key) throws SQLException {
    	
    	String shipperQuery = "select shipper_id, shipper_cd from tms_delivery_db.shipper where shipper_id = ?";
    	String shipperDashboardQuery = "insert into analytics_db.shipper_dashboard(shipper_id, shipper_cd, createdby, createdt) values(?,?,'lecelq', now())";
    	
    	long shipperId = 0;
    	String shipperCode = null;
    	
    	try {
    		java.sql.Connection deliveryConnection = DbConfigTmsDelivery.getDataSource().getConnection();
			java.sql.Connection analyticsConnection = DbConfigAnalytics.getDataSource().getConnection();
			
			PreparedStatement deliveryPstmt = deliveryConnection.prepareStatement(shipperQuery);
			deliveryPstmt.setLong(1, key);
			
			try (ResultSet rs = deliveryPstmt.executeQuery()) {
				if (rs.next()) {
					shipperId = rs.getInt("shipper_id");
					shipperCode = rs.getString("shipper_cd");
		        }
	        }
			
			if (shipperId != 0 && shipperCode != null) {
				PreparedStatement analyticsPstmt = analyticsConnection.prepareStatement(shipperDashboardQuery);
				analyticsPstmt.setLong(1, shipperId);
				analyticsPstmt.setString(2, shipperCode);
				
				analyticsPstmt.execute();
			}
			
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
    }
    
    public List<Map<String,Object>> getDataLoaderOptions(String companyCd) throws SQLException {
    	List<Map<String,Object>> dataLoaderOptions = new LinkedList<Map<String,Object>>();
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT PARAMETER_1_VALUE as fileFormat, PARAMETER_1_DESC_TXT as descriptionTxt ")
		.append("FROM tms_utf8_db.tms_parameter WHERE PROGRAM_ID = 'DATA_UPLOAD' AND PARAMETER_CD = 'UPLOAD' AND  PARAMETER_3_VALUE = ? ")
		.append("AND ? in ( PARAMETER_4_DESC_TXT) GROUP BY PARAMETER_1_VALUE, PARAMETER_1_DESC_TXT");
		
    	try (
			java.sql.Connection conn = DbConfigTmsUtf8.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
    		p.setString(1, companyCd);
    		p.setString(2, companyCd);
    		ResultSet rs = p.executeQuery();
    		
    		ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			while(rs.next()){
				Map<String,Object> rowData = new LinkedHashMap<String,Object>();
				for (int i = 1; i <= columnCount; i++ ) {
				  String columnLabel = rsmd.getColumnLabel(i);
				  rowData.put(columnLabel, rs.getObject(columnLabel));
				}
				dataLoaderOptions.add(rowData);
			}
			
    	}catch (Exception e) {
    		log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
    	}
    	
    	return dataLoaderOptions;
    }

	private static PreparedStatement createStatement(PreparedStatement ps, Object... args) throws Exception {
		int index = 0;
		for(Object o : args)
			ps.setObject(++index, o);
		return ps;
	}

	public static long getLong(String paramCd, String paramName, int valueNum, long defaultValue) {
		return NumberUtils.toInt(getParamValue(paramCd, paramName, valueNum, String.valueOf(defaultValue)));
	}

	public static long getLong(String paramCd, String paramName, long defaultValue) {
		return getLong(paramCd, paramName,1, defaultValue);
	}

	public static String getString(String paramCd, String paramName, int valueNum, String defaultValue) {
		return getParamValue(paramCd, paramName, valueNum, defaultValue);
	}

	public static String getString(String paramCd, String paramName, String defaultValue) {
		return getString(paramCd, paramName, 1, defaultValue);
	}

	private static String getParamValue(String paramCd, String paramName, int valueNum, String defaultValue) {
		try(Connection conn = DbConfigTmsUtf8.getDataSource().getConnection();
			PreparedStatement ps = createStatement(conn.prepareStatement(String.format(SQL_PARAMTER_QUERY, valueNum)), paramCd, paramName);
			ResultSet rs = ps.executeQuery()) {
			if(rs.next()) {
				return rs.getString(1);
			}
			log.info(String.format("No parameter found for %s.%s. Using default value %d", paramCd, paramName, defaultValue));
			return defaultValue;
		}catch (Exception ex) {
			log.warn(String.format("Unable to get TMS parameter %s.%s. Using default value %s", paramCd, paramName, defaultValue), ex);
			return defaultValue;
		}
	}
}
