package com.openport.marketplace.repository;


import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.EntityAddress;
import com.openport.marketplace.json.PoLineItem;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.Shipment;
import com.openport.marketplace.json.ShipmentContact;
import com.openport.marketplace.json.ShipmentDetail;
import com.openport.marketplace.model.PoHeader;
import com.openport.util.db.ResultSetMapper;


public class TenderShipmentRepository {
    private static final Logger log = Logger.getLogger(TenderShipmentRepository.class);

    private final String DELIVERY = "delivery";
    private final String PICKUP = "pickup";
    private final String LPO_TYPE_CODE = "LPO";
    private final String DPO_TYPE_CODE = "DPO";
    
    public Shipment getShipmentByReleaseNumber(String releaseNumber, String accountCode) {
        // Get the shipment details - not yet posted to marketplace

        log.info("Getting shipment data to edit: " + releaseNumber);
        
        try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
                CallableStatement pstmt = connection.prepareCall("call sp_marketplace_shipment_update_get(?)");) {
            pstmt.setString(1, releaseNumber);

            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    Shipment shipment = new Shipment();

                    shipment.setShipmentId(rs.getInt("shipment_id")); // set to 0. record could not be found in shipment
                    shipment.setStatusDescription(rs.getString("status"));
                    shipment.setStatusId(rs.getInt("STATUS_ID")); // status id from marketplace_db.shipment
                    shipment.setSent204Flag(rs.getBoolean("SENT_204_FLG"));
                    shipment.setBrFlag(rs.getBoolean("BR_FLG"));
                    shipment.setActualDeliveryDate(rs.getTimestamp("ACTUAL_DELIVERY_DT"));
                    
                    shipment.setEquipmentTypeCd(rs.getString("EQUIPMENT_TYPE_CD"));
                    shipment.setEquipmentTypeId(rs.getInt("equipment_type_id"));
                    shipment.setRequestedEquipmentTypeId(rs.getInt("equipment_type_id"));

                    shipment.setPurchaseOrderNumber(rs.getString("PO_NBR"));
                    shipment.setReleaseNumber(rs.getString("RELEASE_NBR"));
                    shipment.setShipmentNbr(rs.getString("RELEASE_NBR"));
                    shipment.setUpdateDt(rs.getTimestamp("UPDATE_TSTAMP"));
                    shipment.setShptType(rs.getString("PO_TYPE_CD"));

                    shipment.setRequestedPickupDt(rs.getTimestamp("requested_pickup_dt"));
                    shipment.setRequestedDeliveryDt(rs.getTimestamp("requested_delivery_dt"));

                    shipment.setProBillNbr(rs.getString("PRO_BILL_NBR"));
                    shipment.setCommodityDescTxt(rs.getString("commodity_desc"));

                    shipment.setBidRank(0);
                    shipment.setRateCount(0);

                    ShipmentContact pickupShipmentContact = new ShipmentContact();
                    pickupShipmentContact.setContactTypeCd('P');
                    pickupShipmentContact.setContactName(rs.getString("pickup_contact_person"));
                    pickupShipmentContact.setContactEmailAddress(rs.getString("pickup_contact_email"));
                    pickupShipmentContact.setContactPhoneNbr(rs.getString("pickup_contact_telephone"));

                    ShipmentContact deliverShipmentContact = new ShipmentContact();
                    deliverShipmentContact.setContactTypeCd('D');
                    deliverShipmentContact.setContactName(rs.getString("deliver_contact_person"));
                    deliverShipmentContact.setContactEmailAddress(rs.getString("deliver_contact_email"));
                    deliverShipmentContact.setContactPhoneNbr(rs.getString("deliver_contact_telephone"));

                    List<ShipmentContact> shipmentContacts = new ArrayList<ShipmentContact>();
                    shipmentContacts.add(pickupShipmentContact);
                    shipmentContacts.add(deliverShipmentContact);

                    EntityAddress pickupAddress = new EntityAddress();
                    pickupAddress.setCityLocality(rs.getString("pickup_city"));
                    pickupAddress.setCountry(rs.getString("pickup_country_cd"));
                    pickupAddress.setEntityName(rs.getString("pickup_location"));
                    pickupAddress.setLine1(rs.getString("pickup_address"));
                    pickupAddress.setPostalCode(rs.getString("pickup_postal"));
                    pickupAddress.setProvince(rs.getString("pickup_state_province"));

                    EntityAddress deliveryAddress = new EntityAddress();
                    deliveryAddress.setCityLocality(rs.getString("deliver_city"));
                    deliveryAddress.setCountry(rs.getString("deliver_country_cd"));
                    deliveryAddress.setEntityName(rs.getString("deliver_location"));
                    deliveryAddress.setLine1(rs.getString("deliver_address"));
                    deliveryAddress.setPostalCode(rs.getString("deliver_postal"));
                    deliveryAddress.setProvince(rs.getString("deliver_state_province"));

                    shipment.setPickupAddress(pickupAddress);
                    shipment.setDeliveryAddress(deliveryAddress);
                    shipment.setShipmentDetails(this.getShipmenLineItem(releaseNumber, accountCode));
                    shipment.setShipmentContacts(shipmentContacts);

                    return shipment;
                }
            }
        } catch (SQLTimeoutException e) {
            log.error(e, e);
            new ServiceException(Response.Status.GATEWAY_TIMEOUT);
        } catch (SQLFeatureNotSupportedException e) {
            log.error(e, e);
            new ServiceException(Response.Status.BAD_GATEWAY);
        } catch (SQLException e) {
            log.error(e, e);
            new ServiceException(Response.Status.BAD_GATEWAY);
        } catch (Exception e) {
            log.error(e, e);
            new ServiceException(Response.Status.BAD_REQUEST);
        }

        return null;
    }

    private List<ShipmentDetail> getShipmenLineItem(String releaseNumber, String accountCode)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Getting shipment details for shipment: " + releaseNumber);
        
        String sql = "SELECT li.SHIPPED_QTY, li.QTY_UOM, li.TOTAL_WT, li.WT_UOM, li.TOTAL_VOL, li.VOL_UOM, li.HAZARDOUS_FLG, li.PRODUCT_DESC_TXT, li.PRODUCT_CD, " + 
        		"COALESCE(li.ACTUAL_PO_NBR, li.PO_NBR) AS ACTUAL_PO_NBR, COALESCE(li.ACTUAL_PO_RELEASE_NBR, li.RELEASE_NBR) AS ACTUAL_PO_RELEASE_NBR " + 
        		"li.UNIT_PRICE, li.goods_value_bill_to_cd " +
        		"FROM tms_utf8_db.po_line_item li WHERE RELEASE_NBR IN (SELECT COALESCE(ACTUAL_PO_RELEASE_NBR, RELEASE_NBR) FROM tms_utf8_db.po_line_item WHERE RELEASE_NBR = ? " +
        		"AND ? IN (PARENT_PARTNER_CD, PARTNER_CD, CUSTOMER_CD, SHIPPER_CD)) AND ? IN (li.PARENT_PARTNER_CD, li.PARTNER_CD, li.CUSTOMER_CD, li.SHIPPER_CD)" +
        		"ORDER BY li.PO_LINE_ITEM_NBR asc";
        
        try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            pstmt.setString(1, releaseNumber);
            pstmt.setString(2, accountCode);
            pstmt.setString(3, accountCode);

            List<ShipmentDetail> shipmentDetails = new ArrayList<ShipmentDetail>();
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    ShipmentDetail shipmentDetail = new ShipmentDetail();
                    shipmentDetail.setQty(rs.getDouble("SHIPPED_QTY"));
                    shipmentDetail.setQtyUom(rs.getString("QTY_UOM"));
                    shipmentDetail.setShipmentWt(rs.getDouble("TOTAL_WT"));
                    shipmentDetail.setWtUom(rs.getString("WT_UOM"));
                    shipmentDetail.setShipmentVol(rs.getDouble("TOTAL_VOL"));
                    shipmentDetail.setVolUom(rs.getString("VOL_UOM"));
                    shipmentDetail.setIsHazardous(rs.getBoolean("HAZARDOUS_FLG"));
                    shipmentDetail.setProductDescTxt(rs.getString("PRODUCT_DESC_TXT"));
                    shipmentDetail.setProductCode(rs.getString("PRODUCT_CD"));
                    shipmentDetail.setActualPoNumber(rs.getString("ACTUAL_PO_NBR"));
                    shipmentDetail.setActualPoReleaseNumber(rs.getString("ACTUAL_PO_RELEASE_NBR"));
                    shipmentDetail.setUnitPrice(rs.getDouble("UNIT_PRICE"));
                    shipmentDetail.setGoodsValueBillToCode(rs.getString("goods_value_bill_to_cd"));

                    shipmentDetails.add(shipmentDetail);
                }

                return shipmentDetails;
            }
        } catch (SQLTimeoutException e) {
            throw new SQLTimeoutException(e);
        } catch (SQLFeatureNotSupportedException e) {
            throw new SQLFeatureNotSupportedException(e);
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
    
    public boolean updateShipmentByReleaseNumber(Shipment shipment, 
    		boolean isShipFromChanged, 
    		boolean isShipToChanged, 
    		boolean isEquipmentChanged, 
    		boolean isShipmentDetialChanged,
    		boolean isPickupDateChanged,
    		boolean isDeliveryDateChanged,
    		String accountCode) {
        // update shipment that are not yet posted to marketplace
        String releaseNumber = shipment.getShipmentNbr();
        
        log.info("Updating shipment: " + releaseNumber);
        
        List<ShipmentContact> shipmentContacts = shipment.getShipmentContacts();
        ShipmentContact pickupShipmentContact = new ShipmentContact();
        ShipmentContact deliverShipmentContact = new ShipmentContact();
        for (ShipmentContact contact : shipmentContacts) {
            if (contact.getContactTypeCd() == 'P' && isShipFromChanged) {
                pickupShipmentContact.setContactTypeCd(contact.getContactTypeCd());
                pickupShipmentContact.setContactName(contact.getContactName());
                pickupShipmentContact.setContactEmailAddress(contact.getContactEmailAddress());
                pickupShipmentContact.setContactPhoneNbr(contact.getContactPhoneNbr());
            } else if (contact.getContactTypeCd() == 'D' && isShipToChanged) {
                deliverShipmentContact.setContactTypeCd(contact.getContactTypeCd());
                deliverShipmentContact.setContactName(contact.getContactName());
                deliverShipmentContact.setContactEmailAddress(contact.getContactEmailAddress());
                deliverShipmentContact.setContactPhoneNbr(contact.getContactPhoneNbr());
            }
        }
        
        try {
            // get the release numbers
            Set<String> shipmentActualPoReleaseNumbers = new HashSet<String>();
            Set<String> shipmentToUpdateDestination = new HashSet<String>();
            
            List<ShipmentDetail> shipmentDetails = shipment.getShipmentDetails();
            for(ShipmentDetail detail : shipmentDetails) {
                log.info("Shipment Detail: " + detail.toString());
                
                shipmentActualPoReleaseNumbers.add(detail.getActualPoReleaseNumber());
                
                if(detail.isAddressUpdated()) {
                	shipmentToUpdateDestination.add(detail.getActualPoReleaseNumber());
                }
            }
            
            shipmentActualPoReleaseNumbers.add(releaseNumber);
            shipmentToUpdateDestination.add(releaseNumber);
            
        	if(isShipFromChanged) {
        		this.updateEntityAddressAndContact(shipment.getPickupAddress(), pickupShipmentContact, PICKUP, new ArrayList<String>(shipmentActualPoReleaseNumbers));
        	} else if(isShipToChanged) {
        		this.updateEntityAddressAndContact(shipment.getDeliveryAddress(), deliverShipmentContact, DELIVERY, new ArrayList<String>(shipmentToUpdateDestination));
        	}
            
            // get the current po line items
            if(isShipmentDetialChanged) {
	            List<PoLineItem> poLineItems = this.getPoLineItemsByReleaseNumber(new ArrayList<String>(shipmentActualPoReleaseNumbers), accountCode);
	            List<PoLineItem> vpoLineItems = new ArrayList<PoLineItem>();
	            List<PoLineItem> lpoDpoLineItems = new ArrayList<PoLineItem>();
	            
	            Set<String> vpoActualReleaseNumbers = new HashSet<String>();
	            Set<String> vpoDifferenceActualReleaseNumbers = new HashSet<String>(shipmentActualPoReleaseNumbers);
	            
	            for(PoLineItem lineItem : poLineItems) {
	                log.info("PO Line Item: " + lineItem.toString());
	                
	                if(lineItem.getActualPoReleaseNumber() != null 
	                        && !lineItem.getActualPoReleaseNumber().equalsIgnoreCase(lineItem.getReleaseNumber())
	                        && lineItem.getReleaseNumber().equalsIgnoreCase(releaseNumber)) {
	                    
	                    vpoLineItems.add(lineItem);
	                    vpoActualReleaseNumbers.add(lineItem.getActualPoReleaseNumber());
	                } else {
	                    lpoDpoLineItems.add(lineItem);
	                }
	            }
	            
	            // add LPO
	            if(!vpoLineItems.isEmpty() && vpoLineItems.size() < lpoDpoLineItems.size()) {
	                // check if new lpo(release number) is added to vpo
	                
	                vpoDifferenceActualReleaseNumbers.removeAll(vpoActualReleaseNumbers);
	                vpoDifferenceActualReleaseNumbers.remove(releaseNumber);
	                
	                List<PoLineItem> clonedLineItems  = new ArrayList<PoLineItem>();
	                for(PoLineItem item : poLineItems) {
	                	clonedLineItems.add(new PoLineItem(item));
	                }
	                
		            for(String release : vpoDifferenceActualReleaseNumbers) {
		            	vpoLineItems.addAll(clonedLineItems
	            			.stream()
	            			.filter(li -> li.getReleaseNumber().equalsIgnoreCase(release))
	            			.map(li -> {
	            				String actualReleaseNumber = li.getReleaseNumber() != null ? li.getReleaseNumber() : li.getActualPoReleaseNumber();
	            				String actualPoNumber = li.getPoNumber() != null ? li.getPoNumber() : li.getActualPoNumber();
	            				
	            				li.setReleaseNumber(releaseNumber);
	                            li.setPoNumber(releaseNumber);
	                            li.setActualPoReleaseNumber(actualReleaseNumber);
	                            li.setActualPoNumber(actualPoNumber);
	                            
	                            return li;
	            			})
	            			.collect(Collectors.toList()));
		            }
	                
	                // Set LPO
	                if(vpoDifferenceActualReleaseNumbers.size() > 0) {
	                	this.updatePoHeader(new ArrayList<String>(vpoDifferenceActualReleaseNumbers), LPO_TYPE_CODE);
	                }
	            }
	            
	            if(!lpoDpoLineItems.isEmpty()) {
	            	int liSize = lpoDpoLineItems.size();
        			int detailSize = shipmentDetails.size();
        			int diffSize = detailSize - liSize;
        			
        			while(diffSize > 0 && diffSize != 0) {
        				PoLineItem tmpLi = new PoLineItem(lpoDpoLineItems.get(0));
        				lpoDpoLineItems.add(tmpLi);
        				
        				diffSize--;
        			}
	            }
	            
	            // process
	            List<PoLineItem> mergedLineItems = new ArrayList<PoLineItem>();
	            Set<String> vpoReleaseNumberSet = new HashSet<String>();
	            
	            if(!lpoDpoLineItems.isEmpty()) {
	            	mergedLineItems.addAll(this.prepareLineItem(shipmentDetails, lpoDpoLineItems, releaseNumber));
	            	
	            	// If LPO - prepare to update its VPO
	            	if(shipment.getShptType().equalsIgnoreCase(LPO_TYPE_CODE)) {
	            		// The VPO
	            		mergedLineItems.addAll(this.prepareVpoLineItem(mergedLineItems, releaseNumber, accountCode));
	            		
	            		for(PoLineItem li : mergedLineItems) {
	            			if(li.getReleaseNumber() != null && !li.getReleaseNumber().equalsIgnoreCase(releaseNumber)) {
	            				vpoReleaseNumberSet.add(li.getReleaseNumber());
	            			}
	            		}
	            		vpoReleaseNumberSet.add(releaseNumber);
	            	}
	            }
	            if(!vpoLineItems.isEmpty()) {
	            	mergedLineItems.addAll(this.prepareLineItem(shipmentDetails, vpoLineItems, releaseNumber));
	            }
	            
	            log.info(mergedLineItems);
	            
	            if(shipment.getShptType().equalsIgnoreCase(LPO_TYPE_CODE)) {
    	            this.deletePoLineItem(new ArrayList<String>(vpoReleaseNumberSet));
            	} else {
            		this.deletePoLineItem(new ArrayList<String>(shipmentActualPoReleaseNumbers));
            	}
	            
	            this.addPoLineItem(mergedLineItems);
	            
	            // Set DPO
	            Set<String> dpoActualReleaseNumbers = new HashSet<String>(vpoActualReleaseNumbers);
	            dpoActualReleaseNumbers.removeAll(shipmentActualPoReleaseNumbers);
	            if(dpoActualReleaseNumbers.size() > 0) {
	            	this.updatePoHeader(new ArrayList<String>(dpoActualReleaseNumbers), DPO_TYPE_CODE);
	            }
            }
            
            if(isShipmentDetialChanged || isEquipmentChanged || isPickupDateChanged || isDeliveryDateChanged) {
            	List<PoHeader> header = this.preparePoHeader(shipment, new ArrayList<String>(shipmentActualPoReleaseNumbers));
                this.updatePoHeader(header);
                
                // shipment is not yet in marketplace_db
                if(shipment.getShipmentId() == 0) {
                	this.updateEquipment(releaseNumber, shipment.getEquipmentTypeCd(), shipment.getEquipmentTypeId());
                }
            }

            
            return true;
            
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
        }

        return false;
    }
    
    private List<PoHeader> preparePoHeader(Shipment shipment, List<String> releaseNumbers) 
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Preparing PO Header: " + releaseNumbers.toString());
        
        double totalVpoQuantity = 0;
        double totalVpoVolume = 0;
        double totalVpoWeight = 0;
        
        List<PoHeader> poHeaders = new ArrayList<PoHeader>();
        for(String release : releaseNumbers) {
            PoHeader poHeader = new PoHeader();
            
            double totalQuantity = 0;
            double totalVolume = 0;
            double totalWeight = 0;
            
            String quantityUom = "";
            String volumeUom = "";
            String weightUom = "";
            
            Set<String> quantityUomSet = new LinkedHashSet<String>();
            Set<String> volumeUomSet = new LinkedHashSet<String>();
            Set<String> weightUomSet = new LinkedHashSet<String>();
            
            List<ShipmentDetail> shipmentDetails = shipment.getShipmentDetails();
            for(ShipmentDetail detail : shipmentDetails) {
                if(detail.getActualPoReleaseNumber().equalsIgnoreCase(release)) {
                    totalQuantity += detail.getQty();
                    totalVolume += detail.getShipmentVol();
                    totalWeight += detail.getShipmentWt();
                    
                    quantityUomSet.add(detail.getQtyUom());
                    volumeUomSet.add(detail.getVolUom());
                    weightUomSet.add(detail.getWtUom());
                }
            }
            
            totalVpoQuantity += totalQuantity;
            totalVpoVolume += totalVolume;
            totalVpoWeight += totalWeight;
            
            if(quantityUomSet.size() == 1) {
                for(Object object : quantityUomSet) {
                    quantityUom = (String) object;
                }
            } else {
                quantityUom = "MXP";
            }
            
            for(Object object : volumeUomSet) {
                volumeUom = (String) object;
            }
            
            for(Object object : weightUomSet) {
                weightUom = (String) object;
            }
            
            poHeader.setRequestedDeliveryDate(shipment.getRequestedDeliveryDt());
            poHeader.setAvailableDate(shipment.getRequestedPickupDt());
            poHeader.setEquipmentTypeCode(shipment.getEquipmentTypeCd());
            poHeader.setReleaseNumber(shipment.getShipmentNbr());
            poHeader.setActualPoReleaseNumber(release);
            poHeader.setUpdateUserId(shipment.getCreateUserId());
            poHeader.setUpdateTimestamp(new Timestamp(new Date().getTime()));
            
            if(release.equalsIgnoreCase(shipment.getShipmentNbr())) {
                // VPO
                poHeader.setTotalQuantity(totalVpoQuantity);
                poHeader.setTotalVolume(totalVpoVolume);
                poHeader.setTotalWeight(totalVpoWeight);
                
            } else {
                // DPO or LPO
                poHeader.setTotalQuantity(totalQuantity);
                poHeader.setTotalVolume(totalVolume);
                poHeader.setTotalWeight(totalWeight);
            }
            
            poHeader.setQuantityUom(quantityUom);
            poHeader.setVolumeUom(volumeUom);
            poHeader.setWeightUom(weightUom);
            
            log.info(poHeader.toString());

            poHeaders.add(poHeader);
        }
        
        return poHeaders;
    }
    
    private void updatePoHeader(List<PoHeader> poHeaders) 
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Updating PO Header");
        
        String sql = "update tms_utf8_db.po_header a set "
                + "a.AVAILABLE_DT = ?, "
                + "a.REQUESTED_DELIVERY_DT = ?, "
                + "a.EQUIPMENT_TYPE_CD = ?, "
                + "a.UPDATE_TSTAMP = ?, "
                + "a.UPDATE_USER_ID = ?, "
                + "a.TOTAL_QTY = ?, "
                + "a.TOTAL_VOL = ?, "
                + "a.TOTAL_WT = ?, "
                + "a.QTY_UOM = ?, "
                + "a.VOL_UOM = ?, "
                + "a.WT_UOM = ? "
                + "where a.RELEASE_NBR = ?";
        
        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            
            for(PoHeader poHeader : poHeaders) {
                log.info("Updating PO Header: " + poHeader.toString());
                
                pstmt.setTimestamp(1, new Timestamp(poHeader.getAvailableDate().getTime()));
                pstmt.setTimestamp(2, new Timestamp(poHeader.getRequestedDeliveryDate().getTime()));
                pstmt.setString(3, poHeader.getEquipmentTypeCode());
                pstmt.setTimestamp(4, new Timestamp(new Date().getTime()));
                pstmt.setString(5, poHeader.getUpdateUserId());
                pstmt.setDouble(6, poHeader.getTotalQuantity());
                pstmt.setDouble(7, poHeader.getTotalVolume());
                pstmt.setDouble(8, poHeader.getTotalWeight());
                pstmt.setString(9, poHeader.getQuantityUom());
                pstmt.setString(10, poHeader.getVolumeUom());
                pstmt.setString(11, poHeader.getWeightUom());
                pstmt.setString(12, poHeader.getActualPoReleaseNumber());
                
                pstmt.addBatch();
            }
            
            pstmt.executeBatch();
            
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }
        
    }
    
    private void updatePoHeader(List<String> releaseNumbers, String typeCode) 
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Updating PO Header PO Type code");
        
        String sql = "update tms_utf8_db.po_header a set a.PO_TYPE_CD = ? where a.RELEASE_NBR = ?";
        
        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            
            for(String release : releaseNumbers) {
                log.info("Updating PO Header PO Type code: " + release + " : " + typeCode);
                
                pstmt.setString(1, typeCode);
                pstmt.setString(2, release);
                
                pstmt.addBatch();
            }
            
            pstmt.executeBatch();
            
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }
        
    }
    
    
    private void updateEntityAddressAndContact(EntityAddress entityAddress, ShipmentContact shipmentContact,
            String type, List<String> releaseNumbers) 
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
    	
        log.info("Updating address for shipment: " + releaseNumbers.toString() + ", type: " + type);

        String sql = "UPDATE tms_utf8_db.po_reference_info SET " + " LOCATION_NAME = ?," + " LINE_1_ADDR = ?,"
                + " CITY_NAME = ?," + " STATE_NAME = ?," + " POSTAL_CD = ?," + " COUNTRY_CD = ?,"
                + " CONTACT_PERSON_NAME = ?," + " EMAIL_ADDR = ?," + " TELEPHONE_NBR = ?"
                + " WHERE RELEASE_NBR = ?";
        
        if (type.equalsIgnoreCase(PICKUP)) {
            sql = sql.concat(" AND REFERENCE_TYPE_CD IN ('pickup', 'shipper')");
        } else if(type.equalsIgnoreCase(DELIVERY)) {
        	sql = sql.concat(" AND REFERENCE_TYPE_CD IN ('consignee', 'delivery')");
        }

        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
        	
        	for(String release : releaseNumbers) {
	            pstmt.setString(1, entityAddress.getEntityName());
	            pstmt.setString(2, entityAddress.getLine1());
	            pstmt.setString(3, entityAddress.getCityLocality());
	            pstmt.setString(4, entityAddress.getProvince());
	            pstmt.setString(5, entityAddress.getPostalCode());
	            pstmt.setString(6, entityAddress.getCountry());
	            pstmt.setString(7, shipmentContact.getContactName());
	            pstmt.setString(8, shipmentContact.getContactEmailAddress());
	            pstmt.setString(9, shipmentContact.getContactPhoneNbr());
	            pstmt.setString(10, release);
	
	            pstmt.addBatch();
            }
            
            pstmt.executeBatch();
            
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }

    }

    private void addPoLineItem(List<PoLineItem> lineItems)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Inserting PO Line Items for shipment");

        String sql = "INSERT INTO tms_utf8_db.PO_LINE_ITEM (PARTNER_CD, PARENT_PARTNER_CD, SHIPPER_CD, CUSTOMER_CD, PO_NBR, RELEASE_NBR, EQUIPMENT_SHIPMENT_SEQ_NBR, "
                + "PO_LINE_ITEM_NBR, PRODUCT_CD, PRODUCT_DESC_TXT, RELEASE_LINE_NBR, HAZARDOUS_FLG, ORDERED_QTY, PACKAGE_UOM, ORDERED_WT, ORDERED_VOL, "
                + "DUE_QTY, DUE_WT, DUE_VOL, SHIPPED_QTY, SHIPPED_WT, QTY_UOM, WT_UOM, VOL_UOM, CREATE_TSTAMP, CREATE_USER_ID, TOTAL_WT, TOTAL_VOL, "
                + "SUPPLIER_PRODUCT_CD, OUTER_PKG_CNT, PACKAGE_CNT, PACKAGE_CODE, PACKAGE_DESCRIPTION, CARGO_REFERENCE_NBR, ACTUAL_PO_NBR, ACTUAL_PO_RELEASE_NBR, UNIT_PRICE, goods_value_bill_to_cd "
                + ") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {

            for(PoLineItem lineItem : lineItems) {
                log.info("Batch insert with release number " + lineItem.getReleaseNumber() + " and actual PO release number " + lineItem.getActualPoReleaseNumber());
                log.info(lineItem.toString());
                
                pstmt.setString(1, lineItem.getPartnerCode());
                pstmt.setString(2, lineItem.getParentPartnerCode());
                pstmt.setString(3, lineItem.getShipperCode());
                pstmt.setString(4, lineItem.getCustomerCode());
                pstmt.setString(5, lineItem.getPoNumber());
                pstmt.setString(6, lineItem.getReleaseNumber());
                pstmt.setInt(7, lineItem.getEquipmentShipmentSequenceNumber());
                pstmt.setInt(8, lineItem.getPoLineItemNumber());
                pstmt.setString(9, lineItem.getProductCode());
                pstmt.setString(10, lineItem.getProductDescription());
                pstmt.setString(11, lineItem.getReleaseLineNumber());
                pstmt.setString(12, lineItem.isHazardous() ? "Y" : "N");
                pstmt.setDouble(13, lineItem.getOrderQuantity());
                pstmt.setString(14, lineItem.getQuantityUom());
                pstmt.setDouble(15, lineItem.getOrderWeight());
                pstmt.setDouble(16, lineItem.getOrderVolume());
                pstmt.setDouble(17, lineItem.getDueQuantity());
                pstmt.setDouble(18, lineItem.getDueWeight());
                pstmt.setDouble(19, lineItem.getDueVolume());
                pstmt.setDouble(20, lineItem.getShippedQuantity());
                pstmt.setDouble(21, lineItem.getShippedWeight());
                pstmt.setString(22, lineItem.getQuantityUom());
                pstmt.setString(23, lineItem.getWeightUom());
                pstmt.setString(24, lineItem.getVolumeUom());
                pstmt.setTimestamp(25, new Timestamp(lineItem.getCreateTimestamp().getTime()));
                pstmt.setString(26, lineItem.getCreateUserId());
                pstmt.setDouble(27, lineItem.getTotalWeight());
                pstmt.setDouble(28, lineItem.getTotalVolume());
                pstmt.setString(29, lineItem.getSupplierProductCode());
                pstmt.setDouble(30, lineItem.getOrderQuantity());
                pstmt.setDouble(31, lineItem.getOrderQuantity());
                pstmt.setString(32, lineItem.getQuantityUom());
                pstmt.setString(33, lineItem.getPackageDescription());
                pstmt.setString(34, lineItem.getCargoReferenceNumber());
                pstmt.setString(35, lineItem.getActualPoNumber());
                pstmt.setString(36, lineItem.getActualPoReleaseNumber());
                pstmt.setDouble(37, lineItem.getUnitPrice());
                pstmt.setString(38, lineItem.getGoodsValueBillToCode());
                
                pstmt.addBatch();
            }

            pstmt.executeBatch();
             
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }
        
    }

    private List<PoLineItem> getPoLineItemsByReleaseNumber(List<String> releaseNumbers, String accountCode)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        // For improvement use latest c3p0
        /*PreparedStatement pstmt = conn.prepareStatement("select * from employee where id in (?)");
        java.sql.Array array = conn.createArrayOf("VARCHAR", new Object[]{"1", "2","3"});
        pstmt.setArray(1, array);
        ResultSet rs = pstmt.executeQuery();*/
        
        String params = "";
        int paramLength = releaseNumbers.size();
        for(int cnt = 0; cnt < paramLength; cnt++) {
            params += "?";
            
            if(cnt != paramLength - 1) {
                params += ",";
            }
        }
        
        log.info("Getting PO Line Item for release number: " + releaseNumbers.toString());
        
        String sql = "SELECT * FROM tms_utf8_db.po_line_item WHERE RELEASE_NBR IN (" + params + ")"
        		+ " AND ? IN (PARENT_PARTNER_CD, PARTNER_CD, CUSTOMER_CD, SHIPPER_CD) order by RELEASE_NBR, PO_LINE_ITEM_NBR ASC";

        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            
            int index = 0;
            for(int cnt = 1; cnt <= paramLength; cnt++) {
                pstmt.setString(cnt, releaseNumbers.get(index));
                index++;
            }
            pstmt.setString(++index, accountCode);

            try (ResultSet rs = pstmt.executeQuery()) {
                ResultSetMapper<PoLineItem> driverRawMapper = new ResultSetMapper<PoLineItem>();
                List<PoLineItem> pojoList = driverRawMapper.mapResultSetToObject(rs, PoLineItem.class);
                if (pojoList != null && !pojoList.isEmpty()) {
                    return pojoList;
                }
            }
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }

        return null;
    }
    
    private List<PoLineItem> getPoLineItems(String orderNumber, String releaseNumber, String accountCode)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Getting PO Line Item for release number: " + releaseNumber.toString());
        
        String sql = "SELECT li.*"
        		+ " FROM tms_utf8_db.po_line_item li JOIN tms_utf8_db.po_header a"
        		+ " ON a.po_nbr = li.po_nbr AND a.release_nbr = li.release_nbr"
        		+ " WHERE li.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD "
        		+ " AND li.PARTNER_CD = a.PARTNER_CD"
        		+ " AND li.CUSTOMER_CD = a.CUSTOMER_CD"
        		+ " AND li.SHIPPER_CD = a.SHIPPER_CD"
        		+ " AND ? IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD)"
        		+ " AND a.PO_NBR = ?"
        		+ " AND a.RELEASE_NBR= ?"
        		+ " AND a.internal_status_cd NOT IN ('BR', 'CO', 'EO')";

        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            pstmt.setString(1, accountCode);
            pstmt.setString(2, orderNumber);
            pstmt.setString(3, releaseNumber);

            try (ResultSet rs = pstmt.executeQuery()) {
                ResultSetMapper<PoLineItem> driverRawMapper = new ResultSetMapper<PoLineItem>();
                List<PoLineItem> pojoList = driverRawMapper.mapResultSetToObject(rs, PoLineItem.class);
                if (pojoList != null && !pojoList.isEmpty()) {
                    return pojoList;
                }
            }
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }

        return null;
    }

    private void deletePoLineItem(List<String> releaseNumbers)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Removing PO Line Items for: " + releaseNumbers.toString());
        
        String params = "";
        int paramLength = releaseNumbers.size();
        for(int cnt = 0; cnt < paramLength; cnt++) {
            params += "?";
            
            if(cnt != paramLength - 1) {
                params += ",";
            }
        }
        
        String sql = "DELETE FROM tms_utf8_db.po_line_item WHERE RELEASE_NBR IN (" + params + ")";

        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            
            int index = 0;
            for(int cnt = 1; cnt <= paramLength; cnt++) {
                pstmt.setString(cnt, releaseNumbers.get(index));
                index++;
            }

            pstmt.executeUpdate();
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }
    }
    
    private List<PoLineItem> prepareLineItem(List<ShipmentDetail> shipmentDetails, List<PoLineItem> lineItems, String releaseNumber) {
        log.info("Mapping line items for release number : " + releaseNumber);
        
        List<PoLineItem> merged = new ArrayList<PoLineItem>();

        List<PoLineItem> clonedLineItems  = new ArrayList<PoLineItem>();
        for(PoLineItem item : lineItems) {
        	clonedLineItems.add(new PoLineItem(item));
        }
        
        List<ShipmentDetail> clonedShipmentDetails = new ArrayList<ShipmentDetail>();
        for(ShipmentDetail detail : shipmentDetails) {
        	clonedShipmentDetails.add(new ShipmentDetail(detail));
        }
        
        int i = 1;
        String releaseLineNumber = null; 
         
        Iterator<ShipmentDetail> clonedShipmentDetailsIter = clonedShipmentDetails.listIterator();
        while(clonedShipmentDetailsIter.hasNext()) {
        	ShipmentDetail detail = clonedShipmentDetailsIter.next();
        	
        	Iterator<PoLineItem> clonedLineItemsIter = clonedLineItems.listIterator();
        	while(clonedLineItemsIter.hasNext()) {
        		PoLineItem item = clonedLineItemsIter.next();
        		
        		if(detail.getActualPoReleaseNumber().equalsIgnoreCase(item.getReleaseNumber()) ||
        				(item.getReleaseNumber().equalsIgnoreCase(releaseNumber) 
                        && item.getActualPoReleaseNumber().equalsIgnoreCase(detail.getActualPoReleaseNumber()))) {
        			
        			log.info("Mapping " + releaseNumber + " : " + item.getActualPoReleaseNumber() == null ? item.getReleaseNumber() : item.getActualPoReleaseNumber());
        			
        			// release line number
                    if(item.getReleaseLineNumber() != null || item.getReleaseLineNumber().trim().length() > 0) {
                        releaseLineNumber = item.getReleaseLineNumber();
                    } else {
                        releaseLineNumber = Integer.toString(i);
                    }
                    
                    item.setDueQuantity(detail.getQty());
                    item.setOrderQuantity(detail.getQty());
                    item.setShippedQuantity(detail.getQty());
                    item.setPackageCount(detail.getQty().intValue());
                    item.setOuterPackageCount(detail.getQty().intValue());
                    item.setOrderWeight(detail.getShipmentWt());
                    item.setDueWeight(detail.getShipmentWt());
                    item.setShippedWeight(detail.getShipmentWt());
                    item.setTotalWeight(detail.getShipmentWt());
                    item.setOrderVolume(detail.getShipmentVol());
                    item.setDueVolume(detail.getShipmentVol());
                    item.setTotalVolume(detail.getShipmentVol());
                    item.setPackageUom(detail.getUnitOfMeasure());
                    item.setQuantityUom(detail.getUnitOfMeasure());
                    item.setPackageCode(detail.getUnitOfMeasure());
                    item.setWeightUom(detail.getWtUom());
                    item.setVolumeUom(detail.getVolUom());
                    item.setProductDescription(detail.getProductDescTxt());
                    item.setHazardous(detail.getIsHazardous());
                    item.setProductCode(detail.getProductCode());
                    item.setPoLineItemNumber(i++);
                    item.setReleaseLineNumber(releaseLineNumber);
                    item.setUnitPrice(detail.getUnitPrice());
                    item.setGoodsValueBillToCode(detail.getGoodsValueBillToCode());
                    
                    merged.add(item);
                    
                    clonedLineItemsIter.remove();
                    
                    break;
        		} 
        	}
        }

        return merged;
    }
    
    public List<ShipmentDetail> isShipmentExists(String orderNumber, String releaseNumber, String accountCode) 
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
    	
        log.info("Checking shipment with release number " + releaseNumber + " and order number " + orderNumber);
        
        List<ShipmentDetail> shipmentDetails = new ArrayList<ShipmentDetail>();
        List<PoLineItem> lineItems = this.getPoLineItems(orderNumber, releaseNumber, accountCode);
        boolean isVpo = false;
        
        if(lineItems != null) {
        	for(PoLineItem lineItem : lineItems) {
        		if(lineItem.getActualPoNumber() == null 
        				|| lineItem.getActualPoNumber().isEmpty() 
        				|| lineItem.getActualPoReleaseNumber() == null 
        				|| lineItem.getActualPoReleaseNumber().isEmpty()) {
        			
        			ShipmentDetail shipmentDetail = new ShipmentDetail();
                    shipmentDetail.setQty(lineItem.getOrderQuantity());
                    shipmentDetail.setQtyUom(lineItem.getQuantityUom());
                    shipmentDetail.setShipmentWt(lineItem.getTotalWeight());
                    shipmentDetail.setWtUom(lineItem.getWeightUom());
                    shipmentDetail.setShipmentVol(lineItem.getTotalVolume());
                    shipmentDetail.setVolUom(lineItem.getVolumeUom());
                    shipmentDetail.setIsHazardous(lineItem.isHazardous());
                    shipmentDetail.setProductDescTxt(lineItem.getProductDescription());
                    shipmentDetail.setProductCode(lineItem.getProductCode());
                    shipmentDetail.setActualPoNumber(lineItem.getPoNumber());
                    shipmentDetail.setActualPoReleaseNumber(lineItem.getReleaseNumber());
                    shipmentDetail.setUnitPrice(lineItem.getUnitPrice());
                    shipmentDetail.setGoodsValueBillToCode(lineItem.getGoodsValueBillToCode());
                    
                    shipmentDetails.add(shipmentDetail);
        		} else {
        			log.info("Shipment " + releaseNumber + " is under a VPO and could not be added as LPO");
        			
        			isVpo = true;
        		}
        	}
        }
        
        return isVpo ? null : shipmentDetails;
    }
    
    private void updateEquipment(String releaseNumber, String equipmentCode, int equipmentId)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Updating equipment for shipment " + releaseNumber + ": equipment code = " + equipmentCode + ", equipment id = " + equipmentId);

        String sql = "update tms_utf8_db.po_equipment_requirement set equipment_type_cd = ?, " + 
        		" equipment_desc_txt = (select distinct Equipment_Type_Name from marketplace_db.equipment_type where Equipment_Type_Cd = ? and equipment_type_id = ?) " + 
        		" where release_nbr = ?";
        
        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {

            pstmt.setString(1, equipmentCode);
            pstmt.setString(2, equipmentCode);
            pstmt.setInt(3, equipmentId);
            pstmt.setString(4, releaseNumber);   

            pstmt.execute();
             
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }
        
    }
    
    private List<PoLineItem> getPoLineItemsByActualPoReleaseNumber(String actualPoReleaseNumbers, String accountCode)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Getting PO Line Item for actual po release number: " + actualPoReleaseNumbers.toString());
        
        String sql = "select  li.actual_po_nbr, li.actual_po_release_nbr, li.* from tms_utf8_db.po_line_item li where " + 
        		"li.release_nbr = (select  li.release_nbr from tms_utf8_db.po_line_item li where " + 
        		"	li.actual_po_release_nbr = ? " + 
        		"	and li.actual_po_release_nbr <> li.release_nbr" + 
        		"	and li.actual_po_release_nbr is not null limit 1) " + 
        		"and li.actual_po_release_nbr <> ?  " + 
        		"and li.actual_po_release_nbr <> li.release_nbr " + 
        		"and li.actual_po_release_nbr is not null " + 
        		"and ? IN (li.PARENT_PARTNER_CD, li.PARTNER_CD, li.CUSTOMER_CD, li.SHIPPER_CD) " + 
        		"order by li.po_line_item_nbr asc";

        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            pstmt.setString(1, actualPoReleaseNumbers);
            pstmt.setString(2, actualPoReleaseNumbers);
            pstmt.setString(3, accountCode);

            try (ResultSet rs = pstmt.executeQuery()) {
                ResultSetMapper<PoLineItem> driverRawMapper = new ResultSetMapper<PoLineItem>();
                List<PoLineItem> pojoList = driverRawMapper.mapResultSetToObject(rs, PoLineItem.class);
                if (pojoList != null && !pojoList.isEmpty()) {
                    return pojoList;
                }
            }
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }

        return null;
    }
    
    private List<PoLineItem> prepareVpoLineItem(List<PoLineItem> lpoLineItems, String releaseNumber, String accountCode) 
    		throws Exception, SQLFeatureNotSupportedException, SQLException, Exception {
    	
    	List<PoLineItem> merged = new ArrayList<PoLineItem>();
    	
    	List<PoLineItem> otherLpoLineItems = new ArrayList<PoLineItem>();
    	otherLpoLineItems = this.getPoLineItemsByActualPoReleaseNumber(releaseNumber, accountCode);
    	String vpoReleaseNumber = this.getVpoReleaseNumber(releaseNumber, accountCode);
    	
    	if(otherLpoLineItems != null && otherLpoLineItems.size() > 0) {
    		merged.addAll(otherLpoLineItems);
        	
        	log.info("Other VPO Line items: " + otherLpoLineItems.toString());
    	}
    	
    	int i = 1;
    	List<PoLineItem> toAddLpoLineItems = new ArrayList<PoLineItem>();
    	for(PoLineItem li : lpoLineItems) {
    		PoLineItem tmpLi = new PoLineItem(li);
    		
    		String actualReleaseNumber = tmpLi.getReleaseNumber();
    		String actualPoNumber = tmpLi.getPoNumber();
    		
    		tmpLi.setReleaseNumber(vpoReleaseNumber);
    		tmpLi.setPoNumber(vpoReleaseNumber);
    		tmpLi.setActualPoReleaseNumber(actualReleaseNumber);
    		tmpLi.setActualPoNumber(actualPoNumber);
    		
    		toAddLpoLineItems.add(tmpLi);
    	}
    	
    	if(toAddLpoLineItems.size() > 0) {
    		merged.addAll(toAddLpoLineItems);
    	}
    	
    	for(PoLineItem li : merged) {
    		li.setPoLineItemNumber(i++);
    	}
    	
    	
    	log.info("Merged to add LPO line items: " + merged.toString());
    	
    	return merged.size() > 0 ? merged : null;
    }
    
    private String getVpoReleaseNumber(String releaseNumber, String accountCode)
            throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Getting actual release number: " + releaseNumber.toString());
        
        String sql = "select li.release_nbr from tms_utf8_db.po_line_item li"
        		+ " where li.actual_po_release_nbr = ? and ? IN (li.PARENT_PARTNER_CD, li.PARTNER_CD, li.CUSTOMER_CD, li.SHIPPER_CD) limit 1";

        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            pstmt.setString(1, releaseNumber);
            pstmt.setString(2, accountCode);

            try (ResultSet rs = pstmt.executeQuery()) {
                if(rs.next()) {
                	return rs.getString("release_nbr");
                }

                return null;
            }
            
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }
    }
    
}
