package com.openport.marketplace.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.util.List;

import org.apache.log4j.Logger;

import com.openport.marketplace.model.GoodsValueBillToParty;
import com.openport.marketplace.model.Partner;
import com.openport.util.db.ResultSetMapper;

public class PartnerRepository {
	
	private static final Logger log = Logger.getLogger(PartnerRepository.class);
	

	public String getLogoPath(String partner) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		String sql = "select p.document_logo_filename from tms_utf8_db.partner p where p.partner_cd = ?";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, partner);
			
			ResultSet rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				String s = rs.getString("document_logo_filename"); 
				
				return s;
			}
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
		
		return null;
	}
	
	public Partner getBillToParty(String code) 
    		throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
        
        log.info("Getting bill to party for: " + code);
        
        String sql = "select p.* from tms_utf8_db.partner p where ? in (p.bill_to_party_cd, p.partner_cd)";

        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            pstmt.setString(1, code);

            try (ResultSet rs = pstmt.executeQuery()) {
                if(rs.next()) {
                	return new Partner(rs);
                }
            }
            
        } catch (SQLFeatureNotSupportedException e) {
            log.error("SQLFeatureNotSupportedException error message: " + e, e);
            throw e;
        } catch (SQLTimeoutException e) {
            log.error("SQLTimeoutException error message: " + e, e);
            throw e;
        } catch (SQLException e) {
            log.error("SQLException error message: " + e, e);
            throw e;
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
            throw e;
        }
    	
    	return null;
    }
	
	public Partner getPartner(String code) 
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		
		log.info("Getting partner for: " + code);
		
		String sql = "select p.* from tms_utf8_db.partner p where p.partner_cd = ?";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, code);
			
			try (ResultSet rs = pstmt.executeQuery()) {
                if(rs.next()) {
                	return new Partner(rs);
                }
            }
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
		
		return null;
	}
	
	public List<GoodsValueBillToParty> getGoodsValueBillToList(String code) {
		
        log.info("Getting goods value bill to party for: " + code);
        
        String sql = "select partner_cd, partner_name, currency_cd, CONCAT(partner_name, ' (', partner_cd, ')') as name_code from tms_utf8_db.partner "
        		+ "where ? in (partner_cd, parent_partner_cd) and (bill_to_party_flg='Y' OR consignee_flg='Y')";
        
        try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
                PreparedStatement pstmt = connection.prepareStatement(sql);) {
            pstmt.setString(1, code);
            
            try (ResultSet rs = pstmt.executeQuery()) {
                ResultSetMapper<GoodsValueBillToParty> driverRawMapper = new ResultSetMapper<GoodsValueBillToParty>();
                List<GoodsValueBillToParty> pojoList = driverRawMapper.mapResultSetToObject(rs, GoodsValueBillToParty.class);
                if (pojoList != null && !pojoList.isEmpty()) {
                	
                    return pojoList;
                }
            }
            
        } catch (Exception e) {
            log.error("Exception error message: " + e, e);
        }
    	
    	return null;
	}
}
