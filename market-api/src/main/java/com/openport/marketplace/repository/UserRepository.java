package com.openport.marketplace.repository;

import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.openport.marketplace.json.AnalyticsMenu;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.CompanyAddress;
import com.openport.marketplace.json.CorporateAddress;
import com.openport.marketplace.json.Password;
import com.openport.marketplace.json.Role;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.json.Token;
import com.openport.marketplace.json.User;
import com.openport.marketplace.json.UserLoginRequest;
import com.openport.marketplace.json.UserRole;
import com.openport.marketplace.json.UserStatus;
import com.openport.marketplace.repository.DbConfigTMS;
import com.openport.util.db.ResultSetMapper;
import com.openport.util.password.MySqlPassword;

public class UserRepository {

	static final transient Logger log = Logger.getLogger(UserRepository.class);

	public UserRepository() {
		// TODO Auto-generated constructor stub
	}
	
	/**
     * author JMACH
     * @param token
     * @return List<User> list of users that match the token
     * @throws SQLException
     */
    public static List<User> getTokenUserInfo(String token) throws SQLException{
		List<User> userList = new LinkedList<User>();
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT u.*, c.Company_ID as company_id ")
		.append("FROM marketplace_db.user_session  us ")
		.append("JOIN marketplace_db.user u ON u.User_ID = us.User_ID ")
		.append("JOIN marketplace_db.company_user cu ON cu.User_ID = u.User_ID ")
		.append("JOIN marketplace_db.company c ON c.Company_ID = cu.Company_ID ")
		.append("WHERE token = ? ");

		log.debug("SQL: "+query.toString());
		try(java.sql.Connection conn = DbConfigTMS.getDataSource().getConnection()){
			PreparedStatement p = conn.prepareStatement(query.toString());
			p.setString(1, token);
			
			ResultSet rs = p.executeQuery();
			ResultSetMapper<User> userRawMapper = new ResultSetMapper<User>();
			userList = userRawMapper.mapResultSetToObject(rs, User.class);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return userList;
	}

	public static User findUser(UserLoginRequest userLogin) {
		String sql = "SELECT User_ID, email_address, first_name, last_name " // , password_hash 
				+ "        , phone_nbr, Primary_User_Role_Id, Is_Test, Is_Active, Is_Password_Weak "
				+ "        , Is_Internal, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "
				+ "     FROM marketplace_db.user "
				+ "    WHERE email_address = ? "
				+ "      AND password_hash = ? "
				+ "      AND Is_Active = 1;";
		System.out.println("sql:"+sql);
		User usr1 = null;
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			pstmt.setString(1, userLogin.getUsername());
			String passwordHash = MySqlPassword.passwordHash(userLogin.getPassword());
			pstmt.setString(2, passwordHash);
			
		     try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
		    	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
		    	if (pojoList!=null && !pojoList.isEmpty()) {
		    		//							Token tkn = new Token();
		    		//							tkn.setAccessToken(usr.getActiveToken());
		    		//							tkn.setExpires(futureExpiration());
		    		//							dDao.updateToken(usr, tkn);
		    		//							sts.setToken(tkn);
		    		
		    		usr1 = pojoList.get(0);
		    	}
		     }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return usr1;
	}

	public static Company getCompanyByUserId(int userID) {
		Company company = null;
		String sql = "SELECT c.Company_ID, c.Company_Name, c.Is_Carrier, c.Is_Shipper, c.Company_Code, c.Corporate_Address_ID, "
				+ "           c.Truck_Pool_Address_ID, c.Branch_Address_ID, c.Create_User_ID, c.Create_Dt, c.Update_User_ID, c.Update_Dt, "
				+ "           c.Wizard_Step_Nbr, c.Years_of_operation,c.TIMEZONE_ID, c.business_Headline, c.business_Description, coalesce(currency_cd, '') as currency_cd "
				+ "      FROM marketplace_db.company c, marketplace_db.company_user u "
				+ "     WHERE c.Company_ID = u.Company_ID AND u.User_ID = ?;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, userID);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
	        	List<Company> pojoList = driverRawMapper.mapResultSetToObject(rs, Company.class);
	        	if (!pojoList.isEmpty()) {
	        		company = pojoList.get(0);
	        		company.setCountryCd(getCompanyAddress(company.getCorporateAddressId()).getCountry2Cd());
	        		company.setCorporateAddress(getCompanyCorporateAddress(company.getCorporateAddressId()));
	        		company.setAnalytics(getAnalyticsMenu(company.getCompanyCode()));
	        	}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return company;
	}
	
	public static CompanyAddress getCompanyCorporateAddress(int corporateId) {
		
		String sql = "SELECT a.company_address_id,a.Address_Line_1, a.Address_Line_2, a.Address_Line_3, a.City_Name, a.Province_ID, c.province_name, "
					+ " a.Country_ID, b.country_2_cd, Postal_Cd,"
					+ " a.latitude, a.longtitude, a.update_dt "
					+ " from  company_address a inner join country b  on a.country_id=b.Country_ID"
					+ " left join province c on c.province_id=a.province_id "
					+ " where a.company_address_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, corporateId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CompanyAddress> driverRawMapper = new ResultSetMapper<CompanyAddress>();
	        	List<CompanyAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, CompanyAddress.class);
	        	if (!pojoList.isEmpty() && pojoList.size()>0) {
	        		return pojoList.get(0);
	        	}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}

	public static CorporateAddress getCompanyAddress(int corporateId) {
		String sql = "SELECT country_2_cd from  company_address a inner join country b  on a.country_id=b.Country_ID where a.company_address_id=?";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setInt(1, corporateId);

			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<CorporateAddress> driverRawMapper = new ResultSetMapper<CorporateAddress>();
	        	List<CorporateAddress> pojoList = driverRawMapper.mapResultSetToObject(rs, CorporateAddress.class);
	        	if (!pojoList.isEmpty() && pojoList.size()>0) {
	        		return pojoList.get(0);
	        	}
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return null;
	}
	
	

	
	public static User getUserByUserId(int userId) {
		User user = null;
		String sql = "SELECT User_ID, email_address, first_name, last_name " // , password_hash
					+ "        , phone_nbr, Primary_User_Role_Id, Is_Test, Is_Active, Is_Password_Weak "
					+ "        , Is_Internal, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "
					+ "     FROM marketplace_db.user "
					+ "    WHERE User_ID = ? "
					+ "      AND Is_Active = 1;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			pstmt.setLong(1, userId);
			
		     try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
		    	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
		    	if (!pojoList.isEmpty()) {
//							Token tkn = new Token();
//							tkn.setAccessToken(usr.getActiveToken());
//							tkn.setExpires(futureExpiration());
//							dDao.updateToken(usr, tkn);
//							sts.setToken(tkn);
		    		user = pojoList.get(0);
		    	}
		     }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}			  
		return user;
	}

	public static User getUserByEmail(String emailAddress) {
		User user = null;
		String sql = "SELECT User_ID, email_address, first_name, last_name " // , password_hash
					+ "        , phone_nbr, Primary_User_Role_Id, Is_Test, Is_Active, Is_Password_Weak "
					+ "        , Is_Internal, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "
					+ "     FROM marketplace_db.user "
					+ "    WHERE email_address = ? "
					+ "      AND Is_Active = 1;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			pstmt.setString(1, emailAddress);
			
		     try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
		    	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
		    	if (!pojoList.isEmpty()) {
//							Token tkn = new Token();
//							tkn.setAccessToken(usr.getActiveToken());
//							tkn.setExpires(futureExpiration());
//							dDao.updateToken(usr, tkn);
//							sts.setToken(tkn);
		    		user = pojoList.get(0);
		    	}
		     }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}			  
		return user;
	}

	public static List<User> getAllUsers() {
		List<User> shps = new ArrayList<User>();
		String sql = "SELECT User_ID, email_address, first_name, last_name " // , password_hash 
					+ "        , phone_nbr, Primary_User_Role_Id, Is_Test, Is_Active, Is_Password_Weak "
					+ "        , Is_Internal, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt "
					+ "     FROM marketplace_db.user "
					+ "    WHERE Is_Active = 1;";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
	         try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
	        	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
	        	if (!pojoList.isEmpty()) {
	        		pojoList.forEach(u->shps.add(u));
//						Token tkn = new Token();
//						tkn.setAccessToken(usr.getActiveToken());
//						tkn.setExpires(futureExpiration());
//						dDao.updateToken(usr, tkn);
//						sts.setToken(tkn);
				}
//				return shps;
	         }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return shps;
	}

	public static boolean isExistingUser(User usr) {
		String sql = "SELECT Create_User_ID, Create_Dt, Update_User_ID, Update_Dt, Is_Password_Weak, email_address "
				+ "     FROM marketplace_db.user "
				+ "    WHERE email_address = ? ";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			String emailAddress = usr.getUsername()!=null?usr.getUsername():usr.getEmailAddress();
			pstmt.setString(1, emailAddress);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<User> driverRawMapper = new ResultSetMapper<User>();
		    	List<User> pojoList = driverRawMapper.mapResultSetToObject(rs, User.class);
		    	if (pojoList!=null && !pojoList.isEmpty()) {
		    		return true;
		    	}
		     }
		} catch (SQLException e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.CONFLICT);
		}
		return false;
	}

	public static Company findCompanyByName(User usr) {
		log.info("Start - '"+usr.getCompany().getCompanyName()+"'");
		String sql = "SELECT Company_Id, Company_Code, is_Carrier, is_Shipper "
				+ "  FROM marketplace_db.company "
				+ " WHERE Company_Name = ? ";
		Company company = null;
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			pstmt.setString(1, usr.getCompany().getCompanyName());
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
		    	List<Company> pojoList = driverRawMapper.mapResultSetToObject(rs, Company.class);
		    	if (pojoList!=null && !pojoList.isEmpty()) {
		    		company = pojoList.get(0);
		    		log.info("Found ID:"+company.getCompanyId());
		    	}
		     }
		} catch (SQLException e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done - '"+usr.getCompany().getCompanyName()+"'");
		}
		return company;
	}
	
	public static Company findCompanyByEmail(User usr) {
		log.info("Start - '"+usr.getCompany().getCompanyName()+"'");
		String sql = "SELECT c.Company_Id, c.Company_Code, c.is_Carrier, c.is_Shipper "
				+" FROM marketplace_db.company c join  marketplace_db.company_user cu  on c.company_id=cu.company_id "
				+ "join marketplace_db.user u on cu.user_id=u.user_id "
				+ "    WHERE u.email_address = ? ";

		Company company = null;
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			String emailAddress = usr.getUsername()!=null?usr.getUsername():usr.getEmailAddress();
			pstmt.setString(1, emailAddress);
			try (ResultSet rs = pstmt.executeQuery()) {
				ResultSetMapper<Company> driverRawMapper = new ResultSetMapper<Company>();
		    	List<Company> pojoList = driverRawMapper.mapResultSetToObject(rs, Company.class);
		    	if (pojoList!=null && !pojoList.isEmpty()) {
		    		company = pojoList.get(0);
		    		log.info("Found ID:"+company.getCompanyId());
		    	}
		     }
		} catch (SQLException e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done - '"+usr.getCompany().getCompanyName()+"'");
		}
		return company;
	}

	public static void createCompanyUser(int userId, boolean isFirst, int companyId) {
		log.info("Creating Company User UserId:"+userId+"; CompanyId:"+companyId);
		int executeUpdate = 0;
		String sql = "INSERT INTO marketplace_db.company_user(Company_ID, User_ID, Is_Primary_Contact, Is_Technical_Contact, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt) "+
		" VALUES (?, ?, ?, ?, 0, CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP());";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) {
			pstmt.setInt(1, companyId); // Company_ID
			pstmt.setInt(2, userId); // User_ID
			pstmt.setBoolean(3, isFirst); // Is_Primary_Contact
			pstmt.setBoolean(4, isFirst); // Is_Technical_Contact
			executeUpdate = pstmt.executeUpdate();
		} catch (Exception e) {
			log.error("Unable to create Company User UserId:"+userId+"; CompanyId:"+companyId+"; Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done UserId:"+userId+"; CompanyId:"+companyId+"; Inserted:"+executeUpdate);
		}
	}

	public static String generateToken() {
		UUID idOne = UUID.randomUUID();
		return DigestUtils.sha1Hex(idOne.toString());
	}

	public static List<AnalyticsMenu> getAnalyticsMenu(String partner) {
		log.info("partner "+partner);
		try {
			String sql = " select partner_cd,menu_label_txt,bookmark_id,report_id,report_type_cd from analytics_menu\n"
					+ " where partner_cd=? and report_type_cd is not null order by menu_label_txt";
			try (java.sql.Connection connection = DbConfigAnalytics.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql);) {
				pstmt.setString(1, partner);
				
				try (ResultSet rs = pstmt.executeQuery()) {
					ResultSetMapper<AnalyticsMenu> driverRawMapper = new ResultSetMapper<AnalyticsMenu>();
					List<AnalyticsMenu> pojoList = driverRawMapper.mapResultSetToObject(rs, AnalyticsMenu.class);
					// List<Driver> nw_pojoList = new LinkedList<Driver>();
					if (pojoList != null && !pojoList.isEmpty()) {
						return pojoList;
					}
				}

			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}
		} finally {
			log.debug("Done");
		}
		return null;

		
	}
	
	public static Token createToken(int userId) {
		log.info("Creating Token for UserId:"+userId);
		int executeUpdate = 0;
		String accessToken = generateToken();
		String sql = "INSERT INTO marketplace_db.user_session(token, User_ID, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt) "+
					" VALUES (?, ?, 0, CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP());";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) {
			pstmt.setString(1, accessToken);
			pstmt.setInt(2, userId);
			executeUpdate = pstmt.executeUpdate();
		} catch (Exception e) {
			log.error("Unable to create Token for UserId:"+userId+"; Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done UserId:"+userId+"; Inserted:"+executeUpdate);
		}
		Token token = new Token();
		token.setAccessToken(accessToken);
		return token;
	}

	public static UserStatus createUser(User usr, UserStatus sts) {
		log.info("Start "+usr.getUsername());
		String sql = "insert into `user`(email_address,password_hash,first_name,last_name,Primary_User_Role_Id,is_active,is_test,is_internal,Create_user_id,Create_Dt,update_user_id,Update_dt,Is_Password_Weak) "
				+ "values (?,?,?,?,1,1,0,0,0,CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP(), 0);";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) {
			pstmt.setString(1, usr.getUsername());
			String passwordHash = MySqlPassword.passwordHash(usr.getPassword());
			pstmt.setString(2, passwordHash);
			pstmt.setString(3, usr.getFirstName());
			pstmt.setString(4, usr.getLastName());
			int id = pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
		    if(rs.next()) {
		      id =rs.getInt(1); 
		      usr.setUserId(id);
		      log.debug("New User create with ID:"+id);
		    }
			
			User usr1 = new User();
			usr1.setUsername(usr.getFirstName()+" "+usr.getLastName());
			usr1.setEmailAddress(usr.getEmailAddress());
			usr1.setUserId(id);
			usr1.setCompanyId(usr.getCompanyId());
			sts.setUser(usr1);
			sts.setSuccess("true");
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done "+usr.getUsername());
		}
		return sts;
	}

	public static int createCompany(User usr, Company company) {
		log.info("Start - "+usr.getCompany().getCompanyName());
		String companyCode = null;
		int companyId = -1;
		if (company!= null) {
			log.info("Reusing Company "+company+"; dup.getCompanyId():"+company.getCompanyId());
			companyId = company.getCompanyId();
			companyCode = company.getCompanyCode();
		} else {
			log.info("Creating Company");
			companyCode = generateCompanyCode();
			String sql = "INSERT INTO marketplace_db.company(Company_Name, Is_Carrier, Is_Shipper, Company_Code, Corporate_Address_ID, Truck_Pool_Address_ID, Branch_Address_ID, Create_User_ID, Create_Dt, Update_User_ID, Update_Dt, Wizard_Step_Nbr, Years_of_operation) "+
			" VALUES (?, ?, ?, ?, -1, -1, -1, 0,CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP(), 0, -1);";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS );) {
				pstmt.setString(1, usr.getCompany().getCompanyName());
				pstmt.setBoolean(2, usr.getCompany().isCarrier());
				pstmt.setBoolean(3, usr.getCompany().isShipper());
				pstmt.setString(4, companyCode);
				companyId = pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
			    if(rs.next()) {
			    	companyId =rs.getInt(1); 
			      usr.setUserId(companyId);
			      log.debug("New Company create with ID:"+companyId);
			    }
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			} finally {
				log.info("Done - "+usr.getCompany().getCompanyName());
			}
		}
		return companyId;
	}

	private static String generateCompanyCode() {
		String companyCode;
		String alphabet= "abcdefghijklmnopqrstuvwxyz";
		String s = "";
		Random random = new Random();
		int fixedLen = 6;
		for (int i = 0; i < fixedLen; i++) {
		    char c = alphabet.charAt(random.nextInt(26));
		    s+=c;
		}
		companyCode = s;
		return companyCode;
	}

	public static boolean updateTempPassword(String emailAddress, String firstName, String lastName, String tmpPassword) {
		String sql = "UPDATE marketplace_db.user "
				+ "     SET Update_Dt = now(), "
				+ "         Is_Password_Weak = 1, "
				+ "         Password_Hash = ? "
				+ "    WHERE email_address = ? "
				+ "      AND first_name = ? "
				+ "      AND last_name = ? ";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			String passwordHash = MySqlPassword.passwordHash(tmpPassword);
			pstmt.setString(1, passwordHash);
			pstmt.setString(2, emailAddress);
			pstmt.setString(3, firstName);
			pstmt.setString(4, lastName);
			if (pstmt.executeUpdate()==0) {
				return false;
			}
		} catch (SQLException e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.CONFLICT);
		}
		return true;
	}

	public static void update(int userId, Password usr, UserStatus sts) {
		try {
			String newPassword = usr.getNewPassword();
			String emailAddress = usr.getEmailAddress();
			String oldPassword = usr.getOldPassword();

			String sql = "UPDATE marketplace_db.user "
					+ "     SET Update_Dt = now(), "
					+ "         Password_Hash = ?, "
					+ "         Is_Password_Weak = 0 "
					+ "    WHERE email_address = ? "
					+ "      AND Password_Hash = ? "
					+ "      AND User_Id = ? ";
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setString(1, MySqlPassword.passwordHash(newPassword));
				pstmt.setString(2, emailAddress);
				pstmt.setString(3, MySqlPassword.passwordHash(oldPassword));
				pstmt.setInt(4, userId);
				if (pstmt.executeUpdate()==0) {
					sts.setStatus(Response.Status.CONFLICT.getStatusCode());
					sts.setMessage("Unable to change password");
				}
			} catch (SQLException e1) {
				log.error("Error message: " + e1, e1);
				new ServiceException(Response.Status.CONFLICT);
			}

		} catch (Exception e) {
			log.error(e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done");
		}
	}
	
	public static UserRole getUserRole(String emailAddress) {
		try {
			User user = UserRepository.getUserByEmail(emailAddress);
			String sql = "SELECT * FROM marketplace_db.user_role WHERE User_ID = ?";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql );) {
				pstmt.setInt(1, user.getUserId());
				
				UserRole userRole = new UserRole();
				try (ResultSet rs = pstmt.executeQuery()) {
					// If multiple role is set, the first role is returned based from user role id
					if (rs.next()) {
						com.openport.marketplace.model.UserRole role = new com.openport.marketplace.model.UserRole(rs);
						userRole.setUserRoleId(role.getUserRoleId());
						userRole.setUserId(role.getUserId());
						userRole.setRoleId(role.getRoleId());
						userRole.setCreateUserId(role.getCreateUserId());
						userRole.setCreateDt(role.getCreateDt());
						userRole.setUpdateUserId(role.getUpdateUserId());
						userRole.setUpdateDt(role.getUpdateDt());
			        }
					return userRole;
		         }
			} catch (SQLException e1) {
				log.error("Error message: " + e1, e1);
				new ServiceException(Response.Status.CONFLICT);
				
				return null;
			}

		} catch (Exception e) {
			log.error(e, e);
			new ServiceException(Response.Status.CONFLICT);
			
			return null;
		} finally {
			log.info("Done");
		}
	}
	
	public static void setUserRole(UserRole userRole) {
		try {
			String sql = "insert marketplace_db.user_role(User_ID, Role_Id, Create_User_ID, Create_Dt, Update_Dt) values(?,?,?,?,?);";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
					pstmt.setInt(1, userRole.getUserId());
					pstmt.setInt(2, userRole.getRoleId());
					pstmt.setInt(3, userRole.getCreateUserId());
					pstmt.setTimestamp(4, userRole.getCreateDt());
					pstmt.setTimestamp(5, userRole.getUpdateDt());
					pstmt.execute();
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}		
		} catch (Exception e) {
			log.error(e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done");
		}
	}
	
	public static void updateUserRole(UserRole userRole) {
		try {
			String sql = "update marketplace_db.user_role set Role_Id = ?, Update_User_ID = ?, Update_Dt = ?  where User_ID = ?";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
					pstmt.setInt(1, userRole.getRoleId());
					pstmt.setInt(2, userRole.getUpdateUserId());
					pstmt.setTimestamp(3, userRole.getUpdateDt());
					pstmt.setInt(4, userRole.getUserId());
					pstmt.execute();
			} catch (Exception e) {
				log.error("Error message: " + e, e);
				new ServiceException(Response.Status.CONFLICT);
			}		
		} catch (Exception e) {
			log.error(e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done");
		}
	}
	
	
	public static String getAllUserRole() {
		
		try {
			//String sql = "select u.User_ID, u.first_name, u.last_name, u.email_address, r.Role_Name, r.Role_Id from marketplace_db.user u, marketplace_db.user_role ur, marketplace_db.role r where u.User_ID = ur.User_ID and ur.Role_Id = r.Role_Id";
			String sql = "select u.User_ID, u.first_name, u.last_name, u.email_address, r.Role_Name, r.Role_Id from marketplace_db.user u left outer join  marketplace_db.user_role ur on u.User_ID = ur.User_ID left outer join marketplace_db.role r on r.Role_Id = ur.Role_Id";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql );) {
				
				ResultSet rs = pstmt.executeQuery();
				
				JsonArrayBuilder jsonArray = Json.createArrayBuilder();
				while(rs.next()) {
					JsonObjectBuilder jsonObject = Json.createObjectBuilder();
					jsonObject.add("userId", rs.getInt("User_ID"));
					jsonObject.add("firstName", rs.getString("first_name"));
					jsonObject.add("lastName", rs.getString("last_name"));
					jsonObject.add("emailAddress", rs.getString("email_address"));
					jsonObject.add("roleName", rs.getString("Role_Name") == null ? "" : rs.getString("Role_Name"));
					jsonObject.add("roleId", rs.getInt("Role_Id") == 0 ? 0 : rs.getInt("Role_Id"));
					
					jsonArray.add(jsonObject.build());
				}
				
		        StringWriter stringWriter = new StringWriter();
		        JsonWriter writer = Json.createWriter(stringWriter);
		        writer.writeArray(jsonArray.build());
		        writer.close();
		        
		        return stringWriter.getBuffer().toString();
				
			} catch (SQLException e1) {
				log.error("Error message: " + e1, e1);
				new ServiceException(Response.Status.CONFLICT);
			}

		} catch (Exception e) {
			log.error(e, e);
			new ServiceException(Response.Status.CONFLICT);
		} finally {
			log.info("Done");
		}
		
		return null;
	}
	
	public static List<Role> getRoles() {
		try {
			String sql = "SELECT * FROM marketplace_db.role";
			
			try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
					PreparedStatement pstmt = connection.prepareStatement(sql );) {
				
				List<Role> roleList = new ArrayList<Role>();
				try (ResultSet rs = pstmt.executeQuery()) {
					while(rs.next()) {
						Role role = new Role();
						com.openport.marketplace.model.Role r = new com.openport.marketplace.model.Role(rs);
						role.setRoleId(r.getRoleId());
						role.setRoleName(r.getRoleName());
						role.setCreateUserId(r.getCreateUserId());
						role.setCreateDt(r.getCreateDt());
						role.setUpdateUserId(r.getUpdateUserId());
						role.setUpdateDt(r.getUpdateDt());
						
						roleList.add(role);
			        }
					return roleList;
		         }
			} catch (SQLException e1) {
				log.error("Error message: " + e1, e1);
				new ServiceException(Response.Status.CONFLICT);
				
				return null;
			}

		} catch (Exception e) {
			log.error(e, e);
			new ServiceException(Response.Status.CONFLICT);
			
			return null;
		} finally {
			log.info("Done");
		}
	}

}
