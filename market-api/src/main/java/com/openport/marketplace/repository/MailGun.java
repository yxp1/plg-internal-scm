package com.openport.marketplace.repository;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

/**
 * @author Oscar
 *
 */
public class MailGun {

	public MailGun() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        String sender = "Do-Not-Reply@openport.com";
        String sendTo = "oscar.yu@openport.com";
        String subject = "testing";
        String message = "testing";
        Response response = MailGun.sendMessage(sender, sendTo, subject, message);
        //If everything goes correct you should be able to see status 200 in response
        System.out.println("Mail sent : " + response);
	}

	public static Response sendMessage(String sendTo, String subject, String message) {
		return sendMessage("Do-Not-Reply@openport.com", sendTo, subject, message);
	}
	
	public static Response sendMessage(String sender, String sendTo, String subject, String message) {
        Form formData = new Form();
		formData.param("from", sender + " <postmaster@openport.com>");
		formData.param("to", sendTo);
		formData.param("subject", subject);
		formData.param("text", message);
        
        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        resteasyClient.register(new BasicAuthentication("api", "key-31a46ec04ba0a8fda226313bb4af9a11"));
        Response response = resteasyClient.target("https://api.mailgun.net/v3/openport.com/messages").request().buildPost(Entity.form(formData)).invoke();
        
        return response;
	}

}
