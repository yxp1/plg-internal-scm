package com.openport.marketplace.repository;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.openport.marketplace.json.AccessorialCode;
import com.openport.marketplace.json.ConsolidatedShipment;
import com.openport.marketplace.json.LogisticsColumns;
import com.openport.marketplace.json.ServiceException;

public class LogisticsRepository {
	private static final Logger log = Logger.getLogger(LogisticsRepository.class);
	
	public boolean postMarketplace(String data) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, JsonParseException, JsonMappingException, IOException, Exception{
		boolean result;
		
		try {
			StringJoiner joiner = new StringJoiner(",");
			String joinedAccessorials = "";
			
			Map<String, Object> map = new ObjectMapper().readValue(data, new TypeReference<Map<String,Object>>(){});
			
			Gson gson = new Gson();
			Type datasetListType = new TypeToken<List<AccessorialCode>>(){}.getType();
			
			// gson.fromJson doesn't accept value with white space
			// convert to json string
			String newAcc = map.get("newAccessorials").toString().replaceAll("=", ":").toString();
			
			// replace all white space to other character
			newAcc = newAcc.replaceAll(" ", "__");
			
			// replace the json separator plus __ to proper json separator with space
			newAcc = newAcc.replaceAll(",__", ", ");

			List<AccessorialCode> accessorialCodeList = gson.fromJson(newAcc, datasetListType);
			accessorialCodeList.forEach(accessorial -> {
				int sequenceNumber = 0;
				try {
					sequenceNumber = this.getSequenceNbr();
					String accessorialCode = accessorial.getAccessorialCd() + sequenceNumber;
					
					accessorial.setAccessorialCd(accessorialCode);
					accessorial.setDescription(accessorial.getDescription().replaceAll("__", " "));
					this.createAccessorial(accessorial);
					
					joiner.add(accessorialCode);
				} catch (SQLFeatureNotSupportedException e) {
					log.error("SQLFeatureNotSupportedException error message: " + e, e);
				} catch (SQLTimeoutException e) {
					log.error("SQLTimeoutException error message: " + e, e);
				} catch (SQLException e) {
					log.error("SQLException error message: " + e, e);
				} catch (Exception e) {
					log.error("Exception error message: " + e, e);
				}
			});
			
			if((String) map.get("accessorials") != null || !((String) map.get("accessorials")).isEmpty()) {
				joiner.add((String) map.get("accessorials"));
			}
			
			joinedAccessorials = joiner.toString();
			
			if(joinedAccessorials.endsWith(",")) {
				joinedAccessorials = joinedAccessorials.substring(0, joinedAccessorials.lastIndexOf(",") - 1);
			}
			
			java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			CallableStatement pstmt = connection.prepareCall("call sp_TMS_to_MP_loader(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			pstmt.setString(1, (String) map.get("partnerCode"));
			pstmt.setString(2, (String) map.get("parentPartnerCode"));
			pstmt.setString(3, (String) map.get("shipperCode"));
			pstmt.setString(4, (String) map.get("customerCode"));
			pstmt.setString(5, (String) map.get("purchaseOrderNumber"));
			pstmt.setString(6, (String) map.get("releaseNumber"));
			pstmt.setInt(7, (Integer) map.get("paymentTerm"));
			pstmt.setDouble(8, new Double(((String) map.get("lowEndBid")).isEmpty() ? "0" : (String) map.get("lowEndBid")));
			pstmt.setDouble(9, new Double(((String) map.get("highEndBid")).isEmpty() ? "0" : (String) map.get("highEndBid")));
			pstmt.setDouble(10, new Double((String) map.get("targetRate")));
			pstmt.setString(11, (String) map.get("currencyCode"));
			pstmt.setTimestamp(12, new Timestamp(new Long((long) map.get("tenderExpirationDate"))));
			pstmt.setInt(13, (Integer) map.get("equipmentType"));
			pstmt.setString(14, (String) map.get("singleCommodityDescription"));
			pstmt.setDouble(15, new Double(((String) map.get("originWaitTime")).isEmpty() ? "0" : (String) map.get("originWaitTime")));
			pstmt.setString(16, (String) map.get("originWaitTimeUOM"));
			pstmt.setDouble(17, new Double(((String) map.get("destinationWaitTime")).isEmpty() ? "0" : (String) map.get("destinationWaitTime")));
			pstmt.setString(18, (String) map.get("destinationWaitTimeUOM"));
			pstmt.setDouble(19, new Double(((String) map.get("insuranceValue")).isEmpty() ? "0" : (String) map.get("insuranceValue")));
			pstmt.setString(20, joinedAccessorials);
			pstmt.executeUpdate();
			
			this.updateShipmentStatusToMp((String) map.get("partnerCode"), (String) map.get("parentPartnerCode"), 
					(String) map.get("shipperCode"), (String) map.get("customerCode"), (String) map.get("purchaseOrderNumber"), (String) map.get("releaseNumber"));
			 
			result = true;
		} catch (JsonParseException e) {
			log.error("JsonParseException error message: " + e, e);
			throw e;
		} catch (JsonMappingException e) {
			log.error("JsonMappingException error message: " + e, e);
			throw e;
		} catch (IOException e) {
			log.error("IOException error message: " + e, e);
			throw e;
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
		
		return result;
	}
	
	private void createAccessorial(AccessorialCode accessorial) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		String dataSource = "";
		String accessorialType = "";
		String sql = "insert into accessorial_code (customer_cd, parent_partner_cd, accessorial_cd, accessorial_type,"
				+ "accessorial_data_source, description, last_update, last_update_source) " 
				+ "values(" + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?)";

		try (java.sql.Connection connection = DbConfigRater.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			pstmt.setString(1, accessorial.getCustomerCd());
			pstmt.setString(2, accessorial.getParentPartnerCd());
			pstmt.setString(3, accessorial.getAccessorialCd());
			
			if (accessorial.getAccessorialType().startsWith("RPU")) {
				accessorialType = "RPU";
			} else {
				accessorialType = accessorial.getAccessorialType();
			}

			if (accessorial.getAccessorialType().equalsIgnoreCase("RPUH")) {
				dataSource = "Hour";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPUW")) {
				dataSource = "Weight";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPUV")) {
				dataSource = "Volume";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPUF")) {
				dataSource = "Floor";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPUS")) {
				dataSource = "Stop";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPUT")) {
				dataSource = "Toll";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPUD")) {
				dataSource = "Day";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPU12HR")) {
				dataSource = "12 HR";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("RPU30MIN")) {
				dataSource = "1/2 HR";
			} else if (accessorial.getAccessorialType().equalsIgnoreCase("FC")) {
				dataSource = "Fixed Charge";
			}
			
			pstmt.setString(4, accessorialType);
			pstmt.setString(5, dataSource);
			pstmt.setString(6, accessorial.getDescription());
			pstmt.setTimestamp(7, new Timestamp(new Date().getTime()));
			pstmt.setString(8, "MP");

			pstmt.execute();
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
	}
	
	private void updateShipmentStatusToMp(String partnerCode, String parentPartnerCode, String shipperCode, String customerCode, String poNumber, String releaseNumber)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		String sql = "UPDATE tms_utf8_db.po_header a SET a.INTERNAL_STATUS_CD = 'MP', a.LAST_EVENT = 'Posted to Marketplace', a.LAST_EVENT_DT = NOW() WHERE a.PARTNER_CD = ? AND a.PARENT_PARTNER_CD = ? AND a.SHIPPER_CD = ? AND a.CUSTOMER_CD = ? AND a.PO_NBR = ? AND a.RELEASE_NBR = ?";
	
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, partnerCode);
			pstmt.setString(2, parentPartnerCode);
			pstmt.setString(3, shipperCode);
			pstmt.setString(4, customerCode);
			pstmt.setString(5, poNumber);
			pstmt.setString(6, releaseNumber);
			pstmt.executeUpdate();
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
	}
	
	private Integer getSequenceNbr() throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		String sql = "insert into dbseq_shipment (create_tstamp,update_tstamp) values(now(),now())";

		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
		
		return null;
	}
	
	public List<ConsolidatedShipment> getConsoloditedShipemnts(String parent) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		String sql = "select z.*, a.pro_bill_nbr from ("
				+ " select sku.po_nbr as vpo,"
				+ " sku.ACTUAL_PO_NBR as po_nbr,"
				+ " sku.ACTUAL_PO_RELEASE_NBR as delivery_nbr,"
				+ " sum(sku.ORDERED_QTY) as qty,"
				+ " coalesce(unit.UOM_DESCRIPTION_TXT, sku.QTY_UOM) as uom,"
				+ " coalesce(dest.location_name, '') as location,"
				+ " a.last_event as status"
				+ " from tms_utf8_db.po_line_item sku"
				+ " inner join tms_utf8_db.po_reference_info dest on dest.po_nbr = sku.ACTUAL_PO_NBR"
				+ "    and dest.release_nbr = sku.ACTUAL_PO_RELEASE_NBR" 
				+ "    and dest.partner_cd = sku.partner_cd"
				+ "    and dest.parent_partner_cd = sku.parent_partner_cd"
				+ "    and dest.shipper_cd = sku.shipper_cd"
				+ "    and dest.customer_cd = sku.customer_cd"
				+ "    and dest.reference_type_cd = 'Delivery'"
				+ " left join marketplace_db.unit_of_measure unit on unit.uom_cd = sku.QTY_UOM"
				+ " left join tms_utf8_db.po_header a on a.po_nbr = sku.ACTUAL_PO_NBR"
				+ " where sku.po_nbr = ?"
				+ " group by sku.ACTUAL_PO_NBR, sku.ACTUAL_PO_RELEASE_NBR, sku.QTY_UOM, dest.location_name, a.last_event) z,"
				+ " tms_utf8_db.po_header a where a.release_nbr = z.delivery_nbr";
		
		List<ConsolidatedShipment> list = new ArrayList<ConsolidatedShipment>();
		try (java.sql.Connection connection = DbConfigTmsUtf8.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql);) {
			pstmt.setString(1, parent);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				while(rs.next()) {
					ConsolidatedShipment shipment = new ConsolidatedShipment();
					shipment.setVpo(rs.getString("vpo"));
					shipment.setPurchaseOrderNumber(rs.getString("po_nbr"));
					shipment.setDeliveryNumber(rs.getString("delivery_nbr"));
					shipment.setStatus(rs.getString("status"));
					shipment.setQuantity(rs.getInt("qty"));
					shipment.setUom(rs.getString("uom"));
					shipment.setLocation(rs.getString("location"));
					shipment.setProBill(rs.getString("pro_bill_nbr"));
					
					list.add(shipment);
				}
			}
			
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
		
		return list;
	}
	
	public List<LogisticsColumns> getDefaultColumns() throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		List<LogisticsColumns> list = new ArrayList<LogisticsColumns>();
		
		String sql = "select id, title, field, null as position, width, isShow from logistics_screen";
		
		try {
			java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql);

	        try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					LogisticsColumns logisticsColumns = new LogisticsColumns();
					logisticsColumns.setPosition(rs.getInt("position"));
					logisticsColumns.setTitle(rs.getString("title"));
					logisticsColumns.setField(rs.getString("field"));
					logisticsColumns.setWidth(rs.getInt("width"));
					logisticsColumns.setShow(rs.getBoolean("isShow"));
					
					list.add(logisticsColumns);
		        }
				return list;
	         }
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
	}
	
	public List<LogisticsColumns> getUserColumns(Integer userId) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		List<LogisticsColumns> list = new ArrayList<LogisticsColumns>();
		
		String sql = "SELECT ls.id, ls.title, ls.field, uls.position, " +
				"CASE WHEN (uls.user_id = ?) THEN uls.width ELSE ls.width END AS width, " +
				"CASE WHEN (uls.user_id = ?) THEN uls.isShow ELSE ls.isShow END AS isShow " +
			"FROM logistics_screen ls LEFT JOIN user_logistics_screen uls ON ls.id = uls.column_id AND uls.user_id = ? " +
			"ORDER BY CASE WHEN (uls.position = 0 AND uls.user_id = ?) THEN ls.id WHEN (uls.user_id = ?) THEN uls.position ELSE ls.id END ASC, ls.id ASC";
		
		try {
			java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, userId);
			pstmt.setInt(3, userId);
			pstmt.setInt(4, userId);
			pstmt.setInt(5, userId);

	        try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					LogisticsColumns logisticsColumns = new LogisticsColumns();
					logisticsColumns.setPosition(rs.getInt("position"));
					logisticsColumns.setTitle(rs.getString("title"));
					logisticsColumns.setField(rs.getString("field"));
					logisticsColumns.setWidth(rs.getInt("width"));
					logisticsColumns.setShow(rs.getBoolean("isShow"));
					
					list.add(logisticsColumns);
		        }
				return list;
	         }
		} catch (SQLFeatureNotSupportedException e) {
			log.error("SQLFeatureNotSupportedException error message: " + e, e);
			throw e;
		} catch (SQLTimeoutException e) {
			log.error("SQLTimeoutException error message: " + e, e);
			throw e;
		} catch (SQLException e) {
			log.error("SQLException error message: " + e, e);
			throw e;
		} catch (Exception e) {
			log.error("Exception error message: " + e, e);
			throw e;
		}
	}
	
	public void setUserColumns(Integer userId, List<LogisticsColumns> list) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException, Exception {
		String checkSql = "SELECT 1 FROM user_logistics_screen WHERE user_id = ? LIMIT 1";
		String deleteSql = "DELETE FROM user_logistics_screen WHERE user_id = ?";
		
		try {
			java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
			PreparedStatement checkPstmt = connection.prepareStatement(checkSql);
			PreparedStatement deletePstmt = connection.prepareStatement(deleteSql);
			
			checkPstmt.setInt(1, userId);

	        try (ResultSet checkRs = checkPstmt.executeQuery()) {
				if(checkRs.next()) {
					connection.setAutoCommit(false);
					deletePstmt.setInt(1, userId);
					deletePstmt.execute();
					this.insertUserColumns(userId, list, connection);
					connection.commit();
		        } else {
		        	connection.setAutoCommit(false);
		        	this.insertUserColumns(userId, list, connection);
					connection.commit();
		        }
	         }
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		
	}
	
	private void insertUserColumns(int userId, List<LogisticsColumns> list, java.sql.Connection connection) throws Exception {
		String sql = "insert into marketplace_db.user_logistics_screen(user_id, column_id, position, width, isShow) "
				+ "values (?, (select id from marketplace_db.logistics_screen where field = ?), ?, ?, ?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		
		Iterator<LogisticsColumns> iter = list.iterator();
		while(iter.hasNext()) {
			LogisticsColumns col = iter.next();
			
			pstmt.setInt(1, userId);
			pstmt.setString(2, col.getField());
			pstmt.setInt(3, col.getPosition());
			pstmt.setInt(4, col.getWidth());
			pstmt.setBoolean(5, col.isShow());
			pstmt.addBatch();
		}
		
		pstmt.executeBatch();
	}
	
}
