/**
 * 
 */
package com.openport.marketplace.repository;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * @author oscar_2
 *
 */
public class DbConfigRater {

	static final transient Logger log = Logger.getLogger(DbConfigRater.class);
	
	private static DataSource ds;
	
	static {
		try {
			log.info("Initializing DataSource");
			Context initContext = new InitialContext();
			ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/AbsRaterUtfDB");
			log.info("Rater DataSource initialized");
		} catch (NamingException ex) {
			log.error(ex, ex);
		}
	}


	/**
	 * 
	 */
	private DbConfigRater() {
		// TODO Auto-generated constructor stub
	}


	public static DataSource getDataSource() {
		return ds;
	}


}
