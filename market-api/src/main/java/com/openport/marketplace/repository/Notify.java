/**
 * 
 */
package com.openport.marketplace.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.ServiceException;

/**
 * @author oscar_2
 *
 */
public class Notify {

	static final transient Logger log = Logger.getLogger(Notify.class);

	/**
	 * 
	 */
	public Notify() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static boolean logEmail(String sendTo, String subject, String message) {
		return logEmail("Do-No-Reply@OpenPort.com", sendTo, subject, message);
	}
	public static boolean logEmail(String sender, String sendTo, String subject, String message) {
		String sql = "INSERT INTO marketplace_db.email_log (sender, sendTo, subject, message, Create_Dt) "
				+ " VALUES (?, ?, ?, ?, now()); ";
		try (java.sql.Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql );) {
			pstmt.setString(1, sender);
			pstmt.setString(2, sendTo);
			pstmt.setString(3, subject);
			pstmt.setString(4, message);
			if (pstmt.executeUpdate()==1) {
				return true;
			}
		} catch (SQLException e1) {
			log.error("Error message: " + e1, e1);
			new ServiceException(Response.Status.CONFLICT);
		}
		return false;
	}

	public static Response sendEmail(String sendTo, String subject, String message) {
		Notify.logEmail(sendTo, subject, message);
		return MailGun.sendMessage(sendTo, subject, message);
	}

	public static Response sendEmail(String sender, String sendTo, String subject, String message) {
		Notify.logEmail(sender, sendTo, subject, message);
		return MailGun.sendMessage(sender, sendTo, subject, message);
	}

}
