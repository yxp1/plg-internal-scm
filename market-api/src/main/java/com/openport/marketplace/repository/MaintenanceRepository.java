package com.openport.marketplace.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openport.marketplace.json.Carrier;
import com.openport.marketplace.json.RewardsPointsConfig;
import com.openport.marketplace.json.LongHaulReward;
import com.openport.marketplace.json.User;
import com.openport.util.db.ResultSetMapper;

public class MaintenanceRepository {
	static final transient Logger log = Logger.getLogger(MaintenanceRepository.class);
	
	private static final int REWARDS_CONVERSION_FLAG = 0; 
	private static final int VPO_REWARD_ROLLUP_FLAG = 1;
	private static final int PINGCOUNT_FLAG = 2;
	
	public List<Carrier> getShipperCarriers(String providerCd)throws SQLException{		
		StringBuilder query = new StringBuilder();
		query
		.append("SELECT CARRIER_CD, cp.CARRIER_NAME ")
		.append("FROM tms_utf8_db.carrier_profile cp ")
		.append("JOIN marketplace_db.company c ON c.company_code = cp.CARRIER_CD ")
		.append("WHERE cp.PARENT_PARTNER_CD = ?");

		List<Carrier> carrierList = null;
		log.debug("SQL: "+query.toString());
		try(
				Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
				PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, providerCd);
			ResultSet rs = p.executeQuery();
			ResultSetMapper<Carrier> objectRawMapper = new ResultSetMapper<Carrier>();
			carrierList = objectRawMapper.mapResultSetToObject(rs, Carrier.class);
		}
		log.info("---  END METHOD CALL ---");
		return carrierList;
	}
		
	public Integer getLatestSeqNbr(Integer configFlag) throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT PARAMETER_SEQ_NBR as latest FROM tms_utf8_db.tms_parameter ");
		switch(configFlag) {
			case REWARDS_CONVERSION_FLAG:
				query.append("WHERE PARAMETER_CD = 'REWARDS_CONVERSION' AND PARAMETER_NAME = 'CONVERSION' ORDER BY PARAMETER_SEQ_NBR DESC LIMIT 1;");
				break;
			case VPO_REWARD_ROLLUP_FLAG:
				query.append("WHERE PARAMETER_CD = 'VPO_REWARD_ROLLUP' AND PARAMETER_NAME = 'VPO_SHIPPER_CARRIER' ORDER BY PARAMETER_SEQ_NBR DESC LIMIT 1;");
				break;
			case PINGCOUNT_FLAG:
				query.append("WHERE PARAMETER_CD = 'PINGCOUNT' AND PARAMETER_NAME = 'PINGCOUNT' ORDER BY PARAMETER_SEQ_NBR DESC LIMIT 1;");
				break;
		}
		

		Integer latest = null;
		log.debug("SQL: "+query.toString());
		try(
				Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
				PreparedStatement p = conn.prepareStatement(query.toString());
		){
			ResultSet rs = p.executeQuery();
			if(rs!=null){
				 while (rs.next()) {
					 latest = rs.getInt("latest");
				 }
			}
		}
		log.info("---  END METHOD CALL ---");
		return latest;
	}
	
	public List<RewardsPointsConfig> getRewardsConfig(String providerCd)throws SQLException{		
		StringBuilder query = new StringBuilder();
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id, ")
		.append("cp.CARRIER_NAME as carrier_cd, ")		
		.append("PARAMETER_1_VALUE as event, PARAMETER_1_DESC_TXT as description, PARAMETER_2_VALUE as points, ")
		.append("PARAMETER_4_VALUE as default_status, EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("JOIN  tms_utf8_db.carrier_profile  cp ON cp.CARRIER_CD = PARAMETER_3_VALUE AND cp.PARENT_PARTNER_CD = ?")
		.append("WHERE PARAMETER_CD = 'REWARDS_CONVERSION' AND PARAMETER_NAME = 'CONVERSION' AND PROGRAM_ID = ? ORDER BY cp.CARRIER_NAME ASC) tblmain");

		List<RewardsPointsConfig> rewardsPointsConfigs = null;
		log.debug("SQL: "+query.toString());
		try(
				Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
				PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, providerCd);
			p.setString(2, providerCd);
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			rewardsPointsConfigs = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
		}
		log.info("---  END METHOD CALL ---");
		return rewardsPointsConfigs;
	}
	
	public RewardsPointsConfig findRewardsConfigByFields(RewardsPointsConfig rewardsPointsConfig)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id,  c.company_id as shipper_id,  PROGRAM_ID as shipper_cd,  c2.company_id as carrier_id, PARAMETER_3_VALUE as carrier_cd, ")
		.append("PARAMETER_1_VALUE as event, PARAMETER_1_DESC_TXT as description, PARAMETER_2_VALUE as points, ")
		.append("PARAMETER_4_VALUE as default_status, EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("JOIN marketplace_db.company c ON c.company_code = PROGRAM_ID ")
		.append("JOIN marketplace_db.company c2 ON c2.company_code = PARAMETER_3_VALUE ")
		.append("WHERE PARAMETER_CD = 'REWARDS_CONVERSION' AND PARAMETER_NAME = 'CONVERSION' ")
		.append("AND PARAMETER_3_VALUE = ? ")
		.append("AND PARAMETER_1_VALUE = ? AND PARAMETER_1_DESC_TXT = ? ")
		.append("AND PARAMETER_4_VALUE = ? AND EFFECTIVE_DT = ? AND TERMINATION_DT = ? ")
		.append(" ) tblmain");
		
		
		List<RewardsPointsConfig> rewardsPointsConfigs = null;
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, rewardsPointsConfig.getCarrierCd());
			p.setString(2, rewardsPointsConfig.getEvent());
			p.setString(3, rewardsPointsConfig.getDescription());
			p.setString(4, rewardsPointsConfig.getDefaultStatus());
			p.setDate(5, rewardsPointsConfig.getStartDate());
			p.setDate(6, rewardsPointsConfig.getTerminationDate());
			
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			rewardsPointsConfigs = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
			
			if(rewardsPointsConfigs != null)
				return rewardsPointsConfigs.get(0);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return null;
	}
	
	public RewardsPointsConfig findRewardsConfigById(Integer id)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id,  c.company_id as shipper_id,  PROGRAM_ID as shipper_cd, PARAMETER_3_VALUE as carrier_cd, ")
		.append("PARAMETER_1_VALUE as event, PARAMETER_1_DESC_TXT as description, PARAMETER_2_VALUE as points, PARAMETER_4_VALUE as default_status, ")
		.append("EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("LEFT JOIN marketplace_db.company c ON c.company_code = PROGRAM_ID ")
		.append("LEFT JOIN marketplace_db.company c2 ON c2.company_code = PARAMETER_3_VALUE ")
		.append("WHERE PARAMETER_CD = 'REWARDS_CONVERSION' AND PARAMETER_NAME = 'CONVERSION' AND tms_parameter.PARAMETER_SEQ_NBR = ? ) tblmain");
		
		
		List<RewardsPointsConfig> rewardsPointsConfigs = null;
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setInt(1, id);
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			rewardsPointsConfigs = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
			
			if(rewardsPointsConfigs != null)
				return rewardsPointsConfigs.get(0);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return null;
	}
	
	
	
	public Integer createRewardConfig(String currentUserName, RewardsPointsConfig rewardsPointsConfig, Integer configFlag) throws SQLException{
		
		StringBuilder query = new StringBuilder();
		
		query
		.append("INSERT INTO tms_utf8_db.tms_parameter ")
		.append("(PARAMETER_CD,PARAMETER_SEQ_NBR,PARAMETER_NAME,PROGRAM_ID,REQUIRED_FLG,PARAMETER_1_VALUE, ")
		.append("PARAMETER_1_DESC_TXT,PARAMETER_2_VALUE,PARAMETER_2_DESC_TXT,PARAMETER_3_VALUE,PARAMETER_3_DESC_TXT,PARAMETER_4_VALUE,PARAMETER_4_DESC_TXT, ")
		.append("EFFECTIVE_DT,TERMINATION_DT,CREATE_USER_ID,CREATE_TSTAMP) ")
		.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())");
		
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			switch(configFlag) {
				case REWARDS_CONVERSION_FLAG:					
					p.setString(1, "REWARDS_CONVERSION");
					p.setInt(2, rewardsPointsConfig.getId());
					p.setString(3, "CONVERSION");
					p.setString(4, rewardsPointsConfig.getShipperCd());
					p.setString(5, "Y");
					p.setString(6, rewardsPointsConfig.getEvent());
					p.setString(7, rewardsPointsConfig.getDescription());
					p.setInt(8, rewardsPointsConfig.getPoints());
					p.setString(9, "POINTS EQUIVALENT");
					p.setString(10, rewardsPointsConfig.getCarrierCd());
					p.setString(11, "");
					p.setString(12, rewardsPointsConfig.getDefaultStatus());
					p.setString(13, "");
					p.setDate(14, rewardsPointsConfig.getStartDate());
					p.setDate(15, rewardsPointsConfig.getTerminationDate());
					p.setString(16, currentUserName.toLowerCase());
					
					break;
					
				case VPO_REWARD_ROLLUP_FLAG:					
					p.setString(1, "VPO_REWARD_ROLLUP");
					p.setInt(2, rewardsPointsConfig.getId());
					p.setString(3, "VPO_SHIPPER_CARRIER");
					p.setString(4, "BATCH_REWARDER");
					p.setString(5, "Y");
					p.setString(6, rewardsPointsConfig.getShipperCd());
					p.setString(7, "Shipper Code");
					p.setString(8, rewardsPointsConfig.getCarrierCd());
					p.setString(9, "Carrier Code");
					p.setString(10, null);
					p.setString(11, null);
					p.setString(12, null);
					p.setString(13, null);
					p.setDate(14, rewardsPointsConfig.getStartDate());
					p.setDate(15, rewardsPointsConfig.getTerminationDate());
					p.setString(16, currentUserName.toLowerCase());
					
					break;
					
				case PINGCOUNT_FLAG:
					p.setString(1, "PINGCOUNT");
					p.setInt(2, rewardsPointsConfig.getId());
					p.setString(3, "PINGCOUNT");
					p.setString(4, "PINGCOUNT");
					p.setString(5, "Y");
					p.setInt(6, rewardsPointsConfig.getPingcount());
					p.setString(7, "number of pings");
					p.setString(8, rewardsPointsConfig.getCarrierCd());
					p.setString(9, "scaccd_carrier");
					p.setString(10, rewardsPointsConfig.getShipperCd());
					p.setString(11, "scaccd_shipper");
					p.setString(12, "");
					p.setString(13, "");
					p.setDate(14, rewardsPointsConfig.getStartDate());
					p.setDate(15, rewardsPointsConfig.getTerminationDate());
					p.setString(16, currentUserName.toLowerCase());
					
					break;
			}
			
			
			
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public Integer updateRewardConfig(String currentUserName, Integer id, RewardsPointsConfig rewardPointsConfig) throws SQLException{
		
		StringBuilder query = new StringBuilder();
		
		query
		.append("UPDATE tms_utf8_db.tms_parameter SET PARAMETER_2_VALUE = ?, PARAMETER_4_VALUE = ?, EFFECTIVE_DT = ?, TERMINATION_DT = ?, ")
		.append("UPDATE_USER_ID = ?, UPDATE_TSTAMP = NOW() ")
		.append("WHERE PARAMETER_CD = 'REWARDS_CONVERSION' AND PARAMETER_NAME = 'CONVERSION' AND PARAMETER_SEQ_NBR = ? ");
		
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, String.valueOf(rewardPointsConfig.getPoints()));
			p.setString(2, rewardPointsConfig.getDefaultStatus());
			p.setDate(3, rewardPointsConfig.getStartDate());
			p.setDate(4, rewardPointsConfig.getTerminationDate());
			p.setString(5, currentUserName.toLowerCase());
			p.setString(6, String.valueOf(id));
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public Integer deleteRewardConfig(Integer id)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query.append("DELETE FROM tms_utf8_db.tms_parameter ")
		.append("WHERE PARAMETER_CD = 'REWARDS_CONVERSION' AND PARAMETER_NAME = 'CONVERSION' AND PARAMETER_SEQ_NBR = ? ");
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setInt(1, id);
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public List<Map<String,Object>> getRewardsValue(String currency)throws SQLException{		
		StringBuilder query = new StringBuilder();
		query
		.append("SELECT * FROM (SELECT PARAMETER_2_VALUE as 'currency', PARAMETER_3_VALUE as 'conversion_rate', ")
		.append("PARAMETER_4_VALUE as 'min_claimable_amt', EFFECTIVE_DT as 'start_date', TERMINATION_DT as 'termination_date' ") 
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("WHERE PARAMETER_CD = 'REWARDS_VALUE' and PARAMETER_1_VALUE = ?) tblmain");

		List<Map<String,Object>> rewardsValueList = new LinkedList<Map<String,Object>>();
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();	
			PreparedStatement p = conn.prepareStatement(query.toString());			
		){
			p.setString(1, currency);
			ResultSet rs = p.executeQuery();
			
			

			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			while(rs.next()){
				Map<String,Object> rowData = new LinkedHashMap<String,Object>();
				for (int i = 1; i <= columnCount; i++ ) {
				  String columnLabel = rsmd.getColumnLabel(i);
				  if(columnLabel.contains("date")) {
					  rowData.put(columnLabel, rs.getDate(columnLabel));
				  }else {
					  rowData.put(columnLabel, rs.getObject(columnLabel));
				  }
				}
				rewardsValueList.add(rowData);
			}
		}
		log.info("---  END METHOD CALL ---");
		return rewardsValueList;
	}
	
	public List<Map<String,Object>> getPaymentMethodStatus()throws SQLException{		
		StringBuilder query = new StringBuilder();
		query
		.append("SELECT * FROM (SELECT PARAMETER_1_DESC_TXT as 'payment_method', PARAMETER_2_VALUE as 'activation_status', ") 
		.append("EFFECTIVE_DT as 'start_date',  TERMINATION_DT as 'termination_date' ")
		.append("FROM tms_utf8_db.tms_parameter WHERE PARAMETER_CD = 'MICRO_REWARDS' and PARAMETER_NAME = 'PAYMENT_TYPE') tblmain");

		List<Map<String,Object>>  dataResultMap = new LinkedList<Map<String,Object>>();
		log.debug("SQL: "+query.toString());
		try(
				Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
				PreparedStatement p = conn.prepareStatement(query.toString());
				ResultSet rs = p.executeQuery();
		){
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			while(rs.next()){
				Map<String,Object> rowData = new LinkedHashMap<String,Object>();
				for (int i = 1; i <= columnCount; i++ ) {
				  String columnLabel = rsmd.getColumnLabel(i);
				  if(columnLabel.contains("date")) {
					  rowData.put(columnLabel, rs.getDate(columnLabel));
				  }else {
					  rowData.put(columnLabel, rs.getObject(columnLabel));
				  }
				}
				dataResultMap.add(rowData);
			}
		}
		log.info("---  END METHOD CALL ---");
		return dataResultMap;
	}
	
	public List<RewardsPointsConfig> getVPORewardsRollup(String providerCd)throws SQLException{		
		StringBuilder query = new StringBuilder();
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id, ")
		.append("cp.CARRIER_NAME as carrier_cd, ")
		.append("EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("JOIN  tms_utf8_db.carrier_profile  cp ON cp.CARRIER_CD = PARAMETER_2_VALUE AND cp.PARENT_PARTNER_CD = ?")
		.append("WHERE PARAMETER_CD = 'VPO_REWARD_ROLLUP' AND PARAMETER_NAME = 'VPO_SHIPPER_CARRIER' AND PARAMETER_1_VALUE = ? ORDER BY cp.CARRIER_NAME ASC) tblmain");

		
		List<RewardsPointsConfig> vpoRewardRollupList = null;
		log.debug("SQL: "+query.toString());
		try(
				Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
				PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, providerCd);
			p.setString(2, providerCd);
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			vpoRewardRollupList = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
		}
		log.info("---  END METHOD CALL ---");
		return vpoRewardRollupList;
	}
	
	public Integer updateVPORewardRollup(String currentUserName, Integer id, RewardsPointsConfig vpoRewardRollup) throws SQLException{
		
		StringBuilder query = new StringBuilder();
		
		query
		.append("UPDATE tms_utf8_db.tms_parameter SET EFFECTIVE_DT = ?, TERMINATION_DT = ?, UPDATE_USER_ID = ?, UPDATE_TSTAMP = NOW() ")
		.append("WHERE PARAMETER_CD = 'VPO_REWARD_ROLLUP' AND PARAMETER_NAME = 'VPO_SHIPPER_CARRIER' AND PARAMETER_SEQ_NBR = ? ");
		
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setDate(1, vpoRewardRollup.getStartDate());
			p.setDate(2, vpoRewardRollup.getTerminationDate());
			p.setString(3, currentUserName.toLowerCase());
			p.setString(4, String.valueOf(id));
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public Integer deleteVPORewardRollup(Integer id)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query.append("DELETE FROM tms_utf8_db.tms_parameter ")
		.append("WHERE PARAMETER_CD = 'VPO_REWARD_ROLLUP' AND PARAMETER_NAME = 'VPO_SHIPPER_CARRIER' AND PARAMETER_SEQ_NBR = ? ");
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setInt(1, id);
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public RewardsPointsConfig findVPORewardRollupById(Integer id)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id,  c.company_id as shipper_id,  PARAMETER_1_VALUE as shipper_cd, PARAMETER_2_VALUE as carrier_cd, ")
		.append("EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("LEFT JOIN marketplace_db.company c ON c.company_code = PARAMETER_1_VALUE ")
		.append("LEFT JOIN marketplace_db.company c2 ON c2.company_code = PARAMETER_2_VALUE ")
		.append("WHERE PARAMETER_CD = 'VPO_REWARD_ROLLUP' AND PARAMETER_NAME = 'VPO_SHIPPER_CARRIER' AND tms_parameter.PARAMETER_SEQ_NBR = ?) tblmain");
		
		
		List<RewardsPointsConfig> rewardsPointsConfigs = null;
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setInt(1, id);
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			rewardsPointsConfigs = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
			
			if(rewardsPointsConfigs != null)
				return rewardsPointsConfigs.get(0);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return null;
	}
	
	public RewardsPointsConfig findVPORewardRollupByFields(RewardsPointsConfig rewardsPointsConfig)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id,  c.company_id as shipper_id,  PARAMETER_1_VALUE as shipper_cd, PARAMETER_2_VALUE as carrier_cd, ")
		.append("EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("LEFT JOIN marketplace_db.company c ON c.company_code = PARAMETER_1_VALUE ")
		.append("LEFT JOIN marketplace_db.company c2 ON c2.company_code = PARAMETER_2_VALUE ")
		.append("WHERE PARAMETER_CD = 'VPO_REWARD_ROLLUP' AND PARAMETER_NAME = 'VPO_SHIPPER_CARRIER' ")
		.append("AND PARAMETER_2_VALUE = ? AND EFFECTIVE_DT = ? AND TERMINATION_DT = ? ")
		.append(") tblmain");
		
		
		List<RewardsPointsConfig> rewardsPointsConfigs = null;
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, rewardsPointsConfig.getCarrierCd());
			p.setDate(2, rewardsPointsConfig.getStartDate());
			p.setDate(3, rewardsPointsConfig.getTerminationDate());
			
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			rewardsPointsConfigs = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
			
			if(rewardsPointsConfigs != null)
				return rewardsPointsConfigs.get(0);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return null;
	}
	
	public List<RewardsPointsConfig> getPingcounts(String providerCd)throws SQLException{		
		StringBuilder query = new StringBuilder();
		query.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id, ")
		.append("cp.CARRIER_NAME as carrier_cd, ")
		.append("PARAMETER_1_VALUE as pingcount, EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("JOIN  tms_utf8_db.carrier_profile  cp ON cp.CARRIER_CD = PARAMETER_2_VALUE AND cp.PARENT_PARTNER_CD = ?")
		.append("WHERE PARAMETER_CD = 'PINGCOUNT' AND PARAMETER_NAME = 'PINGCOUNT' AND PARAMETER_3_VALUE = ? ORDER BY PARAMETER_2_VALUE ASC) tblmain");

		
		List<RewardsPointsConfig> pingcount = null;
		log.debug("SQL: "+query.toString());
		try(
				Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
				PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, providerCd);
			p.setString(2, providerCd);
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			pingcount = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
		}
		log.info("---  END METHOD CALL ---");
		return pingcount;
	}
	
	public Integer updatePingcount(String currentUserName, Integer id, RewardsPointsConfig rewardPointsConfig) throws SQLException{
		
		StringBuilder query = new StringBuilder();
		
		query
		.append("UPDATE tms_utf8_db.tms_parameter SET PARAMETER_1_VALUE = ?, EFFECTIVE_DT = ?, TERMINATION_DT = ?, ")
		.append("UPDATE_USER_ID = ?, UPDATE_TSTAMP = NOW() ")
		.append("WHERE PARAMETER_CD = 'PINGCOUNT' AND PARAMETER_NAME = 'PINGCOUNT' AND PARAMETER_SEQ_NBR = ?");
		
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, String.valueOf(rewardPointsConfig.getPingcount()));
			p.setDate(2, rewardPointsConfig.getStartDate());
			p.setDate(3, rewardPointsConfig.getTerminationDate());
			p.setString(4, currentUserName.toLowerCase());
			p.setString(5, String.valueOf(id));
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public RewardsPointsConfig findPingCountByFields(RewardsPointsConfig rewardsPointsConfig)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id,  c.company_id as shipper_id,  PARAMETER_3_VALUE as shipper_cd,  ")
		.append("PARAMETER_2_VALUE as carrier_cd, PARAMETER_1_VALUE as pingcount, ")
		.append("EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("LEFT JOIN marketplace_db.company c ON c.company_code = PARAMETER_3_VALUE ")
		.append("LEFT JOIN marketplace_db.company c2 ON c2.company_code = PARAMETER_2_VALUE ")
		.append("WHERE PARAMETER_CD = 'PINGCOUNT' AND PARAMETER_NAME = 'PINGCOUNT' ")
		.append("AND PARAMETER_2_VALUE = ? AND PARAMETER_1_VALUE = ? ")
		.append("AND EFFECTIVE_DT = ? AND TERMINATION_DT = ? ")
		.append(") tblmain");
		
		
		List<RewardsPointsConfig> rewardsPointsConfigs = null;
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, rewardsPointsConfig.getCarrierCd());
			p.setInt(2, rewardsPointsConfig.getPingcount());
			p.setDate(3, rewardsPointsConfig.getStartDate());
			p.setDate(4, rewardsPointsConfig.getTerminationDate());
			
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			rewardsPointsConfigs = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
			
			if(rewardsPointsConfigs != null)
				return rewardsPointsConfigs.get(0);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return null;
	}
	
	public RewardsPointsConfig findPingcountById(Integer id)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM (SELECT PARAMETER_SEQ_NBR as id,  c.company_id as shipper_id,  PARAMETER_3_VALUE as shipper_cd,  ")
		.append("PARAMETER_2_VALUE as carrier_cd, PARAMETER_1_VALUE as pingcount, ")
		.append("EFFECTIVE_DT as start_date, TERMINATION_DT as termination_date ")
		.append("FROM tms_utf8_db.tms_parameter ")
		.append("LEFT JOIN marketplace_db.company c ON c.company_code = PARAMETER_3_VALUE ")
		.append("LEFT JOIN marketplace_db.company c2 ON c2.company_code = PARAMETER_2_VALUE ")
		.append("WHERE PARAMETER_CD = 'PINGCOUNT' AND PARAMETER_NAME = 'PINGCOUNT' AND tms_parameter.PARAMETER_SEQ_NBR = ? ) tblmain");
		
		
		List<RewardsPointsConfig> rewardsPointsConfigs = null;
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setInt(1, id);
			ResultSet rs = p.executeQuery();
			ResultSetMapper<RewardsPointsConfig> objectRawMapper = new ResultSetMapper<RewardsPointsConfig>();
			rewardsPointsConfigs = objectRawMapper.mapResultSetToObject(rs, RewardsPointsConfig.class);
			
			if(rewardsPointsConfigs != null)
				return rewardsPointsConfigs.get(0);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return null;
	}
	
	public Integer deletePingcount(Integer id)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query.append("DELETE FROM tms_utf8_db.tms_parameter ")
		.append("WHERE PARAMETER_CD = 'PINGCOUNT' AND PARAMETER_NAME = 'PINGCOUNT' AND PARAMETER_SEQ_NBR = ? ");
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setInt(1, id);
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	
	public List<LongHaulReward> getLongHaulRewards(String countryCd) throws SQLException{
		List<LongHaulReward> longHaulList = null;
		StringBuilder query = new StringBuilder();		
		query.append("SELECT * FROM tms_delivery_db.route WHERE origin_country_cd = ? ");
		
		log.debug("SQL: "+query.toString());
		
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, countryCd);
			ResultSet rs = p.executeQuery();
			
			ResultSetMapper<LongHaulReward> objectRawMapper = new ResultSetMapper<LongHaulReward>();
			longHaulList = objectRawMapper.mapResultSetToObject(rs, LongHaulReward.class);
		}finally {
			log.info("---  END METHOD CALL ---");
		}
		return longHaulList;
	}
	
	public Integer createLongHaul(LongHaulReward longHaul) throws SQLException{
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO tms_delivery_db.route ")
		.append("(origin_city, origin_province, origin_postal_cd, origin_country_cd, dest_city, dest_province, dest_postal_cd, dest_country_cd, ")
		.append("distance, distance_uom, duration, duration_uom, long_haul_cnt, create_user_id, create_dt, update_user_id, update_dt) ")
		.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, longHaul.getOriginCity());
			p.setString(2, longHaul.getOriginProvince());
			p.setString(3, longHaul.getOriginPostalCd());
			p.setString(4, longHaul.getOriginCountryCd());
			p.setString(5, longHaul.getDestCity());
			p.setString(6, longHaul.getDestProvince());
			p.setString(7, longHaul.getDestPostalCd());
			p.setString(8, longHaul.getDestCountryCd());
			p.setDouble(9, longHaul.getDistance());
			p.setString(10, longHaul.getDistanceUom());
			p.setDouble(11, longHaul.getDuration());
			p.setString(12, longHaul.getDurationUom());
			p.setInt(13, longHaul.getLongHaulCnt());
			p.setString(14, longHaul.getUpdateUserId());
			p.setDate(15, longHaul.getUpdateDt());
			p.setString(16, longHaul.getUpdateUserId());
			p.setDate(17, longHaul.getUpdateDt());
			
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public LongHaulReward findLongHaulByFields(LongHaulReward longHaulReward)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query
		.append("SELECT * FROM tms_delivery_db.route WHERE ")
		.append("origin_city = ? AND origin_province = ? AND origin_postal_cd = ? AND origin_country_cd = ? ")
		.append("AND dest_city = ? AND dest_province = ? AND dest_postal_cd = ? AND dest_country_cd = ? ");
		
		
		List<LongHaulReward> longHaul = null;
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, longHaulReward.getOriginCity());
			p.setString(2, longHaulReward.getOriginProvince());
			p.setString(3, longHaulReward.getOriginPostalCd());
			p.setString(4, longHaulReward.getOriginCountryCd());
			p.setString(5, longHaulReward.getDestCity());
			p.setString(6, longHaulReward.getDestProvince());
			p.setString(7, longHaulReward.getDestPostalCd());
			p.setString(8, longHaulReward.getDestCountryCd());
			
			ResultSet rs = p.executeQuery();
			ResultSetMapper<LongHaulReward> objectRawMapper = new ResultSetMapper<LongHaulReward>();
			longHaul = objectRawMapper.mapResultSetToObject(rs, LongHaulReward.class);
			
			if(longHaul != null)
				return longHaul.get(0);
		}finally{
			log.info("---  END METHOD CALL ---");
		}
		return null;
	}
	
	public Integer updateLongHaulItem(LongHaulReward longHaul) throws SQLException{
		StringBuilder query = new StringBuilder();
		query.append("UPDATE tms_delivery_db.route SET distance = ?, distance_uom = ?, duration = ?, duration_uom = ?, long_haul_cnt = ?, ")
		.append("update_user_id = ?, update_dt = NOW() ")
		.append("WHERE origin_city = ? AND origin_province = ? AND origin_postal_cd = ? AND origin_country_cd = ? ")
		.append("AND dest_city = ? AND dest_province = ? AND dest_postal_cd = ? AND dest_country_cd = ? ");
		
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setDouble(1, longHaul.getDistance());
			p.setString(2, longHaul.getDistanceUom());
			p.setDouble(3, longHaul.getDuration());
			p.setString(4, longHaul.getDurationUom());
			p.setInt(5, longHaul.getLongHaulCnt());
			p.setString(6,  longHaul.getUpdateUserId());				
			p.setString(7, longHaul.getOriginCity());
			p.setString(8, longHaul.getOriginProvince());
			p.setString(9, longHaul.getOriginPostalCd());
			p.setString(10, longHaul.getOriginCountryCd());
			p.setString(11, longHaul.getDestCity());
			p.setString(12, longHaul.getDestProvince());
			p.setString(13, longHaul.getDestPostalCd());
			p.setString(14, longHaul.getDestCountryCd());
			
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public Integer deleteLongHaulItem(LongHaulReward longHaul)throws SQLException{
		StringBuilder query = new StringBuilder();
		
		query.append("DELETE FROM tms_delivery_db.route ")
		.append("WHERE origin_city = ? AND origin_province = ? AND origin_postal_cd = ? AND origin_country_cd = ? ")
		.append("AND dest_city = ? AND dest_province = ? AND dest_postal_cd = ? AND dest_country_cd = ? ");
		log.debug("SQL: "+query.toString());
		try(
			Connection conn = DbConfigTmsDelivery.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());
		){
			p.setString(1, longHaul.getOriginCity());
			p.setString(2, longHaul.getOriginProvince());
			p.setString(3, longHaul.getOriginPostalCd());
			p.setString(4, longHaul.getOriginCountryCd());
			p.setString(5, longHaul.getDestCity());
			p.setString(6, longHaul.getDestProvince());
			p.setString(7, longHaul.getDestPostalCd());
			p.setString(8, longHaul.getDestCountryCd());
			return p.executeUpdate();
		}finally{
			log.info("---  END METHOD CALL ---");
		}
	}
	
	public String getLocationApiUrl() throws SQLException{
		StringBuilder query = new StringBuilder();
		query.append("SELECT PARAMETER_1_VALUE FROM tms_utf8_db.tms_parameter WHERE PARAMETER_CD = 'PARAM_LOCATION_API' ");
		query.append("AND PROGRAM_ID = 'LOCATION_API_URL' ");
		query.append("AND PARAMETER_NAME = 'LOCATION_API_URL' ");
		
		String url = null;
		
		try(
			Connection conn = DbConfigTmsUtf8.getDataSource().getConnection();
			PreparedStatement p = conn.prepareStatement(query.toString());			
		){
			ResultSet rs = p.executeQuery();
			if(rs!=null){
				 while (rs.next()) {
					 url = rs.getString("PARAMETER_1_VALUE");
				 }
			}
						
		}finally {
			log.info("---  END METHOD CALL ---");
		}
		
		return url;
	}
}
