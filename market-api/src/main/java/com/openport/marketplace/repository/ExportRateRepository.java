package com.openport.marketplace.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ExportRateRepository {

	static final transient Logger log = Logger.getLogger(ExportRateRepository.class);
	
	public List<Map<String,Object>> executeExportRatesQuery(List<String> tenderIds) throws SQLException {
		List<Map<String,Object>> dataResultMap =  new LinkedList<Map<String,Object>>();
		
		if(!tenderIds.isEmpty()){
			StringBuilder query = new StringBuilder();
			query.append("select *, ")
			.append("DATE_FORMAT(cct.CONTRACT_EFFECTIVE_DT, '%d/%m/%Y %H:%i:%s') as EFFECTIVE_DT, ")
			.append("DATE_FORMAT(cct.CONTRACT_TERMINATION_DT, '%d/%m/%Y %H:%i:%s') as TERMINATION_DT ")
			.append("from company_contract_tender_lane cctl ")
			.append("inner join company shipper on shipper.Company_ID = cctl.SHIPPER_COMPANY_ID ")
			.append("left join company carrier on carrier.Company_ID = cctl.CARRIER_COMPANY_ID ")
			.append("inner join lane l on l.lane_id = cctl.LANE_ID ")
			.append("inner join commodity_code commodity ON commodity.COMMODITY_CD = cctl.COMMODITY_CD ")
			.append("inner join rate_per_unit rpu on rpu.RATE_PER_UNIT_CD = cctl.RATE_PER_UNIT_CD ")
			.append("inner join equipment_type et ON et.Equipment_Type_ID = cctl.EQUIPMENT_TYPE_ID ")
			.append("inner join commodity_code cc ON cc.COMMODITY_CD = cctl.COMMODITY_CD ")
			.append("left join company_contract_tender cct on cct.CONTRACT_NBR = cctl.CONTRACT_NBR ")
			.append("WHERE cctl.COMPANY_CONTRACT_TENDER_LANE_ID IN ("+StringUtils.join(tenderIds.toArray(),",")+") ");
	
			try(java.sql.Connection conn = DbConfigMarketPlace.getDataSource().getConnection()){
				PreparedStatement p = conn.prepareStatement(query.toString());
				ResultSet rs = p.executeQuery();
	
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				while(rs.next()){
					Map<String,Object> rowData = new LinkedHashMap<String,Object>();
					for (int i = 1; i <= columnCount; i++ ) {
					  String columnLabel = rsmd.getColumnLabel(i);
					  rowData.put(columnLabel, rs.getObject(columnLabel));
					}
					dataResultMap.add(rowData);
				}
			}
		}
		return dataResultMap;
	}
}
