package com.openport.marketplace.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.openport.marketplace.constants.LogisticsFilter;
import com.openport.marketplace.json.ServiceException;
import com.openport.marketplace.model.LogisticsTab;

// created by jero.dungog@openport.come
public class LogisticsSearch {

	static final transient Logger log = Logger.getLogger(LogisticsSearch.class);
	
	
	public List<LogisticsTab> searchBaseQueryByManualBatchTendering(String accountCode, String date1, String date2, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForManualBatchTendering(getBaseQueryBody(), accountCode, date1, date2);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);
		
		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForManualBatchTendering(String baseQuery, String accountCode, String date1, String date2) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD,a.SHIPPER_CD))");
		query.append(" AND (carrier.SENT_204_FLG IS NULL  OR carrier.SENT_204_FLG = 'N')");
		query.append(" AND (carrier.ACCEPT_990_FLG IS NULL OR carrier.ACCEPT_990_FLG = 'N')");
		query.append(" AND a.CREATE_TSTAMP BETWEEN");
		query.append(" '").append(date1).append("' AND");
		query.append(" '").append(date2).append("'");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseProductCodeQueryByProductCode(String accountCode, String productCode, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseProductCodeQueryForProductCode(getBaseProductCodeQuery(), accountCode, productCode);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseProductCodeQueryForProductCode(String baseProductCodeQuery, String accountCode,
			String productCode) {
		StringBuilder query = new StringBuilder(baseProductCodeQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND  pol.product_cd =");
		query.append(" '").append(productCode).append("'");
		
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByPROBLPLNbr(String accountCode, String proBillNbr, int filter) { 
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForProBlPlNbr(getBaseQueryBody(), accountCode, proBillNbr);
		
		if(filter != 0) {
			query = this.setFilter(query, this.getFilter(filter));
		}
		
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForProBlPlNbr(String baseQuery, String accountCode, String proBillNbr) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND a.PRO_BILL_NBR =");
		query.append(" '").append(proBillNbr).append("'");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByStatusWaitingForAcknowledgement(String accountCode, String date1,
			String date2, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForStatusWaitingForAcknowledgement(getBaseQueryBody(), accountCode, date1, date2);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForStatusWaitingForAcknowledgement(String baseQuery, String accountCode, String date1,
			String date2) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND carrier.SENT_204_FLG = 'Y'");
		query.append(" AND (carrier.ACCEPT_990_FLG IS NULL OR carrier.ACCEPT_990_FLG = 'N')");
		query.append(" AND a.CREATE_TSTAMP BETWEEN");
		query.append(" '").append(date1).append("' AND");
		query.append(" '").append(date2).append("'");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByStatusNotTendereYet(String accountCode, String date1, String date2, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForStatusNotTenderedYet(getBaseQueryBody(), accountCode, date1, date2);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForStatusNotTenderedYet(String baseQuery, String accountCode, String date1, String date2) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND (carrier.SENT_204_FLG IS NULL OR carrier.SENT_204_FLG= 'N')");
		query.append(" AND a.CREATE_TSTAMP BETWEEN");
		query.append(" '").append(date1).append("' AND");
		query.append(" '").append(date2).append("'");
		
		return query.toString();

	}

	public List<LogisticsTab> searchBaseQueryByStatusTendered(String accountCode, String date1, String date2, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForStatusTendered(getBaseQueryBody(), accountCode, date1, date2);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForStatusTendered(String baseQuery, String accountCode, String date1, String date2) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND carrier.SENT_204_FLG = 'Y'");
		query.append(" AND a.CREATE_TSTAMP BETWEEN");
		query.append(" '").append( date1).append("' and");
		query.append(" '").append(date2).append("'");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByPickupDate(String accountCode, String date1, String date2, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForPickupDate(getBaseQueryBody(), accountCode, date1, date2);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForPickupDate(String baseQuery, String accountCode, String date1, String date2) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND a.AVAILABLE_DT BETWEEN");
		query.append(" '").append(date1).append("' AND");
		query.append(" '").append(date2).append("'");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByPOOrderNbr(String accountCode, String poOrderNbr, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForPOOrderNbr(getBaseQueryBody(), accountCode, poOrderNbr);
		
		if(filter != 0) {
			query = this.setFilter(query, this.getFilter(filter));
		}
		
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForPOOrderNbr(String baseQuery, String accountCode, String poOrderNbr) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND a.PO_NBR IN ");
		query.append(" (").append(poOrderNbr).append(")");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByConsigneeOrder(String accountCode, String purchaseOrderNumber, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForConsigneeOrder(getBaseQueryBody(), accountCode, purchaseOrderNumber);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForConsigneeOrder(String baseQuery, String accountCode, String purchaseOrderNumber) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND a.PURCHASE_ORDER_NUMBER =");
		query.append(" '").append(purchaseOrderNumber).append("'");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByDeliveryShipmentSOReleaseNbr(String accountCode, String releaseNbr, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForDeliveryShipmentSOReleaseNbr(getBaseQueryBody(), accountCode, releaseNbr);
		
		if(filter != 0) {
			query = this.setFilter(query, this.getFilter(filter));
		}
		
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForDeliveryShipmentSOReleaseNbr(String baseQuery, String accountCode,
			String releaseNbr) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND a.RELEASE_NBR IN");
		query.append(" (").append(releaseNbr).append(")");
		
		return query.toString();
	}

	public List<LogisticsTab> searchBaseQueryByDateCreated(String accountCode, String date1, String date2, int filter) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();
		String query = getBaseQueryForByDateCreated(getBaseQueryBody(), accountCode, date1, date2);
		query = this.setFilter(query, this.getFilter(filter));
		resultsList = getResults(query);

		log.info(query);
		log.info("size: " + resultsList.size());
		
		return resultsList;
	}

	private String getBaseQueryForByDateCreated(String baseQuery, String accountCode, String date1, String date2) {
		StringBuilder query = new StringBuilder(baseQuery);
		query.append("WHERE (");
		query.append("'").append(accountCode).append("'");
		query.append(" IN (a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD))");
		query.append(" AND a.CREATE_TSTAMP BETWEEN");
		query.append(" '").append(date1).append("'");
		query.append(" AND");
		query.append(" '").append(date2).append("'");
		
		return query.toString();
	}
	
	private LogisticsFilter getFilter(int filter) {
		switch(filter) {
			case 0: 
				return LogisticsFilter.ALL;
			case 1: 
				return LogisticsFilter.EXCLUDE_CONSOLIDATED_ORDERS;
			case 2: 
				return LogisticsFilter.ONLY_CONSOLIDATED_LOADS;
			case 3: 
				return LogisticsFilter.EXCLUDE_CANCELLED_SHIPMENT;
			default:
				return LogisticsFilter.ALL;
		}
	}
	
	private String setFilter(String base, LogisticsFilter filter) {
		StringBuffer query = new StringBuffer(base);
		
		switch(filter) {
			case ALL: 
				query.append(" AND a.PO_TYPE_CD <> 'LPO'");
				break;
			case EXCLUDE_CONSOLIDATED_ORDERS: 
				query.append(" AND a.PO_TYPE_CD = 'DPO'");
				break;
			case ONLY_CONSOLIDATED_LOADS: 
				query.append(" AND a.PO_TYPE_CD = 'VPO'");
				break;
			case EXCLUDE_CANCELLED_SHIPMENT: 
				query.append(" AND a.CANCELLED_DT is null");
				break;
			default:
				break; 
		}
		
		return query.toString();
	}
	
	private List<LogisticsTab> getResults(String query) {
		List<LogisticsTab> resultsList = new ArrayList<LogisticsTab>();

		try (Connection connection = DbConfigMarketPlace.getDataSource().getConnection();
				PreparedStatement pstmt = connection.prepareStatement(query);) {

			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					LogisticsTab resultObject = new LogisticsTab();
					resultObject.setAssigned_carrier(rs.getString("assigned_carrier"));
					resultObject.setCategory(rs.getString("category"));
					resultObject.setConsignee_order_nbr(rs.getString("consignee_order_nbr"));
					resultObject.setCUSTOMER_CD(rs.getString("CUSTOMER_CD"));
					resultObject.setDelivery_nbr(rs.getString("delivery_nbr"));
					resultObject.setDest_address(rs.getString("dest_address"));
					resultObject.setDest_city(rs.getString("dest_city"));
					resultObject.setDest_district(rs.getString("dest_district"));
					resultObject.setDest_state_province(rs.getString("dest_state_province"));
					resultObject.setDestination(rs.getString("destination"));
					resultObject.setDriver_phone(rs.getString("driver_phone"));
					resultObject.setEquipment_type(rs.getString("equipment_type"));
					resultObject.setOrder_nbr(rs.getString("order_nbr"));
					resultObject.setOrder_type(rs.getString("order_type"));
					resultObject.setOrder_upload_Dt(rs.getString("Order_upload_Dt"));
					resultObject.setOrigin(rs.getString("origin"));
					resultObject.setPARENT_PARTNER_CD(rs.getString("PARENT_PARTNER_CD"));
					resultObject.setPARTNER_CD(rs.getString("PARTNER_CD"));
					resultObject.setPickup_dt(rs.getString("pickup_dt"));
					resultObject.setPRO_BILL_NBR(rs.getString("PRO_BILL_NBR"));
					resultObject.setRated_cost(rs.getString("rated_cost"));
					resultObject.setRecommended_carrier(rs.getString("recommended_carrier"));
					resultObject.setRequested_delivery_dt(rs.getString("requested_delivery_dt"));
					resultObject.setSales_channel(rs.getString("sales_channel"));
					resultObject.setSHIPPER_CD(rs.getString("SHIPPER_CD"));
					resultObject.setStatus(rs.getString("status"));
					resultObject.setTotal_qty(rs.getInt("total_qty"));
					resultObject.setTransportation(rs.getString("transportation"));
					resultObject.setUom(rs.getString("uom"));
					resultObject.setVolume(rs.getInt("volume"));
					resultObject.setWeight(rs.getInt("weight"));
					resultObject.setLegNumber(rs.getString("legNbr"));
					resultObject.setProductCode(rs.getString("product_code"));
					resultObject.setParent(rs.getString("parent"));
					resultObject.setChild(rs.getString("child"));
					resultObject.setVpo(rs.getString("vpo"));
					resultObject.setComment(rs.getString("comment"));
					
					resultsList.add(resultObject);
				}
				return resultsList;
			}
		} catch (Exception e) {
			log.error("Error message: " + e, e);
			new ServiceException(Response.Status.CONFLICT);
		}
		return resultsList;
	}

	public String getBaseQueryBody() {
		String queryBody = "SELECT DISTINCT" 
				+ " (SELECT rc.CARRIER_CD "
				+ " FROM  tms_utf8_db.po_release_carrier rc"
				+ " WHERE a.PO_NBR = rc.PO_NBR"
				+ " AND a.RELEASE_NBR = rc.RELEASE_NBR"
				+ " AND a.PARTNER_CD = rc.PARTNER_CD"
				+ " AND a.PARENT_PARTNER_CD = rc.PARENT_PARTNER_CD"
				+ " AND a.CUSTOMER_CD = rc.CUSTOMER_CD"
				+ " AND a.SHIPPER_CD = rc.SHIPPER_CD"
				+ " AND a.EQUIPMENT_SHIPMENT_SEQ_NBR = rc.EQUIPMENT_SHIPMENT_SEQ_NBR"
				+ " AND leg.TRANSPORT_LEG_ID = rc.TRANSPORT_LEG_ID"
				+ " AND rc.CHARGE_TYPE_CD = 'C'"
				+ " AND rc.BR_FLG = 'Y' LIMIT 1) AS assigned_carrier,"
				+ " a.SHIPMENT_STATUS_CD AS category,"
				+ " a.PURCHASE_ORDER_NUMBER AS consignee_order_nbr," 
				+ " coalesce(a.FINAL_DELIVERY_ADDR, deliver.LINE_1_ADDR)  AS dest_address,"
				+ " coalesce(a.FINAL_DELIVERY_CITY, deliver.CITY_NAME) AS dest_city,"
				+ " coalesce(a.FINAL_DELIVERY_DISTRICT, deliver.DISTRICT_NAME) AS dest_district,"
				+ " coalesce(a.FINAL_DELIVERY_PROVINCE, deliver.STATE_NAME) AS dest_state_province,"
				+ " coalesce(a.FINAL_DELIVERY_NAME, deliver.LOCATION_NAME) AS destination,"
				+ " f.phone_nbr AS driver_phone," + " equipment.EQUIPMENT_TYPE_CD AS equipment_type,"
				+ " a.RELEASE_TYPE AS order_type," + " pickup.LOCATION_NAME AS origin,"
				+ " a.AVAILABLE_DT AS pickup_dt," + " a.PRO_BILL_NBR AS PRO_BILL_NBR," + " a.PO_NBR AS order_nbr, "
				+ " (select case "
				+ " when count(*) > 0 then ("
				+ " select case when SUM(TOTAL_TRANSPORT_AMT) = 1 then 0 else SUM(TOTAL_TRANSPORT_AMT) end "
				+ " from tms_utf8_db.po_release_carrier "
				+ " where rc.CHARGE_TYPE_CD = CHARGE_TYPE_CD"
				+ " and BR_FLG = 'Y'"
				+ " and rc.PARENT_PARTNER_CD = PARENT_PARTNER_CD"
				+ " and rc.PARTNER_CD = PARTNER_CD"
				+ " and rc.SHIPPER_CD = SHIPPER_CD"
				+ " and rc.CUSTOMER_CD = CUSTOMER_CD"
				+ " and rc.po_nbr = po_nbr"
				+ " and a.RELEASE_NBR = RELEASE_NBR"
				+ " and leg.TRANSPORT_LEG_ID = TRANSPORT_LEG_ID"
				+ " ) else ("
				+ " (select SUM(cc.CHARGE_AMT) "
				+ " from tms_utf8_db.carrier_charge cc"
				+ " where cc.CHARGE_TYPE_CD = 'C'"
				+ " and cc.CHARGE_CD in ('FRT', 'FSC', 'MISC')"
				+ " and cc.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " and cc.PARTNER_CD = a.PARTNER_CD"
				+ " and cc.SHIPPER_CD = a.SHIPPER_CD"
				+ " and cc.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " and cc.RELEASE_NBR = a.RELEASE_NBR"
				+ " and cc.po_nbr = a.PO_NBR"
				+ " and leg.TRANSPORT_LEG_ID = cc.TRANSPORT_LEG_ID"
				+ " and cc.CARRIER_SEQ_NBR = 1) - "
				+ " (select SUM(cc.CHARGE_AMT) "
				+ " from tms_utf8_db.carrier_charge cc"
				+ " where cc.CHARGE_TYPE_CD = 'C'"
				+ " and cc.CHARGE_CD in ('DSCFRT')"
				+ " and cc.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " and cc.PARTNER_CD = a.PARTNER_CD"
				+ " and cc.SHIPPER_CD = a.SHIPPER_CD"
				+ " and cc.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " and cc.RELEASE_NBR = a.RELEASE_NBR"
				+ " and cc.po_nbr = a.PO_NBR"
				+ " and leg.TRANSPORT_LEG_ID = cc.TRANSPORT_LEG_ID"
				+ " and cc.CARRIER_SEQ_NBR = 1)"
				+ " ) end"
				+ " from tms_utf8_db.po_release_carrier rc "
				+ " where rc.CHARGE_TYPE_CD = 'C' "
				+ " and rc.BR_FLG = 'Y'"
				+ " and rc.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " and rc.PARTNER_CD = a.PARTNER_CD"
				+ " and rc.SHIPPER_CD = a.SHIPPER_CD"
				+ " and rc.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " and rc.RELEASE_NBR = a.RELEASE_NBR"
				+ " and leg.TRANSPORT_LEG_ID = rc.TRANSPORT_LEG_ID"
				+ " and rc.po_nbr = a.PO_NBR ) AS rated_cost,"
			    + " COALESCE((SELECT "
			    + "     rc.CARRIER_CD"
			    + " FROM"
			    + "     tms_utf8_db.po_release_carrier rc"
			    + " WHERE"
			    + "     (a.PO_NBR = rc.PO_NBR)"
			    + "         AND (a.RELEASE_NBR = rc.RELEASE_NBR)"
			    + "         AND (a.PARTNER_CD = rc.PARTNER_CD)"
			    + "         AND (a.PARENT_PARTNER_CD = rc.PARENT_PARTNER_CD)"
			    + "         AND (a.CUSTOMER_CD = rc.CUSTOMER_CD)"
			    + "         AND (a.SHIPPER_CD = rc.SHIPPER_CD) "
			    + "         AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = rc.EQUIPMENT_SHIPMENT_SEQ_NBR)"
			    + "         AND (leg.TRANSPORT_LEG_ID = rc.TRANSPORT_LEG_ID)"
			    + "         AND (rc.CHARGE_TYPE_CD = 'C') "
			    + "         AND (rc.CARRIER_SEQ_NBR = 1) LIMIT 1), "
			    + " (SELECT "
			    + "     leg.RECOMMENDED_CARRIER_CD"
			    + " FROM"
			    + "     tms_utf8_db.transportation_leg leg"
			    + " WHERE"
			    + "     (a.PO_NBR = leg.PO_NBR)"
			    + "         AND (a.RELEASE_NBR = leg.RELEASE_NBR)"
			    + "         AND (a.PARTNER_CD = leg.PARTNER_CD)"
			    + "         AND (a.PARENT_PARTNER_CD = leg.PARENT_PARTNER_CD)"
			    + "         AND (a.CUSTOMER_CD = leg.CUSTOMER_CD)"
			    + "         AND (a.SHIPPER_CD = leg.SHIPPER_CD) "
			    + "         AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = leg.EQUIPMENT_SHIPMENT_SEQ_NBR) LIMIT 1)) AS recommended_carrier,"
				+ " a.RELEASE_NBR AS delivery_nbr," + " a.REQUESTED_DELIVERY_DT AS requested_delivery_dt,"
				+ " a.BUSINESS_RULE_CD AS sales_channel," + " a.LAST_EVENT AS status,"
				+ " a.TRANSPORT_MODE_CD AS transportation, coalesce(unit.UOM_DESCRIPTION_TXT, a.QTY_UOM) AS uom, "
				+ " a.CREATE_TSTAMP AS Order_upload_Dt,"
				+ " a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD,a.SHIPPER_CD,"
				+ " coalesce(a.TRANSPORTATION_LEG_CNT, 1) AS legNbr,"
				+ " (select max(li.PRODUCT_DESC_TXT) FROM tms_utf8_db.PO_LINE_ITEM li" 
				+ "        where (a.PO_NBR = li.PO_NBR)"
			    + "                  and (a.RELEASE_NBR = li.RELEASE_NBR)"
			    + "                  and (a.PARTNER_CD = li.PARTNER_CD)"
			    + "                  and (a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD)"
			    + "                 and (a.CUSTOMER_CD = li.CUSTOMER_CD)"
			    + "                  and (a.SHIPPER_CD = li.SHIPPER_CD)"
			    + "       ) AS product_code,"
				+ " (SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END FROM tms_utf8_db.po_line_item li"
				+ " WHERE li.RELEASE_NBR = a.RELEASE_NBR"
				+ " AND li.ACTUAL_PO_RELEASE_NBR <> li.release_nbr"
				+ " AND li.ACTUAL_PO_RELEASE_NBR IS NOT NULL"
				+ " AND a.PARTNER_CD = li.PARTNER_CD"
				+ " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
				+ " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
				+ " AND a.SHIPPER_CD = li.SHIPPER_CD LIMIT 1) AS parent,"
				+ " (SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END FROM tms_utf8_db.po_line_item li"
				+ " WHERE li.RELEASE_NBR = a.RELEASE_NBR"
				+ " AND (li.ACTUAL_PO_RELEASE_NBR = li.release_nbr OR li.ACTUAL_PO_RELEASE_NBR IS NULL)"
				+ " AND a.PARTNER_CD = li.PARTNER_CD"
				+ " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
				+ " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
				+ " AND a.SHIPPER_CD = li.SHIPPER_CD LIMIT 1) AS child,"
				+ " (SELECT li.PO_NBR"
				+ " FROM tms_utf8_db.po_line_item li"
				+ " WHERE a.PO_NBR <> ''"
				+ " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
				+ " AND a.PARTNER_CD = li.PARTNER_CD"
				+ " AND a.SHIPPER_CD = li.SHIPPER_CD"
				+ " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
				+ " AND li.ACTUAL_PO_NBR = a.PO_NBR order by li.release_line_nbr asc limit 1) AS vpo,"
 			    + " (SELECT CASE WHEN COUNT(*) > 0 THEN"
			    + " (SELECT SUM(aa.TOTAL_WT)"
			    + " FROM tms_utf8_db.po_header AS aa"
			    + " WHERE aa.PO_NBR IN (SELECT ACTUAL_PO_NBR"
			    + " FROM tms_utf8_db.po_line_item"
			    + " WHERE po_nbr = a.PO_NBR and a.PO_NBR <> '')"
			    + " AND aa.PO_TYPE_CD = 'LPO'"
			    + " AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
			    + " AND aa.PARTNER_CD = a.PARTNER_CD"
			    + " AND aa.CUSTOMER_CD = a.CUSTOMER_CD"
			    + " AND aa.SHIPPER_CD = a.SHIPPER_CD)"
			    + " ELSE a.TOTAL_WT END"
			    + " FROM tms_utf8_db.po_line_item li"
			    + " WHERE a.PO_NBR = li.po_nbr"
			    + " AND a.PO_NBR <> ''"
			    + " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
			    + " AND a.PARTNER_CD = li.PARTNER_CD"
			    + " AND a.SHIPPER_CD = li.SHIPPER_CD"
			    + " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
			    + " AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS weight,"
			    + " (SELECT CASE WHEN COUNT(*) > 0 THEN"
			    + " (SELECT SUM(aa.TOTAL_VOL)"
			    + " FROM tms_utf8_db.po_header AS aa"
			    + " WHERE aa.PO_NBR IN (SELECT ACTUAL_PO_NBR"
			    + " FROM tms_utf8_db.po_line_item"
			    + " WHERE po_nbr = a.PO_NBR and a.PO_NBR <> '')"
			    + " AND aa.PO_TYPE_CD = 'LPO'"
			    + " AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
			    + " AND aa.PARTNER_CD = a.PARTNER_CD"
			    + " AND aa.CUSTOMER_CD = a.CUSTOMER_CD"
			    + " AND aa.SHIPPER_CD = a.SHIPPER_CD)"
			    + " ELSE a.TOTAL_VOL END"
			    + " FROM tms_utf8_db.po_line_item li"
			    + " WHERE a.PO_NBR = li.po_nbr"
			    + " AND a.PO_NBR <> ''"
			    + " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
			    + " AND a.PARTNER_CD = li.PARTNER_CD"
			    + " AND a.SHIPPER_CD = li.SHIPPER_CD"
			    + " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
			    + " AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS volume,"
			    + " (SELECT CASE WHEN COUNT(*) > 0 THEN"
			    + " (SELECT SUM(aa.TOTAL_QTY)"
			    + " FROM tms_utf8_db.po_header AS aa"
			    + " WHERE aa.PO_NBR IN (SELECT ACTUAL_PO_NBR"
			    + " FROM tms_utf8_db.po_line_item"
			    + " WHERE po_nbr = a.PO_NBR and a.PO_NBR <> '')"
			    + " AND aa.PO_TYPE_CD = 'LPO'"
			    + " AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
			    + " AND aa.PARTNER_CD = a.PARTNER_CD"
			    + " AND aa.CUSTOMER_CD = a.CUSTOMER_CD"
			    + " AND aa.SHIPPER_CD = a.SHIPPER_CD)"
			    + " ELSE a.TOTAL_QTY END"
			    + " FROM tms_utf8_db.po_line_item li"
			    + " WHERE a.PO_NBR = li.po_nbr"
			    + " AND a.PO_NBR <> ''"
			    + " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
			    + " AND a.PARTNER_CD = li.PARTNER_CD"
			    + " AND a.SHIPPER_CD = li.SHIPPER_CD"
			    + " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
			    + " AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS total_qty,"
				+ " (select poc.COMMENT_TXT from tms_utf8_db.po_comment poc"
			    + " where a.PO_NBR = poc.po_nbr"
			    + " AND a.PO_NBR <> ''"
			    + " AND a.PARENT_PARTNER_CD = poc.PARENT_PARTNER_CD"
			    + " AND a.PARTNER_CD = poc.PARTNER_CD"
			    + " AND a.SHIPPER_CD = poc.SHIPPER_CD"
			    + " AND a.CUSTOMER_CD = poc.CUSTOMER_CD limit 1) AS comment"				
				+ " FROM ((((((((((((tms_utf8_db.po_header a " + "JOIN tms_utf8_db.po_reference_info pickup"
				+ " ON ((    (a.PO_NBR = pickup.PO_NBR) AND (a.RELEASE_NBR = pickup.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = pickup.PARTNER_CD)"
				+ " AND (a.PARENT_PARTNER_CD = pickup.PARENT_PARTNER_CD) AND (a.CUSTOMER_CD = pickup.CUSTOMER_CD)"
				+ " AND (a.SHIPPER_CD = pickup.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = pickup.EQUIPMENT_SHIPMENT_SEQ_NBR)"
				+ " AND (pickup.REFERENCE_TYPE_CD = 'Pickup')))) JOIN tms_utf8_db.po_reference_info deliver"
				+ " ON ((    (a.PO_NBR = deliver.PO_NBR) AND (a.RELEASE_NBR = deliver.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = deliver.PARTNER_CD) AND (a.PARENT_PARTNER_CD = deliver.PARENT_PARTNER_CD)"
				+ " AND (a.CUSTOMER_CD = deliver.CUSTOMER_CD) AND (a.SHIPPER_CD = deliver.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = deliver.EQUIPMENT_SHIPMENT_SEQ_NBR)"
				+ " AND (deliver.REFERENCE_TYPE_CD = 'Delivery')))) JOIN tms_utf8_db.po_equipment_requirement equipment"
				+ " ON (((a.PO_NBR = equipment.PO_NBR) AND (a.RELEASE_NBR = equipment.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = equipment.PARTNER_CD) AND (a.PARENT_PARTNER_CD = equipment.PARENT_PARTNER_CD)"
				+ " AND (a.CUSTOMER_CD = equipment.CUSTOMER_CD) AND (a.SHIPPER_CD = equipment.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = equipment.EQUIPMENT_SHIPMENT_SEQ_NBR))))"
				+ " LEFT JOIN tms_utf8_db.transportation_leg leg ON (((a.PO_NBR = leg.PO_NBR)"
				+ " AND (a.RELEASE_NBR = leg.RELEASE_NBR) AND (a.PARTNER_CD = leg.PARTNER_CD)"
				+ " AND (a.PARENT_PARTNER_CD = leg.PARENT_PARTNER_CD) AND (a.CUSTOMER_CD = leg.CUSTOMER_CD)"
				+ " AND (a.SHIPPER_CD = leg.SHIPPER_CD) AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = leg.EQUIPMENT_SHIPMENT_SEQ_NBR))))"
				+ " LEFT JOIN tms_utf8_db.po_release_carrier carrier"
				+ " ON (((a.PO_NBR = carrier.PO_NBR)"
				+ " AND (a.RELEASE_NBR = carrier.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = carrier.PARTNER_CD)"
				+ " AND (a.PARENT_PARTNER_CD = carrier.PARENT_PARTNER_CD)"
				+ " AND (a.CUSTOMER_CD = carrier.CUSTOMER_CD)"
				+ " AND (a.SHIPPER_CD = carrier.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = carrier.EQUIPMENT_SHIPMENT_SEQ_NBR)"
				+ " AND (leg.TRANSPORT_LEG_ID = carrier.TRANSPORT_LEG_ID)"
				+ " AND (carrier.CHARGE_TYPE_CD = 'C'))))"
				+ " LEFT JOIN tms_delivery_db.shipment b ON ((a.PRO_BILL_NBR = b.shipment_nbr and leg.TRANSPORT_leg_nbr = b.leg_nbr)))"
				+ " LEFT JOIN tms_delivery_db.shipment_user c ON ((b.shipment_id = c.shipment_id)))"
				+ " LEFT JOIN tms_delivery_db.mobile_user f ON ((f.mobile_user_id = c.mobile_user_id)))"
				+ " LEFT JOIN tms_delivery_db.mobile_user_provider d ON ((d.mobile_user_id = c.mobile_user_id)))"
				+ " LEFT JOIN tms_delivery_db.provider e ON ((e.Provider_Id = c.Provider_Id)))"
				+ " LEFT JOIN tms_delivery_db.status s ON ((s.Status_Id = b.Status_Id)))"
				+ " LEFT JOIN marketplace_db.unit_of_measure unit on unit.uom_cd = a.QTY_UOM)";

		return queryBody;
	}

	public String getBaseProductCodeQuery() {
		String baseProductCodeQuery = "SELECT DISTINCT "
				+ " (SELECT rc.CARRIER_CD "
				+ " FROM  tms_utf8_db.po_release_carrier rc"
				+ " WHERE a.PO_NBR = rc.PO_NBR"
				+ " AND a.RELEASE_NBR = rc.RELEASE_NBR"
				+ " AND a.PARTNER_CD = rc.PARTNER_CD"
				+ " AND a.PARENT_PARTNER_CD = rc.PARENT_PARTNER_CD"
				+ " AND a.CUSTOMER_CD = rc.CUSTOMER_CD"
				+ " AND a.SHIPPER_CD = rc.SHIPPER_CD"
				+ " AND a.EQUIPMENT_SHIPMENT_SEQ_NBR = rc.EQUIPMENT_SHIPMENT_SEQ_NBR"
				+ " AND leg.TRANSPORT_LEG_ID = rc.TRANSPORT_LEG_ID"
				+ " AND rc.CHARGE_TYPE_CD = 'C'"
				+ " AND rc.BR_FLG = 'Y' LIMIT 1) AS assigned_carrier,"
				+ " a.SHIPMENT_STATUS_CD AS category,"
				+ " a.PURCHASE_ORDER_NUMBER AS consignee_order_nbr," 
				+ " coalesce(a.FINAL_DELIVERY_ADDR, deliver.LINE_1_ADDR)  AS dest_address,"
				+ " coalesce(a.FINAL_DELIVERY_CITY, deliver.CITY_NAME) AS dest_city,"
				+ " coalesce(a.FINAL_DELIVERY_DISTRICT, deliver.DISTRICT_NAME) AS dest_district,"
				+ " coalesce(a.FINAL_DELIVERY_PROVINCE, deliver.STATE_NAME) AS dest_state_province,"
				+ " coalesce(a.FINAL_DELIVERY_NAME, deliver.LOCATION_NAME) AS destination,"
				+ " f.phone_nbr AS driver_phone,"
				+ " equipment.EQUIPMENT_TYPE_CD AS equipment_type, a.RELEASE_TYPE AS order_type,"
				+ " pickup.LOCATION_NAME AS origin, a.AVAILABLE_DT AS pickup_dt,"
				+ " a.PRO_BILL_NBR AS PRO_BILL_NBR, a.PO_NBR AS order_nbr, "
				+ " (select case "
				+ " when count(*) > 0 then ("
				+ " select case when SUM(TOTAL_TRANSPORT_AMT) = 1 then 0 else SUM(TOTAL_TRANSPORT_AMT) end "
				+ " from tms_utf8_db.po_release_carrier "
				+ " where rc.CHARGE_TYPE_CD = CHARGE_TYPE_CD"
				+ " and BR_FLG = 'Y'"
				+ " and rc.PARENT_PARTNER_CD = PARENT_PARTNER_CD"
				+ " and rc.PARTNER_CD = PARTNER_CD"
				+ " and rc.SHIPPER_CD = SHIPPER_CD"
				+ " and rc.CUSTOMER_CD = CUSTOMER_CD"
				+ " and rc.po_nbr = po_nbr"
				+ " and a.RELEASE_NBR = RELEASE_NBR"
				+ " and leg.TRANSPORT_LEG_ID = TRANSPORT_LEG_ID"
				+ " ) else ("
				+ " (select SUM(cc.CHARGE_AMT) "
				+ " from tms_utf8_db.carrier_charge cc"
				+ " where cc.CHARGE_TYPE_CD = 'C'"
				+ " and cc.CHARGE_CD in ('FRT', 'FSC', 'MISC')"
				+ " and cc.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " and cc.PARTNER_CD = a.PARTNER_CD"
				+ " and cc.SHIPPER_CD = a.SHIPPER_CD"
				+ " and cc.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " and cc.RELEASE_NBR = a.RELEASE_NBR"
				+ " and cc.po_nbr = a.PO_NBR"
				+ " and leg.TRANSPORT_LEG_ID = cc.TRANSPORT_LEG_ID"
				+ " and cc.CARRIER_SEQ_NBR = 1) - "
				+ " (select SUM(cc.CHARGE_AMT) "
				+ " from tms_utf8_db.carrier_charge cc"
				+ " where cc.CHARGE_TYPE_CD = 'C'"
				+ " and cc.CHARGE_CD in ('DSCFRT')"
				+ " and cc.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " and cc.PARTNER_CD = a.PARTNER_CD"
				+ " and cc.SHIPPER_CD = a.SHIPPER_CD"
				+ " and cc.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " and cc.RELEASE_NBR = a.RELEASE_NBR"
				+ " and cc.po_nbr = a.PO_NBR"
				+ " and leg.TRANSPORT_LEG_ID = cc.TRANSPORT_LEG_ID"
				+ " and cc.CARRIER_SEQ_NBR = 1)"
				+ " ) end"
				+ " from tms_utf8_db.po_release_carrier rc "
				+ " where rc.CHARGE_TYPE_CD = 'C' "
				+ " and rc.BR_FLG = 'Y'"
				+ " and rc.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " and rc.PARTNER_CD = a.PARTNER_CD"
				+ " and rc.SHIPPER_CD = a.SHIPPER_CD"
				+ " and rc.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " and rc.RELEASE_NBR = a.RELEASE_NBR"
				+ " and leg.TRANSPORT_LEG_ID = rc.TRANSPORT_LEG_ID"
				+ " and rc.po_nbr = a.PO_NBR ) AS rated_cost,"
			    + " COALESCE((SELECT "
			    + "     rc.CARRIER_CD"
			    + " FROM"
			    + "     tms_utf8_db.po_release_carrier rc"
			    + " WHERE"
			    + "     (a.PO_NBR = rc.PO_NBR)"
			    + "         AND (a.RELEASE_NBR = rc.RELEASE_NBR)"
			    + "         AND (a.PARTNER_CD = rc.PARTNER_CD)"
			    + "         AND (a.PARENT_PARTNER_CD = rc.PARENT_PARTNER_CD)"
			    + "         AND (a.CUSTOMER_CD = rc.CUSTOMER_CD)"
			    + "         AND (a.SHIPPER_CD = rc.SHIPPER_CD) "
			    + "         AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = rc.EQUIPMENT_SHIPMENT_SEQ_NBR)"
			    + "         AND (leg.TRANSPORT_LEG_ID = rc.TRANSPORT_LEG_ID)"
			    + "         AND (rc.CHARGE_TYPE_CD = 'C')"
			    + "         AND (rc.CARRIER_SEQ_NBR = 1) LIMIT 1), "
			    + " (SELECT "
			    + "     leg.RECOMMENDED_CARRIER_CD"
			    + " FROM"
			    + "     tms_utf8_db.transportation_leg leg"
			    + " WHERE"
			    + "     (a.PO_NBR = leg.PO_NBR)"
			    + "         AND (a.RELEASE_NBR = leg.RELEASE_NBR)"
			    + "         AND (a.PARTNER_CD = leg.PARTNER_CD)"
			    + "         AND (a.PARENT_PARTNER_CD = leg.PARENT_PARTNER_CD)"
			    + "         AND (a.CUSTOMER_CD = leg.CUSTOMER_CD)"
			    + "         AND (a.SHIPPER_CD = leg.SHIPPER_CD) "
			    + "         AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = leg.EQUIPMENT_SHIPMENT_SEQ_NBR) LIMIT 1)) AS recommended_carrier,"
				+ " a.RELEASE_NBR AS delivery_nbr, a.REQUESTED_DELIVERY_DT AS requested_delivery_dt,"
				+ " a.BUSINESS_RULE_CD AS sales_channel, a.LAST_EVENT AS status,"
				+ " a.TRANSPORT_MODE_CD AS transportation, coalesce(unit.UOM_DESCRIPTION_TXT, a.QTY_UOM)  AS uom,"
				+ " a.CREATE_TSTAMP AS Order_upload_Dt,"
				+ " a.PARENT_PARTNER_CD, a.PARTNER_CD, a.CUSTOMER_CD, a.SHIPPER_CD, carrier.SENT_204_FLG AS sent_204_flg,"
				+ " coalesce(a.TRANSPORTATION_LEG_CNT, 1) AS legNbr,"
				+ " (select max(li.PRODUCT_DESC_TXT) FROM tms_utf8_db.PO_LINE_ITEM li" 
				+ "        where (a.PO_NBR = li.PO_NBR)"
			    + "                  and (a.RELEASE_NBR = li.RELEASE_NBR)"
			    + "                  and (a.PARTNER_CD = li.PARTNER_CD)"
			    + "                  and (a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD)"
			    + "                 and (a.CUSTOMER_CD = li.CUSTOMER_CD)"
			    + "                  and (a.SHIPPER_CD = li.SHIPPER_CD)"
			    + "       ) AS product_code,"
			    + " (SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END FROM tms_utf8_db.po_line_item li"
			    + " WHERE li.RELEASE_NBR = a.RELEASE_NBR"
			    + " AND li.ACTUAL_PO_RELEASE_NBR <> li.release_nbr"
			    + " AND li.ACTUAL_PO_RELEASE_NBR IS NOT NULL"
			    + " AND a.PARTNER_CD = li.PARTNER_CD"
			    + " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
			    + " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
			    + " AND a.SHIPPER_CD = li.SHIPPER_CD LIMIT 1) AS parent,"
			    + " (SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END FROM tms_utf8_db.po_line_item li"
			    + " WHERE li.RELEASE_NBR = a.RELEASE_NBR"
			    + " AND (li.ACTUAL_PO_RELEASE_NBR = li.release_nbr OR li.ACTUAL_PO_RELEASE_NBR IS NULL)"
			    + " AND a.PARTNER_CD = li.PARTNER_CD"
			    + " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
			    + " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
			    + " AND a.SHIPPER_CD = li.SHIPPER_CD LIMIT 1) AS child,"
				+ " (SELECT li.PO_NBR"
				+ " FROM tms_utf8_db.po_line_item li"
				+ " WHERE a.PO_NBR <> ''"
				+ " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
				+ " AND a.PARTNER_CD = li.PARTNER_CD"
				+ " AND a.SHIPPER_CD = li.SHIPPER_CD"
				+ " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
				+ " AND li.ACTUAL_PO_NBR = a.PO_NBR order by li.release_line_nbr asc limit 1) AS vpo,"
				+ " (SELECT CASE WHEN COUNT(*) > 0 THEN"
				+ " (SELECT SUM(aa.TOTAL_WT)"
				+ " FROM tms_utf8_db.po_header AS aa"
				+ " WHERE aa.PO_NBR IN (SELECT ACTUAL_PO_NBR"
				+ " FROM tms_utf8_db.po_line_item"
				+ " WHERE po_nbr = a.PO_NBR and a.PO_NBR <> '')"
				+ " AND aa.PO_TYPE_CD = 'LPO'"
				+ " AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " AND aa.PARTNER_CD = a.PARTNER_CD"
				+ " AND aa.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " AND aa.SHIPPER_CD = a.SHIPPER_CD)"
				+ " ELSE a.TOTAL_WT END"
				+ " FROM tms_utf8_db.po_line_item li"
				+ " WHERE a.PO_NBR = li.po_nbr"
				+ " AND a.PO_NBR <> ''"
				+ " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
				+ " AND a.PARTNER_CD = li.PARTNER_CD"
				+ " AND a.SHIPPER_CD = li.SHIPPER_CD"
				+ " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
				+ " AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS weight,"
				+ " (SELECT CASE WHEN COUNT(*) > 0 THEN"
				+ " (SELECT SUM(aa.TOTAL_VOL)"
				+ " FROM tms_utf8_db.po_header AS aa"
				+ " WHERE aa.PO_NBR IN (SELECT ACTUAL_PO_NBR"
				+ " FROM tms_utf8_db.po_line_item"
				+ " WHERE po_nbr = a.PO_NBR and a.PO_NBR <> '')"
				+ " AND aa.PO_TYPE_CD = 'LPO'"
				+ " AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " AND aa.PARTNER_CD = a.PARTNER_CD"
				+ " AND aa.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " AND aa.SHIPPER_CD = a.SHIPPER_CD)"
				+ " ELSE a.TOTAL_VOL END"
				+ " FROM tms_utf8_db.po_line_item li"
				+ " WHERE a.PO_NBR = li.po_nbr"
				+ " AND a.PO_NBR <> ''"
				+ " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
				+ " AND a.PARTNER_CD = li.PARTNER_CD"
				+ " AND a.SHIPPER_CD = li.SHIPPER_CD"
				+ " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
				+ " AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS volume,"
				+ " (SELECT CASE WHEN COUNT(*) > 0 THEN"
				+ " (SELECT SUM(aa.TOTAL_QTY)"
				+ " FROM tms_utf8_db.po_header AS aa"
				+ " WHERE aa.PO_NBR IN (SELECT ACTUAL_PO_NBR"
				+ " FROM tms_utf8_db.po_line_item"
				+ " WHERE po_nbr = a.PO_NBR and a.PO_NBR <> '')"
				+ " AND aa.PO_TYPE_CD = 'LPO'"
				+ " AND aa.PARENT_PARTNER_CD = a.PARENT_PARTNER_CD"
				+ " AND aa.PARTNER_CD = a.PARTNER_CD"
				+ " AND aa.CUSTOMER_CD = a.CUSTOMER_CD"
				+ " AND aa.SHIPPER_CD = a.SHIPPER_CD)"
				+ " ELSE a.TOTAL_QTY END"
				+ " FROM tms_utf8_db.po_line_item li"
				+ " WHERE a.PO_NBR = li.po_nbr"
				+ " AND a.PO_NBR <> ''"
				+ " AND a.PARENT_PARTNER_CD = li.PARENT_PARTNER_CD"
				+ " AND a.PARTNER_CD = li.PARTNER_CD"
				+ " AND a.SHIPPER_CD = li.SHIPPER_CD"
				+ " AND a.CUSTOMER_CD = li.CUSTOMER_CD"
				+ " AND (LENGTH(TRIM(COALESCE(li.ACTUAL_PO_RELEASE_NBR, ''))) > 0)) AS total_qty,"
				+ " (select poc.COMMENT_TXT from tms_utf8_db.po_comment poc"
				+ " where a.PO_NBR = poc.po_nbr"
				+ " AND a.PO_NBR <> ''"
				+ " AND a.PARENT_PARTNER_CD = poc.PARENT_PARTNER_CD"
				+ " AND a.PARTNER_CD = poc.PARTNER_CD"
				+ " AND a.SHIPPER_CD = poc.SHIPPER_CD"
				+ " AND a.CUSTOMER_CD = poc.CUSTOMER_CD limit 1) AS comment"			    
			    + " FROM (((((((((((((tms_utf8_db.po_header a JOIN tms_utf8_db.po_reference_info pickup"
				+ " ON (((a.PO_NBR = pickup.PO_NBR) AND (a.RELEASE_NBR = pickup.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = pickup.PARTNER_CD)" + " AND (a.PARENT_PARTNER_CD = pickup.PARENT_PARTNER_CD)"
				+ " AND (a.CUSTOMER_CD = pickup.CUSTOMER_CD)" + " AND (a.SHIPPER_CD = pickup.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = pickup.EQUIPMENT_SHIPMENT_SEQ_NBR)"
				+ " AND (pickup.REFERENCE_TYPE_CD = 'Pickup'))))"
				+ " JOIN tms_utf8_db.po_reference_info deliver ON (((a.PO_NBR = deliver.PO_NBR)"
				+ " AND (a.RELEASE_NBR = deliver.RELEASE_NBR) AND (a.PARTNER_CD = deliver.PARTNER_CD)"
				+ " AND (a.PARENT_PARTNER_CD = deliver.PARENT_PARTNER_CD) AND (a.CUSTOMER_CD = deliver.CUSTOMER_CD)"
				+ " AND (a.SHIPPER_CD = deliver.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = deliver.EQUIPMENT_SHIPMENT_SEQ_NBR)"
				+ " AND (deliver.REFERENCE_TYPE_CD = 'Delivery')))) JOIN tms_utf8_db.po_line_item pol"
				+ " ON (((a.PO_NBR = pol.PO_NBR) AND (a.RELEASE_NBR = pol.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = pol.PARTNER_CD)" + " AND (a.PARENT_PARTNER_CD = pol.PARENT_PARTNER_CD)"
				+ " AND (a.CUSTOMER_CD = pol.CUSTOMER_CD) AND (a.SHIPPER_CD = pol.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = pol.EQUIPMENT_SHIPMENT_SEQ_NBR))))"
				+ " JOIN tms_utf8_db.po_equipment_requirement equipment"
				+ " ON (((a.PO_NBR = equipment.PO_NBR) AND (a.RELEASE_NBR = equipment.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = equipment.PARTNER_CD)"
				+ " AND (a.PARENT_PARTNER_CD = equipment.PARENT_PARTNER_CD) AND (a.CUSTOMER_CD = equipment.CUSTOMER_CD)"
				+ " AND (a.SHIPPER_CD = equipment.SHIPPER_CD) AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = equipment.EQUIPMENT_SHIPMENT_SEQ_NBR))))"
				+ " LEFT JOIN tms_utf8_db.transportation_leg leg ON (((a.PO_NBR = leg.PO_NBR) AND (a.RELEASE_NBR = leg.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = leg.PARTNER_CD) AND (a.PARENT_PARTNER_CD = leg.PARENT_PARTNER_CD)"
				+ " AND (a.CUSTOMER_CD = leg.CUSTOMER_CD) AND (a.SHIPPER_CD = leg.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = leg.EQUIPMENT_SHIPMENT_SEQ_NBR))))"
				+ " LEFT JOIN tms_utf8_db.po_release_carrier carrier"
				+ " ON (((a.PO_NBR = carrier.PO_NBR)"
				+ " AND (a.RELEASE_NBR = carrier.RELEASE_NBR)"
				+ " AND (a.PARTNER_CD = carrier.PARTNER_CD)"
				+ " AND (a.PARENT_PARTNER_CD = carrier.PARENT_PARTNER_CD)"
				+ " AND (a.CUSTOMER_CD = carrier.CUSTOMER_CD)"
				+ " AND (a.SHIPPER_CD = carrier.SHIPPER_CD)"
				+ " AND (a.EQUIPMENT_SHIPMENT_SEQ_NBR = carrier.EQUIPMENT_SHIPMENT_SEQ_NBR)"
				+ " AND (leg.TRANSPORT_LEG_ID = carrier.TRANSPORT_LEG_ID)"
				+ " AND (carrier.CHARGE_TYPE_CD = 'C'))))"
				+ " LEFT JOIN tms_delivery_db.shipment b ON ((a.PRO_BILL_NBR = b.shipment_nbr and leg.TRANSPORT_leg_nbr = b.leg_nbr)))"
				+ " LEFT JOIN tms_delivery_db.shipment_user c ON ((b.shipment_id = c.shipment_id)))"
				+ " LEFT JOIN tms_delivery_db.mobile_user f ON ((f.mobile_user_id = c.mobile_user_id)))"
				+ " LEFT JOIN tms_delivery_db.mobile_user_provider d ON ((d.mobile_user_id = c.mobile_user_id)))"
				+ " LEFT JOIN tms_delivery_db.provider e ON ((e.Provider_Id = c.Provider_Id))) LEFT JOIN tms_delivery_db.status s ON ((s.Status_Id = b.Status_Id)))"
				+ " LEFT JOIN marketplace_db.unit_of_measure unit on unit.uom_cd = a.QTY_UOM)";

		return baseProductCodeQuery;
	}

}
