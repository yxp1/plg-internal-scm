package com.openport.marketplace.auth;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.util.function.Predicate;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.openport.marketplace.auth.annotation.NoAuthorization;
import com.openport.marketplace.helper.JwtTokenHelper;
import com.openport.marketplace.json.Company;
import com.openport.marketplace.json.User;
import com.openport.marketplace.local.DataHolderLocal;
import com.openport.marketplace.model.Partner;
import com.openport.marketplace.repository.PartnerRepository;
import com.openport.marketplace.repository.UserRepository;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter, ContainerResponseFilter {

	static final transient Logger log = Logger.getLogger(AuthenticationFilter.class);
	
	private final String AUTHORIZATION = "authorization";

	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		Predicate<AnnotatedElement> hasNoAuth = (aEl) -> aEl.getAnnotation(NoAuthorization.class) != null;
		boolean noAuth = hasNoAuth.test(resourceInfo.getResourceClass()) || hasNoAuth.test(resourceInfo.getResourceMethod());
		//bypass endpoint marked with NoAuthorization annotation
		if(noAuth) return;

		String authorization = requestContext.getHeaderString(AUTHORIZATION);
		String token = null;
		
		if(authorization != null && !authorization.trim().isEmpty()) {
			String[] authArray = authorization.split(" ");
			if(authArray.length > 1) {
				token = authArray[1].trim();
			}
		}

		if(token == null || !JwtTokenHelper.validateToken(token)) {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}

		int userId = (int) JwtTokenHelper.getClaimData(token, "userId");

		Company company = UserRepository.getCompanyByUserId(userId);
		
		Partner partner = null;
		try {
			partner = new PartnerRepository().getPartner(company.getCompanyCode());
		} catch (Exception e) {
			log.error("Unable to get partner");
		}
		
		DataHolderLocal.set(partner);
		DataHolderLocal.set(UserRepository.getUserByUserId(userId));
		DataHolderLocal.set(company);
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		DataHolderLocal.remove();
	}
}
