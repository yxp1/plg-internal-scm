package com.openport.marketplace.helper;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.openport.marketplace.json.User;
import com.openport.marketplace.repository.TmsParameterRepository;
import org.apache.log4j.Logger;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtTokenHelper {

    private static final String DEFAULT_KEY = "ABCDEFJHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890987654321";

    private static final String OPEN_TM_CONFIG_CD = "OPEN_TM_CONFIG";
    private static final String OPEN_TM_TOKEN_SECRET_KEY_NAME = "OPEN_TM_TOKEN_SECRET_KEY";
    private static Key SIGNED_KEY = null;

	static final transient Logger log = Logger.getLogger(JwtTokenHelper.class);

	private static Key getSignKeyFromTMSParameter() {
	    if(SIGNED_KEY == null) {
            String secretKey = TmsParameterRepository.getString(OPEN_TM_CONFIG_CD, OPEN_TM_TOKEN_SECRET_KEY_NAME, DEFAULT_KEY);
            SIGNED_KEY = new SecretKeySpec(DatatypeConverter.parseBase64Binary(secretKey), SignatureAlgorithm.HS512.getJcaName());
        }
        return SIGNED_KEY;
    }

	public static String createToken(User user, long expired) {
	    Date expiresIn = calculateExpirationDate(expired);
	    Map<String, Object> claims = new HashMap<>();

	    claims.put("userId", user.getUserId());
	    claims.put("companyId", user.getCompanyId());

		return Jwts.builder()
					.setSubject(user.getUsername())
					.setIssuedAt(new Date())
					.signWith(SignatureAlgorithm.HS512, getSignKeyFromTMSParameter())
					.setExpiration(expiresIn)
					.setClaims(claims)
					.compact();
	}

	private static Date calculateExpirationDate(long expiresIn) {
        Calendar timeout = Calendar.getInstance();
        timeout.setTimeInMillis( new Date().getTime() + TimeUnit.MINUTES.toMillis(expiresIn));
        return timeout.getTime();
    }

    public static Object getClaimData(String token, String key) {
        Jws<Claims> claims = getJwtClaims(token);
        return claims.getBody().get(key);
	}

    public static Jws<Claims> getJwtClaims(String token) {
		return Jwts.parser()
				.setSigningKey(getSignKeyFromTMSParameter())
				 .parseClaimsJws(token);
	}
	
	public static boolean validateToken(String token) {
	    
		try {
	    	getJwtClaims(token);
	
	        return true;
	    } catch (Exception e) {
		    e.printStackTrace();
	    	log.error(e);
	    }
		
		return false;
	}
	
}
