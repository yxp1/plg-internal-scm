package com.openport.marketplace.helper;

public class TemporaryPassword {

	public TemporaryPassword() {
		// TODO Auto-generated constructor stub
	}

	public static String generate() {
		return generate(7);
	}
	
	public static String generate(int length) {
		String captchStrings = "0Aa1Bb2Cc3Dd4Ee5Ff6Gg7Hh8Ii9Jj0Kk1Ll2Mm3Nn4Oo5Pp6Qq7Rr8Ss9Tt0Uu1Vv2Ww3Xx4Yy5Zz";
		String str = "";
		while (str.isEmpty()) {
			for (int i = 0; i < length; i++) {
				Double rnum = Math.floor(Math.random() * captchStrings.length());
				str += captchStrings.substring(rnum.intValue(), rnum.intValue() + 1);
			}
		}
		return str;
	}

}
