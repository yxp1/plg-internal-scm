package com.openport.marketplace.helper;

import java.sql.SQLException;
import java.util.List;


import org.apache.log4j.Logger;

import com.openport.marketplace.json.User;
import com.openport.marketplace.repository.UserRepository;

public class AuthenticationHelper {
	static final transient Logger log = Logger.getLogger(AuthenticationHelper.class);
	
	public Boolean isValidToken(String token) throws SQLException{
		List<User> userList = getTokenUserInfo(token);
		return userList != null;
	}
	
	public List<User> getUserInfo(String token)throws SQLException{
		return getTokenUserInfo(token);
	}
	
	private List<User> getTokenUserInfo(String token)throws SQLException{
		return UserRepository.getTokenUserInfo(token);
	}
}
