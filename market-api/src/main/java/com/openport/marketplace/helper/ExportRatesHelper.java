package com.openport.marketplace.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.core.UriInfo;

public class ExportRatesHelper {
	public static String generateName(String name, String ext){
		DateFormat  formatter = new SimpleDateFormat("yyyyMMddhhmmss");
		Date dt = new Date();
		return name+"_"+formatter.format(dt)+ext;
	}
	
	private static final String LOCAL_TEMPLATE_PATH = "D:\\openwb\\template\\";
	private static final String LOCAL_OUTPUT_PATH = "D:\\openwb\\output\\";
	
	private static final String REMOTE_TEMPLATE_PATH = "/opt/openport/openwb/template/";
	private static final String REMOTE_OUTPUT_PATH = "/opt/openport/openwb/output/";
	
	public static String getTemplatePath(UriInfo ui){
		if(isLocalHost(ui)){
			return LOCAL_TEMPLATE_PATH;
		}else{
			return REMOTE_TEMPLATE_PATH;
		}
	}
	
	public static String getOutputPath(UriInfo ui){
		if(isLocalHost(ui)){
			return LOCAL_OUTPUT_PATH;
		}else{
			return REMOTE_OUTPUT_PATH;
		}
	}
	
	public static Boolean isLocalHost(UriInfo ui){
		return ui.getBaseUri().toString().contains("localhost:");
	}
}
