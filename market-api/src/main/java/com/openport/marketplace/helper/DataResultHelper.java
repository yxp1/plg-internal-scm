package com.openport.marketplace.helper;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

public class DataResultHelper {
	
	@SuppressWarnings("unchecked")
	public static JsonArrayBuilder parseDateResultToJSON(List<Map<String,Object>> dataResult){
		JsonArrayBuilder jsonArray = Json.createArrayBuilder();
		for(Map<String,Object> data : dataResult){
			JsonObjectBuilder jsObject = Json.createObjectBuilder(); 
			for(String key : data.keySet()){
				if(data.get(key) != null){
					if(data.get(key).getClass() == LinkedList.class){
						JsonArrayBuilder array = parseDateResultToJSON((List<Map<String, Object>>)data.get(key));
						jsObject.add(key, array);
						continue;
					}
				}
				jsObject.add(key, String.valueOf(data.get(key)));
			}
			jsonArray.add(jsObject);
		}
		return jsonArray;
	}
	
	public static void getPrimaryKeys(List<String> keys, List<Map<String,Object>> dataResult, String primaryKey ){
		for(Map<String,Object> data : dataResult){
			keys.add(String.valueOf(data.get(primaryKey)));
		}
	}
	
	public static String createResponseJsonObject(Integer status, String message){
		JsonObjectBuilder o = Json.createObjectBuilder();
		o.add("STATUS", status);
		o.add("MESSAGE", message);
		return o.build().toString();
	}
	
	public static boolean isValidDate(String dateString) {
		
		String[] dateArr = dateString.split("/");
		
		if(dateArr.length != 3)
			return false;
		
		dateString = dateArr[2]+"/"+dateArr[0]+"/"+dateArr[1];
		
		dateString = dateString.replace("/", "");
		
	    if (dateString == null || dateString.length() != "yyyyMMdd".length()) {
	        return false;
	    }

	    int date;
	    try {
	        date = Integer.parseInt(dateString);
	    } catch (NumberFormatException e) {
	        return false;
	    }

	    int year = date / 10000;
	    int month = (date % 10000) / 100;
	    int day = date % 100;

	    // leap years calculation not valid before 1581
	    boolean yearOk = (year >= 1581) && (year <= 2500);
	    boolean monthOk = (month >= 1) && (month <= 12);
	    boolean dayOk = (day >= 1) && (day <= daysInMonth(year, month));

	    return (yearOk && monthOk && dayOk);
	}

	private static int daysInMonth(int year, int month) {
	    int daysInMonth;
	    switch (month) {
	        case 1: // fall through
	        case 3: // fall through
	        case 5: // fall through
	        case 7: // fall through
	        case 8: // fall through
	        case 10: // fall through
	        case 12:
	            daysInMonth = 31;
	            break;
	        case 2:
	            if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
	                daysInMonth = 29;
	            } else {
	                daysInMonth = 28;
	            }
	            break;
	        default:
	            // returns 30 even for nonexistant months 
	            daysInMonth = 30;
	    }
	    return daysInMonth;
	}
}
